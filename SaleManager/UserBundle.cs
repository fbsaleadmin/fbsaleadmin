//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaleManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserBundle
    {
        public int UserBundleId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> BundleId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> IsPaid { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
    }
}
