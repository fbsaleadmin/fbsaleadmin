﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class Const
    {
        public enum PaymentStatus
        {
            INIT = 0,
            PAID_NOT_ENOUGH = 1,
            COMPLETED = 2,            
            WAITING_FOR_COLLECT = 4
        }

        public enum DeliveryStatus
        {
            INIT = 0,
            DELIVERING = 1,
            COMPLETED = 2,
            COULD_NOT_DELIVERY = 3,          
            READY = 4,
        }

        public enum OrderStatus
        {
            INIT = 0,
            INCOMPLETED = 1,
            COMPLETED = 2,           
            RETURNED = 3,
            DELETED = 99,
            CANCELED = -1,
            TEMP = 10            
        }
        public enum OrderType
        {
            SALE = 0,
            RETURN = 1
        }

        public enum Status
        {
            NORMAL = 0,
            DELETED = 99
        }
        public enum ProductStatus
        {
            NORMAL = 0,
            NOT_SALE = 98,
            DELETED = 99
        }

        public enum CashType
        {
            OUTCOME = 0,
            INCOME = 1,
            DELETED = 99
        }

        public enum DebtType
        {
            OTHERS_DEBT = 0,
            SHOP_DEBT = 1
        }

        public enum EximType
        {
            IMPORT = 0,
            EXPORT = 1,
            CANCELED = 2
        }

        public enum EximStatus
        {
            INIT = 0,
            INCOMPLETED = 2,
            COMPLETED = 2,
            DELETED = 99,
            CANCELED = -1,         
            TEMP = 10
        }

        public enum PaymentType
        {
            CASH = 0,
            INTERNET_BANKING = 1,
            CASH_ON_DELIVERY = 2
        }

        public enum CashFlowType
        {
            PAID = 0,
            ORDER = 1,
            ORDER_RETURN = 2,
            IMPORT = 3,
            IMPORT_RETURN = 4,
            CANCELED = 5
        }

        public enum PaidTargetType
        {
            CUSTOMER = 0,
            VENDOR = 1,
            EMPLOYEE = 2,
            DELIVERY_VENDOR = 3
        }

        public enum UserStatus
        {
            NORMAL = 0,
            LOCKED = 98,
            DELETED = 99
        }

        public enum CommentStatus
        {
            SHOWN = 0,
            LIKED = 10,
            HIDDEN = 1,
            LIKED_HIDDEN = 11
        }

        public enum ConversationStatus
        {
            NOT_READ = 1,
            READ = 0,            
            REPLIED = 2
        }
    }
}