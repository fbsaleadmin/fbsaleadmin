﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SaleManager.Models
{
    public class WebhookMessage
    {
        [JsonProperty("object")]
        public string Object { get; set; }

        [JsonProperty("entry")]
        public List<EntryMessage> entry { get; set; }
    }

    public class EntryMessage
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("time")]
        public long time { get; set; }
        [JsonProperty("messaging")]
        public List<Messaging> messaging { get; set; }
    }

    public class Messaging
    {
        [JsonProperty("sender")]
        public Sender sender { get; set; }
        [JsonProperty("recipient")]
        public Recipient recipient { get; set; }
        [JsonProperty("timestamp")]
        public long timestamp;
        [JsonProperty("message")]
        public MessageFB message { get; set; }
    }

    public class Sender
    {
        [JsonProperty("id")]
        public string id { get; set; }
    }

    public class Recipient
    {
        [JsonProperty("id")]
        public string id {get;set;}
    }

    public class MessageFB
    {
        [JsonProperty("mid")]
        public string mid { get; set; }
        [JsonProperty("seq")]
        public int seq { get; set; }
        [JsonProperty("text")]
        public string text { get; set; }
        [JsonProperty("attachments")]
        public List<Attachments> attachments { get; set; }
    }

    public class Attachments
    {
        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("payload")]
        public Payload payload { get; set; }
    }

    public class Payload
    {
        [JsonProperty("url")]
        public string url { get; set; }
    }
}