﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class Messenger
    {
        public string Content { get; set; }
        public int ConversationId { get; set; }
        public string Receiver { get; set; }
        public string Sender { get; set; }
        public string SenderName { get; set; }
        public string Timestamp { get; set; }
        public string Type { get; set; }
        public int CommentId { get; set; }
        public List<string> images { get; set; }
    }
}