﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FacebookUserResponse
    {
        public string name { get; set; }
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string profile_pic { get; set; }
        public FBPicture picture { get; set; }
    }

    public class FBPicture
    {
        public FBData data { get; set; }
    }

    public class FBData
    {
        public int height { get; set; }
        public bool is_silhouette { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }
}