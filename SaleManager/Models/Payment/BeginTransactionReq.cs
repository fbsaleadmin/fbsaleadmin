﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Payment
{
    public class BeginTransactionReq
    {
        public int UserId { get; set; }
        public int BundleId { get; set; }
        public string ReturnUrl { get; set; }
        public string signature { get; set; }
        public string time { get; set; }
    }
}