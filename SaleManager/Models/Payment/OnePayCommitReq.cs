﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Payment
{
    public class OnePayCommitReq
    {
        public string access_key { get; set; }       
        public string command { get; set; }
        public string trans_ref { get; set; }
        public string signature {get;set;}
    }
}