﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Payment
{
    public class OnePayTxRequestResp
    {
        public string pay_url { get; set; }
        public string status { get; set; }
        public string trans_ref { get; set; }
    }
}