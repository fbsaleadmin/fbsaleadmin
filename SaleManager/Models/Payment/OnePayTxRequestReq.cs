﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Payment
{
    public class OnePayTxRequestReq
    {
        public string access_key { get; set; }
        public int amount { get; set; }
        public string command { get; set; }
        public int order_id { get; set; }
        public string order_info { get; set; }
        public string return_url { get; set; }
        public string signature {get;set;}
    }
}