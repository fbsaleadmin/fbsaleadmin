﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Payment
{
    public class OnePayProcessReq
    {
        public string access_key { get; set; }
        public int amount { get; set; }
        public string card_name { get; set; }
        public string card_type { get; set; }
        public string order_id { get; set; }
        public string order_info { get; set; }
        public string order_type { get; set; }
        public DateTime request_time { get; set; }
        public string response_code { get; set; }
        public string response_message { get; set; }
        public DateTime response_time { get; set; }
        public string trans_ref { get; set; }
        public string trans_status { get; set; }
        public string signature { get; set; }
    }
}