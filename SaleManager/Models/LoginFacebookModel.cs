﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class LoginFacebookModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string token { get; set; }
    }
}