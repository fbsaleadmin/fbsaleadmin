﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class SendActionReq
    {       
        public int conversation_id { get; set; }
        public int employee_id { get; set; }
        public string message { get; set; }
        public int action_type { get; set; }
    }
}