﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class BlockUserReq
    {
        public int shop_id { get; set; }     
        public string facebook_user_id { get; set; }
        public string facebook_page_id { get; set; }
    }
}