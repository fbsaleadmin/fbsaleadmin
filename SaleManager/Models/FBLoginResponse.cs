﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FBLoginResponse
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}