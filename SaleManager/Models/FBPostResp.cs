﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FBPostResp
    {
        public List<FBPostItem> data { get; set; }
    }
    public class FBPostItem
    {
        public string id { get; set; }
        public string full_picture { get; set; }
        public string message { get; set; }
        public string story { get; set; }
        public string caption { get; set; }
        public FBPostComment comments { get; set; }
        public DateTime updated_time { get; set; }
    }

    public class FBPostComment
    {
        public List<FBCommentItem> data { get; set; }
    }

    public class FBCommentItem
    {
        public string message { get; set; }
        public string id { get; set; }
        public FBCommentFrom from { get; set; }
        public bool is_hidden { get; set; }
        public FBCommentAttachmentItem attachment { get; set; }
        public CommentReply comments { get; set; }
        public DateTime created_time { get; set; }
    }

    public class CommentReply
    {
        public List<FBReplyItem> data { get; set; }
    }

    public class FBReplyItem
    {
        public string message { get; set; }
        public string id { get; set; }
        public FBCommentFrom from { get; set; }
        public bool is_hidden { get; set; }
        public FBCommentAttachmentItem attachment { get; set; }
        public DateTime created_time { get; set; }
    }

    public class FBCommentFrom
    {
        public string name { get; set; }
        public string id { get; set; }       
    }

    public class FBCommentAttachmentItem
    {
        public string type {get;set;}
        public string url {get;set;}
        public FBCommentAttachmentMedia media { get; set; }
    }

    public class FBCommentAttachmentMedia
    {
        public FBCommentAttachmentMediaImage image { get; set; }
    }

    public class FBCommentAttachmentMediaImage
    {
        public int width { get; set; }
        public int height { get; set; }
        public string src { get; set; }
    }
}