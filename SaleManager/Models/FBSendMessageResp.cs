﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FBSendMessageResp
    {
        public string recipient_id {get;set;}
        public string message_id { get; set; }
        public string attachment_id { get; set; }
    }
}