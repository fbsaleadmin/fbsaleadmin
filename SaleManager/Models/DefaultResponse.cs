﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class DefaultResponse
    {
        public Meta meta { get; set; }
        public object data { get; set; }

        public object metadata { get; set; }
        public object metadatainfo { get; set; }
    }

    public class Meta
    {
        public int error_code { get; set; }
        public string error_message { get; set; }

        public Meta()
        { }

        public Meta(int errorCode, string errorMessage)
        {
            this.error_code = errorCode;
            this.error_message = errorMessage;
        }
    }

    public class Metadata
    {
        public int item_count { get; set; }
        public Metadata()
        { }

        public Metadata(int item_count)
        {
            this.item_count = item_count;
        }
    }

    public class MetadataTotal
    {
        public int Count { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> TotalDiscountPrice { get; set; }
        public Nullable<decimal> TotalDeliveryPrice { get; set; }
        public Nullable<decimal> TotalPaidPrice { get; set; }
        public Nullable<int> TotalOrderImport { get; set; }
        public Nullable<decimal> TotalOrderImportPrice { get; set; }
        public Nullable<decimal> TotalDebtPrice { get; set; }
        public Nullable<int> TotalStock { get; set; }
        public Nullable<decimal> TotalStockPrice { get; set; }
        public Nullable<int> TotalDeliveryCompleted { get; set; }
        public MetadataTotal()
        { }

        public MetadataTotal(int Count, decimal TotalPrice)
        {
            this.Count = Count;
            this.TotalPrice = TotalPrice;
        }

        public MetadataTotal(int Count, decimal TotalPrice, decimal TotalDiscountPrice, decimal TotalDeliveryPrice, decimal TotalPaidPrice, int TotalDeliveryCompleted)
        {
            this.Count = Count;
            this.TotalPrice = TotalPrice;
            this.TotalDiscountPrice = TotalDiscountPrice;
            this.TotalDeliveryPrice = TotalDeliveryPrice;
            this.TotalPaidPrice = TotalPaidPrice;
            this.TotalDeliveryCompleted = TotalDeliveryCompleted;
        }

        public MetadataTotal(int Count, int TotalOrderImport, decimal TotalOrderImportPrice, decimal TotalDebtPrice)
        {
            this.Count = Count;
            this.TotalOrderImport = TotalOrderImport;
            this.TotalOrderImportPrice = TotalOrderImportPrice;
            this.TotalDebtPrice = TotalDebtPrice;
        }

        public MetadataTotal(int Count, int TotalStock, decimal TotalStockPrice)
        {
            this.Count = Count;
            this.TotalStock = TotalStock;
            this.TotalStockPrice = TotalStockPrice;
        }
    }

    public class MetadataCash
    {
        public int item_count { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal Total { get; set; }
        public decimal TotalCachesIn { get; set; }
        public decimal TotalCachesOut { get; set; }
    }

    public class MetadataInfo
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string branchName { get; set; }
        public MetadataInfo()
        { }

        public MetadataInfo(string branchName)
        {
            this.branchName = branchName;
        }

        public MetadataInfo(DateTime startDate, DateTime endDate)
        {
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public MetadataInfo(DateTime startDate, DateTime endDate, string branchName)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.branchName = branchName;
        }
    }

    public class MetadataDateSale
    {
        public int Count { get; set; }
        public int TotalProduct { get; set; }
        public decimal TotalOrderPrice { get; set; }
        public decimal TotalDeliveryPrice { get; set; }
        public decimal TotalDiscountPrice { get; set; }


        public MetadataDateSale()
        { }

        public MetadataDateSale(int Count, int TotalProduct, decimal TotalOrderPrice, decimal TotalDeliveryPrice, decimal TotalDiscountPrice)
        {
            this.Count = Count;
            this.TotalProduct = TotalProduct;
            this.TotalOrderPrice = TotalOrderPrice;
            this.TotalDeliveryPrice = TotalDeliveryPrice;
            this.TotalDiscountPrice = TotalDiscountPrice;
        }
    }

    public class MetadataDateCashes
    {
        public int Count { get; set; }
        public decimal TotalCashes { get; set; }
        public decimal TotalPrice { get; set; }

        public MetadataDateCashes()
        { }

        public MetadataDateCashes(int Count, decimal TotalCashes, decimal TotalPrice)
        {
            this.Count = Count;
            this.TotalCashes = TotalCashes;
            this.TotalPrice = TotalPrice;
        }
    }

    public class MetadataDateProduct
    {
        public int Count { get; set; }
        public decimal TotalSale { get; set; }
        public decimal TotalPriceSale { get; set; }
        public decimal TotalReturn { get; set; }
        public decimal TotalPriceReturn { get; set; }
        public decimal TotalOriginPrice { get; set; }

        public MetadataDateProduct()
        { }

        public MetadataDateProduct(int Count, decimal TotalSale, decimal TotalPriceSale, decimal TotalReturn, decimal TotalPriceReturn)
        {
            this.Count = Count;
            this.TotalSale = TotalSale;
            this.TotalPriceSale = TotalPriceSale;
            this.TotalReturn = TotalReturn;
            this.TotalPriceReturn = TotalPriceReturn;
        }

        public MetadataDateProduct(int Count, decimal TotalSale, decimal TotalPriceSale, decimal TotalReturn, decimal TotalPriceReturn, decimal TotalOriginPrice)
        {
            this.Count = Count;
            this.TotalSale = TotalSale;
            this.TotalPriceSale = TotalPriceSale;
            this.TotalReturn = TotalReturn;
            this.TotalPriceReturn = TotalPriceReturn;
            this.TotalOriginPrice = TotalOriginPrice;
        }

    }

    public class MetadataExim
    {
        public int Count { get; set; }
        public decimal TotalStart { get; set; }
        public decimal TotalPriceStart { get; set; }
        public decimal TotalInport { get; set; }
        public decimal TotalPriceInport { get; set; }
        public decimal TotalExport { get; set; }
        public decimal TotalPriceExport { get; set; }
        public decimal TotalEnd { get; set; }
        public decimal TotalPriceEnd { get; set; }

        public MetadataExim()
        { }

        public MetadataExim(int Count, decimal TotalStart, decimal TotalPriceStart, decimal TotalInport,
            decimal TotalPriceInport, decimal TotalExport, decimal TotalPriceExport, decimal TotalEnd, decimal TotalPriceEnd)
        {
            this.Count = Count;
            this.TotalStart = TotalStart;
            this.TotalPriceStart = TotalPriceStart;
            this.TotalInport = TotalInport;
            this.TotalPriceInport = TotalPriceInport;
            this.TotalExport = TotalExport;
            this.TotalPriceExport = TotalPriceExport;
            this.TotalEnd = TotalEnd;
            this.TotalPriceEnd = TotalPriceEnd;
        }

    }

    public class MetadataPartner
    {
        public int Count { get; set; }
        //public decimal TotalPartner { get; set; }
        public decimal TotalProduct { get; set; }
        public decimal TotalPrice { get; set; }

        public MetadataPartner()
        { }

        public MetadataPartner(int Count, decimal TotalProduct, decimal TotalPrice)
        {
            this.Count = Count;
            //this.TotalPartner = TotalPartner;
            this.TotalProduct = TotalProduct;
            this.TotalPrice = TotalPrice;
        }
    }

    public class MetadataSale
    {
        public int Count { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalPriceReturn { get; set; }
        public decimal TotalOrderPrice { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalDelivery { get; set; }
        public decimal TotalOriginPrice { get; set; }

        public MetadataSale()
        { }

        public MetadataSale(int Count, decimal TotalPrice, decimal TotalPriceReturn)
        {
            this.Count = Count;
            this.TotalPrice = TotalPrice;
            this.TotalPriceReturn = TotalPriceReturn;
        }

        public MetadataSale(int Count, decimal TotalOrderPrice, decimal TotalDiscount, decimal TotalDelivery, decimal TotalPrice, decimal TotalOriginPrice)
        {
            this.Count = Count;
            this.TotalOrderPrice = TotalPrice;
            this.TotalDiscount = TotalDiscount;
            this.TotalDelivery = TotalDelivery;
            this.TotalPrice = TotalPrice;
            this.TotalOriginPrice = TotalOriginPrice;
        }

        //public MetadataSale(int Count, decimal TotalPrice, decimal TotalDiscount)
        //{
        //    this.Count = Count;
        //    this.TotalPrice = TotalPrice;
        //    this.TotalDiscount = TotalDiscount;
        //}

    }

}