﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class ExchangeTokenResp
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}