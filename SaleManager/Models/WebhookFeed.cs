﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SaleManager.Models
{
    
        public class WebhookFeed
        {
            [JsonProperty("object")]
            public string Object { get; set; }

            [JsonProperty("entry")]
            public List<EntryFeed> entry { get; set; }
        }

        public class EntryFeed
        {
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("time")]
            public long time { get; set; }
            [JsonProperty("changes")]
            public List<Change> changes { get; set; }
        }

        public class Change
        {
            [JsonProperty("field")]
            public string field { get; set; }
            [JsonProperty("value")]
            public Value value { get; set; }
        }

        public class Value
        {
            [JsonProperty("parent_id")]
            public string parent_id { get; set; }
            [JsonProperty("sender_name")]
            public string sender_name { get; set; }
            [JsonProperty("comment_id")]
            public string comment_id { get; set; }
            [JsonProperty("sender_id")]
            public string sender_id { get; set; }
            [JsonProperty("photo")]
            public string photo { get; set; }
            [JsonProperty("item")]
            public string item { get; set; }
            [JsonProperty("verb")]
            public string verb { get; set; }
            [JsonProperty("created_time")]
            public long created_time { get; set; }
            [JsonProperty("post_id")]
            public string post_id { get; set; }
             [JsonProperty("message")]
            public string message { get; set; }
            public string thread_id { get; set; }
        }
    
}