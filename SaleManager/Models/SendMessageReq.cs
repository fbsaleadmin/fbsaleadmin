﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class SendMessageReq
    {
        public int shop_id { get; set; }
        public int page_id { get; set; }
        public string message { get; set; }
        public string image_url { get; set; }        
        public string facebook_user_id { get; set; }
        public string customer_phone_number { get; set; }
        public int employee_id { get; set; }
    }

    public class NewSendMessageReq
    {       
        public int conversation_id { get; set; }
        public int employee_id { get; set; }
        public string message { get; set; }
        public string image_url { get; set; }
    }
}