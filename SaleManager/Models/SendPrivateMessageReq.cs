﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class SendPrivateMessageReq
    {
        public int id { get; set; }
        public string type { get; set; }
        public string message { get; set; }
    }
}