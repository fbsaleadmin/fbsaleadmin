﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class EditCommentReq
    {
        public string facebook_comment_id { get; set; }       
        public string facebook_reply_id { get; set; }
        public int conversation_id { get; set; }
        public string message { get; set; }
        public string image_url { get; set; }
    }
}