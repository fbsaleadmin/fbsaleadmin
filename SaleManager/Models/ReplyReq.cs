﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class ReplyReq
    {
        public int shop_id { get; set; }   
        public string facebook_post_id { get; set; }
        public string facebook_comment_id { get; set; }
        public string message { get; set; }
        public string image_url { get; set; }
        public int employee_id { get; set; }
    }
}