﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SaleManager.Data;

namespace SaleManager.Models
{
    public class ShopStat
    {      
        public Nullable<int> countProduct { get; set; }
        public Nullable<int> countOrder { get; set; }
        public Nullable<int> countMessage { get; set; }
        public List<PageInfo> pages { get; set; }
    }

    public class ShopDetailStat
    {
        public string ShopName { get; set; }
        public string Address { get; set; }    
        public Nullable<int> CountProduct { get; set; }
        public Nullable<int> CountItem { get; set; }
        public Nullable<int> CountExportedItem { get; set; }
        public Nullable<int> CountMessage { get; set; }
        public Nullable<int> TotalOrder { get; set; }
        public Nullable<double> TotalOrderAmount { get; set; }
        public Nullable<int> DebtCount { get; set; }
        public Nullable<double> DebtAmount { get; set; }
        public Nullable<int> TodayOrderCount { get; set; }
        public Nullable<decimal> TodayOrderAmount { get; set; }
        public Nullable<int> TodayReturnCount { get; set; }
        public Nullable<decimal> TodayReturnAmount { get; set; }
        public string Avatar { get; set; }
        public Nullable<int> CustomerCount { get; set; }
        public Nullable<int> VendorCount { get; set; }
    }

    public class PageInfo
    {
        public Nullable<int> Pageid { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string PageFacebookId { get; set; }
        public string PageName { get; set; }
        public bool? isSubscribed { get; set; }
    }

    public class FacebookPostResponse
    {
        public List<FacebookPost> data { get; set; }
        public Paging paging { get; set; }
    }

    public class Paging
    {
        public string previous { get; set; }
        public string next { get; set; }
    }

    public class FacebookPost
    {
        public string full_picture { get; set; }
        public string message { get; set; }
        public string id { get; set; }
        public string story { get; set; }
        public DateTime updated_time { get; set; }
    }

    public class ShopConversation
    {
        public Nullable<int> ConversationId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> PageId { get; set; }
        public string FacebookUserId { get; set; }
        public string Name { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Tag { get; set; }
        public string Note { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public string LastMessage { get; set; }
        public string FacebookPostId { get; set; }
        public Nullable<int> Type { get; set; }
        public string FBUserId { get; set; }
        public string UserAvatar { get; set; }
        public Nullable<System.DateTime> LastMessageTime { get; set; }
        public List<ShopLabel> lables { get; set; }
        public ShopPage page { get; set; }
        public string SearchQuery { get; set; }
        public string FacebookCommentId { get; set; }
        public string Phone { get; set; }
        public int CommentId { get; set; }
        public Nullable<bool> IsPMSent { get; set; }
        public long LastUnixTimestamp { get; set; }
        public bool IsUserBlocked { get; set; }
        public int TotalConversation { get; set; }
    }

    public class ShopPage
    {
        public Nullable<int> PageId {get;set;}
        public string PageFacebookId {get;set;}
        public string PageName {get;set;}
    }

    public class ShopLabel
    {
        public Nullable<int> LabelId { get; set; }
        public Nullable<int> LabelConversationId { get; set; }
        public string LabelName { get; set; }
        public string LabelColor { get; set; }
    }
    

    public class ShopComment
    {
        public Nullable<int> CommentId { get; set; }
        public Nullable<int> PostId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string FacebookCommentId { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string Photo { get; set; }
        public string Content { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> FacebookStatus { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> ConversationId { get; set; }

        public List<ShopReply> replies { get; set; }
    }

    public class ShopReply
    {
        public Nullable<int> ReplyId { get; set; }
        public Nullable<int> CommentId { get; set; }
        public string FacebookCommentId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string Photo { get; set; }
        public string Content { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> FacebookStatus { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> EmployeeId { get; set; }
       
    }

    public class ShopMessage
    {
        public Nullable<int> MessageId { get; set; }
        public Nullable<int> ConversationId { get; set; }
        public string FacebookMessageId { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string RecipientId { get; set; }
        public string RecipientName { get; set; }
        public Nullable<int> Sequence { get; set; }
        public Nullable<int> FacebookStatus { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<bool> IsBotMessage { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<bool> HasAttachment { get; set; }
        public List<ShopAttachment> attachments { get; set; }
        public long UnixTimestamp { get; set; }
    }

    public class ShopAttachment
    {
        public Nullable<int> AttachmentId { get; set; }
        public Nullable<int> MessageId { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string OwnUrl { get; set; }
    }

    public class ShopProduct
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Category category { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public string SubName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public string Tag { get; set; }
        public Nullable<bool> AllowSell { get; set; }
        public Nullable<bool> AllowOrder { get; set; }
        public Nullable<bool> AllowDelivery { get; set; }
        public Nullable<int> PStatus { get; set; }
        public string Code { get; set; }
        public Nullable<double> Discount { get; set; }
        public Nullable<double> OtherPrice { get; set; }
        public Nullable<double> NetWeight { get; set; }
        public Nullable<int> PAStatus { get; set; }
        public Nullable<int> MinStock { get; set; }
        public Nullable<int> StockLimit { get; set; }
        public Nullable<int> ManufactureId { get; set; }
        public List<PI> images { get; set; }
        public List<ShopProductPrice> prices { get; set; }
        public List<ShopProductStock> stock { get; set; }
        public Nullable<int> SumStock { get; set; }
        public Nullable<decimal> StockPrice { get; set; }
        public string SearchQuery { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
    }

    public class ShopProductPrice
    {
        public Nullable<int> ProductPriceId { get; set; }
        public string Unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<decimal> AverageImportPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
    }

    public class ShopProductStock
    {
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> Stock { get; set; }
        public string BranchName { get; set; }
    }

    public class Category
    {
        public Nullable<int> CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> CategoryParentId { get; set; }
    }

    public class ShopCategory
    {
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> CategoryParentId { get; set; }
        public Nullable<int> ProductCount { get; set; }
        public Nullable<int> StockCount { get; set; }
        public List<ShopChildCategory> children { get; set; }
    }

    public class ShopChildCategory
    {
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> CategoryParentId { get; set; }
        public Nullable<int> ProductCount { get; set; }
        public Nullable<int> StockCount { get; set; }
    }

    public class ShopPost
    {
        public Nullable<int> PostId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> PageId { get; set; }
        public string FacebookPostId { get; set; }
        public string FacebookPostUrl { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string Image { get; set; }
        public Nullable<int> ShowComment { get; set; }
    }

    public class ShopCustomer
    {
        public Nullable<int> CustomerId { get; set; }
        public string FacebookUserId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public ShopDistrict District { get; set; }
        public ShopProvince Province { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<DateTime> DOB { get; set; }
        public Nullable<bool> Gender { get; set; }
        public Nullable<int> Level { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public string Tag { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string ID { get; set; }
        public Nullable<int> Type { get; set; }
        public string TaxID { get; set; }
        public string CustomerCode { get; set; }
        public string FBUserAvatar { get; set; }
        public ShopCustomerGroup customerGroup { get; set; }
        public Nullable<int> TotalOrder { get; set; }
        public Nullable<decimal> TotalOrderPrice { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> TotalDebt { get; set; }
        public string SearchQuery { get; set; }
    }

    public class ShopProvince
    {
        public Nullable<int> ProvinceId { get; set; }
        public string Name { get; set; }
    }

    public class ShopDistrict
    {
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public string Name { get; set; }
    }

    public class ShopCustomerGroup
    {
        public Nullable<int> CustomerGroupId { get; set; }
        public string Name { get; set; }
    }

    public class ShopProductPrices
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Category category { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public string SubName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SearchQuery { get; set; }
        public List<ShopProductStock> stock { get; set; }
        public List<Price> prices { get; set; }
        //public Nullable<double> AverageImportPrice { get; set; }
        public Nullable<int> PAStatus { get; set; }
    }

    public class Price
    {
        public Nullable<int> ProductPriceId { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<decimal> AverageImportPrice { get; set; }        
        public ProductUnit unit { get; set; }
    }

    public class ProductUnit
    {
        public int ProductUnitId { get; set; }
        public string Unit { get; set; }
        public Nullable<int> Quantity { get; set; }
    }

    public class ShopVendor
    {
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string Address { get; set; }
        public string Company { get; set; }
        public ShopDistrict District { get; set; }
        public ShopProvince Province { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string Phone { get; set; }
        public Nullable<int> TotalOrder { get; set; }
        public Nullable<decimal> TotalOrderPrice { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> TotalDebt { get; set; }
        public string SearchQuery { get; set; }
    }

    public class ShopDeliveryVendor
    {
        public Nullable<int> DeliveryVendorId { get; set; }
        public string VendorName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public ShopDistrict District { get; set; }
        public ShopProvince Province { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string VendorCode { get; set; }
        public Nullable<double> TotalDebt { get; set; }
        public Nullable<double> TotalPaid { get; set; }
        public string SearchQuery { get; set; }
        public string Note { get; set; }
    }

    public class ShopAttribute
    {
        public Nullable<int> AttributeId { get; set; }
        public string AttributeName { get; set; }
    }

}