﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class DeleteCommentReq
    {
        public string facebook_comment_id { get; set; }       
        public string facebook_reply_id { get; set; }
        public int conversation_id { get; set; }      
    }
}