﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class DeleteRequest
    {
        public int ShopId { get; set; }
        public int EmployeeId { get; set; }        
    }
}