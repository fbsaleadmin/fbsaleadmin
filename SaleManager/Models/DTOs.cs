﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class DTOs
    {

    }

    public partial class ActionDTO
    {
        public Nullable<int> ActionId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string ActionName { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string ActionType { get; set; }
        public Nullable<int> Result { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public string TargetType { get; set; }
    }

    public partial class AttributeDTO
    {

        public Nullable<int> AttributeId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string AttributeName { get; set; }
        public Nullable<bool> IsCustom { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }

    }

    public partial class AttributeMapDTO
    {
        public Nullable<int> AttributeMapId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> AttributeId { get; set; }
        public string Value { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }

    }

    public partial class ConfigDTO
    {
        public Nullable<int> ConfigId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<bool> NewMessageNotification { get; set; }
        public Nullable<bool> NotificationSound { get; set; }
        public Nullable<bool> NewMessagePriority { get; set; }
        public Nullable<bool> NewCommentLike { get; set; }
        public Nullable<bool> AutoCreateOrder { get; set; }
        public Nullable<bool> AutoHideComment { get; set; }
        public Nullable<bool> AllowBotMessage { get; set; }
        public Nullable<bool> AutoHidePhoneComment { get; set; }
    }

    public partial class ProvinceDTO
    {
        public Nullable<int> ProvinceId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Nullable<int> Priority { get; set; }
    }

    public partial class DistrictDTO
    {
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Nullable<int> Priority { get; set; }  
    }

    public partial class BlockListDTO
    {
        public Nullable<int> BlockListId { get; set; }
        public string FacebookUserId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> ShopId { get; set; }
    }

    public partial class BranchDTO
    {
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Phone { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<bool> IsMainBranch { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
    }

    public partial class BranchStockDTO
    {
        public Nullable<int> BranchStockId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> Stock { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
    }

    public partial class CashDTO
    {
        public Nullable<int> CashId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string CashCode { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetCode { get; set; }
        public string TargetType { get; set; }
        public Nullable<int> CashTypeId { get; set; }      
        public string Note { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }        
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Title { get; set; }
        public Nullable<int> PaymentMethod { get; set; }
        public Nullable<int> PaidTargetId { get; set; }
        public string PaidTargetType { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<decimal> Remain { get; set; }
        public Nullable<int> Type { get; set; }
    }

    public partial class CashTypeDTO
    {
        public Nullable<int> CashTypeId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string Name { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<decimal> DefaultValue { get; set; }
        public Nullable<bool> IsCustom { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string CashTypeCode { get; set; }
    }

    public partial class CategoryDTO
    {
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> CategoryParentId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDesc { get; set; }
        public Nullable<int> Status { get; set; }
        public string AvailableBranchIds { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
    }

    public partial class CommentDTO
    {       
        public Nullable<int> CommentId { get; set; }
        public Nullable<int> PostId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string FacebookCommentId { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string Photo { get; set; }
        public string Content { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> FacebookStatus { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> ConversationId { get; set; }
    }

    public partial class ConversationDTO
    {
        public Nullable<int> ConversationId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> PageId { get; set; }
        public string FacebookUserId { get; set; }
        public string Name { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Tag { get; set; }
        public string Note { get; set; }
        public Nullable<int> Status { get; set; }        
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string LastMessage { get; set; }
        public string FacebookPostId { get; set; }
        public Nullable<int> Type { get; set; }
        public string FBUserId { get; set; }
        public string UserAvatar { get; set; }
        public Nullable<DateTime> LastMessageTime { get; set; }
        public string SearchQuery { get; set; }
        public string FacebookCommentId { get; set; }
    }

    public partial class CustomerDTO
    {
        public Nullable<int> CustomerId { get; set; }
        public string FacebookUserId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<DateTime> DOB { get; set; }
        public Nullable<bool> Gender { get; set; }
        public Nullable<int> Level { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public string Tag { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string ID { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> CustomerGroupId { get; set; }
        public string TaxID { get; set; }
        public string CustomerCode { get; set; }
        public string FBUserAvatar { get; set; }
        public string SearchQuery { get; set; }
        public string FacebookMessengerId { get; set; }
        public int TotalConversation { get; set; }
    }

    public partial class CustomerGroupDTO
    {     
        public Nullable<int> CustomerGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreateAt { get; set; }
        public Nullable<int> ShopId { get; set; }
    }

    public partial class DebtDTO
    {
        public Nullable<int> DebtId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<int> DebtType { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<decimal> Remain { get; set; }
    }

    public partial class DeliveryVendorDTO
    {
        public Nullable<int> DeliveryVendorId { get; set; }
        public string VendorName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string VendorCode { get; set; }
        public string Note { get; set; }
        public string SearchQuery { get; set; }
    }

    public partial class EmployeeDTO
    {
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string EmployeeName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> GroupId { get; set; }
        public string Group { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }       
        public Nullable<DateTime> TokenSince { get; set; }
        public string FacebookUserId { get; set; }
    }

    public partial class EmployeeuserDTO
    {
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> BundleId { get; set; }
        public string EmployeeName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ShopName { get; set; }
        public string BundleName { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> GroupId { get; set; }
        public string Group { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> StartAt { get; set; }
        public Nullable<DateTime> EndAt { get; set; }
        public Nullable<DateTime> TokenSince { get; set; }
        public string FacebookUserId { get; set; }
    }

    public partial class EximDTO
    {
        public Nullable<int> EximId { get; set; }
        public string Code { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<decimal> ImportPrice { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<DateTime> CreatetAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string Title { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<DateTime> CustomDate { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
    }

    public partial class EximProductDTO
    {
        public Nullable<int> EximProductId { get; set; }
        public Nullable<int> EximId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> LastStock { get; set; }
        public Nullable<int> PreviousStock { get; set; }
        public Nullable<decimal> DiscountPerItem { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
    }

    public partial class LabelDTO
    {
        public Nullable<int> LabelId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string LabelName { get; set; }
        public string LabelColor { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> Status { get; set; }
    }

    public partial class LabelConversationDTO
    {
        public Nullable<int> LabelConversationId { get; set; }
        public Nullable<int> LabelId { get; set; }
        public Nullable<int> ConversationId { get; set; }
    }

    public partial class ManufactureDTO
    {
        public Nullable<int> ManufactureId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string Name { get; set; }
        public string Origin { get; set; }
    }

    public partial class MessageDTO
    {
        public Nullable<int> MessageId { get; set; }
        public Nullable<int> ConversationId { get; set; }
        public string FacebookMessageId { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string RecipientId { get; set; }
        public string RecipientName { get; set; }
        public Nullable<int> Sequence { get; set; }
        public Nullable<int> FacebookStatus { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<bool> IsBotMessage { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<bool> HasAttachment { get; set; }
    }

    public partial class NotificationDTO
    {
        public Nullable<int> NotificationId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public string Message { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> SenderId { get; set; }
        public string SenderName { get; set; }
    }

    public partial class NoteDTO
    {
        public Nullable<int> NoteId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public string Content { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> Status { get; set; }
    }

    public partial class AttachmentDTO
    {
        public Nullable<int> AttachmentId { get; set; }
        public Nullable<int> MessageId { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
    }

    public partial class OrderDTO
    {     

        public Nullable<int> OrderId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string OrderCode { get; set; }
        public Nullable<decimal> OrderPrice { get; set; }
        public Nullable<decimal> DiscountPrice { get; set; }
        public Nullable<decimal> DeliveryPrice { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> OrderStatus { get; set; }
        public string Note { get; set; }
        public string OrderAddress { get; set; }
        public string OrderEmail { get; set; }
        public string OrderPhone { get; set; }
        public string Channel { get; set; }
        public string PaymentChannel { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<decimal> CustomerPaid { get; set; }
        public Nullable<int> DeliveryVendorId { get; set; }
        public Nullable<decimal> MoneyCollect { get; set; }
        public Nullable<int> SaleEmployeeId { get; set; }
        public Nullable<decimal> TotalOriginPrice { get; set; }
        public Nullable<decimal> DeliveryPaid { get; set; }
    }

    public partial class OrderProductDTO
    {
        public Nullable<int> OrderProductId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
    }


    public partial class PageDTO
    {
        public Nullable<int> PageId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string PageFacebookId { get; set; }
        public string PageFacebookToken { get; set; }
        public string PageName { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> IsSubscribed { get; set; }
    }

    public partial class PostDTO
    {
        public Nullable<int> PostId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> PageId { get; set; }
        public string FacebookPostId { get; set; }
        public string FacebookPostUrl { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string Image { get; set; }
        public Nullable<int> ShowComment { get; set; }
    }

    public partial class ProductDTO
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public Nullable<int> ManufactureId { get; set; }
        public string Tag { get; set; }
        public Nullable<bool> AllowSell { get; set; }
        public Nullable<bool> AllowOrder { get; set; }
        public Nullable<bool> AllowDelivery { get; set; }
        public string AvailableBranchIds { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }      
        public Nullable<int> Status { get; set; }
        public Nullable<int> MinStock { get; set; }
        public Nullable<int> StockLimit { get; set; }
    }

    public partial class ProductAttributeDTO
    {

        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string ProductCode { get; set; }
        public string Code { get; set; }      
        public Nullable<bool> Discount { get; set; }               
        public Nullable<bool> NetWeight { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string SubName { get; set; }
        public Nullable<int> Status { get; set; }
        public string SearchQuery { get; set; }
   
    }

    public partial class ProductImageDTO
    {
        public Nullable<int> ProductImageId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string Path { get; set; }
        public string RelativePath { get; set; }
        public string Alt { get; set; }
        public string FileName { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
    }

    public partial class ProductPriceDTO
    {
        public Nullable<int> ProductPriceId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<DateTime> UtilDate { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
    }

    public partial class ProductUnitDTO
    {
        public Nullable<int> ProductUnitId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string Unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> Status { get; set; }
    }

    public partial class ReplyDTO
    {
        public Nullable<int> ReplyId { get; set; }
        public Nullable<int> CommentId { get; set; }
        public string FacebookCommentId { get; set; }
        public Nullable<int> CustomerId { get; set; }       
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string Photo { get; set; }
        public string Content { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> FacebookStatus { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> EmployeeId { get; set; }
    }

    public partial class ReturnDTO
    {

        public int ReturnId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public string Code { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> BranchId { get; set; }
    }

    public partial class ReturnProductDTO
    {
        public int ReturnProductId { get; set; }
        public Nullable<int> ReturnId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
    }

    public partial class ShopDTO
    {
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> UserId { get; set; }
        public string ShopName { get; set; }
        public Nullable<int> ShopCategoryId { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string WorkTimeStart { get; set; }
        public string WorkTimeEnd { get; set; }
        public string ShopProductPrefix { get; set; }
        public string ShopOrderPrefix { get; set; }
        public string ShopUniqueName { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string ShopCustomerPrefix { get; set; }
        public string ShopVendorPrefix { get; set; }
        public string ShopDVendorPrefix { get; set; }
        public string ShopImportPrefix { get; set; }
        public string ShopExportPrefix { get; set; }
        public string ShopCashInPrefix { get; set; }
        public string ShopCashOutPrefix { get; set; }
    }

    public partial class ShopCategoryDTO
    {
        public Nullable<int> ShopCategoryId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Priority { get; set; }
    }

    public partial class TemplateDTO
    {
        public Nullable<int> TemplateId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> PageId { get; set; }
        public string Name { get; set; }
        public string Mark { get; set; }
        public Nullable<bool> IsAutoMessage { get; set; }
        public string Content { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
    }

    public partial class TransferDTO
    {
        public int TransferId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> FromBranchId { get; set; }
        public Nullable<int> ToBranchId { get; set; }
        public string Code { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<int> EmployeeId { get; set; }
    }

    public partial class TransferProductDTO
    {
        public int TransferProductId { get; set; }
        public Nullable<int> TransferId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
    }

    public partial class UserDTO
    {
        public Nullable<int> UserId { get; set; }
        public string UserFacebookId { get; set; }
        public string UserFacebookToken { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<bool> IsThirdPartyLogin { get; set; }
    }

    public partial class VendorDTO
    {
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string Address { get; set; }
        public string Company { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public string Phone { get; set; }
        public Nullable<int> Status { get; set; }
        public string SearchQuery { get; set; }
    }
}