﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class SenderConversation
    {
        public string name { get; set; }
        public string email { get; set; }
        public string id { get; set; }
    }

    public class Senders
    {
        public List<SenderConversation> data { get; set; }
    }

    public class SenderObj
    {
        public Senders senders { get; set; }
        public FBConversationMessages messages { get; set; }
        public string id { get; set; }
    }

    public class FBConversationResponse
    {
        public List<SenderObj> data { get; set; }
    }

    public class FBConversationMessage
    {
        public string id { get; set; }
        public string created_time { get; set; }
    }

    public class FBConversationMessages
    {
        public List<FBConversationMessage> data { get; set; }
    }
}