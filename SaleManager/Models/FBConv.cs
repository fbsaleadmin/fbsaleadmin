﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FBConv
    {
        public List<FBConvItem> data { get; set; }
        public FBConvPaging paging { get; set; }
    }
    public class FBConvItem
    {
        public string id { get; set; }
        public FBConvSender senders { get; set; }
        public FBConvMessage messages { get; set; }
        public DateTime updated_time { get; set; }
    }

    public class FBConvSender
    {
        public List<FBConvSenderItem> data { get; set; }
    }

    public class FBConvSenderItem
    {
        public string name { get; set; }
        public string email { get; set; }
        public string id { get; set; }
    }

    public class FBConvMessage
    {
        public List<FBConvMessageItem> data { get; set; }
    }

    public class FBConvMessageItem
    {
        public DateTime created_time { get; set; }
        public FBConvAttachment attachments { get; set; }
        public string id { get; set; }
        public FBConvSenderItem from { get; set; }
        public FBConvMessageTo to { get; set; }
        public string message { get; set; }
    }

    public class FBConvMessageTo
    {
        public List<FBConvSenderItem> data { get; set; }
    }

    public class FBConvAttachment
    {
        public List<FBConvAttachmentItem> data { get; set; }
    }

    public class FBConvAttachmentItem
    {
        public FBConvAttDataImageData image_data { get; set; }
    }

    public class FBConvAttDataImageData
    {
        public int width { get; set; }
        public int height { get; set; }
        public int max_width { get; set; }
        public int max_height { get; set; }
        public string url { get; set; }
        public string preview_url { get; set; }
        public int image_type { get; set; }
        public bool render_as_sticker { get; set; }
    }

    public class FBConvPaging
    {
        public string next { get; set; }
        public FBConvCursor cursors { get; set; }
    }

    public class FBConvCursor
    {
        public string before { get; set; }
        public string next { get; set; }
    }
}