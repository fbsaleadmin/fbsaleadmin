﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class DateTimeReport
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
