﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class TokenResp
    {
        public string access_token { get; set; }
        public string id { get; set; }
    }
}