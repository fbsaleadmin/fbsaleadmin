﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FacebookPostResp
    {
        public string created_time { get; set; }
        public string message { get; set; }
        public string story { get; set; }
        public string id { get; set; }

        public string full_picture { get; set; }
    }
}