﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Data
{
    public class Shop
    {
        public int ShopId { get; set; }
        public string ShopName { get; set; }
    }
    public class CreateOrder
    {
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> OrderPrice { get; set; }
        public Nullable<decimal> DiscountPrice { get; set; }
        public Nullable<decimal> DeliveryPrice { get; set; }
        public Nullable<decimal> TotalOriginPrice { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public string Note { get; set; }
        public string OrderAddress { get; set; }
        public string OrderEmail { get; set; }
        public string OrderPhone { get; set; }
        public string Channel { get; set; }
        public string PaymentChannel { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<decimal> CustomerPaid { get; set; }
        public Nullable<int> DeliveryVendorId { get; set; }
        public Nullable<decimal> MoneyCollect { get; set; }
        public Nullable<int> SaleEmployeeId { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public List<OrderProduct> products { get; set; }     
    }

    public class OrderProduct
    {
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> Price { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
    }

    public class CreateProduct
    {
        //product info
        public Nullable<int> CategoryId { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public Nullable<int> ManufactureId { get; set; }
        public string Tag { get; set; }
        public Nullable<bool> AllowSell { get; set; }
        public Nullable<bool> AllowOrder { get; set; }
        public Nullable<bool> AllowDelivery { get; set; }
        public Nullable<int> MinStock { get; set; }
        public Nullable<int> StockLimit { get; set; }
        public Nullable<int> CurrentStock { get; set; }
        public string BaseUnit { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        //product attribute
        public string ProductCode { get; set; }
        public string Code { get; set; }     
        public Nullable<double> NetWeight { get; set; }
        //attribute set
        public List<List<Attribute>> attributeSets { get; set; }
        public List<ProductUnit> units { get; set; }     
        //Emp
        public Nullable<int> EmployeeId { get; set; }
    }

    public class Attribute
    {
        public string AttributeId { get; set; }
        public string Value { get; set; }
    }

    public class ProductUnit
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Price { get; set; }
        public string Status { get; set; } 
    }

    public class ProductAttributeImport
    {
        public Nullable<int> ProductAttributeId { get; set; }
        public string ImportCode { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> ImportPrice { get; set; }
        public Vendor vendor { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> LastStock { get; set; }
        public Nullable<int> PreviousStock { get; set; }
    }

    public class ProductAttributeExims
    {
        public Nullable<int> ProductAttributeId { get; set; }
        public string EximsCode { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<decimal> EximsPrice { get; set; }
        public Target target { get; set; }
        public Branch branch { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> LastStock { get; set; }
        public Nullable<int> PreviousStock { get; set; }
    }

    public class Vendor
    {
        public Nullable<int> VendorId {get;set;}
        public string VendorName {get;set;}
        public string VendorCode { get; set; }
    }

    public class DeliveryVendor
    {
        public Nullable<int> DeliveryVendorId { get; set; }
        public string VendorName { get; set; }
        public string VendorCode { get; set; }
    }

    public class ProductAttributeOrder
    {
        public Nullable<int> ProductAttributeId { get; set; }
        public string OrderCode { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }      
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Customer customer { get; set; }
        public Branch branch { get; set; }
        public Unit unit { get; set; }
    }

    public class Customer
    {
        public Nullable<int> CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string FacebookUserId { get; set; }
    }

    public class Branch
    {
        public Nullable<int> BranchId { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }

    public class Unit
    {
        public Nullable<int> ProductUnitId { get; set; }
        public string Name { get; set; }
    }

    public class UpdateProductPrice
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductPriceId { get; set; }
        public Nullable<decimal> NewPrice { get; set; }
        public Nullable<int> EmployeeId { get; set; }
    }

    public class CustomerOrder
    {
        public Nullable<int> OrderId { get; set; }
        public string OrderCode { get; set; }       
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }        
        public Branch branch { get; set; }
        public Nullable<decimal> OrderPrice { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Employee employee { get; set; }
        public Employee saleEmployee { get; set; }
        public Nullable<int> OrderStatus { get; set; }
        public Nullable<int> Type { get; set; }
    }

    public class CashFlow
    {
        public Target target { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> Debt { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
    }

    public class CustomerNote
    {        
        public Nullable<int> NoteId { get; set; }
        public Nullable<int> CustomerId { get; set; }        
        public string Content { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
    }

    public class OrderNote
    {
        public Nullable<int> NoteId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string Content { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
    }

    public class CreateImport
    {
        public Nullable<int> EmployeeId {get;set;}
        public string Note {get;set;}
        public Nullable<int> PaymentType {get;set;}        
        public Nullable<decimal> discount {get;set;}
        public Nullable<decimal> paid {get;set;}
        public Nullable<int> selectedVendorId {get;set;}
        public string VendorName {get;set;}
        public List<CreateImportProduct> selectedProducts {get;set;}
        public Nullable<int> BranchId { get; set; }
        public bool isTemp { get; set; }
    }

    public class CreateImportProduct
    {
        public decimal Discount {get;set;}
        public decimal OriginPrice {get;set;}
        public int ProductAttributeId {get;set;}
        public int Quantity {get;set;}
    }

    public class CancelImport
    {
        public Nullable<int> EmployeeId {get;set;}
        public Nullable<int> EximId {get;set;}      
    }

    public class CompleteImport
    {
        public Nullable<int> EmployeeId {get;set;}
        public Nullable<int> EximId {get;set;}  
    }

    public class Cash
    {
        public Nullable<int> CashId { get; set; }
        public string CashCode { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Note { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> PaymentMethod { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<int> PaidTargetId { get; set; }
        public string PaidTargetType { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<bool> IsManual { get; set; }
        public Target target { get; set; }
        public PaidTarget paidTarget { get; set; }
        public CashType cashType { get; set; }
        public Employee employee { get; set; }
        public Nullable<int> CashTypeId { get; set; }
    }

    public class Target
    {
        public Nullable<int> TargetId { get; set; }
        public string TargetName { get; set; }
        public string TargetCode { get; set; }
        public string SearchQuery { get; set; }
        public Nullable<int> Type { get; set; }
    }

    public class PaidTarget
    {
        public Nullable<int> PaidTargetId { get; set; }
        public string TargetName { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public string TargetCode { get; set; }
    }

    public class CashType
    {
        public Nullable<int> CashTypeId { get; set; }
        public string Name { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<decimal> DefaultValue { get; set; }
    }

    public class Employee
    {
        public Nullable<int> EmployeeId { get; set; }
        public string EmployeeName { get; set; }
    }

    public class PAAtrribute
    {
        public int AttributeMapId { get; set; }
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
    }

    public class UpdateProduct
    {
        //product info
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public Nullable<int> ManufactureId { get; set; }
        public string Tag { get; set; }
        public Nullable<bool> AllowSell { get; set; }
        public Nullable<bool> AllowOrder { get; set; }
        public Nullable<bool> AllowDelivery { get; set; }
        public Nullable<int> MinStock { get; set; }
        public Nullable<int> StockLimit { get; set; }
        public Nullable<int> CurrentStock { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }       
        //attribute set
        public List<UpdateProductAttribute> attrs { get; set; }
        public List<UpdateProductPrices> prices { get; set; }
        //Emp
        public Nullable<int> EmployeeId { get; set; }
        public List<UpdateProductDeletedImage> deletedImages { get; set; }
    }

    public class UpdateProductPrices
    {
        public Nullable<int> ProductPriceId { get; set; }
        public string Unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public string Status { get; set; }
    }

    public class UpdateProductAttribute
    {
        public Nullable<int> AttributeId { get; set; }
        public Nullable<int> AttributeMapId { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
        public string Status { get; set; }
    }

    public class UpdateProductDeletedImage
    {
        public string Alt { get; set; }
        public string Path { get; set; }
        public string RelativePath { get; set; }
        public int ProductImageId { get; set; }
    }

    public class Exim
    {
        public Nullable<int> EximId { get; set; }
        public string Code { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> CustomerPaid { get; set; }
        public Nullable<decimal> ReturnPaid { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public string Title { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Employee employee { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<DateTime> CreatetAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<int> Status { get; set; }
        public Target target { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Branch branch { get; set; }
        public Nullable<int> TotalReturnProduct { get; set; }
    }
   
    public class EximProduct
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> EximProductId { get; set; }
        public Nullable<int> EximId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SubName { get; set; }
        public Unit unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> LastStock { get; set; }
        public Nullable<int> PreviousStock { get; set; }
        public Nullable<decimal> DiscountPerItem { get; set; }
    }

    public class Order
    {
        public Nullable<int> OrderId { get; set; }
        public string OrderCode { get; set; }
        public Customer customer { get; set; }
        public Employee employee { get; set; }
        public Employee saleEmployee { get; set; }
        public Branch branch { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> CustomerPaid { get; set; }
        public Nullable<decimal> CashesPaid { get; set; }
        public Nullable<decimal> ReturnPaid { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<int> OrderStatus { get; set; }
        public string Note { get; set; }
        public string OrderAddress { get; set; }
        public string OrderPhone { get; set; }
        public Nullable<int> DeliveryVendorId { get; set; }
        public DeliveryVendor deliveryVendor { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> ProductCount { get; set; }
        public Nullable<decimal> DeliveryPrice { get; set; }
        public Nullable<decimal> OrderPrice { get; set; }
        public Nullable<decimal> MoneyCollect { get; set; }
    }

    public class OrderDetailProduct
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> OrderProductId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SubName { get; set; }
        public Unit unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> QuantityReturn { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public List<PI> images { get; set; }
    }

    public class Return
    {
        public Nullable<int> ReturnId { get; set; }
        public string Code { get; set; }
        public Customer customer { get; set; }
        public Employee employee { get; set; } 
        public Branch branch { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<decimal> CashesPaid { get; set; }   
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }  
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> ProductCount { get; set; }   
    }

    public class ReturnProduct
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> ReturnProductId { get; set; }
        public Nullable<int> ReturnId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SubName { get; set; }
        public Unit unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public List<PI> images { get; set; }
    }

    public class ReturnDetailProduct
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> ReturnProductId { get; set; }
        public Nullable<int> ReturnId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SubName { get; set; }
        public Unit unit { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public List<PI> images { get; set; }
    }

    public class OrderDetailCash
    {
        public Nullable<int> CashId { get; set; }
        public string CashCode { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> Total { get; set; }     
        public CashType cashType { get; set; }
        public Employee employee { get; set; }
    }

    public class ReturnDetailCash
    {
        public Nullable<int> CashId { get; set; }
        public string CashCode { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> Total { get; set; }
        public CashType cashType { get; set; }
        public Employee employee { get; set; }
    }

    public class OrderDetail
    {
        public Nullable<int> OrderId { get; set; }
        public string OrderCode { get; set; }
        public Customer customer { get; set; }
        public Employee employee { get; set; }
        public Branch branch { get; set; }
        public Nullable<decimal> OrderPrice { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> Delivery { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> CustomerPaid { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public string Note { get; set; }
        public string OrderAddress { get; set; }
        public string OrderPhone { get; set; }
        public Nullable<int> DeliveryVendorId { get; set; }
        public DeliveryVendor deliveryVendor { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public List<OrderDetailProductWithUnit> products { get; set; }
    }

    public class OrderDetailProductWithUnit
    {
        public Nullable<int> ProductId { get; set; }     
        public Nullable<int> ProductAttributeId { get; set; }
        public string SubName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }      
        public string Code { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> PriceOriginAvg { get; set; }
        public List<ProductPrice> prices { get; set; }
        public ProductUnit unit { get; set; }
    }

    public class ProductPrice
    {
        public Nullable<int> ProductPriceId { get; set; }
        public string Unit { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> OriginPrice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
    }

    public class ReturnOrder
    {
        public Nullable<int> OrderId { get; set; }
        public Nullable<decimal> fee { get; set; }
        public Nullable<decimal> paid { get; set; }
        public Nullable<decimal> TotalCustomerCashes { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<int> PaymentType { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> CustomerId { get; set; }        
        public string Note { get; set; }
        public List<ReturnOrderProduct> products { get; set; }

    }

    public class ReturnOrderProduct
    {
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> ProductUnitId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> MaxQuantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> PriceOriginAvg { get; set; }
    }

    public class CancelOrder
    {  
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> OrderId { get; set; }
    }

    public class CancelCashes
    {
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> CashId { get; set; }
    }

    public class EmployeeDetail
    {
        public Nullable<int> EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Username { get; set; }
        public string Group { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Branch branch { get; set; }
    }

    public class Action
    {
        public Nullable<int> ActionId { get; set; }
        public string ActionName { get; set; }
        public Employee employee { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public string ActionType { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public ActionTarget target { get; set; }
    }

    public class ActionTarget
    {
        public Nullable<int> TargetId { get; set; }
        public string TargetName { get; set; }
        public string TargetCode { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<decimal> Price { get; set; }
    }

    public class Manufacture
    {
        public Nullable<int> ManufactureId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string Name { get; set; }
        public string Origin { get; set; }
    }

    public class Page
    {
        public string id { get; set; }
        public string name { get; set; }
        public int PageId { get; set; }        
        public Nullable<bool> isSubscribed { get; set; }        
    }

    public class Transfer
    {
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> FromBranchId { get; set; }
        public Nullable<int> ToBranchId { get; set; }        
        public string Note { get; set; }
        public Nullable<decimal> Total { get; set; }
        public List<TransferProduct> products { get; set; }
    }

    public class TransferProduct
    {
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
    }

    public partial class EmployeeLogin
    {
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> ShopId { get; set; }
        public string ShopName { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string EmployeeName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> GroupId { get; set; }
        public string Group { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> TokenSince { get; set; }
        public string FacebookUserId { get; set; }
    }

    public class VendorCash
    {
        public Nullable<int> CashId { get; set; }
        public string CashCode { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> Total { get; set; }
        public CashType cashType { get; set; }
        public Employee employee { get; set; }
        public Nullable<int> Status { get; set; }
    }

    public class PI
    {
        public Nullable<int> ProductImageId { get; set; }
        public string Path { get; set; }
        public string RelativePath { get; set; }
        public string Alt { get; set; }
    }

    public class ShopTransfer
    {
        public Nullable<int> TransferId { get; set; }
        public string Code { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Branch fromBranch { get; set; }
        public Branch toBranch { get; set; }
        public string Note { get; set; }
        public Employee employee { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> Status { get; set; }
    }

    public class ShopTransferProduct
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public Nullable<int> TransferProductId { get; set; }
        public Nullable<int> TransferId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SubName { get; set; }      
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }        
    }

    public class CreateCash
    {
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetCode { get; set; }
        public string TargetType { get; set; }
        public Nullable<int> CashTypeId { get; set; }
        public string Note { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<int> PaidTargetId { get; set; }
        public string PaidTargetType { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> Type { get; set; }
        public bool IsAddToFlow { get; set; }
    }

    public class ImportDetail
    {
        public int EximId { get; set; }
        public string Code { get; set; }
        public Nullable<int> TargetId { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> TotalDiscount { get; set; }
        public Nullable<decimal> VendorPaid { get; set; }
        public Target target { get; set; }        
        public Nullable<DateTime> CreatedAt { get; set; }   
        public List<ImportDetailProduct> products { get; set; }
        public Nullable<int> Status { get; set; }
    }

    public class ImportDetailCash
    {
        public int CashId { get; set; }
        public Nullable<Decimal> PaidAmount { get; set; }
        public string CashCode { get; set; }       
    }

    public class ImportDetailProduct 
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> ProductAttributeId { get; set; }
        public string SubName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Code { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> MaxQuantity { get; set; }
        public Nullable<decimal> Price { get; set; }       
    }

    public class ReturnImport
    {
        public Nullable<int> EximId { get; set; }
        public List<ReturnImportProduct> products { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<decimal> fee { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> VendorCash { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<int> PaymentMethod { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public string Note { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string VendorName { get; set; }
    }

    public class ReturnImportProduct
    {
        public int ProductAttributeId { get; set; }
        public int Quantity { get; set; }
        public decimal ReturnPrice { get; set; }
    }

    //import return
    public class ImportReturn
    {
        public Nullable<int> EximId { get; set; }
        public string Code { get; set; }
        public Vendor vendor { get; set; }
        public Employee employee { get; set; }
        public Branch branch { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<int> Status { get; set; }
        public string Note { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public Nullable<int> ProductCount { get; set; }
        public Nullable<int> TargetId { get; set; }      
    }    
    //end import return
}