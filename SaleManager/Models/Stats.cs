﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class Stats
    {
        public int total { get; set; }
        public int comment { get; set; }
        public int mess { get; set; }
        public int post { get; set; }
        public int postHaveComment { get; set; }
        public int postHaveReply { get; set; }
        public int messReceive { get; set; }
        public int messReply { get; set; }
    }

    public class OrderStats
    {
        public int total { get; set; }
        public int totalProcess { get; set; }
        public int totalFinish { get; set; }
        //Số tiền bán hàng
        public decimal totalMoney { get; set; }
        //Số tiền đã thu
        public decimal totalReceive { get; set; }
        //Số tiền còn thiếu
        public decimal totalLost { get; set; }
    }
}