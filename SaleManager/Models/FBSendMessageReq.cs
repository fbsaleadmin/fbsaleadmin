﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Facebook
{
    public class FBSendMessageReq
    {
        public Recipient recipient { get; set; }
        public Message message { get; set; }
        public string sender_action { get; set; }
    }

    public class Recipient
    {
        public string phone_number { get; set; }
        public string id { get; set; }

        public Recipient() {
        
        }

        public Recipient(string id)
        {
            this.id = id;
        }
    }

    public class Message
    {
        public string text { get; set; }
        public Attachment attachment { get; set; }
    }

    public class Attachment
    {
        public string type { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string url { get; set; }
        public bool is_reusable { get; set; }
        public string attachment_id { get; set; }
    }
}