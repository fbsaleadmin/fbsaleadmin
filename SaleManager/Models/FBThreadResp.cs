﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FBThreadResp
    {
        public string id {get;set;}
        public FBThreadMessages messages { get; set; }
    }

    public class FBThreadMessages
    {
        public List<FBThreadMessageItem> data { get; set; }
    }

    public class FBThreadMessageItem
    {
        public string message { get; set; }
        public string id { get; set; }
        public DateTime created_time { get; set; }
        public FBThreadMessageFrom from { get; set; }
        public FBThreadMessageTo to { get; set; }
        public FBThreadMessageAtt attachments { get; set; }
    }
    public class FBThreadMessageFrom
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }        
    }
    public class FBThreadMessageTo
    {
        public List<FBThreadMessageToItem> data { get; set; }
    }

    public class FBThreadMessageToItem
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }

    public class FBThreadMessageAtt
    {
        public List<FBThreadMessageAttItem> data { get; set; }
    }

    public class FBThreadMessageAttItem
    {
        public FBThreadMessageAttImageData image_data { get; set; }
    }

    public class FBThreadMessageAttImageData
    {
        public int width { get; set; }
        public int height { get; set; }
        public int max_width { get; set; }
        public int max_height { get; set; }
        public string url { get; set; }
        public string preview_url { get; set; }
        public int image_type { get; set; }
        public bool render_as_sticker { get; set; }
    }
}