﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models
{
    public class FBOnlyAttachmentResp
    {
        public FBAttachment attachment { get; set; }
        public string id { get; set; }
    }   
 
    public class FBAttachment
    {
        public FBAttachmentMedia media { get; set; }
    }

    public class FBAttachmentMedia
    {
        public FBAttachmentMediaImage image { get; set; }
    }

    public class FBAttachmentMediaImage
    {
        public int width { get; set; }
        public int height { get; set; }
        public string src { get; set; }
    }
}