﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Report
{
    public class SaleByTime
    {
        public int? OrderId { get; set; }
        public string OrderCode { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? BranchId { get; set; }
        public int? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string CustomerName { get; set; }
        public Nullable<decimal> TotalPriceSale { get; set; }
        public Nullable<decimal> TotalPriceReturn { get; set; }
    }
}