﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Report
{
    public class FinanceByQuarter
    {
        public Nullable<int> Quarter { get; set; }
        public Nullable<decimal> TotalProfit { get; set; }
    }
}