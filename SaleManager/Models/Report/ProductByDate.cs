﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Report
{
    public class ProductByDate
    {
        public string ProductCode { get; set; }
        public string ProductFullName { get; set; }
        public Nullable<int> QuantitySale { get; set; }
        public Nullable<decimal> TotalPriceSale { get; set; }
        public Nullable<int> QuantityReturn { get; set; }
        public Nullable<decimal> TotalPriceReturn { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
    }
}