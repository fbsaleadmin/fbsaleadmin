﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Report
{
    public class EximByProduct
    {
        public string ProductCode { get; set; }
        public int? ProductAttributeId { get; set; }
        public int? ProductUnitId { get; set; }
        public string ProductFullName { get; set; }
        public Nullable<int> QuantityInportStart { get; set; }
        public Nullable<decimal> TotalPriceInportStart { get; set; }
        public Nullable<int> QuantityExportStart { get; set; }
        public Nullable<decimal> TotalPriceExportStart { get; set; }
        public Nullable<int> QuantityInport { get; set; }
        public Nullable<decimal> TotalPriceInport { get; set; }
        public Nullable<int> QuantityExport { get; set; }
        public Nullable<decimal> TotalPriceExport { get; set; }       
    }
}