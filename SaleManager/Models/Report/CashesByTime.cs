﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Report
{
    public class CashesByTime
    {
        public string CashCode { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string CashTypeName { get; set; }
        public string PaidTargetName { get; set; }
        public int? PaidTargetId { get; set; }
        public int? CashTypeId { get; set; }
        public string PaidTargetType { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public Nullable<decimal> Total { get; set; }
    }
}