﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleManager.Models.Report
{
    public class SaleByProduct
    {
        public int? OrderId { get; set; }
        public int? BranchId { get; set; }
        public int? ProductAttributeId { get; set; }
        public int? ProductUnitId { get; set; }
        public string ProductCode { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string ProductFullName { get; set; }
        public Nullable<int> QuantitySale { get; set; }
        public Nullable<decimal> TotalPriceSale { get; set; }
        public Nullable<int> QuantityReturn { get; set; }
        public Nullable<decimal> TotalPriceReturn { get; set; }
        public Nullable<decimal> TotalOriginPrice { get; set; }
        public Nullable<decimal> TotalProfit { get; set; }
    }
}