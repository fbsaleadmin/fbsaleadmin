﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaleManager
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class SalesManagerEntities : DbContext
    {
        public SalesManagerEntities()
            : base("name=SalesManagerEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Action> Actions { get; set; }
        public virtual DbSet<Attribute> Attributes { get; set; }
        public virtual DbSet<AttributeMap> AttributeMaps { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<BranchStock> BranchStocks { get; set; }
        public virtual DbSet<Cash> Cashes { get; set; }
        public virtual DbSet<CashType> CashTypes { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Conversation> Conversations { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Manufacture> Manufactures { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderProduct> OrderProducts { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductAttribute> ProductAttributes { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<Reply> Replies { get; set; }
        public virtual DbSet<Shop> Shops { get; set; }
        public virtual DbSet<ShopCategory> ShopCategories { get; set; }
        public virtual DbSet<Template> Templates { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Vendor> Vendors { get; set; }
        public virtual DbSet<CustomerGroup> CustomerGroups { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Label> Labels { get; set; }
        public virtual DbSet<LabelConversation> LabelConversations { get; set; }
        public virtual DbSet<BlockList> BlockLists { get; set; }
        public virtual DbSet<Config> Configs { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<ProductPrice> ProductPrices { get; set; }
        public virtual DbSet<DeliveryVendor> DeliveryVendors { get; set; }
        public virtual DbSet<ProductUnit> ProductUnits { get; set; }
        public virtual DbSet<Debt> Debts { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Exim> Exims { get; set; }
        public virtual DbSet<EximProduct> EximProducts { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<Bundle> Bundles { get; set; }
        public virtual DbSet<UserBundle> UserBundles { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Dictionary> Dictionaries { get; set; }
        public virtual DbSet<ShopDictionary> ShopDictionaries { get; set; }
        public virtual DbSet<CashFlow> CashFlows { get; set; }
        public virtual DbSet<Transfer> Transfers { get; set; }
        public virtual DbSet<TransferProduct> TransferProducts { get; set; }
    }
}
