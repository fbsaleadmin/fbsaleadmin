﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;
using SaleManager.Data;

namespace SaleManager.Controllers
{
    public class ProductPricesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/ProductPrices/5       
        public IHttpActionResult GetProductPrice(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductPriceDTO productPrice = db.ProductPrices.Where(p => p.ProductPriceId == id).Select(p => new ProductPriceDTO()
            {
                FromDate = p.FromDate,
                IsCurrent = p.IsCurrent,
                OriginPrice = p.OriginPrice,
                ProductAttributeId = p.ProductAttributeId,
                ProductPriceId = p.ProductPriceId,
                SalePrice = p.SalePrice,
                UtilDate = p.UtilDate,
                ProductUnitId = p.ProductPriceId
            }).FirstOrDefault();
            if (productPrice == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = productPrice;
            return Ok(def);
        }

        // PUT: api/ProductPrices/5       
        //public IHttpActionResult PutProductPrice(int id, ProductPriceDTO productPrice)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    if (id != productPrice.ProductPriceId)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //    ProductPrice current = db.ProductPrices.Where(s => s.ProductPriceId == id).FirstOrDefault();
        //    if (current == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        current.IsCurrent = productPrice.IsCurrent;
        //        current.OriginPrice = productPrice.OriginPrice;
        //        current.SalePrice = productPrice.SalePrice;
        //        current.ProductUnitId = productPrice.ProductUnitId;
        //        current.UtilDate = DateTime.Now;

        //        db.Entry(current).State = EntityState.Modified;
        //        try
        //        {
        //            db.SaveChanges();
        //            def.meta = new Meta(200, "Success");
        //            return Ok(def);
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            def.meta = new Meta(500, "Internal Server Error");
        //            return Ok(def);
        //        }
        //    }  
        //}

        //// POST: api/ProductPrices       
        //public IHttpActionResult PostProductPrice(ProductPriceDTO productPrice)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    ProductPrice c = new ProductPrice();
        //    c.FromDate = DateTime.Now;
        //    c.IsCurrent = productPrice.IsCurrent;
        //    c.OriginPrice = productPrice.OriginPrice;
        //    c.ProductAttributeId = productPrice.ProductAttributeId;
        //    c.SalePrice = productPrice.SalePrice;
        //    c.UtilDate = DateTime.Now;
        //    c.ProductUnitId = productPrice.ProductUnitId;        

        //    db.ProductPrices.Add(c);
        //    db.SaveChanges();
        //    productPrice.ProductPriceId = c.ProductPriceId;

        //    def.meta = new Meta(200, "Success");
        //    def.data = productPrice;

        //    return Ok(def);
        //}

        // DELETE: api/ProductPrices/5      
        public IHttpActionResult DeleteProductPrice(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductPrice productPrice = db.ProductPrices.Find(id);
            if (productPrice == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.ProductPrices.Remove(productPrice);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpPost]
        [Route("api/productprices/UpdateNewPrice")]
        public IHttpActionResult UpdateNewPrice(UpdateProductPrice productPrice)
        {
            //check session valid
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());            
            DefaultResponse def = new DefaultResponse();
            //find product and unit
            ProductPrice pp = db.ProductPrices.Where(p => p.ProductPriceId == productPrice.ProductPriceId && p.IsCurrent == true).FirstOrDefault();
            if (pp != null)
            {
                ProductAttribute pa = db.ProductAttributes.Find(pp.ProductAttributeId);
                if (pa != null)
                {
                    if (pa.Product.ShopId != ShopId)
                    {
                        def.meta = new Meta(404, "Not found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not found");
                    return Ok(def);
                }
                ProductPrice newPrice = new ProductPrice();
                newPrice.FromDate = DateTime.Now;
                newPrice.IsCurrent = true;
                newPrice.OriginPrice = pp.OriginPrice;
                newPrice.ProductAttributeId = pp.ProductAttributeId;
                newPrice.ProductUnitId = pp.ProductUnitId;
                newPrice.SalePrice = productPrice.NewPrice;
                newPrice.UtilDate = DateTime.Now;
                db.ProductPrices.Add(newPrice);
                pp.IsCurrent = false;
                pp.UtilDate = DateTime.Now;
                db.SaveChanges();
                //insert action
                Action action = new Action();
                action.ActionName = "thay đổi giá sản phẩm";
                action.ActionType = "UPDATE";
                action.CreatedAt = DateTime.Now;
                action.EmployeeId = productPrice.EmployeeId;
                action.Result = 0;
                action.ShopId = ShopId;
                action.TargetId = pa.ProductAttributeId;
                action.TargetType = "PRODUCT_ATTRIBUTE";
                db.Actions.Add(action);
                db.SaveChanges();
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(404, "Not found");
                return Ok(def);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductPriceExists(int id)
        {
            return db.ProductPrices.Count(e => e.ProductPriceId == id) > 0;
        }
    }
}