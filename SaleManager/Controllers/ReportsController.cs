﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Security.Claims;
using System.IO;
using System.Configuration;
using SaleManager.Data;
using SaleManager.Models.Report;
using log4net;


namespace SaleManager.Controllers
{
    [Authorize]
    public class ReportsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        private DateTimeReport getStartAndEndDate(string type)
        {
            var today = DateTime.Now;
            DateTimeReport dtp = new DateTimeReport();
            //type=1--> Hôm nay, =2--> tuần này,=3--> tháng này, =4--> quý này, =5--> năm nay
            //type=6--> Hôm qua, =7--> tuần trước,=8--> 7 ngày qua, =9--> tháng trước, =10--> quý trước, =11--> năm trước
            if (type == "1")
            {
                //hôm nay
                dtp.startDate = new DateTime(today.Year, today.Month, today.Day, 00, 00, 00);
                dtp.endDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
            }
            else if (type == "2")
            {
                //tuần này
                var dayOfWeek = today.DayOfWeek;
                int start = 0;
                if (dayOfWeek == DayOfWeek.Sunday)
                {
                    start = 6;
                }
                else
                {
                    start = dayOfWeek - DayOfWeek.Monday;
                }
                var startD = today.AddDays(-start);
                var endD = today.AddDays(-start + 6);
                dtp.startDate = new DateTime(startD.Year, startD.Month, startD.Day, 00, 00, 00);
                dtp.endDate = new DateTime(endD.Year, endD.Month, endD.Day, 23, 59, 59);
            }
            else if (type == "3")
            {
                //tháng này
                var startD = today.AddDays((-today.Day) + 1);
                var dtResult = today.AddMonths(1);
                var endD = dtResult.AddDays(-(dtResult.Day));
                dtp.startDate = new DateTime(startD.Year, startD.Month, startD.Day, 00, 00, 00);
                dtp.endDate = new DateTime(endD.Year, endD.Month, endD.Day, 23, 59, 59);
            }
            else if (type == "4")
            {
                //quý này
                if (today.Month >= 1 && today.Month <= 3)
                {
                    dtp.startDate = new DateTime(today.Year, 1, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 3, 31, 23, 59, 59);
                }
                else if (today.Month >= 4 && today.Month <= 6)
                {
                    dtp.startDate = new DateTime(today.Year, 4, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 6, 30, 23, 59, 59);
                }
                else if (today.Month >= 7 && today.Month <= 9)
                {
                    dtp.startDate = new DateTime(today.Year, 7, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 9, 30, 23, 59, 59);
                }
                else if (today.Month >= 10 && today.Month <= 12)
                {
                    dtp.startDate = new DateTime(today.Year, 10, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 12, 31, 23, 59, 59);
                }
            }
            else if (type == "5")
            {
                //năm này
                dtp.startDate = new DateTime(today.Year, 1, 1, 00, 00, 00);
                dtp.endDate = new DateTime(today.Year, 12, 31, 23, 59, 59);
            }
            else if (type == "6")
            {
                //hôm qua
                today = today.AddDays(-1);
                dtp.startDate = new DateTime(today.Year, today.Month, today.Day, 00, 00, 00);
                dtp.endDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
            }
            else if (type == "7")
            {
                //tuần trước
                today = today.AddDays(-7);
                var dayOfWeek = today.DayOfWeek;
                int start = 0;
                if (dayOfWeek == DayOfWeek.Sunday)
                {
                    start = 6;
                }
                else
                {
                    start = dayOfWeek - DayOfWeek.Monday;
                }
                var startD = today.AddDays(-start);
                var endD = today.AddDays(-start + 6);
                dtp.startDate = new DateTime(startD.Year, startD.Month, startD.Day, 00, 00, 00);
                dtp.endDate = new DateTime(endD.Year, endD.Month, endD.Day, 23, 59, 59);
            }
            else if (type == "8")
            {
                //7 ngày qua
                var startD = today.AddDays(-7);
                var endD = today;
                dtp.startDate = new DateTime(startD.Year, startD.Month, startD.Day, 00, 00, 00);
                dtp.endDate = new DateTime(endD.Year, endD.Month, endD.Day, 23, 59, 59);
            }
            else if (type == "9")
            {
                //tháng trước
                today = today.AddMonths(-1);
                var startD = today.AddDays((-today.Day) + 1);
                var dtResult = today.AddMonths(1);
                var endD = dtResult.AddDays(-(dtResult.Day));
                dtp.startDate = new DateTime(startD.Year, startD.Month, startD.Day, 00, 00, 00);
                dtp.endDate = new DateTime(endD.Year, endD.Month, endD.Day, 23, 59, 59);
            }
            else if (type == "10")
            {
                //quý trước
                today = today.AddMonths(-3);
                if (today.Month >= 1 && today.Month <= 3)
                {
                    dtp.startDate = new DateTime(today.Year, 1, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 3, 31, 23, 59, 59);
                }
                else if (today.Month >= 4 && today.Month <= 6)
                {
                    dtp.startDate = new DateTime(today.Year, 4, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 6, 30, 23, 59, 59);
                }
                else if (today.Month >= 7 && today.Month <= 9)
                {
                    dtp.startDate = new DateTime(today.Year, 7, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 9, 30, 23, 59, 59);
                }
                else if (today.Month >= 10 && today.Month <= 12)
                {
                    dtp.startDate = new DateTime(today.Year, 10, 1, 00, 00, 00);
                    dtp.endDate = new DateTime(today.Year, 12, 31, 23, 59, 59);
                }
            }
            else if (type == "11")
            {
                //năm trước
                today = today.AddYears(-1);
                dtp.startDate = new DateTime(today.Year, 1, 1, 00, 00, 00);
                dtp.endDate = new DateTime(today.Year, 12, 31, 23, 59, 59);
            }

            return dtp;
        }

        #region Báo cáo hàng ngày
        //Lấy dánh sách sản phẩm bán trong ngày
        [HttpGet]
        [Route("api/reports/{id}/{branch}/date-sale")]
        public IHttpActionResult reportDateSale(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from od in db.Orders
                             join op in db.OrderProducts on od.OrderId equals op.OrderId
                             where od.ShopId == id && od.BranchId == branch && od.OrderStatus!=-1 && od.Type== (int)Const.OrderType.SALE
                             && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                             orderby od.OrderId descending
                             group op by new
                             {
                                  od.OrderCode,
                                  od.CreatedAt
                             } into god
                             select new 
                             {
                                  god.Key.OrderCode,
                                  CreatedAt=god.FirstOrDefault().Order.CreatedAt,
                                  Quantity = god.Sum(p => p.Quantity),
                                  OrderPrice = god.FirstOrDefault().Order.OrderPrice,
                                  DeliveryPrice=god.FirstOrDefault().Order.DeliveryPrice,
                                  DiscountPrice = god.FirstOrDefault().Order.DiscountPrice
                             }).AsEnumerable();

            int count = result.Count();
            int totalproduct = result.Sum(e => e.Quantity).Value;
            decimal totalorderprice = result.Sum(e => e.OrderPrice).Value;
            decimal totaldeliveryprice = result.Sum(e => e.DeliveryPrice).Value;
            decimal totaldiscountprice = result.Sum(e => e.DiscountPrice).Value;

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
                count = result.Count();
                totalproduct = result.Sum(e => e.Quantity).Value;
                totalorderprice = result.Sum(e => e.OrderPrice).Value;
                totaldeliveryprice = result.Sum(e => e.DeliveryPrice).Value;
                totaldiscountprice = result.Sum(e => e.DiscountPrice).Value;
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            string branchName = db.Branches.Find(branch).BranchName;

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataDateSale(count, totalproduct, totalorderprice, totaldeliveryprice, totaldiscountprice);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Lấy dánh sách thu chi trong ngày
        [HttpGet]
        [Route("api/reports/{id}/{branch}/date-cashe")]
        public IHttpActionResult reportDateCashes(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from c in db.Cashes
                          where c.ShopId == id 
                          orderby c.CreatedAt descending
                          select new CashesByTime
                          {
                              CashCode = c.CashCode,
                              CreatedAt = c.CreatedAt,
                              PaidAmount = c.PaidAmount,
                              Total = c.Total,
                              PaidTargetId = c.PaidTargetId,
                              PaidTargetType = c.PaidTargetType,
                              CashTypeId=c.CashTypeId,
                              CashTypeName=c.CashType.Name,
                              PaidTargetName="",
                          }
                         ).AsEnumerable();           

            

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            int count = result.Count();
            decimal totalpaid = result.Sum(e => e.PaidAmount).Value;
            decimal total = result.Sum(e => e.Total).Value;

            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            var returnData = result.ToList();

            //select target
            for (int i = 0; i < returnData.Count; i++)
            {
                int paidTargetId = (int)returnData[i].PaidTargetId;
                switch (returnData[i].PaidTargetType)
                {
                    case "CUSTOMER":
                        returnData[i].PaidTargetName = db.Customers.Where(c => c.CustomerId == paidTargetId).FirstOrDefault().CustomerName;
                        break;
                    case "VENDOR":
                        returnData[i].PaidTargetName = db.Vendors.Where(c => c.VendorId == paidTargetId).FirstOrDefault().VendorName;
                        break;
                    case "DELIVERY_VENDOR":
                        returnData[i].PaidTargetName = db.DeliveryVendors.Where(c => c.DeliveryVendorId == paidTargetId).FirstOrDefault().VendorName;
                        break;
                    default:
                        returnData[i].PaidTargetName = "";
                        break;
                }

                var cashTypeId = returnData[i].CashTypeId;
                if (cashTypeId != null)
                {
                    returnData[i].CashTypeName = db.CashTypes.Where(c => c.CashTypeId == cashTypeId).FirstOrDefault().Name;
                }
            }

            if (returnData == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }    
                   
            string branchName = db.Branches.Find(branch).BranchName;

            def.meta = new Meta(200, "Success");
            def.data = returnData.ToList();
            def.metadata = new MetadataDateCashes(count, totalpaid, total);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Lấy dánh sách hàng hóa trong ngày
        [HttpGet]
        [Route("api/reports/{id}/{branch}/date-product")]
        public IHttpActionResult reportDateProduct(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE 
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus!=(int)Const.OrderStatus.CANCELED
                          && pa.Status!=(int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              QuantitySale = op.Quantity,
                              TotalPriceSale = op.TotalPrice,
                              QuantityReturn = 0,
                              TotalPriceReturn = 0
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.OrderProducts.Where(e => e.Order.RelatedOrderId == item.OrderId).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.QuantityReturn += od.Quantity;
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }
            }

            var result2 = (from op in returnData
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductCode = god.Key.ProductCode,
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               CreatedAt = god.FirstOrDefault().CreatedAt,
                               QuantitySale = god.Sum(p => p.QuantitySale),
                               TotalPriceSale = god.Sum(p => p.TotalPriceSale),
                               QuantityReturn = god.Sum(p => p.QuantityReturn),
                               TotalPriceReturn = god.Sum(p => p.TotalPriceReturn)
                           }).AsEnumerable();

            int count = result2.Count();
            decimal totalsale = result2.Sum(e => e.QuantitySale).Value;
            decimal totalpricesale = result2.Sum(e => e.TotalPriceSale).Value;
            decimal totalreturn = result2.Sum(e => e.QuantityReturn).Value;
            decimal totalpricereturn = result2.Sum(e => e.TotalPriceReturn).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            string branchName = db.Branches.Find(branch).BranchName;

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataDateProduct(count, totalsale, totalpricesale, totalreturn, totalpricereturn);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        #endregion

        #region Báo cáo bán hàng
        //Báo cáo bán hàng theo thời gian
        [HttpGet]
        [Route("api/reports/{id}/sale-time")]
        public IHttpActionResult reportSaleTime(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from cus in db.Customers
                         join od in db.Orders on cus.CustomerId equals od.CustomerId
                         where od.ShopId == id && od.Type==(int)Const.OrderType.SALE
                         && od.OrderStatus!=(int)Const.OrderStatus.CANCELED && od.OrderStatus!=(int)Const.OrderStatus.DELETED
                         orderby od.OrderCode descending
                         group od by new
                         {
                             od.OrderId,
                             od.OrderCode,
                             od.CreatedAt,
                         } into god
                         select new SaleByTime
                         {
                             OrderId=god.Key.OrderId,
                             BranchId=god.FirstOrDefault().BranchId,
                             OrderCode= god.FirstOrDefault().OrderCode,
                             CreatedAt = god.FirstOrDefault().CreatedAt,
                             CustomerName = god.FirstOrDefault().Customer.CustomerName,
                             TotalPriceSale = god.Sum(p=>p.TotalPrice),
                             TotalPriceReturn = 0,
                         }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            int count = result.Count();

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.Orders.Where(e => e.RelatedOrderId == item.OrderId).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }
            }

            decimal totalprice = returnData.Sum(e => e.TotalPriceSale).Value;
            decimal totalpricereturn = returnData.Sum(e => e.TotalPriceReturn).Value;

            var result2 = returnData.AsEnumerable();
            result2= result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataSale(count, totalprice, totalpricereturn);
            return Ok(def);
        }

        //Biều đồ bán hàng theo thời gian
        [HttpGet]
        [Route("api/reports/{id}/sale-time-chart")]
        public IHttpActionResult reportSaleTimeChart(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = from od in db.Orders
                         where od.ShopId == id && od.Type==0
                         && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                         orderby od.CreatedAt descending
                         select new
                         {
                             CreatedAt=od.CreatedAt,
                             BranchId=od.BranchId,
                             TotalPrice = od.TotalPrice
                         };

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 = from od in result
                         where od.CreatedAt.HasValue
                         group od by new
                         {
                             od.CreatedAt.Value.Day,
                             od.CreatedAt.Value.Month,
                             od.CreatedAt.Value.Year
                         } into god
                         select new
                         {
                             Day = god.Key.Day,
                             Month = god.Key.Month,
                             Year = god.Key.Year,
                             TotalPrice = god.Sum(p => p.TotalPrice)
                         };

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result2.Count());
            return Ok(def);
        }

        //Báo cáo lợi nhuận theo hóa đơn
        [HttpGet]
        [Route("api/reports/{id}/sale-profit")]
        public IHttpActionResult reportSaleProfit(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from od in db.Orders
                          where od.ShopId == id && od.Type==(int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                         orderby od.CreatedAt descending
                         group od by new
                         {
                             od.CreatedAt
                         } into god
                         select new
                         {
                             god.Key.CreatedAt,
                             BranchId=god.FirstOrDefault().BranchId,
                             OrderPrice = god.Sum(p => p.OrderPrice),
                             DiscountPrice = god.Sum(p => p.DiscountPrice),
                             DeliveryPrice = god.Sum(p => p.DeliveryPrice),
                             TotalPrice = god.Sum(p => p.TotalPrice),
                             TotalOriginPrice = god.Sum(p => p.TotalOriginPrice)
                         }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            int count = result.Count();
            decimal totalprice = result.Sum(e => e.TotalPrice).Value;
            decimal totaldelivery = result.Sum(e => e.DeliveryPrice).Value;
            decimal totaldiscount = result.Sum(e => e.DiscountPrice).Value;
            decimal totalorderprice = result.Sum(e => e.OrderPrice).Value;
            decimal totaloriginprice = result.Sum(e => e.TotalOriginPrice).Value;

            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataSale(count, totalorderprice, totaldiscount, totaldelivery, totalprice, totaloriginprice);
            return Ok(def);
        }

        //Biều đồ lợi nhuận theo hóa đơn
        [HttpGet]
        [Route("api/reports/{id}/{branch}/sale-profit-chart")]
        public IHttpActionResult reportSaleProfitChart(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = from od in db.Orders
                         where od.ShopId == id && od.BranchId==branch && od.Type==(int)Const.OrderType.SALE
                         && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                         select new
                         {
                             CreatedAt=od.CreatedAt,
                             BranchId=od.BranchId,
                             OrderPrice=od.OrderPrice,
                             TotalPrice = od.TotalPrice,
                             TotalOriginPrice=od.TotalOriginPrice
                         };

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 = from od in result
                         where od.CreatedAt.HasValue
                         orderby od.CreatedAt.Value.Day descending
                         group od by new
                         {
                             od.CreatedAt.Value.Day,
                             od.CreatedAt.Value.Month,
                             od.CreatedAt.Value.Year
                         } into god
                         select new
                         {
                             Day = god.Key.Day,
                             Month = god.Key.Month,
                             Year = god.Key.Year,
                             OrderPrice = god.Sum(p => p.OrderPrice),
                             TotalPrice = god.Sum(p => p.TotalPrice),
                             TotalOriginPrice = god.Sum(p => p.TotalOriginPrice)
                         };


            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result2.Count());
            return Ok(def);
        }

        //Báo cáo trả hàng theo hóa đơn
        [HttpGet]
        [Route("api/reports/{id}/sale-return-product")]
        public IHttpActionResult reportSaleReturnProduct(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from cus in db.Customers
                          join od in db.Orders on cus.CustomerId equals od.CustomerId
                          where od.ShopId == id && od.Type == (int)Const.OrderType.RETURN
                          && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                          orderby od.OrderCode descending
                          group od by new
                          {
                              od.OrderCode,
                              od.CreatedAt,
                          } into god
                          select new SaleByTime
                          {                            
                              OrderCode = god.Key.OrderCode,
                              BranchId=god.FirstOrDefault().BranchId,
                              CreatedAt = god.FirstOrDefault().CreatedAt,
                              CustomerName = god.FirstOrDefault().Customer.CustomerName,
                              TotalPriceSale = 0,
                              TotalPriceReturn = god.Sum(p => p.TotalPrice),
                              OrderId = god.FirstOrDefault().RelatedOrderId
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odSale = db.Orders.Where(e => e.OrderId == item.OrderId && e.OrderStatus != (int)Const.OrderStatus.CANCELED && e.OrderStatus != (int)Const.OrderStatus.DELETED).ToList();
                if (odSale != null && odSale.Count > 0)
                {
                    foreach (var od in odSale)
                    {
                        item.TotalPriceSale += od.TotalPrice;
                    }
                }
            }
            var result2 = returnData.AsEnumerable();
            int count = result2.Count();
            decimal totalprice = result2.Sum(e => e.TotalPriceSale).Value;
            decimal totalpricereturn = result2.Sum(e => e.TotalPriceReturn).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataSale(count, totalprice, totalpricereturn);
            return Ok(def);
        }

        //Báo cáo về giảm giá hóa đơn
        [HttpGet]
        [Route("api/reports/{id}/{branch}/sale-subprice")]
        public IHttpActionResult reportSaleSubPrice(int id, int branch,[FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from cus in db.Customers
                         join od in db.Orders on cus.CustomerId equals od.CustomerId
                         join em in db.Employees on od.EmployeeId equals em.EmployeeId
                          where od.ShopId == id && od.BranchId == branch && od.Type==0
                          && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                         orderby od.OrderCode descending
                         group od by new
                         {
                             od.OrderCode,
                             od.CreatedAt,
                         } into god
                         select new
                         {
                             OrderCode = god.Key.OrderCode,
                             CreatedAt = god.FirstOrDefault().CreatedAt,
                             EmployeeName= god.FirstOrDefault().Employee.EmployeeName,
                             CustomerName = god.FirstOrDefault().Customer.CustomerName,
                             TotalPrice = god.Sum(p => p.OrderPrice),
                             DiscountPrice = god.Sum(p => p.DiscountPrice),
                         }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            int count = result.Count();
            decimal totalprice = result.Sum(e => e.TotalPrice).Value;
            decimal totaldiscount = result.Sum(e => e.DiscountPrice).Value;
            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            string branchName = db.Branches.Find(branch).BranchName;

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataSale(count, totalprice, totaldiscount);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Báo cáo và biều đồ bán hàng theo nhân viên
        [HttpGet]
        [Route("api/reports/{id}/{branch}/sale-employees")]
        public IHttpActionResult reportSaleEmployees(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from em in db.Employees
                          join od in db.Orders on em.EmployeeId equals od.EmployeeId
                          where od.ShopId == id && od.BranchId == branch && od.Type == 0
                          && od.OrderStatus != (int)Const.OrderStatus.CANCELED && od.OrderStatus != (int)Const.OrderStatus.DELETED
                          orderby od.EmployeeId descending
                          select new SaleByTime
                          {
                              OrderId=od.OrderId,
                              EmployeeId= od.EmployeeId,
                              EmployeeName = od.Employee.EmployeeName,
                              CreatedAt = od.CreatedAt,
                              TotalPriceSale = od.TotalPrice,
                              TotalPriceReturn = 0
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.Orders.Where(e => e.RelatedOrderId == item.OrderId).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }
            }

            var returnData2 = (from em in returnData
                          orderby em.EmployeeId descending
                          group em by new
                          {
                              em.EmployeeId
                          } into god
                          select new
                          {
                              god.Key.EmployeeId,
                              EmployeeName = god.FirstOrDefault().EmployeeName,
                              TotalPriceSale = god.Sum(p => p.TotalPriceSale),
                              TotalPriceReturn = god.Sum(p => p.TotalPriceReturn)
                          }).AsEnumerable();

            int count = returnData2.Count();
            decimal totalprice = returnData2.Sum(e => e.TotalPriceSale).Value;
            decimal totalpricereturn = returnData2.Sum(e => e.TotalPriceReturn).Value;

            returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            string branchName = db.Branches.Find(branch).BranchName;

            if (returnData2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = returnData2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataSale(count, totalprice, totalpricereturn);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        #endregion

        #region Báo cáo đặt hàng
        //Báo cáo đặt hàng theo hàng hóa
        [HttpGet]
        [Route("api/reports/{id}/{branch}/order-product")]
        public IHttpActionResult reportOrderProduct(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            DateTimeReport dtp = new DateTimeReport();
            dtp = getStartAndEndDate(paging.query);

            //Lấy kết quả
            var result = from pd in db.Products
                         join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                         join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                         where pd.ShopId == id && op.CreatedAt >= dtp.startDate && op.CreatedAt <= dtp.endDate
                         orderby pa.ProductCode descending
                         group op by new
                         {
                             pa.ProductCode,
                             pa.SubName,
                             pd.ProductName
                         } into gop
                         select new
                         {
                             gop.Key.ProductCode,
                             ProductFullName = gop.FirstOrDefault().ProductAttribute.Product.ProductName + gop.FirstOrDefault().ProductAttribute.SubName,
                             Quantity = gop.Sum(p => p.Quantity),
                             TotalPrice = gop.Sum(p => p.TotalPrice)
                         };

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result.Count());
            def.metadatainfo = new MetadataInfo(dtp.startDate, dtp.endDate, "");
            return Ok(def);
        }

        //Biều đồ top 10 hàng hóa được đặt nhiều nhất
        [HttpGet]
        [Route("api/reports/{id}/order-product-chart")]
        public IHttpActionResult reportOrderProductChart(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            DateTimeReport dtp = new DateTimeReport();
            dtp = getStartAndEndDate(paging.query);

            //Lấy kết quả
            var result = from pd in db.Products
                         join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                         join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                         where pd.ShopId == id && op.CreatedAt >= dtp.startDate && op.CreatedAt <= dtp.endDate
                         orderby op.Quantity descending
                         group op by new
                         {
                             pa.ProductCode,
                             pa.SubName,
                             pd.ProductName
                         } into gop
                         select new
                         {
                             gop.Key.ProductCode,
                             ProductFullName = gop.FirstOrDefault().ProductAttribute.Product.ProductName + gop.FirstOrDefault().ProductAttribute.SubName,
                             Quantity = gop.Sum(p => p.Quantity),
                             TotalPrice = gop.Sum(p => p.TotalPrice)
                         };

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.Take(10).OrderByDescending(e => e.Quantity).ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result.Count());
            def.metadatainfo = new MetadataInfo(dtp.startDate, dtp.endDate, "");
            return Ok(def);
        }

        //Báo cáo danh sách đơn đặt hàng
        [HttpGet]
        [Route("api/reports/{id}/{branch}/order-exchange")]
        public IHttpActionResult reportOrderExchange(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            DateTimeReport dtp = new DateTimeReport();
            dtp = getStartAndEndDate(paging.query);

            //Lấy kết quả
            var result = from cus in db.Customers
                         join od in db.Orders on cus.CustomerId equals od.CustomerId
                         join op in db.OrderProducts on od.OrderId equals op.OrderId
                         where od.ShopId == id && op.CreatedAt >= dtp.startDate && op.CreatedAt <= dtp.endDate && od.OrderStatus != 3
                         orderby od.OrderCode descending
                         group op by new
                         {
                             od.OrderCode,
                             od.CreatedAt,
                             cus.CustomerName
                         } into gop
                         select new
                         {
                             gop.Key.OrderCode,
                             CreatedAt = gop.FirstOrDefault().CreatedAt,
                             CustomerName = gop.FirstOrDefault().Order.Customer.CustomerName,
                             Quantity = gop.Sum(p => p.Quantity),
                             TotalPrice = gop.Sum(p => p.TotalPrice)
                         };

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result.Count());
            def.metadatainfo = new MetadataInfo(dtp.startDate, dtp.endDate, "");
            return Ok(def);
        }

        //Biều đồ Top 10 khách hàng đặt hàng nhiều nhất
        [HttpGet]
        [Route("api/reports/{id}/order-exchange-chart")]
        public IHttpActionResult reportOrderExchangeChart(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            DateTimeReport dtp = new DateTimeReport();
            dtp = getStartAndEndDate(paging.query);

            //Lấy kết quả
            var result = from cus in db.Customers
                         join od in db.Orders on cus.CustomerId equals od.CustomerId
                         join op in db.OrderProducts on od.OrderId equals op.OrderId
                         where od.ShopId == id && op.CreatedAt >= dtp.startDate && op.CreatedAt <= dtp.endDate && od.OrderStatus != 3
                         orderby od.OrderCode descending
                         group op by new
                         {
                             od.OrderCode,
                             od.CreatedAt,
                             cus.CustomerName
                         } into gop
                         select new
                         {
                             gop.Key.OrderCode,
                             CreatedAt = gop.FirstOrDefault().CreatedAt,
                             CustomerName = gop.FirstOrDefault().Order.Customer.CustomerName,
                             Quantity = gop.Sum(p => p.Quantity),
                             TotalPrice = gop.Sum(p => p.TotalPrice)
                         };

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.Take(10).OrderByDescending(e => e.Quantity).ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result.Count());
            def.metadatainfo = new MetadataInfo(dtp.startDate, dtp.endDate, "");
            return Ok(def);
        }
        #endregion

        #region Báo cáo hàng hóa
        //Báo cáo bán hàng theo hàng hóa
        [HttpGet]
        [Route("api/reports/{id}/product-sale")]
        public IHttpActionResult reportProductSale(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.Type == (int)Const.OrderType.SALE 
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              BranchId=op.Order.BranchId,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              QuantitySale = op.Quantity,
                              TotalPriceSale = op.TotalPrice,
                              QuantityReturn = 0,
                              TotalPriceReturn = 0
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.OrderProducts.Where(e => e.Order.RelatedOrderId == item.OrderId && e.Order.OrderStatus != (int)Const.OrderStatus.DELETED && e.Order.OrderStatus != (int)Const.OrderStatus.CANCELED).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.QuantityReturn += od.Quantity;
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }
            }

            var result2 = (from op in returnData
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductCode = god.Key.ProductCode,
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               QuantitySale = god.Sum(p => p.QuantitySale),
                               TotalPriceSale = god.Sum(p => p.TotalPriceSale),
                               QuantityReturn = god.Sum(p => p.QuantityReturn),
                               TotalPriceReturn = god.Sum(p => p.TotalPriceReturn)
                           }).AsEnumerable();

            int count = result2.Count();
            decimal totalsale = result2.Sum(e => e.QuantitySale).Value;
            decimal totalpricesale = result2.Sum(e => e.TotalPriceSale).Value;
            decimal totalreturn = result2.Sum(e => e.QuantityReturn).Value;
            decimal totalpricereturn = result2.Sum(e => e.TotalPriceReturn).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataDateProduct(count, totalsale, totalpricesale, totalreturn, totalpricereturn);
            return Ok(def);
        }

        //Biều đồ Top 10 sản phẩm có doanh số cao nhất - trả hàng
        [HttpGet]
        [Route("api/reports/{id}/product-sale-chart1")]
        public IHttpActionResult reportProductSaleChart1(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              BranchId=od.BranchId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              TotalPriceSale = op.TotalPrice,
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 = (from op in result
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               TotalPrice = god.Sum(p => p.TotalPriceSale),
                           }).AsEnumerable();

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.Take(10).OrderByDescending(e=>e.TotalPrice).ToList();
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        //Biểu đồ Top 10 hàng hóa bán chạy theo số lượng -trả hàng
        [HttpGet]
        [Route("api/reports/{id}/product-sale-chart2")]
        public IHttpActionResult reportProductSaleChart2(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              BranchId=od.BranchId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              QuantitySale = op.Quantity,
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 = (from op in result
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               Quantity = god.Sum(p => p.QuantitySale),
                           }).AsEnumerable();

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.Take(10).OrderByDescending(e => e.Quantity).ToList();
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        //Báo cáo lợi nhuận theo hàng hóa
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-profit")]
        public IHttpActionResult reportProductProfit(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              ProductAttributeId=op.ProductAttributeId,
                              ProductUnitId=op.ProductUnitId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              QuantitySale = op.Quantity,
                              TotalPriceSale = op.TotalPrice,
                              QuantityReturn = 0,
                              TotalPriceReturn = 0,
                              TotalOriginPrice=0
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.OrderProducts.Where(e => e.Order.RelatedOrderId == item.OrderId && e.Order.OrderStatus != (int)Const.OrderStatus.DELETED && e.Order.OrderStatus != (int)Const.OrderStatus.CANCELED).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.QuantityReturn += od.Quantity;
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }

                var priceOrigin = db.ProductPrices.Where(e => e.ProductAttributeId == item.ProductAttributeId && e.ProductUnitId == item.ProductUnitId).ToList();
                if (priceOrigin != null && priceOrigin.Count > 0)
                {
                    item.TotalOriginPrice += (priceOrigin.FirstOrDefault().OriginPrice * item.QuantitySale);
                }
            }

            var result2 = (from op in returnData
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductCode = god.Key.ProductCode,
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               QuantitySale = god.Sum(p => p.QuantitySale),
                               TotalPriceSale = god.Sum(p => p.TotalPriceSale),
                               QuantityReturn = god.Sum(p => p.QuantityReturn),
                               TotalPriceReturn = god.Sum(p => p.TotalPriceReturn),
                               TotalOriginPrice = god.Sum(p => p.TotalOriginPrice),
                           }).AsEnumerable();

            int count = result2.Count();
            decimal totalsale = result2.Sum(e => e.QuantitySale).Value;
            decimal totalpricesale = result2.Sum(e => e.TotalPriceSale).Value;
            decimal totalreturn = result2.Sum(e => e.QuantityReturn).Value;
            decimal totalpricereturn = result2.Sum(e => e.TotalPriceReturn).Value;
            decimal totaloriginprice = result2.Sum(e => e.TotalOriginPrice).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            string branchName = db.Branches.Find(branch).BranchName;

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataDateProduct(count, totalsale, totalpricesale, totalreturn, totalpricereturn, totaloriginprice);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Biểu đồ top 10 sản phẩm có lợi nhuận cao nhất
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-profit-chart1")]
        public IHttpActionResult reportProductProfitChart1(int id, int branch,[FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              ProductAttributeId = op.ProductAttributeId,
                              ProductUnitId = op.ProductUnitId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              QuantitySale = op.Quantity,
                              TotalPriceSale = op.TotalPrice,
                              QuantityReturn = 0,
                              TotalPriceReturn = 0,
                              TotalOriginPrice = 0,
                              TotalProfit=0
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.OrderProducts.Where(e => e.Order.RelatedOrderId == item.OrderId && e.Order.OrderStatus != (int)Const.OrderStatus.DELETED && e.Order.OrderStatus != (int)Const.OrderStatus.CANCELED).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.QuantityReturn += od.Quantity;
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }

                var priceOrigin = db.ProductPrices.Where(e => e.ProductAttributeId == item.ProductAttributeId && e.ProductUnitId == item.ProductUnitId).ToList();
                if (priceOrigin != null && priceOrigin.Count > 0)
                {
                    item.TotalOriginPrice += (priceOrigin.FirstOrDefault().OriginPrice * item.QuantitySale);
                }
                item.TotalProfit = item.TotalPriceSale - item.TotalPriceReturn - item.TotalOriginPrice;
            }

            var result2 = (from op in returnData
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               TotalProfit = god.Sum(p=>p.TotalProfit),
                           }).AsEnumerable();
            

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.Take(10).OrderByDescending(e => e.TotalProfit).ToList();
            def.metadata = new Metadata(result2.Count());
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        //Biểu đồ top 10 sản phẩm có tỷ xuất cao nhất
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-profit-chart2")]
        public IHttpActionResult reportProductProfitChart2(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                          select new SaleByProduct
                          {
                              OrderId = od.OrderId,
                              ProductAttributeId = op.ProductAttributeId,
                              ProductUnitId = op.ProductUnitId,
                              ProductCode = pa.ProductCode,
                              CreatedAt = op.CreatedAt,
                              ProductFullName = pa.Product.ProductName + " " + pa.SubName,
                              QuantitySale = op.Quantity,
                              TotalPriceSale = op.TotalPrice,
                              QuantityReturn = 0,
                              TotalPriceReturn = 0,
                              TotalOriginPrice = 0,
                              TotalProfit = 0,                              
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var returnData = result.ToList();
            foreach (var item in returnData)
            {
                var odReturn = db.OrderProducts.Where(e => e.Order.RelatedOrderId == item.OrderId && e.Order.OrderStatus != (int)Const.OrderStatus.DELETED && e.Order.OrderStatus != (int)Const.OrderStatus.CANCELED).ToList();
                if (odReturn != null && odReturn.Count > 0)
                {
                    foreach (var od in odReturn)
                    {
                        item.QuantityReturn += od.Quantity;
                        item.TotalPriceReturn += od.TotalPrice;
                    }
                }

                var priceOrigin = db.ProductPrices.Where(e => e.ProductAttributeId == item.ProductAttributeId && e.ProductUnitId == item.ProductUnitId).ToList();
                if (priceOrigin != null && priceOrigin.Count > 0)
                {
                    item.TotalOriginPrice += (priceOrigin.FirstOrDefault().OriginPrice * item.QuantitySale);
                }
                item.TotalProfit = item.TotalPriceSale - item.TotalPriceReturn - item.TotalOriginPrice;
            }

            var result2 = (from op in returnData
                           group op by new
                           {
                               op.ProductCode
                           } into god
                           select new
                           {
                               ProductFullName = god.FirstOrDefault().ProductFullName,
                               Rates = (god.Sum(p => p.TotalPriceSale) - god.Sum(p => p.TotalPriceReturn) - god.Sum(p => p.TotalOriginPrice)) * 100 / god.Sum(p => p.TotalPriceSale)
                           }).AsEnumerable();


            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.Take(10).OrderByDescending(e => e.Rates).ToList();
            def.metadata = new Metadata(result2.Count());
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        //Báo cáo hàng hóa xuất nhập tồn
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-exims")]
        public IHttpActionResult reportProductExim(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            string[] date = paging.query.Split('(');
            string[] dateS = date[1].Split(')')[0].Split(',');
            string[] dateE = date[2].Split(')')[0].Split(',');
            var dateStart = new DateTime(int.Parse(dateS[0]), int.Parse(dateS[1]), int.Parse(dateS[2]), int.Parse(dateS[3]), int.Parse(dateS[4]), int.Parse(dateS[5]));
            var dateEnd = new DateTime(int.Parse(dateE[0]), int.Parse(dateE[1]), int.Parse(dateE[2]), int.Parse(dateE[3]), int.Parse(dateE[4]), int.Parse(dateE[5]));

            var resultInportStart =
                             (from pd in db.Products
                              join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                              join ep in db.EximProducts on pa.ProductAttributeId equals ep.ProductAttributeId
                              orderby pa.ProductCode descending
                              where pd.ShopId == id && ep.Exim.BranchId==branch && ep.PreviousStock < ep.LastStock && ep.CreatedAt < dateStart
                              group ep by new
                              {
                                  pa.ProductCode,
                                  pd.ProductName,
                                  pa.SubName
                              } into god
                              select new EximByProduct
                              {
                                  ProductCode = god.Key.ProductCode,
                                  ProductAttributeId = god.FirstOrDefault().ProductAttributeId,
                                  ProductUnitId = god.FirstOrDefault().ProductUnitId,
                                  ProductFullName = god.FirstOrDefault().ProductAttribute.Product.ProductName + " " + god.FirstOrDefault().ProductAttribute.SubName,
                                  QuantityInportStart = god.Sum(p => p.Quantity),
                                  TotalPriceInportStart = 0,
                                  QuantityExportStart = 0,
                                  TotalPriceExportStart= 0,
                                  QuantityInport = 0,
                                  TotalPriceInport = 0,
                                  QuantityExport = 0,
                                  TotalPriceExport = 0,
                              }).AsEnumerable();

            var resultExportStart =
                             (from pd in db.Products
                              join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                              join ep in db.EximProducts on pa.ProductAttributeId equals ep.ProductAttributeId
                              orderby pa.ProductCode descending
                              where pd.ShopId == id && ep.Exim.BranchId == branch && ep.PreviousStock > ep.LastStock && ep.CreatedAt < dateStart
                              group ep by new
                              {
                                  pa.ProductCode,
                                  pd.ProductName,
                                  pa.SubName
                              } into god
                              select new EximByProduct
                              {
                                  ProductCode = god.Key.ProductCode,
                                  ProductAttributeId = god.FirstOrDefault().ProductAttributeId,
                                  ProductUnitId = god.FirstOrDefault().ProductUnitId,
                                  ProductFullName = god.FirstOrDefault().ProductAttribute.Product.ProductName + " " + god.FirstOrDefault().ProductAttribute.SubName,
                                  QuantityInportStart = 0,
                                  TotalPriceInportStart = 0,
                                  QuantityExportStart = god.Sum(p => p.Quantity),
                                  TotalPriceExportStart = 0,
                                  QuantityInport = 0,
                                  TotalPriceInport = 0,
                                  QuantityExport = 0,
                                  TotalPriceExport = 0                           
                              }).AsEnumerable();

            var resultImport =
                             (from pd in db.Products
                              join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                              join ep in db.EximProducts on pa.ProductAttributeId equals ep.ProductAttributeId
                              orderby pa.ProductCode descending
                              where pd.ShopId == id && ep.Exim.BranchId == branch && ep.Exim.Type==(int)Const.EximType.IMPORT && ep.CreatedAt >= dateStart && ep.CreatedAt <= dateEnd
                              group ep by new
                              {
                                  pa.ProductCode,
                                  pd.ProductName,
                                  pa.SubName
                              } into god
                              select new EximByProduct
                              {
                                  ProductCode = god.Key.ProductCode,
                                  ProductAttributeId = god.FirstOrDefault().ProductAttributeId,
                                  ProductUnitId = god.FirstOrDefault().ProductUnitId,
                                  ProductFullName = god.FirstOrDefault().ProductAttribute.Product.ProductName + " " + god.FirstOrDefault().ProductAttribute.SubName,
                                  QuantityInportStart = 0,
                                  TotalPriceInportStart = 0,
                                  QuantityExportStart = 0,
                                  TotalPriceExportStart = 0,
                                  QuantityInport = god.Sum(p => p.Quantity),
                                  TotalPriceInport = 0,
                                  QuantityExport = 0,
                                  TotalPriceExport = 0,                               
                              }).AsEnumerable();

            var resultExport =
                             (from pd in db.Products
                              join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                              join ep in db.EximProducts on pa.ProductAttributeId equals ep.ProductAttributeId
                              orderby pa.ProductCode descending
                              where pd.ShopId == id && ep.Exim.BranchId == branch && ep.Exim.Type == (int)Const.EximType.EXPORT && ep.CreatedAt >= dateStart && ep.CreatedAt <= dateEnd
                              group ep by new
                              {
                                  pa.ProductCode,
                                  pd.ProductName,
                                  pa.SubName
                              } into god
                              select new EximByProduct
                              {
                                  ProductCode = god.Key.ProductCode,
                                  ProductAttributeId = god.FirstOrDefault().ProductAttributeId,
                                  ProductUnitId = god.FirstOrDefault().ProductUnitId,
                                  ProductFullName = god.FirstOrDefault().ProductAttribute.Product.ProductName + " " + god.FirstOrDefault().ProductAttribute.SubName,
                                  QuantityInportStart = 0,
                                  TotalPriceInportStart = 0,
                                  QuantityExportStart = 0,
                                  TotalPriceExportStart = 0,
                                  QuantityInport = 0,
                                  TotalPriceInport = 0,
                                  QuantityExport = god.Sum(p => p.Quantity),
                                  TotalPriceExport = 0,
                              }).AsEnumerable();

            List<EximByProduct> returnData = new List<EximByProduct>();

            var dataIS = resultInportStart.ToList();
            foreach (var item in dataIS)
            {
                EximByProduct data = new EximByProduct();
                decimal price = 0;
                var importPrice = (from ep in db.EximProducts
                                   where ep.ProductAttributeId == item.ProductAttributeId && ep.ProductUnitId == item.ProductUnitId
                                   && ep.Exim.Type == (int)Const.EximType.IMPORT && ep.Exim.TargetType == "VENDOR"
                                   && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                   orderby ep.CreatedAt descending
                                   select new
                                   {
                                       Price = ep.Price,
                                       Quantity = ep.Quantity
                                   }).AsEnumerable();
                importPrice = importPrice.ToList();
                if ((importPrice == null || importPrice.Count() == 0))
                {
                    price = 0;
                }
                else
                {
                    double sumImportPrice = 0;
                    int countImportPrice = 0;
                    foreach (var itemIP in importPrice)
                    {
                        countImportPrice += (int)itemIP.Quantity;
                        sumImportPrice += (double)(itemIP.Price * itemIP.Quantity);
                    }
                    price = (decimal)(sumImportPrice / countImportPrice);
                }

                data.ProductAttributeId = item.ProductAttributeId;
                data.ProductCode = item.ProductCode;
                data.ProductFullName = item.ProductFullName;
                data.ProductUnitId = item.ProductUnitId;
                data.QuantityInportStart = item.QuantityInportStart;
                data.TotalPriceInportStart = (item.QuantityInportStart * price);
                var itemIM = resultImport.Where(e => e.ProductCode == item.ProductCode).ToList();
                foreach (var itemI in itemIM)
                {
                    item.QuantityInport += itemI.QuantityInport;
                }
                data.QuantityInport = item.QuantityInport;
                data.TotalPriceInport = (item.QuantityInport * price);

                var itemEX = resultExport.Where(e => e.ProductCode == item.ProductCode).ToList();
                foreach (var itemE in itemEX)
                {
                    item.QuantityExport += itemE.QuantityExport;
                }
                data.QuantityExport = item.QuantityExport;
                data.TotalPriceExport = (item.QuantityExport * price);

                var itemEPE = resultExportStart.Where(e => e.ProductCode == item.ProductCode).ToList();
                foreach (var itemEE in itemEPE)
                {
                    item.QuantityExportStart += itemEE.QuantityExportStart;
                }
                data.QuantityExportStart = item.QuantityExportStart;
                data.TotalPriceExportStart = (item.QuantityExportStart * price);

                returnData.Add(data);
            }

            var dataI = resultImport.ToList();
            foreach (var item in dataI)
            {
                string pCode=item.ProductCode.Trim();
                if (returnData.Where(e => e.ProductCode.Trim().Equals(pCode)).ToList().Count <= 0)
                {
                    EximByProduct data = new EximByProduct();
                    decimal price = 0;
                    var importPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == item.ProductAttributeId && ep.ProductUnitId == item.ProductUnitId
                                       && ep.Exim.Type == (int)Const.EximType.IMPORT && ep.Exim.TargetType == "VENDOR"
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();
                    importPrice = importPrice.ToList();
                    if ((importPrice == null || importPrice.Count() == 0))
                    {
                        price = 0;
                    }
                    else
                    {
                        double sumImportPrice = 0;
                        int countImportPrice = 0;
                        foreach (var itemIP in importPrice)
                        {
                            countImportPrice += (int)itemIP.Quantity;
                            sumImportPrice += (double)(itemIP.Price * itemIP.Quantity);
                        }
                        price = (decimal)(sumImportPrice / countImportPrice);
                    }

                    data.ProductAttributeId = item.ProductAttributeId;
                    data.ProductCode = item.ProductCode;
                    data.ProductFullName = item.ProductFullName;
                    data.ProductUnitId = item.ProductUnitId;
                    data.QuantityInport = item.QuantityInport;
                    data.TotalPriceInport = (item.QuantityInport * price);

                    var itemIM = resultInportStart.Where(e => e.ProductCode == item.ProductCode).ToList();
                    foreach (var itemI in itemIM)
                    {
                        item.QuantityInportStart += itemI.QuantityInportStart;
                    }
                    data.QuantityInportStart = item.QuantityInportStart;
                    data.TotalPriceInportStart = (item.QuantityInportStart * price);

                    var itemEX = resultExport.Where(e => e.ProductCode == item.ProductCode).ToList();
                    foreach (var itemE in itemEX)
                    {
                        item.QuantityExport += itemE.QuantityExport;
                    }
                    data.QuantityExport = item.QuantityExport;
                    data.TotalPriceExport = (item.QuantityExport * price);

                    var itemEPE = resultExportStart.Where(e => e.ProductCode == item.ProductCode).ToList();
                    foreach (var itemEE in itemEPE)
                    {
                        item.QuantityExportStart += itemEE.QuantityExportStart;
                    }
                    data.QuantityExportStart = item.QuantityExportStart;
                    data.TotalPriceExportStart = (item.QuantityExportStart * price);

                    returnData.Add(data);
                }
            }

            var dataE = resultExport.ToList();
            foreach (var item in dataE)
            {
                string pCode = item.ProductCode.Trim();
                if (returnData.Where(e => e.ProductCode.Trim().Equals(pCode)).ToList().Count <= 0)
                {
                    EximByProduct data = new EximByProduct();
                    decimal price = 0;
                    var importPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == item.ProductAttributeId && ep.ProductUnitId == item.ProductUnitId
                                       && ep.Exim.Type == (int)Const.EximType.IMPORT && ep.Exim.TargetType == "VENDOR"
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();
                    importPrice = importPrice.ToList();
                    if ((importPrice == null || importPrice.Count() == 0))
                    {
                        price = 0;
                    }
                    else
                    {
                        double sumImportPrice = 0;
                        int countImportPrice = 0;
                        foreach (var itemIP in importPrice)
                        {
                            countImportPrice += (int)itemIP.Quantity;
                            sumImportPrice += (double)(itemIP.Price * itemIP.Quantity);
                        }
                        price = (decimal)(sumImportPrice / countImportPrice);
                    }

                    data.ProductAttributeId = item.ProductAttributeId;
                    data.ProductCode = item.ProductCode;
                    data.ProductFullName = item.ProductFullName;
                    data.ProductUnitId = item.ProductUnitId;
                    data.QuantityExport = item.QuantityExport;
                    data.TotalPriceExport = (item.QuantityExport * price);

                    var itemIM = resultInportStart.Where(e => e.ProductCode == item.ProductCode).ToList();
                    foreach (var itemI in itemIM)
                    {
                        item.QuantityInportStart += itemI.QuantityInportStart;
                    }
                    data.QuantityInportStart = item.QuantityInportStart;
                    data.TotalPriceInportStart = (item.QuantityInportStart * price);

                    var itemEX = resultImport.Where(e => e.ProductCode == item.ProductCode).ToList();
                    foreach (var itemE in itemEX)
                    {
                        item.QuantityInport += itemE.QuantityInport;
                    }
                    data.QuantityInport = item.QuantityInport;
                    data.TotalPriceInport = (item.QuantityInport * price);

                    var itemEPE = resultExportStart.Where(e => e.ProductCode == item.ProductCode).ToList();
                    foreach (var itemEE in itemEPE)
                    {
                        item.QuantityExportStart += itemEE.QuantityExportStart;
                    }
                    data.QuantityExportStart = item.QuantityExportStart;
                    data.TotalPriceExportStart = (item.QuantityExportStart * price);

                    returnData.Add(data);
                }
            }

            var result = returnData.Where(e => e.QuantityInport > 0 || e.QuantityExport > 0).AsEnumerable();


            int count = result.Count();        

            decimal totalstart = result.Sum(e => e.QuantityInportStart).Value - result.Sum(e => e.QuantityExportStart).Value;
            decimal totalpricestart = result.Sum(e => e.TotalPriceInportStart).Value - result.Sum(e => e.TotalPriceExportStart).Value;
            decimal totalinport = result.Sum(e => e.QuantityInport).Value;
            decimal totalpriceinport = result.Sum(e => e.TotalPriceInport).Value;
            decimal totalexport = result.Sum(e => e.QuantityExport).Value;
            decimal totalpriceexport = result.Sum(e => e.TotalPriceExport).Value;
            decimal totalend = result.Sum(e => e.QuantityInportStart).Value - result.Sum(e => e.QuantityExportStart).Value + result.Sum(e => e.QuantityInport).Value - result.Sum(e => e.QuantityExport).Value;
            decimal totalpricend = result.Sum(e => e.TotalPriceInportStart).Value - result.Sum(e => e.TotalPriceExportStart).Value + result.Sum(e => e.TotalPriceInport).Value - result.Sum(e => e.TotalPriceExport).Value;

            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            string branchName = db.Branches.Find(branch).BranchName;

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataExim(count, totalstart, totalpricestart, totalinport, totalpriceinport, 
                totalexport, totalpriceexport, totalend, totalpricend);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Báo cáo danh sách nhân viên theo bán hàng
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-employees")]
        public IHttpActionResult reportProductEmployees(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result =
                              (from pd in db.Products
                               join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                               join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                               join od in db.Orders on op.OrderId equals od.OrderId
                               where pd.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                               select new
                               {
                                   ProductCode = pa.ProductCode,
                                   CreatedAt = op.CreatedAt,
                                   ProductFullName = pd.ProductName + " " + pa.SubName,
                                   SaleEmployeeName = od.SaleEmployee.EmployeeName,
                                   Quantity = op.Quantity,
                                   TotalPrice = op.TotalPrice
                               }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 =
                              (from op in result
                               group op by new
                               {
                                   op.ProductCode,
                                   op.ProductFullName,
                                   op.SaleEmployeeName
                               } into god
                               select new
                               {
                                   god.Key.ProductCode,
                                   ProductFullName = god.FirstOrDefault().ProductFullName,
                                   SaleEmployeeName = god.FirstOrDefault().SaleEmployeeName,
                                   QuantitySale = god.Sum(p => p.Quantity),
                                   TotalPriceSale = god.Sum(p => p.TotalPrice)
                               }).AsEnumerable();

            int count = result2.Count();
            decimal totalproduct = result2.Sum(e => e.QuantitySale).Value;
            decimal totalprice = result2.Sum(e => e.TotalPriceSale).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            string branchName = db.Branches.Find(branch).BranchName;

            def.data = result2.OrderByDescending(e => e.TotalPriceSale).ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataPartner(count, totalproduct, totalprice);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Báo cáo danh sách khách hàng theo hàng bán
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-customer")]
        public IHttpActionResult reportProductCustomer(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result =
                               (from pd in db.Products
                                join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                                join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                                join od in db.Orders on op.OrderId equals od.OrderId
                                where pd.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          && pa.Status != (int)Const.ProductStatus.DELETED
                                select new
                                {
                                    ProductCode = pa.ProductCode,
                                    CreatedAt = op.CreatedAt,
                                    ProductFullName = pd.ProductName + " " + pa.SubName,
                                    CustomerName = od.Customer.CustomerName,
                                    Quantity = op.Quantity,
                                    TotalPrice = op.TotalPrice
                                }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 =
                              (from op in result
                               group op by new
                               {
                                   op.ProductCode,
                                   op.ProductFullName,
                                   op.CustomerName
                               } into god
                               select new
                               {
                                   god.Key.ProductCode,
                                   ProductFullName = god.FirstOrDefault().ProductFullName,
                                   CustomerName = god.FirstOrDefault().CustomerName,
                                   QuantityBuy = god.Sum(p => p.Quantity),
                                   TotalPriceBuy = god.Sum(p => p.TotalPrice)
                               }).AsEnumerable();

            int count = result2.Count();
            decimal totalproduct = result2.Sum(e => e.QuantityBuy).Value;
            decimal totalprice = result2.Sum(e => e.TotalPriceBuy).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            string branchName = db.Branches.Find(branch).BranchName;

            def.data = result2.OrderByDescending(e => e.TotalPriceBuy).ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataPartner(count, totalproduct, totalprice);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        //Báo cáo danh sách nhà cung cấp theo hàng bán
        [HttpGet]
        [Route("api/reports/{id}/{branch}/product-vendor")]
        public IHttpActionResult reportProductVendor(int id, int branch, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result =
                               (from pd in db.Products
                                join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                                join ep in db.EximProducts on pa.ProductAttributeId equals ep.ProductAttributeId
                                join ex in db.Exims on ep.EximId equals ex.EximId
                                join vd in db.Vendors on ex.TargetId equals vd.VendorId
                                where ex.ShopId == id && ex.BranchId == branch && ex.TargetType == "VENDOR"
                                && pa.Status != (int)Const.ProductStatus.DELETED
                                select new
                                {
                                    ProductCode = pa.ProductCode,
                                    CreatedAt = ep.CreatedAt,
                                    ProductFullName = pd.ProductName + " " + pa.SubName,
                                    VendorName = vd.VendorName,
                                    Quantity = ep.Quantity,
                                    TotalPrice = ep.Price
                                }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 =
                              (from ep in result
                               group ep by new
                               {
                                   ep.ProductCode,
                                   ep.ProductFullName,
                                   ep.VendorName
                               } into god
                               select new
                               {
                                   god.Key.ProductCode,
                                   ProductFullName = god.FirstOrDefault().ProductFullName,
                                   VendorName = god.FirstOrDefault().VendorName,
                                   QuantityProduct = god.Sum(p => p.Quantity),
                                   TotalPrice = god.Sum(p => p.TotalPrice)
                               }).AsEnumerable();
            //var returnData

            //var result3 =
            //                  (from ep in result2
            //                   group ep by new
            //                   {
            //                       ep.ProductCode,
            //                       ep.ProductFullName
            //                   } into god
            //                   select new
            //                   {
            //                       god.Key.ProductCode,
            //                       ProductFullName = god.FirstOrDefault().ProductFullName,
            //                       QuantityVendor = god.Count(),
            //                       QuantityProduct = god.Sum(p => p.Quantity),
            //                       TotalPrice = god.Sum(p => p.TotalPrice)
            //                   }).AsEnumerable();

            int count = result2.Count();
            //decimal totalpartner = result2.Sum(e => e.QuantityVendor);
            decimal totalproduct = result2.Sum(e => e.QuantityProduct).Value;
            decimal totalprice = result2.Sum(e => e.TotalPrice).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            string branchName = db.Branches.Find(branch).BranchName;

            def.data = result2.OrderByDescending(e => e.TotalPrice).ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new MetadataPartner(count, totalproduct, totalprice);
            def.metadatainfo = new MetadataInfo(branchName);
            return Ok(def);
        }

        #endregion

        #region Báo cáo khách hàng
        //Báo cáo bán hàng theo khách hàng
        //[HttpGet]
        //[Route("api/reports/{id}/customer-sale")]
        //public IHttpActionResult reportCustomerSale(int id, [FromUri] FilteredPagination paging)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    var identity = (ClaimsIdentity)User.Identity;

        //    DateTimeReport dtp = new DateTimeReport();
        //    dtp = getStartAndEndDate(paging.query);

        //    var result = from tb in 
        //                 ((from cus in db.Customers
        //                  join od in db.Orders on cus.CustomerId equals od.CustomerId
        //                  where od.ShopId == id && od.OrderStatus != 3 && od.CreatedAt >= dtp.startDate && od.CreatedAt <= dtp.endDate
        //                  group od by new
        //                  {
        //                      cus.CustomerCode,
        //                      cus.CustomerName
        //                  } into god
        //                  select new
        //                  {
        //                      god.Key.CustomerCode,
        //                      CustomerName = god.FirstOrDefault().Customer.CustomerName,
        //                      TotalPrice = god.Sum(p => p.TotalPrice),
        //                      TotalPriceReturn = 0,
        //                  }).Union(
        //                      (from cus in db.Customers
        //                      join od in db.Orders on cus.CustomerId equals od.CustomerId
        //                      where od.ShopId == id && od.OrderStatus != 3 && od.CreatedAt >= dtp.startDate && od.CreatedAt <= dtp.endDate
        //                      group od by new
        //                      {
        //                          cus.CustomerCode,
        //                          cus.CustomerName
        //                      } into god
        //                      select new
        //                      {
        //                          god.Key.CustomerCode,
        //                          CustomerName = god.FirstOrDefault().Customer.CustomerName,
        //                          TotalPrice = 0,
        //                          TotalPriceReturn = god.Sum(p => p.TotalPrice),
        //                      })
        //                  ))
        //                 group tb by new
        //                 {
        //                     tb.CustomerCode,
        //                     tb.CustomerName
        //                 } into god
        //                 select new
        //                 {
        //                     god.Key.CustomerCode,
        //                     CustomerName = god.FirstOrDefault().CustomerName,
        //                     TotalPrice = god.Sum(p => p.TotalPrice),
        //                     TotalPriceReturn = god.Sum(p => p.TotalPriceReturn),
        //                 };

        //    if (result == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    def.data = result.ToList();
        //    def.metadata = new Metadata(result.Count());
        //    def.meta = new Meta(200, "Success");
        //    def.metadatadate = new MetadataDate(dtp.startDate, dtp.endDate);
        //    return Ok(def);
        //}

        //Biều đồ Top 10 sản phẩm có doanh số cao nhất - trả hàng
        [HttpGet]
        [Route("api/reports/{id}/customer-sale-chart")]
        public IHttpActionResult reportCustomerSaleChart1(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            DateTimeReport dtp = new DateTimeReport();
            dtp = getStartAndEndDate(paging.query);

            var result = from pd in db.Products
                         join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                         join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                         join od in db.Orders on op.OrderId equals od.OrderId
                         orderby pa.ProductCode descending
                         where pd.ShopId == id && od.OrderStatus != 3 && od.CreatedAt >= dtp.startDate && od.CreatedAt <= dtp.endDate
                         group op by new
                         {
                             pa.ProductCode,
                             pd.ProductName,
                             pa.SubName
                         } into god
                         select new
                         {
                             god.Key.ProductCode,
                             ProductFullName = god.FirstOrDefault().ProductAttribute.Product.ProductName + " " + god.FirstOrDefault().ProductAttribute.SubName,
                             TotalPrice = god.Sum(p => p.TotalPrice),
                         };

            if (result == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result.Take(10).OrderByDescending(e => e.TotalPrice).ToList();
            def.meta = new Meta(200, "Success");
            def.metadatainfo = new MetadataInfo(dtp.startDate, dtp.endDate, "");
            return Ok(def);
        }

        #endregion

        #region Báo cáo Tài chính

        //Biều đồ lợi nhuận theo tháng
        [HttpGet]
        [Route("api/reports/{id}/finance-month-chart")]
        public IHttpActionResult reportFinanceMonthChart(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = from od in db.Orders
                         where od.ShopId == id && od.Type == (int)Const.OrderType.SALE
                         && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                         orderby od.CreatedAt descending
                         select new
                         {
                             CreatedAt = od.CreatedAt,
                             TotalPrice = od.TotalPrice,
                             TotalOriginPrice = od.TotalOriginPrice

                         };

            var result2 = from od in result
                          where od.CreatedAt.HasValue
                          orderby od.CreatedAt.Value.Month descending
                          group od by new
                          {
                              od.CreatedAt.Value.Month,
                              od.CreatedAt.Value.Year
                          } into god
                          select new
                          {
                              Month = god.Key.Month,
                              Year = god.Key.Year,
                              TotalProfit = god.Sum(p => p.TotalPrice) - god.Sum(p => p.TotalOriginPrice)
                          };

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result2.Count());
            return Ok(def);
        }

        //Biều đồ lợi nhuận theo quý
        [HttpGet]
        [Route("api/reports/{id}/{branch}/finance-quarter-chart")]
        public IHttpActionResult reportFinanceQuarterChart(int id, int branch)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = from od in db.Orders
                         where od.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                          && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                         orderby od.CreatedAt descending
                         select new
                         {
                             CreatedAt = od.CreatedAt,
                             TotalPrice = od.TotalPrice,
                             TotalOriginPrice = od.TotalOriginPrice
                         };

            var today = DateTime.Now;
            List<FinanceByQuarter> fq = new List<FinanceByQuarter>();

            var datestart1 = new DateTime(today.Year, 1, 1, 00, 00, 00);
            var dateend1 = new DateTime(today.Year, 3, 31, 23, 59, 59);
            var result1 = (from od in result
                          where od.CreatedAt >= datestart1 && od.CreatedAt <= dateend1
                          group od by new
                          {
                              od.CreatedAt.Value.Year
                          } into god
                          select new
                          {
                              Quarter = 1,
                              TotalProfit = god.Sum(p => p.TotalPrice) - god.Sum(p => p.TotalOriginPrice)
                          }).AsEnumerable();

            if (result1 != null && result1.Count() > 0)
            {
                FinanceByQuarter fbq = new FinanceByQuarter();
                fbq.Quarter = result1.FirstOrDefault().Quarter;
                fbq.TotalProfit = result1.FirstOrDefault().TotalProfit;
                fq.Add(fbq);
            }

            var datestart2 = new DateTime(today.Year, 4, 1, 00, 00, 00);
            var dateend2 = new DateTime(today.Year, 6, 30, 23, 59, 59);
            var result2 = (from od in result
                          where od.CreatedAt >= datestart2 && od.CreatedAt <= dateend2
                          group od by new
                          {
                              od.CreatedAt.Value.Year
                          } into god
                          select new
                          {
                              Quarter = 2,
                              TotalProfit = god.Sum(p => p.TotalPrice) - god.Sum(p => p.TotalOriginPrice)
                          }).AsEnumerable();

            if (result2 != null && result2.Count() > 0)
            {
                FinanceByQuarter fbq = new FinanceByQuarter();
                fbq.Quarter = result2.FirstOrDefault().Quarter;
                fbq.TotalProfit = result2.FirstOrDefault().TotalProfit;
                fq.Add(fbq);
            }

            var datestart3 = new DateTime(today.Year, 7, 1, 00, 00, 00);
            var dateend3 = new DateTime(today.Year, 9, 30, 23, 59, 59);
            var result3 = (from od in result
                          where od.CreatedAt >= datestart3 && od.CreatedAt <= dateend3
                          group od by new
                          {
                              od.CreatedAt.Value.Year
                          } into god
                          select new
                          {
                              Quarter = 3,
                              TotalProfit = god.Sum(p => p.TotalPrice) - god.Sum(p => p.TotalOriginPrice)
                          }).AsEnumerable();
            if (result3 != null && result3.Count() > 0)
            {
                FinanceByQuarter fbq = new FinanceByQuarter();
                fbq.Quarter = result3.FirstOrDefault().Quarter;
                fbq.TotalProfit = result3.FirstOrDefault().TotalProfit;
                fq.Add(fbq);
            }

            var datestart4 = new DateTime(today.Year, 10, 1, 00, 00, 00);
            var dateend4 = new DateTime(today.Year, 12, 31, 23, 59, 59);
            var result4 = (from od in result
                          where od.CreatedAt >= datestart4 && od.CreatedAt <= dateend4
                          group od by new
                          {
                              od.CreatedAt.Value.Year
                          } into god
                          select new
                          {
                              Quarter = 4,
                              TotalProfit = god.Sum(p => p.TotalPrice) - god.Sum(p => p.TotalOriginPrice)
                          }).AsEnumerable();
            if (result4 != null && result4.Count() > 0)
            {
                FinanceByQuarter fbq = new FinanceByQuarter();
                fbq.Quarter = result4.FirstOrDefault().Quarter;
                fbq.TotalProfit = result4.FirstOrDefault().TotalProfit;
                fq.Add(fbq);
            }

            if (fq == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = fq.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(fq.Count());
            return Ok(def);
        }

        //Biều đồ lợi nhuận theo năm
        [HttpGet]
        [Route("api/reports/{id}/{branch}/finance-year-chart")]
        public IHttpActionResult reportFinanceYearChart(int id, int branch)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;

            var result = from od in db.Orders
                         where od.ShopId == id && od.BranchId == branch && od.Type == (int)Const.OrderType.SALE
                         && od.OrderStatus != (int)Const.OrderStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                         orderby od.CreatedAt descending
                         select new
                         {
                             CreatedAt = od.CreatedAt,
                             TotalPrice = od.TotalPrice,
                             TotalOriginPrice = od.TotalOriginPrice

                         };

            var result2 = from od in result
                          where od.CreatedAt.HasValue
                          orderby od.CreatedAt.Value.Month descending
                          group od by new
                          {
                              od.CreatedAt.Value.Year
                          } into god
                          select new
                          {
                              Year = god.Key.Year,
                              TotalProfit = god.Sum(p => p.TotalPrice) - god.Sum(p => p.TotalOriginPrice)
                          };

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.ToList();
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(result2.Count());
            return Ok(def);
        }


        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
