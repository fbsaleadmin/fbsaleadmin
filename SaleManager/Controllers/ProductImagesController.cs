﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ProductImagesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/ProductImages
        public IHttpActionResult GetProductImages()
        {
            DefaultResponse def = new DefaultResponse();
            def.meta = new Meta(200, "Success");
            def.data = db.ProductImages.Select(p => new ProductImageDTO() { 
                Alt = p.Alt,
                CreatedAt = p.CreatedAt,
                FileName = p.FileName,
                Path = p.Path,
                Priority = p.Priority,
                ProductId = p.ProductId,
                ProductImageId = p.ProductImageId,
                RelativePath = p.RelativePath
            });
            return Ok(def);
        }

        [Route("api/orders/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<ProductImage> productImages;
                if (paging.order_by != null)
                {
                    productImages = db.ProductImages.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    productImages = db.ProductImages.OrderBy("ProductImageId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                if (paging.query != null)
                    productImages = productImages.Where(HttpUtility.UrlDecode(paging.query));
                def.data = productImages.Select(p => new ProductImageDTO()
                {
                    Alt = p.Alt,
                    CreatedAt = p.CreatedAt,
                    FileName = p.FileName,
                    Path = p.Path,
                    Priority = p.Priority,
                    ProductId = p.ProductId,
                    ProductImageId = p.ProductImageId,
                    RelativePath = p.RelativePath
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/ProductImages/5      
        public IHttpActionResult GetProductImage(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductImageDTO productImage = db.ProductImages.Where(p => p.ProductImageId == id).Select(p => new ProductImageDTO()
            {
                Alt = p.Alt,
                CreatedAt = p.CreatedAt,
                FileName = p.FileName,
                Path = p.Path,
                Priority = p.Priority,
                ProductId = p.ProductId,
                ProductImageId = p.ProductImageId,
                RelativePath = p.RelativePath
            }).FirstOrDefault();
            if (productImage == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = productImage;
            return Ok(def);
        }

        // PUT: api/ProductImages/5
        public IHttpActionResult PutProductImage(int id, ProductImageDTO productImage)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != productImage.ProductImageId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            ProductImage current = db.ProductImages.Where(s => s.ProductImageId == id).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Alt = productImage.Alt;
                current.FileName = productImage.FileName;
                current.Path = productImage.Path;
                current.Priority = productImage.Priority;
                current.RelativePath = productImage.RelativePath;
                
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/ProductImages     
        public IHttpActionResult PostProductImage(ProductImageDTO productImage)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            ProductImage c = new ProductImage();
            c.Alt = productImage.Alt;
            c.CreatedAt = DateTime.Now;
            c.FileName = productImage.FileName;
            c.Path = productImage.Path;
            c.Priority = productImage.Priority;
            c.ProductId = productImage.ProductId;
            c.RelativePath = productImage.RelativePath;
       
            db.ProductImages.Add(c);
            db.SaveChanges();
            productImage.ProductImageId = c.ProductImageId;

            def.meta = new Meta(200, "Success");
            def.data = productImage;

            return Ok(def);
        }

        // DELETE: api/ProductImages/5        
        public IHttpActionResult DeleteProductImage(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductImage productImage = db.ProductImages.Find(id);
            if (productImage == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.ProductImages.Remove(productImage);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductImageExists(int id)
        {
            return db.ProductImages.Count(e => e.ProductImageId == id) > 0;
        }
    }
}