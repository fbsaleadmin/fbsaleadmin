﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class CustomerGroupsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/customergroups/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<CustomerGroup> customerGroups = db.CustomerGroups;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    customerGroups = customerGroups.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    customerGroups = customerGroups.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    customerGroups = customerGroups.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    customerGroups = customerGroups.OrderBy("CustomerGroupId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
              
                def.data = customerGroups.Select(c => new CustomerGroupDTO()
                {
                    CreateAt = c.CreateAt,
                    CustomerGroupId = c.CustomerGroupId,
                    Description = c.Description,
                    Name = c.Name,
                    Status = c.Status,
                    ShopId = c.ShopId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/CustomerGroups/5      
        public IHttpActionResult GetCustomerGroup(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            CustomerGroupDTO customerGroup = db.CustomerGroups.Where(a => a.CustomerGroupId == id).Select(c => new CustomerGroupDTO()
            {
                CreateAt = c.CreateAt,
                CustomerGroupId = c.CustomerGroupId,
                Description = c.Description,
                Name = c.Name,
                Status = c.Status,
                ShopId = c.ShopId
            }).FirstOrDefault();
            if (customerGroup == null || customerGroup.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = customerGroup;
            return Ok(def);
        }

        // PUT: api/CustomerGroups/5        
        public IHttpActionResult PutCustomerGroup(int id, CustomerGroupDTO customerGroup)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != customerGroup.CustomerGroupId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CustomerGroup current = db.CustomerGroups.Where(s => s.CustomerGroupId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Description = customerGroup.Description;
                current.Name = customerGroup.Name;
                current.Status = current.Status;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/CustomerGroups   
        public IHttpActionResult PostCustomerGroup(CustomerGroupDTO customerGroup)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(cl => cl.Type == "ShopId").Select(cl => cl.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            CustomerGroup c = new CustomerGroup();
            c.CreateAt = DateTime.Now;
            c.Description = customerGroup.Description;
            c.Name = customerGroup.Name;
            c.Status = customerGroup.Status;
            c.ShopId = ShopId;
            db.CustomerGroups.Add(c);
            db.SaveChanges();
            customerGroup.CustomerGroupId = c.CustomerGroupId;

            def.meta = new Meta(200, "Success");
            def.data = customerGroup;

            return Ok(def);
        }

        // DELETE: api/CustomerGroups/5        
        public IHttpActionResult DeleteCustomerGroup(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(cl => cl.Type == "ShopId").Select(cl => cl.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            CustomerGroup c = db.CustomerGroups.Find(id);
            if (c == null || c.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.CustomerGroups.Remove(c);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerGroupExists(int id)
        {
            return db.CustomerGroups.Count(e => e.CustomerGroupId == id) > 0;
        }
    }
}