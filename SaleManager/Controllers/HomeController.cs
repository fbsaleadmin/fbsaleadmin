﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Firebase;
using Firebase.Database;
using System.Threading.Tasks;

namespace SaleManager.Controllers
{
    public class HomeController : Controller
    {  
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            //get conversation
            DateTime d = Utils.UnixTimeStampMilisecondToDateTime(1496224295117);
            ViewBag.Title = d.ToString("ddMMyyyyHHmmssfff");
            return View();
        }
    }
}
