﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;
using log4net;

namespace SaleManager.Controllers
{
    
    public class EmployeesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly ILog log = LogMaster.GetLogger("employee", "employee");

        [Route("api/employees/login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody] LoginModel loginModel)
        {
            DefaultResponse def = new DefaultResponse();
            if (loginModel != null)
            {
                Meta meta;
                string email = loginModel.email;
                string password = loginModel.password.ToLower().Trim();        
                Data.EmployeeLogin employee = db.Employees.Where(e => e.Email == email && e.Password == password && e.Status != (int) Const.Status.DELETED).Select(e => new Data.EmployeeLogin()
                {
                    Address = e.Address,
                    BranchId = e.BranchId,
                    CreatedAt = e.CreatedAt,
                    Email = e.Email,
                    EmployeeId = e.EmployeeId,
                    EmployeeName = e.EmployeeName,
                    GroupId = e.GroupId,
                    Password = e.Password,
                    Phone = e.Phone,
                    ShopId = e.ShopId,
                    ShopName = e.Shop.ShopName,
                    Status = e.Status,
                    UpdatedAt = e.UpdatedAt,
                    UserId = e.UserId,
                    Group = e.Group,
                    TokenSince = e.TokenSince,
                    FacebookUserId = e.FacebookUserId
                }).FirstOrDefault();
               
                if (employee != null)             
                    meta = new Meta(200, "Success");              
                else
                    meta = new Meta(404, "Not Found");
                def.meta = meta;
                def.data = employee;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }
        
        [Route("api/employees/loginfacebook")]
        [HttpPost]
        public IHttpActionResult loginfacebook([FromBody] LoginFacebookModel loginModel)
        {
            DefaultResponse def = new DefaultResponse();
            if (loginModel != null)
            {                
                string id = loginModel.id;
                string email = loginModel.email;
                string token = loginModel.token;
                //check email token
                if (loginModel.id == null || loginModel.token == null)
                {
                    def.meta = new Meta(211, "Invalid parameter");
                    return Ok(def);
                }      
                //if email not valid
                if (loginModel.email == null || loginModel.email.Trim() == "")
                {
                    loginModel.email = loginModel.id + "@facebook.com";
                    email = loginModel.email;
                }                
                //check if admin user
                User user = db.Users.Where(u => u.Email == email && u.UserFacebookId == id).FirstOrDefault();
                if (user != null)
                {
                    if (user.IsThirdPartyLogin != null && (bool)user.IsThirdPartyLogin)
                    {
                        user.UserFacebookToken = loginModel.token;
                        user.Password = Utils.GetMD5Hash(user.UserId + Security.RandomString(7, false) + DateTime.Now.ToString("fff"));                        
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();         
                        int emp_id = user.Employees.FirstOrDefault().EmployeeId;
                        //Update password
                        Employee emp = db.Employees.Find(emp_id);
                        if (emp != null)
                        {
                            emp.Password = user.Password;                            
                            db.Entry(emp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        Data.EmployeeLogin employee = db.Employees.Where(e => e.EmployeeId == emp_id && e.Status != (int)Const.Status.DELETED).Select(e => new Data.EmployeeLogin()
                        {
                            Address = e.Address,
                            BranchId = e.BranchId,
                            CreatedAt = e.CreatedAt,
                            Email = e.Email,
                            EmployeeId = e.EmployeeId,
                            EmployeeName = e.EmployeeName,
                            GroupId = e.GroupId,
                            Password = e.Password,
                            Phone = e.Phone,
                            ShopId = e.ShopId,
                            ShopName = e.Shop.ShopName,
                            Status = e.Status,
                            UpdatedAt = e.UpdatedAt,
                            UserId = e.UserId,
                            Group = e.Group,
                            TokenSince = e.TokenSince,
                            FacebookUserId = e.FacebookUserId
                        }).FirstOrDefault();

                        if (employee == null)
                        {
                            def.meta = new Meta(404, "Not Found");
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(200, "Success");
                            def.data = employee;
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(201, "Invalid login method");
                        return Ok(def);
                    }
                }
                else
                {                    
                    //check exist email
                    if (loginModel.id != null && !loginModel.id.Trim().Equals(""))
                    {
                        int count = db.Users.Count(u => u.UserFacebookId.Trim() == loginModel.id);
                        if (count > 0)
                        {
                            def.meta = new Meta(500, "Existing Facebook User Id");
                            return Ok(def);
                        }
                    }
                    //check if email exist
                    if (loginModel.email != null && !loginModel.email.Trim().Equals(""))
                    {
                        int count = db.Users.Count(u => u.Email.Trim() == loginModel.email);
                        if (count > 0)
                        {
                            def.meta = new Meta(500, "Existing Email");
                            return Ok(def);
                        }
                    }
                    //create user
                    user = new User();
                    user.CreatedAt = DateTime.Now;
                    user.Email = loginModel.email;
                    user.FullName = loginModel.name;
                    user.Password = Utils.GetMD5Hash(user.UserId + Security.RandomString(7, false) + DateTime.Now.ToString("fff"));
                    user.Phone = "";
                    user.Status = 0;
                    user.UpdatedAt = DateTime.Now;
                    user.UserFacebookId = loginModel.id;
                    user.UserFacebookToken = loginModel.token;
                    user.IsThirdPartyLogin = true;
                    db.Users.Add(user);
                    db.SaveChanges();
                    //create shop
                    Shop shop = new Shop();
                    shop.Address = null;
                    shop.CreatedAt = DateTime.Now;
                    shop.Phone = "";
                    shop.ShopCategoryId = -1;
                    shop.ShopName = "Cửa hàng của " + loginModel.name;
                    shop.ShopProductPrefix = "SP";
                    shop.ShopOrderPrefix = "HD";
                    shop.ShopUniqueName = null;
                    shop.ShopCustomerPrefix = "KH";
                    shop.ShopCashInPrefix = "PT";
                    shop.ShopCashOutPrefix = "PC";
                    shop.ShopExportPrefix = "PX";
                    shop.ShopImportPrefix = "PN";
                    shop.ShopVendorPrefix = "NCC";
                    shop.ShopDVendorPrefix = "CVC";
                    shop.ShopReturnPrefix = "TH";
                    shop.ShopTransferPrefix = "CH";
                    shop.UpdatedAt = DateTime.Now;
                    shop.UserId = user.UserId;
                    shop.WorkTimeEnd = null;
                    shop.WorkTimeStart = null;
                    db.Entry(shop).State = EntityState.Added;
                    db.Shops.Add(shop);
                    db.SaveChanges();
                    //create cashtype
                    //cash in 
                    CashType cashIn = new CashType();
                    cashIn.CashTypeCode = "PT";
                    cashIn.CreatedAt = DateTime.Now;
                    cashIn.DefaultValue = 0;
                    cashIn.IsCustom = false;
                    cashIn.Name = "Phiếu thu";
                    cashIn.ShopId = shop.ShopId;
                    cashIn.Type = 1;
                    cashIn.UpdatedAt = DateTime.Now;
                    db.CashTypes.Add(cashIn);
                    db.SaveChanges();
                    //cash out
                    CashType cashOut = new CashType();
                    cashOut.CashTypeCode = "PC";
                    cashOut.CreatedAt = DateTime.Now;
                    cashOut.DefaultValue = 0;
                    cashOut.IsCustom = false;
                    cashOut.Name = "Phiếu chi";
                    cashOut.ShopId = shop.ShopId;
                    cashOut.Type = 0;
                    cashOut.UpdatedAt = DateTime.Now;
                    db.CashTypes.Add(cashOut);
                    db.SaveChanges();
                    //config
                    Config config = new Config();
                    config.AutoCreateOrder = false;
                    config.AutoHideComment = false;
                    config.NewCommentLike = false;
                    config.NewMessageNotification = true;
                    config.NewMessagePriority = true;
                    config.NotificationSound = true;
                    config.AutoHidePhoneComment = true;
                    config.AllowBotMessage = false;
                    config.ShopId = shop.ShopId;
                    db.Configs.Add(config);
                    db.SaveChanges();
                    //cash type            
                    Branch branch = new Branch();
                    branch.Address = shop.Address;
                    branch.ShopId = shop.ShopId;
                    branch.Status = 0;
                    branch.CreatedAt = DateTime.Now;
                    branch.UpdatedAt = DateTime.Now;
                    branch.Phone = shop.Phone;
                    branch.IsMainBranch = true;
                    branch.BranchName = shop.ShopName + " - " + "Chi nhánh chính";
                    db.Entry(branch).State = EntityState.Added;
                    db.Branches.Add(branch);
                    db.SaveChanges();
                    //end create shop
                    //save employee
                    Employee employee = new Employee();
                    employee.UserId = user.UserId;
                    employee.CreatedAt = DateTime.Now;
                    employee.Status = 0;
                    employee.UpdatedAt = DateTime.Now;
                    employee.GroupId = -1;
                    employee.EmployeeName = user.FullName;
                    employee.Password = user.Password;
                    employee.Email = user.Email;
                    employee.Group = "Admin";
                    employee.ShopId = shop.ShopId;
                    employee.BranchId = branch.BranchId;
                    employee.TokenSince = DateTime.Now;
                    employee.FacebookUserId = loginModel.id;
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    if (employee.EmployeeId != -1)
                    {
                        def.meta = new Meta(200, "Success");
                        def.data = employee;
                        return Ok(def);
                    }
                    else
                    {
                        //failed to create employee
                        //delete user
                        db.Users.Remove(user);
                        db.SaveChanges();
                        def.meta = new Meta(400, "Bad request");
                        def.data = employee;
                        return Ok(def);
                    }
                }
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [Route("api/employees/loginEmpFB")]
        [HttpPost]
        public async Task<IHttpActionResult> loginEmpFacebook([FromBody] LoginFacebookModel loginModel)
        {
            DefaultResponse def = new DefaultResponse();
            if (loginModel != null)
            {
                string id = loginModel.id;                
                string token = loginModel.token;
                //check email token
                if (loginModel.id == null || loginModel.token == null)
                {
                    def.meta = new Meta(211, "Invalid parameter");
                    return Ok(def);
                }
                //find emp 
                //check facebook token
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/me?access_token=" + loginModel.token);
                httpClient.Dispose();
                string result = response.Content.ReadAsStringAsync().Result;
                FBLoginResponse resp = JsonConvert.DeserializeObject<FBLoginResponse>(result);
                if (resp != null && resp.id != null)
                {
                    if (resp.id == loginModel.id)
                    {
                        Data.EmployeeLogin employee = db.Employees.Where(e => e.FacebookUserId == loginModel.id && e.Status != (int)Const.Status.DELETED).Select(e => new Data.EmployeeLogin()
                        {
                            Address = e.Address,
                            BranchId = e.BranchId,
                            CreatedAt = e.CreatedAt,
                            Email = e.Email,
                            EmployeeId = e.EmployeeId,
                            EmployeeName = e.EmployeeName,
                            GroupId = e.GroupId,
                            Password = e.Password,
                            Phone = e.Phone,
                            ShopId = e.ShopId,
                            ShopName = e.Shop.ShopName,
                            Status = e.Status,
                            UpdatedAt = e.UpdatedAt,
                            UserId = e.UserId,
                            Group = e.Group,
                            TokenSince = e.TokenSince,
                            FacebookUserId = e.FacebookUserId
                        }).FirstOrDefault();
                        def.data = employee;
                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(213, "Invalid user id");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(212, "Bad access token");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(211, "Invalid parameter");
                return Ok(def);
            }
        }

        [Authorize]
        // GET: api/Employees/5       
        public IHttpActionResult GetEmployee(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            EmployeeDTO employee = db.Employees.Where(e => e.EmployeeId == id).Select(e => new EmployeeDTO()
            {
                Address = e.Address,
                BranchId = e.BranchId,
                CreatedAt = e.CreatedAt,
                Email = e.Email,
                EmployeeId = e.EmployeeId,
                EmployeeName = e.EmployeeName,
                Group=e.Group,
                GroupId = e.GroupId,
                Password = e.Password,
                Phone = e.Phone,
                ShopId = e.ShopId,
                Status = e.Status,
                UpdatedAt = e.UpdatedAt,
                UserId = e.UserId,
                FacebookUserId = e.FacebookUserId
            }).FirstOrDefault();
            if (employee == null || employee.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = employee;
            return Ok(def);
        }

        [Authorize]
        [HttpGet]
        [Route("api/employees/GetUserInfo")]
        public IHttpActionResult GetUserInfo()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int EmployeeId = Int32.Parse(identity.Claims.Where(c => c.Type == "EmployeeId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (EmployeeId == 0 || EmployeeId == null)
            {
                def.meta = new Meta(400, "Invalid token");
                return Ok(def);
            }        
            Data.EmployeeLogin employee = db.Employees.Where(e => e.EmployeeId == EmployeeId).Select(e => new Data.EmployeeLogin()
            {
                Address = e.Address,
                BranchId = e.BranchId,
                CreatedAt = e.CreatedAt,
                Email = e.Email,
                EmployeeId = e.EmployeeId,
                EmployeeName = e.EmployeeName,
                GroupId = e.GroupId,               
                Phone = e.Phone,
                ShopId = e.ShopId,
                ShopName = e.Shop.ShopName,
                Status = e.Status,
                UpdatedAt = e.UpdatedAt,
                UserId = e.UserId,
                Group = e.Group,
                FacebookUserId = e.FacebookUserId
            }).FirstOrDefault();
            if (employee != null)
                def.meta = new Meta(200, "Success");
            else
                def.meta = new Meta(404, "Not Found");
            def.data = employee;
            return Ok(def);
        }

        [Authorize]
        // PUT: api/Employees/5
        public IHttpActionResult PutEmployee(int id, EmployeeDTO employee)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != employee.EmployeeId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Employee current = db.Employees.Where(s => s.EmployeeId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else if(current.Email != employee.Email)
            {
                def.meta = new Meta(211, "Can't Update");
                return Ok(def);
            }
            else
            {
                current.Address = employee.Address;
                current.BranchId = employee.BranchId;
                current.EmployeeName = employee.EmployeeName;
                current.GroupId = employee.GroupId;
                if (employee.Password != null && employee.Password.Trim().Length > 0)
                {
                    current.Password = Utils.GetMD5Hash(employee.Password);
                }
                current.Group = employee.Group;
                current.Phone = employee.Phone;                
                current.UpdatedAt = DateTime.Now;                
                
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        [Authorize]
        [HttpPut]
        [Route("api/employees/PutEmployeebyAdmin/{id}")]
        // PUT: api/Employees/5
        public IHttpActionResult PutEmployeebyAdmin(int id, EmployeeDTO employee)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Employee current = db.Employees.Where(s => s.EmployeeId == employee.EmployeeId).FirstOrDefault();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != employee.EmployeeId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            else
            {
                current.Address = employee.Address;
                current.BranchId = employee.BranchId;
                current.Status = employee.Status;
                current.EmployeeName = employee.EmployeeName;
                current.GroupId = employee.GroupId;
                if (employee.Password != null && employee.Password.Trim().Length > 0)
                {
                    current.Password = Utils.GetMD5Hash(employee.Password);
                }
                current.Group = employee.Group;
                current.Phone = employee.Phone;
                current.UpdatedAt = DateTime.Now;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        [Authorize]
        // POST: api/Employees     
        public IHttpActionResult PostEmployee(EmployeeDTO employee)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            //check email
            var count = db.Employees.Where(em => em.Email == employee.Email).Count();
            if (count > 0)
            {
                def.meta = new Meta(211, "Email exists");
                return Ok(def);
            }
           

            Employee e = new Employee();
            e.Address = employee.Address;
            e.BranchId = employee.BranchId;
            e.CreatedAt = DateTime.Now;
            e.Email = employee.Email;
            e.EmployeeName = employee.EmployeeName;
            e.GroupId = employee.GroupId;
            if (employee.Password != null && employee.Password.Trim().Length > 0)
            {
                e.Password = Utils.GetMD5Hash(employee.Password);
            }
            e.Phone = employee.Phone;
            e.ShopId = ShopId;
            e.Status = employee.Status;
            e.UpdatedAt = DateTime.Now;
            e.UserId = employee.UserId;
            e.Status = 0;
            e.GroupId = -1;
            e.Group = employee.Group;
       
            db.Employees.Add(e);
            db.SaveChanges();
            employee.EmployeeId = e.EmployeeId;

            def.meta = new Meta(200, "Success");
            def.data = employee;

            return Ok(def);
        }

        [Authorize]
        // DELETE: api/Employees/5       
        public IHttpActionResult DeleteEmployee(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Employee employee = db.Employees.Find(id);
            if (employee == null || employee.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            employee.Status = 99;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [Authorize]
        [Route("api/employees/GetUserPage")]
        public IHttpActionResult GetUserPage([FromUri] FilteredPagination paging)
        {
            //var identity = (ClaimsIdentity)User.Identity;
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            int count = 0;
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Employee> epuser = db.Employees.Where(ey=>ey.Group=="Admin");
                if (paging.query != null)
                {
                    count = epuser.Count();
                    paging.query = HttpUtility.UrlDecode(paging.query);
                    epuser = epuser.Where(paging.query);
                }
                else
                {
                    paging.query = "Group='Admin'";
                    epuser = epuser.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    epuser = epuser.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    epuser = epuser.OrderBy("EmployeeId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                var buls = db.UserBundles.Join(db.Bundles,
                    ub => ub.BundleId,
                    b => b.BundleId,
                     (ub, b) => new { userb = ub, bund = b }).
                     Where(ulp=>ulp.userb.IsCurrent==true).
                     Select(
                    dat => new { UserId = dat.userb.UserId,
                        BundleId = dat.userb.BundleId,
                        StartDate = dat.userb.StartDate,
                        EndDate = dat.userb.EndDate,
                        BundleName = dat.bund.BundleName
                    });

                def.data = epuser.Join(db.Shops, emp => emp.ShopId,
                                                shop => shop.ShopId,
                                                (emp, shop) => new
                                                {
                                                    emp.EmployeeId,
                                                    emp.UserId,
                                                    emp.ShopId,
                                                    emp.BranchId,
                                                    emp.EmployeeName,
                                                    emp.Address,
                                                    emp.Phone,
                                                    emp.Email,
                                                    emp.Status,
                                                    emp.GroupId,
                                                    emp.Group,
                                                    emp.CreatedAt,
                                                    emp.UpdatedAt,
                                                    emp.FacebookUserId,
                                                    shop.ShopName,
                                                }
                    ).SelectMany(
      emp => buls.Where(bu => bu.UserId == emp.UserId).DefaultIfEmpty(),
                         (emp,bu)=>new {
                        emp.EmployeeId,
                        emp.UserId,
                        emp.ShopId,
                        emp.BranchId,
                        emp.EmployeeName,
                        emp.Address,
                        emp.Phone,
                        emp.Email,
                        emp.Status,
                        emp.GroupId,
                        emp.Group,
                        emp.CreatedAt,
                        emp.UpdatedAt,
                        emp.FacebookUserId,
                        emp.ShopName,
                        bu.BundleId,
                        bu.BundleName,
                        bu.StartDate,
                        bu.EndDate
                    }
                    ).
                    
                    Select(v => new EmployeeuserDTO()
                {
                    EmployeeId = v.EmployeeId,
                    UserId   = v.UserId,
                    ShopId   = v.ShopId,
                    BranchId  = v.BranchId,
                    EmployeeName = v.EmployeeName,
                    Address = v.Address,
                    Phone = v.Phone,
                    Email = v.Email,
                    Status = v.Status,
                    GroupId = v.GroupId,
                    Group = v.Group,
                    CreatedAt = v.CreatedAt,
                    UpdatedAt = v.UpdatedAt,
                    FacebookUserId = v.FacebookUserId,
                    ShopName=v.ShopName,
                    BundleName=v.BundleName,
                    BundleId=v.BundleId,
                    StartAt=v.StartDate,
                    EndAt=v.EndDate
                });
                def.metadata = new MetadataTotal(count,0);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.EmployeeId == id) > 0;
        }


    }
}