﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.IO;
using System.Drawing;
using System.Web;
using System.Security.Claims;
using System.Configuration;

namespace SaleManager.Controllers
{
    public class UploadController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [HttpPost]
        [Route("api/uploadProduct/{id}")]
        public IHttpActionResult Upload(int id)
        {
            string domain = ConfigurationManager.AppSettings["domain"].ToString();
            //var identity = (ClaimsIdentity)User.Identity;            
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            Product product = db.Products.Find(id);
            int ShopId = (int)product.ShopId;
            DefaultResponse def = new DefaultResponse();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                List<String> files = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 4096 * 4096 * 1; //Size = 16 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var name = postedFile.FileName.Substring(0,postedFile.FileName.LastIndexOf('.') - 1);
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
                            def.meta = new Meta(600, message);
                            return Ok(def);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 16 mb.");
                            def.meta = new Meta(600, message);
                            return Ok(def);
                        }
                        else
                        {
                            byte[] fileData = null;
                            using (var binaryReader = new BinaryReader(postedFile.InputStream))
                            {
                                DateTime now = DateTime.Now;
                                fileData = binaryReader.ReadBytes(postedFile.ContentLength);
                                var filePath = HttpContext.Current.Server.MapPath("~/Images/" + ShopId + "/p" + id + "/" + name + "_thumb500_" + now.ToString("yyyyMMddHHmmssfff") + extension);
                                Utils.createThumb(500, filePath, fileData);
                                string img = domain + "/images/" + ShopId + "/p" + id + "/" + name + "_thumb500_" + now.ToString("yyyyMMddHHmmssfff") + extension;
                                string rel = "/images/" + ShopId + "/p" + id + "/" + name + "_thumb500_" + now.ToString("yyyyMMddHHmmssfff") + extension;
                                files.Add(img);
                                //add image to product
                                ProductImage pi = new ProductImage();
                                pi.Path = img;
                                pi.Alt = name;
                                pi.CreatedAt = DateTime.Now;
                                pi.FileName = postedFile.FileName;
                                pi.Priority = 0;
                                pi.ProductId = id;
                                pi.RelativePath = rel;
                                db.ProductImages.Add(pi);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                def.meta = new Meta(200, "Success");
                def.data = files;
                return Ok(def);
            }
            catch (Exception ex)
            {
                def.meta = new Meta(400, "Bad request");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/uploadImage/{id}")]
        [Authorize]
        public IHttpActionResult UploadImage(int id)
        {
            string domain = ConfigurationManager.AppSettings["domain"].ToString();
            var identity = (ClaimsIdentity)User.Identity;            
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (id != ShopId)
            {
                def.meta = new Meta(400, "Bad request");                
                return Ok(def);
            }           
            try
            {
                var httpRequest = HttpContext.Current.Request;
                List<String> files = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 4096 * 4096 * 1; //Size = 16 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var name = postedFile.FileName.Substring(0, postedFile.FileName.LastIndexOf('.') - 1);
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
                            def.meta = new Meta(600, message);
                            return Ok(def);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 16 mb.");
                            def.meta = new Meta(600, message);
                            return Ok(def);
                        }
                        else
                        {
                            byte[] fileData = null;
                            using (var binaryReader = new BinaryReader(postedFile.InputStream))
                            {
                                DateTime now = DateTime.Now;
                                fileData = binaryReader.ReadBytes(postedFile.ContentLength);
                                var filePath = HttpContext.Current.Server.MapPath("~/Images/" + ShopId + "/" + name + "_thumb500_" + now.ToString("yyyyMMddHHmmssfff") + extension);
                                Utils.createThumb(500, filePath, fileData);
                                string img = domain + "/images/" + ShopId + "/" + name + "_thumb500_" + now.ToString("yyyyMMddHHmmssfff") + extension;
                                string rel = "/images/" + ShopId + "/" + name + "_thumb500_" + now.ToString("yyyyMMddHHmmssfff") + extension;
                                files.Add(img);                                
                            }
                        }
                    }
                }
                def.meta = new Meta(200, "Success");
                def.data = files;
                return Ok(def);
            }
            catch (Exception ex)
            {
                def.meta = new Meta(400, "Bad request");
                return Ok(def);
            }
        }
    }
}
