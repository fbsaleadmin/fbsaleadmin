﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    public class ProductUnitsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/ProductUnits
        public IHttpActionResult GetProductUnits()
        {
            DefaultResponse def = new DefaultResponse();
            def.meta = new Meta(200, "Success");
            def.data = db.ProductUnits.Select(p => new ProductUnitDTO()
            {
                ProductId = p.ProductId,
                ProductUnitId = p.ProductUnitId,
                Quantity = p.Quantity,
                Unit = p.Unit,
                Status = p.Status
            });
            return Ok(def);
        }

        [Route("api/productunits/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<ProductUnit> productUnits;
                if (paging.order_by != null)
                {
                    productUnits = db.ProductUnits.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    productUnits = db.ProductUnits.OrderBy("ProductUnitId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                if (paging.query != null)
                    productUnits = productUnits.Where(HttpUtility.UrlDecode(paging.query));
                def.data = productUnits.Select(p => new ProductUnitDTO()
                {
                    ProductId = p.ProductId,
                    ProductUnitId = p.ProductUnitId,
                    Quantity = p.Quantity,
                    Unit = p.Unit,
                    Status = p.Status
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/ProductUnits/5      
        public IHttpActionResult GetProductUnit(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductUnitDTO productUnit = db.ProductUnits.Where(p => p.ProductUnitId == id).Select(p => new ProductUnitDTO()
            {
                ProductId = p.ProductId,
                ProductUnitId = p.ProductUnitId,
                Quantity = p.Quantity,
                Unit = p.Unit,
                Status = p.Status
            }).FirstOrDefault();
            if (productUnit == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = productUnit;
            return Ok(def);
        }

        // PUT: api/ProductUnits/5       
        public IHttpActionResult PutProductUnit(int id, ProductUnitDTO productUnit)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != productUnit.ProductUnitId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            ProductUnit current = db.ProductUnits.Where(s => s.ProductUnitId == id).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {

                current.Quantity = productUnit.Quantity;
                current.Unit = productUnit.Unit;
                current.Status = productUnit.Status;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/ProductUnits
        [ResponseType(typeof(ProductUnit))]
        public IHttpActionResult PostProductUnit(ProductUnitDTO productUnit)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            ProductUnit p = new ProductUnit();
            p.ProductId = productUnit.ProductId;
            p.Quantity = productUnit.Quantity;
            p.Unit = productUnit.Unit;
            p.Status = productUnit.Status;

            db.ProductUnits.Add(p);
            db.SaveChanges();
            productUnit.ProductUnitId = p.ProductUnitId;

            def.meta = new Meta(200, "Success");
            def.data = productUnit;

            return Ok(def);
        }

        // DELETE: api/ProductUnits/5
        [ResponseType(typeof(ProductUnit))]
        public IHttpActionResult DeleteProductUnit(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductUnit productUnit = db.ProductUnits.Find(id);
            if (productUnit == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            productUnit.Status = 99;
            //db.ProductUnits.Remove(productUnit);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductUnitExists(int id)
        {
            return db.ProductUnits.Count(e => e.ProductUnitId == id) > 0;
        }
    }
}