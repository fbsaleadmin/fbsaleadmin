﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class OrdersController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/orders/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Order> orders = db.Orders;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    orders = orders.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    orders = orders.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    orders = orders.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    orders = orders.OrderBy("OrderId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = orders.Select(o => new OrderDTO()
                {
                    BranchId = o.BranchId,
                    Channel = o.Channel,
                    CreatedAt = o.CreatedAt,
                    CustomerId = o.CustomerId,
                    DeliveryPrice = o.CustomerId,
                    DeliveryStatus = o.DeliveryStatus,
                    DiscountPrice = o.DiscountPrice,
                    EmployeeId = o.EmployeeId,
                    Note = o.Note,
                    OrderAddress = o.OrderAddress,
                    OrderCode = o.OrderCode,
                    OrderEmail = o.OrderEmail,
                    OrderId = o.OrderId,
                    OrderPhone = o.OrderPhone,
                    OrderPrice = o.OrderPrice,
                    OrderStatus = o.OrderStatus,
                    PaymentChannel = o.PaymentChannel,
                    PaymentStatus = o.PaymentStatus,
                    ShopId = o.ShopId,
                    TotalPrice = o.TotalPrice,
                    UpdatedAt = o.UpdatedAt,
                    DeliveryVendorId = o.DeliveryVendorId,
                    Paid = o.Paid,
                    CustomerPaid = o.CustomerPaid,
                    MoneyCollect = o.MoneyCollect,
                    SaleEmployeeId = o.SaleEmployeeId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }
        
        [HttpGet]
        [Route("api/orders/{id}/products")]
        public IHttpActionResult getProducts(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from o in db.OrderProducts
                          join pa in db.ProductAttributes on o.ProductAttributeId equals pa.ProductAttributeId
                          join p in db.Products on pa.ProductId equals p.ProductId
                          where o.OrderId == id
                          orderby o.CreatedAt descending
                          select new SaleManager.Data.OrderDetailProduct
                          {
                              OrderId = o.OrderId,
                              OrderProductId = o.OrderProductId,
                              ProductId = p.ProductId,
                              ProductName = p.ProductName,
                              SubName = pa.SubName,
                              Price = o.Price,
                              TotalPrice = o.Price * o.Quantity,
                              Quantity = o.Quantity,
                              QuantityReturn = 0,
                              ProductAttributeId = o.ProductAttributeId,
                              ProductCode = pa.ProductCode,
                              unit = new SaleManager.Data.Unit()
                              {
                                  Name = o.ProductUnit.Unit,
                                  ProductUnitId = o.ProductUnitId
                              },
                              images = p.ProductImages.Select(i => new Data.PI() { Alt = i.Alt, Path = i.Path, ProductImageId = i.ProductImageId, RelativePath = i.RelativePath }).ToList(),
                          }
                         ).AsEnumerable();

            var returnData = result.ToList();

            for (int i = 0; i < returnData.Count; i++)
            {
                var paid = returnData[i].ProductAttributeId;
                var productUnitId = returnData[i].unit.ProductUnitId;
                var listReturnOrder = (from od in db.Orders
                                       join op in db.OrderProducts on od.OrderId equals op.OrderId
                                       where od.RelatedOrderId == id && op.ProductAttributeId == paid && op.ProductUnitId == productUnitId
                                       group op by new
                                       {
                                           op.ProductAttributeId,
                                           op.ProductUnitId
                                       } into gop
                                       select new
                                       {
                                           Quantity = gop.Sum(e => e.Quantity),
                                       }).AsEnumerable().ToList();

                //int quantityMax = (int)products[i].Quantity;
                if (listReturnOrder != null && listReturnOrder.Count > 0)
                {
                    returnData[i].QuantityReturn += (int)listReturnOrder.FirstOrDefault().Quantity;
                }
            }

            def.meta = new Meta(200, "Success");
            def.data = returnData;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/returns/{id}/products")]
        public IHttpActionResult getReturnProducts(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from o in db.OrderProducts
                          join pa in db.ProductAttributes on o.ProductAttributeId equals pa.ProductAttributeId
                          join p in db.Products on pa.ProductId equals p.ProductId
                          where o.OrderId == id
                          orderby o.CreatedAt descending
                          select new SaleManager.Data.OrderDetailProduct
                          {
                              OrderId = o.OrderId,
                              OrderProductId = o.OrderProductId,
                              ProductId = p.ProductId,
                              ProductName = p.ProductName,
                              SubName = pa.SubName,
                              Price = o.Price,
                              TotalPrice = o.Price * o.Quantity,
                              Quantity = o.Quantity,
                              ProductAttributeId = o.ProductAttributeId,
                              ProductCode = pa.ProductCode,
                              unit = new SaleManager.Data.Unit()
                              {
                                  Name = o.ProductUnit.Unit,
                                  ProductUnitId = o.ProductUnitId
                              },
                              images = p.ProductImages.Select(i => new Data.PI() { Alt = i.Alt, Path = i.Path, ProductImageId = i.ProductImageId, RelativePath = i.RelativePath }).ToList(),
                          }
                         ).AsEnumerable();

            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/orders/{id}/cashe")]
        public IHttpActionResult getCashes(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Cashes
                          where c.TargetId == id && c.TargetType == "ORDER"
                          orderby c.CreatedAt descending
                          select new SaleManager.Data.OrderDetailCash
                          {
                              CashCode = c.CashCode,
                              CashId = c.CashId,
                              cashType = new Data.CashType()
                              {
                                  CashTypeId = c.CashType.CashTypeId,
                                  Name = c.CashType.Name,
                                  Type = c.CashType.Type
                              },
                              employee = new Data.Employee()
                              {
                                  EmployeeId = c.Employee.EmployeeId,
                                  EmployeeName = c.Employee.EmployeeName
                              },
                              PaidAmount = c.PaidAmount,
                              Title = c.Title,
                              Total = c.Total
                          }
                         ).AsEnumerable();
            if (result==null)
            {
                def.meta = new Meta(404, "Not found");
                return Ok(def);
            }
            int count = result.Count();
            def.meta = new Meta(200, "Success");
            def.data = result;
            def.metadata =new Metadata(count);
            return Ok(def);
        }

        [HttpGet]
        [Route("api/orders/{code}/cashes")]
        public IHttpActionResult getCashesByCode(string code)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            //find order
            Order order = db.Orders.Where(o => o.OrderCode == code).FirstOrDefault();
            if (order != null)
            {
                var result = (from c in db.Cashes
                              where c.CashCode == code && c.TargetType == "ORDER"
                              orderby c.CreatedAt descending
                              select new SaleManager.Data.OrderDetailCash
                              {
                                  CashCode = c.CashCode,
                                  CashId = c.CashId,
                                  cashType = new Data.CashType()
                                  {
                                      CashTypeId = c.CashType.CashTypeId,
                                      Name = c.CashType.Name,
                                      Type = c.CashType.Type
                                  },
                                  employee = new Data.Employee()
                                  {
                                      EmployeeId = c.Employee.EmployeeId,
                                      EmployeeName = c.Employee.EmployeeName
                                  },
                                  PaidAmount = c.PaidAmount,
                                  Title = c.Title,
                                  Total = c.Total
                              }
                             ).AsEnumerable();
                def.meta = new Meta(200, "Success");
                def.data = result;
            }
            else
            {
                def.meta = new Meta(404, "Not found");
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("api/returns/{id}/cashes")]
        public IHttpActionResult getReturnCashes(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Cashes
                          where c.TargetId == id && c.TargetType == "RETURN"
                          orderby c.CreatedAt descending
                          select new SaleManager.Data.OrderDetailCash
                          {
                              CashCode = c.CashCode,
                              CashId = c.CashId,
                              cashType = new Data.CashType()
                              {
                                  CashTypeId = c.CashType.CashTypeId,
                                  Name = c.CashType.Name,
                                  Type = c.CashType.Type
                              },
                              employee = new Data.Employee()
                              {
                                  EmployeeId = c.Employee.EmployeeId,
                                  EmployeeName = c.Employee.EmployeeName
                              },
                              PaidAmount = c.PaidAmount,
                              Title = c.Title,
                              Total = c.Total
                          }
                         ).AsEnumerable();
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/orders/{code}/detail")]
        public IHttpActionResult getCashes(string code)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from o in db.Orders
                          where o.OrderCode == code && o.OrderStatus != 99
                          orderby o.CreatedAt descending
                          select new SaleManager.Data.OrderDetail
                          {
                              CreatedAt = o.CreatedAt,
                              DeliveryStatus = o.DeliveryStatus,
                              Discount = o.DiscountPrice,
                              Note = o.Note,
                              OrderAddress = o.OrderAddress,
                              OrderCode = o.OrderCode,
                              OrderId = o.OrderId,
                              OrderPhone = o.OrderPhone,
                              PaymentStatus = o.PaymentStatus,
                              TotalPrice = o.TotalPrice,
                              branch = new SaleManager.Data.Branch()
                              {
                                  BranchId = o.BranchId,
                                  BranchName = o.Branch.BranchName
                              },
                              customer = new SaleManager.Data.Customer()
                              {
                                  CustomerCode = o.Customer.CustomerCode,
                                  CustomerId = o.Customer.CustomerId,
                                  CustomerName = o.Customer.CustomerName
                              },
                              employee = new SaleManager.Data.Employee()
                              {
                                  EmployeeId = o.Employee.EmployeeId,
                                  EmployeeName = o.Employee.EmployeeName
                              }
                          }).FirstOrDefault();
            //product
            //
            List<SaleManager.Data.OrderDetailProductWithUnit> lstProduct = new List<SaleManager.Data.OrderDetailProductWithUnit>();
            var products = db.OrderProducts.Where(op => op.OrderId == result.OrderId).ToList();
            for (int i = 0; i < products.Count; i++)
            {
                var paid = products[i].ProductAttributeId;
                var resultProduct = (from op in db.OrderProducts                                     
                                     join pa in db.ProductAttributes on op.ProductAttributeId equals pa.ProductAttributeId
                                     join p in db.Products on pa.ProductId equals p.ProductId
                                     where pa.ProductAttributeId == paid && op.OrderId == result.OrderId
                                     orderby pa.ProductAttributeId descending
                                     select new SaleManager.Data.OrderDetailProductWithUnit
                                     {
                                         ProductAttributeId = op.ProductAttributeId,
                                         ProductCode = pa.ProductCode,
                                         ProductId = p.ProductId,       
                                         ProductName = p.ProductName,
                                         SubName = pa.SubName,                                       
                                         Code = pa.Code,
                                         Quantity = op.Quantity,
                                         Price = op.Price                                         
                                     }).FirstOrDefault();
                if (paid != null)
                {
                    resultProduct.prices = (from pp in db.ProductPrices
                                            join pu in db.ProductUnits on pp.ProductUnitId equals pu.ProductUnitId
                                            where pp.ProductAttributeId == paid && pp.IsCurrent == true && pu.Status != 99
                                            orderby pp.SalePrice descending
                                            select new SaleManager.Data.ProductPrice()
                                            {
                                                ProductPriceId = pp.ProductPriceId,
                                                Unit = pp.ProductUnit.Unit,
                                                Quantity = pp.ProductUnit.Quantity,
                                                OriginPrice = pp.OriginPrice,
                                                SalePrice = pp.SalePrice
                                            }).ToList();
                    lstProduct.Add(resultProduct);
                }
            }
            result.products = lstProduct;
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/orders/{id}/{code}/returnorder")]
        public IHttpActionResult getReturnOrder(int id, string code)
        {
            var identity = (ClaimsIdentity)User.Identity;
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from o in db.Orders
                          where o.OrderCode == code && o.OrderStatus != (int)Const.OrderStatus.DELETED
                          && o.OrderStatus != (int)Const.OrderStatus.CANCELED && o.Type == (int)Const.OrderType.SALE && o.ShopId == id
                          orderby o.CreatedAt descending
                          select new SaleManager.Data.OrderDetail
                          {
                              CreatedAt = o.CreatedAt,
                              DeliveryStatus = o.DeliveryStatus,
                              Discount = o.DiscountPrice,
                              Note = o.Note,
                              OrderAddress = o.OrderAddress,
                              OrderCode = o.OrderCode,
                              OrderId = o.OrderId,
                              OrderPhone = o.OrderPhone,
                              PaymentStatus = o.PaymentStatus,
                              CustomerPaid=o.CustomerPaid,
                              TotalPrice = o.TotalPrice,
                              OrderPrice = o.OrderPrice,
                              Delivery=o.DeliveryPrice,

                              branch = new SaleManager.Data.Branch()
                              {
                                  BranchId = o.BranchId,
                                  BranchName = o.Branch.BranchName
                              },
                              customer = new SaleManager.Data.Customer()
                              {
                                  CustomerCode = o.Customer.CustomerCode,
                                  CustomerId = o.Customer.CustomerId,
                                  CustomerName = o.Customer.CustomerName
                              },
                              employee = new SaleManager.Data.Employee()
                              {
                                  EmployeeId = o.Employee.EmployeeId,
                                  EmployeeName = o.Employee.EmployeeName
                              },
                          }).FirstOrDefault();
            //product
            //
            if (result == null)
            {
                def.meta = new Meta(404, "Not found");
                def.data = result;
            }

            result.CustomerPaid = db.Cashes.Where(e => e.TargetId == result.OrderId && e.TargetType == "ORDER").Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(e => e.TargetId == result.OrderId && e.TargetType == "ORDER").Select(e => e.PaidAmount).Sum().Value : result.CustomerPaid;

            List<SaleManager.Data.OrderDetailProductWithUnit> lstProduct = new List<SaleManager.Data.OrderDetailProductWithUnit>();
            var products = db.OrderProducts.Where(op => op.OrderId == result.OrderId).ToList();

            for (int i = 0; i < products.Count; i++)
            {              
                var paid = products[i].ProductAttributeId;
                var price = products[i].Price;
                var productUnitId = products[i].ProductUnitId;
                DateTime CreatedAt = (DateTime)products[i].CreatedAt;

                var listReturnOrder = (from od in db.Orders
                                       join op in db.OrderProducts on od.OrderId equals op.OrderId
                                       where od.RelatedOrderId == result.OrderId && op.ProductAttributeId == paid && op.ProductUnitId == productUnitId
                                       group op by new
                                       {
                                           op.ProductAttributeId,
                                           op.ProductUnitId
                                       } into gop
                                       select new
                                       {
                                           Quantity = gop.Sum(e => e.Quantity),
                                       }).AsEnumerable().ToList();

                int quantityMax = (int)products[i].Quantity;
                if (listReturnOrder != null && listReturnOrder.Count > 0)
                {
                    quantityMax = (int)products[i].Quantity - (int)listReturnOrder.FirstOrDefault().Quantity;
                }

                var resultProduct = (from pa in db.ProductAttributes 
                                     join p in db.Products on pa.ProductId equals p.ProductId
                                     where pa.ProductAttributeId == paid
                                     orderby pa.ProductAttributeId descending
                                     select new SaleManager.Data.OrderDetailProductWithUnit
                                     {
                                         ProductAttributeId = pa.ProductAttributeId,
                                         ProductCode = pa.ProductCode,
                                         ProductId = p.ProductId,
                                         ProductName = p.ProductName,
                                         SubName = pa.SubName,
                                         Code = pa.Code,
                                         Quantity = quantityMax,
                                         Price = price,
                                         PriceOriginAvg = 0,
                                     }).FirstOrDefault();

                if (paid != null)
                {
                    resultProduct.prices = (from pp in db.ProductPrices
                                            join pu in db.ProductUnits on pp.ProductUnitId equals pu.ProductUnitId
                                            where pp.ProductAttributeId == paid && pp.IsCurrent == true && pu.Status != 99 && pu.ProductUnitId == productUnitId
                                            orderby pp.SalePrice descending
                                            select new SaleManager.Data.ProductPrice()
                                            {
                                                ProductPriceId = pp.ProductPriceId,
                                                Unit = pp.ProductUnit.Unit,
                                                Quantity = pp.ProductUnit.Quantity,
                                                OriginPrice = pp.OriginPrice,
                                                SalePrice = pp.SalePrice,
                                                ProductUnitId = pp.ProductUnitId
                                            }).ToList();

                    var importPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == paid && ep.ProductUnitId == productUnitId
                                       && ep.Exim.Type == (int)Const.EximType.IMPORT// && ep.CreatedAt <= CreatedAt
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();

                    var exportPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == paid && ep.ProductUnitId == productUnitId
                                       && ep.Exim.TargetType == "VENDOR" && ep.Exim.Type == (int)Const.EximType.EXPORT 
                                       && ep.Exim.Status != (int)Const.EximStatus.TEMP //&& ep.CreatedAt <= CreatedAt
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();

                    importPrice = importPrice.ToList();
                    decimal priceAVG = 0;
                    if ((importPrice == null || importPrice.Count() == 0))
                    {
                        priceAVG = 0;
                    }
                    else
                    {
                        double sumImportPrice = 0;
                        int countImportPrice = 0;
                        foreach (var itemIP in importPrice)
                        {
                            if (itemIP.Price != null)
                            {
                                int quantityExport = exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().HasValue ? exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().Value : 0;
                                int quantity = (int)itemIP.Quantity - quantityExport;
                                countImportPrice += quantity;
                                sumImportPrice += (double)(itemIP.Price * quantity);
                            }
                        }

                        if (countImportPrice != 0)
                        {
                            priceAVG = Math.Round((decimal)(sumImportPrice / (int)countImportPrice), 0);
                        }
                        else
                        {
                            priceAVG = 0;
                        }
                    }

                    resultProduct.PriceOriginAvg = priceAVG;
                    lstProduct.Add(resultProduct);
                }
            }

            result.products = lstProduct;
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        // GET: api/Orders/5      
        public IHttpActionResult GetOrder(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            OrderDTO order = db.Orders.Where(o => o.OrderId == id).Select(o => new OrderDTO()
            {
                BranchId = o.BranchId,
                Channel = o.Channel,
                CreatedAt = o.CreatedAt,
                CustomerId = o.CustomerId,
                DeliveryPrice = o.CustomerId,
                DeliveryStatus = o.DeliveryStatus,
                DiscountPrice = o.DiscountPrice,
                EmployeeId = o.EmployeeId,
                Note = o.Note,
                OrderAddress = o.OrderAddress,
                OrderCode = o.OrderCode,
                OrderEmail = o.OrderEmail,
                OrderId = o.OrderId,
                OrderPhone = o.OrderPhone,
                OrderPrice = o.OrderPrice,
                OrderStatus = o.OrderStatus,
                PaymentChannel = o.PaymentChannel,
                PaymentStatus = o.PaymentStatus,
                ShopId = o.ShopId,
                TotalPrice = o.TotalPrice,
                UpdatedAt = o.UpdatedAt,
                DeliveryVendorId = o.DeliveryVendorId,
                Paid = o.Paid,
                CustomerPaid = o.CustomerPaid,
                MoneyCollect = o.MoneyCollect,
                SaleEmployeeId = o.SaleEmployeeId
            }).FirstOrDefault();
            if (order == null || order.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = order;
            return Ok(def);
        }

        // PUT: api/Orders/5     
        public IHttpActionResult PutOrder(int id, OrderDTO order)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != order.OrderId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Order current = db.Orders.Where(s => s.OrderId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                string lastNote = current.Note != null ? current.Note.Trim() : "";
                current.DeliveryStatus = order.DeliveryStatus;   
                if (order.Note != null && order.Note.Trim().Length > 0 && !lastNote.Equals(order.Note.Trim()))
                    current.Note = order.Note;              
                if (order.Note != null && order.Note.Trim().Length > 0 && !lastNote.Equals(order.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = order.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.OrderId;
                    note.TargetType = "ORDER";                  
                    db.Notes.Add(note);
                    db.SaveChanges();
                }                
                db.Entry(current).State = EntityState.Modified;           
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        [HttpGet]
        [Route("api/orders/getOrderCode/{id}")]
        public IHttpActionResult getProductCode(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            int count = db.Orders.Where(o => o.ShopId == ShopId).Count();
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopOrderPrefix + code;
            def.meta = new Meta(200, "Success");
            def.data = code;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/orders/{id}/notes")]
        public IHttpActionResult GetNotes(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from n in db.Notes
                              where n.TargetId == id && n.TargetType == "ORDER"
                              orderby n.CreatedAt descending
                              select new SaleManager.Data.OrderNote
                              {
                                  Content = n.Content,
                                  CreatedAt = n.CreatedAt,
                                  NoteId = n.NoteId,
                                  OrderId = n.TargetId
                              }).AsEnumerable();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/returns/{id}")]
        public IHttpActionResult updateReturn(int id, ReturnDTO returns)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != returns.ReturnId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Order current = db.Orders.Where(s => s.OrderId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                string lastNote = current.Note != null ? current.Note.Trim() : "";
                if (returns.Note != null && returns.Note.Trim().Length > 0 && !lastNote.Equals(returns.Note.Trim()))
                    current.Note = returns.Note;
                if (returns.Note != null && returns.Note.Trim().Length > 0 && !lastNote.Equals(returns.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = returns.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.OrderId;
                    note.TargetType = "RETURN";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
                db.Entry(current).State = EntityState.Modified;  
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.OrderId == id) > 0;
        }
    }
}