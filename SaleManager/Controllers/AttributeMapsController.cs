﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class AttributeMapsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        //[Route("api/attributemap/GetByPage")]
        //public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (paging != null)
        //    {
        //        def.meta = new Meta(200, "Success");
        //        IQueryable<AttributeMap> attributeMaps;
        //        if (paging.order_by != null)
        //        {
        //            attributeMaps = db.AttributeMaps.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        else
        //        {
        //            attributeMaps = db.AttributeMaps.OrderBy("AttributeMapId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        if (paging.query != null)
        //            attributeMaps = attributeMaps.Where(HttpUtility.UrlDecode(paging.query));
        //        def.data = attributeMaps.Select(a => new AttributeMapDTO()
        //        {
        //            AttributeId = a.AttributeId,
        //            AttributeMapId = a.AttributeMapId,
        //            CreatedAt = a.CreatedAt,
        //            ProductAttributeId = a.ProductAttributeId,
        //            Status = a.Status,
        //            UpdatedAt = a.UpdatedAt,
        //            Value = a.Value
        //        });
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //}

        //// GET: api/AttributeMaps/5      
        //public IHttpActionResult GetAttributeMap(int id)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    AttributeMapDTO attributeMap = db.AttributeMaps.Where(a => a.AttributeMapId == id).Select(a => new AttributeMapDTO()
        //    {
        //        AttributeId = a.AttributeId,
        //        AttributeMapId = a.AttributeMapId,
        //        CreatedAt = a.CreatedAt,
        //        ProductAttributeId = a.ProductAttributeId,
        //        Status = a.Status,
        //        UpdatedAt = a.UpdatedAt,
        //        Value = a.Value
        //    }).FirstOrDefault();
        //    if (attributeMap == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    def.meta = new Meta(200, "Success");
        //    def.data = attributeMap;
        //    return Ok(def);
        //}

        //// PUT: api/AttributeMaps/5 
        //public IHttpActionResult PutAttributeMap(int id, AttributeMapDTO attributeMap)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    if (id != attributeMap.AttributeMapId)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //    AttributeMap current = db.AttributeMaps.Where(s => s.AttributeMapId == id).FirstOrDefault();
        //    if (current == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        current.Status = attributeMap.Status;
        //        current.Value = attributeMap.Value;
        //        current.AttributeId = attributeMap.AttributeId;
        //        current.ProductAttributeId = attributeMap.ProductAttributeId;
        //        current.UpdatedAt = DateTime.Now;                
        //        db.Entry(current).State = EntityState.Modified;
        //        try
        //        {
        //            db.SaveChanges();
        //            def.meta = new Meta(200, "Success");
        //            return Ok(def);
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            def.meta = new Meta(500, "Internal Server Error");
        //            return Ok(def);
        //        }
        //    }  
        //}

        //// POST: api/AttributeMaps        
        //public IHttpActionResult PostAttributeMap(AttributeMapDTO attributeMap)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    AttributeMap am = new AttributeMap();
        //    am.AttributeId = attributeMap.AttributeId;
        //    am.CreatedAt = DateTime.Now;
        //    am.ProductAttributeId = attributeMap.ProductAttributeId;
        //    am.Status = attributeMap.Status;
        //    am.UpdatedAt = DateTime.Now;
        //    am.Value = attributeMap.Value;

        //    db.AttributeMaps.Add(am);
        //    db.SaveChanges();
        //    am.AttributeId = attributeMap.AttributeId;

        //    def.meta = new Meta(200, "Success");
        //    def.data = attributeMap;

        //    return Ok(def);
        //}

        //// DELETE: api/AttributeMaps/5        
        //public IHttpActionResult DeleteAttributeMap(int id)
        //{

        //    DefaultResponse def = new DefaultResponse();
        //    AttributeMap attributeMap = db.AttributeMaps.Find(id);
        //    if (attributeMap == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    db.AttributeMaps.Remove(attributeMap);
        //    db.SaveChanges();

        //    def.meta = new Meta(200, "Success");
        //    return Ok(def);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AttributeMapExists(int id)
        {
            return db.AttributeMaps.Count(e => e.AttributeMapId == id) > 0;
        }
    }
}