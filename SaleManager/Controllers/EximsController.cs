﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using SaleManager.Data;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    public class EximsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // DELETE: api/Exims/5
        [ResponseType(typeof(Exim))]
        public IHttpActionResult DeleteExim(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Exim exim = db.Exims.Find(id);
            if (exim == null || exim.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            exim.Status = 99;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpGet]
        [Route("api/exims/{id}/products")]
        public IHttpActionResult getProducts(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from ep in db.EximProducts
                          join pa in db.ProductAttributes on ep.ProductAttributeId equals pa.ProductAttributeId
                          join p in db.Products on pa.ProductId equals p.ProductId 
                          where ep.EximId == id
                          orderby ep.CreatedAt descending
                          select new SaleManager.Data.EximProduct
                          {
                            EximId = ep.EximId,
                            EximProductId = ep.EximProductId,
                            LastStock = ep.LastStock,
                            PreviousStock = ep.PreviousStock,
                            Price = ep.Price,
                            ProductAttributeId = ep.ProductAttributeId,
                            ProductId = p.ProductId,
                            ProductCode = pa.ProductCode,
                            ProductName = p.ProductName,
                            Quantity = ep.Quantity,
                            DiscountPerItem = ep.DiscountPerItem,
                            SubName = pa.SubName,
                            unit = new Unit()
                            {
                                Name = ep.ProductUnit.Unit,
                                ProductUnitId = ep.ProductUnitId
                            }                           
                          }
                         ).AsEnumerable();

            if (paging.query != null)
                result = result.Where(HttpUtility.UrlDecode(paging.query)).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/exims/{id}/cashe")]
        public IHttpActionResult getCashes(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            try { 
            var result = (from c in db.Cashes
                          where c.TargetId == id && (c.TargetType == "IMPORT" || c.TargetType == "IMPORT_RETURN")
                          select new SaleManager.Data.OrderDetailCash
                          {
                              CashCode = c.CashCode,
                              CashId = c.CashId,
                              cashType = new Data.CashType()
                              {
                                  CashTypeId = c.CashType.CashTypeId,
                                  Name = c.CashType.Name,
                                  Type = c.CashType.Type
                              },
                              employee = new Data.Employee()
                              {
                                  EmployeeId = c.Employee.EmployeeId,
                                  EmployeeName = c.Employee.EmployeeName
                              },
                              PaidAmount = c.PaidAmount,
                              Title = c.Title,
                              Total = c.Total
                          }
                         ).AsEnumerable();
                int count = result.Count();
            if (paging.query != null)
                result = result.Where(HttpUtility.UrlDecode(paging.query)).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.data = result;
                def.metadata = new Metadata(count);
            return Ok(def);
            }
            catch(Exception e)
            {
                def.meta = new Meta(404, "Not found");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/exims/{id}/cashes")]
        public IHttpActionResult getCashesByCode(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            //find target
            Exim exim = db.Exims.Where(ex => ex.EximId == id).FirstOrDefault();
            if (exim != null)
            {
                var result = (from c in db.Cashes
                              join v in db.Vendors on c.PaidTargetId equals v.VendorId
                              where c.TargetId == exim.EximId && c.TargetType == "IMPORT_RETURN"
                              select new SaleManager.Data.OrderDetailCash
                              {
                                  CashCode = c.CashCode,
                                  CashId = c.CashId,
                                  cashType = new Data.CashType()
                                  {
                                      CashTypeId = c.CashType.CashTypeId,
                                      Name = c.CashType.Name,
                                      Type = c.CashType.Type
                                  },
                                  employee = new Data.Employee()
                                  {
                                      EmployeeId = c.Employee.EmployeeId,
                                      EmployeeName = c.Employee.EmployeeName
                                  },
                                  PaidAmount = c.PaidAmount,
                                  Title = c.Title,
                                  Total = c.Total
                              }
                             ).AsEnumerable();
                if (paging.query != null)
                    result = result.Where(HttpUtility.UrlDecode(paging.query)).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                else
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
            }
            else
            {
                def.meta = new Meta(404, "Not found");
            }
            return Ok(def);
        }

        public IHttpActionResult PutExim(int id, EximDTO exim)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != exim.EximId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Exim current = db.Exims.Where(s => s.EximId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.PaymentStatus = exim.PaymentStatus;
                current.DeliveryStatus = exim.DeliveryStatus;              
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        [HttpGet]
        [Route("api/imports/{code}")]
        public IHttpActionResult getImportByCode(string code)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from e in db.Exims
                          where e.Code == code && e.Status != (int)Const.EximStatus.DELETED && e.Status != (int) Const.EximStatus.CANCELED && e.Status != (int) Const.EximStatus.TEMP
                          orderby e.CreatetAt descending
                          select new SaleManager.Data.ImportDetail
                          {
                            Code = e.Code,
                            CreatedAt = e.CreatetAt,
                            EximId = e.EximId,
                            TargetId = e.TargetId,
                            Status = e.Status,
                            TotalPrice=e.Price,
                            TotalDiscount=e.Discount,
                          }).FirstOrDefault();
            //product
            //
            if (result == null)
            {
                def.meta = new Meta(404, "Not found");
                def.data = result;
            }

            result.VendorPaid = db.Cashes.Where(e => e.TargetId == result.EximId && e.TargetType == "IMPORT").Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(e => e.TargetId == result.EximId && e.TargetType == "IMPORT").Select(e => e.PaidAmount).Sum().Value : 0;

            List<SaleManager.Data.ImportDetailProduct> lstProduct = new List<SaleManager.Data.ImportDetailProduct>();
            var products = db.EximProducts.Where(ep => ep.EximId == result.EximId).ToList();
            for (int i = 0; i < products.Count; i++)
            {
                var paid = products[i].ProductAttributeId;
                var productUnitId = products[i].ProductUnitId;
                var quantity = products[i].Quantity;
                var price = products[i].Price;

                var listReturnExim = (from od in db.Exims
                                       join op in db.EximProducts on od.EximId equals op.EximId
                                       where od.RelatedEximId == result.EximId && op.ProductAttributeId == paid && op.ProductUnitId == productUnitId
                                       group op by new
                                       {
                                           op.ProductAttributeId,
                                           op.ProductUnitId
                                       } into gop
                                       select new
                                       {
                                           Quantity = gop.Sum(e => e.Quantity),
                                       }).AsEnumerable().ToList();

                int quantityMax = (int)products[i].Quantity;
                if (listReturnExim != null && listReturnExim.Count > 0)
                {
                    quantityMax = (int)products[i].Quantity - (int)listReturnExim.FirstOrDefault().Quantity;
                }

                var resultProduct = (from pa in db.ProductAttributes
                                     join p in db.Products on pa.ProductId equals p.ProductId
                                     where pa.ProductAttributeId == paid
                                     orderby pa.ProductAttributeId descending
                                     select new SaleManager.Data.ImportDetailProduct
                                     {
                                         ProductAttributeId = pa.ProductAttributeId,
                                         ProductCode = pa.ProductCode,
                                         ProductId = p.ProductId,
                                         ProductName = p.ProductName,
                                         SubName = pa.SubName,
                                         Code = pa.Code,
                                         Quantity = quantityMax,
                                         MaxQuantity = quantityMax,
                                         Price = price
                                     }).FirstOrDefault();
                lstProduct.Add(resultProduct);                
            }
            result.products = lstProduct;
            //target
            result.target = db.Vendors.Where(v => v.VendorId == result.TargetId).Select(v => new Target() { 
                TargetId = v.VendorId,
                TargetCode = v.VendorCode,
                TargetName = v.VendorName                
            }).FirstOrDefault();
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EximExists(int id)
        {
            return db.Exims.Count(e => e.EximId == id) > 0;
        }
    }
}