﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class TemplatesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/shops/{id}/templates")]
        [Route("api/templates/GetByPage")]
        public IHttpActionResult GetByPage(int id,[FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Template> templates = db.Templates;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    templates = templates.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    templates = templates.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    templates = templates.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    templates = templates.OrderBy("TemplateId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }          
                def.data = templates.Select(t => new TemplateDTO()
                {
                    Content = t.Content,
                    CreatedAt = t.CreatedAt,
                    IsAutoMessage = t.IsAutoMessage,
                    Mark = t.Mark,
                    Name = t.Name,
                    PageId = t.PageId,
                    ShopId = t.ShopId,
                    TemplateId = t.TemplateId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Templates/5
        [ResponseType(typeof(Template))]
        public IHttpActionResult GetTemplate(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            TemplateDTO template = db.Templates.Where(v => v.TemplateId == id).Select(t => new TemplateDTO()
            {
                Content = t.Content,
                CreatedAt = t.CreatedAt,
                IsAutoMessage = t.IsAutoMessage,
                Mark = t.Mark,
                Name = t.Name,
                PageId = t.PageId,
                ShopId = t.ShopId,
                TemplateId = t.TemplateId
            }).FirstOrDefault();
            if (template == null || template.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = template;
            return Ok(def);
        }

        // PUT: api/Templates/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTemplate(int id, TemplateDTO template)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != template.TemplateId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Template current = db.Templates.Where(s => s.TemplateId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Mark = template.Mark;
                current.Name = template.Name;
                if (template.PageId != null)
                    current.PageId = template.PageId;
                current.IsAutoMessage = template.IsAutoMessage;
                current.Content = template.Content; 
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Templates
        [ResponseType(typeof(Template))]
        public IHttpActionResult PostTemplate(TemplateDTO template)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Template temp = new Template();
            temp.Content = template.Content;
            temp.CreatedAt = DateTime.Now;
            temp.IsAutoMessage = template.IsAutoMessage;
            temp.Mark = "";//template.Mark;
            temp.Name = template.Name;
            temp.PageId = template.PageId;
            temp.ShopId = ShopId;

            db.Templates.Add(temp);
            db.SaveChanges();
            template.TemplateId = temp.TemplateId;

            def.meta = new Meta(200, "Success");
            def.data = template;

            return Ok(def);
        }

        // DELETE: api/Templates/5
        [ResponseType(typeof(Template))]
        public IHttpActionResult DeleteTemplate(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Template template = db.Templates.Find(id);
            if (template == null || template.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.Templates.Remove(template);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TemplateExists(int id)
        {
            return db.Templates.Count(e => e.TemplateId == id) > 0;
        }
    }
}