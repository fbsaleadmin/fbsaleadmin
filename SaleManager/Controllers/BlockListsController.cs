﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class BlockListsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/blocklists/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<BlockList> blocklists = db.BlockLists;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    blocklists = blocklists.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    blocklists = blocklists.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    blocklists = db.BlockLists.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    blocklists = db.BlockLists.OrderBy("BlockListId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }             
                def.data = blocklists.Select(c => new BlockListDTO()
                {
                    CreatedAt = DateTime.Now,
                    CustomerId = c.CustomerId,
                    FacebookUserId = c.FacebookUserId,
                    ShopId = c.ShopId,
                    Status = c.Status
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }
      
        // GET: api/BlockLists/5     
        public IHttpActionResult GetBlockList(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            BlockListDTO blockList = db.BlockLists.Where(c => c.BlockListId == id).Select(c => new BlockListDTO()
            {
                CreatedAt = DateTime.Now,
                CustomerId = c.CustomerId,
                FacebookUserId = c.FacebookUserId,
                ShopId = c.ShopId,
                Status = c.Status
            }).FirstOrDefault();
            if (blockList == null || !blockList.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = blockList;
            return Ok(def);
        }

        // POST: api/BlockLists
        [ResponseType(typeof(BlockList))]
        public IHttpActionResult PostBlockList(BlockListDTO blockList)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(cl => cl.Type == "ShopId").Select(cl => cl.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            BlockList c = new BlockList();
            c.CreatedAt = DateTime.Now;
            c.CustomerId = blockList.CustomerId;
            c.FacebookUserId = blockList.FacebookUserId;
            c.ShopId = ShopId;
            c.Status = blockList.Status;
            db.BlockLists.Add(c);
            db.SaveChanges();
            blockList.BlockListId = c.BlockListId;

            def.meta = new Meta(200, "Success");
            def.data = blockList;

            return Ok(def);
        }

        // DELETE: api/BlockLists/5
        [ResponseType(typeof(BlockList))]
        public IHttpActionResult DeleteBlockList(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            BlockList blockList = db.BlockLists.Find(id);
            if (blockList == null || blockList.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            //delete message belong to conversation      
            db.BlockLists.Remove(blockList);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BlockListExists(int id)
        {
            return db.BlockLists.Count(e => e.BlockListId == id) > 0;
        }
    }
}