﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SaleManager.Models;
using SaleManager.Models.Payment;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using log4net;

namespace SaleManager.Controllers
{
    public class PaymentController : ApiController
    {
        public static string access_key = "nmxaym7inas80e00dgfu";
        public static string secret_key = "gnu8hz3nyiccxtxut7twa99p7yqelz1b";
        public static string return_url = "https://apiv1.fb.sale/api/payment/ProcessTransaction";
        public static string one_pay_begin_transaction_url = "http://api.1pay.vn/bank-charging/service";
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly ILog log = LogMaster.GetLogger("payment", "payment");

        [HttpPost]
        [Route("api/payment/BeginTransaction")]
        public async Task<IHttpActionResult> BeginTransaction(BeginTransactionReq req)
        {
            DefaultResponse def = new DefaultResponse();
            if (req.BundleId == null || req.signature == null || req.ReturnUrl == null || req.time == null || req.UserId == null)
            {
                def.meta = new Meta(400, "Bad request");           
                return Ok(def);
            }
            //check valid signature
            //time = unix time
            //hash = md5 (UserId + "|" + BundleId + "|" + "fb.sale" + "|" + time);
            string signature = Utils.GetMD5Hash(req.UserId + "|" + req.BundleId + "|" + "fb.sale" + "|" + req.time).ToLower();
            log.Info(JsonConvert.SerializeObject(req));
            if (signature == req.signature)
            {
                //Get Bundle
                Bundle bundle = db.Bundles.Find(req.BundleId);
                if (bundle == null)
                {
                    def.meta = new Meta(202, "Invalid Bundle");
                    return Ok(def);
                }
                //init transaction
                DateTime now = DateTime.Now;
                Transaction trans = new Transaction();
                trans.Amount = bundle.Price;
                trans.CardName = "";
                trans.CardType = "";
                trans.CreatedAt = now;
                trans.CurrentCommand = "request_transaction";
                trans.Description = "Mua goi " + bundle.BundleName + " cho user " + req.UserId;
                trans.OrderType = "";
                trans.PaymentCode = "-1";
                trans.PaymentMessage = "Init";
                trans.PayUrl = "";
                trans.RequestTime = null;
                trans.ResponseTime = null;
                trans.TargetId = bundle.BundleId;
                trans.TargetType = "BUNDLE";
                trans.TxRefId = "";
                trans.TxStatus = -1;
                trans.UpdatedAt = now;
                trans.UserId = req.UserId;
                trans.ReturnUrl = req.ReturnUrl;
                db.Transactions.Add(trans);
                db.SaveChanges();
                if (trans.TransactionId != null && trans.TransactionId > 0)
                { 
                    //create request to onepay
                    OnePayTxRequestReq onePayReq = new OnePayTxRequestReq();
                    onePayReq.access_key = access_key;
                    onePayReq.amount = (int)trans.Amount;
                    onePayReq.command = "request_transaction";
                    onePayReq.order_id = trans.TransactionId;
                    onePayReq.order_info = trans.Description;
                    onePayReq.return_url = return_url;
                    onePayReq.signature = Security.HmacSha256Digest("access_key=" + access_key + "&amount=" + (int)trans.Amount + "&command=request_transaction&order_id=" + trans.TransactionId + "&order_info=" + trans.Description + "&return_url=" + onePayReq.return_url,secret_key);
                    log.Info(JsonConvert.SerializeObject(onePayReq));
                    //form data
                    List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                    values.Add(new KeyValuePair<string, string>("access_key", onePayReq.access_key));
                    values.Add(new KeyValuePair<string, string>("amount", onePayReq.amount.ToString()));
                    values.Add(new KeyValuePair<string, string>("command", onePayReq.command));
                    values.Add(new KeyValuePair<string, string>("order_id", onePayReq.order_id.ToString()));
                    values.Add(new KeyValuePair<string, string>("order_info", onePayReq.order_info));
                    values.Add(new KeyValuePair<string, string>("return_url", onePayReq.return_url));
                    values.Add(new KeyValuePair<string, string>("signature", onePayReq.signature));
                    var content = new FormUrlEncodedContent(values);
                    log.Info(content.ReadAsStringAsync().Result);
                    //begin request
                    HttpClient httpClient = new HttpClient();
                    HttpResponseMessage response = await httpClient.PostAsync(one_pay_begin_transaction_url, content);
                    httpClient.Dispose();
                    string result = response.Content.ReadAsStringAsync().Result;
                    log.Info(result);
                    if (result != null)
                    {
                        //get pay url
                        try
                        {
                            OnePayTxRequestResp onePayResp = JsonConvert.DeserializeObject<OnePayTxRequestResp>(result);
                            if (onePayResp.pay_url != null && onePayResp.status != null && onePayResp.trans_ref != null)
                            {
                                //return pay url to client
                                trans.TxRefId = onePayResp.trans_ref;
                                trans.PaymentMessage = onePayResp.status;
                                trans.PayUrl = onePayResp.pay_url;
                                trans.UpdatedAt = DateTime.Now;
                                trans.TxStatus = 90;
                                trans.UpdatedAt = DateTime.Now;
                                db.Entry(trans).State = EntityState.Modified;
                                db.SaveChanges();
                                def.data = onePayResp.pay_url;
                                def.meta = new Meta(200, "Success");
                                return Ok(def);
                            }
                            else
                            {
                                trans.TxStatus = 97;
                                trans.UpdatedAt = DateTime.Now;
                                db.Entry(trans).State = EntityState.Modified;
                                db.SaveChanges();
                                def.meta = new Meta(500, "Internal Server Error");
                                return Ok(def);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Parse json error
                            trans.TxStatus = 98;
                            trans.UpdatedAt = DateTime.Now;
                            db.Entry(trans).State = EntityState.Modified;
                            db.SaveChanges();
                            def.meta = new Meta(500, "Internal Server Error");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        //transaction request error
                        trans.TxStatus = 99;
                        trans.UpdatedAt = DateTime.Now;
                        db.Entry(trans).State = EntityState.Modified;                        
                        db.SaveChanges();
                        def.meta = new Meta(500, "Internal Server Error");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(201, "Invalid signature");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/payment/ProcessTransaction")]
        public async Task<IHttpActionResult> ProcessTransaction([FromUri]OnePayProcessReq req)
        {
            try
            {
                //get tx
                Transaction trans = db.Transactions.Find(Int32.Parse(req.order_id));
                //check signature
                string signature = "access_key=" + req.access_key + "&amount=" + req.amount + "&card_name=" + req.card_name + "&card_type=" + req.card_type + "&order_id=" + req.order_id + "&order_info=" + req.order_info + "&order_type=" + req.order_type + "&request_time=" + req.request_time.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'") + "&response_code=" + req.response_code + "&response_message=" + req.response_message + "&response_time=" + req.response_time.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'") + "&trans_ref=" + req.trans_ref + "&trans_status=" + req.trans_status;
                log.Info(signature);
                signature = Security.HmacSha256Digest(signature, secret_key);
                log.Info(req.signature + "-" + signature);
                //ignore signature valid
                if (req.signature != signature + "123")
                {
                    //ignore processed pay
                    if (trans.PaymentCode != "-1")
                    {
                        //return Ok();
                        //redirect to return url
                        return Redirect(trans.ReturnUrl + "?error_code=-1&id=" + trans.TransactionId);
                    }
                    //ok
                    //check response code
                    if (req.response_code == "00")
                    {
                        //payment success
                        trans.TxStatus = 0;
                        trans.PaymentMessage = "Thành công";
                        trans.PaymentCode = "00";
                        trans.CardName = req.card_name;
                        trans.CardType = trans.CardType;
                        trans.OrderType = trans.OrderType;
                        trans.RequestTime = req.request_time;
                        trans.ResponseTime = req.response_time;                        
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                        //get bundle
                        //change status user bundle
                        List<UserBundle> userBundles = db.UserBundles.Where(ub => ub.UserId == trans.UserId).ToList();
                        if (userBundles != null && userBundles.Count > 0)
                        {
                            //change status
                            for (int i = 0;i<userBundles.Count;i++)
                            {
                                UserBundle userBundle = userBundles[i];
                                userBundle.IsCurrent = false;
                                db.Entry(userBundle).State = EntityState.Modified;                                
                            }
                            db.SaveChanges();
                        }
                        //Bundle
                        Bundle bundle = db.Bundles.Find(trans.TargetId);
                        if (bundle != null)
                        {
                            //create new bundle
                            DateTime now = DateTime.Now;
                            UserBundle newBundle = new UserBundle();
                            newBundle.BundleId = trans.TargetId;
                            newBundle.CreatedAt = now;
                            newBundle.EndDate = now.AddDays((int)bundle.ExpiredDay);
                            newBundle.IsCurrent = true;
                            newBundle.IsPaid = true;
                            newBundle.StartDate = now;
                            newBundle.Status = 0;
                            newBundle.UpdatedAt = now;
                            newBundle.UserId = trans.UserId;
                            db.UserBundles.Add(newBundle);
                            db.SaveChanges();
                            //ok done
                            trans.TxStatus = 00;
                            trans.PaymentMessage = "Success";
                            trans.PaymentCode = "00";
                            db.Entry(trans).State = EntityState.Modified;
                            db.SaveChanges();                           
                        }
                        else
                        {
                            trans.TxStatus = 50;
                            trans.PaymentMessage = "Invalid bundle";
                            trans.PaymentCode = "50";
                            db.Entry(trans).State = EntityState.Modified;
                            db.SaveChanges();                           
                        }
                    }
                    else if (req.response_code == "01")
                    {
                        trans.TxStatus = 1;
                        trans.PaymentMessage = "Ngân hàng từ chối thanh toán: thẻ/tài khoản bị khóa.";
                        trans.PaymentCode = "01";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "02")
                    {
                        trans.TxStatus = 2;
                        trans.PaymentMessage = "Thông tin thẻ không hợp lệ.";
                        trans.PaymentCode = "02";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "03")
                    {
                        trans.TxStatus = 3;
                        trans.PaymentMessage = "Thẻ hết hạn.";
                        trans.PaymentCode = "03";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "04")
                    {
                        trans.TxStatus = 4;
                        trans.PaymentMessage = "Lỗi người mua hàng: Quá số lần cho phép. (Sai OTP, quá hạn mức trong ngày).";
                        trans.PaymentCode = "04";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "05" || req.response_code == "06")
                    {
                        trans.TxStatus = 5;
                        trans.PaymentMessage = "Không có trả lời của Ngân hàng.";
                        trans.PaymentCode = "05";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "07")
                    {
                        trans.TxStatus = 7;
                        trans.PaymentMessage = "Tài khoản không đủ tiền.";
                        trans.PaymentCode = "07";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "11" || req.response_code == "14" || req.response_code == "21")
                    {
                        trans.TxStatus = 11;
                        trans.PaymentMessage = "Lỗi OTP";
                        trans.PaymentCode = "11";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (req.response_code == "15" || req.response_code == "16" || req.response_code == "17" || req.response_code == "18" || req.response_code == "19" || req.response_code == "22" || req.response_code == "23")
                    {
                        trans.TxStatus = 15;
                        trans.PaymentMessage = "Lỗi thẻ";
                        trans.PaymentCode = "15";
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        trans.TxStatus = Int32.Parse(req.response_code);
                        trans.PaymentMessage = "Lỗi không xác định";
                        trans.PaymentCode = req.response_code;
                        db.Entry(trans).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    //Commit transation
                    OnePayCommitReq commitReq = new OnePayCommitReq();
                    commitReq.access_key = access_key;
                    commitReq.command = "close_transaction";
                    commitReq.trans_ref = trans.TxRefId;
                    string commitSignature = "access_key="+ access_key +"&command="+ commitReq.signature +"&trans_ref=" + commitReq.trans_ref;
                    commitSignature = Security.HmacSha256Digest(commitSignature, secret_key);
                    commitReq.signature = commitSignature;
                    List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                    values.Add(new KeyValuePair<string, string>("access_key", commitReq.access_key));
                    values.Add(new KeyValuePair<string, string>("command", commitReq.command));
                    values.Add(new KeyValuePair<string, string>("trans_ref", commitReq.trans_ref.ToString()));
                    values.Add(new KeyValuePair<string, string>("signature", commitReq.signature));
                    var content = new FormUrlEncodedContent(values);
                    log.Info(content.ReadAsStringAsync().Result);
                    HttpClient httpClient = new HttpClient();
                    HttpResponseMessage response = await httpClient.PostAsync(one_pay_begin_transaction_url, content);
                    httpClient.Dispose();
                    string result = response.Content.ReadAsStringAsync().Result;
                    log.Info("commit trans " + trans.TransactionId + "--" + result);
                    return Redirect(trans.ReturnUrl + "?error_code=" + trans.TxStatus + "&id=" + trans.TransactionId);
                }
                else
                {
                    //invalid signature
                    trans.TxStatus = 90;
                    trans.PaymentMessage = "Invalid Signature";
                    trans.PaymentCode = "90";
                    db.Entry(trans).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
            }
            return Ok();
        }
    }
}
