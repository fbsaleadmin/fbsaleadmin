﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;
using SaleManager.Data;
using log4net;
using Newtonsoft;
using Newtonsoft.Json;

namespace SaleManager.Controllers
{
    [Authorize]
    public class CustomersController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly ILog log = LogMaster.GetLogger("customer", "customer");

        [Route("api/customers/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Customer> customers = db.Customers;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    customers = customers.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    customers = customers.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    customers = customers.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    customers = customers.OrderBy("CustomerId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }              
                
                def.data = customers.Select(c => new CustomerDTO()
                {
                    Address = c.Address,
                    CreatedAt = c.CreatedAt,
                    CustomerId = c.CustomerId,
                    CustomerName = c.CustomerName,
                    DistrictId = c.DistrictId,
                    DOB = c.DOB,
                    Email = c.Email,
                    FacebookUserId = c.FacebookUserId,
                    Gender = c.Gender,
                    Level = c.Level,
                    Note = c.Note,
                    Phone = c.Phone,
                    ProvinceId = c.ProvinceId,
                    ShopId = c.ShopId,
                    Status = c.Status,
                    Tag = c.Tag,
                    UpdatedAt = c.UpdatedAt,
                    ID = c.ID,
                    Type = c.Type,
                    CustomerGroupId = c.CustomerGroupId,
                    TaxID = c.TaxID,
                    CustomerCode = c.CustomerCode,
                    FBUserAvatar = c.FBUserAvatar,
                    SearchQuery = c.SearchQuery,                    
                    FacebookMessengerId = c.FacebookMessengerId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Customers/5      
        public IHttpActionResult GetCustomer(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            CustomerDTO customer = db.Customers.Where(c => c.CustomerId == id).Select(c => new CustomerDTO()
            {
                Address = c.Address,
                CreatedAt = c.CreatedAt,
                CustomerId = c.CustomerId,
                CustomerName = c.CustomerName,
                DistrictId = c.DistrictId,
                DOB = c.DOB,
                Email = c.Email,
                FacebookUserId = c.FacebookUserId,
                Gender = c.Gender,
                Level = c.Level,
                Note = c.Note,
                Phone = c.Phone,
                ProvinceId = c.ProvinceId,
                ShopId = c.ShopId,
                Status = c.Status,
                Tag = c.Tag,
                UpdatedAt = c.UpdatedAt,
                ID = c.ID,
                Type = c.Type,
                CustomerGroupId = c.CustomerGroupId,
                TaxID = c.TaxID,
                CustomerCode = c.CustomerCode,
                FBUserAvatar = c.FBUserAvatar,
                SearchQuery = c.SearchQuery,                
                FacebookMessengerId = c.FacebookMessengerId
            }).FirstOrDefault();
            if (customer == null || customer.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = customer;
            return Ok(def);
        }

        // GET: api/Customers/5  
        [Route("api/customers/GetCustomerAction")]
        public IHttpActionResult GetCustomerAction(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Customers
                              //join p in db.Provinces on c.ProvinceId equals p.ProvinceId
                              //join d in db.Districts on c.DistrictId equals d.DistrictId
                          from d in db.Districts.Where(d => d.DistrictId == c.DistrictId).DefaultIfEmpty()
                          from p in db.Provinces.Where(p => p.ProvinceId == c.ProvinceId).DefaultIfEmpty()
                          from cg in db.CustomerGroups.Where(cg => cg.CustomerGroupId == c.CustomerGroupId).DefaultIfEmpty()
                          where c.ShopId == ShopId && c.Status != (int)Const.Status.DELETED
                          && c.CustomerId==id
                          select new SaleManager.Models.ShopCustomer
                          {
                              Address = c.Address,
                              CreatedAt = c.CreatedAt,
                              CustomerCode = c.CustomerCode,
                              CustomerId = c.CustomerId,
                              CustomerName = c.CustomerName,
                              District = new ShopDistrict()
                              {
                                  DistrictId = d != null ? d.DistrictId : -1,
                                  Name = d != null ? d.Name : "",
                                  ProvinceId = d != null ? d.ProvinceId : -1
                              },
                              DOB = c.DOB,
                              Email = c.Email,
                              FacebookUserId = c.FacebookUserId,
                              Gender = c.Gender,
                              ID = c.ID,
                              Level = c.Level,
                              Note = c.Note,
                              Phone = c.Phone,
                              Province = new ShopProvince()
                              {
                                  ProvinceId = p != null ? p.ProvinceId : -1,
                                  Name = p != null ? p.Name : ""
                              },
                              ShopId = c.ShopId,
                              Status = c.Status,
                              Tag = c.Tag,
                              TaxID = c.TaxID,
                              Type = c.Type,
                              UpdatedAt = c.UpdatedAt,
                              SearchQuery = c.SearchQuery,
                              FBUserAvatar = c.FBUserAvatar,
                              customerGroup = new ShopCustomerGroup()
                              {
                                  CustomerGroupId = cg != null ? cg.CustomerGroupId : -1,
                                  Name = cg != null ? cg.Name : ""
                              }
                          }
                         ).FirstOrDefault();

                if(result != null)
            {

            
                int CustomerId = (int)result.CustomerId;
                result.TotalOrder = db.Orders.Where(o => o.CustomerId == CustomerId && o.Type == (int)Const.OrderType.SALE).Count();
                result.TotalOrderPrice = db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;
                result.TotalPaid = db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;
                result.TotalDebt = db.CashFlows.Where(cf => cf.PaidTargetId == CustomerId && cf.PaidTargetType == "CUSTOMER").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault() != null ? db.CashFlows.Where(cf => cf.PaidTargetId == CustomerId && cf.PaidTargetType == "CUSTOMER").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault().Debt : 0;
            }
            if (result == null || result.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        // PUT: api/Customers/5               
        public IHttpActionResult PutCustomer(int id, CustomerDTO customer)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            log.Info(JsonConvert.SerializeObject(customer));
            DefaultResponse def = new DefaultResponse();
            //if (!ModelState.IsValid)
            //{
            //    def.meta = new Meta(400, "Bad Request");
            //    return Ok(def);
            //}

            if (id != customer.CustomerId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            if (customer.CustomerName == null || customer.CustomerName.Trim() == "" || customer.Phone == null || customer.Phone.Trim() == "")
            {
                def.meta = new Meta(210, "Invalid input");
                return Ok(def);
            }
            Customer current = db.Customers.Where(s => s.CustomerId == id).FirstOrDefault();
            string lastNote = current.Note != null ? current.Note.Trim() : "";
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            //Customer cpo = db.Customers.Where(cps => cps.ID == customer.ID).FirstOrDefault();
            List<Customer> cp = db.Customers.Where(cps => cps.Phone == customer.Phone && cps.ShopId == ShopId && cps.CustomerId != customer.CustomerId).ToList();
            if (cp.Count() > 0)
            {
                def.meta = new Meta(211, "EXIST PHONE");
                return Ok(def);
            }
            else
            {
                current.Address = customer.Address;
                current.CustomerName = customer.CustomerName;
                current.DistrictId = customer.DistrictId;
                current.DOB = customer.DOB;
                current.Email = customer.Email;
                current.FacebookUserId = customer.FacebookUserId;
                current.Gender = customer.Gender;
                current.Level = customer.Level;
                current.Phone = customer.Phone;
                current.ProvinceId = customer.ProvinceId;
                current.Status = customer.Status;
                current.Tag = customer.Tag;
                current.ID = customer.ID;
                current.Type = customer.Type;
                current.CustomerGroupId = customer.CustomerGroupId != -1 ? customer.CustomerGroupId : null;
                current.TaxID = customer.TaxID;
                current.UpdatedAt = DateTime.Now;
                current.FBUserAvatar = customer.FBUserAvatar;
                if (customer.Note != null && customer.Note.Trim().Length > 0 && !lastNote.Equals(customer.Note.Trim()))
                    current.Note = customer.Note;
                string signString = current.CustomerName + " " + current.CustomerCode;
                if (current.Email != null && current.Email.Trim().Length > 0)
                    signString += " " + current.Email;
                if (current.Phone != null && current.Phone.Trim().Length > 0)
                    signString += " " + current.Phone;
                if (current.FacebookUserId != null && current.FacebookUserId.Trim().Length > 0)
                    signString += " " + current.FacebookUserId;
                current.SearchQuery = Utils.unsignString(signString);
                //check if note change >> create new note
                if (customer.Note != null && customer.Note.Trim().Length > 0 && !lastNote.Equals(customer.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = current.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.CustomerId;
                    note.TargetType = "CUSTOMER";
                    current.Note = note.Content;
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
                //check if user sent message to page

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/Customers    
        public IHttpActionResult PostCustomer(CustomerDTO customer)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(cl => cl.Type == "ShopId").Select(cl => cl.Value).SingleOrDefault());
            log.Info(JsonConvert.SerializeObject(customer));
            DefaultResponse def = new DefaultResponse();

            //validate
            if (customer.CustomerName == null || customer.CustomerName.Trim() == "" || customer.Phone == null || customer.Phone.Trim() == "")
            {
                def.meta = new Meta(210, "Invalid input");
                return Ok(def);
            }

            //check exit phone
            Customer cp = db.Customers.Where(cps => cps.Phone == customer.Phone && customer.ShopId == ShopId).FirstOrDefault();
            if (cp != null)
            {
                def.meta = new Meta(211, "EXIST PHONE");
                return Ok(def);
            }
            //chekc exist fb id
            Customer c = db.Customers.Where(cus => cus.FacebookUserId == customer.FacebookUserId && cus.FacebookUserId != null && cus.ShopId == ShopId).FirstOrDefault();
            if (c == null)
            {
                c = new Customer();
                c.Address = customer.Address;
                c.CreatedAt = DateTime.Now;
                c.CustomerName = customer.CustomerName;
                c.DistrictId = customer.DistrictId;
                c.DOB = customer.DOB;
                c.Email = customer.Email;
                c.FacebookUserId = customer.FacebookUserId;
                c.Gender = customer.Gender;
                c.Level = customer.Level;
                if (customer.Note != null && customer.Note.Trim().Length > 0)
                    c.Note = customer.Note;
                c.Phone = customer.Phone;
                c.ProvinceId = customer.ProvinceId;
                c.ShopId = ShopId;
                c.Status = customer.Status;
                c.Tag = customer.Tag;
                c.ID = customer.ID;
                c.Type = customer.Type;
                if (customer.CustomerGroupId == null || customer.CustomerGroupId == -1)
                    customer.CustomerGroupId = null;
                else
                    c.CustomerGroupId = customer.CustomerGroupId;
                c.TaxID = customer.TaxID;
                c.UpdatedAt = DateTime.Now;
                //c.FBUserAvatar = customer.FBUserAvatar;
                if (c.FacebookUserId != null)
                    c.FBUserAvatar = "http://graph.facebook.com/" + c.FacebookUserId + "/picture?type=square";

                //find code 
                c.CustomerCode = generateCode();
                c.SearchQuery = Utils.unsignString(c.CustomerName + " " + c.CustomerCode + " " + c.Phone + " " + c.Email);
                db.Customers.Add(c);
                db.SaveChanges();
            }
            else
            {
                c.Address = customer.Address;
                c.CustomerName = customer.CustomerName;
                c.DistrictId = customer.DistrictId;
                c.DOB = customer.DOB;
                c.Email = customer.Email;
                c.Gender = customer.Gender;
                c.Level = customer.Level;
                if (customer.Note != null && customer.Note.Trim().Length > 0)
                    c.Note = customer.Note;
                c.Phone = customer.Phone;
                c.ProvinceId = customer.ProvinceId;
                c.ShopId = ShopId;
                c.Tag = customer.Tag;
                c.ID = customer.ID;
                c.Type = customer.Type;
                if (customer.CustomerGroupId == null || customer.CustomerGroupId == -1)
                    customer.CustomerGroupId = null;
                else
                    c.CustomerGroupId = customer.CustomerGroupId;
                c.TaxID = customer.TaxID;
                c.UpdatedAt = DateTime.Now;
                if (c.FacebookUserId != null)
                    c.FBUserAvatar = "http://graph.facebook.com/" + c.FacebookUserId + "/picture?type=square";
                c.SearchQuery = Utils.unsignString(c.CustomerName + " " + c.CustomerCode + " " + c.Phone + " " + c.Email);
                db.SaveChanges();
            }
            //save notes
            if (c.Note != null && c.Note.Trim().Length > 0)
            {
                Note note = new Note();
                note.Content = c.Note;
                note.CreatedAt = DateTime.Now;
                note.ShopId = ShopId;
                note.Status = 0;
                note.TargetId = c.CustomerId;
                note.TargetType = "CUSTOMER";
                db.Notes.Add(note);
                db.SaveChanges();
            }
            customer.CustomerId = c.CustomerId;
            customer.CreatedAt = c.CreatedAt;
            customer.UpdatedAt = c.UpdatedAt;

            def.meta = new Meta(200, "Success");
            def.data = customer;

            var identityt = (ClaimsIdentity)User.Identity;
            int EmployeeId = Int32.Parse(identityt.Claims.Where(ac => ac.Type == "EmployeeId").Select(ac => ac.Value).SingleOrDefault());
            Action action = new Action();
            action.ActionName = "thêm khách hàng";
            action.ActionType = "ADD";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = customer.CustomerId;
            action.TargetType = "CUSTOMER";
            db.Actions.Add(action);
            db.SaveChanges();


            return Ok(def);
        }

        private string generateCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Customers.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopCustomerPrefix + code;
            return code;
        }

        // DELETE: api/Customers/5    
        public IHttpActionResult DeleteCustomer(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int EmployeeId = Int32.Parse(identity.Claims.Where(c => c.Type == "EmployeeId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Customer customer = db.Customers.Find(id);
            if (customer == null || customer.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            customer.Status = 99;
            Action action = new Action();
            action.ActionName = "xóa khách hàng";
            action.ActionType = "DELETE";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = customer.CustomerId;
            action.TargetType = "CUSTOMER";
            db.Actions.Add(action);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpGet]
        [Route("api/customers/getCustomerCode")]
        public IHttpActionResult getCustomerCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            int count = db.Customers.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopCustomerPrefix + code;
            def.meta = new Meta(200, "Success");
            def.data = code;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/customers/{id}/orders")]
        public IHttpActionResult GetOrder(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from o in db.Orders
                              join c in db.Customers on o.CustomerId equals c.CustomerId
                              where c.CustomerId == id
                              orderby o.CreatedAt descending
                              select new CustomerOrder
                              {
                                  CreatedAt = (DateTime)o.CreatedAt,
                                  branch = db.Branches.Where(b => b.BranchId == o.BranchId).Select(b => new SaleManager.Data.Branch()
                                  {
                                      BranchId = b.BranchId,
                                      BranchName = b.BranchName
                                  }).FirstOrDefault(),
                                  DeliveryStatus = (int)o.DeliveryStatus,
                                  OrderCode = o.OrderCode,
                                  PaymentStatus = (int)o.PaymentStatus,
                                  OrderPrice = (decimal)o.OrderPrice,
                                  TotalPrice = (decimal)o.TotalPrice,
                                  OrderStatus = o.OrderStatus,
                                  employee = new Data.Employee()
                                  {
                                      EmployeeId = o.Employee.EmployeeId,
                                      EmployeeName = o.Employee.EmployeeName
                                  },
                                  saleEmployee = new Data.Employee()
                                  {
                                      EmployeeId = o.SaleEmployee.EmployeeId,
                                      EmployeeName = o.SaleEmployee.EmployeeName
                                  },
                                  Type = o.Type,
                                  OrderId = o.OrderId
                              }).AsEnumerable();
                int count = result != null ? result.Count() : 0;
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.metadata = new Metadata(count);
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/customers/{id}/cashflows")]
        public IHttpActionResult GetCashFlows(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from cf in db.CashFlows
                              where cf.PaidTargetId == id && cf.PaidTargetType == "CUSTOMER"
                              orderby cf.CreatedAt descending
                              select new Data.CashFlow
                              {
                                  Amount = cf.Amount,
                                  CreatedAt = cf.CreatedAt,
                                  Debt = cf.Debt,
                                  Type = cf.Type,
                                  TargetId = cf.TargetId,
                                  TargetType = cf.TargetType
                              }).AsEnumerable();
                int count = result != null ? result.Count() : 0;
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                //target
                var returnData = result.ToList();
                //select target
                for (int i = 0; i < returnData.Count; i++)
                {
                    int targetId = (int)returnData[i].TargetId;
                    switch (returnData[i].TargetType)
                    {
                        case "ORDER":
                            returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new Target()
                            {
                                TargetCode = c.OrderCode,
                                TargetId = c.OrderId
                            }).FirstOrDefault();
                            break;
                        case "CASH":
                            returnData[i].target = db.Cashes.Where(c => c.CashId == targetId).Select(c => new Target()
                            {
                                TargetId = c.CashId,
                                TargetCode = c.CashCode
                            }).FirstOrDefault();
                            break;
                        case "RETURN":
                            returnData[i].target = db.Orders.Where(c => c.OrderId == targetId && c.Type == (int)Const.OrderType.RETURN).Select(c => new Target()
                            {
                                TargetId = c.OrderId,
                                TargetCode = c.OrderCode
                            }).FirstOrDefault();
                            break;
                        case "CANCELED":
                            returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new Target()
                            {
                                TargetId = c.OrderId,
                                TargetCode = c.OrderCode
                            }).FirstOrDefault();
                            break;
                        default:
                            returnData[i].target = null;
                            break;
                    }
                }
                def.meta = new Meta(200, "Success");
                def.metadata = new Metadata(count);
                def.data = returnData;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/customers/{id}/eximproduct")]
        public IHttpActionResult getEximProduct(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            DefaultResponse def = new DefaultResponse();
            var result = (from e in db.Exims
                          join ep in db.EximProducts on e.EximId equals ep.EximId
                          where e.TargetId == id && e.TargetType == "CUSTOMER"
                          orderby e.CreatetAt descending
                          group e by new
                          {
                              e.EximId
                          } into gep
                          select new
                          {
                              Code = gep.FirstOrDefault().Code,
                              CreatetAt = gep.FirstOrDefault().CreatetAt,
                              Type = gep.FirstOrDefault().Type == 1 ? "Mua hàng" : "Trả hàng",
                              TotalProduct = gep.Sum(e => e.EximProducts.FirstOrDefault().Quantity),
                              Price = gep.FirstOrDefault().Price,
                              Discount = gep.FirstOrDefault().Discount
                          }).AsEnumerable();

            int count = result != null ? result.Count() : 0;
            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(count);
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/customers/{id}/notes")]
        public IHttpActionResult GetNotes(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from n in db.Notes
                              where n.TargetId == id && n.TargetType == "CUSTOMER" && n.Status != (int)Const.Status.DELETED
                              orderby n.CreatedAt descending
                              select new SaleManager.Data.CustomerNote
                              {
                                  Content = n.Content,
                                  CreatedAt = n.CreatedAt,
                                  CustomerId = n.TargetId,
                                  NoteId = n.NoteId
                              }).AsEnumerable();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/customers/{id}/notes")]
        public IHttpActionResult PostNotes(int id, SaleManager.Data.CustomerNote note)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            if (note.Content == null || note.Content.Trim() == "" || note.CustomerId == null)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Note n = new Note();
            n.Content = note.Content;
            n.CreatedAt = DateTime.Now;
            n.ShopId = ShopId;
            n.Status = 0;
            n.TargetId = note.CustomerId;
            n.TargetType = "CUSTOMER";
            db.Notes.Add(n);
            db.SaveChanges();
            note.NoteId = n.NoteId;

            def.meta = new Meta(200, "Success");
            def.data = note;

            return Ok(def);
        }

        [HttpPut]
        [Route("api/customers/{id}/notes/{noteId}")]
        public IHttpActionResult PutNotes(int id, int noteId, SaleManager.Data.CustomerNote note)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            if (note.Content == null || note.Content.Trim() == "" || note.CustomerId == null)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Note n = db.Notes.Find(note.NoteId);
            if (n == null || n.NoteId != noteId)
            {
                def.meta = new Meta(404, "Not found");
                return Ok(def);
            }

            n.Content = note.Content;
            db.Entry(n).State = EntityState.Modified;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            def.data = n;

            return Ok(def);
        }

        [HttpDelete]
        [Route("api/customers/{id}/notes/{noteId}")]
        public IHttpActionResult DeleteNotes(int id, int noteId)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            Note n = db.Notes.Find(noteId);
            if (n == null || n.NoteId != noteId)
            {
                def.meta = new Meta(404, "Not found");
                return Ok(def);
            }

            n.Status = (int)Const.Status.DELETED;
            db.Entry(n).State = EntityState.Modified;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        [HttpGet]
        [Route("api/customers/find/{fbid}")]
        public IHttpActionResult GetOrder(string fbid)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            CustomerDTO customer = db.Customers.Where(c => c.FacebookUserId == fbid && c.ShopId == ShopId).Select(c => new CustomerDTO()
            {
                Address = c.Address,
                CreatedAt = c.CreatedAt,
                CustomerId = c.CustomerId,
                CustomerName = c.CustomerName,
                DistrictId = c.DistrictId,
                DOB = c.DOB,
                Email = c.Email,
                FacebookUserId = c.FacebookUserId,
                Gender = c.Gender,
                Level = c.Level,
                Note = c.Note,
                Phone = c.Phone,
                ProvinceId = c.ProvinceId,
                ShopId = c.ShopId,
                Status = c.Status,
                Tag = c.Tag,
                UpdatedAt = c.UpdatedAt,
                ID = c.ID,
                Type = c.Type,
                CustomerGroupId = c.CustomerGroupId,
                TaxID = c.TaxID,
                CustomerCode = c.CustomerCode,
                FBUserAvatar = c.FBUserAvatar,
                SearchQuery = c.SearchQuery,                
                FacebookMessengerId = c.FacebookMessengerId,
                TotalConversation = db.Conversations.Where(cv => cv.FBUserId == c.FacebookUserId && cv.ShopId == ShopId).Count()
            }).FirstOrDefault();
            if (customer == null || customer.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = customer;
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return db.Customers.Count(e => e.CustomerId == id) > 0;
        }
    }
}