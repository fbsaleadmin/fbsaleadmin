﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;
using log4net;

namespace SaleManager.Controllers
{    
    public class UsersController : ApiController
    {
        private static readonly ILog log = LogMaster.GetLogger("fbsale", "fbsale");
        private SalesManagerEntities db = new SalesManagerEntities();
        // GET: api/Users with query  
        //[Authorize]
        //[HttpGet]
        //[Route("api/users/GetByPage")]
        //public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (paging != null)
        //    {               
        //        def.meta = new Meta(200, "Success");
        //        IQueryable<User> users;
        //        if (paging.order_by != null)
        //        {
        //            users = db.Users.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);                
        //        }
        //        else
        //        {
        //            users = db.Users.OrderBy("UserId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);               
        //        }
        //        if (paging.query != null)
        //            users = users.Where(HttpUtility.UrlDecode(paging.query));
        //        def.data = users.Select(u => new UserDTO()
        //        {
        //            CreatedAt = u.CreatedAt,
        //            Email = u.Email,
        //            FullName = u.FullName,
        //            Password = u.Password,
        //            Phone = u.Phone,
        //            Status = u.Status,
        //            UpdatedAt = u.UpdatedAt,
        //            UserFacebookId = u.UserFacebookId,
        //            UserFacebookToken = u.UserFacebookToken,
        //            UserId = u.UserId
        //        });
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //}

        // GET: api/Users/5 
        [Authorize]
        [HttpGet]
        [Route("api/users/{id}/shops")]
        public IHttpActionResult GetShops(string id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string UserId = identity.Claims.Where(c => c.Type == "UserId").Select(c => c.Value).SingleOrDefault();      
            DefaultResponse def = new DefaultResponse();
            try
            {
                if (!UserId.Equals("") && id.Equals(UserId))
                {
                    int iUserId = Int32.Parse(UserId);                    
                    var result = db.Shops.Where(s => s.UserId == iUserId).Select(s => new ShopDTO()
                    {
                        ShopId = s.ShopId,
                        Address = s.Address,
                        Phone = s.Phone,
                        ShopName = s.ShopName,
                        ShopOrderPrefix = s.ShopOrderPrefix,
                        ShopProductPrefix = s.ShopProductPrefix,
                        ShopUniqueName = s.ShopUniqueName,
                        UpdatedAt = s.UpdatedAt,
                        CreatedAt = s.CreatedAt,
                        UserId = s.UserId,
                        WorkTimeEnd = s.WorkTimeEnd,
                        WorkTimeStart = s.WorkTimeStart,
                        ShopCustomerPrefix = s.ShopCustomerPrefix
                    });
                    def.meta = new Meta(200, "Success");
                    def.data = result;
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                log.Info("get shops ex:" + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace);
                return Ok(def);
            }
        }

        [Authorize]
        public IHttpActionResult GetUser(int id)
        {
            DefaultResponse def = new DefaultResponse();
            UserDTO user = db.Users.Where(u => u.UserId == id).Select(u => new UserDTO()
            {
                CreatedAt = u.CreatedAt,
                Email = u.Email,
                FullName = u.FullName,
                Password = u.Password,
                Phone = u.Phone,
                Status = u.Status,
                UpdatedAt = u.UpdatedAt,
                UserFacebookId = u.UserFacebookId,
                UserFacebookToken = u.UserFacebookToken,
                UserId = u.UserId
            }).FirstOrDefault();
            if (user == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = user;
            return Ok(def);
        }

        [Authorize]
        [HttpPut]
        [Route("api/users/{userid}/updateToken")]
        // PUT: api/Users/5    
        public IHttpActionResult updateToken(int userid, LoginFacebookModel model)
        {
            DefaultResponse def = new DefaultResponse();

            User current = db.Users.Where(s => s.UserId == userid).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                //check exist                
                if (model.id == current.UserFacebookId || current.UserFacebookId == null || current.UserFacebookId == "")
                {
                    User exist = db.Users.Where(u => u.UserFacebookId == model.id).FirstOrDefault();
                    if (exist == null)
                    {
                        current.UserFacebookId = model.id;
                        current.UserFacebookToken = model.token;
                        db.Entry(current).State = EntityState.Modified;
                        try
                        {
                            db.SaveChanges();
                            def.meta = new Meta(200, "Success");
                            return Ok(def);
                        }
                        catch (DbUpdateConcurrencyException)
                        {
                            def.meta = new Meta(500, "Internal Server Error");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        current.UserFacebookToken = model.token;
                        db.Entry(current).State = EntityState.Modified;
                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(400, "Invalid shop owner!");
                    return Ok(def);
                }
            }  
        }

        [Authorize]
        // PUT: api/Users/5    
        public IHttpActionResult PutUser(int id, UserDTO user)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != user.UserId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            User current = db.Users.Where(s => s.UserId == id).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Email = user.Email;
                current.FullName = user.FullName;
                current.Password = user.Password;
                current.Phone = user.Phone;
                current.Status = user.Status;
                current.UpdatedAt = DateTime.Now;
                current.UserFacebookId = user.UserFacebookId;
                current.UserFacebookToken = user.UserFacebookToken;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/Users      
        public IHttpActionResult PostUser(UserDTO user)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            //check exist facebook userid
            if (user.UserFacebookId != null && !user.UserFacebookId.Trim().Equals(""))
            {
                int count = db.Users.Count(u => u.UserFacebookId.Trim() == user.UserFacebookId.Trim());
                if (count > 0)
                {
                    def.meta = new Meta(500, "Existing Facebook User Id");
                    return Ok(def);
                }
            }
            //check if email exist
            if (user.Email != null && !user.Email.Trim().Equals(""))
            {
                int count = db.Users.Count(u => u.Email.Trim() == user.Email.Trim());
                if (count > 0)
                {
                    def.meta = new Meta(500, "Existing Email");
                    return Ok(def);
                }
            }

            User current = new User();
            current.CreatedAt = DateTime.Now;
            current.Email = user.Email;
            current.FullName = user.FullName;
            current.Password = user.Password;
            current.Phone = user.Phone;
            current.Status = user.Status;
            current.UpdatedAt = DateTime.Now;
            current.UserFacebookId = user.UserFacebookId;
            current.UserFacebookToken = user.UserFacebookToken;
            current.Status = 0;
            current.IsThirdPartyLogin = user.IsThirdPartyLogin;
            db.Entry(current).State = EntityState.Added;
            db.Users.Add(current);
            db.SaveChanges();
            user.UserId = current.UserId;
            //create shop
            Shop shop = new Shop();
            shop.Address = null;
            shop.CreatedAt = DateTime.Now;
            shop.Phone = current.Phone;
            shop.ShopCategoryId = -1;
            shop.ShopName = "Cửa hàng của " + current.FullName;
            shop.ShopProductPrefix = "SP";
            shop.ShopOrderPrefix = "HD";
            shop.ShopUniqueName = null;
            shop.ShopCustomerPrefix = "KH";
            shop.ShopCashInPrefix = "PT";
            shop.ShopCashOutPrefix = "PC";
            shop.ShopExportPrefix = "PX";
            shop.ShopImportPrefix = "PN";
            shop.ShopVendorPrefix = "NCC";
            shop.ShopDVendorPrefix = "CVC";
            shop.UpdatedAt = DateTime.Now;
            shop.UserId = current.UserId;
            shop.WorkTimeEnd = null;
            shop.WorkTimeStart = null;
            db.Entry(shop).State = EntityState.Added;
            db.Shops.Add(shop);
            db.SaveChanges();
            //create cashtype
            //cash in 
            CashType cashIn = new CashType();
            cashIn.CashTypeCode = "PT";
            cashIn.CreatedAt = DateTime.Now;
            cashIn.DefaultValue = 0;
            cashIn.IsCustom = false;
            cashIn.Name = "Phiếu thu";
            cashIn.ShopId = shop.ShopId;
            cashIn.Type = 1;
            cashIn.UpdatedAt = DateTime.Now;
            db.CashTypes.Add(cashIn);
            db.SaveChanges();
            //cash out
            CashType cashOut = new CashType();
            cashOut.CashTypeCode = "PC";
            cashOut.CreatedAt = DateTime.Now;
            cashOut.DefaultValue = 0;
            cashOut.IsCustom = false;
            cashOut.Name = "Phiếu chi";
            cashOut.ShopId = shop.ShopId;
            cashOut.Type = 0;
            cashOut.UpdatedAt = DateTime.Now;
            db.CashTypes.Add(cashOut);
            db.SaveChanges();
            //config
            Config config = new Config();
            config.AutoCreateOrder = false;
            config.AutoHideComment = true;
            config.NewCommentLike = false;
            config.NewMessageNotification = true;
            config.NewMessagePriority = true;
            config.NotificationSound = true;
            config.ShopId = shop.ShopId;
            db.Configs.Add(config);
            db.SaveChanges();
            //cash type            
            Branch branch = new Branch();
            branch.Address = shop.Address;
            branch.ShopId = shop.ShopId;
            branch.Status = 0;
            branch.CreatedAt = DateTime.Now;
            branch.UpdatedAt = DateTime.Now;
            branch.Phone = shop.Phone;
            branch.IsMainBranch = true;
            branch.BranchName = shop.ShopName + " - " + "Chi nhánh chính";
            db.Entry(branch).State = EntityState.Added;
            db.Branches.Add(branch);
            db.SaveChanges();
            //end create shop
            //save employee
            Employee employee = new Employee();
            employee.UserId = user.UserId;
            employee.CreatedAt = DateTime.Now;           
            employee.Status = 0;
            employee.UpdatedAt = DateTime.Now;
            employee.GroupId = -1;
            employee.EmployeeName = user.FullName;
            employee.Password = user.Password;
            employee.Email = user.Email;
            employee.Group = "Admin";
            employee.ShopId = shop.ShopId;
            employee.BranchId = branch.BranchId;
            db.Entry(employee).State = EntityState.Added;
            db.Employees.Add(employee);
            db.SaveChanges();

            Data.EmployeeLogin _employee = db.Employees.Where(e => e.EmployeeId == employee.EmployeeId).Select(e => new Data.EmployeeLogin()
            {
                Address = e.Address,
                BranchId = e.BranchId,
                CreatedAt = e.CreatedAt,
                Email = e.Email,
                EmployeeId = e.EmployeeId,
                EmployeeName = e.EmployeeName,
                GroupId = e.GroupId,
                Password = e.Password,
                Phone = e.Phone,
                ShopId = e.ShopId,
                ShopName = e.Shop.ShopName,
                Status = e.Status,
                UpdatedAt = e.UpdatedAt,
                UserId = e.UserId,
                Group = e.Group
            }).FirstOrDefault();
            
            def.meta = new Meta(200, "Success");
            def.data = _employee;
            def.metadata = new Metadata(current.UserId);

            return Ok(def);
        }

        //[Authorize]
        //// DELETE: api/Users/5     
        //public IHttpActionResult DeleteUser(int id)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    User user = db.Users.Find(id);
        //    if (user == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    db.Users.Remove(user);
        //    db.SaveChanges();

        //    def.meta = new Meta(200, "Success"); 
        //    return Ok(def);
        //}

        [HttpGet]
        [Route("api/users/CheckExistEmail")]
        public IHttpActionResult CheckExistEmail(string email)
        {
            DefaultResponse def = new DefaultResponse();
            int count = db.Users.Count(u => u.Email.ToLower().Trim() == email.ToLower().Trim());
            if (count > 0)
            {
                def.meta = new Meta(201, "Existing Email");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.UserId == id) > 0;
        }
    }
}