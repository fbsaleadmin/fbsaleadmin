﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class TransfersController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        
        [HttpGet]
        [Route("api/transfers/{id}/products")]
        public IHttpActionResult getProducts(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from o in db.TransferProducts
                          join pa in db.ProductAttributes on o.ProductAttributeId equals pa.ProductAttributeId
                          join p in db.Products on pa.ProductId equals p.ProductId
                          where o.TransferId == id
                          orderby o.CreatedAt descending
                          select new SaleManager.Data.ShopTransferProduct
                          {
                              TransferId = o.TransferId,
                              TransferProductId = o.TransferProductId,
                              ProductId = p.ProductId,
                              ProductName = p.ProductName,
                              SubName = pa.SubName,
                              Price = o.Price,
                              TotalPrice = o.Price * o.Quantity,
                              Quantity = o.Quantity,
                              ProductAttributeId = o.ProductAttributeId,
                              ProductCode = pa.ProductCode     
                          }
                         ).AsEnumerable();
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        // PUT: api/Orders/5     
        public IHttpActionResult PutTransfer(int id, TransferDTO transfer)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != transfer.TransferId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Transfer current = db.Transfers.Where(s => s.TransferId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                string lastNote = current.Note != null ? current.Note.Trim() : "";
                if (transfer.Note != null && transfer.Note.Trim().Length > 0 && !lastNote.Equals(transfer.Note.Trim()))
                    current.Note = transfer.Note;
                if (transfer.Note != null && transfer.Note.Trim().Length > 0 && !lastNote.Equals(transfer.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = transfer.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.TransferId;
                    note.TargetType = "TRANSFER";                  
                    db.Notes.Add(note);
                    db.SaveChanges();
                }                
                db.Entry(current).State = EntityState.Modified;           
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.OrderId == id) > 0;
        }
    }
}