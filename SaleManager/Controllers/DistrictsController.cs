﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class DistrictsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/Districts
        public IHttpActionResult GetDistricts()
        {
            DefaultResponse def = new DefaultResponse();
            def.meta = new Meta(200, "Success");
            def.data = db.Districts.Select(p => new DistrictDTO()
            {
                Code = p.Code,
                Name = p.Name,
                Priority = p.Priority,
                ProvinceId = p.ProvinceId,
                DistrictId = p.DistrictId
            });
            return Ok(def);
        }

        [Route("api/districts/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<District> districts;
                if (paging.order_by != null)
                {
                    districts = db.Districts.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    districts = db.Districts.OrderBy("DistrictId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                if (paging.query != null)
                    districts = districts.Where(HttpUtility.UrlDecode(paging.query));
                def.data = districts.Select(p => new DistrictDTO()
                {
                    Code = p.Code,
                    Name = p.Name,
                    Priority = p.Priority,
                    ProvinceId = p.ProvinceId,
                    DistrictId = p.DistrictId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Districts/5
        public IHttpActionResult GetDistrict(int id)
        {
            DistrictDTO district = db.Districts.Where(d => d.DistrictId == id).Select(d => new DistrictDTO()
            {
                Code = d.Code,
                DistrictId = d.DistrictId,
                Name = d.Name,
                Priority = d.Priority,
                ProvinceId = d.ProvinceId
            }).FirstOrDefault();
            if (district == null)
            {
                return NotFound();
            }

            return Ok(district);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DistrictExists(int id)
        {
            return db.Districts.Count(e => e.DistrictId == id) > 0;
        }
    }
}