﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class CommentsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        //with reply
        [Route("api/comments/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());         
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Comment> comments = db.Comments;
                if (paging.query != null)
                {
                    paging.query = HttpUtility.UrlDecode(paging.query);
                    comments = comments.Where(paging.query);
                }      
                else
                {
                    def.meta = new Meta(404, "Not found");
                    return Ok(def);
                }
                int count = comments.Count();
                if (paging.order_by != null)
                {
                    comments = comments.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    comments = comments.OrderBy("UpdatedAt desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = comments.Select(c => new ShopComment()
                {
                    CommentId = c.CommentId,
                    Content = c.Content,
                    CreatedAt = c.CreatedAt,
                    CustomerId = c.CustomerId,
                    FacebookCommentId = c.FacebookCommentId,
                    FacebookStatus = c.FacebookStatus,
                    Photo = c.Photo,
                    PostId = c.PostId,
                    SenderId = c.SenderId,
                    SenderName = c.SenderName,
                    Status = c.Status,
                    UpdatedAt = c.UpdatedAt,
                    ConversationId = c.ConversationId,
                    replies = db.Replies.Where(r => r.CommentId == c.CommentId).Select(r => new ShopReply()
                    {
                        ReplyId = r.ReplyId,
                        CommentId = r.CommentId,
                        Content = r.Content,
                        CreatedAt = r.CreatedAt,
                        CustomerId = r.CustomerId,
                        FacebookCommentId = r.FacebookCommentId,
                        FacebookStatus = r.FacebookStatus,
                        Photo = r.Photo,
                        SenderId = r.SenderId,
                        SenderName = r.SenderName,
                        Status = r.Status,
                        UpdatedAt = r.UpdatedAt,
                        EmployeeId = r.EmployeeId
                    }).ToList()
                });
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Comments/5      
        public IHttpActionResult GetComment(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            ShopComment comment = db.Comments.Where(c => c.CommentId == id).Select(c => new ShopComment()
                {
                    CommentId = c.CommentId,
                    Content = c.Content,
                    CreatedAt = c.CreatedAt,
                    CustomerId = c.CustomerId,
                    FacebookCommentId = c.FacebookCommentId,
                    FacebookStatus = c.FacebookStatus,
                    Photo = c.Photo,
                    PostId = c.PostId,
                    SenderId = c.SenderId,
                    SenderName = c.SenderName,
                    Status = c.Status,
                    UpdatedAt = c.UpdatedAt,
                    ConversationId = c.ConversationId,
                    replies = db.Replies.Where(r => r.CommentId == c.CommentId).Select(r => new ShopReply()
                    {
                        ReplyId = r.ReplyId,
                        CommentId = r.CommentId,
                        Content = r.Content,
                        CreatedAt = r.CreatedAt,
                        CustomerId = r.CustomerId,
                        FacebookCommentId = r.FacebookCommentId,
                        FacebookStatus = r.FacebookStatus,
                        Photo = r.Photo,
                        SenderId = r.SenderId,
                        SenderName = r.SenderName,
                        Status = r.Status,
                        UpdatedAt = r.UpdatedAt,
                        EmployeeId = r.EmployeeId
                    }).ToList()
                }).FirstOrDefault();
            if (comment == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = comment;
            return Ok(def);
        }

        // PUT: api/Comments/5       
        //public IHttpActionResult PutComment(int id, CommentDTO comment)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    if (id != comment.CommentId)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //    Comment current = db.Comments.Where(a => a.CommentId == id).FirstOrDefault();
        //    if (current == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        current.Content = comment.Content;
        //        current.CustomerId = comment.CustomerId;
        //        current.FacebookCommentId = comment.FacebookCommentId;
        //        current.FacebookStatus = comment.FacebookStatus;
        //        current.Photo = comment.Photo;
        //        current.SenderName = comment.SenderName;
        //        current.Status = comment.Status;
        //        current.UpdatedAt = DateTime.Now;                
        //        db.Entry(current).State = EntityState.Modified;
        //        try
        //        {
        //            db.SaveChanges();
        //            def.meta = new Meta(200, "Success");
        //            return Ok(def);
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            def.meta = new Meta(500, "Internal Server Error");
        //            return Ok(def);
        //        }
        //    } 
        //}

        //// POST: api/Comments       
        //public IHttpActionResult PostComment(CommentDTO comment)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    Comment c = new Comment();
        //    c.Content = comment.Content;
        //    c.CreatedAt = DateTime.Now;
        //    c.CustomerId = comment.CustomerId;
        //    c.FacebookCommentId = comment.FacebookCommentId;
        //    c.FacebookStatus = comment.FacebookStatus;
        //    c.Photo = comment.Photo;
        //    c.PostId = comment.PostId;
        //    c.SenderId = comment.SenderId;
        //    c.SenderName = comment.SenderName;
        //    c.Status = comment.Status;
        //    c.UpdatedAt = DateTime.Now;
        //    c.ConversationId = comment.ConversationId;
        //    db.Comments.Add(c);
        //    db.SaveChanges();
        //    comment.CommentId = c.CommentId;

        //    def.meta = new Meta(200, "Success");
        //    def.data = comment;

        //    return Ok(def);
        //}

        //// DELETE: api/Comments/5
        //public IHttpActionResult DeleteComment(int id)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    Comment comment = db.Comments.Find(id);
        //    if (comment == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
        //    //delete all replies belong to comment
        //    db.Replies.RemoveRange(comment.Replies);
        //    db.Comments.Remove(comment);
        //    db.SaveChanges();

        //    def.meta = new Meta(200, "Success");
        //    return Ok(def);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CommentExists(int id)
        {
            return db.Comments.Count(e => e.CommentId == id) > 0;
        }
    }
}