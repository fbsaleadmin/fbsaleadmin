﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class AttachmentsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        //[Route("api/attachments/GetByPage")]
        //public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        //{
        //    var identity = (ClaimsIdentity)User.Identity;
        //    string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
        //    DefaultResponse def = new DefaultResponse();
        //    if (paging != null)
        //    {
        //        def.meta = new Meta(200, "Success");
        //        IQueryable<Attachment> attachments = db.Attachments;
        //        if (paging.query != null)
        //        {
        //            paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
        //            attachments = attachments.Where(paging.query);
        //        }
        //        else
        //        {
        //            paging.query = "ShopId=" + ShopId;
        //            attachments = attachments.Where(paging.query);
        //        }
        //        if (paging.order_by != null)
        //        {
        //            attachments = attachments.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        else
        //        {
        //            attachments = attachments.OrderBy("AttachmentId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        def.data = attachments.Select(a => new AttachmentDTO()
        //        {
        //            AttachmentId = a.AttachmentId,
        //            CreatedAt = a.CreatedAt,
        //            MessageId = a.MessageId,
        //            Type = a.Type,
        //            Url = a.Url
        //        });
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //}

        //// GET: api/Attachments/5
        //[ResponseType(typeof(Attachment))]
        //public IHttpActionResult GetAttachment(int id)
        //{
        //    var identity = (ClaimsIdentity)User.Identity;
        //    string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
        //    DefaultResponse def = new DefaultResponse();
        //    AttachmentDTO attachment = db.Attachments.Where(a => a.AttachmentId == id).Select(a => new AttachmentDTO()
        //    {
        //        AttachmentId = a.AttachmentId,
        //        CreatedAt = a.CreatedAt,
        //        MessageId = a.MessageId,
        //        Type = a.Type,
        //        Url = a.Url
        //    }).FirstOrDefault();
        //    if (attachment == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    def.meta = new Meta(200, "Success");
        //    def.data = attachment;
        //    return Ok(def);
        //}

        //// PUT: api/Attachments/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutAttachment(int id, AttachmentDTO attachment)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    if (id != attachment.AttachmentId)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //    Attachment current = db.Attachments.Where(a => a.AttachmentId == id).FirstOrDefault();
        //    if (current == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        current.Type = attachment.Type;
        //        current.Url = attachment.Url;
        //        db.Entry(current).State = EntityState.Modified;
        //        try
        //        {
        //            db.SaveChanges();
        //            def.meta = new Meta(200, "Success");
        //            return Ok(def);
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            def.meta = new Meta(500, "Internal Server Error");
        //            return Ok(def);
        //        }
        //    } 
        //}

        //// POST: api/Attachments
        //[ResponseType(typeof(Attachment))]
        //public IHttpActionResult PostAttachment(AttachmentDTO attachment)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    Attachment a = new Attachment();
        //    a.CreatedAt = DateTime.Now;
        //    a.MessageId = attachment.MessageId;
        //    a.Type = attachment.Type;
        //    a.Url = attachment.Url;
        //    db.Attachments.Add(a);
        //    db.SaveChanges();
        //    attachment.AttachmentId = a.AttachmentId;

        //    def.meta = new Meta(200, "Success");
        //    def.data = attachment;

        //    return Ok(def);
        //}

        //// DELETE: api/Attachments/5
        //[ResponseType(typeof(Attachment))]
        //public IHttpActionResult DeleteAttachment(int id)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    Attachment attachment = db.Attachments.Find(id);
        //    if (attachment == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
    
        //    db.Attachments.Remove(attachment);
        //    db.SaveChanges();

        //    def.meta = new Meta(200, "Success");
        //    return Ok(def);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AttachmentExists(int id)
        {
            return db.Attachments.Count(e => e.AttachmentId == id) > 0;
        }
    }
}