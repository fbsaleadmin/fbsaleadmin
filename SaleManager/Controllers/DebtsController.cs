﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    public class DebtsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/debts/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Debt> debts = db.Debts;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    debts = debts.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    debts = debts.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    debts = debts.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    debts = debts.OrderBy("DebtId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }               
                def.data = debts.Select(d => new DebtDTO()
                {
                    CreatedAt = d.CreatedAt,
                    DebtId = d.DebtId,
                    DebtType = d.DebtType,
                    Paid = d.Paid,
                    ShopId = d.ShopId,
                    Status = d.Status,
                    TargetId = d.TargetId,
                    TargetType = d.TargetType,
                    Title = d.Title,
                    Total = d.Total,
                    UpdatedAt = d.UpdatedAt,
                    Remain = d.Remain
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Debts/5
        [ResponseType(typeof(Debt))]
        public IHttpActionResult GetDebt(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            DebtDTO debt = db.Debts.Where(c => c.DebtId == id).Select(d => new DebtDTO()
            {
                CreatedAt = d.CreatedAt,
                DebtId = d.DebtId,
                DebtType = d.DebtType,
                Paid = d.Paid,
                ShopId = d.ShopId,
                Status = d.Status,
                TargetId = d.TargetId,
                TargetType = d.TargetType,
                Title = d.Title,
                Total = d.Total,
                UpdatedAt = d.UpdatedAt,
                Remain = d.Remain
            }).FirstOrDefault();
            if (debt == null || debt.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = debt;
            return Ok(def);
        }

        // PUT: api/Debts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDebt(int id, DebtDTO debt)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != debt.DebtId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Debt current = db.Debts.Where(s => s.DebtId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.DebtType = debt.DebtType;
                current.Paid = debt.Paid;
                current.Status = debt.Status;
                current.TargetId = debt.TargetId;
                current.TargetType = debt.TargetType;
                current.Title = debt.Title;
                current.Total = debt.Total;
                current.UpdatedAt = DateTime.Now;
                current.Remain = debt.Remain;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Debts
        [ResponseType(typeof(Debt))]
        public IHttpActionResult PostDebt(DebtDTO debt)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Debt d = new Debt();
            d.CreatedAt = DateTime.Now;
            d.DebtType = debt.DebtType;
            d.Paid = debt.Paid;
            d.ShopId = ShopId;
            d.Status = debt.Status;
            d.TargetId = debt.TargetId;
            d.TargetType = debt.TargetType;
            d.Title = debt.Title;
            d.Total = debt.Total;
            d.UpdatedAt = DateTime.Now;
            d.Remain = debt.Remain;

            db.Debts.Add(d);
            db.SaveChanges();
            debt.DebtId = d.DebtId;

            def.meta = new Meta(200, "Success");
            def.data = debt;

            return Ok(def);
        }

        // DELETE: api/Debts/5
        [ResponseType(typeof(Debt))]
        public IHttpActionResult DeleteDebt(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Debt debt = db.Debts.Find(id);
            if (debt == null || debt.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            debt.Status = 99;
            //db.Debts.Remove(debt);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DebtExists(int id)
        {
            return db.Debts.Count(e => e.DebtId == id) > 0;
        }
    }
}