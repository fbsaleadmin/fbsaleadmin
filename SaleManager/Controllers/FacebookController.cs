﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using SaleManager.Models;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Text;
using log4net;
using Firebase;
using Firebase.Database;
using System.Text.RegularExpressions;

namespace SaleManager.Controllers
{
    public class FacebookController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly ILog log = LogMaster.GetLogger("facebook", "facebook");
        private static readonly string facebook_app_id = "313601012371988";
        private static readonly string facebook_app_secret_key = "a993e6c800e78daf7f299f15e933b588";
        private static FirebaseClient client;
        private static readonly string firebase_secret = "48SnJ3RsvUMc1Ih8OvUgUNkbNFIy3JWzbVDzN8QJ";
        private static readonly string firebase_url = "https://facebook-sales.firebaseio.com/";
        private static readonly string PhoneRegex = @"(09|012)(\d{8,8})";

        [Route("api/facebook/webhook")]
        public HttpResponseMessage GetFacebook([FromUri(Name = "hub.mode")]string mode, [FromUri(Name = "hub.challenge")]string challenge, [FromUri(Name = "hub.verify_token")]string verifyToken)
        {
            if (verifyToken.Equals("12@fsale!abc"))
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(challenge);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
            else
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent("");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
        }

        [Route("api/facebook/webhook")]
        public async Task<IHttpActionResult> PostFacebook(HttpRequestMessage request)
        {
            try
            {
                StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
                string req = reader.ReadToEnd().Trim();
                log.Info("webhook request :" + req);
                var tasks = new[]
                {
                    Task.Run(() => ProcessWebhook(req))
                };

                return Ok();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "-" + ex.StackTrace);
                return Ok();
            }

        }

        private async void ProcessWebhook(string req)
        {
            WebhookMessage message = JsonConvert.DeserializeObject<WebhookMessage>(req);
            try
            {
                if (message.entry != null && message.entry.Count > 0)
                {
                    //message
                    #region message
                    if (message.entry[0].messaging != null)
                    {
                        //insert to message
                        for (int i = 0; i < message.entry.Count; i++)
                        {
                            EntryMessage entry = message.entry[i];
                            //check if page exist                        
                            Page page = db.Pages.Where(p => p.PageFacebookId == entry.id && p.IsSubscribed == true).FirstOrDefault();
                            if (page != null)
                            {
                                //check if conversation exist
                                for (int j = 0; j < entry.messaging.Count; j++)
                                {
                                    //ignore if no content or emotion 
                                    if ((entry.messaging[j].message == null || entry.messaging[j].message.text == null) && entry.messaging[j].message.attachments == null)
                                    {
                                        continue;
                                    }
                                    bool isEmptyMessage = false;
                                    if (entry.messaging[j].message == null || entry.messaging[j].message.text == null)
                                    {
                                        isEmptyMessage = true;
                                    }
                                    string sender_id = entry.messaging[j].sender.id;
                                    string recipient_id = entry.messaging[j].recipient.id;
                                    #region detect phone
                                    bool hasPhone = false;
                                    string phone = "NaN";
                                    if (!isEmptyMessage && entry.messaging[j].message.text != null)
                                    {
                                        Match m = Regex.Match(entry.messaging[j].message.text.Replace("-", "").Replace(" ", ""), PhoneRegex);
                                        if (m.Success)
                                        {
                                            hasPhone = true;
                                            phone = m.Groups[1].Value;
                                            phone += m.Groups[2].Value;
                                        }
                                        //phone = regex.Matches(entry.messaging[j].message.text.Replace("-", "").Replace(" ", ""))[0] != null ? regex.Matches(entry.messaging[j].message.text.Replace("-", "").Replace(" ", ""))[0].Value : "";
                                    }
                                    //find real facebook user id
                                    HttpClient httpClient = new HttpClient();
                                    HttpResponseMessage response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/conversations?fields=senders,messages.limit(10)&access_token=" + page.PageFacebookToken);
                                    httpClient.Dispose();
                                    string result = response.Content.ReadAsStringAsync().Result;
                                    log.Info(result);
                                    FBConversationResponse resp = JsonConvert.DeserializeObject<FBConversationResponse>(result);
                                    string realFbId = "";
                                    string realFBName = "";
                                    string fbmid = entry.messaging[j].message.mid;
                                    if (resp != null && resp.data != null && resp.data.Count > 0)
                                    {
                                        for (int k = 0; k < resp.data.Count; k++)
                                        {
                                            for (int l = 0; l < resp.data[k].messages.data.Count; l++)
                                            {
                                                if (resp.data[k].messages.data[l].id.Contains(fbmid))
                                                {
                                                    string id = "";
                                                    string name = "";
                                                    for (int m = 0; m < resp.data[k].senders.data.Count; m++)
                                                    {
                                                        if (resp.data[k].senders.data[m].id != page.PageFacebookId)
                                                        {
                                                            id = resp.data[k].senders.data[m].id;
                                                            name = resp.data[k].senders.data[m].name;
                                                            break;
                                                        }
                                                    }
                                                    realFbId = id;
                                                    realFBName = name;
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    #endregion
                                    //check if conversation exist
                                    Conversation conversation = db.Conversations.Where(c => (c.FacebookUserId == sender_id || c.FacebookUserId == recipient_id || c.FBUserId == realFbId) && c.Type == 0 && c.ShopId == page.ShopId && c.PageId == page.PageId).FirstOrDefault();
                                    if (conversation == null)
                                    {
                                        string userfbid = entry.messaging[j].sender.id;
                                        //create new conversation
                                        conversation = new Conversation();
                                        conversation.CreatedAt = DateTime.Now;
                                        conversation.FacebookUserId = entry.messaging[j].sender.id;
                                        conversation.FBUserId = realFbId;
                                        conversation.Name = "";
                                        conversation.Note = "";
                                        conversation.PageId = page.PageId;
                                        conversation.ShopId = page.ShopId;
                                        conversation.Status = (int)Const.ConversationStatus.NOT_READ;
                                        conversation.Tag = "";
                                        conversation.Type = 0;
                                        conversation.UpdatedAt = DateTime.Now;
                                        conversation.LastMessageTime = DateTime.Now;
                                        conversation.LastMessage = entry.messaging[j].message.text;
                                        conversation.SearchQuery = Utils.unsignString(conversation.LastMessage);
                                        if (hasPhone)
                                            conversation.Phone = phone;
                                        //get user info
                                        conversation.Name = realFBName;
                                        conversation.UserAvatar = "http://graph.facebook.com/" + realFbId + "/picture?type=square";
                                        conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                        if (!isEmptyMessage)
                                            conversation.LastMessage = entry.messaging[j].message.text;
                                        else
                                            conversation.LastMessage = conversation.Name + " đã gửi ảnh cho bạn";
                                        db.Entry(conversation).State = EntityState.Added;
                                        db.Conversations.Add(conversation);
                                    }
                                    else
                                    {
                                        if (!isEmptyMessage)
                                            conversation.LastMessage = entry.messaging[j].message.text;
                                        else
                                            conversation.LastMessage = conversation.Name + " đã gửi ảnh cho bạn";
                                        conversation.LastMessageTime = DateTime.Now;
                                        conversation.UpdatedAt = DateTime.Now;
                                        conversation.Status = (int)Const.ConversationStatus.NOT_READ;
                                        if (hasPhone && (conversation.Phone == null || conversation.Phone == ""))
                                            conversation.Phone = phone;
                                        conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                        //request name if user change name or not exist
                                        conversation.Name = realFBName;
                                        conversation.FBUserId = realFbId;
                                        conversation.UserAvatar = "http://graph.facebook.com/" + realFbId + "/picture?type=square";
                                        conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                        db.Entry(conversation).State = EntityState.Modified;
                                    }
                                    //map for customer
                                    if (conversation.CustomerId == null || conversation.CustomerId == -1)
                                    {
                                        //map customer
                                        Customer customer = db.Customers.Where(c => c.FacebookUserId == conversation.FBUserId && c.ShopId == page.ShopId).FirstOrDefault();
                                        if (customer != null)
                                        {
                                            conversation.CustomerId = customer.CustomerId;
                                        }
                                    }

                                    //check fb conversations last 10 conversations
                                    //if (conversation.FBUserId == null || conversation.FBUserId.Trim().Length == 0)
                                    //{
                                    //    HttpClient httpClient = new HttpClient();
                                    //    HttpResponseMessage response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/conversations?fields=senders,messages.limit(5)&access_token=" + page.PageFacebookToken);
                                    //    httpClient.Dispose();
                                    //    string result = response.Content.ReadAsStringAsync().Result;
                                    //    log.Info("getconversations :" + result);
                                    //    FBConversationResponse resp = JsonConvert.DeserializeObject<FBConversationResponse>(result);
                                    //    if (resp != null && resp.data != null && resp.data.Count > 0)
                                    //    {
                                    //        for (int k = 0; k < resp.data.Count; k++)
                                    //        {
                                    //            for (int l = 0; l < resp.data[k].messages.data.Count; l++)
                                    //            {
                                    //                if (resp.data[k].messages.data[l].id.Contains(fbmid))
                                    //                {
                                    //                    string id = "";
                                    //                    for (int m = 0; m < resp.data[k].senders.data.Count; m++)
                                    //                    {
                                    //                        if (resp.data[k].senders.data[m].id != page.PageFacebookId)
                                    //                        {
                                    //                            id = resp.data[k].senders.data[m].id;
                                    //                            break;
                                    //                        }
                                    //                    }
                                    //                    conversation.FBUserId = id;
                                    //                    break;
                                    //                }
                                    //            }                                          
                                    //        }
                                    //    }
                                    //}
                                    db.SaveChanges();
                                    //send notify                                
                                    var firebaseClient = new FirebaseClient(
                                      firebase_url,
                                      new FirebaseOptions
                                      {
                                          AuthTokenAsyncFactory = () => Task.FromResult(firebase_secret)
                                      });
                                    if (client == null)
                                    {
                                        client = new FirebaseClient(
                                                            firebase_url,
                                                            new FirebaseOptions
                                                            {
                                                                AuthTokenAsyncFactory = () => Task.FromResult(firebase_secret)
                                                            });
                                    }
                                    var root = client.Child("Messenger/" + conversation.ShopId + "/" + conversation.ConversationId);
                                    //get image
                                    List<string> images = new List<string>();
                                    if (entry.messaging[j].message.attachments != null && entry.messaging[j].message.attachments.Count > 0)
                                    {
                                        //has image
                                        foreach (Attachments a in entry.messaging[j].message.attachments)
                                        {
                                            images.Add(a.payload.url);
                                        }
                                    }
                                    await root.PutAsync(new Messenger()
                                    {
                                        Content = entry.messaging[j].message.text,
                                        ConversationId = conversation.ConversationId,
                                        Receiver = entry.messaging[j].recipient.id,
                                        Sender = entry.messaging[j].sender.id,
                                        Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                        images = images,
                                        SenderName = conversation.Name
                                    });
                                    //create message
                                    //check if mid exist
                                    string mid = entry.messaging[j].message.mid;
                                    int count = db.Messages.Where(m => m.FacebookMessageId == mid).Count();
                                    if (count == 0)
                                    {
                                        //insert
                                        Message _message = new Message();
                                        _message.ConversationId = conversation.ConversationId;
                                        _message.Content = entry.messaging[j].message.text != null ? entry.messaging[j].message.text : "";
                                        _message.CreatedAt = DateTime.Now;
                                        _message.FacebookMessageId = entry.messaging[j].message.mid;
                                        _message.FacebookStatus = 0;
                                        _message.IsBotMessage = false;
                                        _message.RecipientId = entry.messaging[j].recipient.id;
                                        _message.RecipientName = page.PageName;
                                        _message.SenderId = entry.messaging[j].sender.id;
                                        _message.SenderName = conversation.Name;
                                        _message.Sequence = entry.messaging[j].message.seq;
                                        _message.Status = 0;
                                        _message.HasAttachment = entry.messaging[j].message.attachments != null ? true : false;
                                        _message.Subject = "";
                                        _message.UpdatedAt = Utils.UnixTimeStampMilisecondToDateTime(entry.messaging[j].timestamp);
                                        db.Messages.Add(_message);
                                        db.SaveChanges();
                                        //get attchment
                                        if (entry.messaging[j].message.attachments != null && entry.messaging[j].message.attachments.Count > 0)
                                        {
                                            _message.HasAttachment = true;
                                            foreach (Attachments a in entry.messaging[j].message.attachments)
                                            {
                                                //process
                                                Attachment att = new Attachment();
                                                att.CreatedAt = DateTime.Now;
                                                att.MessageId = _message.MessageId;
                                                att.Type = a.type;
                                                att.Url = a.payload.url;
                                                db.Attachments.Add(att);
                                                db.SaveChanges();
                                            }
                                        }
                                        #region check bot
                                        Config config = db.Configs.Where(c => c.ShopId == page.ShopId).FirstOrDefault();
                                        if (config != null)
                                        {
                                            if (config.AllowBotMessage.HasValue && config.AllowBotMessage.Value)
                                            {
                                                //check template
                                                //find all template
                                                var templates = db.Templates.Where(t => t.ShopId == page.ShopId && t.PageId == page.PageId).ToList();
                                                if (templates != null && templates.Count > 0)
                                                {
                                                    string cleanMessage = Utils.unsignString(_message.Content);
                                                    string answer = "";
                                                    for (int k = 0; k < templates.Count; k++)
                                                    {
                                                        var temp = templates[k];
                                                        if (temp.Mark != null && temp.Mark.Trim() != "")
                                                        {
                                                            if (temp.Mark.Contains("," + cleanMessage + ","))
                                                            {
                                                                answer = temp.Content;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    if (answer != "")
                                                    {
                                                        //reply
                                                        NewSendMessageReq sendMessageReq = new NewSendMessageReq();
                                                        sendMessageReq.conversation_id = conversation.ConversationId;
                                                        sendMessageReq.employee_id = -1;
                                                        sendMessageReq.image_url = null;
                                                        sendMessageReq.message = answer;
                                                        autoReplyMessage(sendMessageReq);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region feed and conversation
                    else
                    {
                        //feed or conversation
                        WebhookFeed messageFeed = JsonConvert.DeserializeObject<WebhookFeed>(req);
                        if (messageFeed != null)
                        {
                            //chec kif post exist
                            if (messageFeed.entry != null && messageFeed.entry.Count > 0)
                            {
                                //check if page exist
                                for (int i = 0; i < messageFeed.entry.Count; i++)
                                {
                                    EntryFeed entry = messageFeed.entry[i];
                                    Page page = db.Pages.Where(p => p.PageFacebookId == entry.id && p.IsSubscribed == true).FirstOrDefault();
                                    //changes
                                    if (page != null)
                                    {
                                        for (int j = 0; j < entry.changes.Count; j++)
                                        {
                                            #region feed
                                            if (entry.changes[j].field == "feed" && entry.changes[j].value.verb == "add" && entry.changes[j].value.item == "comment")
                                            {
                                                //if ((entry.changes[j].value.message == null || entry.changes[j].value.message == "") && (entry.changes[j].value.photo == null))
                                                //{
                                                //    continue;
                                                //}
                                                bool isMessageEmpty = false;
                                                if (entry.changes[j].value.message == null || entry.changes[j].value.message == "")
                                                    isMessageEmpty = true;
                                                #region detect phone
                                                bool hasPhone = false;
                                                string phone = "NaN";
                                                if (!isMessageEmpty && entry.changes[j].value.message != null)
                                                {
                                                    Match m = Regex.Match(entry.changes[j].value.message.Replace("-", "").Replace(" ", ""), PhoneRegex);
                                                    if (m.Success)
                                                    {
                                                        hasPhone = true;
                                                        phone = m.Groups[1].Value;
                                                        phone += m.Groups[2].Value;
                                                    }
                                                }
                                                #endregion
                                                #region check sticker
                                                if (isMessageEmpty && entry.changes[j].value.photo == null)
                                                {
                                                    //user send sticker
                                                    HttpClient httpClient = new HttpClient();
                                                    HttpResponseMessage response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + entry.changes[j].value.comment_id + "?fields=attachment&access_token=" + page.PageFacebookToken);
                                                    httpClient.Dispose();
                                                    string result = response.Content.ReadAsStringAsync().Result;
                                                    log.Info(result);
                                                    FBOnlyAttachmentResp facebookAttResp = JsonConvert.DeserializeObject<FBOnlyAttachmentResp>(result);
                                                    if (facebookAttResp != null && facebookAttResp.id != null)
                                                    {
                                                        entry.changes[j].value.photo = facebookAttResp.attachment.media.image.src;
                                                    }
                                                }
                                                #endregion
                                                string post_id = entry.changes[j].value.post_id;
                                                string sender_id = entry.changes[j].value.sender_id;
                                                Post post = db.Posts.Where(p => p.FacebookPostId == post_id && p.ShopId == page.ShopId).FirstOrDefault();
                                                List<string> images = new List<string>();
                                                if (entry.changes[j].value.photo != null && entry.changes[j].value.photo != "")
                                                    images.Add(entry.changes[j].value.photo);

                                                if (post == null)
                                                {
                                                    //create new post
                                                    post = new Post();
                                                    post.Content = "";
                                                    post.CreatedDate = DateTime.Now;
                                                    post.FacebookPostId = post_id;
                                                    post.FacebookPostUrl = "https://facebook.com/" + post_id;
                                                    post.PageId = page.PageId;
                                                    post.ShopId = page.ShopId;
                                                    post.Status = 0;
                                                    post.Title = "";
                                                    post.Image = "";//a
                                                    post.UpdatedAt = DateTime.Now;
                                                    post.ShowComment = 0;
                                                    //fecth post data
                                                    HttpClient httpClient = new HttpClient();
                                                    HttpResponseMessage response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + post.FacebookPostId + "?fields=full_picture,message,story&access_token=313601012371988|a993e6c800e78daf7f299f15e933b588");
                                                    httpClient.Dispose();
                                                    string result = response.Content.ReadAsStringAsync().Result;
                                                    //log.Info(result);
                                                    FacebookPostResp facebookPostResp = JsonConvert.DeserializeObject<FacebookPostResp>(result);
                                                    if (facebookPostResp != null && facebookPostResp.id != null)
                                                    {
                                                        post.Content = facebookPostResp.message != null ? facebookPostResp.message : facebookPostResp.story;
                                                        post.Image = facebookPostResp.full_picture;
                                                    }
                                                    db.Posts.Add(post);
                                                    db.SaveChanges();
                                                }

                                                //send notify
                                                var firebaseClient = new FirebaseClient(
                                                  firebase_url,
                                                  new FirebaseOptions
                                                  {
                                                      AuthTokenAsyncFactory = () => Task.FromResult(firebase_secret)
                                                  });
                                                if (client == null)
                                                {
                                                    client = new FirebaseClient(
                                                                        firebase_url,
                                                                        new FirebaseOptions
                                                                        {
                                                                            AuthTokenAsyncFactory = () => Task.FromResult(firebase_secret)
                                                                        });
                                                }
                                                Config config = db.Configs.Where(c => c.ShopId == page.ShopId).FirstOrDefault();
                                                //check if someone reply in other's comment
                                                bool isReply = false;
                                                string parent_id = entry.changes[j].value.parent_id;
                                                string fbcomment_id = entry.changes[j].value.comment_id;
                                                if (!parent_id.Contains(page.PageFacebookId))
                                                {
                                                    isReply = true;
                                                    var existComment = db.Replies.Where(c => c.FacebookCommentId == parent_id && c.SenderId != page.PageFacebookId).FirstOrDefault();
                                                    if (existComment != null)
                                                        if (existComment.SenderId != sender_id)
                                                        {
                                                            //ignore
                                                            continue;
                                                        }
                                                }
                                                //ignore page manually commment
                                                if (page.PageFacebookId == entry.changes[j].value.sender_id)
                                                {
                                                    if (!isReply)
                                                    {
                                                        //page manually comment
                                                        //check conversation
                                                        try
                                                        {
                                                            Conversation conv = db.Conversations.Where(c => c.FacebookPostId == post_id && c.FacebookCommentId == fbcomment_id && c.FacebookUserId == page.PageFacebookId).FirstOrDefault();
                                                            if (conv == null)
                                                            {
                                                                conv = new Conversation();
                                                                conv = new Conversation();
                                                                conv.CreatedAt = DateTime.Now;
                                                                conv.CustomerId = -1;
                                                                conv.FacebookPostId = post_id;
                                                                conv.FacebookUserId = page.PageFacebookId;
                                                                conv.FBUserId = page.PageFacebookId;
                                                                conv.Name = page.PageName;
                                                                conv.Note = ""; //a
                                                                conv.PageId = page.PageId;
                                                                conv.ShopId = page.ShopId;
                                                                conv.Status = (int)Const.ConversationStatus.NOT_READ;
                                                                conv.Tag = "";
                                                                conv.Type = 1;
                                                                conv.UpdatedAt = DateTime.Now;
                                                                if (!isMessageEmpty)
                                                                    conv.LastMessage = entry.changes[j].value.message;
                                                                else
                                                                    conv.LastMessage = "Bạn vừa gửi ảnh cho bạn";
                                                                conv.Phone = "";
                                                                conv.LastMessageTime = DateTime.Now;
                                                                conv.UserAvatar = "http://graph.facebook.com/" + sender_id + "/picture?type=square";
                                                                conv.SearchQuery = Utils.unsignString(conv.Name + " " + conv.Phone + " " + conv.LastMessage);
                                                                conv.FacebookCommentId = fbcomment_id;
                                                                db.Entry(conv).State = EntityState.Added;
                                                                db.Conversations.Add(conv);
                                                                db.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                if (!isMessageEmpty)
                                                                    conv.LastMessage = entry.changes[j].value.message;
                                                                else
                                                                    conv.LastMessage = "Bạn vừa gửi ảnh cho bạn";
                                                                conv.UpdatedAt = DateTime.Now;
                                                                db.Entry(conv).State = EntityState.Modified;
                                                                db.SaveChanges();
                                                            }
                                                            //create comment
                                                            Comment comment = db.Comments.Where(c => c.FacebookCommentId == fbcomment_id).FirstOrDefault();
                                                            if (comment == null)
                                                            {
                                                                comment = new Comment();
                                                                comment.Content = entry.changes[j].value.message;
                                                                comment.CreatedAt = DateTime.Now;
                                                                comment.FacebookCommentId = fbcomment_id;
                                                                comment.FacebookStatus = 0;
                                                                comment.Photo = entry.changes[j].value.photo;
                                                                comment.PostId = post.PostId;
                                                                comment.SenderId = entry.changes[j].value.sender_id;
                                                                comment.SenderName = entry.changes[j].value.sender_name;
                                                                comment.Status = 0;
                                                                comment.UpdatedAt = DateTime.Now;
                                                                comment.ConversationId = conv.ConversationId;
                                                                db.Comments.Add(comment);
                                                                db.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                //exist comment ??
                                                                //change conversation >> 
                                                                comment.ConversationId = conv.ConversationId;
                                                                db.Entry(comment).State = EntityState.Modified;
                                                                db.SaveChanges();
                                                            }
                                                            try
                                                            {
                                                                var root = client.Child("Messenger/" + conv.ShopId + "/" + conv.ConversationId);
                                                                if (root != null)
                                                                {
                                                                    await root.PutAsync(new Messenger()
                                                                    {
                                                                        Content = entry.changes[j].value.message,
                                                                        ConversationId = conv != null ? conv.ConversationId : 0,
                                                                        Receiver = page != null ? page.PageFacebookId : "0",
                                                                        Sender = sender_id,
                                                                        Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                                                        Type = "COMMENT",
                                                                        CommentId = comment.CommentId,
                                                                        images = images,
                                                                        SenderName = page.PageName
                                                                    });
                                                                }
                                                                else
                                                                {
                                                                    log.Info("root null ???");
                                                                }
                                                                continue;
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                log.Info("firebase failed :" + ex.Message + "-" + ex.StackTrace);
                                                                continue;
                                                                //log.Info(entry.changes[j].value.message + "-" + conversation.ConversationId + "-" + page.PageFacebookId + "-" + sender_id);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.Info(ex.Message + "-" + ex.StackTrace);
                                                        }
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        //manually reply
                                                        //find comment
                                                        //check reply
                                                        Comment comment = db.Comments.Where(c => c.FacebookCommentId == parent_id).FirstOrDefault();
                                                        if (comment != null)
                                                        {
                                                            //>> conversation
                                                            Conversation conv = db.Conversations.Where(c => c.ConversationId == comment.ConversationId).FirstOrDefault();
                                                            if (conv != null)
                                                            {
                                                                //update
                                                                conv.LastMessageTime = DateTime.Now;
                                                                conv.LastMessage = entry.changes[j].value.message;
                                                                conv.UpdatedAt = DateTime.Now;
                                                                conv.SearchQuery = Utils.unsignString(conv.Name + " " + conv.LastMessage);
                                                                conv.Status = (int)Const.ConversationStatus.NOT_READ;
                                                                db.Entry(conv).State = EntityState.Modified;
                                                                db.SaveChanges();
                                                                //add reply
                                                                //check exist
                                                                Reply reply = db.Replies.Where(r => r.FacebookCommentId == fbcomment_id).FirstOrDefault();
                                                                if (reply == null)
                                                                {
                                                                    reply = new Reply();
                                                                    reply.CommentId = comment.CommentId;
                                                                    reply.Content = entry.changes[j].value.message;
                                                                    reply.CreatedAt = DateTime.Now;
                                                                    reply.FacebookCommentId = fbcomment_id;
                                                                    reply.FacebookStatus = 0;
                                                                    reply.Photo = entry.changes[j].value.photo;
                                                                    reply.SenderId = entry.changes[j].value.sender_id;
                                                                    reply.SenderName = entry.changes[j].value.sender_name;
                                                                    reply.Status = 0;
                                                                    reply.UpdatedAt = DateTime.Now;
                                                                    db.Replies.Add(reply);
                                                                    //update last message
                                                                    db.SaveChanges();
                                                                    //push to firebase
                                                                    try
                                                                    {
                                                                        var root = client.Child("Messenger/" + conv.ShopId + "/" + conv.ConversationId);
                                                                        if (root != null)
                                                                        {
                                                                            await root.PutAsync(new Messenger()
                                                                            {
                                                                                Content = entry.changes[j].value.message,
                                                                                ConversationId = conv != null ? conv.ConversationId : 0,
                                                                                Receiver = page != null ? page.PageFacebookId : "0",
                                                                                Sender = sender_id,
                                                                                Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                                                                Type = "REPLY",
                                                                                CommentId = comment.CommentId,
                                                                                images = images,
                                                                                SenderName = reply.SenderName
                                                                            });
                                                                        }
                                                                        else
                                                                        {
                                                                            log.Info("root null ???");
                                                                        }
                                                                        continue;
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        log.Info("firebase failed :" + ex.Message + "-" + ex.StackTrace);
                                                                        continue;
                                                                        //log.Info(entry.changes[j].value.message + "-" + conversation.ConversationId + "-" + page.PageFacebookId + "-" + sender_id);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    continue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                continue;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    // replyt to exist comment
                                                    if (isReply)
                                                    {
                                                        Comment comment = db.Comments.Where(c => c.FacebookCommentId == parent_id).FirstOrDefault();
                                                        if (comment != null)
                                                        {
                                                            //có cho người khác comment hay không
                                                            //if (comment.SenderId != sender_id)
                                                            //{
                                                            //    //ignore
                                                            //    continue;
                                                            //}    
                                                            //>> conversation
                                                            Conversation conv = db.Conversations.Where(c => c.ConversationId == comment.ConversationId).FirstOrDefault();
                                                            if (conv != null)
                                                            {
                                                                //update
                                                                conv.LastMessageTime = DateTime.Now;
                                                                if (!isMessageEmpty)
                                                                    conv.LastMessage = entry.changes[j].value.message;
                                                                else
                                                                    conv.LastMessage = conv.Name + " vừa gửi ảnh cho bạn";
                                                                if (hasPhone && (conv.Phone == null || conv.Phone == ""))
                                                                    conv.Phone = phone;
                                                                conv.UpdatedAt = DateTime.Now;
                                                                conv.SearchQuery = Utils.unsignString(conv.Name + "" + conv.Phone + " " + conv.LastMessage);
                                                                db.Entry(conv).State = EntityState.Modified;
                                                                db.SaveChanges();
                                                                //add reply
                                                                //check exist
                                                                Reply reply = db.Replies.Where(r => r.FacebookCommentId == fbcomment_id).FirstOrDefault();
                                                                if (reply == null)
                                                                {
                                                                    reply = new Reply();
                                                                    reply.CommentId = comment.CommentId;
                                                                    reply.Content = entry.changes[j].value.message;
                                                                    reply.CreatedAt = DateTime.Now;
                                                                    reply.FacebookCommentId = fbcomment_id;
                                                                    reply.FacebookStatus = 0;
                                                                    reply.Photo = entry.changes[j].value.photo;
                                                                    reply.SenderId = entry.changes[j].value.sender_id;
                                                                    reply.SenderName = entry.changes[j].value.sender_name;
                                                                    reply.Status = 0;
                                                                    reply.UpdatedAt = DateTime.Now;
                                                                    db.Replies.Add(reply);
                                                                    //update last message
                                                                    db.SaveChanges();
                                                                    //hide phoen comment
                                                                    if ((config.AutoHidePhoneComment.HasValue && (bool)config.AutoHidePhoneComment))
                                                                    {
                                                                        if (hasPhone)
                                                                        {
                                                                            //hide phone comment
                                                                            HttpClient httpClient = new HttpClient();
                                                                            HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "?is_hidden=true&access_token=" + page.PageFacebookToken, null);
                                                                            httpClient.Dispose();
                                                                            string result = response.Content.ReadAsStringAsync().Result;
                                                                            log.Info("hide reply : " + reply.ReplyId + ":" + result);
                                                                            DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                                                                            if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                                                                            {
                                                                                //delete comment
                                                                                reply.FacebookStatus = 1;
                                                                                reply.Status = 1;
                                                                                db.SaveChanges();
                                                                            }
                                                                        }
                                                                    }
                                                                    //push to firebase
                                                                    try
                                                                    {
                                                                        var root = client.Child("Messenger/" + conv.ShopId + "/" + conv.ConversationId);
                                                                        if (root != null)
                                                                        {
                                                                            await root.PutAsync(new Messenger()
                                                                            {
                                                                                Content = entry.changes[j].value.message,
                                                                                ConversationId = conv != null ? conv.ConversationId : 0,
                                                                                Receiver = page != null ? page.PageFacebookId : "0",
                                                                                Sender = sender_id,
                                                                                Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                                                                Type = "REPLY",
                                                                                CommentId = comment.CommentId,
                                                                                images = images,
                                                                                SenderName = reply.SenderName
                                                                            });
                                                                        }
                                                                        else
                                                                        {
                                                                            log.Info("root null ???");
                                                                        }
                                                                        continue;
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        log.Info("firebase failed :" + ex.Message + "-" + ex.StackTrace);
                                                                        continue;
                                                                        //log.Info(entry.changes[j].value.message + "-" + conversation.ConversationId + "-" + page.PageFacebookId + "-" + sender_id);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    continue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                continue;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }
                                                //new comment
                                                //page comment or reply
                                                //check conversation      
                                                string sender_name = entry.changes[j].value.sender_name;
                                                //chekc exist reply by id                                                
                                                Conversation conversation;
                                                if (!isReply)
                                                    conversation = db.Conversations.Where(c => c.FacebookPostId == post_id && c.FacebookUserId == sender_id && c.FacebookCommentId == fbcomment_id && c.Type == 1).FirstOrDefault();
                                                else
                                                {
                                                    conversation = db.Conversations.Where(c => c.FacebookPostId == post_id && c.FacebookUserId == sender_id && c.FacebookCommentId == parent_id && c.Type == 1).FirstOrDefault();
                                                }
                                                //log.Info("conversation : " + JsonConvert.SerializeObject(conversation));
                                                if (conversation == null)
                                                {
                                                    try
                                                    {
                                                        conversation = new Conversation();
                                                        conversation.CreatedAt = DateTime.Now;
                                                        conversation.CustomerId = -1;
                                                        conversation.FacebookPostId = post_id;
                                                        conversation.FacebookUserId = sender_id;
                                                        conversation.FBUserId = sender_id;
                                                        conversation.Name = sender_name;
                                                        conversation.Note = ""; //a
                                                        conversation.PageId = page.PageId;
                                                        conversation.ShopId = page.ShopId;
                                                        conversation.Status = (int)Const.ConversationStatus.NOT_READ;
                                                        conversation.Tag = "";
                                                        conversation.Type = 1;
                                                        conversation.UpdatedAt = DateTime.Now;
                                                        if (hasPhone)
                                                            conversation.Phone = phone;
                                                        if (!isMessageEmpty)
                                                            conversation.LastMessage = entry.changes[j].value.message;
                                                        else
                                                            conversation.LastMessage = conversation.Name + " vừa gửi ảnh cho bạn";
                                                        conversation.LastMessageTime = DateTime.Now;
                                                        conversation.UserAvatar = "http://graph.facebook.com/" + sender_id + "/picture?type=square";
                                                        conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                                        if (isReply)
                                                            conversation.FacebookCommentId = parent_id;
                                                        else
                                                            conversation.FacebookCommentId = fbcomment_id;
                                                        db.Entry(conversation).State = EntityState.Added;
                                                        db.Conversations.Add(conversation);
                                                        db.SaveChanges();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //log.Error(ex.Message + "\n" + ex.StackTrace + "\n" + ex.InnerException + "\n" + ex.Data);
                                                        log.Info("conversation ex: " + JsonConvert.SerializeObject(ex));
                                                    }
                                                }
                                                else
                                                {
                                                    conversation.FacebookPostId = post_id;
                                                    conversation.UpdatedAt = DateTime.Now;
                                                    conversation.LastMessageTime = DateTime.Now;
                                                    if (!isMessageEmpty)
                                                        conversation.LastMessage = entry.changes[j].value.message;
                                                    else
                                                        conversation.LastMessage = conversation.Name + " vừa gửi ảnh cho bạn";
                                                    conversation.Status = (int)Const.ConversationStatus.NOT_READ;
                                                    if (hasPhone && (conversation.Phone == null || conversation.Phone == ""))
                                                        conversation.Phone = phone;
                                                    conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                                    db.Entry(conversation).State = EntityState.Modified;
                                                    db.SaveChanges();
                                                }
                                                //map for customer
                                                if (conversation.CustomerId == null || conversation.CustomerId == -1)
                                                {
                                                    //map customer
                                                    Customer customer = db.Customers.Where(c => c.FacebookUserId == conversation.FBUserId).FirstOrDefault();
                                                    if (customer != null)
                                                    {
                                                        conversation.CustomerId = customer.CustomerId;
                                                    }
                                                }
                                                //create new comment or reply                                             
                                                if (parent_id.Contains(page.PageFacebookId))
                                                {
                                                    //comment
                                                    //check if comment exist
                                                    string comment_id = entry.changes[j].value.comment_id;
                                                    int count = db.Comments.Where(c => c.FacebookCommentId == comment_id).Count();
                                                    if (count == 0)
                                                    {
                                                        Comment comment = new Comment();
                                                        comment.Content = entry.changes[j].value.message;
                                                        comment.CreatedAt = DateTime.Now;
                                                        comment.FacebookCommentId = comment_id;
                                                        comment.FacebookStatus = 0;
                                                        comment.Photo = entry.changes[j].value.photo;
                                                        comment.PostId = post.PostId;
                                                        comment.SenderId = entry.changes[j].value.sender_id;
                                                        comment.SenderName = entry.changes[j].value.sender_name;
                                                        comment.Status = 0;
                                                        comment.UpdatedAt = DateTime.Now;
                                                        comment.ConversationId = conversation.ConversationId;
                                                        db.Comments.Add(comment);
                                                        db.SaveChanges();
                                                        //push to firebase
                                                        try
                                                        {
                                                            var root = client.Child("Messenger/" + conversation.ShopId + "/" + conversation.ConversationId);
                                                            if (root != null)
                                                            {
                                                                await root.PutAsync(new Messenger()
                                                                {
                                                                    Content = entry.changes[j].value.message,
                                                                    ConversationId = conversation != null ? conversation.ConversationId : 0,
                                                                    Receiver = page != null ? page.PageFacebookId : "0",
                                                                    Sender = sender_id,
                                                                    Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                                                    Type = "COMMENT",
                                                                    CommentId = comment.CommentId,
                                                                    images = images,
                                                                    SenderName = comment.SenderName
                                                                });
                                                            }
                                                            else
                                                            {
                                                                log.Info("root null ???");
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.Info("firebase failed :" + ex.Message + "-" + ex.StackTrace);
                                                            //log.Info(entry.changes[j].value.message + "-" + conversation.ConversationId + "-" + page.PageFacebookId + "-" + sender_id);
                                                        }
                                                        try
                                                        {
                                                            if (config != null)
                                                            {
                                                                //auto hide comment
                                                                //check post                                                                
                                                                if ((config.AutoHideComment.HasValue && (bool)config.AutoHideComment))
                                                                {
                                                                    if (post.ShowComment == 0)
                                                                    {
                                                                        HttpClient httpClient = new HttpClient();
                                                                        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "?is_hidden=true&access_token=" + page.PageFacebookToken, null);
                                                                        httpClient.Dispose();
                                                                        string result = response.Content.ReadAsStringAsync().Result;
                                                                        log.Info("hide comment : " + comment.CommentId + ":" + result);
                                                                        DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                                                                        if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                                                                        {
                                                                            //delete comment
                                                                            comment.FacebookStatus = 1;
                                                                            comment.Status = 1;
                                                                            db.SaveChanges();
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if ((config.AutoHidePhoneComment.HasValue && (bool)config.AutoHidePhoneComment))
                                                                    {
                                                                        if (hasPhone)
                                                                        {
                                                                            //hide phone comment
                                                                            HttpClient httpClient = new HttpClient();
                                                                            HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "?is_hidden=true&access_token=" + page.PageFacebookToken, null);
                                                                            httpClient.Dispose();
                                                                            string result = response.Content.ReadAsStringAsync().Result;
                                                                            log.Info("hide comment : " + comment.CommentId + ":" + result);
                                                                            DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                                                                            if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                                                                            {
                                                                                //delete comment
                                                                                comment.FacebookStatus = 1;
                                                                                comment.Status = 1;
                                                                                db.SaveChanges();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //auto like comment
                                                                if (config.NewCommentLike.HasValue && (bool)config.NewCommentLike)
                                                                {
                                                                    HttpClient httpClient = new HttpClient();
                                                                    HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "/likes?access_token=" + page.PageFacebookToken, null);
                                                                    httpClient.Dispose();
                                                                    string result = response.Content.ReadAsStringAsync().Result;
                                                                    DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                                                                    if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                                                                    {
                                                                        //delete comment
                                                                        if (comment.Status == 1)
                                                                        {
                                                                            comment.FacebookStatus = 11;
                                                                            comment.Status = 11;
                                                                            db.SaveChanges();
                                                                        }
                                                                        else
                                                                        {
                                                                            comment.FacebookStatus = 10;
                                                                            comment.Status = 10;
                                                                            db.SaveChanges();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //reply     
                                                    try
                                                    {
                                                        string comment_id = entry.changes[j].value.comment_id;
                                                        //log.Info("comment_id :" + comment_id);
                                                        int count = db.Replies.Where(c => c.FacebookCommentId == comment_id).Count();
                                                        if (count == 0)
                                                        {
                                                            //search for parent                                                   
                                                            Comment comment = db.Comments.Where(c => c.FacebookCommentId == parent_id).FirstOrDefault();
                                                            //log.Info("comment:" + JsonConvert.SerializeObject(comment));
                                                            if (comment != null)
                                                            {
                                                                Reply reply = new Reply();
                                                                reply.CommentId = comment.CommentId;
                                                                reply.Content = entry.changes[j].value.message;
                                                                reply.CreatedAt = DateTime.Now;
                                                                reply.FacebookCommentId = comment_id;
                                                                reply.FacebookStatus = 0;
                                                                reply.Photo = entry.changes[j].value.photo;
                                                                reply.SenderId = entry.changes[j].value.sender_id;
                                                                reply.SenderName = entry.changes[j].value.sender_name;
                                                                reply.Status = 0;
                                                                reply.UpdatedAt = DateTime.Now;
                                                                db.Replies.Add(reply);
                                                                //update last message
                                                                db.SaveChanges();
                                                                //push to firebase
                                                                //hide reply with phone
                                                                //if ((config.AutoHidePhoneComment.HasValue && (bool)config.AutoHidePhoneComment))
                                                                //{
                                                                //    if (hasPhone)
                                                                //    {
                                                                //        //hide phone comment
                                                                //        HttpClient httpClient = new HttpClient();
                                                                //        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "?is_hidden=true&access_token=" + page.PageFacebookToken, null);
                                                                //        httpClient.Dispose();
                                                                //        string result = response.Content.ReadAsStringAsync().Result;
                                                                //        log.Info("hide reply : " + reply.ReplyId + ":" + result);
                                                                //        DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                                                                //        if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                                                                //        {
                                                                //            //delete comment
                                                                //            reply.FacebookStatus = 1;
                                                                //            reply.Status = 1;
                                                                //            db.SaveChanges();
                                                                //        }
                                                                //    }
                                                                //}
                                                                try
                                                                {
                                                                    var root = client.Child("Messenger/" + conversation.ShopId + "/" + conversation.ConversationId);
                                                                    if (root != null)
                                                                    {
                                                                        await root.PutAsync(new Messenger()
                                                                        {
                                                                            Content = entry.changes[j].value.message,
                                                                            ConversationId = conversation != null ? conversation.ConversationId : 0,
                                                                            Receiver = page != null ? page.PageFacebookId : "0",
                                                                            Sender = sender_id,
                                                                            Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                                                            Type = "REPLY",
                                                                            CommentId = comment.CommentId,
                                                                            images = images,
                                                                            SenderName = reply.SenderName
                                                                        });
                                                                    }
                                                                    else
                                                                    {
                                                                        log.Info("root null ???");
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    log.Info("firebase failed :" + ex.Message + "-" + ex.StackTrace);
                                                                    //log.Info(entry.changes[j].value.message + "-" + conversation.ConversationId + "-" + page.PageFacebookId + "-" + sender_id);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex1)
                                                    {
                                                        log.Error(ex1.Message + "-" + ex1.StackTrace);
                                                    }
                                                }
                                            }
                                            #endregion end feed
                                            #region conversations
                                            else
                                            {
                                                //conversation
                                                if (entry.changes[j].field == "conversations")
                                                {
                                                    //process other message
                                                    if (entry.changes[j].value.thread_id != null && entry.changes[j].value.thread_id != "")
                                                    {
                                                        //log.Info("thread_id :" + entry.changes[j].value.thread_id);
                                                        //check if page send message to usser directly from facebook
                                                        HttpClient httpClient = new HttpClient();
                                                        HttpResponseMessage response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + entry.changes[j].value.thread_id + "?fields=messages.limit(5){message,from,to,created_time,attachments{file_url,image_data,name}}&access_token=" + page.PageFacebookToken);
                                                        httpClient.Dispose();
                                                        string result = response.Content.ReadAsStringAsync().Result;
                                                        //log.Info(result);
                                                        FBThreadResp resp = JsonConvert.DeserializeObject<FBThreadResp>(result);
                                                        //log.Info("message count : " + resp.messages.data.Count);
                                                        if (resp != null && resp.messages != null && resp.messages.data != null && resp.messages.data.Count > 0)
                                                        {
                                                            for (int k = 0; k < resp.messages.data.Count; k++)
                                                            {
                                                                //check message from page only
                                                                if (page.PageFacebookId == resp.messages.data[k].from.id)
                                                                {
                                                                    //log.Info("correct :" + resp.messages.data[k].from.id);
                                                                    //check if message exist
                                                                    string firstReceiverId = resp.messages.data[k].to.data[0].id;
                                                                    //log.Info("first id :" + resp.messages.data[k].to.data[0].id);
                                                                    //get conversation
                                                                    Conversation conversation = db.Conversations.Where(c => c.FBUserId == firstReceiverId && c.ShopId == page.ShopId && c.Type == 0 && c.PageId == page.PageId).FirstOrDefault();
                                                                    if (conversation != null)
                                                                    {
                                                                        //log.Info("conv id :" + conversation.ConversationId);
                                                                        //exist conversation
                                                                        //check message exist
                                                                        string mid = resp.messages.data[k].id.Replace("m_", "");
                                                                        Message existMessage = db.Messages.Where(m => m.FacebookMessageId == mid).FirstOrDefault();
                                                                        //log.Info("existMessage : "  + JsonConvert.SerializeObject(existMessage));
                                                                        if (existMessage == null)
                                                                        {
                                                                            try
                                                                            {
                                                                                existMessage = new Message();
                                                                                existMessage.Content = resp.messages.data[k].message;
                                                                                existMessage.ConversationId = conversation.ConversationId;
                                                                                existMessage.CreatedAt = resp.messages.data[k].created_time;
                                                                                existMessage.EmployeeId = null;
                                                                                existMessage.FacebookMessageId = resp.messages.data[k].id.Replace("m_", "");
                                                                                existMessage.FacebookStatus = 0;
                                                                                existMessage.HasAttachment = resp.messages.data[k].attachments != null ? true : false;
                                                                                existMessage.IsBotMessage = false;
                                                                                existMessage.RecipientId = resp.messages.data[k].to.data[0].id;
                                                                                existMessage.RecipientName = resp.messages.data[k].to.data[0].name;
                                                                                existMessage.SenderId = resp.messages.data[k].from.id;
                                                                                existMessage.SenderName = resp.messages.data[k].from.name;
                                                                                existMessage.Sequence = 0;
                                                                                existMessage.Status = 0;
                                                                                existMessage.Subject = "";
                                                                                existMessage.UpdatedAt = resp.messages.data[k].created_time;
                                                                                db.Messages.Add(existMessage);
                                                                                db.SaveChanges();
                                                                                //log.Info(existMessage.MessageId);
                                                                                //attachment
                                                                                if (resp.messages.data[k].attachments != null)
                                                                                {
                                                                                    List<Attachment> attachments = new List<Attachment>();
                                                                                    for (int l = 0; l < resp.messages.data[l].attachments.data.Count; k++)
                                                                                    {
                                                                                        if (resp.messages.data[k].attachments.data[l].image_data.image_type == 1)
                                                                                        {
                                                                                            Attachment attachment = new Attachment();
                                                                                            attachment.CreatedAt = DateTime.Now;
                                                                                            attachment.MessageId = existMessage.MessageId;
                                                                                            attachment.Type = "image";
                                                                                            attachment.Url = resp.messages.data[k].attachments.data[l].image_data.url;
                                                                                            attachments.Add(attachment);
                                                                                        }
                                                                                    }
                                                                                    db.Attachments.AddRange(attachments);
                                                                                    db.SaveChangesAsync();
                                                                                }
                                                                                //push
                                                                                var firebaseClient = new FirebaseClient(
                                                                                  firebase_url,
                                                                                  new FirebaseOptions
                                                                                  {
                                                                                      AuthTokenAsyncFactory = () => Task.FromResult(firebase_secret)
                                                                                  });
                                                                                if (client == null)
                                                                                {
                                                                                    client = new FirebaseClient(
                                                                                                        firebase_url,
                                                                                                        new FirebaseOptions
                                                                                                        {
                                                                                                            AuthTokenAsyncFactory = () => Task.FromResult(firebase_secret)
                                                                                                        });
                                                                                }
                                                                                var root = client.Child("Messenger/" + conversation.ShopId + "/" + conversation.ConversationId);
                                                                                List<string> images = new List<string>();
                                                                                if (resp.messages.data[k].attachments != null && resp.messages.data[k].attachments.data != null && resp.messages.data[k].attachments.data.Count > 0)
                                                                                {
                                                                                    //has image
                                                                                    foreach (FBThreadMessageAttItem a in resp.messages.data[k].attachments.data)
                                                                                    {
                                                                                        images.Add(a.image_data.url);
                                                                                    }
                                                                                }
                                                                                await root.PutAsync(new Messenger()
                                                                                {
                                                                                    Content = resp.messages.data[k].message,
                                                                                    ConversationId = conversation.ConversationId,
                                                                                    Receiver = resp.messages.data[k].to.data[0].id,
                                                                                    Sender = resp.messages.data[k].from.id,
                                                                                    Timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fff"),
                                                                                    images = images,
                                                                                    SenderName = resp.messages.data[k].from.name
                                                                                });
                                                                            }
                                                                            catch (Exception ex)
                                                                            {
                                                                                log.Info("push error : " + ex.Message + "-" + ex.StackTrace);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                log.Info(ex.Message + "-" + ex.StackTrace);
            }
        }

        //[HttpPost]
        //[Route("api/facebook/postComment")]
        //public async Task<IHttpActionResult> postComment(HttpRequestMessage request)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
        //    string sRequest = reader.ReadToEnd().Trim();
        //    CommentReq req = JsonConvert.DeserializeObject<CommentReq>(sRequest);
        //    if (req != null)
        //    {
        //            Shop shop = db.Shops.Find(req.shop_id);
        //            if (shop != null)
        //            {
        //                User user = db.Users.Find(shop.UserId);
        //                if (user != null)
        //                {                            
        //                    //find post id
        //                    Post post = db.Posts.Where(p => p.FacebookPostId == req.facebook_post_id).FirstOrDefault();
        //                    //find page id 
        //                    if (req.facebook_post_id.IndexOf("_") > 0)
        //                    {
        //                        string[] temps = req.facebook_post_id.Split('_');
        //                        string facebook_page_id = temps[0];
        //                        Page page = db.Pages.Where(p => p.PageFacebookId == facebook_page_id).FirstOrDefault();
        //                        if (page != null)
        //                        {
        //                            HttpClient httpClient;
        //                            string result;
        //                            if (post == null)
        //                            {                                        
        //                                def.meta = new Meta(404, "Post Not Found");
        //                                return Ok(def);                                      
        //                            }                            
        //                            httpClient = new HttpClient();
        //                            List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
        //                            values.Add(new KeyValuePair<string, string>("access_token",page.PageFacebookToken));
        //                            values.Add(new KeyValuePair<string, string>("message", req.message));
        //                            if (req.image_url != null)
        //                                values.Add(new KeyValuePair<string, string>("attachment_url", req.image_url));                                    
        //                            var content = new FormUrlEncodedContent(values);
        //                            HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/"+ req.facebook_post_id +"/comments",content);

        //                            httpClient.Dispose();
        //                            result = response.Content.ReadAsStringAsync().Result;
        //                            log.Info("comment :" + result);
        //                            CommentResp commentResponse = JsonConvert.DeserializeObject<CommentResp>(result);
        //                            if (commentResponse != null && commentResponse.id != null)
        //                            {
        //                                //add comment to comment table
        //                                Comment comment = new Comment();
        //                                comment.Content = req.message;
        //                                comment.CreatedAt = DateTime.Now;
        //                                comment.FacebookCommentId = commentResponse.id;
        //                                comment.FacebookStatus = 0;
        //                                comment.Photo = req.image_url;
        //                                comment.PostId = post.PostId;
        //                                comment.SenderId = page.PageFacebookId;
        //                                comment.SenderName = page.PageName;
        //                                comment.Status = 0;
        //                                comment.UpdatedAt = DateTime.Now;                                        
        //                                db.Comments.Add(comment);
        //                                db.SaveChanges();

        //                                CommentDTO returnComment = db.Comments.Where(c => c.CommentId == comment.CommentId).Select(c => new CommentDTO() { 
        //                                    CommentId = c.CommentId,
        //                                    Content = c.Content,
        //                                    CreatedAt = c.CreatedAt,
        //                                    CustomerId = c.CustomerId,
        //                                    FacebookCommentId = c.FacebookCommentId,
        //                                    FacebookStatus = c.FacebookStatus,
        //                                    Photo = c.Photo,
        //                                    PostId = c.PostId,
        //                                    SenderId = c.SenderId,
        //                                    SenderName = c.SenderName,
        //                                    Status = c.Status,
        //                                    UpdatedAt = c.UpdatedAt
        //                                }).FirstOrDefault();

        //                                def.meta = new Meta(200, "Success");
        //                                def.data = returnComment;
        //                                return Ok(def);
        //                            }
        //                            else
        //                            {
        //                                def.meta = new Meta(500, "Interal Server Error");
        //                                return Ok(def);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            //invalid page ??
        //                            def.meta = new Meta(404,"Not Found");
        //                            return Ok(def);
        //                        }
        //                    }         
        //                    else
        //                    {
        //                        //invalid post id
        //                        def.meta = new Meta(400,"Bad Request");
        //                        return Ok(def);
        //                    }
        //                }
        //                else
        //                {
        //                    def.meta = new Meta(404,"Not Found");
        //                    return Ok(def);
        //                }
        //            }
        //            else
        //            {
        //                def.meta = new Meta(404,"Not Found");
        //                return Ok(def);
        //            }                
        //    }
        //    else
        //    {
        //        def.meta = new Meta(400,"Bad Request");
        //        return Ok(def);
        //    }
        //}

        [HttpPost]
        [Route("api/facebook/postReply")]
        public async Task<IHttpActionResult> postReply(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();
            log.Info("reply request :" + sRequest);
            ReplyReq req = JsonConvert.DeserializeObject<ReplyReq>(sRequest);
            if (req != null)
            {
                //find post id   
                if (req.facebook_post_id.IndexOf("_") > 0)
                {
                    string[] temps = req.facebook_post_id.Split('_');
                    string facebook_page_id = temps[0];
                    Page page = db.Pages.Where(p => p.PageFacebookId == facebook_page_id).FirstOrDefault();
                    if (page != null)
                    {
                        HttpClient httpClient = new HttpClient();
                        string result;
                        //check if comment exist
                        Comment comment = db.Comments.Where(c => c.FacebookCommentId == req.facebook_comment_id).FirstOrDefault();
                        if (comment == null)
                        {
                            def.meta = new Meta(404, "Not found");
                            return Ok(def);
                        }
                        //send reply first
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", page.PageFacebookToken));
                        values.Add(new KeyValuePair<string, string>("message", req.message == null || req.message == "" ? " " : req.message));
                        if (req.image_url != null)
                            values.Add(new KeyValuePair<string, string>("attachment_url", req.image_url));
                        var content = new FormUrlEncodedContent(values);
                        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + req.facebook_comment_id + "/comments", content);
                        httpClient.Dispose();
                        result = response.Content.ReadAsStringAsync().Result;
                        CommentResp commentResponse = JsonConvert.DeserializeObject<CommentResp>(result);
                        //save to db
                        Reply reply = new Reply();
                        reply.Content = req.message;
                        reply.CreatedAt = DateTime.Now;
                        reply.FacebookCommentId = commentResponse.id;
                        reply.FacebookStatus = 0;
                        reply.Photo = req.image_url;
                        reply.CommentId = comment.CommentId;
                        reply.SenderId = page.PageFacebookId;
                        reply.SenderName = page.PageName;
                        reply.UpdatedAt = DateTime.Now;
                        reply.EmployeeId = req.employee_id;
                        if (commentResponse != null && commentResponse.id != null)
                        {
                            //send ok
                            reply.Status = 0;
                            db.Entry(reply).State = EntityState.Added;
                            Conversation conversation = db.Conversations.Where(c => c.ConversationId == comment.ConversationId).FirstOrDefault();
                            conversation.LastMessage = req.message;
                            conversation.LastMessageTime = DateTime.Now;
                            conversation.UpdatedAt = DateTime.Now;
                            conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.LastMessage);
                            conversation.Status = (int)Const.ConversationStatus.REPLIED;
                            db.Entry(conversation).State = EntityState.Modified;
                            db.SaveChanges();
                            ReplyDTO returnReply = db.Replies.Where(c => c.ReplyId == reply.ReplyId).Select(c => new ReplyDTO()
                            {
                                CommentId = c.CommentId,
                                Content = c.Content,
                                CreatedAt = c.CreatedAt,
                                CustomerId = c.CustomerId,
                                FacebookCommentId = c.FacebookCommentId,
                                FacebookStatus = c.FacebookStatus,
                                Photo = c.Photo,
                                ReplyId = c.ReplyId,
                                SenderId = c.SenderId,
                                SenderName = c.SenderName,
                                Status = c.Status,
                                UpdatedAt = c.UpdatedAt,
                                EmployeeId = c.EmployeeId
                            }).FirstOrDefault();

                            def.meta = new Meta(200, "Success");
                            def.data = returnReply;
                            return Ok(def);
                        }
                        else
                        {
                            reply.Status = -1;
                            db.SaveChanges();
                            def.meta = new Meta(201, "Send reply failed");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        //invalid page ??
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    //invalid post id
                    def.meta = new Meta(400, "Bad Request");
                    return Ok(def);
                }

            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/deleteComment")]
        public async Task<IHttpActionResult> deleteComment([FromBody] DeleteCommentReq req)
        {
            DefaultResponse def = new DefaultResponse();
            //find conversation
            Conversation conversation = db.Conversations.Find(req.conversation_id);
            if (conversation != null)
            {
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.DeleteAsync("https://graph.facebook.com/v2.8/" + req.facebook_comment_id + "?access_token=" + conversation.Page.PageFacebookToken);
                httpClient.Dispose();
                string result = response.Content.ReadAsStringAsync().Result;
                log.Info(result);
                DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                {
                    //ok
                    //delete reply belong to comment
                    var tasks = new[]
                        {
                            Task.Run(() => deleteCommentCallback(req))
                        };                   
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    FBErrorResp resp = JsonConvert.DeserializeObject<FBErrorResp>(result);
                    if (resp.error.code == 100)
                    {
                        var tasks = new[]
                        {
                            Task.Run(() => deleteReplyCallback(req))
                        };                       
                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(500, "Internal Server Error");
                        return Ok(def);
                    }
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        private async void deleteCommentCallback(DeleteCommentReq req)
        {
            Comment comment = db.Comments.Where(r => r.FacebookCommentId == req.facebook_comment_id).FirstOrDefault();
            Conversation conversation = db.Conversations.Find(req.conversation_id);
            if (comment != null)
            {
                var replies = db.Replies.Where(r => r.CommentId == comment.CommentId);
                db.Replies.RemoveRange(replies);
                //delete comment
                db.Comments.Remove(comment);
                db.Conversations.Remove(conversation);
                db.SaveChanges();
            }
        }

        [HttpPost]
        [Route("api/facebook/deleteReply")]
        public async Task<IHttpActionResult> deleteReply([FromBody] DeleteCommentReq req)
        {
            DefaultResponse def = new DefaultResponse();
            Conversation conversation = db.Conversations.Find(req.conversation_id);
            if (conversation != null)
            {
                //delete comment from facebook
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.DeleteAsync("https://graph.facebook.com/v2.8/" + req.facebook_reply_id + "?access_token=" + conversation.Page.PageFacebookToken);
                httpClient.Dispose();
                string result = response.Content.ReadAsStringAsync().Result;
                log.Info(result);
                DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                {
                    //delete comment
                    var tasks = new[]
                        {
                            Task.Run(() => deleteReplyCallback(req))
                        };
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        private async void deleteReplyCallback(DeleteCommentReq req)
        {
            Reply reply = db.Replies.Where(r => r.FacebookCommentId == req.facebook_reply_id).FirstOrDefault();
            if (reply != null)
            {
                db.Replies.Remove(reply);
                db.SaveChanges();
            }
        }

        [HttpPost]
        [Route("api/facebook/editComment")]
        public async Task<IHttpActionResult> editComment([FromBody] EditCommentReq req)
        {
            DefaultResponse def = new DefaultResponse();
            if (req != null)
            {
                Conversation conversation = db.Conversations.Find(req.conversation_id);
                if (conversation != null)
                {
                    HttpClient httpClient;
                    string result;
                    httpClient = new HttpClient();
                    List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                    values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                    values.Add(new KeyValuePair<string, string>("message", req.message));
                    if (req.image_url != null)
                        values.Add(new KeyValuePair<string, string>("attachment_url", req.image_url));
                    var content = new FormUrlEncodedContent(values);
                    HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + req.facebook_comment_id, content);
                    httpClient.Dispose();
                    result = response.Content.ReadAsStringAsync().Result;
                    DeleteResp resp = JsonConvert.DeserializeObject<DeleteResp>(result);
                    if (resp != null && resp.success != null && resp.success)
                    {
                        //done
                        var tasks = new[]
                        {
                            Task.Run(() => editCommentCallback(req))
                        };

                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                    else
                    {
                        //update false
                        def.meta = new Meta(201, "Failed");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        private async void editCommentCallback(EditCommentReq req)
        {
            Comment comment = db.Comments.Where(r => r.FacebookCommentId == req.facebook_comment_id).FirstOrDefault();
            if (comment != null)
            {
                comment.Content = req.message;
                comment.Photo = req.image_url;
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        [HttpPost]
        [Route("api/facebook/editReply")]
        public async Task<IHttpActionResult> editReply([FromBody] EditCommentReq req)
        {
            DefaultResponse def = new DefaultResponse();
            if (req != null)
            {
                //find conversation
                Conversation conversation = db.Conversations.Find(req.conversation_id);
                if (conversation != null)
                {
                    HttpClient httpClient;
                    string result;
                    httpClient = new HttpClient();
                    List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                    values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                    values.Add(new KeyValuePair<string, string>("message", req.message));
                    if (req.image_url != null)
                        values.Add(new KeyValuePair<string, string>("attachment_url", req.image_url));
                    var content = new FormUrlEncodedContent(values);
                    HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + req.facebook_reply_id, content);
                    httpClient.Dispose();
                    result = response.Content.ReadAsStringAsync().Result;
                    DeleteResp resp = JsonConvert.DeserializeObject<DeleteResp>(result);
                    if (resp != null && resp.success != null && resp.success)
                    {
                        var tasks = new[]
                        {
                            Task.Run(() => editReplyCallback(req))
                        };
                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                    else
                    {
                        //update false
                        def.meta = new Meta(201, "Failed");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        private async void editReplyCallback(EditCommentReq req)
        {
            Reply reply = db.Replies.Where(r => r.FacebookCommentId == req.facebook_reply_id).FirstOrDefault();
            if (reply != null)
            {
                reply.Content = req.message;
                reply.Photo = req.image_url;
                db.Entry(reply).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        [HttpPost]
        [Route("api/facebook/hideComment/{id}")]
        public async Task<IHttpActionResult> hideComment(int id)
        {
            DefaultResponse def = new DefaultResponse();
            try
            {
                Comment comment = db.Comments.Find(id);
                if (comment != null)
                {
                    Conversation conversation = db.Conversations.Find(comment.ConversationId);
                    if (conversation != null)
                    {
                        //delete comment from facebook
                        HttpClient httpClient = new HttpClient();
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                        var content = new FormUrlEncodedContent(values);
                        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "?is_hidden=true", content);
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        log.Info(result);
                        DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                        if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                        {
                            //delete comment
                            if (comment.Status == 0)
                            {
                                comment.FacebookStatus = 1;
                                comment.Status = 1;
                            }
                            else if (comment.Status == 10)
                            {
                                comment.FacebookStatus = 11;
                                comment.Status = 11;
                            }
                            else
                            {
                                comment.FacebookStatus = 1;
                                comment.Status = 1;
                            }
                            db.SaveChanges();
                            def.meta = new Meta(200, "Success");
                            return Ok(def);
                        }
                        else
                        {
                            //face book error
                            FBErrorResp errResp = JsonConvert.DeserializeObject<FBErrorResp>(result);
                            if (errResp.error.code == 200)
                            {
                                def.meta = new Meta(211, "Cannot hide admin comment");
                                return Ok(def);
                            }
                            else
                            {
                                def.meta = new Meta(500, "Internal Server Error");
                                return Ok(def);
                            }
                        }
                    }
                    else
                    {
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                log.Info(ex.Message + "-" + ex.StackTrace);
                def.meta = new Meta(500, "Internal Server Error");
                return Ok(def);
            }
        }


        [HttpPost]
        [Route("api/facebook/unhideComment/{id}")]
        public async Task<IHttpActionResult> unhideComment(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Comment comment = db.Comments.Find(id);
            if (comment != null)
            {
                //find conversation
                Conversation conversation = db.Conversations.Find(comment.ConversationId);
                if (conversation != null)
                {
                    //delete comment from facebook
                    HttpClient httpClient = new HttpClient();
                    List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                    values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                    var content = new FormUrlEncodedContent(values);
                    HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "?is_hidden=false", content);
                    httpClient.Dispose();
                    string result = response.Content.ReadAsStringAsync().Result;
                    log.Info(result);
                    DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                    if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                    {
                        //delete comment
                        if (comment.Status == 1)
                        {
                            comment.FacebookStatus = 0;
                            comment.Status = 0;
                        }
                        else if (comment.Status == 11)
                        {
                            comment.FacebookStatus = 10;
                            comment.Status = 10;
                        }
                        db.SaveChanges();
                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(500, "Internal Server Error");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/hideReply/{id}")]
        public async Task<IHttpActionResult> hideReply(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Reply reply = db.Replies.Find(id);
            if (reply != null)
            {
                //find comment 
                Comment comment = db.Comments.Find(reply.CommentId);
                if (comment != null)
                {
                    //find conversation
                    Conversation conversation = db.Conversations.Find(comment.ConversationId);
                    if (conversation != null)
                    {
                        HttpClient httpClient = new HttpClient();
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                        var content = new FormUrlEncodedContent(values);
                        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "?is_hidden=true", content);
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        log.Info(result);
                        DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                        if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                        {
                            //delete comment
                            //check status
                            if (reply.Status == 0)
                            {
                                reply.FacebookStatus = 1;
                                reply.Status = 1;
                            }
                            else if (reply.Status == 10)
                            {
                                reply.FacebookStatus = 11;
                                reply.Status = 11;
                            }
                            else
                            {
                                reply.FacebookStatus = 1;
                                reply.Status = 1;
                            }
                            db.SaveChanges();
                            def.meta = new Meta(200, "Success");
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(500, "Internal Server Error");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/unhideReply/{id}")]
        public async Task<IHttpActionResult> unhideReply(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Reply reply = db.Replies.Find(id);
            if (reply != null)
            {
                //find comment 
                Comment comment = db.Comments.Find(reply.CommentId);
                if (comment != null)
                {
                    //find conversation
                    Conversation conversation = db.Conversations.Find(comment.ConversationId);
                    if (conversation != null)
                    {
                        HttpClient httpClient = new HttpClient();
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                        var content = new FormUrlEncodedContent(values);
                        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "?is_hidden=false", content);
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        log.Info(result);
                        DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                        if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                        {
                            //delete comment
                            if (reply.Status == 1)
                            {
                                reply.FacebookStatus = 0;
                                reply.Status = 0;
                            }
                            else if (reply.Status == 11)
                            {
                                reply.FacebookStatus = 10;
                                reply.Status = 10;
                            }
                            else
                            {
                                reply.FacebookStatus = 0;
                                reply.Status = 0;
                            }
                            db.SaveChanges();
                            def.meta = new Meta(200, "Success");
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(500, "Internal Server Error");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/sendMessage")]
        public async Task<IHttpActionResult> sendMessage(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();
            SendMessageReq req = JsonConvert.DeserializeObject<SendMessageReq>(sRequest);
            log.Info(sRequest);
            Page page = db.Pages.Find(req.page_id);
            Shop shop = db.Shops.Find(req.shop_id);
            if (page != null && shop != null)
            {
                //check converastion
                Conversation conversation = db.Conversations.Where(c => c.FacebookUserId == req.facebook_user_id).FirstOrDefault();
                if (conversation == null)
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }

                Models.Facebook.Attachment attachment = null;
                if (req.image_url != null)
                {
                    Models.Facebook.Payload payload = new Models.Facebook.Payload();
                    payload.is_reusable = false;
                    if (req.image_url != null)
                        payload.url = req.image_url;
                    attachment = new Models.Facebook.Attachment();
                    attachment.payload = payload;
                    attachment.type = "image";
                }
                Models.Facebook.Message message = new Models.Facebook.Message();
                if (req.message != null || req.message == "")
                    message.text = req.message;
                if (req.image_url != null)
                    message.attachment = attachment;
                if (message.text != null || message.attachment != null)
                {
                    //recipient
                    Models.Facebook.Recipient recipient = new Models.Facebook.Recipient();
                    if (req.facebook_user_id != null)
                        recipient.id = req.facebook_user_id;
                    else if (req.customer_phone_number != null)
                    {
                        string phone_number = "";
                        //format phone
                        if (req.customer_phone_number.IndexOf('0') == 0)
                            phone_number = "+84" + req.customer_phone_number.Substring(1);
                        else if (req.customer_phone_number.IndexOf("84") == 0)
                            phone_number = "+" + req.customer_phone_number;
                        else
                            phone_number = req.customer_phone_number;
                        recipient.phone_number = phone_number;
                    }
                    Models.Facebook.FBSendMessageReq fbReq = new Models.Facebook.FBSendMessageReq();
                    fbReq.message = message;
                    fbReq.recipient = recipient;
                    // begin send message
                    HttpClient httpClient = new HttpClient();
                    HttpResponseMessage response = await httpClient.PostAsJsonAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/messages?access_token=" + page.PageFacebookToken, fbReq);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        FBSendMessageResp responseFB = JsonConvert.DeserializeObject<FBSendMessageResp>(result);
                        if (responseFB != null && responseFB.message_id != null)
                        {
                            //send ok
                            //create message                            
                            conversation.LastMessage = req.message;
                            conversation.UpdatedAt = DateTime.Now;
                            conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.LastMessage);
                            db.Entry(conversation).State = EntityState.Modified;
                            db.SaveChanges();
                            //create message
                            Message messageDB = new Message();
                            messageDB.Content = req.message;
                            messageDB.ConversationId = conversation.ConversationId;
                            messageDB.CreatedAt = DateTime.Now;
                            messageDB.FacebookStatus = 2;
                            messageDB.IsBotMessage = false;
                            messageDB.RecipientId = req.facebook_user_id;
                            messageDB.RecipientName = "";
                            messageDB.SenderId = page.PageFacebookId;
                            messageDB.SenderName = page.PageName;
                            messageDB.Sequence = -1;
                            messageDB.Status = 0;
                            messageDB.Subject = "";
                            messageDB.UpdatedAt = DateTime.Now;
                            messageDB.EmployeeId = req.employee_id;
                            messageDB.FacebookMessageId = responseFB.message_id;
                            db.Messages.Add(messageDB);
                            db.SaveChanges();
                            //attachment
                            if (req.image_url != null)
                            {
                                Attachment attachmentDB = new Attachment();
                                attachmentDB.CreatedAt = DateTime.Now;
                                attachmentDB.MessageId = messageDB.MessageId;
                                attachmentDB.Type = "image";
                                attachmentDB.Url = req.image_url;
                                db.Attachments.Add(attachmentDB);
                                db.SaveChanges();
                            }
                            def.meta = new Meta(200, "Success");
                            def.data = messageDB.MessageId;
                            return Ok(def);
                        }
                        else
                        {
                            //send faild
                            def.meta = new Meta(201, "Failed");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        if (response.StatusCode == HttpStatusCode.BadRequest)
                        {
                            string resultSend = response.Content.ReadAsStringAsync().Result;
                            //page in develop
                            if (resultSend.IndexOf("Cannot message users who are not admins") >= 0)
                            {
                                def.meta = new Meta(202, "Page in Developement");
                                return Ok(def);
                            }
                            User user = db.Users.Find(shop.UserId);
                            if (user != null)
                            {
                                HttpClient httpClientToken = new HttpClient();
                                string urlLongLiveToken;
                                urlLongLiveToken = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=313601012371988&client_secret=a993e6c800e78daf7f299f15e933b588&fb_exchange_token=" + user.UserFacebookToken;
                                HttpResponseMessage responseToken = await httpClientToken.GetAsync(urlLongLiveToken);
                                httpClientToken.Dispose();
                                string resultToken = responseToken.Content.ReadAsStringAsync().Result;
                                string userLongTermToken = "";
                                if (resultToken != null && resultToken.Length > 0)
                                {
                                    LongTermToken token = JsonConvert.DeserializeObject<LongTermToken>(resultToken);
                                    userLongTermToken = token.access_token;
                                    HttpClient httpClientTokenPage = new HttpClient();
                                    responseToken = await httpClientTokenPage.GetAsync("https://graph.facebook.com/v2.8/me/accounts/?access_token=" + userLongTermToken);
                                    httpClientToken.Dispose();
                                    resultToken = responseToken.Content.ReadAsStringAsync().Result;
                                    log.Info("page long term token : " + resultToken);
                                    FBAccountResp resp = JsonConvert.DeserializeObject<FBAccountResp>(resultToken);
                                    if (resp != null)
                                    {
                                        if (resp.data != null && resp.data.Count > 0)
                                        {
                                            foreach (FBPage p in resp.data)
                                            {
                                                if (p.id == page.PageFacebookId)
                                                {
                                                    page.PageFacebookToken = p.access_token;
                                                    db.Entry(page).State = EntityState.Modified;
                                                    db.SaveChanges();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    //resend
                                    response = await httpClient.PostAsJsonAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/messages?access_token=" + page.PageFacebookToken, fbReq);
                                    httpClient.Dispose();
                                    string result = response.Content.ReadAsStringAsync().Result;
                                    FBSendMessageResp responseFB = JsonConvert.DeserializeObject<FBSendMessageResp>(result);
                                    if (responseFB != null && responseFB.message_id != null)
                                    {
                                        //resend ok
                                        conversation.LastMessage = req.message;
                                        conversation.UpdatedAt = DateTime.Now;
                                        conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.LastMessage);
                                        db.Entry(conversation).State = EntityState.Modified;
                                        db.SaveChanges();
                                        //create message
                                        Message messageDB = new Message();
                                        messageDB.Content = req.message;
                                        messageDB.ConversationId = conversation.ConversationId;
                                        messageDB.CreatedAt = DateTime.Now;
                                        messageDB.FacebookStatus = 2;
                                        messageDB.IsBotMessage = false;
                                        messageDB.RecipientId = req.facebook_user_id;
                                        messageDB.RecipientName = "";
                                        messageDB.SenderId = page.PageFacebookId;
                                        messageDB.SenderName = page.PageName;
                                        messageDB.Sequence = -1;
                                        messageDB.Status = 0;
                                        messageDB.Subject = "";
                                        messageDB.UpdatedAt = DateTime.Now;
                                        messageDB.EmployeeId = req.employee_id;
                                        messageDB.FacebookMessageId = responseFB.message_id;
                                        db.Messages.Add(messageDB);
                                        db.SaveChanges();
                                        //attachment
                                        if (req.image_url != null)
                                        {
                                            Attachment attachmentDB = new Attachment();
                                            attachmentDB.CreatedAt = DateTime.Now;
                                            attachmentDB.MessageId = messageDB.MessageId;
                                            attachmentDB.Type = "image";
                                            attachmentDB.Url = req.image_url;
                                            db.Attachments.Add(attachmentDB);
                                            db.SaveChanges();
                                        }
                                        def.meta = new Meta(200, "Success");
                                        def.data = messageDB.MessageId;
                                        return Ok(def);
                                    }
                                    else
                                    {
                                        def.meta = new Meta(203, "Invalid token");
                                        return Ok(def);
                                    }
                                }
                                else
                                {
                                    def.meta = new Meta(203, "Invalid token");
                                    return Ok(def);
                                }
                            }
                            else
                            {
                                def.meta = new Meta(404, "User Not Found");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            string result = response.Content.ReadAsStringAsync().Result;
                            log.Info("send not bad request : " + result);
                            def.meta = new Meta(201, "Failed");
                            return Ok(def);
                        }
                    }
                }
                else
                {
                    def.meta = new Meta(201, "Failed");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/sendMessageNew")]
        public async Task<IHttpActionResult> sendMessageNew([FromBody] NewSendMessageReq req)
        {
            DefaultResponse def = new DefaultResponse();    
            Conversation conversation = db.Conversations.Find(req.conversation_id);            
            if (conversation != null)
            {
                //create dummny message
                Message messageDB = new Message();
                messageDB.Content = req.message;
                messageDB.ConversationId = conversation.ConversationId;
                messageDB.CreatedAt = DateTime.Now;
                messageDB.FacebookStatus = 0;
                messageDB.IsBotMessage = false;
                messageDB.RecipientId = conversation.FacebookUserId;
                messageDB.RecipientName = conversation.Name;
                messageDB.SenderId = conversation.Page.PageFacebookId;
                messageDB.SenderName = conversation.Page.PageName;
                messageDB.Sequence = -1;
                messageDB.Status = 0;
                messageDB.Subject = "";
                messageDB.EmployeeId = req.employee_id;
                messageDB.FacebookMessageId = "";
                db.Messages.Add(messageDB);
                db.SaveChanges();
                //process send
                def.meta = new Meta(200, "Success");
                return Ok(def);            
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }
        private async void processSendMessage(Conversation conversation, Message messageDB, NewSendMessageReq req)
        {
            Models.Facebook.Attachment attachment = null;
            if (req.image_url != null)
            {
                Models.Facebook.Payload payload = new Models.Facebook.Payload();
                payload.is_reusable = false;
                if (req.image_url != null)
                    payload.url = req.image_url;
                attachment = new Models.Facebook.Attachment();
                attachment.payload = payload;
                attachment.type = "image";
            }
            Models.Facebook.Message message = new Models.Facebook.Message();
            if (req.message != null || req.message == "")
                message.text = req.message;
            if (req.image_url != null)
                message.attachment = attachment;
            Models.Facebook.Recipient recipient  = new Models.Facebook.Recipient();
            if (message.text != null || message.attachment != null)
            {
                recipient.id = conversation.FacebookUserId;
            }          

            //facebook send message object
            Models.Facebook.FBSendMessageReq fbReq = new Models.Facebook.FBSendMessageReq();
            fbReq.message = message;
            fbReq.recipient = recipient;
            //begin send
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.PostAsJsonAsync("https://graph.facebook.com/v2.8/" + conversation.Page.PageFacebookId + "/messages?access_token=" + conversation.Page.PageFacebookToken, fbReq);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                FBSendMessageResp responseFB = JsonConvert.DeserializeObject<FBSendMessageResp>(result);
                if (responseFB != null && responseFB.message_id != null)
                {
                    DateTime now = DateTime.Now;
                    //send ok
                    //create message                
                    conversation.LastMessage = req.message;
                    conversation.UpdatedAt = DateTime.Now;
                    conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.LastMessage);
                    conversation.Status = (int)Const.ConversationStatus.REPLIED;
                    db.Entry(conversation).State = EntityState.Modified;               
                    //create message                    
                    messageDB.FacebookMessageId = responseFB.message_id;
                    db.Messages.Add(messageDB);
                    db.SaveChanges();
                    //get message
                    httpClient = new HttpClient();
                    response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + "m_" + responseFB.message_id + "?fields=id,created_time&access_token=" + conversation.Page.PageFacebookToken);
                    result = response.Content.ReadAsStringAsync().Result;
                    FBGetMessageResp fbGetMessageResp = JsonConvert.DeserializeObject<FBGetMessageResp>(result);
                    if (fbGetMessageResp.id != null)
                    {
                        messageDB.UpdatedAt = fbGetMessageResp.created_time;
                    }
                    else
                    {
                        messageDB.UpdatedAt = now;
                    }
                    db.Entry(messageDB).State = EntityState.Modified;
                    if (req.image_url != null)
                    {
                        Attachment attachmentDB = new Attachment();
                        attachmentDB.CreatedAt = DateTime.Now;
                        attachmentDB.MessageId = messageDB.MessageId;
                        attachmentDB.Type = "image";
                        attachmentDB.Url = req.image_url;
                        db.Attachments.Add(attachmentDB);
                    }
                    db.SaveChanges();   
                }                
            }            
        }

        public async void autoReplyMessage(NewSendMessageReq req)
        {
            log.Info(req);
            //check conversation
            Conversation conversation = db.Conversations.Find(req.conversation_id);
            if (conversation != null)
            {
                Models.Facebook.Message message = new Models.Facebook.Message();
                if (req.message != null || req.message == "")
                    message.text = req.message;
                Models.Facebook.Recipient recipient;
                if (message.text != null || message.attachment != null)
                {
                    //recipient
                    recipient = new Models.Facebook.Recipient();
                    recipient.id = conversation.FacebookUserId;
                }
                else
                {
                    return;
                }

                //facebook send message object
                Models.Facebook.FBSendMessageReq fbReq = new Models.Facebook.FBSendMessageReq();
                fbReq.message = message;
                fbReq.recipient = recipient;
                //begin send
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.PostAsJsonAsync("https://graph.facebook.com/v2.8/" + conversation.Page.PageFacebookId + "/messages?access_token=" + conversation.Page.PageFacebookToken, fbReq);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    FBSendMessageResp responseFB = JsonConvert.DeserializeObject<FBSendMessageResp>(result);
                    if (responseFB != null && responseFB.message_id != null)
                    {
                        //send ok
                        //create message                            
                        conversation.LastMessage = req.message;
                        conversation.UpdatedAt = DateTime.Now;
                        conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.LastMessage);
                        conversation.Status = (int)Const.ConversationStatus.REPLIED;
                        db.Entry(conversation).State = EntityState.Modified;
                        db.SaveChanges();
                        Page page = db.Pages.Find(conversation.PageId);
                        //create message
                        Message messageDB = new Message();
                        messageDB.Content = req.message;
                        messageDB.ConversationId = conversation.ConversationId;
                        messageDB.CreatedAt = DateTime.Now;
                        messageDB.FacebookStatus = 2;
                        messageDB.IsBotMessage = true;
                        messageDB.RecipientId = conversation.FacebookUserId;
                        messageDB.RecipientName = conversation.Name;
                        messageDB.SenderId = page.PageFacebookId;
                        messageDB.SenderName = page.PageName;
                        messageDB.Sequence = -1;
                        messageDB.Status = 0;
                        messageDB.Subject = "";
                        messageDB.UpdatedAt = DateTime.Now;
                        messageDB.EmployeeId = req.employee_id;
                        messageDB.FacebookMessageId = responseFB.message_id;
                        db.Messages.Add(messageDB);
                        db.SaveChanges();
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {

                }
            }
            else
            {
                return;
            }
        }

        [HttpGet]
        [Route("api/facebook/likeComment/{id}")]
        public async Task<IHttpActionResult> likeComment(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Comment comment = db.Comments.Find(id);
            if (comment != null)
            {
                Conversation conversation = db.Conversations.Find(comment.ConversationId);
                if (conversation != null)
                {
                    //delete comment from facebook
                    HttpClient httpClient = new HttpClient();
                    HttpResponseMessage response;
                    if (comment.Status == 0 || comment.Status == 1)
                    {
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                        var content = new FormUrlEncodedContent(values);
                        response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "/likes", content);
                    }
                    else
                    {
                        response = await httpClient.DeleteAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "/likes?access_token=" + conversation.Page.PageFacebookToken);
                    }
                    httpClient.Dispose();
                    string result = response.Content.ReadAsStringAsync().Result;
                    DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                    if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                    {
                        //delete comment     
                        if (comment.Status == 0)
                        {
                            comment.Status = 10;
                            comment.FacebookStatus = 10;
                        }
                        else if (comment.Status == 1)
                        {
                            comment.Status = 11;
                            comment.FacebookStatus = 11;
                        }
                        else if (comment.Status == 10)
                        {
                            comment.Status = 0;
                            comment.FacebookStatus = 0;
                        }
                        else if (comment.Status == 11)
                        {
                            comment.Status = 1;
                            comment.FacebookStatus = 1;
                        }
                        db.Entry(comment).State = EntityState.Modified;
                        db.SaveChanges();
                        def.meta = new Meta(200, "Success");
                        def.data = comment.Status;
                        return Ok(def);
                    }
                    else
                    {
                        def.meta = new Meta(500, "Internal Server Error");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/facebook/likeReply/{id}")]
        public async Task<IHttpActionResult> likeReply(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Reply reply = db.Replies.Find(id);
            if (reply != null)
            {
                Comment comment = db.Comments.Find(reply.CommentId);
                if (comment != null)
                {
                    Conversation conversation = db.Conversations.Find(comment.ConversationId);
                    if (conversation != null)
                    {
                        //delete comment from facebook
                        HttpClient httpClient = new HttpClient();
                        HttpResponseMessage response;
                        if (reply.Status == 0 || reply.Status == 1)
                        {
                            List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                            values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                            var content = new FormUrlEncodedContent(values);
                            response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "/likes", content);
                        }
                        else
                        {
                            response = await httpClient.DeleteAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "/likes?access_token=" + conversation.Page.PageFacebookToken);
                        }
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                        if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                        {
                            //delete comment     
                            if (reply.Status == 0)
                            {
                                reply.Status = 10;
                                reply.FacebookStatus = 10;
                            }
                            else if (reply.Status == 1)
                            {
                                reply.Status = 11;
                                reply.FacebookStatus = 11;
                            }
                            else if (reply.Status == 10)
                            {
                                reply.Status = 0;
                                reply.FacebookStatus = 0;
                            }
                            else if (reply.Status == 11)
                            {
                                reply.Status = 1;
                                reply.FacebookStatus = 1;
                            }
                            db.Entry(reply).State = EntityState.Modified;
                            db.SaveChanges();
                            def.meta = new Meta(200, "Success");
                            def.data = reply.Status;
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(500, "Internal Server Error");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/blockUser")]
        public async Task<IHttpActionResult> blockUser(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();
            log.Info("block user: " + sRequest);
            //EditCommentReq req = JsonConvert.DeserializeObject<EditCommentReq>(sRequest);
            BlockUserReq req = JsonConvert.DeserializeObject<BlockUserReq>(sRequest);
            Page page = await db.Pages.Where(p => p.ShopId == req.shop_id && p.PageFacebookId == req.facebook_page_id).FirstOrDefaultAsync();
            if (page != null)
            {
                List<string> users = new List<string>();
                users.Add(req.facebook_user_id);
                HttpClient httpClient = new HttpClient();
                List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("asid", "[" + string.Join(",", users) + "]"));
                var content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + req.facebook_page_id + "/blocked?access_token=" + page.PageFacebookToken, content);
                httpClient.Dispose();
                string result = response.Content.ReadAsStringAsync().Result;
                log.Info("block user result: " + result);
                if (result.IndexOf("true") > -1)
                {
                    //delete comment     
                    //insert to block
                    //check exist
                    BlockList bl = db.BlockLists.Where(b => b.FacebookUserId == req.facebook_user_id && b.ShopId == req.shop_id).FirstOrDefault();
                    if (bl == null)
                    {
                        bl = new BlockList();
                        bl.ShopId = req.shop_id;
                        bl.FacebookUserId = req.facebook_user_id;
                        bl.Status = 0;
                        bl.CustomerId = -1;
                        db.BlockLists.Add(bl);
                        db.SaveChanges();
                    }
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(211, "Facebook request error");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Page not found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/unblockUser")]
        public async Task<IHttpActionResult> unblockUser(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();

            //EditCommentReq req = JsonConvert.DeserializeObject<EditCommentReq>(sRequest);
            BlockUserReq req = JsonConvert.DeserializeObject<BlockUserReq>(sRequest);

            Page page = await db.Pages.Where(p => p.ShopId == req.shop_id && p.PageFacebookId == req.facebook_page_id).FirstOrDefaultAsync();
            if (page != null)
            {
                HttpClient httpClient = new HttpClient();
                //HttpResponseMessage response = await httpClient.DeleteAsync("https://graph.facebook.com/v2.8/" + req.facebook_page_id + "/banned");
                //httpClient.Dispose();
                HttpRequestMessage httpRequest = new HttpRequestMessage
                {
                    Content = new StringContent("asid=" + req.facebook_user_id + "&access_token=" + page.PageFacebookToken, Encoding.UTF8, "text/plain"),
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri("https://graph.facebook.com/v2.8/" + req.facebook_page_id + "/blocked")
                };
                HttpResponseMessage response = await httpClient.SendAsync(httpRequest);
                string result = response.Content.ReadAsStringAsync().Result;
                DeleteResp deleteResp = JsonConvert.DeserializeObject<DeleteResp>(result);
                if (deleteResp != null && deleteResp.success != null && deleteResp.success)
                {
                    //delete comment     
                    //insert to block
                    string uid = req.facebook_user_id;
                    BlockList first = await db.BlockLists.Where(b => b.FacebookUserId == uid).FirstOrDefaultAsync();
                    first.Status = 99;
                    db.SaveChanges();
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(211, "Facebook request error");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Page Not found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/sendAction")]
        public async Task<IHttpActionResult> sendAction(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();
            SendActionReq req = JsonConvert.DeserializeObject<SendActionReq>(sRequest);

            Conversation conversation = db.Conversations.Find(req.conversation_id);
            if (conversation != null)
            {
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(404, "Conversation Not found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/facebook/sendPrivateMessage")]
        public async Task<IHttpActionResult> sendPrivateMessage(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();
            log.Info(sRequest);
            SendPrivateMessageReq req = JsonConvert.DeserializeObject<SendPrivateMessageReq>(sRequest);
            if (req.type == "reply")
            {
                Reply reply = db.Replies.Find(req.id);
                if (reply != null)
                {
                    //find comment 
                    Comment comment = db.Comments.Find(reply.CommentId);
                    if (comment != null)
                    {
                        //find conversation
                        Conversation conversation = db.Conversations.Find(comment.ConversationId);
                        if (conversation != null)
                        {
                            HttpClient httpClient = new HttpClient();
                            List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                            values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                            values.Add(new KeyValuePair<string, string>("message", req.message));
                            var content = new FormUrlEncodedContent(values);
                            HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + reply.FacebookCommentId + "/private_replies ", content);
                            httpClient.Dispose();
                            string result = response.Content.ReadAsStringAsync().Result;
                            log.Info(result);
                            FBOnlyIDResponse idResp = JsonConvert.DeserializeObject<FBOnlyIDResponse>(result);
                            if (idResp != null && idResp.id != null)
                            {
                                //send ok
                                conversation.IsPMSent = true;
                                //response comment                              
                                Message message = new Message();
                                message.Content = conversation.Page.PageName + " trả lời bình luận của bạn \"" + reply.Content + "\" : " + req.message;
                                message.ConversationId = conversation.ConversationId;
                                message.CreatedAt = DateTime.Now;
                                message.EmployeeId = -1;
                                message.FacebookMessageId = idResp.id;
                                message.FacebookStatus = 0;
                                message.HasAttachment = false;
                                message.IsBotMessage = false;
                                message.RecipientId = conversation.FBUserId;
                                message.RecipientName = conversation.Name;
                                message.SenderId = conversation.Page.PageFacebookId;
                                message.SenderName = conversation.Page.PageName;
                                message.Sequence = 0;
                                message.Status = 0;
                                message.Subject = "";
                                message.UpdatedAt = DateTime.Now;
                                db.Messages.Add(message);
                                db.SaveChanges();
                                def.meta = new Meta(200, "Success");
                                return Ok(def);
                            }
                            else
                            {
                                def.meta = new Meta(210, "Sent!");
                                return Ok(def);
                            }
                        }
                        else
                        {
                            def.meta = new Meta(404, "Not Found");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
            else
            {
                Comment comment = db.Comments.Find(req.id);
                if (comment != null)
                {
                    //find conversation
                    Conversation conversation = db.Conversations.Find(comment.ConversationId);
                    if (conversation != null)
                    {
                        HttpClient httpClient = new HttpClient();
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", conversation.Page.PageFacebookToken));
                        values.Add(new KeyValuePair<string, string>("message", req.message));
                        var content = new FormUrlEncodedContent(values);
                        HttpResponseMessage response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + comment.FacebookCommentId + "/private_replies", content);
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        log.Info(result);
                        FBOnlyIDResponse idResp = JsonConvert.DeserializeObject<FBOnlyIDResponse>(result);
                        if (idResp != null && idResp.id != null)
                        {
                            //send ok
                            conversation.IsPMSent = true;
                            Message message = new Message();
                            message.Content = conversation.Page.PageName + " trả lời bình luận của bạn \"" + comment.Content + "\" : " + req.message;
                            message.ConversationId = conversation.ConversationId;
                            message.CreatedAt = DateTime.Now;
                            message.EmployeeId = -1;
                            message.FacebookMessageId = idResp.id;
                            message.FacebookStatus = 0;
                            message.HasAttachment = false;
                            message.IsBotMessage = false;
                            message.RecipientId = conversation.FBUserId;
                            message.RecipientName = conversation.Name;
                            message.SenderId = conversation.Page.PageFacebookId;
                            message.SenderName = conversation.Page.PageName;
                            message.Sequence = 0;
                            message.Status = 0;
                            message.Subject = "";
                            message.UpdatedAt = DateTime.Now;
                            db.Messages.Add(message);
                            db.SaveChanges();
                            def.meta = new Meta(200, "Success");
                            return Ok(def);
                        }
                        else
                        {
                            def.meta = new Meta(500, "Internal Server Error");
                            return Ok(def);
                        }
                    }
                    else
                    {
                        def.meta = new Meta(404, "Not Found");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(404, "Not Found");
                    return Ok(def);
                }
            }
        }

        [HttpPost]
        [Route("api/facebook/loadPageContent")]
        public async Task<IHttpActionResult> loadPageContent(HttpRequestMessage request)
        {
            DefaultResponse def = new DefaultResponse();
            StreamReader reader = new StreamReader(request.Content.ReadAsStreamAsync().Result);
            string sRequest = reader.ReadToEnd().Trim();
            log.Info(sRequest);
            LoadPageContentReq req = JsonConvert.DeserializeObject<LoadPageContentReq>(sRequest);
            DateTime since;
            try
            {
                since = DateTime.ParseExact(req.since, "ddMMyyyyHHmm", null);
            }
            catch (Exception ex)
            {
                since = DateTime.Now.AddDays(-7);
            }
            Page page = db.Pages.Find(req.page_id);

            if (page != null)
            {
                //find facebook user id
                //get messages
                try
                {
                    bool isNext = true;
                    string query = "https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/conversations?fields=id,senders,messages.limit(100){created_time,attachments{file_url,image_data,name},message,id,from,to},updated_time&access_token=" + page.PageFacebookToken;
                    do
                    {
                        HttpClient httpClient = new HttpClient();
                        HttpResponseMessage response = await httpClient.GetAsync(query);
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        log.Info(result);
                        FBConv fbConv = JsonConvert.DeserializeObject<FBConv>(result);
                        if (fbConv == null)
                        {
                            FBErrorResp resp = JsonConvert.DeserializeObject<FBErrorResp>(result);
                            if (resp.error.code == 190)
                            {
                                def.meta = new Meta(210, "Token expired, please login Facebook to get new token!");
                                return Ok(def);
                            }
                            else
                            {
                                def.meta = new Meta(500, "Can not process request");
                                return Ok(def);
                            }
                        }
                        if (fbConv.data != null && fbConv.data.Count > 0)
                        {
                            for (int i = 0; i < fbConv.data.Count; i++)
                            {
                                FBConvItem convItem = fbConv.data[i];
                                //find user facebook id
                                string realFbUserId = "";
                                string realFbName = "";
                                for (int j = 0; j < convItem.senders.data.Count; j++)
                                {
                                    if (convItem.senders.data[j].name != page.PageFacebookId)
                                    {
                                        realFbUserId = convItem.senders.data[j].id;
                                        realFbName = convItem.senders.data[j].name;
                                        break;
                                    }
                                }
                                //check updated at                   
                                if (DateTime.Compare(since, convItem.updated_time) <= 0)
                                {
                                    //find non empty last message
                                    string lastMessage = "";
                                    for (int j = 0; j < convItem.messages.data.Count; j++)
                                    {
                                        if (convItem.messages.data[j].message != "")
                                        {
                                            lastMessage = convItem.messages.data[j].message;
                                        }
                                        else if (convItem.messages.data[j].attachments != null && convItem.messages.data[j].attachments.data[0].image_data.image_type != null)
                                        {
                                            //gửi ảnh
                                            if (convItem.messages.data[j].from.id == page.PageFacebookId)
                                            {
                                                lastMessage = "Bạn đã gửi ảnh cho " + convItem.messages.data[j].to.data[0].name;
                                            }
                                            else
                                            {
                                                lastMessage = convItem.messages.data[j].from.name + " đã gửi ảnh cho bạn";
                                            }
                                        }
                                        else
                                        {
                                            lastMessage = "";
                                        }
                                    }
                                    log.Info(realFbUserId + "-" + realFbName + "-" + lastMessage);
                                    //check if conversation exist
                                    try
                                    {
                                        Conversation conversation = db.Conversations.Where(c => c.FBUserId == realFbUserId && c.ShopId == page.ShopId && c.Type == 0 && c.PageId == page.PageId).FirstOrDefault();
                                        if (conversation == null)
                                        {
                                            //create new conversation
                                            conversation = new Conversation();
                                            conversation.CreatedAt = DateTime.Now;
                                            conversation.FacebookUserId = "";
                                            conversation.FBUserId = realFbUserId;
                                            conversation.Name = "";
                                            conversation.Note = "";
                                            conversation.PageId = page.PageId;
                                            conversation.ShopId = page.ShopId;
                                            conversation.Status = 0;
                                            conversation.Tag = "";
                                            conversation.Type = 0;
                                            conversation.UpdatedAt = convItem.updated_time;
                                            conversation.LastMessageTime = convItem.updated_time;
                                            conversation.Type = 0;
                                            conversation.LastMessage = lastMessage;
                                            conversation.SearchQuery = Utils.unsignString(conversation.LastMessage);
                                            //get user info                                    
                                            conversation.Name = realFbName; ;
                                            conversation.UserAvatar = "http://graph.facebook.com/" + realFbUserId + "/picture?type=square";
                                            conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                            db.Entry(conversation).State = EntityState.Added;
                                            db.Conversations.Add(conversation);
                                        }
                                        else
                                        {
                                            //do not change anything
                                        }
                                        //map for customer
                                        if (conversation.CustomerId == null || conversation.CustomerId == -1)
                                        {
                                            //map customer
                                            Customer customer = db.Customers.Where(c => c.FacebookUserId == conversation.FBUserId).FirstOrDefault();
                                            if (customer != null)
                                            {
                                                conversation.CustomerId = customer.CustomerId;
                                            }
                                        }
                                        // ok
                                        // load message
                                        for (int j = 0; j < convItem.messages.data.Count; j++)
                                        {
                                            //check date
                                            if (DateTime.Compare(since, convItem.messages.data[j].created_time) <= 0)
                                            {
                                                //check exist
                                                string mid = convItem.messages.data[j].id.Replace("m_", "");
                                                Message message = db.Messages.Where(m => m.FacebookMessageId == mid).FirstOrDefault();
                                                if (message == null)
                                                {
                                                    message = new Message();
                                                    message.Content = convItem.messages.data[j].message;
                                                    message.ConversationId = conversation.ConversationId;
                                                    message.CreatedAt = convItem.messages.data[j].created_time;
                                                    message.EmployeeId = null;
                                                    message.FacebookMessageId = mid;
                                                    message.FacebookStatus = 0;
                                                    message.HasAttachment = convItem.messages.data[j].attachments != null ? true : false;
                                                    message.IsBotMessage = false;
                                                    message.RecipientId = convItem.messages.data[j].to.data[0].id;
                                                    message.RecipientName = convItem.messages.data[j].to.data[0].name;
                                                    message.SenderId = convItem.messages.data[j].from.id;
                                                    message.SenderName = convItem.messages.data[j].from.name;
                                                    message.Sequence = 0;
                                                    message.Status = 0;
                                                    message.Subject = "";
                                                    message.UpdatedAt = convItem.messages.data[j].created_time;
                                                    db.Messages.Add(message);
                                                    db.SaveChanges();
                                                    //attachment
                                                    if (convItem.messages.data[j].attachments != null)
                                                    {
                                                        List<Attachment> attachments = new List<Attachment>();
                                                        for (int k = 0; k < convItem.messages.data[j].attachments.data.Count; k++)
                                                        {
                                                            if (convItem.messages.data[j].attachments.data[k].image_data.image_type == 1)
                                                            {
                                                                Attachment attachment = new Attachment();
                                                                attachment.CreatedAt = DateTime.Now;
                                                                attachment.MessageId = message.MessageId;
                                                                attachment.Type = "image";
                                                                attachment.Url = convItem.messages.data[j].attachments.data[k].image_data.url;
                                                                attachments.Add(attachment);
                                                                //
                                                            }
                                                        }
                                                        db.Attachments.AddRange(attachments);
                                                        db.SaveChangesAsync();
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        log.Info(ex.Message + "-" + ex.StackTrace);
                                        continue;
                                    }
                                }
                            }
                            var lastDate = fbConv.data[fbConv.data.Count - 1].updated_time;
                            if (DateTime.Compare(since, lastDate) <= 0)
                            {
                                isNext = true;
                                query = fbConv.paging.next;
                                if (query == null)
                                    isNext = false;
                            }
                            else
                                isNext = false;
                        }
                        else
                        {
                            //emply
                            isNext = false;
                        }
                    }
                    while (isNext);
                }
                catch (Exception ex)
                {
                    log.Info(ex.Message + "-" + ex.StackTrace);
                }

                //get comment
                bool isCommentNext = true;
                string queryComment = "https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/posts?fields=comments.limit(50){message,from,is_hidden,id,attachment,created_time,comments.limit(50){created_time,message,from,id,attachment,is_hidden}},caption,story,full_picture,message,updated_time&limit=100&since=" + since.ToString("dd-MM-yyyy") + "&limit=100&access_token=" + page.PageFacebookToken;
                log.Info("queryComment :" + queryComment);
                do
                {
                    HttpClient httpClient = new HttpClient();
                    HttpResponseMessage response = await httpClient.GetAsync(queryComment);
                    httpClient.Dispose();
                    string result = response.Content.ReadAsStringAsync().Result;
                    log.Info("posts :" + result);
                    FBPostResp posts = JsonConvert.DeserializeObject<FBPostResp>(result);
                    if (posts != null && posts.data != null && posts.data.Count > 0)
                    {
                        for (int i = 0; i < posts.data.Count; i++)
                        {
                            //check exist post
                            string post_id = posts.data[i].id;
                            Post post = db.Posts.Where(p => p.FacebookPostId == post_id && p.ShopId == page.ShopId).FirstOrDefault();
                            if (post == null)
                            {
                                post = new Post();
                                post.Content = posts.data[i].message != null ? posts.data[i].message : posts.data[i].story;
                                post.FacebookPostId = posts.data[i].id;
                                post.FacebookPostUrl = "https://facebook.com/" + posts.data[i].id;
                                post.Image = posts.data[i].full_picture;
                                post.PageId = page.PageId;
                                post.ShopId = page.ShopId;
                                post.Status = 0;
                                post.Title = "";
                                post.CreatedDate = posts.data[i].updated_time;
                                post.UpdatedAt = posts.data[i].updated_time;
                                post.ShowComment = 0;
                                db.Posts.Add(post);
                            }
                            // comment
                            if (posts.data[i].comments != null && posts.data[i].comments.data != null && posts.data[i].comments.data.Count > 0)
                            {
                                for (int j = 0; j < posts.data[i].comments.data.Count; j++)
                                {
                                    string comment_id = posts.data[i].comments.data[j].id;
                                    string idFrom = posts.data[i].comments.data[j].from.id;
                                    string nameFrom = posts.data[i].comments.data[j].from.name;
                                    //ignore page comment
                                    //if (idFrom == page.PageFacebookId)
                                    //    continue;
                                    //check conversation
                                    string lastMessage = "";
                                    Nullable<DateTime> lastMessageTime = DateTime.Now;
                                    Conversation conversation = db.Conversations.Where(cv => cv.FacebookPostId == post.FacebookPostId && cv.FacebookCommentId == comment_id && cv.FacebookUserId == idFrom && cv.ShopId == page.ShopId).FirstOrDefault();
                                    if (conversation == null)
                                    {
                                        //create new conversation
                                        conversation = new Conversation();
                                        conversation.CreatedAt = DateTime.Now;
                                        conversation.CustomerId = -1;
                                        conversation.FacebookCommentId = comment_id;
                                        conversation.FacebookPostId = post.FacebookPostId;
                                        conversation.FacebookUserId = idFrom;
                                        conversation.FBUserId = idFrom;
                                        conversation.IsPMSent = false;
                                        conversation.LastMessage = "";
                                        conversation.LastMessageTime = post.UpdatedAt;
                                        conversation.Name = nameFrom;
                                        conversation.Note = "";
                                        conversation.PageId = page.PageId;
                                        conversation.Phone = "";
                                        conversation.SearchQuery = "";
                                        conversation.ShopId = page.ShopId;
                                        conversation.Status = 0;
                                        conversation.Tag = "";
                                        conversation.Type = 1;
                                        conversation.UpdatedAt = post.UpdatedAt;
                                        conversation.UserAvatar = "http://graph.facebook.com/" + idFrom + "/picture?type=square";
                                        db.Conversations.Add(conversation);
                                        db.SaveChanges();
                                    }
                                    //check comment exist
                                    Comment comment = db.Comments.Where(c => c.FacebookCommentId == comment_id).FirstOrDefault();
                                    if (comment == null)
                                    {
                                        comment = new Comment();
                                        comment.Content = posts.data[i].comments.data[j].message;
                                        comment.ConversationId = conversation.ConversationId;
                                        comment.CreatedAt = DateTime.Now;
                                        comment.CustomerId = -1;
                                        comment.FacebookCommentId = posts.data[i].comments.data[j].id;
                                        comment.FacebookStatus = posts.data[i].comments.data[j].is_hidden ? 1 : 0;
                                        comment.Photo = posts.data[i].comments.data[j].attachment != null ? posts.data[i].comments.data[j].attachment.media.image.src : "";
                                        comment.PostId = post.PostId;
                                        comment.SenderId = idFrom;
                                        comment.SenderName = nameFrom;
                                        comment.Status = posts.data[i].comments.data[j].is_hidden ? 1 : 0;
                                        comment.UpdatedAt = posts.data[i].comments.data[j].created_time;
                                        db.Comments.Add(comment);
                                        db.SaveChanges();
                                        if (comment.Content == "")
                                            if (idFrom == page.PageFacebookId)
                                                lastMessage = "Bạn đã gửi ảnh.";
                                            else
                                                lastMessage = nameFrom + " đã gửi ảnh cho bạn.";
                                        else
                                            lastMessage = comment.Content;
                                        lastMessageTime = comment.UpdatedAt;

                                        //attachment
                                        //end attachment
                                        //reply
                                        if (posts.data[i].comments.data[j].comments != null && posts.data[i].comments.data[j].comments.data != null && posts.data[i].comments.data[j].comments.data.Count > 0)
                                        {
                                            //check duplicate reply
                                            for (int k = 0; k < posts.data[i].comments.data[j].comments.data.Count; k++)
                                            {
                                                string reply_id = posts.data[i].comments.data[j].comments.data[k].id;
                                                string idFromReply = posts.data[i].comments.data[j].comments.data[k].from.id;
                                                string nameFromReply = posts.data[i].comments.data[j].comments.data[k].from.name;
                                                Reply reply = db.Replies.Where(r => r.FacebookCommentId == reply_id).FirstOrDefault();
                                                if (reply == null)
                                                {
                                                    reply = new Reply();
                                                    reply.CommentId = comment.CommentId;
                                                    reply.Content = posts.data[i].comments.data[j].comments.data[k].message;
                                                    reply.CreatedAt = DateTime.Now;
                                                    reply.CustomerId = -1;
                                                    reply.EmployeeId = -1;
                                                    reply.FacebookCommentId = reply_id;
                                                    reply.FacebookStatus = posts.data[i].comments.data[j].comments.data[k].is_hidden ? 1 : 0;
                                                    reply.Photo = posts.data[i].comments.data[j].comments.data[k].attachment != null ? posts.data[i].comments.data[j].comments.data[k].attachment.media.image.src : "";
                                                    reply.SenderId = idFromReply;
                                                    reply.SenderName = nameFromReply;
                                                    reply.Status = posts.data[i].comments.data[j].comments.data[k].is_hidden ? 1 : 0;
                                                    reply.UpdatedAt = posts.data[i].comments.data[j].comments.data[k].created_time;
                                                    db.Replies.Add(reply);
                                                    db.SaveChanges();
                                                    if (reply.Content == "")
                                                        if (idFrom == page.PageFacebookId)
                                                            lastMessage = "Bạn đã gửi ảnh.";
                                                        else
                                                            lastMessage = nameFrom + " đã gửi ảnh cho bạn.";
                                                    else
                                                        lastMessage = reply.Content;
                                                    lastMessageTime = reply.UpdatedAt;
                                                }
                                            }
                                        }
                                        //end reply
                                    }
                                    //update conversation                                    
                                    conversation.LastMessage = lastMessage;
                                    conversation.LastMessageTime = lastMessageTime;
                                    conversation.UpdatedAt = lastMessageTime;
                                    conversation.SearchQuery = Utils.unsignString(conversation.Name + " " + conversation.Phone + " " + conversation.LastMessage);
                                    db.Entry(conversation).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    isCommentNext = false;
                }
                while (isCommentNext);
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/conversations/{id}/read")]
        public IHttpActionResult readConversation(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Conversation c = db.Conversations.Find(id);
            if (c != null)
            {
                c.Status = (int)Const.ConversationStatus.READ;
                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(404, "Not found");
                return Ok(def);
            }
        }
    }
}

