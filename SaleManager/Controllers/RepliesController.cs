﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class RepliesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/Replies
        public IHttpActionResult GetReplies()
        {
            DefaultResponse def = new DefaultResponse();
            def.meta = new Meta(200, "Success");
            def.data = db.Replies.Select(c => new ReplyDTO()
            {
                ReplyId = c.ReplyId,
                CommentId = c.CommentId,
                Content = c.Content,
                CreatedAt = c.CreatedAt,
                CustomerId = c.CustomerId,
                FacebookCommentId = c.FacebookCommentId,
                FacebookStatus = c.FacebookStatus,
                Photo = c.Photo,              
                SenderId = c.SenderId,
                SenderName = c.SenderName,
                Status = c.Status,
                UpdatedAt = c.UpdatedAt,
                EmployeeId = c.EmployeeId
            });
            return Ok(def);
        }

        [Route("api/replies/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Reply> replies;
                if (paging.order_by != null)
                {
                    replies = db.Replies.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    replies = db.Replies.OrderBy("UpdatedAt desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                if (paging.query != null)
                    replies = replies.Where(HttpUtility.UrlDecode(paging.query));
                def.data = replies.Select(c => new ReplyDTO()
                {
                    CommentId = c.CommentId,
                    Content = c.Content,
                    CreatedAt = c.CreatedAt,
                    CustomerId = c.CustomerId,
                    FacebookCommentId = c.FacebookCommentId,
                    FacebookStatus = c.FacebookStatus,
                    Photo = c.Photo,
                    ReplyId = c.ReplyId,
                    SenderId = c.SenderId,
                    SenderName = c.SenderName,
                    Status = c.Status,
                    UpdatedAt = c.UpdatedAt,
                    EmployeeId = c.EmployeeId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Replies/5
        [ResponseType(typeof(Reply))]
        public IHttpActionResult GetReply(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ReplyDTO reply = db.Replies.Where(c => c.ReplyId == id).Select(c => new ReplyDTO()
            {
                CommentId = c.CommentId,
                Content = c.Content,
                CreatedAt = c.CreatedAt,
                CustomerId = c.CustomerId,
                FacebookCommentId = c.FacebookCommentId,
                FacebookStatus = c.FacebookStatus,
                Photo = c.Photo,
                ReplyId = c.ReplyId,
                SenderId = c.SenderId,
                SenderName = c.SenderName,
                Status = c.Status,
                UpdatedAt = c.UpdatedAt,
                EmployeeId = c.EmployeeId
            }).FirstOrDefault();
            if (reply == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = reply;
            return Ok(def);
        }

        // PUT: api/Replies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReply(int id, ReplyDTO reply)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != reply.ReplyId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Reply current = db.Replies.Where(a => a.ReplyId == id).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Content = reply.Content;
                current.CustomerId = reply.CustomerId;
                current.FacebookCommentId = reply.FacebookCommentId;
                current.FacebookStatus = reply.FacebookStatus;
                current.Photo = reply.Photo;
                current.SenderName = reply.SenderName;
                current.Status = reply.Status;
                current.UpdatedAt = DateTime.Now;
                
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            } 
        }

        // POST: api/Replies
        [ResponseType(typeof(Reply))]
        public IHttpActionResult PostReply(ReplyDTO reply)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Reply c = new Reply();
            c.Content = reply.Content;
            c.CreatedAt = DateTime.Now;
            c.CustomerId = reply.CustomerId;
            c.FacebookCommentId = reply.FacebookCommentId;
            c.FacebookStatus = reply.FacebookStatus;
            c.Photo = reply.Photo;
            c.CommentId = reply.CommentId;
            c.SenderId = reply.SenderId;
            c.SenderName = reply.SenderName;
            c.Status = reply.Status;
            c.UpdatedAt = DateTime.Now;
            c.EmployeeId = reply.EmployeeId;
            db.Replies.Add(c);
            db.SaveChanges();
            reply.ReplyId = c.ReplyId;

            def.meta = new Meta(200, "Success");
            def.data = reply;

            return Ok(def);
        }

        // DELETE: api/Replies/5
        [ResponseType(typeof(Reply))]
        public IHttpActionResult DeleteReply(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Reply reply = db.Replies.Find(id);
            if (reply == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            //delete all replies belong to comment        
            db.Replies.Remove(reply);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReplyExists(int id)
        {
            return db.Replies.Count(e => e.ReplyId == id) > 0;
        }
    }
}