﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ShopCategoriesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/ShopCategories
        public IHttpActionResult GetShopCategories()
        {
            DefaultResponse def = new DefaultResponse();
            def.meta = new Meta(200, "Success");
            def.data = db.ShopCategories.Select(s => new ShopCategoryDTO()
            {
                CreatedDate = s.CreatedDate,
                Description = s.Description,
                Image = s.Image,
                Name = s.Name,
                Priority = s.Priority,
                ShopCategoryId = s.ShopCategoryId,
                Status = s.Status
            });
            return Ok(def);
        }

        [Route("api/shopcategories/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<ShopCategory> shops;
                if (paging.order_by != null)
                {
                    shops = db.ShopCategories.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    shops = db.ShopCategories.OrderBy("ShopCategoryId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                if (paging.query != null)
                    shops = shops.Where(HttpUtility.UrlDecode(paging.query));
                def.data = shops.Select(s => new ShopCategoryDTO()
                {
                    CreatedDate = s.CreatedDate,
                    Description = s.Description,
                    Image = s.Image,
                    Name = s.Name,
                    Priority = s.Priority,
                    ShopCategoryId = s.ShopCategoryId,
                    Status = s.Status
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/ShopCategories/5       
        public IHttpActionResult GetShopCategory(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ShopCategoryDTO shopCategory = db.ShopCategories.Where(s => s.ShopCategoryId == id).Select(s => new ShopCategoryDTO()
                {
                    CreatedDate = s.CreatedDate,
                    Description = s.Description,
                    Image = s.Image,
                    Name = s.Name,
                    Priority = s.Priority,
                    ShopCategoryId = s.ShopCategoryId,
                    Status = s.Status
                }).FirstOrDefault();
            if (shopCategory == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = shopCategory;
            return Ok(def);
        }

        // PUT: api/ShopCategories/5        
        public IHttpActionResult PutShopCategory(int id, ShopCategoryDTO shopCategory)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != shopCategory.ShopCategoryId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            ShopCategory current = db.ShopCategories.Where(s => s.ShopCategoryId == id).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Image = shopCategory.Image;
                current.Name = shopCategory.Name;
                current.Priority = shopCategory.Priority;
                current.Status = shopCategory.Status;
                current.Description = shopCategory.Description;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }   
        }

        // POST: api/ShopCategories       
        public IHttpActionResult PostShopCategory(ShopCategoryDTO shopCategory)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            ShopCategory c = new ShopCategory();
            c.CreatedDate = DateTime.Now;
            c.Description = shopCategory.Description;
            c.Image = shopCategory.Image;
            c.Name = shopCategory.Name;
            c.Priority = shopCategory.Priority;
            c.Status = shopCategory.Status;

            db.ShopCategories.Add(c);
            db.SaveChanges();
            shopCategory.ShopCategoryId = c.ShopCategoryId;

            def.meta = new Meta(200, "Success");
            def.data = shopCategory;

            return Ok(def);
        }

        // DELETE: api/ShopCategories/5        
        public IHttpActionResult DeleteShopCategory(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ShopCategory shopCategory = db.ShopCategories.Find(id);
            if (shopCategory == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.ShopCategories.Remove(shopCategory);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShopCategoryExists(int id)
        {
            return db.ShopCategories.Count(e => e.ShopCategoryId == id) > 0;
        }
    }
}