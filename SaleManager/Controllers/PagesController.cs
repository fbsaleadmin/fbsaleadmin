﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Claims;
using log4net;

namespace SaleManager.Controllers
{
    [Authorize]
    public class PagesController : ApiController
    {
        private static readonly ILog log = LogMaster.GetLogger("page", "page");
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly string facebook_app_id = "313601012371988";
        private static readonly string facebook_app_secret_key = "a993e6c800e78daf7f299f15e933b588";

        [Route("api/pages/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Page> pages = db.Pages;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    pages = pages.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    pages = pages.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    pages = pages.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    pages = pages.OrderBy("PageId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = pages.Select(p => new PageDTO()
                {
                    CreatedAt = p.CreatedAt,
                    PageFacebookId = p.PageFacebookId,
                    PageFacebookToken = p.PageFacebookToken,
                    PageId = p.PageId,
                    PageName = p.PageName,
                    ShopId = p.ShopId,
                    Status = p.Status,
                    UpdatedAt = p.UpdatedAt
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Pages/5   
        public IHttpActionResult GetPage(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            PageDTO page = db.Pages.Where(p => p.PageId == id).Select(p => new PageDTO()
            {
                CreatedAt = p.CreatedAt,
                PageFacebookId = p.PageFacebookId,
                PageFacebookToken = p.PageFacebookToken,
                PageId = p.PageId,
                PageName = p.PageName,
                ShopId = p.ShopId,
                Status = p.Status,
                UpdatedAt = p.UpdatedAt
            }).FirstOrDefault();
            if (page == null || page.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = page;
            return Ok(def);
        }

        // PUT: api/Pages/5     
        public IHttpActionResult PutPage(int id, PageDTO page)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != page.PageId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Page current = db.Pages.Where(s => s.PageId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.PageFacebookId = page.PageFacebookId;
                current.PageFacebookToken = page.PageFacebookToken;
                current.PageName = page.PageName;
                current.Status = page.Status;
                current.UpdatedAt = DateTime.Now;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/Pages
        public async Task<IHttpActionResult> PostPage(PageDTO pageDTO)
        {
            log.Info("postpage :" + JsonConvert.SerializeObject(pageDTO));
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //log.Info("postpage ShopId:" + ShopId);
            db = new SalesManagerEntities();
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            //check token
            //if (pageDTO.PageFacebookToken == null || pageDTO.PageFacebookToken.Trim() == "")
            //{
            //    def.meta = new Meta(211, "Invalid page access token");
            //    return Ok(def);
            //}
            //check exist facebook userid
            try
            {
                Page page = db.Pages.Where(p => p.PageFacebookId == pageDTO.PageFacebookId && p.IsSubscribed == true).FirstOrDefault();

                //subcriber exist
                if (page != null && page.ShopId == ShopId)
                {
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    //if page subscribed
                    if (page != null && page.IsSubscribed.HasValue && page.IsSubscribed.Value)
                    {
                        def.meta = new Meta(212, "Page subcribed to another shop");
                        return Ok(def);
                    }
                    //no shop subcribed to this page
                    //check exist page
                    //exchange token       
                    try
                    {
                        page = db.Pages.Where(p => p.PageFacebookId == pageDTO.PageFacebookId && p.ShopId == ShopId).FirstOrDefault();
                        if (page == null)
                        {
                            page = new Page();
                            page.CreatedAt = DateTime.Now;
                            page.PageFacebookId = pageDTO.PageFacebookId;
                            page.PageFacebookToken = pageDTO.PageFacebookToken;
                            page.PageName = pageDTO.PageName;
                            page.ShopId = pageDTO.ShopId;
                            page.Status = 0;
                            page.UpdatedAt = DateTime.Now;
                            page.IsSubscribed = false;
                            db.Pages.Add(page);
                            db.SaveChanges();
                            pageDTO.PageId = page.PageId;
                        }                       
                        HttpClient httpClient = new HttpClient();
                        string token = "";
                        if (page != null)
                            token = page.PageFacebookToken;
                        else
                            token = pageDTO.PageFacebookToken;
                        string exchangeUrl = "https://graph.facebook.com/oauth/access_token?client_id=" + facebook_app_id + "&client_secret=" + facebook_app_secret_key + "&grant_type=fb_exchange_token&fb_exchange_token=" + token;
                        log.Info(exchangeUrl);
                        HttpResponseMessage response = await httpClient.GetAsync(exchangeUrl);
                        httpClient.Dispose();
                        string result = response.Content.ReadAsStringAsync().Result;
                        ExchangeTokenResp exchange = JsonConvert.DeserializeObject<ExchangeTokenResp>(result);
                        if (exchange.access_token == null || exchange.access_token == "")
                        {
                            //failed to exchange
                            log.Info(result);
                            def.meta = new Meta(211, "Failed to exchange token");
                            return Ok(def);
                        }
                        page.PageFacebookToken = exchange.access_token;
                        db.SaveChanges();
                        httpClient = new HttpClient();
                        List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                        values.Add(new KeyValuePair<string, string>("access_token", page.PageFacebookToken));
                        var content = new FormUrlEncodedContent(values);
                        response = await httpClient.PostAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/subscribed_apps", content);
                        httpClient.Dispose();
                        result = response.Content.ReadAsStringAsync().Result;
                        log.Info(result);
                        DeleteResp subResult = JsonConvert.DeserializeObject<DeleteResp>(result);
                        if (subResult != null && subResult.success)
                        {
                            page.IsSubscribed = true;
                            db.Entry(page).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            FBErrorResp resp = JsonConvert.DeserializeObject<FBErrorResp>(result);
                            if (resp != null && resp.error != null)
                            {
                                def.meta = new Meta(213, "Failed to subscribed to page");
                                return Ok(def);
                            }
                            else
                            {
                                def.meta = new Meta(500, "Internal error");
                                return Ok(def);
                            }
                        }
                        //fetch all post   
                        httpClient = new HttpClient();
                        response = await httpClient.GetAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/posts?fields=full_picture,message,story,updated_time&limit=1000&access_token=" + page.PageFacebookToken);
                        httpClient.Dispose();
                        result = response.Content.ReadAsStringAsync().Result;
                        FacebookPostResponse posts = JsonConvert.DeserializeObject<FacebookPostResponse>(result);
                        if (posts != null && posts.data != null)
                        {
                            foreach (FacebookPost _page in posts.data)
                            {
                                //check post exist
                                int exists = db.Posts.Where(d => d.FacebookPostId == _page.id).Count();
                                if (exists > 0 || _page.story.Contains("their profile") || _page.story.Contains("their cover"))
                                    continue;
                                Post post = new Post();
                                post.Content = _page.message != null ? _page.message : _page.story;
                                post.FacebookPostId = _page.id;
                                post.FacebookPostUrl = "https://facebook.com/" + _page.id;
                                post.Image = _page.full_picture;
                                post.PageId = page.PageId;
                                post.ShopId = page.ShopId;
                                post.Status = 0;
                                post.Title = "";
                                post.CreatedDate = DateTime.Now;
                                post.UpdatedAt = _page.updated_time;
                                post.ShowComment = 0;
                                db.Posts.Add(post);
                            }
                        }
                        pageDTO.PageId = page.PageId;
                        def.meta = new Meta(200, "Success");
                        def.data = pageDTO;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        def.meta = new Meta(500, "Error");
                        log.Info(ex.Message + "-" + ex.StackTrace);
                    }

                }
            }
            catch (Exception ex)
            {
                log.Info(ex.Message + "-" + ex.StackTrace);
            }
            return Ok(def);
        }

        [HttpPost]
        [Route("api/pages/{id}/removeSubscribed")]
        public async Task<IHttpActionResult> RemoveSubscribe(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Page page = db.Pages.Find(id);
            if (page != null)
            {
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.DeleteAsync("https://graph.facebook.com/v2.8/" + page.PageFacebookId + "/subscribed_apps?access_token=" + page.PageFacebookToken);
                httpClient.Dispose();
                string result = response.Content.ReadAsStringAsync().Result;
                log.Info("unsub :" + result);
                DeleteResp subResult = JsonConvert.DeserializeObject<DeleteResp>(result);
                if (subResult != null && subResult.success)
                {
                    page.IsSubscribed = false;
                    db.Entry(page).State = EntityState.Modified;
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(201, "Failed");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Page not found");
                return Ok(def);
            }
        }

        // DELETE: api/Pages/5     
        public IHttpActionResult DeletePage(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Page page = db.Pages.Find(id);
            if (page == null || page.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.Pages.Remove(page);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpPost]
        [Route("api/pages/UpdatePageToken")]
        public IHttpActionResult UpdatePageToken(UpdatePageTokenReq req)
        {
            log.Info("updatepagetoken request" + JsonConvert.SerializeObject(req));
            var identity = (ClaimsIdentity)User.Identity;
            int EmpId = Int32.Parse(identity.Claims.Where(c => c.Type == "EmployeeId").Select(c => c.Value).SingleOrDefault());
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (req != null)
            {
                if (req.ids != null && req.tokens != null && req.ids.Count == req.tokens.Count)
                {
                    for (int i = 0; i < req.ids.Count; i++)
                    {
                        string id = req.ids[i];
                        Page page = db.Pages.Where(p => p.PageFacebookId == id && p.ShopId == ShopId).FirstOrDefault();
                        if (page != null)
                        {
                            log.Info("from :" + page.PageFacebookToken);
                            page.PageFacebookToken = req.tokens[i];
                            log.Info("to :" + page.PageFacebookToken);
                            db.SaveChanges();
                        }
                    }
                    if (EmpId > 0)
                    {
                        Employee emp = db.Employees.Find(EmpId);
                        emp.TokenSince = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PageExists(int id)
        {
            return db.Pages.Count(e => e.PageId == id) > 0;
        }
    }
}