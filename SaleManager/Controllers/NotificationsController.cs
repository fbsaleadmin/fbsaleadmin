﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class NotificationsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        
        [Route("api/notifications/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Notification> notifications = db.Notifications;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    notifications = notifications.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    notifications = notifications.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    notifications = notifications.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    notifications = notifications.OrderBy("NotificationId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }              
                def.data = notifications.Select(a => new NotificationDTO()
                {
                    CreatedAt = a.CreatedAt,
                    EmployeeId = a.EmployeeId,
                    Message = a.Message,
                    NotificationId = a.NotificationId,
                    SenderId = a.SenderId,
                    SenderName = a.SenderName,
                    ShopId = a.ShopId,
                    Status = a.Status
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Notifications/5        
        public IHttpActionResult GetNotification(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            NotificationDTO notification = db.Notifications.Where(a => a.NotificationId == id).Select(a => new NotificationDTO()
            {
                CreatedAt = a.CreatedAt,
                EmployeeId = a.EmployeeId,
                Message = a.Message,
                NotificationId = a.NotificationId,
                SenderId = a.SenderId,
                SenderName = a.SenderName,
                ShopId = a.ShopId,
                Status = a.Status
            }).FirstOrDefault();
            if (notification == null || notification.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = notification;
            return Ok(def);
        }

        // PUT: api/Notifications/5       
        public IHttpActionResult PutNotification(int id, NotificationDTO notification)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != notification.NotificationId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Notification current = db.Notifications.Where(s => s.NotificationId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Message = notification.Message;
                current.Status = notification.Status;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Notifications
        [ResponseType(typeof(Notification))]
        public IHttpActionResult PostNotification(NotificationDTO notification)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Notification no = new Notification();
            no.CreatedAt = DateTime.Now;
            no.EmployeeId = notification.EmployeeId;
            no.Message = notification.Message;
            no.SenderId = notification.SenderId;
            no.SenderName = notification.SenderName;
            no.ShopId = ShopId;
            no.Status = notification.Status;

            db.Notifications.Add(no);
            db.SaveChanges();
            notification.NotificationId = no.NotificationId;

            def.meta = new Meta(200, "Success");
            def.data = notification;

            return Ok(def);
        }

        // DELETE: api/Notifications/5
        [ResponseType(typeof(Notification))]
        public IHttpActionResult DeleteNotification(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Notification notification = db.Notifications.Find(id);
            if (notification == null || notification.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.Notifications.Remove(notification);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NotificationExists(int id)
        {
            return db.Notifications.Count(e => e.NotificationId == id) > 0;
        }
    }
}