﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Security.Claims;
using System.IO;
using System.Configuration;
using SaleManager.Data;
using log4net;
using Newtonsoft;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ShopsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly ILog log = LogMaster.GetLogger("fbsale", "fbsale");

        // PUT: api/Shops/5        
        public IHttpActionResult PutShop(int id, ShopDTO shop)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != shop.ShopId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Shop current = db.Shops.Where(s => s.ShopId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Address = shop.Address;
                current.Phone = shop.Phone;
                current.ShopCategoryId = shop.ShopCategoryId;
                current.ShopName = shop.ShopName;
                current.UpdatedAt = DateTime.Now;
                current.ShopOrderPrefix = shop.ShopOrderPrefix;
                current.ShopProductPrefix = shop.ShopProductPrefix;
                current.ShopCustomerPrefix = shop.ShopCustomerPrefix;
                current.ShopVendorPrefix = shop.ShopVendorPrefix;
                current.ShopDVendorPrefix = shop.ShopDVendorPrefix;
                current.ShopUniqueName = shop.ShopUniqueName;
                current.WorkTimeEnd = shop.WorkTimeEnd;
                current.WorkTimeStart = shop.WorkTimeStart;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }
        [HttpGet]
        [Route("api/shops/{id}/inforshop")]
        public IHttpActionResult inforshop()
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            ShopDTO result = db.Shops.Where(c => c.ShopId == ShopId).Select(cr => new ShopDTO()
            {
                ShopId = cr.ShopId,
                UserId = cr.UserId,
                ShopName = cr.ShopName,
                ShopCategoryId = cr.ShopCategoryId,
                Address = cr.Address,
                Phone = cr.Phone,
                WorkTimeStart = cr.WorkTimeStart,
                WorkTimeEnd = cr.WorkTimeEnd,
                ShopProductPrefix = cr.ShopProductPrefix,
                ShopOrderPrefix = cr.ShopOrderPrefix,
                ShopUniqueName = cr.ShopUniqueName,
                CreatedAt = cr.CreatedAt,
                UpdatedAt = cr.UpdatedAt,
                ShopCustomerPrefix = cr.ShopCustomerPrefix,
                ShopVendorPrefix = cr.ShopVendorPrefix,
                ShopDVendorPrefix = cr.ShopDVendorPrefix,
                ShopImportPrefix = cr.ShopImportPrefix,
                ShopExportPrefix = cr.ShopExportPrefix,
                ShopCashInPrefix = cr.ShopCashInPrefix,
                ShopCashOutPrefix = cr.ShopCashOutPrefix
            }).FirstOrDefault();
            if (result != null)
            {
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/shops/{id}/stats")]
        public IHttpActionResult stats(int id)
        {
            DefaultResponse def = new DefaultResponse();
            int countProduct = (from p in db.Products
                                join pa in db.ProductAttributes on p.ProductId equals pa.ProductId
                                where p.ShopId == id && pa.Status != (int)Const.ProductStatus.DELETED
                                select pa).Count();
            int countOrder = db.Orders.Where(o => o.ShopId == id && o.Type == 0 && o.OrderStatus != (int)Const.OrderStatus.CANCELED && o.OrderStatus != (int)Const.OrderStatus.DELETED).Count();
            int countMessage = db.Conversations.Where(c => c.ShopId == id).Count();
            var pages = db.Pages.Where(p => p.ShopId == id).Select(p => new PageInfo()
            {
                Pageid = p.PageId,
                PageName = p.PageName,
                ShopId = p.ShopId,
                PageFacebookId = p.PageFacebookId,
                isSubscribed = p.IsSubscribed
            }).ToList();
            ShopStat stat = new ShopStat();
            stat.countProduct = countProduct;
            stat.countOrder = countOrder;
            stat.countMessage = countMessage;
            stat.pages = pages;
            def.meta = new Meta(200, "Success");
            def.data = stat;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/branches")]
        public IHttpActionResult getBranches(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }
            var result = db.Branches.Where(b => b.ShopId == ShopId && b.Status != (int)Const.Status.DELETED).Select(b => new SaleManager.Data.Branch()
            {
                BranchId = b.BranchId,
                BranchName = b.BranchName,
                Address = b.Address,
                Phone = b.Phone
            }).ToList();

            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/pages")]
        public IHttpActionResult getPages(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }
            var result = db.Pages.Where(b => b.ShopId == ShopId && b.Status != (int)Const.Status.DELETED).Select(b => new SaleManager.Data.Page()
            {
                id = b.PageFacebookId,
                isSubscribed = b.IsSubscribed,
                name = b.PageName,
                PageId = b.PageId                
            }).ToList();

            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/detailstats")]
        public IHttpActionResult detailstats(int id, [FromUri] string startdate = null, [FromUri]string enddate = null)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            //id = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            Shop shop = db.Shops.Find(id);
            int countProduct = (from p in db.Products
                                join pa in db.ProductAttributes on p.ProductId equals pa.ProductId
                                where p.ShopId == id && pa.Status != (int)Const.ProductStatus.DELETED
                                select pa).Count();
            var countItem = db.BranchStocks.Where(bs => bs.ProductAttribute.Product.ShopId == id && bs.Branch.Status != (int)Const.Status.DELETED && bs.ProductAttribute.Status != (int)Const.ProductStatus.DELETED).Sum(bs => bs.Stock).HasValue ? db.BranchStocks.Where(bs => bs.ProductAttribute.Product.ShopId == id && bs.Branch.Status != (int)Const.Status.DELETED && bs.ProductAttribute.Status != (int)Const.ProductStatus.DELETED).Sum(bs => bs.Stock).Value : 0;
            var countItemExport = db.EximProducts.Where(ep => ep.Exim.ShopId == id && ep.Exim.Type == (int)Const.EximType.EXPORT).Sum(ep => ep.Quantity);
            int TotalOrder = db.Orders.Where(o => o.ShopId == id && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == 0 && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Count();
            double TotalOrderPrice = db.Orders.Where(o => o.ShopId == id && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == 0 && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? (double)db.Orders.Where(o => o.ShopId == id && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == 0 && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;
            int countMessage = db.Conversations.Where(c => c.ShopId == id).Count();
            //debt
            var debts = db.Customers.Where(e => e.Status != (int)Const.Status.DELETED && e.ShopId == id).AsEnumerable().ToList();           
            decimal debtAmount = 0;
            for (int i = 0; i < debts.Count; i++)
            {
                int idc = (int)debts[i].CustomerId;
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == idc && cf.PaidTargetType == "CUSTOMER").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault() != null ? db.CashFlows.Where(cf => cf.PaidTargetId == idc && cf.PaidTargetType == "CUSTOMER").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault().Debt : 0;
                if (debt.HasValue)
                {
                    debtAmount += debt.Value;
                }
            }
            //today
            var today = DateTime.Now;
            DateTime startDate, endDate;
            if (startdate == null && enddate == null)
            {
                startDate = new DateTime(today.Year, today.Month, today.Day, 00, 00, 00);
                endDate = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
            }
            else
            {
                DateTime sd = DateTime.ParseExact(startdate, "yyyyMMdd", null);
                startDate = new DateTime(sd.Year, sd.Month, sd.Day, 00, 00, 00);
                DateTime ed = DateTime.ParseExact(enddate, "yyyyMMdd", null);
                endDate = new DateTime(ed.Year, ed.Month, ed.Day, 23, 59, 59);
            }
            //order
            var todayOrderCount = db.Orders.Where(o => o.ShopId == id && o.CreatedAt >= startDate && o.CreatedAt <= endDate && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == 0 && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Count();
            var todayOrderAmount = db.Orders.Where(o => o.ShopId == id && o.CreatedAt >= startDate && o.CreatedAt <= endDate && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == (int)Const.OrderType.SALE && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? db.Orders.Where(o => o.ShopId == id && o.CreatedAt >= startDate && o.CreatedAt <= endDate && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == (int)Const.OrderType.SALE && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;         
            //return
            var todayReturnCount = db.Exims.Where(e => e.ShopId == shop.ShopId && e.Type == 0 && e.TargetType == "CUSTOMER" && e.CreatetAt >= startDate && e.CreatetAt <= endDate && e.Status != (int)Const.EximStatus.DELETED).Count();
            //var todayReturnAmout = db.Exims.Where(e => e.ShopId == shop.ShopId && e.Type == 0 && e.TargetType == "CUSTOMER" && e.CreatetAt >= startDate && e.CreatetAt <= endDate && e.Status != (int)Const.EximStatus.DELETED).Select(e => e.Price).DefaultIfEmpty().Sum() != null ? db.Exims.Where(e => e.Type == (int)Const.EximType.IMPORT && e.TargetType == "CUSTOMER" && e.CreatetAt >= startDate && e.CreatetAt <= endDate && e.Status != (int)Const.EximStatus.DELETED).Select(e => e.Price).DefaultIfEmpty().Sum() : 0;
            var todayReturnAmout = db.Orders.Where(o => o.ShopId == id && o.CreatedAt >= startDate && o.CreatedAt <= endDate && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == (int)Const.OrderType.RETURN && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? db.Orders.Where(o => o.ShopId == id && o.CreatedAt >= startDate && o.CreatedAt <= endDate && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.Type == (int)Const.OrderType.RETURN && o.OrderStatus != (int)Const.OrderStatus.CANCELED).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;     
            //page
            var page = db.Pages.Where(p => p.ShopId == shop.ShopId).FirstOrDefault();
            //customer
            var customerCount = db.Customers.Where(c => c.ShopId == shop.ShopId && c.Status != (int)Const.Status.DELETED).Count();
            var vendorCount = db.Vendors.Where(c => c.ShopId == shop.ShopId && c.Status != (int)Const.Status.DELETED).Count();

            var ShopAddress = db.Branches.Where(c => c.ShopId == shop.ShopId && c.IsMainBranch == true).Select(c => c.Address).FirstOrDefault() != null ? db.Branches.Where(c => c.ShopId == shop.ShopId && c.IsMainBranch == true).Select(c => c.Address).FirstOrDefault().ToString() : "";
            ShopDetailStat stat = new ShopDetailStat();
            stat.Address = ShopAddress;
            stat.CountExportedItem = countItemExport;
            stat.CountItem = countItem;
            stat.CountMessage = countMessage;
            stat.CountProduct = countProduct;
            stat.DebtAmount = (double)debtAmount;
            stat.DebtCount = 0;
            stat.ShopName = shop.ShopName;
            stat.TotalOrder = TotalOrder;
            stat.TotalOrderAmount = TotalOrderPrice;
            stat.TodayOrderAmount = todayOrderAmount;
            stat.TodayOrderCount = todayOrderCount;
            stat.TodayReturnAmount = todayReturnAmout;
            stat.TodayReturnCount = todayReturnCount;
            stat.CustomerCount = customerCount;
            stat.VendorCount = vendorCount;           
            if (page != null)
            {
                stat.Avatar = "http://graph.facebook.com/" + page.PageFacebookId + "/picture?type=square";
            }
            def.meta = new Meta(200, "Success");
            def.data = stat;
            //def.data = stat;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/todaystats")]
        public IHttpActionResult GetTodayStats(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;         
            var result = (from pd in db.Products
                          join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                          join op in db.OrderProducts on pa.ProductAttributeId equals op.ProductAttributeId
                          join od in db.Orders on op.OrderId equals od.OrderId
                          orderby pa.ProductCode descending
                          where pd.ShopId == id && od.Type == (int)Const.OrderType.SALE && od.OrderStatus != (int)Const.OrderStatus.DELETED 
                          && pa.Status != (int)Const.ProductStatus.DELETED && od.OrderStatus != (int)Const.OrderStatus.CANCELED
                          select new
                          {
                              PaId = pa.ProductAttributeId,
                              Total = op.TotalPrice,
                              CreatedAt = od.CreatedAt,
                              PId = pa.ProductId,
                              SubName = pa.SubName,
                              ProductCode = pa.ProductCode,
                              ShopId = pa.Product.ShopId,
                              ProductName = pa.Product.ProductName,
                              images = pd.ProductImages.Select(i => new Data.PI() { Alt = i.Alt, Path = i.Path, ProductImageId = i.ProductImageId }).ToList(),
                              Quantity = op.Quantity
                          }).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            var result2 = (from op in result
                           group op by new
                           {
                               op.PaId
                           } into god
                           select new
                           {
                               PaId = god.Key.PaId,
                               Total = god.Sum(p => p.Total),
                               Quantity = god.Sum(p => p.Quantity),
                               PId = god.FirstOrDefault().PaId,
                               SubName = god.FirstOrDefault().SubName,
                               ProductCode = god.FirstOrDefault().ProductCode,
                               ShopId = god.FirstOrDefault().ShopId,
                               ProductName = god.FirstOrDefault().ProductName,
                               images = god.FirstOrDefault().images
                           }).AsEnumerable();

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.data = result2.Take(5).OrderByDescending(e => e.Quantity);
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/CheckExistUniqueName")]
        public IHttpActionResult CheckExistUniqueName(string name)
        {
            DefaultResponse def = new DefaultResponse();
            int count = db.Shops.Count(u => u.ShopUniqueName.ToLower().Trim() == name.ToLower().Trim());
            if (count > 0)
            {
                def.meta = new Meta(201, "Existing Shop UniqueName");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
        }

        // POST: api/Shops        
        public IHttpActionResult PostShop(ShopDTO shop)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Shop c = new Shop();
            c.Address = shop.Address;
            c.CreatedAt = DateTime.Now;
            c.Phone = shop.Phone;
            c.ShopCategoryId = -1;
            c.ShopName = shop.ShopName;
            c.ShopProductPrefix = "SP";
            c.ShopOrderPrefix = "HD";
            c.ShopUniqueName = shop.ShopUniqueName;
            c.ShopCustomerPrefix = "KH";
            c.ShopCashInPrefix = "PT";
            c.ShopCashOutPrefix = "PC";
            c.ShopExportPrefix = "PX";
            c.ShopImportPrefix = "PN";
            c.ShopVendorPrefix = "NCC";
            c.ShopDVendorPrefix = "CVC";
            c.UpdatedAt = DateTime.Now;
            c.UserId = shop.UserId;
            c.WorkTimeEnd = shop.WorkTimeEnd;
            c.WorkTimeStart = shop.WorkTimeStart;
            db.Shops.Add(c);
            //create main branch          
            db.SaveChanges();
            shop.ShopId = c.ShopId;
            //create cashtype
            //cash in 
            CashType cashIn = new CashType();
            cashIn.CashTypeCode = "PT";
            cashIn.CreatedAt = DateTime.Now;
            cashIn.DefaultValue = 0;
            cashIn.IsCustom = false;
            cashIn.Name = "Phiếu thu";
            cashIn.ShopId = shop.ShopId;
            cashIn.Type = 1;
            cashIn.UpdatedAt = DateTime.Now;
            db.CashTypes.Add(cashIn);
            db.SaveChanges();
            //cash out
            CashType cashOut = new CashType();
            cashOut.CashTypeCode = "PC";
            cashOut.CreatedAt = DateTime.Now;
            cashOut.DefaultValue = 0;
            cashOut.IsCustom = false;
            cashOut.Name = "Phiếu chi";
            cashOut.ShopId = shop.ShopId;
            cashOut.Type = 0;
            cashOut.UpdatedAt = DateTime.Now;
            db.CashTypes.Add(cashOut);
            db.SaveChanges();
            //config
            Config config = new Config();
            config.AutoCreateOrder = false;
            config.AutoHideComment = true;
            config.NewCommentLike = false;
            config.NewMessageNotification = true;
            config.NewMessagePriority = true;
            config.NotificationSound = true;
            config.ShopId = shop.ShopId;
            db.Configs.Add(config);
            db.SaveChanges();
            //cash type            
            Branch branch = new Branch();
            branch.Address = shop.Address;
            branch.ShopId = shop.ShopId;
            branch.Status = (int)Const.Status.NORMAL;
            branch.CreatedAt = DateTime.Now;
            branch.UpdatedAt = DateTime.Now;
            branch.Phone = shop.Phone;
            branch.IsMainBranch = true;
            branch.BranchName = shop.ShopName + " - " + "Chi nhánh chính";
            db.Branches.Add(branch);
            db.SaveChanges();
            //update employee
            Employee emp = db.Employees.Where(e => e.UserId == shop.UserId).FirstOrDefault();
            if (emp != null)
            {
                emp.ShopId = shop.ShopId;
                emp.BranchId = branch.BranchId;
                db.Entry(emp).State = EntityState.Modified;
            }
            db.SaveChanges();
            def.meta = new Meta(200, "Success");
            def.data = shop;

            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/products")]
        public IHttpActionResult getProductDetails(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var countquery = HttpUtility.UrlDecode("");
            //apply query first          
            var result = (from p in db.Products
                          join pa in db.ProductAttributes on p.ProductId equals pa.ProductId
                          where p.ShopId == ShopId && pa.Status != (int)Const.ProductStatus.DELETED
                          orderby pa.ProductAttributeId descending
                          select new ShopProduct
                          {
                              category = new Models.Category() { CategoryId = p.Category.CategoryId, CategoryName = p.Category.CategoryName, CategoryParentId = p.Category.CategoryParentId },
                              ProductAttributeId = pa.ProductAttributeId,
                              ProductCode = pa.ProductCode,
                              ProductId = p.ProductId,
                              ShopId = p.ShopId,
                              SubName = pa.SubName,
                              AllowDelivery = p.AllowDelivery,
                              AllowOrder = p.AllowOrder,
                              AllowSell = p.AllowSell,
                              Code = pa.Code,
                              Discount = pa.Discount,
                              images = p.ProductImages.Select(i => new Data.PI() { Alt = i.Alt, Path = i.Path, ProductImageId = i.ProductImageId, RelativePath = i.RelativePath }).ToList(),
                              NetWeight = pa.NetWeight,
                              PAStatus = pa.Status,
                              ProductDesc = p.ProductDesc,
                              ProductName = p.ProductName,
                              PStatus = p.Status,
                              Tag = p.Tag,
                              ManufactureId = p.ManufactureId,
                              MinStock = p.MinStock,
                              StockLimit = p.StockLimit,
                              SearchQuery = pa.SearchQuery,
                              CreatedAt = pa.CreatedAt,
                              UpdatedAt = pa.UpdatedAt
                          }).AsEnumerable();
            
            if (paging.query != null && !paging.query.Contains("SumStock"))
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }
            else
            {
               countquery = HttpUtility.UrlDecode(paging.query);
            }
                
            var returnData = result.ToList();

            for (int i = 0; i < returnData.Count; i++)
            {
                int PaId = (int)returnData[i].ProductAttributeId;
                returnData[i].stock = db.BranchStocks.Join(db.Branches, stock => stock.BranchId, branc => branc.BranchId,
                    (stock, branc) => new { Stock = stock, Branc = branc })
                    .Where(b => b.Stock.ProductAttributeId == PaId && b.Branc.Status != (int)Const.ProductStatus.DELETED).Select(b => new ShopProductStock()
                             {
                                 BranchId = b.Stock.BranchId,
                                 Stock = b.Stock.Stock,
                                 BranchName = b.Branc.BranchName
                             }).ToList();

                returnData[i].prices = (from pp in db.ProductPrices
                                        join pu in db.ProductUnits on pp.ProductUnitId equals pu.ProductUnitId
                                        where pp.ProductAttributeId == PaId && pp.IsCurrent == true && pu.Status != (int)Const.ProductStatus.DELETED
                                        orderby pp.SalePrice descending
                                        select new ShopProductPrice()
                                        {
                                            ProductPriceId = pp.ProductPriceId,
                                            Unit = pp.ProductUnit.Unit,
                                            Quantity = pp.ProductUnit.Quantity,
                                            OriginPrice = pp.OriginPrice,
                                            AverageImportPrice = pp.OriginPrice,
                                            SalePrice = pp.SalePrice,
                                            ProductUnitId = pp.ProductUnitId
                                        }).ToList();
                
                foreach (var itemP in returnData[i].prices)
                {
                    var importPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == PaId && ep.ProductUnitId == itemP.ProductUnitId
                                       && ep.Exim.Type == (int)Const.EximType.IMPORT && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED 
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();

                    var exportPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == PaId && ep.ProductUnitId == itemP.ProductUnitId
                                       && ep.Exim.TargetType == "VENDOR" && ep.Exim.Type == (int)Const.EximType.EXPORT && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();

                    importPrice = importPrice.ToList();
                    if ((importPrice == null || importPrice.Count() == 0))
                    {
                        itemP.AverageImportPrice = 0;
                    }
                    else
                    {
                        itemP.OriginPrice = importPrice.FirstOrDefault().Price;
                        double sumImportPrice = 0;
                        int countImportPrice = 0;
                        foreach (var itemIP in importPrice)
                        {                            
                            if (itemIP.Price != null)
                            {
                                int quantityExport = exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().HasValue ? exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().Value : 0;
                                int quantity = (int)itemIP.Quantity - quantityExport;
                                countImportPrice += quantity;//(int)itemIP.Quantity;
                                sumImportPrice += (double)(itemIP.Price * quantity);
                            }
                        }
                        if (countImportPrice != 0)
                        {
                            itemP.AverageImportPrice = Math.Round((decimal)(sumImportPrice / (int)countImportPrice), 0);
                        }
                        else
                        {
                            itemP.AverageImportPrice = 0;
                        }
                    }
                }

                decimal originPrice = 0;
                foreach (var itemP in returnData[i].prices)
                {
                    if (itemP.Quantity == 1)
                    {
                        originPrice = (decimal)itemP.OriginPrice;
                    }
                }
                
                returnData[i].SumStock = (int)returnData[i].stock.Select(s => s.Stock).Sum();
                returnData[i].StockPrice = returnData[i].SumStock * originPrice;
            }

            string test = !string.IsNullOrEmpty(countquery) ? countquery : "1=1";
            returnData = returnData.Where(test).ToList();

            var result2 = returnData.AsEnumerable();
            int count = result2.Count();
            int totalstock = result2.Sum(e => e.SumStock).Value;
            decimal totalstockprice = result2.Sum(e => e.StockPrice).Value;

            result2 = result2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            if (result2 == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = result2;
            def.metadata = new MetadataTotal(count, totalstock, totalstockprice);
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/productprices")]
        public IHttpActionResult getProductPrices(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from p in db.Products
                          join pa in db.ProductAttributes on p.ProductId equals pa.ProductId
                          where p.ShopId == ShopId && pa.Status != (int)Const.ProductStatus.DELETED
                          orderby pa.ProductAttributeId descending
                          select new ShopProductPrices
                          {
                              category = new Models.Category() { CategoryId = p.Category.CategoryId, CategoryName = p.Category.CategoryName, CategoryParentId = p.Category.CategoryParentId },
                              ProductAttributeId = pa.ProductAttributeId,
                              ProductCode = pa.ProductCode,
                              ProductId = p.ProductId,
                              ProductName = p.ProductName,
                              SearchQuery = pa.SearchQuery,
                              ShopId = p.ShopId,
                              SubName = pa.SubName,
                              PAStatus = pa.Status
                          }).AsEnumerable();

            int count = result.Count();
            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
                count = result.Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            var returnData = result.ToList();
            for (int i = 0; i < returnData.Count; i++)
            {
                int PaId = (int)returnData[i].ProductAttributeId;
                
                returnData[i].stock = db.BranchStocks.Where(b => b.ProductAttributeId == PaId).Select(b => new ShopProductStock()
                              {
                                  BranchId = b.BranchId,
                                  Stock = b.Stock
                              }).ToList();

                returnData[i].prices = (from pp in db.ProductPrices
                                        join pu in db.ProductUnits on pp.ProductUnitId equals pu.ProductUnitId
                                        where pp.ProductAttributeId == PaId && pp.IsCurrent == true && pu.Status != (int)Const.Status.DELETED
                                        orderby pp.SalePrice descending
                                        select new Price()
                                        {
                                            ProductPriceId = pp.ProductPriceId,
                                            OriginPrice = pp.OriginPrice,
                                            SalePrice = pp.SalePrice,
                                            AverageImportPrice = pp.OriginPrice,
                                            unit = new Models.ProductUnit()
                                            {
                                                ProductUnitId = pu.ProductUnitId,
                                                Quantity = pu.Quantity,
                                                Unit = pu.Unit
                                            }
                                        }).ToList();

                foreach (var itemP in returnData[i].prices)
                {
                    var importPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == PaId && ep.ProductUnitId == itemP.unit.ProductUnitId
                                       && ep.Exim.Type == (int)Const.EximType.IMPORT && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();

                    var exportPrice = (from ep in db.EximProducts
                                       where ep.ProductAttributeId == PaId && ep.ProductUnitId == itemP.unit.ProductUnitId
                                       && ep.Exim.TargetType == "VENDOR" && ep.Exim.Type == (int)Const.EximType.EXPORT && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                       && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                       orderby ep.CreatedAt descending
                                       select new
                                       {
                                           Price = ep.Price,
                                           Quantity = ep.Quantity
                                       }).AsEnumerable();

                    importPrice = importPrice.ToList();
                    if ((importPrice == null || importPrice.Count() == 0))
                    {
                        itemP.AverageImportPrice = 0;
                    }
                    else
                    {
                        itemP.OriginPrice = importPrice.FirstOrDefault().Price;
                        double sumImportPrice = 0;
                        int countImportPrice = 0;
                        foreach (var itemIP in importPrice)
                        {
                            if (itemIP.Price != null)
                            {
                                int quantityExport = exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().HasValue ? exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().Value : 0;
                                int quantity = (int)itemIP.Quantity - quantityExport;
                                countImportPrice += quantity;
                                sumImportPrice += (double)(itemIP.Price * quantity);
                            }
                        }
                        if (countImportPrice != 0)
                        {
                            itemP.AverageImportPrice = Math.Round((decimal)(sumImportPrice / (int)countImportPrice), 0);
                        }
                        else
                        {
                            itemP.AverageImportPrice = 0;
                        }
                    }
                }
            }
            def.meta = new Meta(200, "Success");
            def.data = returnData;
            def.metadata = new Metadata(count);
            return Ok(def);
        }

        List<PI> convertToProductImageDTO(ICollection<ProductImage> productImages)
        {
            List<PI> list = new List<PI>();
            foreach (ProductImage p in productImages)
            {
                PI pi = new PI();
                pi.Alt = p.Alt;
                pi.Path = p.Path;
                pi.ProductImageId = p.ProductImageId;
                list.Add(pi);
            }
            return list;
        }

        [HttpGet]
        [Route("api/shops/{id}/categories")]
        public IHttpActionResult getCategories(int id)
        {
            log.Info(id);
            var identity = (ClaimsIdentity)User.Identity;
            log.Info(id + "-" + id);
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Categories
                          where c.ShopId == id && c.CategoryParentId == -1 && c.Status != (int)Const.Status.DELETED
                          select new SaleManager.Models.ShopCategory
                          {
                              CategoryId = c.CategoryId,
                              CategoryName = c.CategoryName,
                              ShopId = c.ShopId,
                              CategoryParentId = c.CategoryParentId,
                              ProductCount = 0,
                              StockCount = 0,
                              children = (from cc in db.Categories
                                          where cc.CategoryParentId == c.CategoryId && cc.Status != (int)Const.Status.DELETED
                                          select new SaleManager.Models.ShopChildCategory
                                          {
                                              CategoryId = cc.CategoryId,
                                              CategoryName = cc.CategoryName,
                                              CategoryParentId = cc.CategoryParentId,
                                              ShopId = cc.ShopId,
                                              ProductCount = 0,
                                              StockCount = 0,
                                          }).ToList(),
                          }).AsEnumerable();

            var returnData = result.ToList();
            for (int i = 0; i < returnData.Count; i++)
            {
                int CategoryId = (int)returnData[i].CategoryId;

                var productCount = (from pd in db.Products
                                    join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                                    where pd.CategoryId == CategoryId && pa.Status != (int)Const.ProductStatus.DELETED
                                    group pd by new
                                    {
                                        pd.CategoryId,
                                    } into gcc
                                    select new
                                    {
                                        ProductCount = gcc.Count(),
                                    }).AsEnumerable();

                if (productCount.ToList().Count > 0)
                {
                    returnData[i].ProductCount = productCount.FirstOrDefault().ProductCount;
                }

                var stockCount = (from pd in db.Products
                                  join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                                  join bs in db.BranchStocks on pa.ProductAttributeId equals bs.ProductAttributeId
                                  join br in db.Branches on bs.BranchId equals br.BranchId
                                  where pd.CategoryId == CategoryId && pa.Status != (int)Const.ProductStatus.DELETED
                                  group bs by new
                                  {
                                      pd.CategoryId
                                  } into gcc
                                  select new
                                  {
                                      StockCount = gcc.Sum(e => e.Stock),
                                  }).AsEnumerable();

                if (stockCount.ToList().Count > 0)
                {
                    returnData[i].StockCount = stockCount.FirstOrDefault().StockCount;
                }


                var children = returnData[i].children.ToList();
                for (int j = 0; j < children.Count; j++)
                {
                    int CategoryId2 = (int)children[j].CategoryId;

                    var productCount2 = (from pd in db.Products
                                         join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                                         where pd.CategoryId == CategoryId2 && pa.Status != (int)Const.ProductStatus.DELETED
                                         group pd by new
                                         {
                                             pd.CategoryId,
                                         } into gcc
                                         select new 
                                         {
                                             ProductCount = gcc.Count(),
                                         }).AsEnumerable();

                    if (productCount2.ToList().Count > 0)
                    {
                        children[j].ProductCount = productCount2.FirstOrDefault().ProductCount;
                        //returnData[i].ProductCount += children[j].ProductCount;
                    }

                    var stockCount2 = (from pd in db.Products
                                        join pa in db.ProductAttributes on pd.ProductId equals pa.ProductId
                                        join bs in db.BranchStocks on pa.ProductAttributeId equals bs.ProductAttributeId
                                        join br in db.Branches on bs.BranchId equals br.BranchId
                                        where pd.CategoryId == CategoryId2 && pa.Status != (int)Const.ProductStatus.DELETED
                                        && br.Status != (int)Const.Status.DELETED
                                        group bs by new
                                        {
                                            pd.CategoryId
                                        } into gcc
                                        select new
                                        {
                                           StockCount = gcc.Sum(e => e.Stock),
                                        }).AsEnumerable();

                    if (stockCount2.ToList().Count > 0)
                    {
                        children[j].StockCount = stockCount2.FirstOrDefault().StockCount;
                        returnData[i].StockCount += children[j].StockCount;
                    }

                }
                returnData[i].children = children;
            }

            def.meta = new Meta(200, "Success");
            def.data = returnData;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/cashtypes")]
        public IHttpActionResult getCategories(int id, [FromUri] int type)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }
            var result = db.CashTypes.Where(ct => ct.ShopId == ShopId && ct.Type == type).Select(ct => new Data.CashType()
            {
                DefaultValue = ct.DefaultValue,
                Name = ct.Name,
                CashTypeId = ct.CashTypeId,
                Type = ct.Type
            }).ToList();
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/customerGroups")]
        public IHttpActionResult getCustomerGroups(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }
            var result = db.CustomerGroups.Where(cg => cg.ShopId == ShopId && cg.Status != (int)Const.Status.DELETED).Select(cg => new CustomerGroupDTO()
            {
                CreateAt = cg.CreateAt,
                CustomerGroupId = cg.CustomerGroupId,
                Description = cg.Description,
                Name = cg.Name,
                Status = cg.Status,
                ShopId = cg.ShopId
            }).ToList();

            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/labels")]
        public IHttpActionResult getLabels(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }
            var result = db.Labels.Where(l => l.ShopId == ShopId && l.Status != (int)Const.Status.DELETED).Select(l => new LabelDTO()
            {
                CreatedAt = l.CreatedAt,
                LabelColor = l.LabelColor,
                LabelId = l.LabelId,
                LabelName = l.LabelName,
                ShopId = l.ShopId,
                Status = l.Status,
                UpdatedAt = l.UpdatedAt
            }).ToList();

            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/posts")]
        public IHttpActionResult getPosts(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Posts
                          where c.ShopId == ShopId && c.Status != (int)Const.Status.DELETED
                          orderby c.CreatedDate descending
                          select new SaleManager.Models.ShopPost
                          {
                              Content = c.Content,
                              CreatedDate = c.CreatedDate,
                              FacebookPostId = c.FacebookPostId,
                              FacebookPostUrl = c.FacebookPostUrl,
                              Image = c.Image,
                              PageId = c.PageId,
                              PostId = c.PostId,
                              ShopId = c.ShopId,
                              Status = c.Status,
                              Title = c.Title,
                              UpdatedAt = c.UpdatedAt,
                              ShowComment = c.ShowComment
                          }).AsEnumerable();
            int item_count = result.Count();
            //result = result.Skip((page - 1) * page_size).Take(page_size);
            var returnData2 = result.AsEnumerable();
            if (paging.query != null)
            {
                returnData2 = returnData2.Where(HttpUtility.UrlDecode(paging.query));
                item_count = returnData2.Count();
                returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            Metadata metadata = new Metadata();
            metadata.item_count = item_count;
            def.data = returnData2.ToList();
            def.metadata = metadata;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/customers")]
        public IHttpActionResult getCustomers(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Customers
                          //join p in db.Provinces on c.ProvinceId equals p.ProvinceId
                          //join d in db.Districts on c.DistrictId equals d.DistrictId
                          from d in db.Districts.Where(d => d.DistrictId == c.DistrictId).DefaultIfEmpty()
                          from p in db.Provinces.Where(p => p.ProvinceId == c.ProvinceId).DefaultIfEmpty()
                          from cg in db.CustomerGroups.Where(cg => cg.CustomerGroupId == c.CustomerGroupId).DefaultIfEmpty()
                          where c.ShopId == ShopId && c.Status != (int)Const.Status.DELETED
                          orderby c.CustomerId descending
                          select new SaleManager.Models.ShopCustomer
                          {
                              Address = c.Address,
                              CreatedAt = c.CreatedAt,
                              CustomerCode = c.CustomerCode,
                              CustomerId = c.CustomerId,
                              CustomerName = c.CustomerName,
                              District = new ShopDistrict()
                              {
                                  DistrictId = d != null ? d.DistrictId : -1,
                                  Name = d != null ? d.Name : "",
                                  ProvinceId = d != null ? d.ProvinceId : -1
                              },
                              DOB = c.DOB,
                              Email = c.Email,
                              FacebookUserId = c.FacebookUserId,
                              Gender = c.Gender,
                              ID = c.ID,
                              Level = c.Level,
                              Note = c.Note,
                              Phone = c.Phone,
                              Province = new ShopProvince()
                              {
                                  ProvinceId = p != null ? p.ProvinceId : -1,
                                  Name = p != null ? p.Name : ""
                              },
                              ShopId = c.ShopId,
                              Status = c.Status,
                              Tag = c.Tag,
                              TaxID = c.TaxID,
                              Type = c.Type,
                              UpdatedAt = c.UpdatedAt,
                              SearchQuery = c.SearchQuery,
                              FBUserAvatar = c.FBUserAvatar,
                              customerGroup = new ShopCustomerGroup()
                              {
                                  CustomerGroupId = cg != null ? cg.CustomerGroupId : -1,
                                  Name = cg != null ? cg.Name : ""
                              }
                          }
                         ).AsEnumerable();

            int count = result.Count();
            
            var returnData = result.ToList();
            for (int i = 0; i < returnData.Count; i++)
            {
                int CustomerId = (int)returnData[i].CustomerId;
                returnData[i].TotalOrder = db.Orders.Where(o => o.CustomerId == CustomerId && o.Type == (int)Const.OrderType.SALE).Count();
                returnData[i].TotalOrderPrice = db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;
                returnData[i].TotalPaid = db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() != null ? db.Orders.Where(o => o.CustomerId == CustomerId && o.OrderStatus == (int)Const.OrderStatus.COMPLETED && o.Type == (int)Const.OrderType.SALE).Select(o => o.TotalPrice).DefaultIfEmpty().Sum() : 0;
                returnData[i].TotalDebt = db.CashFlows.Where(cf => cf.PaidTargetId == CustomerId && cf.PaidTargetType == "CUSTOMER").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault() != null ? db.CashFlows.Where(cf => cf.PaidTargetId == CustomerId && cf.PaidTargetType == "CUSTOMER").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault().Debt : 0;
            }

            int totalorderimport = returnData.Sum(e => e.TotalOrder).Value;
            decimal totalorderprice = returnData.Sum(e => e.TotalOrderPrice).Value;
            decimal totaldebt = returnData.Sum(e => e.TotalDebt).Value;

            var returnData2 = returnData.AsEnumerable();
            if (paging.query != null)
            {
                returnData2 = returnData2.Where(HttpUtility.UrlDecode(paging.query));
                count = returnData2.Count();
                returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            def.meta = new Meta(200, "Success");
            def.data = returnData2;
            def.metadata = new MetadataTotal(count, totalorderimport, totalorderprice, totaldebt);
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/vendors")]
        public IHttpActionResult getVendors(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from v in db.Vendors
                          //join p in db.Provinces on v.ProvinceId equals p.ProvinceId
                          //join d in db.Districts on v.DistrictId equals d.DistrictId
                          from d in db.Districts.Where(d => d.DistrictId == v.DistrictId).DefaultIfEmpty()
                          from p in db.Provinces.Where(p => p.ProvinceId == v.ProvinceId).DefaultIfEmpty()
                          where v.ShopId == id && v.Status != (int)Const.Status.DELETED
                          orderby v.VendorId descending
                          select new SaleManager.Models.ShopVendor
                          {
                              Address = v.Address,
                              Company = v.Company,
                              CreatedAt = v.CreatedAt,
                              District = new ShopDistrict()
                              {
                                  DistrictId = d != null ? d.DistrictId : -1,
                                  Name = d != null ? d.Name : "",
                                  ProvinceId = d != null ? d.ProvinceId : -1
                              },
                              Email = v.Email,
                              Note = v.Note,
                              Phone = v.Phone,
                              Province = new ShopProvince()
                              {
                                  ProvinceId = p != null ? p.ProvinceId : -1,
                                  Name = p != null ? p.Name : ""
                              },
                              ShopId = v.ShopId,
                              VendorCode = v.VendorCode,
                              VendorId = v.VendorId,
                              VendorName = v.VendorName,
                              UpdatedAt = v.UpdatedAt,
                              SearchQuery = v.SearchQuery
                          }
                         ).AsEnumerable();
            int count = result.Count();

            var returnData = result.ToList();
            for (int i = 0; i < result.Count(); i++)
            {
                int VendorId = (int)returnData[i].VendorId;
                returnData[i].TotalOrder = db.Exims.Where(ip => ip.TargetType == "VENDOR" && ip.TargetId == VendorId && ip.Type == 0).Count();
                returnData[i].TotalOrderPrice = db.Exims.Where(ip => ip.TargetType == "VENDOR" && ip.TargetId == VendorId && ip.Type == 0).Sum(c => c.TotalPrice).HasValue ? db.Exims.Where(ip => ip.TargetType == "VENDOR" && ip.TargetId == VendorId && ip.Type == 0).Sum(c => c.TotalPrice).Value : 0;
                returnData[i].TotalPaid = db.Cashes.Where(c => c.PaidTargetType == "VENDOR" && c.PaidTargetId == VendorId && c.CashType.Type == 0).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() != null ? db.Cashes.Where(c => c.PaidTargetType == "VENDOR" && c.PaidTargetId == VendorId && c.CashType.Type == 0).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() : 0;
                returnData[i].TotalDebt = db.CashFlows.Where(cf => cf.PaidTargetId == VendorId && cf.PaidTargetType == "VENDOR").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault() != null ? db.CashFlows.Where(cf => cf.PaidTargetId == VendorId && cf.PaidTargetType == "VENDOR").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault().Debt : 0;
            }

            int totalorderimport = returnData.Sum(e => e.TotalOrder).Value;
            decimal totalorderprice = returnData.Sum(e => e.TotalOrderPrice).Value;
            decimal totaldebt = returnData.Sum(e => e.TotalDebt).Value;

            var returnData2 = returnData.AsEnumerable();
            if (paging.query != null)
            {
                returnData2 = returnData2.Where(HttpUtility.UrlDecode(paging.query));
                count = returnData2.Count();
                returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                returnData2 = returnData2.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            def.meta = new Meta(200, "Success");
            def.data = returnData2;
            def.metadata = new MetadataTotal(count, totalorderimport, totalorderprice, totaldebt);
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/deliveryvendors")]
        public IHttpActionResult getDeliveryVendors(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from v in db.DeliveryVendors
                          //join d in db.Districts on v.DistrictId equals d.DistrictId
                          //join p in db.Provinces on v.ProvinceId equals p.ProvinceId
                          from d in db.Districts.Where(d => d.DistrictId == v.DistrictId).DefaultIfEmpty()
                          from p in db.Provinces.Where(p => p.ProvinceId == v.ProvinceId).DefaultIfEmpty()
                          where v.ShopId == ShopId && v.Status != (int)Const.Status.DELETED
                          select new SaleManager.Models.ShopDeliveryVendor
                          {
                              Address = v.Address,
                              CreatedAt = v.CreatedAt,
                              District = new ShopDistrict()
                              {
                                  DistrictId = d != null ? d.DistrictId : -1,
                                  Name = d != null ? d.Name : "",
                                  ProvinceId = d != null ? d.ProvinceId : -1
                              },
                              Phone = v.Phone,
                              Province = new ShopProvince()
                              {
                                  ProvinceId = p != null ? p.ProvinceId : -1,
                                  Name = p != null ? p.Name : ""
                              },
                              ShopId = v.ShopId,
                              VendorCode = v.VendorCode,
                              DeliveryVendorId = v.DeliveryVendorId,
                              VendorName = v.VendorName,
                              UpdatedAt = v.UpdatedAt,
                              SearchQuery = v.SearchQuery,
                              Note = v.Note
                          }
                         ).AsEnumerable();
            int count = result.Count();
            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
                count = result.Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            var returnData = result.ToList();
            for (int i = 0; i < returnData.Count; i++)
            {
                int DeliveryVendorId = (int)returnData[i].DeliveryVendorId;
                returnData[i].TotalDebt = db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(d => d.Total).DefaultIfEmpty().Sum() != null ? (double)db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(d => d.Total).DefaultIfEmpty().Sum() : 0;
                returnData[i].TotalPaid = db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() != null ? (double)db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() : 0;
            }
            def.meta = new Meta(200, "Success");
            def.data = returnData;
            def.metadata = new Metadata(count);
            return Ok(def);
        }

        #region create order
        [HttpPost]
        [Route("api/shops/{id}/orders")]
        public IHttpActionResult createOrders(int id, CreateOrder shopOrder)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //create order
            log.Info(JsonConvert.SerializeObject(shopOrder));
            //check if product empty
            DefaultResponse def = new DefaultResponse();
            //log.Info(JsonConvert.SerializeObject(shopOrder));
            if (shopOrder.products == null || shopOrder.products.Count == 0)
            {
                def.meta = new Meta(211, "Invalid input parameter - invalid product number");
                return Ok(def);
            }

            //check stock
            bool isEnoughStock = true;
            for (int i = 0; i < shopOrder.products.Count; i++)
            {
                int paid = (int)shopOrder.products[i].ProductAttributeId;
                int puid = (int)shopOrder.products[i].ProductUnitId;
                BranchStock bs = db.BranchStocks.Where(b => b.BranchId == shopOrder.BranchId && b.ProductAttributeId == paid).FirstOrDefault();
                ProductUnit pu = db.ProductUnits.Where(p => p.ProductUnitId == puid).FirstOrDefault();
                if (bs != null)
                {
                    if (bs.Stock < shopOrder.products[i].Quantity * pu.Quantity)
                    {
                        isEnoughStock = false;
                        break;
                    }
                }
                else
                {
                    isEnoughStock = false;
                    break;
                }
            }
            if (!isEnoughStock)
            {
                def.meta = new Meta(211, "Invalid input parameter - invalid product number");
                return Ok(def);
            }

            //validate
            if (shopOrder.BranchId == null || shopOrder.Channel == null || shopOrder.CustomerId == null || shopOrder.CustomerPaid == null || shopOrder.DeliveryPrice == null || shopOrder.DeliveryStatus == null || shopOrder.DiscountPrice == null || shopOrder.EmployeeId == null || shopOrder.MoneyCollect == null || shopOrder.Paid == null || shopOrder.PaymentChannel == null || shopOrder.products == null || shopOrder.SaleEmployeeId == null || shopOrder.ShopId == null)
            {
                def.meta = new Meta(212, "Invalid input parameter - invalid input");
                return Ok(def);
            }
            decimal totalPrice = shopOrder.OrderPrice.Value - shopOrder.DiscountPrice.Value + shopOrder.DeliveryPrice.Value;
            if (shopOrder.CustomerPaid > totalPrice)
            {
                def.meta = new Meta(213, "Invalid input parameter - customer paid greater than total price");
                return Ok(def);
            }

            //check customer
            Customer customer = db.Customers.Find(shopOrder.CustomerId);
            if (customer == null)
            {
                //invalid customer
                def.meta = new Meta(214, "Invalid input parameter - customer invalid");
                return Ok(def);
            }
            //calculate order price & total origin price
            decimal orderPrice = 0;
            decimal totalOriginPrice = 0;
            bool isValid = true;
            for (int i = 0; i < shopOrder.products.Count; i++)
            {
                int paid = (int)shopOrder.products[i].ProductAttributeId;
                int puid = (int)shopOrder.products[i].ProductUnitId;
                ProductPrice pp = db.ProductPrices.Where(p => p.ProductAttributeId == paid && p.ProductUnitId == puid && p.IsCurrent == true).FirstOrDefault();
                if (pp == null)
                {
                    //invalid data ?
                    isValid = true;
                    break;
                }
                //find average price
                var AverageImportPrice = db.EximProducts.Where(ip => ip.ProductAttributeId == paid && ip.Exim.Type == (int)Const.EximType.IMPORT).Select(ip => ip.Price).Average().HasValue ? (double)db.EximProducts.Where(ip => ip.ProductAttributeId == paid && ip.Exim.Type == (int)Const.EximType.IMPORT).Select(ip => ip.Price).Average().Value : 0;
                orderPrice += (decimal)(pp.SalePrice * shopOrder.products[i].Quantity);
                if (AverageImportPrice > 0)
                    totalOriginPrice += (decimal)(AverageImportPrice * shopOrder.products[i].Quantity);
                else
                    totalOriginPrice += (decimal)(pp.OriginPrice * shopOrder.products[i].Quantity);
            }

            if (!isValid)
            {
                def.meta = new Meta(215, "Invalid input parameter - invalid product");
                return Ok(def);
            }


            Order order = new Order();
            order.BranchId = shopOrder.BranchId;
            order.Channel = shopOrder.Channel;
            order.CreatedAt = DateTime.Now;
            order.CustomerId = shopOrder.CustomerId;
            order.CustomerPaid = shopOrder.CustomerPaid;
            order.DeliveryPrice = shopOrder.DeliveryPrice;
            order.DeliveryPaid = 0;
            order.DeliveryStatus = shopOrder.DeliveryStatus == null ? (int)Const.DeliveryStatus.INIT : shopOrder.DeliveryStatus;
            order.DeliveryVendorId = shopOrder.DeliveryVendorId;
            order.DiscountPrice = shopOrder.DiscountPrice;
            order.EmployeeId = shopOrder.EmployeeId;
            order.MoneyCollect = shopOrder.MoneyCollect;
            order.Note = shopOrder.Note;
            order.OrderAddress = shopOrder.OrderAddress;
            order.OrderCode = generateOrderCode();
            order.OrderEmail = shopOrder.OrderEmail;
            order.OrderPhone = shopOrder.OrderPhone;
            order.OrderPrice = orderPrice;
            order.TotalOriginPrice = totalOriginPrice;
            order.Paid = shopOrder.CustomerPaid;
            order.PaymentChannel = shopOrder.PaymentChannel;
            order.UpdatedAt = DateTime.Now;
            order.Type = (int)Const.OrderType.SALE;
            order.TotalPrice = shopOrder.OrderPrice + order.DeliveryPrice - order.DiscountPrice;
            int paymentStatus = (int)Const.PaymentStatus.INIT;
            if (order.Paid == order.TotalPrice)
                paymentStatus = (int)Const.PaymentStatus.COMPLETED;
            else if (order.MoneyCollect > 0)
                paymentStatus = (int)Const.PaymentStatus.WAITING_FOR_COLLECT;
            else if (order.Paid < order.TotalPrice && shopOrder.Paid > 0)
                paymentStatus = (int)Const.PaymentStatus.PAID_NOT_ENOUGH;
            else
                paymentStatus = (int)Const.PaymentStatus.INIT;
            order.PaymentStatus = paymentStatus;
            order.SaleEmployeeId = shopOrder.SaleEmployeeId;
            order.ShopId = ShopId;
            order.OrderStatus = (int)Const.PaymentStatus.COMPLETED;
            db.Orders.Add(order);
            db.SaveChanges();
            //create cashflow
            CashFlow cashFlow = new CashFlow();
            cashFlow.ShopId = ShopId;
            cashFlow.TargetId = order.OrderId;
            cashFlow.TargetType = "ORDER";
            cashFlow.PaidTargetId = customer.CustomerId;
            cashFlow.PaidTargetType = "CUSTOMER";
            cashFlow.Type = (int)Const.CashFlowType.ORDER;
            cashFlow.Amount = 0 - order.TotalPrice;
            //get lastest debt
            //lastest Debt = sum (amout) 
            var debt = db.CashFlows.Where(cf => cf.PaidTargetId == customer.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == customer.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
            cashFlow.Debt = debt + cashFlow.Amount;
            cashFlow.CreatedAt = DateTime.Now;
            db.CashFlows.Add(cashFlow);
            db.SaveChanges();
            //note
            if (order.Note != null && order.Note.Trim().Length > 0)
            {
                Note note = new Note();
                note.Content = order.Note;
                note.CreatedAt = DateTime.Now;
                note.ShopId = ShopId;
                note.Status = (int)Const.Status.NORMAL;
                note.TargetId = order.OrderId;
                note.TargetType = "ORDER";
                db.Notes.Add(note);
                db.SaveChanges();
            }
            string emailProduct = "";
            //save product
            for (int i = 0; i < shopOrder.products.Count; i++)
            {
                OrderProduct orderProduct = new OrderProduct();
                orderProduct.OrderId = order.OrderId;
                orderProduct.Price = shopOrder.products[i].Price;
                orderProduct.ProductAttributeId = shopOrder.products[i].ProductAttributeId;
                orderProduct.Quantity = shopOrder.products[i].Quantity;
                orderProduct.CreatedAt = DateTime.Now;
                orderProduct.UpdatedAt = DateTime.Now;
                orderProduct.ProductUnitId = shopOrder.products[i].ProductUnitId;
                orderProduct.TotalPrice = shopOrder.products[i].Quantity * shopOrder.products[i].Price;
                db.OrderProducts.Add(orderProduct);
               
            }
            db.SaveChanges();
            //create cash out
            if (order.PaymentStatus != (int)Const.PaymentStatus.INIT)
            {
                //cash out
                Cash cash = new Cash();
                cash.CashCode = "PT" + order.OrderCode; //generateCashCode((int)Const.CashType.INCOME);
                cash.Type = (int)Const.CashType.INCOME;               
                cash.CreatedAt = DateTime.Now;
                cash.PaidAmount = shopOrder.CustomerPaid;
                cash.ShopId = ShopId;
                cash.Status = (int)Const.Status.NORMAL;
                cash.PaidTargetId = customer.CustomerId;
                cash.PaidTargetType = "CUSTOMER";
                cash.TargetId = order.OrderId;
                cash.TargetType = "ORDER";
                cash.Title = "Phiếu thu xuất hàng cho khách " + customer.CustomerName;
                cash.Total = shopOrder.TotalPrice;
                cash.Remain = cash.Total - cash.PaidAmount;
                cash.PaymentMethod = (int)Const.PaymentType.CASH;
                cash.EmployeeId = shopOrder.EmployeeId;
                cash.UpdatedAt = DateTime.Now;
                db.Cashes.Add(cash);
                db.SaveChanges();
                //create cash flow
                CashFlow cashFlowIn = new CashFlow();
                cashFlowIn.ShopId = ShopId;
                cashFlowIn.TargetId = cash.CashId;
                cashFlowIn.TargetType = "CASH";
                cashFlowIn.PaidTargetId = customer.CustomerId;
                cashFlowIn.PaidTargetType = "CUSTOMER";
                cashFlowIn.Type = (int)Const.CashFlowType.PAID;
                cashFlowIn.Amount = cash.PaidAmount;
                //get lastest debt
                //lastest Debt = sum (amout)
                debt = db.CashFlows.Where(cf => cf.PaidTargetId == customer.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == customer.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                cashFlowIn.Debt = cashFlowIn.Amount + debt;
                cashFlowIn.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlowIn);
                db.SaveChanges();
            }

            //create export
            Exim exim = new Exim();
            exim.Code = generateEximCode((int)Const.EximType.EXPORT);
            exim.CreatetAt = DateTime.Now;
            exim.CustomDate = DateTime.Now;
            exim.DeliveryStatus = order.DeliveryStatus;
            exim.EmployeeId = shopOrder.EmployeeId;
            exim.PaymentStatus = order.PaymentStatus;
            exim.Price = order.OrderPrice;
            exim.ShopId = ShopId;
            exim.Status = (int)Const.Status.NORMAL;
            exim.TargetId = customer.CustomerId;
            exim.TargetType = "CUSTOMER";
            exim.Title = "Xuất hàng cho khách " + customer.CustomerName;
            exim.Type = (int)Const.EximType.EXPORT;
            exim.UpdatedAt = DateTime.Now;
            exim.Discount = order.DiscountPrice;
            exim.Fee = 0;
            exim.BranchId = shopOrder.BranchId;
            exim.TotalPrice = exim.Price - exim.Discount + exim.Fee;
            db.Exims.Add(exim);
            db.SaveChanges();
            //create exim product
            for (int i = 0; i < shopOrder.products.Count; i++)
            {
                EximProduct ip = new EximProduct();
                ip.CreatedAt = DateTime.Now;
                ip.EximId = exim.EximId;
                ip.DiscountPerItem = 0;
                ip.Price = shopOrder.products[i].Price;
                //chekc stock
                int productAttributeId = (int)shopOrder.products[i].ProductAttributeId;
                int productUnitId = (int)shopOrder.products[i].ProductUnitId;
                var stocks = db.BranchStocks.Where(b => b.ProductAttributeId == productAttributeId).ToList();
                int stock = 0;
                for (int j = 0; j < stocks.Count; j++)
                {
                    stock += (int)stocks[j].Stock;
                }
                //get base unit
                ProductAttribute productAttribute = db.ProductAttributes.Find(productAttributeId);
                ProductUnit productUnit = db.ProductUnits.Find(productUnitId);                
                ip.PreviousStock = stock;
                ip.ProductAttributeId = productAttributeId;
                ip.Quantity = shopOrder.products[i].Quantity * productUnit.Quantity;
                ip.ProductUnitId = productUnit.ProductUnitId;
                ip.LastStock = stock - ip.Quantity;
                ip.UpdatedAt = DateTime.Now;
                db.EximProducts.Add(ip);
                db.SaveChanges();
                var currenctStock = db.BranchStocks.Where(b => b.ProductAttributeId == productAttributeId && b.BranchId == shopOrder.BranchId).FirstOrDefault();
                currenctStock.Stock -= ip.Quantity;
                db.Entry(currenctStock).State = EntityState.Modified;
                db.SaveChanges();
            }

            //calculate delivery debt
            //    if (shopOrder.DeliveryVendorId != null)
            //    {
            //        DeliveryVendor dVendor = db.DeliveryVendors.Find(shopOrder.DeliveryVendorId);
            //        if (dVendor != null)
            //        {
            //            //create debt
            //            if (shopOrder.DeliveryPrice > 0)
            //            {
            //                //create cash
            //                //check exist
            //                DateTime now = DateTime.Now;
            //                DateTime thisMonth = new DateTime(now.Year, now.Month, 1, 00, 00, 00);
            //                var cash = (from c in db.Cashes
            //                            where c.TargetId == dVendor.DeliveryVendorId && c.TargetType == "DELIVERY_VENDOR" && c.CreatedAt >= thisMonth && c.CreatedAt <= now
            //                            select c).FirstOrDefault();
            //                if (cash == null)
            //                {
            //                    //create debt
            //                    Debt debt = new Debt();
            //                    debt.CreatedAt = DateTime.Now;
            //                    debt.DebtType = (int)Const.DebtType.SHOP_DEBT;
            //                    debt.Paid = 0;
            //                    debt.Remain = shopOrder.DeliveryPrice;
            //                    debt.ShopId = ShopId;
            //                    debt.Status = (int)Const.Status.NORMAL;
            //                    debt.TargetId = dVendor.DeliveryVendorId;
            //                    debt.TargetType = "DELIVERY_VENDOR";
            //                    debt.Title = "Nợ nhà cung cấp " + dVendor.VendorName + " tháng " + (now.Month);
            //                    debt.Total = shopOrder.DeliveryPrice;
            //                    debt.UpdatedAt = DateTime.Now;
            //                    db.Debts.Add(debt);
            //                    db.SaveChanges();
            //                    //create new
            //                    Cash dCash = new Cash();
            //                    dCash.CashCode = generateCashCode((int)Const.CashType.OUTCOME);
            //                    CashType cashIn = db.CashTypes.Where(ct => ct.CashTypeCode == "PC" && ct.ShopId == ShopId).FirstOrDefault();
            //                    if (cashIn != null)
            //                        dCash.CashTypeId = cashIn.CashTypeId;
            //                    else
            //                        dCash.CashTypeId = -1;
            //                    dCash.CreatedAt = DateTime.Now;
            //                    dCash.EmployeeId = shopOrder.EmployeeId;
            //                    dCash.Note = shopOrder.Note;
            //                    dCash.PaidAmount = 0;
            //                    dCash.PaidTargetId = dVendor.DeliveryVendorId;
            //                    dCash.PaidTargetType = "DELIVERY_VENDOR";
            //                    dCash.PaymentMethod = (int)Const.PaymentType.CASH;
            //                    dCash.ShopId = ShopId;
            //                    dCash.Status = (int)Const.Status.NORMAL;
            //                    dCash.TargetId = dVendor.DeliveryVendorId;
            //                    dCash.TargetType = "DELIVERY_VENDOR";
            //                    dCash.Title = "Thanh toán vận chuyển cho " + dVendor.VendorName + " tháng " + (now.Month);
            //                    dCash.Total = shopOrder.DeliveryPrice;
            //                    dCash.UpdatedAt = DateTime.Now;
            //                    db.Cashes.Add(dCash);
            //                    db.SaveChanges();
            //                }
            //                else
            //                {
            //                    //update
            //                    //find debt
            //                    //Debt debt = db.Debts.Find(cash.DebtId);
            //                    //debt.Remain += shopOrder.DeliveryPrice;
            //                    //debt.Total += shopOrder.DeliveryPrice;
            //                    //debt.UpdatedAt = DateTime.Now;
            //                    //db.Entry(debt).State = EntityState.Modified;
            //                    //cash.Total += shopOrder.DeliveryPrice;
            //                    //cash.UpdatedAt = DateTime.Now;
            //                    //db.Entry(cash).State = EntityState.Modified;
            //                    //db.SaveChanges();
            //                }
            //            }
            //        }
            //    }         

            //create action
            Action action = new Action();
            action.ActionName = "tạo đơn hàng";
            action.ActionType = "ADD";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = shopOrder.EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = order.OrderId;
            action.TargetType = "ORDER";
            db.Actions.Add(action);
            db.SaveChanges();

            if (order.OrderId > 0)
            {
                //send email
                //sendOrderEmail(order.OrderId);
                var tasks = new[]
                {
                    Task.Run(() => sendOrderEmail(order.OrderId))
                };
                def.meta = new Meta(200, "Success");
                def.data = order.OrderId;
            }
            else
                def.meta = new Meta(201, "Fail");
            return Ok(def);
        }
        #endregion
        private async void sendOrderEmail(int OrderId)
        {
            try
            {
                Order order = db.Orders.Find(OrderId);
                if (order != null)
                {
                    Employee admin = db.Employees.Where(e => e.Group == "Admin" && e.ShopId == order.ShopId).FirstOrDefault();
                    var result = (from o in db.OrderProducts
                                  join pa in db.ProductAttributes on o.ProductAttributeId equals pa.ProductAttributeId
                                  join p in db.Products on pa.ProductId equals p.ProductId
                                  where o.OrderId == order.OrderId
                                  orderby o.CreatedAt descending
                                  select new SaleManager.Data.OrderDetailProduct
                                  {
                                      OrderId = o.OrderId,
                                      OrderProductId = o.OrderProductId,
                                      ProductId = p.ProductId,
                                      ProductName = p.ProductName,
                                      SubName = pa.SubName,
                                      Price = o.Price,
                                      TotalPrice = o.Price * o.Quantity,
                                      Quantity = o.Quantity,
                                      ProductAttributeId = o.ProductAttributeId,
                                      ProductCode = pa.ProductCode,
                                      unit = new SaleManager.Data.Unit()
                                      {
                                          Name = o.ProductUnit.Unit,
                                          ProductUnitId = o.ProductUnitId
                                      },
                                      images = p.ProductImages.Select(i => new Data.PI() { Alt = i.Alt, Path = i.Path, ProductImageId = i.ProductImageId, RelativePath = i.RelativePath }).ToList(),
                                  }
                             ).ToList();
                    //email body
                    string emailProduct = "";
                    for (int i = 0; i < result.Count; i++)
                    {
                        emailProduct += "<tr style='border-bottom:1px solid #d7d7d7;border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7'>";
                        emailProduct += "   <td style='padding:5px 10px'>" + result[i].ProductName + " " + result[i].SubName + "</td>";
                        emailProduct += "   <td style='text-align:center;padding:5px 10px'>" + result[i].ProductCode + "</td>";
                        emailProduct += "   <td style='text-align:center;padding:5px 10px'>" + result[i].Quantity + "</td>";
                        emailProduct += "   <td style='padding:5px 10px;text-align:right'>" + result[i].TotalPrice.Value.ToString("n0") + " VND</td>";
                        emailProduct += "</tr>";
                    }
                    String sBody = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Email/mail-order.html"));
                    string deliveryStatus = order.DeliveryStatus == 2 ? "Đã giao hàng" : "Chưa giao hàng";
                    sBody = String.Format(sBody, admin.EmployeeName, order.OrderCode, order.Customer.CustomerName, order.Customer.Phone, order.OrderCode, order.CreatedAt.Value.ToString("dd/MM/yyyy HH:mm"),
                        emailProduct, order.DiscountPrice.Value.ToString("n0"), order.DeliveryPrice.Value.ToString("n0"), order.TotalPrice.Value.ToString("n0"), order.CustomerPaid.Value.ToString("n0"), deliveryStatus);
                    //Utils.sendNotifyEmail(admin.Email, admin.EmployeeName, "Bạn có đơn hàng mới " + order.OrderCode, sBody);
                    var fromAddress = new MailAddress("noreply.fbsale@gmail.com", "FB.SALE");
                    var toAddress = new MailAddress(admin.Email, admin.EmployeeName);

                    using (var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        Timeout = 30000,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, "fbsale@#123")
                    })
                    {
                        using (var message = new MailMessage(fromAddress, toAddress))
                        {
                            message.IsBodyHtml = true;
                            message.Subject = "Bạn có đơn hàng mới " + order.OrderCode;
                            message.Body = sBody;
                            try
                            {
                                smtp.Send(message);
                                message.Dispose();
                                smtp.Dispose();
                            }
                            catch (Exception ex)
                            {
                                log.Info(ex.Message + "-" + ex.StackTrace);
                            }
                            finally
                            {
                                message.Dispose();
                                smtp.Dispose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("send email error : " + ex.Message + "-" + ex.StackTrace);
            }
        }

        [HttpPost]
        [Route("api/shops/{id}/products")]
        public IHttpActionResult createProduct(int id, CreateProduct createProduct)
        {
            string domain = ConfigurationManager.AppSettings["domain"].ToString();
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //validate input
            if (createProduct.BaseUnit == null || createProduct.BaseUnit.Trim().Length == 0)
            {
                def.meta = new Meta(211, "Invalid input");
                return Ok(def);
            }
            //create product
            Product product = new Product();
            product.AllowDelivery = createProduct.AllowDelivery;
            product.AllowOrder = createProduct.AllowOrder;
            product.AllowSell = createProduct.AllowSell;
            product.AvailableBranchIds = "";
            product.CategoryId = createProduct.CategoryId;
            product.CreatedAt = DateTime.Now;
            product.ManufactureId = createProduct.ManufactureId;
            product.MinStock = createProduct.MinStock != null ? createProduct.MinStock : 0;
            product.ProductDesc = createProduct.ProductDesc;
            product.ProductName = createProduct.ProductName;
            product.ShopId = ShopId;
            product.Status = (int)Const.ProductStatus.NORMAL;
            product.StockLimit = createProduct.StockLimit != null ? createProduct.StockLimit : 0;
            product.Tag = "";
            product.UpdatedAt = DateTime.Now;
            db.Products.Add(product);
            db.SaveChanges();
            //create product attribute
            //product base unit
            ProductUnit unit = new ProductUnit();
            unit.ProductId = product.ProductId;
            unit.Quantity = 1;
            unit.Unit = createProduct.BaseUnit;
            unit.Status = (int)Const.Status.NORMAL;
            db.ProductUnits.Add(unit);
            db.SaveChanges();

            if (createProduct.units != null && createProduct.units.Count > 0)
            {
                for (int k = 0; k < createProduct.units.Count; k++)
                {
                    int quantity = Int32.Parse(createProduct.units[k].Value);
                    string unitName = createProduct.units[k].Name.Trim().ToLower();

                    if (createProduct.units[k].Status != "DELETE")
                    {
                        ProductUnit otherUnit = new ProductUnit();
                        otherUnit.ProductId = product.ProductId;
                        otherUnit.Quantity = quantity;
                        otherUnit.Unit = createProduct.units[k].Name;
                        otherUnit.Status = (int)Const.Status.NORMAL;
                        db.ProductUnits.Add(otherUnit);
                        db.SaveChanges();
                    }
                }
            }
            //find unit product
            List<ProductUnit> units = db.ProductUnits.Where(pu => pu.ProductId == product.ProductId).ToList();
            //product unit           
            if (createProduct.attributeSets != null && createProduct.attributeSets.Count > 0 && createProduct.attributeSets[0].Count > 0)
            {
                //each set
                createProduct.CurrentStock = 0;
                int count = createProduct.attributeSets.Count;
                for (int i = 0; i < createProduct.attributeSets.Count; i++)
                {
                    if (createProduct.attributeSets[i].Count == 0)
                    {
                        //a
                        continue;
                    }
                    //create product attribute each set
                    ProductAttribute productAttribute = new ProductAttribute();
                    productAttribute.Code = "";
                    productAttribute.CreatedAt = DateTime.Now;
                    productAttribute.Discount = 0;
                    productAttribute.NetWeight = createProduct.NetWeight;
                    //string code = generateProductCode();
                    //if (code != createProduct.ProductCode)
                    //    productAttribute.ProductCode = createProduct.ProductCode;
                    //else
                    //    productAttribute.ProductCode = code;
                    productAttribute.ProductCode = generateProductCode();
                    productAttribute.ProductId = product.ProductId;
                    productAttribute.Status = (int)Const.Status.NORMAL;
                    //generate sub name base on attribute
                    string subName = "";
                    for (int j = 0; j < createProduct.attributeSets[i].Count; j++)
                    {
                        //find name 
                        subName += db.Attributes.Find(Int32.Parse(createProduct.attributeSets[i][j].AttributeId)).AttributeName + " ";
                        subName += createProduct.attributeSets[i][j].Value + " ";
                    }
                    productAttribute.SubName = subName;
                    productAttribute.UpdatedAt = DateTime.Now;
                    productAttribute.SearchQuery = Utils.unsignString(product.ProductName + " " + productAttribute.SubName + " " + productAttribute.ProductCode);
                    db.ProductAttributes.Add(productAttribute);
                    db.SaveChanges();
                    //branhc stock
                    var branches = db.Branches.Where(b => b.ShopId == id).ToList();
                    if (branches != null)
                    {
                        for (int m = 0; m < branches.Count; m++)
                        {
                            BranchStock stock = new BranchStock();
                            stock.BranchId = branches[m].BranchId;
                            stock.ProductAttributeId = productAttribute.ProductAttributeId;
                            stock.Stock = 0;
                            stock.UpdatedAt = DateTime.Now;
                            db.BranchStocks.Add(stock);
                            db.SaveChanges();
                        }
                    }
                    //map attribute
                    for (int j = 0; j < createProduct.attributeSets[i].Count; j++)
                    {
                        AttributeMap map = new AttributeMap();
                        map.AttributeId = Int32.Parse(createProduct.attributeSets[i][j].AttributeId);
                        map.CreatedAt = DateTime.Now;
                        map.ProductAttributeId = productAttribute.ProductAttributeId;
                        map.Status = (int)Const.Status.NORMAL;
                        map.UpdatedAt = DateTime.Now;
                        map.Value = createProduct.attributeSets[i][j].Value;
                        db.AttributeMaps.Add(map);
                        db.SaveChanges();
                    }
                    //base
                    ProductPrice productPrice = new ProductPrice();
                    productPrice.IsCurrent = true;
                    productPrice.FromDate = DateTime.Now;
                    productPrice.OriginPrice = createProduct.OriginPrice;
                    productPrice.ProductAttributeId = productAttribute.ProductAttributeId;
                    productPrice.ProductUnitId = unit.ProductUnitId;
                    productPrice.SalePrice = createProduct.SalePrice;
                    productPrice.UtilDate = DateTime.Now;
                    db.ProductPrices.Add(productPrice);
                    db.SaveChanges();
                    //product price
                    //if (units != null && units.Count > 0 && createProduct.units != null)
                    //{
                    //    for (int k = 0; k < units.Count; k++)
                    //    {
                    //        //find other
                    //        for (int l = 0; l < createProduct.units.Count; l++)
                    //        {
                    //            if (units[k].Unit == createProduct.units[l].Name)
                    //            {
                    //                ProductPrice otherProductPrice = new ProductPrice();
                    //                otherProductPrice.IsCurrent = true;
                    //                otherProductPrice.FromDate = DateTime.Now;
                    //                otherProductPrice.OriginPrice = units[k].Quantity * createProduct.OriginPrice;
                    //                otherProductPrice.ProductAttributeId = productAttribute.ProductAttributeId;
                    //                otherProductPrice.ProductUnitId = units[k].ProductUnitId;
                    //                otherProductPrice.SalePrice = Decimal.Parse(createProduct.units[l].Price);
                    //                otherProductPrice.UtilDate = DateTime.Now;
                    //                db.ProductPrices.Add(otherProductPrice);
                    //                db.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
            else
            {
                //product has no attribute
                ProductAttribute productAttribute = new ProductAttribute();
                productAttribute.Code = "";
                productAttribute.CreatedAt = DateTime.Now;
                productAttribute.Discount = 0;
                productAttribute.NetWeight = createProduct.NetWeight;
                productAttribute.ProductCode = generateProductCode(); ;
                productAttribute.ProductId = product.ProductId;
                productAttribute.Status = (int)Const.Status.NORMAL;
                productAttribute.SubName = "";
                productAttribute.UpdatedAt = DateTime.Now;
                productAttribute.SearchQuery = Utils.unsignString(product.ProductName + productAttribute.ProductCode);
                db.ProductAttributes.Add(productAttribute);
                db.SaveChanges();
                //branch stock
                var branches = db.Branches.Where(b => b.ShopId == id).ToList();
                if (branches != null)
                {
                    for (int i = 0; i < branches.Count; i++)
                    {
                        BranchStock stock = new BranchStock();
                        stock.BranchId = branches[i].BranchId;
                        stock.ProductAttributeId = productAttribute.ProductAttributeId;
                        stock.Stock = 0;
                        stock.UpdatedAt = DateTime.Now;
                        db.BranchStocks.Add(stock);
                        db.SaveChanges();
                    }
                }
                // >> create product price base on base unit
                ProductPrice productPrice = new ProductPrice();
                productPrice.IsCurrent = true;
                productPrice.FromDate = DateTime.Now;
                productPrice.OriginPrice = createProduct.OriginPrice;
                productPrice.ProductAttributeId = productAttribute.ProductAttributeId;
                productPrice.ProductUnitId = unit.ProductUnitId;
                productPrice.SalePrice = createProduct.SalePrice;
                productPrice.UtilDate = DateTime.Now;
                db.ProductPrices.Add(productPrice);
                db.SaveChanges();
                if (units != null && units.Count > 0 && createProduct.units != null)
                {
                    for (int k = 0; k < units.Count; k++)
                    {
                        for (int l = 0; l < createProduct.units.Count; l++)
                        {
                            if (units[k].Unit == createProduct.units[l].Name)
                            {
                                ProductPrice otherProductPrice = new ProductPrice();
                                otherProductPrice.IsCurrent = true;
                                otherProductPrice.FromDate = DateTime.Now;
                                otherProductPrice.OriginPrice = units[k].Quantity * createProduct.OriginPrice;
                                otherProductPrice.ProductAttributeId = productAttribute.ProductAttributeId;
                                otherProductPrice.ProductUnitId = units[k].ProductUnitId;
                                otherProductPrice.SalePrice = Decimal.Parse(createProduct.units[l].Price);
                                otherProductPrice.UtilDate = DateTime.Now;
                                db.ProductPrices.Add(otherProductPrice);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }

            if (product.ProductId > 0)
            {
                def.meta = new Meta(200, "Success");
                def.data = product.ProductId;
            }
            else
                def.meta = new Meta(201, "Fail");
            return Ok(def);
        }

        [HttpPut]
        [Route("api/shops/{id}/products/{paid}")]
        public IHttpActionResult updateProduct(int id, int paid, UpdateProduct updateProduct)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //check valid request
            if (updateProduct.ShopId != ShopId || updateProduct.ProductId == null || updateProduct.ProductId <= 0 || updateProduct.ProductAttributeId == null || updateProduct.ProductAttributeId <= 0)
            {
                def.meta = new Meta(400, "Bad request");
                return Ok(def);
            }

            Product product = db.Products.Find(updateProduct.ProductId);
            ProductAttribute productAttribute = db.ProductAttributes.Find(updateProduct.ProductAttributeId);
            if (productAttribute.ProductId != product.ProductId)
            {
                def.meta = new Meta(400, "Bad request");
                return Ok(def);
            }

            //update product
            product.AllowDelivery = updateProduct.AllowDelivery;
            product.AllowOrder = updateProduct.AllowOrder;
            product.AllowSell = updateProduct.AllowSell;
            product.AvailableBranchIds = "";
            product.CategoryId = updateProduct.CategoryId;
            product.ManufactureId = updateProduct.ManufactureId;
            product.MinStock = updateProduct.MinStock;
            product.ProductDesc = updateProduct.ProductDesc;
            product.ProductName = updateProduct.ProductName;
            product.StockLimit = updateProduct.StockLimit;
            product.Tag = "";
            product.UpdatedAt = DateTime.Now;
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            //delete image
            if (updateProduct.deletedImages != null && updateProduct.deletedImages.Count > 0)
            {
                for (int i = 0; i < updateProduct.deletedImages.Count; i++)
                {
                    ProductImage pi = db.ProductImages.Find(updateProduct.deletedImages[i].ProductImageId);
                    if (pi != null)
                        db.ProductImages.Remove(pi);
                }
                db.SaveChanges();
            }
            //update product attribute
            //check attribute
            List<UpdateProductAttribute> attrs = updateProduct.attrs;
            if (attrs != null && attrs.Count > 0)
            {
                for (int i = 0; i < attrs.Count; i++)
                {
                    if (attrs[i].Status == "NEW" || attrs[i].Status == "UPDATE" || attrs[i].Status == null)
                    {
                        //create new attribute
                        //check attribute exist or not
                        int AttributeId = (int)attrs[i].AttributeId;
                        AttributeMap map = db.AttributeMaps.Where(am => am.ProductAttributeId == productAttribute.ProductAttributeId && am.AttributeId == AttributeId).FirstOrDefault();
                        if (map == null)
                        {
                            map = new AttributeMap();
                            map.AttributeId = attrs[i].AttributeId;
                            map.CreatedAt = DateTime.Now;
                            map.ProductAttributeId = productAttribute.ProductAttributeId;
                            map.Status = (int)Const.Status.NORMAL;
                            map.UpdatedAt = DateTime.Now;
                            map.Value = attrs[i].AttributeValue;
                            db.AttributeMaps.Add(map);
                            db.SaveChanges();
                        }
                        else
                        {
                            //update
                            if (map.Value.Trim() != attrs[i].AttributeValue.Trim())
                            {
                                map.Value = attrs[i].AttributeValue;
                                db.SaveChanges();
                            }
                        }
                    }
                    else if (attrs[i].Status == "DELETE")
                    {
                        AttributeMap map = db.AttributeMaps.Find(attrs[i].AttributeMapId);
                        if (map != null)
                        {
                            db.AttributeMaps.Remove(map);
                            db.SaveChanges();
                        }
                    }
                }
            }
            //update subname
            var maps = (from am in db.AttributeMaps
                        join a in db.Attributes on am.AttributeId equals a.AttributeId
                        where am.ProductAttributeId == productAttribute.ProductAttributeId
                        select new UpdateProductAttribute
                        {
                            AttributeName = a.AttributeName,
                            AttributeValue = am.Value
                        }).ToList();
            string subName = "";
            for (int j = 0; j < maps.Count; j++)
            {
                //find name 
                subName += maps[j].AttributeName + " ";
                subName += maps[j].AttributeValue + " ";
            }
            productAttribute.SubName = subName;
            productAttribute.SearchQuery = Utils.unsignString(product.ProductName + " " + subName + " " + productAttribute.ProductCode);
            db.SaveChanges();
            //unit and price
            List<UpdateProductPrices> prices = updateProduct.prices;
            if (prices != null && prices.Count > 0)
            {
                for (int i = 0; i < prices.Count; i++)
                {
                    string unit = prices[i].Unit.Trim();
                    ProductUnit productUnit = db.ProductUnits.Where(pu => pu.Unit == unit && pu.ProductId == product.ProductId).FirstOrDefault();
                    if (prices[i].Status == "DELETE")
                    {
                        //delete unit ?? 
                        if (productUnit != null)
                        {
                            productUnit.Status = 99;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        //update or new
                        if (productUnit != null)
                        {
                            //update
                            //update only selected product attribute
                            ProductPrice price = db.ProductPrices.Where(pp => pp.ProductAttributeId == productAttribute.ProductAttributeId && pp.ProductUnitId == productUnit.ProductUnitId).OrderByDescending(pp => pp.ProductPriceId).FirstOrDefault();
                            if (price != null)
                            {
                                //if price dif
                                //jusst update and create new 
                                if (prices[i].Quantity == 1)//base change
                                {
                                    if (price.SalePrice != updateProduct.SalePrice)
                                    {
                                        //create new
                                        ProductPrice newPrice = new ProductPrice();
                                        newPrice.FromDate = DateTime.Now;
                                        newPrice.IsCurrent = true;
                                        newPrice.OriginPrice = updateProduct.OriginPrice;
                                        newPrice.ProductAttributeId = productAttribute.ProductAttributeId;
                                        newPrice.ProductUnitId = productUnit.ProductUnitId;
                                        newPrice.SalePrice = updateProduct.SalePrice;
                                        newPrice.UtilDate = DateTime.Now;
                                        price.UtilDate = DateTime.Now;
                                        price.IsCurrent = false;
                                        db.ProductPrices.Add(newPrice);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    if (price.SalePrice != prices[i].SalePrice)
                                    {
                                        //create new
                                        ProductPrice newPrice = new ProductPrice();
                                        newPrice.FromDate = DateTime.Now;
                                        newPrice.IsCurrent = true;
                                        newPrice.OriginPrice = updateProduct.OriginPrice * productUnit.Quantity;
                                        newPrice.ProductAttributeId = productAttribute.ProductAttributeId;
                                        newPrice.ProductUnitId = productUnit.ProductUnitId;
                                        newPrice.SalePrice = prices[i].SalePrice;
                                        newPrice.UtilDate = DateTime.Now;
                                        price.UtilDate = DateTime.Now;
                                        price.IsCurrent = false;
                                        db.ProductPrices.Add(newPrice);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        else
                        {
                            //new
                            productUnit = new ProductUnit();
                            productUnit.ProductId = product.ProductId;
                            productUnit.Quantity = prices[i].Quantity;
                            productUnit.Status = (int)Const.Status.NORMAL;
                            productUnit.Unit = prices[i].Unit;
                            db.ProductUnits.Add(productUnit);
                            db.SaveChanges();
                            //create new price each product attribute
                            var pas = db.ProductAttributes.Where(pa => pa.ProductId == product.ProductId).ToList();
                            for (int j = 0; j < pas.Count; j++)
                            {
                                ProductPrice pp = new ProductPrice();
                                pp.FromDate = DateTime.Now;
                                pp.IsCurrent = true;
                                pp.OriginPrice = updateProduct.OriginPrice * productUnit.Quantity;
                                pp.ProductAttributeId = pas[j].ProductAttributeId;
                                pp.ProductUnitId = productUnit.ProductUnitId;
                                pp.SalePrice = prices[i].SalePrice;
                                pp.UtilDate = DateTime.Now;
                                db.ProductPrices.Add(pp);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpPost]
        [Route("api/shops/{id}/imports")]
        public IHttpActionResult createImport(int id, CreateImport createImport)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //create order
            //check if product empty
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }

            //check paramater validity
            if (createImport.BranchId == null || createImport.discount == null || createImport.EmployeeId == null || createImport.isTemp == null || createImport.paid == null || createImport.PaymentType == null || createImport.selectedProducts == null || createImport.selectedProducts.Count == 0 || createImport.selectedVendorId == null || createImport.VendorName == null)
            {
                def.meta = new Meta(212, "Invalid input parameter - invalid input");
                return Ok(def);
            }

            //tạo phiếu nhập
            Exim import = new Exim();
            import.CreatetAt = DateTime.Now;
            import.CustomDate = DateTime.Now;
            import.DeliveryStatus = createImport.isTemp ? (int)Const.DeliveryStatus.INIT : (int)Const.DeliveryStatus.COMPLETED;
            import.EmployeeId = createImport.EmployeeId;
            import.Code = generateEximCode((int)Const.EximType.IMPORT);
            //calculate import price
            decimal price = 0;
            for (int i = 0; i < createImport.selectedProducts.Count; i++)
            {
                decimal productPrice = (createImport.selectedProducts[i].OriginPrice - createImport.selectedProducts[0].Discount) * createImport.selectedProducts[i].Quantity;
                price += productPrice;
            }
            import.Price = price;
            import.Discount = createImport.discount;
            import.Fee = 0;
            import.TotalPrice = import.Price - import.Discount + import.Fee;
            //check payment status - trạng thái thanh toán
            int paymentStatus = (int)Const.PaymentStatus.INIT;
            if (createImport.paid == import.TotalPrice)
                paymentStatus = (int)Const.PaymentStatus.COMPLETED;
            else if (createImport.paid < import.TotalPrice && createImport.paid > 0)
                paymentStatus = (int)Const.PaymentStatus.PAID_NOT_ENOUGH;
            else
                paymentStatus = (int)Const.PaymentStatus.INIT;
            import.PaymentStatus = paymentStatus;
            import.ShopId = ShopId;
            import.TargetId = createImport.selectedVendorId;
            import.TargetType = "VENDOR";
            import.Title = "Nhập hàng từ " + createImport.VendorName;
            import.UpdatedAt = DateTime.Now;
            import.Type = (int)Const.EximType.IMPORT;
            import.BranchId = createImport.BranchId;
            int eximStatus = 0;
            if (import.DeliveryStatus == (int)Const.DeliveryStatus.COMPLETED && import.PaymentStatus == (int)Const.PaymentStatus.COMPLETED)
                eximStatus = (int)Const.EximStatus.COMPLETED;
            else
                eximStatus = (int)Const.EximStatus.INCOMPLETED;
            import.Status = createImport.isTemp ? (int)Const.EximStatus.TEMP : eximStatus;
            db.Exims.Add(import);
            db.SaveChanges();
            //TEMP
            if (createImport.isTemp)
            {
                for (int i = 0; i < createImport.selectedProducts.Count; i++)
                {
                    int productAttributeId = createImport.selectedProducts[i].ProductAttributeId;
                    EximProduct ip = new EximProduct();
                    //find base unit
                    ProductAttribute productAttribute = db.ProductAttributes.Find(productAttributeId);
                    int ProductUnitId = -1;
                    if (productAttribute != null)
                    {
                        var productUnit = db.ProductUnits.Where(pu => pu.ProductId == productAttribute.Product.ProductId && pu.Quantity == 1).FirstOrDefault();
                        if (productUnit != null)
                            ProductUnitId = productUnit.ProductUnitId;
                    }
                    //end find base unit
                    ip.CreatedAt = DateTime.Now;
                    ip.EximId = import.EximId;
                    ip.DiscountPerItem = createImport.selectedProducts[i].Discount;
                    ip.Price = createImport.selectedProducts[i].OriginPrice;
                    ip.PreviousStock = 0;
                    ip.ProductAttributeId = createImport.selectedProducts[i].ProductAttributeId;
                    ip.Quantity = createImport.selectedProducts[i].Quantity;
                    ip.ProductUnitId = ProductUnitId;
                    ip.LastStock = 0;
                    ip.UpdatedAt = DateTime.Now;
                    db.EximProducts.Add(ip);
                    db.SaveChanges();
                }
                //if paid >> create cash
                if (paymentStatus != (int)Const.PaymentStatus.INIT)
                {
                    //cash out
                    Cash cash = new Cash();
                    cash.CashCode = "PC" + import.Code; //generateCashCode((int)Const.CashType.OUTCOME);
                    cash.Type = (int)Const.CashType.OUTCOME;                      
                    cash.CreatedAt = DateTime.Now;
                    cash.PaidAmount = createImport.paid;
                    cash.ShopId = ShopId;
                    cash.Status = (int)Const.Status.NORMAL;
                    cash.PaidTargetId = createImport.selectedVendorId;
                    cash.PaidTargetType = "VENDOR";
                    cash.TargetId = import.EximId;
                    cash.TargetType = "IMPORT";
                    if (paymentStatus == (int)Const.PaymentStatus.PAID_NOT_ENOUGH)
                    {
                        cash.Title = "Phiếu chi nhập hàng công ty " + createImport.VendorName;
                        cash.Remain = import.TotalPrice - createImport.paid;
                    }
                    else if (paymentStatus == (int)Const.PaymentStatus.COMPLETED)
                    {
                        cash.Title = "Phiếu chi nhập hàng công ty " + createImport.VendorName;
                        cash.Remain = 0;
                    }
                    cash.Total = import.TotalPrice;
                    cash.PaymentMethod = createImport.PaymentType;
                    cash.UpdatedAt = DateTime.Now;
                    cash.EmployeeId = createImport.EmployeeId;
                    db.Cashes.Add(cash);
                    db.SaveChanges();

                    //create cash flow
                    CashFlow cashFlow = new CashFlow();
                    cashFlow.ShopId = ShopId;
                    cashFlow.TargetId = import.EximId;
                    cashFlow.TargetType = "IMPORT";
                    cashFlow.PaidTargetId = import.TargetId;
                    cashFlow.PaidTargetType = "VENDOR";
                    cashFlow.Type = (int)Const.CashFlowType.IMPORT;
                    cashFlow.Amount = import.TotalPrice;
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                    cashFlow.Debt = debt + cashFlow.Amount;
                    cashFlow.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlow);
                    db.SaveChanges();
                    CashFlow cashFlowOut = new CashFlow();
                    cashFlowOut.ShopId = ShopId;
                    cashFlowOut.TargetId = cash.CashId;
                    cashFlowOut.TargetType = "CASH";
                    cashFlowOut.PaidTargetId = cash.PaidTargetId;
                    cashFlowOut.PaidTargetType = cash.PaidTargetType;
                    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    cashFlowOut.Amount = 0 - cash.PaidAmount;
                    var debt2 = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                    cashFlowOut.Debt = debt2 + cashFlowOut.Amount;
                    cashFlowOut.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlowOut);
                    db.SaveChanges();
                }
            }
            else
            {
                //create cash flow
                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = ShopId;
                cashFlow.TargetId = import.EximId;
                cashFlow.TargetType = "IMPORT";
                cashFlow.PaidTargetId = import.TargetId;
                cashFlow.PaidTargetType = "VENDOR";
                cashFlow.Type = (int)Const.CashFlowType.IMPORT;
                cashFlow.Amount = import.TotalPrice;
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                cashFlow.Debt = debt + cashFlow.Amount;
                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();
                //exim product
                //đã nhập hàng thì mới + stock         
                for (int i = 0; i < createImport.selectedProducts.Count; i++)
                {
                    int productAttributeId = createImport.selectedProducts[i].ProductAttributeId;
                    EximProduct ip = new EximProduct();
                    //find base unit
                    ProductAttribute productAttribute = db.ProductAttributes.Find(productAttributeId);
                    int ProductUnitId = -1;
                    if (productAttribute != null)
                    {
                        var productUnit = db.ProductUnits.Where(pu => pu.ProductId == productAttribute.Product.ProductId && pu.Quantity == 1).FirstOrDefault();
                        if (productUnit != null)
                            ProductUnitId = productUnit.ProductUnitId;
                    }
                    //end find base unit
                    //find current stock
                    var stocks = db.BranchStocks.Where(b => b.ProductAttributeId == productAttributeId).ToList();
                    int stock = 0;
                    BranchStock bs = db.BranchStocks.Where(b => b.BranchId == createImport.BranchId && b.ProductAttributeId == productAttributeId).FirstOrDefault();
                    if (bs != null)
                    {
                        stock = (int)bs.Stock;
                        bs.Stock += (int)createImport.selectedProducts[i].Quantity;
                        db.SaveChanges();
                    }
                    //end find current stock
                    ip.CreatedAt = DateTime.Now;
                    ip.EximId = import.EximId;
                    ip.DiscountPerItem = createImport.selectedProducts[i].Discount;
                    ip.Price = createImport.selectedProducts[i].OriginPrice;
                    ip.PreviousStock = stock;
                    ip.ProductAttributeId = createImport.selectedProducts[i].ProductAttributeId;
                    ip.Quantity = createImport.selectedProducts[i].Quantity;
                    ip.ProductUnitId = ProductUnitId;
                    ip.LastStock = stock + ip.Quantity;
                    ip.UpdatedAt = DateTime.Now;
                    db.EximProducts.Add(ip);
                    db.SaveChanges();
                    //end import                                     
                }
                //create note
                if (createImport.Note != null && createImport.Note.Trim() != "")
                {
                    Note note = new Note();
                    note.Content = createImport.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = (int)Const.Status.NORMAL;
                    note.TargetId = import.EximId;
                    note.TargetType = "IMPORT";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }

                //cash out chi
                if (paymentStatus != (int)Const.PaymentStatus.INIT)
                {
                    //cash out
                    Cash cash = new Cash();
                    cash.CashCode = "PC" + import.Code;//generateCashCode((int)Const.CashType.OUTCOME);
                    cash.Type = (int)Const.CashType.OUTCOME;                  
                    cash.CreatedAt = DateTime.Now;
                    cash.PaidAmount = createImport.paid;
                    cash.ShopId = ShopId;
                    cash.Status = (int)Const.Status.NORMAL;
                    cash.PaidTargetId = createImport.selectedVendorId;
                    cash.PaidTargetType = "VENDOR";
                    cash.TargetId = import.EximId;
                    cash.TargetType = "IMPORT";
                    if (paymentStatus == (int)Const.PaymentStatus.PAID_NOT_ENOUGH)
                    {
                        cash.Title = "Phiếu chi nhập hàng công ty " + createImport.VendorName;
                        cash.Remain = import.TotalPrice - createImport.paid;
                    }
                    else if (paymentStatus == (int)Const.PaymentStatus.COMPLETED)
                    {
                        cash.Title = "Phiếu chi nhập hàng công ty " + createImport.VendorName;
                        cash.Remain = 0;
                    }
                    cash.Total = import.TotalPrice;
                    cash.PaymentMethod = createImport.PaymentType;
                    cash.UpdatedAt = DateTime.Now;
                    cash.EmployeeId = createImport.EmployeeId;
                    db.Cashes.Add(cash);
                    db.SaveChanges();
                    CashFlow cashFlowOut = new CashFlow();
                    cashFlowOut.ShopId = ShopId;
                    cashFlowOut.TargetId = cash.CashId;
                    cashFlowOut.TargetType = "CASH";
                    cashFlowOut.PaidTargetId = cash.PaidTargetId;
                    cashFlowOut.PaidTargetType = cash.PaidTargetType;
                    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    cashFlowOut.Amount = 0 - cash.PaidAmount;
                    debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                    cashFlowOut.Debt = debt + cashFlowOut.Amount;
                    cashFlowOut.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlowOut);
                    db.SaveChanges();
                }
            }

            //create action
            Action action = new Action();
            action.ActionName = "nhập hàng";
            action.ActionType = "ADD";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = createImport.EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = import.EximId;
            action.TargetType = "IMPORT";
            db.Actions.Add(action);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        //cancel Import
        [HttpPost]
        [Route("api/shops/{id}/imports/{importid}/cancel")]
        public IHttpActionResult cancelImport(int id, CancelImport cancelImport)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //create order
            //check if product empty
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }

            //check paramater validity
            if (cancelImport.EmployeeId == null || cancelImport.EximId == null)
            {
                def.meta = new Meta(212, "Invalid input parameter - invalid input");
                return Ok(def);
            }
            //find exim
            Exim exim = db.Exims.Find(cancelImport.EximId);
            if (exim != null)
            {
                //ok
                if (exim.Status == (int)Const.EximStatus.TEMP)
                {
                    //cancel
                    exim.Status = (int)Const.EximStatus.CANCELED;
                    db.Entry(exim).State = EntityState.Modified;
                    db.SaveChanges();
                    //craete action
                    Action action = new Action();
                    action.ActionName = "hủy phiếu tạm";
                    action.ActionType = "CANCEL";
                    action.CreatedAt = DateTime.Now;
                    action.EmployeeId = cancelImport.EmployeeId;
                    action.Result = 0;
                    action.ShopId = ShopId;
                    action.TargetId = exim.EximId;
                    action.TargetType = "IMPORT";
                    db.Actions.Add(action);
                    db.SaveChanges();
                    //cash in
                    List<Cash> cashes = db.Cashes.Where(c => c.TargetId == exim.EximId && c.TargetType == "IMPORT").ToList();
                    if (cashes != null && cashes.Count > 0)
                    {
                        decimal totalPaid = 0;
                        
                        for (int i = 0; i < cashes.Count; i++)
                        {
                            totalPaid += (decimal)cashes[i].PaidAmount;
                        }

                        decimal totalRemain = (decimal)cashes[0].Total - totalPaid;
                        //create cash in
                        Cash cash = new Cash();
                        cash.CashCode = "PT" + exim.Code;//generateCashCode((int)Const.CashType.INCOME);
                        cash.Type = (int)Const.CashType.INCOME;                  
                        cash.CreatedAt = DateTime.Now;
                        cash.PaidAmount = totalPaid;
                        cash.ShopId = ShopId;
                        cash.Status = (int)Const.Status.NORMAL;
                        cash.PaidTargetId = exim.TargetId;
                        cash.PaidTargetType = "VENDOR";
                        cash.TargetId = exim.EximId;
                        cash.TargetType = "IMPORT";
                        cash.Title = "Phiếu thu hủy phiếu nhập " + exim.Code;
                        cash.Remain = 0;
                        cash.Total = totalPaid;
                        cash.PaymentMethod = (int)Const.PaymentType.CASH;
                        cash.UpdatedAt = DateTime.Now;
                        cash.EmployeeId = cancelImport.EmployeeId;
                        db.Cashes.Add(cash);
                        db.SaveChanges();
                        //craete cash flow
                        //temp >> return paid
                        CashFlow cashFlow = new CashFlow();
                        cashFlow.ShopId = ShopId;
                        cashFlow.TargetId = cash.CashId;
                        cashFlow.TargetType = "CASH";
                        cashFlow.PaidTargetId = cash.PaidTargetId;
                        cashFlow.PaidTargetType = cash.PaidTargetType;
                        cashFlow.Type = (int)Const.CashFlowType.CANCELED;
                        cashFlow.Amount = 0 - totalRemain;
                        //get lastest debt
                        //lastest Debt = sum (amout)
                        var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                        cashFlow.Debt = debt - totalRemain;
                        cashFlow.CreatedAt = DateTime.Now;
                        db.CashFlows.Add(cashFlow);
                        db.SaveChanges();
                    }
                    //- debt

                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    //import success >> use return
                    // only - stock
                    // create export
                    exim.Status = (int)Const.EximStatus.CANCELED;
                    db.Entry(exim).State = EntityState.Modified;
                    Exim export = new Exim();
                    export.BranchId = exim.BranchId;
                    export.Code = "PXH" + exim.Code;
                    export.CreatetAt = DateTime.Now;
                    export.CustomDate = DateTime.Now;
                    export.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
                    export.Discount = 0;
                    export.EmployeeId = cancelImport.EmployeeId;
                    export.Fee = 0;
                    export.PaymentStatus = (int)Const.PaymentStatus.COMPLETED;
                    export.Price = exim.Price;
                    export.ShopId = ShopId;
                    export.Status = (int)Const.EximStatus.COMPLETED;
                    export.TargetId = exim.TargetId;
                    export.TargetType = exim.TargetType;
                    export.Title = "Phiếu xuất hủy phiếu nhập " + exim.Code;
                    export.TotalPrice = exim.TotalPrice;
                    export.Type = (int)Const.EximType.CANCELED;
                    export.UpdatedAt = DateTime.Now;
                    db.Exims.Add(export);
                    //create cash flow
                    CashFlow cashFlow = new CashFlow();
                    cashFlow.ShopId = ShopId;
                    cashFlow.TargetId = export.EximId;
                    cashFlow.TargetType = "EXPORT";
                    cashFlow.PaidTargetId = export.TargetId;
                    cashFlow.PaidTargetType = export.TargetType;
                    cashFlow.Type = (int)Const.CashFlowType.CANCELED;
                    cashFlow.Amount = export.TotalPrice;
                    //get lastest debt
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == export.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == export.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                    //var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                    cashFlow.Debt = debt + cashFlow.Amount;
                    cashFlow.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlow);
                    db.SaveChanges();
                    //end create export
                    //reverse import
                    List<EximProduct> eximProducts = db.EximProducts.Where(ep => ep.EximId == exim.EximId).ToList();
                    if (eximProducts != null && exim.EximProducts.Count > 0)
                    {
                        for (int i = 0; i < eximProducts.Count; i++)
                        {
                            //find stock
                            var paid = eximProducts[i].ProductAttributeId;
                            BranchStock branchStock = db.BranchStocks.Where(bs => bs.ProductAttributeId == paid && bs.BranchId == exim.BranchId).FirstOrDefault();
                            int stock = 0;
                            if (branchStock != null)
                            {
                                stock = (int)branchStock.Stock;
                                branchStock.Stock -= eximProducts[i].Quantity;
                                db.Entry(branchStock).State = EntityState.Modified;
                            }
                            //create exim product
                            EximProduct eximProduct = new EximProduct();
                            eximProduct.CreatedAt = DateTime.Now;
                            eximProduct.DiscountPerItem = 0;
                            eximProduct.EximId = export.EximId;
                            eximProduct.LastStock = stock - eximProducts[i].Quantity;
                            eximProduct.PreviousStock = stock;
                            eximProduct.Price = eximProducts[i].Price;
                            eximProduct.ProductAttributeId = eximProducts[i].ProductAttributeId;
                            eximProduct.ProductUnitId = eximProducts[i].ProductUnitId;
                            eximProduct.Quantity = eximProducts[i].Quantity;
                            eximProduct.UpdatedAt = DateTime.Now;
                            db.EximProducts.Add(eximProduct);
                        }
                        db.SaveChanges();
                    }
                    //find cash
                    List<Cash> cashes = db.Cashes.Where(c => c.TargetId == exim.EximId && c.TargetType == "EXIM").ToList();
                    if (cashes != null && cashes.Count > 0)
                    {
                        decimal totalPaid = 0;
                        for (int i = 0; i < cashes.Count; i++)
                        {
                            totalPaid += (decimal)cashes[i].PaidAmount;
                        }
                        //create cash in
                        Cash cash = new Cash();
                        cash.CashCode = "PT" + exim.Code; //generateCashCode((int)Const.CashType.INCOME);
                        cash.Type = (int)Const.CashType.INCOME;                
                        cash.CreatedAt = DateTime.Now;
                        cash.PaidAmount = totalPaid;
                        cash.ShopId = ShopId;
                        cash.Status = (int)Const.Status.NORMAL;
                        cash.PaidTargetId = exim.TargetId;
                        cash.PaidTargetType = "VENDOR";
                        cash.TargetId = export.EximId;
                        cash.TargetType = "EXPORT";
                        cash.Title = "Phiếu thu hủy phiếu nhập " + exim.Code;
                        cash.Remain = 0;
                        cash.Total = totalPaid;
                        cash.PaymentMethod = (int)Const.PaymentType.CASH;
                        cash.UpdatedAt = DateTime.Now;
                        cash.EmployeeId = cancelImport.EmployeeId;
                        db.Cashes.Add(cash);
                        db.SaveChanges();
                        //create cash flow
                        CashFlow cashFlowOut = new CashFlow();
                        cashFlowOut.ShopId = ShopId;
                        cashFlowOut.TargetId = cash.CashId;
                        cashFlowOut.TargetType = "CASH";
                        cashFlowOut.PaidTargetId = export.TargetId;
                        cashFlowOut.PaidTargetType = export.TargetType;
                        cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                        cashFlowOut.Amount = 0 + export.TotalPrice;
                        //get lastest debt
                        //lastest Debt = sum (amout)
                        debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                        cashFlowOut.Debt = debt + cashFlowOut.Amount;
                        cashFlowOut.CreatedAt = DateTime.Now;
                        db.CashFlows.Add(cashFlowOut);
                        db.SaveChanges();
                    }
                    //update ex
                    //ok
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Import not found");
                return Ok(def);
            }
        }
        //end cancel import

        //complete import
        [HttpPost]
        [Route("api/shops/{id}/imports/{importid}/complete")]
        public IHttpActionResult completeImport(int id, int importid, CompleteImport completeImport)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //create order
            //check if product empty
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }

            //check paramater validity
            if (completeImport.EmployeeId == null || completeImport.EximId == null)
            {
                def.meta = new Meta(211, "Invalid input parameter - invalid input");
                return Ok(def);
            }
            //find exim
            Exim exim = db.Exims.Find(completeImport.EximId);
            if (exim != null)
            {
                //ok
                if (exim.Status == (int)Const.EximStatus.TEMP)
                {
                    //complete temp
                    exim.Status = (int)Const.EximStatus.COMPLETED;
                    exim.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
                    exim.UpdatedAt = DateTime.Now;
                    db.Entry(exim).State = EntityState.Modified;
                    db.SaveChanges();
                    //import to stock
                    //create cash flow import
                    CashFlow cashFlowOut = new CashFlow();
                    cashFlowOut.ShopId = ShopId;
                    cashFlowOut.TargetId = exim.EximId;
                    cashFlowOut.TargetType = "IMPORT";
                    cashFlowOut.PaidTargetId = exim.TargetId;
                    cashFlowOut.PaidTargetType = exim.TargetType;
                    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    cashFlowOut.Amount = 0 - exim.TotalPrice;
                    //get lastest debt
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == exim.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Sum(cf => cf.Amount).Value : 0;
                    cashFlowOut.Debt = debt + cashFlowOut.Amount;
                    cashFlowOut.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlowOut);
                    db.SaveChanges();
                    List<EximProduct> products = db.EximProducts.Where(ep => ep.EximId == exim.EximId).ToList();
                    if (products != null || products.Count > 0)
                    {
                        for (int i = 0; i < products.Count; i++)
                        {
                            EximProduct eximProduct = products[i];
                            int stock = 0;
                            BranchStock bs = db.BranchStocks.Where(b => b.BranchId == exim.BranchId && b.ProductAttributeId == eximProduct.ProductAttributeId).FirstOrDefault();
                            if (bs != null)
                            {
                                stock = (int)bs.Stock;
                                bs.Stock += eximProduct.Quantity;
                                db.SaveChanges();
                            }
                            eximProduct.PreviousStock = stock;
                            eximProduct.LastStock = stock + eximProduct.Quantity;
                            db.Entry(eximProduct).State = EntityState.Modified;
                        }
                        //complete delivery
                        exim.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
                        db.SaveChanges();
                        def.meta = new Meta(200, "Success");
                        return Ok(def);
                    }
                    else
                    {
                        //invalid
                        def.meta = new Meta(212, "Invalid products");
                        return Ok(def);
                    }
                }
                else
                {
                    //not temp
                    //return
                    def.meta = new Meta(213, "Cannot complete - not temp");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(404, "Import not found");
                return Ok(def);
            }
        }
        //end complete import

        //cancel Import
        [HttpPost]
        [Route("api/shops/{id}/imports/return")]
        public IHttpActionResult returnImport(int id, ReturnImport returnImport)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            //create order
            //check if product empty
            DefaultResponse def = new DefaultResponse();
            if (ShopId != id)
            {
                def.meta = new Meta(400, "Invalid request");
                return Ok(def);
            }
            //type == 10 return
            
            //check paramater validity
            if (returnImport.EmployeeId == null || returnImport.EximId == null || returnImport.BranchId == null || returnImport.Paid == null || returnImport.products == null || returnImport.products.Count == 0)
            {
                def.meta = new Meta(211, "Invalid input parameter - invalid input");
                return Ok(def);
            }
            //check product stock
            bool isEnoughStock = true;
            for (int i = 0; i < returnImport.products.Count; i++)
            {
                int paid = (int)returnImport.products[i].ProductAttributeId;                
                BranchStock bs = db.BranchStocks.Where(b => b.BranchId == returnImport.BranchId && b.ProductAttributeId == paid).FirstOrDefault();                
                if (bs != null)
                {
                    if (bs.Stock < returnImport.products[i].Quantity)
                    {
                        isEnoughStock = false;
                        break;
                    }
                }
                else
                {
                    isEnoughStock = false;
                    break;
                }
            }
            if (!isEnoughStock)
            {
                def.meta = new Meta(212, "Invalid input parameter - invalid product number");
                return Ok(def);
            }
            decimal totalPrice = 0;   
            for (int i = 0; i < returnImport.products.Count; i++)
            {
                totalPrice += returnImport.products[i].ReturnPrice * returnImport.products[i].Quantity;
            }

            //valid ok
            Exim import = db.Exims.Find(returnImport.EximId);
            if (import != null)
            {
                if (import.Status == (int)Const.EximStatus.TEMP)
                {
                    def.meta = new Meta(213, "Cannot return temp import");
                    return Ok(def);
                }
                //create thn
                Exim exim = new Exim();
                exim.BranchId = returnImport.BranchId;
                exim.Code = generateReturnImportCode();
                exim.CreatetAt = DateTime.Now;
                exim.CustomDate = DateTime.Now;
                exim.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
                exim.Discount = 0;
                exim.EmployeeId = returnImport.EmployeeId;
                exim.Fee = returnImport.fee;
                if (returnImport.Paid >= totalPrice)
                    exim.PaymentStatus = (int)Const.PaymentStatus.COMPLETED;
                else
                    exim.PaymentStatus = (int)Const.PaymentStatus.PAID_NOT_ENOUGH;
                exim.Price = totalPrice;
                exim.RelatedEximId = import.EximId;
                exim.ShopId = ShopId;
                exim.Status = (int)Const.EximStatus.COMPLETED;
                exim.TargetId = import.TargetId;
                exim.TargetType = import.TargetType;
                exim.Title = "Trả hàng nhập cho nhà cung cấp " + returnImport.VendorName;
                exim.TotalPrice = totalPrice - returnImport.fee;
                exim.Type = (int)Const.EximType.EXPORT;
                exim.UpdatedAt = DateTime.Now;
                db.Exims.Add(exim);
                db.SaveChanges();
                //create exim product
                for (int i = 0; i < returnImport.products.Count; i++)
                {
                    EximProduct ep = new EximProduct();
                    int productAttributeId = returnImport.products[i].ProductAttributeId;
                    //find base unit
                    ProductAttribute productAttribute = db.ProductAttributes.Find(productAttributeId);
                    int ProductUnitId = -1;
                    if (productAttribute != null)
                    {
                        var productUnit = db.ProductUnits.Where(pu => pu.ProductId == productAttribute.Product.ProductId && pu.Quantity == 1).FirstOrDefault();
                        if (productUnit != null)
                            ProductUnitId = productUnit.ProductUnitId;
                    }
                    //end find base unit
                    //find current stock
                    var stocks = db.BranchStocks.Where(b => b.ProductAttributeId == productAttributeId).ToList();
                    int stock = 0;
                    BranchStock bs = db.BranchStocks.Where(b => b.BranchId == returnImport.BranchId && b.ProductAttributeId == productAttributeId).FirstOrDefault();
                    if (bs != null)
                    {
                        stock = (int)bs.Stock;
                        bs.Stock -= (int)returnImport.products[i].Quantity;
                        db.SaveChanges();
                    }
                    //end find current stock
                    ep.CreatedAt = DateTime.Now;
                    ep.EximId = exim.EximId;
                    ep.DiscountPerItem = 0;
                    ep.Price = returnImport.products[i].ReturnPrice;
                    ep.PreviousStock = stock;
                    ep.ProductAttributeId = productAttributeId;
                    ep.Quantity = returnImport.products[i].Quantity;
                    ep.ProductUnitId = ProductUnitId;
                    ep.LastStock = stock - ep.Quantity;
                    ep.UpdatedAt = DateTime.Now;
                    db.EximProducts.Add(ep);
                    db.SaveChanges();
                }

                if (returnImport.VendorCash > 0)
                {
                    decimal eximPaid = returnImport.VendorCash >= returnImport.TotalPrice ? ((decimal)returnImport.TotalPrice - (decimal)returnImport.fee) : (decimal)returnImport.VendorCash;
                    Cash cash = new Cash();
                    cash.CashCode = "PC" + exim.Code;
                    cash.Type = (int)Const.CashType.INCOME;
                    //select cash type                               
                    cash.CreatedAt = DateTime.Now;
                    cash.PaidAmount = eximPaid;
                    cash.ShopId = ShopId;
                    cash.Status = (int)Const.Status.NORMAL;
                    cash.PaidTargetId = exim.TargetId;
                    cash.PaidTargetType = "VENDOR";
                    cash.TargetId = returnImport.EximId;
                    cash.TargetType = "IMPORT";
                    cash.Title = "Phiếu chi nhập hàng cho nhà cung cấp phiếu nhập " + import.Code;
                    cash.Total = eximPaid;
                    cash.PaymentMethod = returnImport.PaymentMethod;
                    cash.Remain = 0;
                    cash.UpdatedAt = DateTime.Now;
                    cash.EmployeeId = returnImport.EmployeeId;
                    cash.IsManual = false;
                    db.Cashes.Add(cash);
                    db.SaveChanges();
                    //create cash flow

                    CashFlow cashFlowOut = new CashFlow();
                    cashFlowOut.ShopId = ShopId;
                    cashFlowOut.TargetId = cash.CashId;
                    cashFlowOut.TargetType = "CASH";
                    cashFlowOut.PaidTargetId = exim.TargetId;
                    cashFlowOut.PaidTargetType = "VENDOR";
                    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    cashFlowOut.Amount = 0 - eximPaid;
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == exim.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == exim.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                    cashFlowOut.Debt = debt + cashFlowOut.Amount;
                    cashFlowOut.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlowOut);
                    db.SaveChanges();
                }

                //create cash flow
                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = ShopId;
                cashFlow.TargetId = exim.EximId;
                cashFlow.TargetType = "IMPORT_RETURN";
                cashFlow.PaidTargetId = import.TargetId;
                cashFlow.PaidTargetType = "VENDOR";
                cashFlow.Type = (int)Const.CashFlowType.IMPORT_RETURN;
                cashFlow.Amount = 0 - (returnImport.TotalPrice - returnImport.fee);
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt2 = db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                cashFlow.Debt = debt2 + cashFlow.Amount;
                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();
                //cash
                //if (returnImport.Paid > 0)
                //{
                //create cash in
                Cash cashIn = new Cash();
                cashIn.CashCode = "PT" + exim.Code;
                cashIn.CreatedAt = DateTime.Now;
                cashIn.EmployeeId = returnImport.EmployeeId;
                cashIn.IsManual = false;
                cashIn.PaidAmount = returnImport.TotalPrice - returnImport.fee - returnImport.TotalPaid + returnImport.Paid;
                cashIn.PaidTargetId = exim.TargetId;
                cashIn.PaidTargetType = exim.TargetType;
                cashIn.PaymentMethod = returnImport.PaymentMethod;
                cashIn.Remain = 0;
                cashIn.ShopId = ShopId;
                cashIn.Status = (int)Const.PaymentStatus.COMPLETED;
                cashIn.TargetId = exim.EximId;
                cashIn.TargetType = "IMPORT_RETURN";
                cashIn.Title = "Phiếu thu trả hàng nhập " + exim.Code;
                cashIn.Total = returnImport.TotalPrice - returnImport.fee - returnImport.TotalPaid + returnImport.Paid;
                cashIn.Type = (int)Const.CashType.INCOME;
                cashIn.UpdatedAt = DateTime.Now;
                cashIn.IsManual = false;
                db.Cashes.Add(cashIn);
                db.SaveChanges();
                //create cashFlow
                if ((returnImport.TotalPrice - returnImport.TotalPaid + returnImport.Paid - returnImport.fee) > 0)
                {
                    CashFlow cashFlowIn = new CashFlow();
                    cashFlowIn.ShopId = ShopId;
                    cashFlowIn.TargetId = cashIn.CashId;
                    cashFlowIn.TargetType = "CASH";
                    cashFlowIn.PaidTargetId = exim.TargetId;
                    cashFlowIn.PaidTargetType = exim.TargetType;
                    cashFlowIn.Type = (int)Const.CashFlowType.PAID;
                    cashFlowIn.Amount = returnImport.TotalPrice - returnImport.fee - returnImport.TotalPaid + returnImport.Paid;
                    //get lastest debt
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == import.TargetId && cf.PaidTargetType == "VENDOR").Sum(cf => cf.Amount).Value : 0;
                    cashFlowIn.Debt = debt + cashFlowIn.Amount;
                    cashFlowIn.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlowIn);
                    db.SaveChanges();
                }
                //create note
                if (returnImport.Note != null && returnImport.Note.Trim() != "")
                {
                    Note note = new Note();
                    note.Content = returnImport.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = (int)Const.Status.NORMAL;
                    note.TargetId = exim.EximId;
                    note.TargetType = "IMPORT_RETURN";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
                //create action
                //create action
                Action action = new Action();
                action.ActionName = "trả hàng nhập";
                action.ActionType = "IMPORT_RETURN";
                action.CreatedAt = DateTime.Now;
                action.EmployeeId = returnImport.EmployeeId;
                action.Result = 0;
                action.ShopId = ShopId;
                action.TargetId = exim.EximId;
                action.TargetType = "IMPORT_RETURN";
                db.Actions.Add(action);
                db.SaveChanges();
                //ok doen
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(404, "Import not found");
                return Ok(def);
            }
        }
        //end cancel import

        [HttpGet]
        [Route("api/shops/{id}/manufactures")]
        public IHttpActionResult getManufactures(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from m in db.Manufactures
                          where m.ShopId == ShopId
                          orderby m.ManufactureId descending
                          select new SaleManager.Data.Manufacture
                          {
                              ManufactureId = m.ManufactureId,
                              Name = m.Name,
                              Origin = m.Origin,
                              ShopId = m.ShopId
                          }
                         ).AsEnumerable();
            int count = result.Count();
            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
                count = result.Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.data = result;
            def.metadata = new Metadata(count);
            return Ok(def);
        }

        //attribute
        [HttpGet]
        [Route("api/shops/{id}/attributes")]
        public IHttpActionResult getAttributes(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from a in db.Attributes
                          where a.ShopId == ShopId || a.ShopId == -1
                          orderby a.AttributeId descending
                          select new SaleManager.Models.ShopAttribute
                          {
                              AttributeId = a.AttributeId,
                              AttributeName = a.AttributeName
                          }
                         ).AsEnumerable();
            if (paging.query != null)
                result = result.Where(HttpUtility.UrlDecode(paging.query)).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/employees")]
        public IHttpActionResult getEmployees(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from e in db.Employees
                          where e.ShopId == ShopId
                          orderby e.EmployeeId descending
                          select new SaleManager.Data.Employee
                          {
                              EmployeeId = e.EmployeeId,
                              EmployeeName = e.EmployeeName
                          }
                         ).AsEnumerable();
            int count = result.Count();
            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
                count = result.Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.data = result;
            def.metadata = new Metadata(count);
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/blocklists")]
        public IHttpActionResult getBlocklists(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from e in db.BlockLists
                          where e.ShopId == ShopId && e.Status == 0                         
                          select new BlockListDTO
                          {
                              BlockListId = e.BlockListId,
                              CreatedAt = e.CreatedAt,
                              CustomerId = e.CustomerId,
                              FacebookUserId = e.FacebookUserId
                          }
                         ).AsEnumerable();
            int count = result.Count();
            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
                count = result.Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            }
            else
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.data = result;
            def.metadata = new Metadata(count);
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/cashes")]
        public IHttpActionResult getCashes(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Cashes                         
                          join e in db.Employees on c.EmployeeId equals e.EmployeeId
                          where c.ShopId == ShopId && c.Status != (int)Const.Status.DELETED
                          orderby c.CreatedAt descending
                          select new SaleManager.Data.Cash
                          {
                              CashCode = c.CashCode,
                              CashId = c.CashId,
                              CreatedAt = c.CreatedAt,
                              Note = c.Note,
                              PaidAmount = c.PaidAmount,
                              PaymentMethod = c.PaymentMethod,
                              Title = c.Title,
                              Total = c.Total,
                              UpdatedAt = (DateTime)c.UpdatedAt,
                              TargetId = c.TargetId,
                              TargetType = c.TargetType,
                              PaidTargetId = c.PaidTargetId,
                              PaidTargetType = c.PaidTargetType,          
                              CashTypeId = c.CashTypeId,
                              Type = c.Type,
                              IsManual = c.IsManual,
                              employee = new Data.Employee()
                              {
                                  EmployeeId = e.EmployeeId,
                                  EmployeeName = e.EmployeeName
                              }                             
                          }
                         ).AsEnumerable();

            if (paging.query != null)
            {
                result = result.Where(HttpUtility.UrlDecode(paging.query));
            }

            int count = result.Count();
            //sum total
            var total = result.Sum(c => c.Total);
            var totalPaid = result.Sum(c => c.PaidAmount);

            var totalCashesIn = result.Where(e => e.Type == 1).Sum(c => c.Total);
            var totalCashesOut = result.Where(e => e.Type == 0).Sum(c => c.Total);

            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

            var returnData = result.ToList();
            //select target
            for (int i = 0; i < returnData.Count; i++)
            {
                int paidTargetId = (int)returnData[i].PaidTargetId;
                switch (returnData[i].PaidTargetType)
                {
                    case "CUSTOMER":
                        returnData[i].paidTarget = db.Customers.Where(c => c.CustomerId == paidTargetId).Select(c => new PaidTarget()
                        {
                            PaidTargetId = c.CustomerId,
                            TargetName = c.CustomerName,
                            ProvinceId = c.ProvinceId,
                            TargetCode = c.CustomerCode
                        }).FirstOrDefault();
                        break;
                    case "VENDOR":
                        returnData[i].paidTarget = db.Vendors.Where(c => c.VendorId == paidTargetId).Select(c => new PaidTarget()
                        {
                            PaidTargetId = c.VendorId,
                            TargetName = c.VendorName,
                            ProvinceId = c.ProvinceId,
                            TargetCode = c.VendorCode
                        }).FirstOrDefault();
                        break;
                    case "DELIVERY_VENDOR":
                        returnData[i].paidTarget = db.DeliveryVendors.Where(c => c.DeliveryVendorId == paidTargetId).Select(c => new PaidTarget()
                        {
                            PaidTargetId = c.DeliveryVendorId,
                            TargetName = c.VendorName,
                            ProvinceId = c.ProvinceId,
                            TargetCode = c.VendorCode
                        }).FirstOrDefault();
                        break;
                    default:
                        returnData[i].target = null;
                        break;
                }

                var targetId = returnData[i].TargetId;
                if (targetId != null)
                {
                    switch (returnData[i].TargetType)
                    {
                        case "EXIM":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId).Select(c => new Target()
                            {
                                TargetId = c.EximId,
                                TargetName = c.Title,
                                TargetCode = c.Code,
                                Type = c.Type
                            }).FirstOrDefault();
                            break;
                        case "ORDER":
                            returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new Target()
                            {
                                TargetId = c.OrderId,
                                TargetName = "Hóa đơn mã " + c.OrderCode,
                                TargetCode = c.OrderCode
                            }).FirstOrDefault();
                            break;
                        default:
                            returnData[i].target = null;
                            break;
                    }
                }

                //cashs type
                var cashTypeId = returnData[i].CashTypeId;
                if (cashTypeId != null)
                {
                    returnData[i].cashType = db.CashTypes.Where(c => c.CashTypeId == cashTypeId).Select(c => new Data.CashType()
                    {
                        CashTypeId = c.CashTypeId,
                        DefaultValue = c.DefaultValue,
                        Name = c.Name,
                        Type = c.Type
                    }).FirstOrDefault();
                }
            }

            def.meta = new Meta(200, "Success");
            def.data = returnData;
            //def.metadata = new MetadataCash(count);
            MetadataCash meta = new MetadataCash();
            meta.item_count = count;
            meta.Total = total.HasValue ? total.Value : 0;
            meta.TotalPaid = totalPaid.HasValue ? totalPaid.Value : 0;
            meta.TotalCachesIn = totalCashesIn.HasValue ? totalCashesIn.Value : 0;
            meta.TotalCachesOut = totalCashesOut.HasValue ? totalCashesOut.Value : 0;
            def.metadata = meta;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/exims")]
        public IHttpActionResult GetExims(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from e in db.Exims
                              join em in db.Employees on e.EmployeeId equals em.EmployeeId
                              join b in db.Branches on e.BranchId equals b.BranchId
                              join vd in db.Vendors on e.TargetId equals vd.VendorId
                              where e.ShopId == id && e.Status != (int)Const.EximStatus.DELETED && e.Status != (int)Const.EximStatus.CANCELED
                              && e.Type == (int)Const.EximType.IMPORT && e.TargetType == "VENDOR"
                              orderby e.CreatetAt descending
                              select new SaleManager.Data.Exim
                              {
                                  Code = e.Code,
                                  CreatetAt = e.CreatetAt,
                                  DeliveryStatus = e.DeliveryStatus,
                                  employee = new Data.Employee()
                                  {
                                      EmployeeId = em.EmployeeId,
                                      EmployeeName = em.EmployeeName
                                  },
                                  EximId = e.EximId,
                                  PaymentStatus = e.PaymentStatus,
                                  Price = e.Price,
                                  CustomerPaid = 0,
                                  Discount = e.Discount,
                                  Fee = e.Fee,
                                  Status = e.Status,
                                  TargetId = e.TargetId,
                                  TargetType = e.TargetType,
                                  Title = e.Title,
                                  TotalPrice = e.TotalPrice,
                                  Type = e.Type,
                                  UpdatedAt = e.UpdatedAt,
                                  target = new Data.Target()
                                  {
                                      TargetCode = vd.VendorCode,
                                      TargetId = vd.VendorId,
                                      TargetName = vd.VendorName,
                                      SearchQuery = vd.SearchQuery
                                  },
                                  branch = new Data.Branch()
                                  {
                                      Address = b.Address,
                                      BranchId = b.BranchId,
                                      BranchName = b.BranchName,
                                      Phone = b.Phone
                                  }
                              }).AsEnumerable();

                int count = result.Count();
                if (paging.query != null)
                {
                    result = result.Where(HttpUtility.UrlDecode(paging.query));
                    count = result.Count();
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

                var returnData = result.ToList();

                for (int i = 0; i < returnData.Count; i++)
                {
                    int paidTargetId = (int)returnData[i].TargetId;
                    int EximId = (int)returnData[i].EximId;

                    returnData[i].CustomerPaid = db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == paidTargetId).Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == paidTargetId).Select(e => e.PaidAmount).Sum().Value : 0;
                    returnData[i].TotalReturnProduct = db.EximProducts.Where(c => c.Exim.RelatedEximId == EximId).Select(e => e.Quantity).Sum().HasValue ? db.EximProducts.Where(c => c.Exim.RelatedEximId == EximId).Select(e => e.Quantity).Sum().Value : 0;
                    //returnData[i].ReturnPaid = db.Exims.Where(c => c.RelatedEximId == EximId).Select(e => e.Price).Sum().HasValue ? db.Exims.Where(c => c.RelatedEximId == EximId).Select(e => e.Price).Sum().Value : 0;
                    var returnpaid = from ex in db.Exims
                                     join ca in db.Cashes on ex.EximId equals ca.TargetId
                                     where ex.RelatedEximId == EximId && ex.Status != (int)Const.EximStatus.DELETED
                                     group ca by new
                                     {
                                         ca.TargetId
                                     } into gca
                                     select new
                                     {
                                         TargetId = gca.Key,
                                         Remain = gca.FirstOrDefault().Remain,
                                     };

                    returnData[i].ReturnPaid = returnpaid.Sum(e => e.Remain);
                }

                def.meta = new Meta(200, "Success");
                def.data = returnData;
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/shops/{id}/orders")]
        public IHttpActionResult GetOrders(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from o in db.Orders
                              where o.ShopId == id && o.Type == (int)Const.OrderType.SALE && o.OrderStatus != (int)Const.OrderStatus.DELETED && o.OrderStatus != (int)Const.OrderStatus.CANCELED
                              orderby o.CreatedAt descending
                              select new SaleManager.Data.Order
                              {
                                  CreatedAt = o.CreatedAt,
                                  CustomerPaid = o.CustomerPaid,
                                  DeliveryStatus = o.DeliveryStatus,
                                  Discount = o.DiscountPrice,
                                  Note = o.Note,
                                  OrderAddress = o.OrderAddress,
                                  OrderCode = o.OrderCode,
                                  OrderId = o.OrderId,
                                  OrderPhone = o.OrderPhone,
                                  PaymentStatus = o.PaymentStatus,
                                  OrderStatus = o.OrderStatus,
                                  TotalPrice = o.TotalPrice,
                                  OrderPrice = o.OrderPrice,
                                  DeliveryPrice = o.DeliveryPrice,
                                  MoneyCollect = o.MoneyCollect,
                                  branch = new SaleManager.Data.Branch()
                                  {
                                      BranchId = o.BranchId,
                                      BranchName = o.Branch.BranchName
                                  },
                                  customer = new SaleManager.Data.Customer()
                                  {
                                      CustomerCode = o.Customer.CustomerCode,
                                      CustomerId = o.Customer.CustomerId,
                                      CustomerName = o.Customer.CustomerName,
                                      FacebookUserId = o.Customer.FacebookUserId
                                  },
                                  employee = new SaleManager.Data.Employee()
                                  {
                                      EmployeeId = o.Employee.EmployeeId,
                                      EmployeeName = o.Employee.EmployeeName
                                  },
                                  saleEmployee = new SaleManager.Data.Employee()
                                  {
                                      EmployeeId = o.SaleEmployee.EmployeeId,
                                      EmployeeName = o.SaleEmployee.EmployeeName
                                  },
                                  ProductCount = o.OrderProducts.Count()
                              }).AsEnumerable();

                

                if (paging.query != null)
                {
                    result = result.Where(HttpUtility.UrlDecode(paging.query));
                }

                int count = result.Count();
                decimal totalprice = result.Sum(e => e.OrderPrice).Value;
                decimal totalpaidprice = result.Sum(e => e.CustomerPaid).Value;
                decimal totaldiscountprice = result.Sum(e => e.Discount).Value;
                decimal totaldeliveryprice = result.Sum(e => e.DeliveryPrice).Value;
                int totaldeliverydompleted = result.Where(e => e.DeliveryStatus == 2).Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

                var returnData = result.ToList();
                for (int i = 0; i < returnData.Count; i++)
                {
                    var orderId = returnData[i].OrderId;
                    //returnData[i].ReturnPaid = db.Orders.Where(e => e.RelatedOrderId == orderId).Select(e => e.OrderPrice).Sum().HasValue ? db.Orders.Where(e => e.RelatedOrderId == orderId).Select(e => e.OrderPrice).Sum().Value : 0;
                    returnData[i].CashesPaid = db.Cashes.Where(e => e.TargetId == orderId && e.TargetType == "ORDER").Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(e => e.TargetId == orderId && e.TargetType == "ORDER").Select(e => e.PaidAmount).Sum().Value : returnData[i].CustomerPaid;

                    var deliveryVendorId = returnData[i].DeliveryVendorId;
                    if (deliveryVendorId != null)
                    {
                        returnData[i].deliveryVendor = db.DeliveryVendors.Where(d => d.DeliveryVendorId == deliveryVendorId).Select(d => new SaleManager.Data.DeliveryVendor()
                        {
                            DeliveryVendorId = d.DeliveryVendorId,
                            VendorCode = d.VendorCode,
                            VendorName = d.VendorName
                        }).FirstOrDefault();
                    } 
                }

                def.meta = new Meta(200, "Success");
                def.data = returnData;
                def.metadata = new MetadataTotal(count, totalprice, totaldiscountprice, totaldeliveryprice, totalpaidprice, totaldeliverydompleted);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/shops/{id}/returns")]
        public IHttpActionResult GetReturns(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from o in db.Orders
                              where o.ShopId == id && o.Type == (int)Const.OrderType.RETURN
                              orderby o.CreatedAt descending
                              select new SaleManager.Data.Return
                              {
                                  ReturnId = o.OrderId,
                                  Code = o.OrderCode,
                                  Price = o.OrderPrice,
                                  Fee = o.Fee,
                                  Status = o.OrderStatus,
                                  CreatedAt = o.CreatedAt,
                                  Note = o.Note,
                                  TotalPrice = o.TotalPrice,
                                  Paid = o.Paid,
                                  branch = new SaleManager.Data.Branch()
                                  {
                                      BranchId = o.BranchId,
                                      BranchName = o.Branch.BranchName
                                  },
                                  customer = new SaleManager.Data.Customer()
                                  {
                                      CustomerCode = o.Customer.CustomerCode,
                                      CustomerId = o.Customer.CustomerId,
                                      CustomerName = o.Customer.CustomerName,
                                      FacebookUserId = o.Customer.FacebookUserId
                                  },
                                  employee = new SaleManager.Data.Employee()
                                  {
                                      EmployeeId = o.Employee.EmployeeId,
                                      EmployeeName = o.Employee.EmployeeName
                                  },
                                  ProductCount = o.OrderProducts.Count()
                              }).AsEnumerable();
                int count = result.Count();
                if (paging.query != null)
                {
                    result = result.Where(HttpUtility.UrlDecode(paging.query));
                    count = result.Count();
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);

                var returnData = result.ToList();
                for (int i = 0; i < returnData.Count; i++)
                {
                    var returnId = returnData[i].ReturnId;
                    returnData[i].CashesPaid = db.Cashes.Where(e => e.TargetId == returnId && e.TargetType == "RETURN").Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(e => e.TargetId == returnId && e.TargetType == "RETURN").Select(e => e.PaidAmount).Sum().Value : returnData[i].Paid;
                }
                def.meta = new Meta(200, "Success");
                def.data = returnData;
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/shops/{id}/importreturns")]
        public IHttpActionResult GetImportReturns(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from o in db.Exims
                              where o.ShopId == id && o.Type == (int)Const.EximType.EXPORT && o.TargetType == "VENDOR"
                              orderby o.CreatetAt descending
                              select new SaleManager.Data.ImportReturn
                              {
                                  EximId = o.EximId,
                                  Code = o.Code,                                  
                                  Status = o.Status,
                                  CreatedAt = o.CreatetAt,                              
                                  TotalPrice = o.Price,
                                  Fee=o.Fee,
                                  branch = new SaleManager.Data.Branch()
                                  {
                                      BranchId = o.BranchId,
                                      BranchName = o.Branch.BranchName
                                  },                                  
                                  employee = new SaleManager.Data.Employee()
                                  {
                                      EmployeeId = o.Employee.EmployeeId,
                                      EmployeeName = o.Employee.EmployeeName
                                  },
                                  ProductCount = o.EximProducts.Count(),
                                  TargetId = o.TargetId                                  
                              }).AsEnumerable();
                int count = result.Count();
                if (paging.query != null)
                {
                    result = result.Where(HttpUtility.UrlDecode(paging.query));
                    count = result.Count();
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                var returnData = result.ToList();
                //target
                for (int i = 0; i < returnData.Count; i++)
                {
                    
                    var targetId = returnData[i].TargetId;
                    if (targetId != null)
                    {
                        returnData[i].vendor = db.Vendors.Where(d => d.VendorId == targetId).Select(d => new SaleManager.Data.Vendor()
                        {
                            VendorId = d.VendorId,
                            VendorCode = d.VendorCode,
                            VendorName = d.VendorName
                        }).FirstOrDefault();
                    }
                    //paid amout
                    var EximId = returnData[i].EximId;
                    returnData[i].PaidAmount = db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == targetId).Sum(c => c.PaidAmount).HasValue ? db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == targetId).Sum(c => c.PaidAmount).Value : 0;
                        
                }
                //
                def.meta = new Meta(200, "Success");
                def.data = returnData;
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/shops/{id}/employeedetail")]
        public IHttpActionResult getShopEmployees(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Employees
                          join b in db.Branches on c.BranchId equals b.BranchId
                          where c.ShopId == ShopId && c.Status != (int)Const.Status.DELETED
                          select new SaleManager.Data.EmployeeDetail
                          {
                              Address = c.Address,
                              Email = c.Email,
                              EmployeeId = c.EmployeeId,
                              EmployeeName = c.EmployeeName,
                              Group = c.Group,
                              Phone = c.Phone,
                              branch = new Data.Branch()
                              {
                                  BranchId = b.BranchId,
                                  BranchName = b.BranchName
                              },
                              Username = c.Email
                          }).AsEnumerable();
           
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/shops/{id}/actions")]
        public IHttpActionResult getActions(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            DefaultResponse def = new DefaultResponse();
            var result = (from a in db.Actions
                          join e in db.Employees on a.EmployeeId equals e.EmployeeId
                          where a.ShopId == ShopId
                          orderby a.CreatedAt descending
                          select new SaleManager.Data.Action
                          {
                              ActionId = a.ActionId,
                              ActionName = a.ActionName,
                              ActionType = a.ActionType,
                              CreatedAt = a.CreatedAt,
                              employee = new Data.Employee()
                              {
                                  EmployeeId = e.EmployeeId,
                                  EmployeeName = e.EmployeeName
                              },
                              TargetId = a.TargetId,
                              TargetType = a.TargetType
                          }).AsEnumerable();
            def.meta = new Meta(200, "Success");
            result = result.Skip(0).Take(5);
            var returnData = result.ToList();
            //select target
            for (int i = 0; i < returnData.Count; i++)
            {
                int targetId = (int)returnData[i].TargetId;
                switch (returnData[i].TargetType)
                {
                    case "CUSTOMER":
                        returnData[i].target = db.Customers.Where(c => c.CustomerId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.CustomerId,
                            TargetName = c.CustomerName,
                            TargetCode = c.CustomerCode
                        }).FirstOrDefault();
                        break;
                    case "VENDOR":
                        returnData[i].target = db.Vendors.Where(c => c.VendorId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.VendorId,
                            TargetName = c.VendorName,
                            TargetCode = c.VendorCode
                        }).FirstOrDefault();
                        break;
                    case "DELIVERY_VENDOR":
                        returnData[i].target = db.DeliveryVendors.Where(c => c.DeliveryVendorId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.DeliveryVendorId,
                            TargetName = c.VendorName,
                            TargetCode = c.VendorCode
                        }).FirstOrDefault();
                        break;
                    case "PRODUCT_ATTRIBUTE":
                        returnData[i].target = db.ProductAttributes.Where(c => c.ProductAttributeId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.ProductAttributeId,
                            TargetCode = c.ProductCode
                        }).FirstOrDefault();
                        break;
                    case "IMPORT":
                        returnData[i].target = db.Exims.Where(c => c.EximId == targetId && c.Type == 0).Select(c => new ActionTarget()
                        {
                            TargetId = c.EximId,
                            TargetCode = c.Code,
                            Price = c.Price
                        }).FirstOrDefault();
                        break;
                    case "EXPORT":
                        returnData[i].target = db.Exims.Where(c => c.EximId == targetId && c.Type == 1).Select(c => new ActionTarget()
                        {
                            TargetId = c.EximId,
                            TargetCode = c.Code,
                            Price = c.Price
                        }).FirstOrDefault();
                        break;
                    case "ORDER":
                        returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.OrderId,
                            TargetCode = c.OrderCode,
                            Price = c.TotalPrice
                        }).FirstOrDefault();
                        break;
                    case "RETURN":
                        returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.OrderId,
                            TargetCode = c.OrderCode,
                            Price = c.TotalPrice
                        }).FirstOrDefault();
                        break;
                    case "IMPORT_RETURN":
                        returnData[i].target = db.Exims.Where(c => c.EximId == targetId).Select(c => new ActionTarget()
                        {
                            TargetId = c.EximId,
                            TargetCode = c.Code,
                            Price = c.Price
                        }).FirstOrDefault();
                        break;
                    default:
                        returnData[i].target = null;
                        break;
                }
            }
            def.data = returnData;
            return Ok(def);
        }


        [HttpGet]
        [Route("api/shops/{id}/pageactions")]
        public IHttpActionResult getPageActions(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from a in db.Actions
                              join e in db.Employees on a.EmployeeId equals e.EmployeeId
                              where a.ShopId == ShopId
                              orderby a.CreatedAt descending
                              select new SaleManager.Data.Action
                              {
                                  ActionId = a.ActionId,
                                  ActionName = a.ActionName,
                                  ActionType = a.ActionType,
                                  CreatedAt = a.CreatedAt,
                                  employee = new Data.Employee()
                                  {
                                      EmployeeId = e.EmployeeId,
                                      EmployeeName = e.EmployeeName
                                  },
                                  TargetId = a.TargetId,
                                  TargetType = a.TargetType
                              }).AsEnumerable();
                def.meta = new Meta(200, "Success");
                int count = result.Count();
                if (paging.query != null)
                {
                    result = result.Where(HttpUtility.UrlDecode(paging.query));
                    count = result.Count();
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                var returnData = result.ToList();
                //select target
                for (int i = 0; i < returnData.Count; i++)
                {
                    int targetId = (int)returnData[i].TargetId;
                    switch (returnData[i].TargetType)
                    {
                        case "CUSTOMER":
                            returnData[i].target = db.Customers.Where(c => c.CustomerId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.CustomerId,
                                TargetName = c.CustomerName,
                                TargetCode = c.CustomerCode
                            }).FirstOrDefault();
                            break;
                        case "VENDOR":
                            returnData[i].target = db.Vendors.Where(c => c.VendorId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.VendorId,
                                TargetName = c.VendorName,
                                TargetCode = c.VendorCode
                            }).FirstOrDefault();
                            break;
                        case "DELIVERY_VENDOR":
                            returnData[i].target = db.DeliveryVendors.Where(c => c.DeliveryVendorId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.DeliveryVendorId,
                                TargetName = c.VendorName,
                                TargetCode = c.VendorCode
                            }).FirstOrDefault();
                            break;
                        case "PRODUCT_ATTRIBUTE":
                            returnData[i].target = db.ProductAttributes.Where(c => c.ProductAttributeId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.ProductAttributeId,
                                TargetCode = c.ProductCode
                            }).FirstOrDefault();
                            break;
                        case "IMPORT":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId && c.Type == 0).Select(c => new ActionTarget()
                            {
                                TargetId = c.EximId,
                                TargetCode = c.Code,
                                Price = c.Price
                            }).FirstOrDefault();
                            break;
                        case "EXPORT":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId && c.Type == 1).Select(c => new ActionTarget()
                            {
                                TargetId = c.EximId,
                                TargetCode = c.Code,
                                Price = c.Price
                            }).FirstOrDefault();
                            break;
                        case "ORDER":
                            returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.OrderId,
                                TargetCode = c.OrderCode,
                                Price = c.TotalPrice
                            }).FirstOrDefault();
                            break;
                        case "RETURN":
                            returnData[i].target = db.Orders.Where(c => c.OrderId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.OrderId,
                                TargetCode = c.OrderCode,
                                Price = c.TotalPrice
                            }).FirstOrDefault();
                            break;
                        case "IMPORT_RETURN":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId).Select(c => new ActionTarget()
                            {
                                TargetId = c.EximId,
                                TargetCode = c.Code,
                                Price = c.Price
                            }).FirstOrDefault();
                            break;
                        default:
                            returnData[i].target = null;
                            break;
                    }
                }
                def.metadata = new MetadataTotal(count, 0);
                def.data = returnData;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/shops/{id}/orders/{orderid}/return")]
        public IHttpActionResult returnOrder(int id, int orderid, ReturnOrder returnOrder)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            log.Info(JsonConvert.SerializeObject(returnOrder));
            //check product
            if (returnOrder.products == null || returnOrder.products.Count == 0)
            {
                def.meta = new Meta(211, "Invalid parameter");
                return Ok(def);
            }
            else
            {
                //check product
                bool isInvalid = false;
                int count = 0;
                for (int i = 0; i < returnOrder.products.Count; i++)
                {
                    if (returnOrder.products[i].Quantity <= 0)
                        count += 1;
                }
                if (count == returnOrder.products.Count)
                    isInvalid = true;
                if (isInvalid)
                {
                    def.meta = new Meta(212, "Invalid parameter");
                    return Ok(def);
                }
            }

            decimal orderOriginPrice = 0;
            Order order = db.Orders.Find(returnOrder.OrderId);
            if (order != null)
            {
                //create return
                Order _return = new Order();//Return _return = new Return();
                _return.OrderCode = generateReturnCode();
                _return.CreatedAt = DateTime.Now;
                _return.CustomerId = order.CustomerId;
                _return.EmployeeId = returnOrder.EmployeeId;
                _return.BranchId = order.BranchId;
                _return.Fee = returnOrder.fee;
                if (returnOrder.Note != null && returnOrder.Note.Trim().Length > 0)
                {
                    _return.Note = returnOrder.Note;
                }
                _return.OrderId = order.OrderId;
                //calculate price
                //calculate order price & total origin price
                decimal orderPrice = 0;
                bool isValid = true;
                for (int i = 0; i < returnOrder.products.Count; i++)
                {
                    int paid = (int)returnOrder.products[i].ProductAttributeId;
                    int puid = (int)returnOrder.products[i].ProductUnitId;
                    ProductPrice pp = db.ProductPrices.Where(p => p.ProductAttributeId == paid && p.ProductUnitId == puid && p.IsCurrent == true).FirstOrDefault();
                    if (pp == null)
                    {
                        //invalid data ?
                        isValid = false;
                        break;
                    }
                    //find average price                    
                    orderPrice += (decimal)(pp.SalePrice * returnOrder.products[i].Quantity);
                    orderOriginPrice += (decimal)(returnOrder.products[i].PriceOriginAvg * returnOrder.products[i].Quantity);
                }
                if (!isValid)
                {
                    def.meta = new Meta(213, "Invalid parameter");
                    return Ok(def);
                }
                _return.OrderPrice = orderPrice;
                _return.ShopId = ShopId;
                _return.OrderStatus = (int)Const.OrderStatus.COMPLETED;
                _return.TotalPrice = _return.OrderPrice - _return.Fee;
                _return.Paid = returnOrder.TotalPrice - returnOrder.fee - returnOrder.TotalPaid + returnOrder.paid;
                _return.UpdatedAt = DateTime.Now;
                _return.Type = (int)Const.OrderType.RETURN;
                _return.RelatedOrderId = order.OrderId;
                db.Orders.Add(_return);
                db.SaveChanges();

                //create return product
                for (int i = 0; i < returnOrder.products.Count; i++)
                {
                    int paid = (int)returnOrder.products[i].ProductAttributeId;
                    int puid = (int)returnOrder.products[i].ProductUnitId;
                    OrderProduct returnProduct = new OrderProduct();
                    returnProduct.CreatedAt = DateTime.Now;
                    ProductPrice pp = db.ProductPrices.Where(p => p.ProductAttributeId == paid && p.ProductUnitId == puid && p.IsCurrent == true).FirstOrDefault();
                    returnProduct.Price = pp.SalePrice;
                    returnProduct.ProductAttributeId = paid;
                    returnProduct.ProductUnitId = puid;
                    returnProduct.Quantity = returnOrder.products[i].Quantity;
                    returnProduct.OrderId = _return.OrderId;
                    returnProduct.TotalPrice = returnProduct.Price * returnProduct.Quantity;
                    returnProduct.UpdatedAt = DateTime.Now;
                    db.OrderProducts.Add(returnProduct);
                }

                //create note
                if (_return.Note != null)
                {
                    Note note = new Note();
                    note.Content = returnOrder.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = _return.OrderId;
                    note.TargetType = "RETURN";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
                //end create note
                //create cash
                
                if (returnOrder.TotalCustomerCashes > 0)
                {
                    decimal orderPaid = returnOrder.TotalCustomerCashes >= returnOrder.TotalPrice ? ((decimal)returnOrder.TotalPrice - (decimal)returnOrder.fee) : (decimal)returnOrder.TotalCustomerCashes;
                    Cash cash = new Cash();
                    cash.CashCode = "PTHD" + _return.OrderCode;
                    cash.Type = (int)Const.CashType.OUTCOME;
                    //select cash type                               
                    cash.CreatedAt = DateTime.Now;
                    cash.PaidAmount = orderPaid;
                    cash.ShopId = ShopId;
                    cash.Status = (int)Const.Status.NORMAL;
                    cash.PaidTargetId = returnOrder.CustomerId;
                    cash.PaidTargetType = "CUSTOMER";
                    cash.TargetId = returnOrder.OrderId;
                    cash.TargetType = "ORDER";
                    cash.Title = "Phiếu thu đơn hàng cho khách hóa đơn " + order.OrderCode;
                    cash.Total = orderPaid;
                    cash.PaymentMethod = returnOrder.PaymentType;
                    cash.Remain = 0;
                    cash.UpdatedAt = DateTime.Now;
                    cash.EmployeeId = returnOrder.EmployeeId;
                    cash.IsManual = false;
                    db.Cashes.Add(cash);
                    db.SaveChanges();
                    //create cash flow

                    CashFlow cashFlowOut = new CashFlow();
                    cashFlowOut.ShopId = ShopId;
                    cashFlowOut.TargetId = cash.CashId;
                    cashFlowOut.TargetType = "CASH";
                    cashFlowOut.PaidTargetId = _return.CustomerId;
                    cashFlowOut.PaidTargetType = "CUSTOMER";
                    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    cashFlowOut.Amount = orderPaid;
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == _return.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == _return.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                    cashFlowOut.Debt = debt + cashFlowOut.Amount;
                    cashFlowOut.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlowOut);
                    db.SaveChanges();
                }

                //create cash flow
                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = ShopId;
                cashFlow.TargetId = _return.OrderId;
                cashFlow.TargetType = "RETURN";
                cashFlow.PaidTargetId = _return.CustomerId;
                cashFlow.PaidTargetType = "CUSTOMER";
                cashFlow.Type = (int)Const.CashFlowType.ORDER_RETURN;
                cashFlow.Amount = returnOrder.TotalPrice - returnOrder.fee;
                var debt2 = db.CashFlows.Where(cf => cf.PaidTargetId == _return.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == _return.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                cashFlow.Debt = debt2 + cashFlow.Amount;
                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();
                //end create cash flow

                //import to stock
                Exim exim = new Exim();
                exim.Code = generateEximCode((int)Const.EximType.IMPORT);
                exim.CreatetAt = DateTime.Now;
                exim.CustomDate = DateTime.Now;
                exim.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
                exim.Discount = 0;
                exim.EmployeeId = returnOrder.EmployeeId;
                exim.Fee = returnOrder.fee;
                exim.PaymentStatus = (int)Const.PaymentStatus.COMPLETED;
                exim.Price = Math.Round(orderOriginPrice, 0);
                exim.ShopId = ShopId;
                exim.Status = (int)Const.EximStatus.INIT;
                exim.TargetId = order.CustomerId;
                exim.TargetType = "CUSTOMER";
                exim.Title = "Trả hàng hóa đơn " + order.OrderCode;
                exim.Type = (int)Const.EximType.IMPORT;
                exim.UpdatedAt = DateTime.Now;
                exim.BranchId = _return.BranchId;
                db.Exims.Add(exim);
                db.SaveChanges();
                //add product to exim
                for (int i = 0; i < returnOrder.products.Count; i++)
                {
                    if (returnOrder.products[i].Quantity > 0 && returnOrder.products[i].Quantity <= returnOrder.products[i].MaxQuantity)
                    {
                        EximProduct eximProduct = new EximProduct();
                        eximProduct.CreatedAt = DateTime.Now;
                        eximProduct.DiscountPerItem = 0;
                        eximProduct.EximId = exim.EximId;
                        //
                        int productAttributeId = (int)returnOrder.products[i].ProductAttributeId;
                        var stocks = db.BranchStocks.Where(b => b.ProductAttributeId == productAttributeId).ToList();
                        int stock = 0;
                        for (int j = 0; j < stocks.Count; j++)
                        {
                            stock += (int)stocks[j].Stock;
                        }
                        //find branch stock
                        var bs = db.BranchStocks.Where(b => b.ProductAttributeId == productAttributeId && b.BranchId == returnOrder.BranchId).FirstOrDefault();
                        bs.Stock += (int)returnOrder.products[i].Quantity;
                        db.Entry(bs).State = EntityState.Modified;
                        db.SaveChanges();
                        //get base unit
                        ProductAttribute productAttribute = db.ProductAttributes.Find(productAttributeId);
                        int ProductUnitId = -1;
                        if (productAttribute != null)
                        {
                            var productUnit = db.ProductUnits.Where(pu => pu.ProductId == productAttribute.Product.ProductId && pu.Quantity == 1).FirstOrDefault();
                            if (productUnit != null)
                                ProductUnitId = productUnit.ProductUnitId;
                        }
                        eximProduct.PreviousStock = stock;
                        eximProduct.ProductAttributeId = productAttributeId;
                        eximProduct.Quantity = returnOrder.products[i].Quantity;
                        eximProduct.ProductUnitId = ProductUnitId;
                        eximProduct.LastStock = stock + eximProduct.Quantity;
                        eximProduct.UpdatedAt = DateTime.Now;
                        eximProduct.Price = Math.Round((decimal)returnOrder.products[i].PriceOriginAvg, 0);
                        db.EximProducts.Add(eximProduct);
                        db.SaveChanges();
                    }
                }

                //cash out
                //if (returnOrder.paid > 0)
                //{
                    Cash cash2 = new Cash();
                    cash2.CashCode = "PC" + _return.OrderCode; //generateCashCode((int)Const.CashType.OUTCOME);
                    cash2.Type = (int)Const.CashType.OUTCOME;
                    //select cash type                               
                    cash2.CreatedAt = DateTime.Now;
                    cash2.PaidAmount = returnOrder.TotalPrice - returnOrder.fee - returnOrder.TotalPaid + returnOrder.paid;
                    cash2.ShopId = ShopId;
                    cash2.Status = (int)Const.Status.NORMAL;
                    cash2.PaidTargetId = returnOrder.CustomerId;
                    cash2.PaidTargetType = "CUSTOMER";
                    cash2.TargetId = _return.OrderId;
                    cash2.TargetType = "RETURN";
                    cash2.Title = "Phiếu chi trả hàng cho khách hóa đơn " + order.OrderCode;
                    cash2.Total = returnOrder.TotalPrice - returnOrder.fee - returnOrder.TotalPaid + returnOrder.paid;
                    cash2.PaymentMethod = returnOrder.PaymentType;
                    cash2.Remain = 0;
                    cash2.UpdatedAt = DateTime.Now;
                    cash2.EmployeeId = returnOrder.EmployeeId;
                    cash2.IsManual = false;
                    db.Cashes.Add(cash2);
                    db.SaveChanges();
                    //create cash flow
                    if ((returnOrder.TotalPrice - returnOrder.TotalPaid + returnOrder.paid - returnOrder.fee) > 0)
                    {
                        CashFlow cashFlowOut = new CashFlow();
                        cashFlowOut.ShopId = ShopId;
                        cashFlowOut.TargetId = cash2.CashId;
                        cashFlowOut.TargetType = "CASH";
                        cashFlowOut.PaidTargetId = _return.CustomerId;
                        cashFlowOut.PaidTargetType = "CUSTOMER";
                        cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                        cashFlowOut.Amount = 0 - (returnOrder.TotalPrice - returnOrder.fee - returnOrder.TotalPaid + returnOrder.paid);
                        var debt = db.CashFlows.Where(cf => cf.PaidTargetId == _return.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == _return.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                        cashFlowOut.Debt = debt + cashFlowOut.Amount;
                        cashFlowOut.CreatedAt = DateTime.Now;
                        db.CashFlows.Add(cashFlowOut);
                        db.SaveChanges();
                    }
                //}
                //create action
                Action action = new Action();
                action.ActionName = "trả hàng";
                action.ActionType = "ADD";
                action.CreatedAt = DateTime.Now;
                action.EmployeeId = returnOrder.EmployeeId;
                action.Result = 0;
                action.ShopId = ShopId;
                action.TargetId = _return.OrderId;
                action.TargetType = "RETURN";
                db.Actions.Add(action);
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();

                def.meta = new Meta(200, "Success");
            }
            else
            {
                def.meta = new Meta(404, "Not found");
            }
            return Ok(def);
        }


        [HttpPost]
        [Route("api/shops/{id}/orders/{orderid}/cancel")]
        public IHttpActionResult cancelOrder(CancelOrder cancelOrder)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            try
            {
                if (cancelOrder != null)
                {
                    Order order = db.Orders.Find(cancelOrder.OrderId);
                    order.OrderStatus = (int)Const.OrderStatus.CANCELED;
                    order.UpdatedAt = DateTime.Now;
                    db.Entry(order).State = EntityState.Modified;
                    //create cash flow
                    CashFlow cashFlow = new CashFlow();
                    cashFlow.ShopId = ShopId;
                    cashFlow.TargetId = order.OrderId;
                    cashFlow.TargetType = "CASH";
                    cashFlow.PaidTargetId = order.CustomerId;
                    cashFlow.PaidTargetType = "CUSTOMER";
                    cashFlow.Type = (int)Const.CashFlowType.CANCELED;
                    cashFlow.Amount = order.TotalPrice;
                    //get lastest debt
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                    cashFlow.Debt = debt + cashFlow.Amount;
                    cashFlow.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlow);
                    db.SaveChanges();
                    //find cash                    
                    //return money if customer paid
                    //if (order.CustomerPaid > 0)
                    //{
                    //    //create new cash
                    //    Cash cash = new Cash();
                    //    cash.CashCode = "PC" + order.OrderCode;//generateCashCode((int)Const.CashType.OUTCOME);
                    //    cash.Type = (int)Const.CashType.OUTCOME;                      
                    //    cash.CreatedAt = DateTime.Now;
                    //    cash.EmployeeId = order.EmployeeId;
                    //    cash.PaidTargetId = order.CustomerId;
                    //    cash.PaidAmount = order.CustomerPaid;
                    //    cash.PaidTargetType = "CUSTOMER";
                    //    cash.PaymentMethod = (int)Const.PaymentType.CASH;
                    //    cash.ShopId = ShopId;
                    //    cash.Status = (int)Const.Status.NORMAL;
                    //    cash.TargetId = order.OrderId;
                    //    cash.TargetType = "ORDER";
                    //    cash.Title = "Trả tiền khách hủy hóa đơn " + order.OrderCode;
                    //    cash.Total = order.CustomerPaid;
                    //    cash.UpdatedAt = DateTime.Now;
                    //    cash.IsManual = false;
                    //    db.Cashes.Add(cash);
                    //    db.SaveChanges();
                    //    //create cash flow
                    //    CashFlow cashFlowOut = new CashFlow();
                    //    cashFlowOut.ShopId = ShopId;
                    //    cashFlowOut.TargetId = cash.CashId;
                    //    cashFlowOut.TargetType = "CASH";
                    //    cashFlowOut.PaidTargetId = order.CustomerId;
                    //    cashFlowOut.PaidTargetType = "CUSTOMER";
                    //    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    //    cashFlowOut.Amount = 0 - cash.PaidAmount;
                    //    //get lastest debt
                    //    //lastest Debt = sum (amout)
                    //    debt = db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                    //    cashFlowOut.Debt = debt + cashFlowOut.Amount;
                    //    cashFlowOut.CreatedAt = DateTime.Now;
                    //    db.CashFlows.Add(cashFlowOut);
                    //    db.SaveChanges();
                    //}
                    //return product to stock
                    for (int i = 0; i < order.OrderProducts.Count; i++)
                    {
                        //get branch stock
                        OrderProduct orderProduct = db.OrderProducts.Find(order.OrderProducts.ToList()[i].OrderProductId);
                        var id = orderProduct.ProductAttributeId;
                        var quantity = orderProduct.Quantity;
                        var productUnit = orderProduct.ProductUnit.Quantity;
                        BranchStock branchStock = db.BranchStocks.Where(bs => bs.BranchId == order.BranchId && bs.ProductAttributeId == id).FirstOrDefault();
                        if (branchStock != null)
                        {
                            branchStock.Stock += quantity * productUnit;
                            branchStock.UpdatedAt = DateTime.Now;
                            db.SaveChanges();
                        }
                    }

                    //create action
                    Action action = new Action();
                    action.ActionName = "hủy đơn hàng";
                    action.ActionType = "DELETE";
                    action.CreatedAt = DateTime.Now;
                    action.EmployeeId = cancelOrder.EmployeeId;
                    action.Result = 0;
                    action.ShopId = ShopId;
                    action.TargetId = order.OrderId;
                    action.TargetType = "ORDER";
                    db.Actions.Add(action);
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(404, "Not found");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(500, "Internal server error");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/shops/{id}/cashes/{cashid}/cancel")]
        public IHttpActionResult cancelCashes(CancelCashes cancelCash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            try
            {
                if (cancelCash != null)
                {
                    Cash cash = db.Cashes.Find(cancelCash.CashId);
                    cash.Status = (int)Const.Status.DELETED;
                    cash.UpdatedAt = DateTime.Now;
                    db.Entry(cash).State = EntityState.Modified;


                    var cf = db.CashFlows.Where(e => e.TargetId == cancelCash.CashId && e.TargetType == "CASH").ToList();
                    if (cf != null || cf.Count > 0)
                    {
                        foreach (var item in cf)
                        {
                            CashFlow cashflow = db.CashFlows.Find(item.CashFlowId);
                            cashflow.Type = (int)Const.CashType.DELETED;
                            db.Entry(cashflow).State = EntityState.Modified;
                        }
                    }
                    //create cash flow
                    //CashFlow cashFlow = new CashFlow();
                    //cashFlow.ShopId = ShopId;
                    //cashFlow.TargetId = order.OrderId;
                    //cashFlow.TargetType = "ORDER";
                    //cashFlow.PaidTargetId = order.CustomerId;
                    //cashFlow.PaidTargetType = "CUSTOMER";
                    //cashFlow.Type = (int)Const.CashFlowType.CANCELED;
                    //cashFlow.Amount = order.TotalPrice;
                    ////get lastest debt
                    ////lastest Debt = sum (amout)
                    //var debt = db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                    //cashFlow.Debt = debt + cashFlow.Amount;
                    //cashFlow.CreatedAt = DateTime.Now;
                    //db.CashFlows.Add(cashFlow);
                    //db.SaveChanges();
                    ////find cash                    
                    ////return money if customer paid
                    //if (order.CustomerPaid > 0)
                    //{
                    //    //create new cash
                    //    Cash cash = new Cash();
                    //    cash.CashCode = "PC" + order.OrderCode;//generateCashCode((int)Const.CashType.OUTCOME);
                    //    cash.Type = (int)Const.CashType.OUTCOME;
                    //    cash.CreatedAt = DateTime.Now;
                    //    cash.EmployeeId = order.EmployeeId;
                    //    cash.PaidTargetId = order.CustomerId;
                    //    cash.PaidAmount = order.CustomerPaid;
                    //    cash.PaidTargetType = "CUSTOMER";
                    //    cash.PaymentMethod = (int)Const.PaymentType.CASH;
                    //    cash.ShopId = ShopId;
                    //    cash.Status = (int)Const.Status.NORMAL;
                    //    cash.TargetId = order.OrderId;
                    //    cash.TargetType = "ORDER";
                    //    cash.Title = "Trả tiền khách hủy hóa đơn " + order.OrderCode;
                    //    cash.Total = order.CustomerPaid;
                    //    cash.UpdatedAt = DateTime.Now;
                    //    db.Cashes.Add(cash);
                    //    db.SaveChanges();
                    //    //create cash flow
                    //    CashFlow cashFlowOut = new CashFlow();
                    //    cashFlowOut.ShopId = ShopId;
                    //    cashFlowOut.TargetId = cash.CashId;
                    //    cashFlowOut.TargetType = "CASH";
                    //    cashFlowOut.PaidTargetId = order.CustomerId;
                    //    cashFlowOut.PaidTargetType = "CUSTOMER";
                    //    cashFlowOut.Type = (int)Const.CashFlowType.PAID;
                    //    cashFlowOut.Amount = 0 - cash.PaidAmount;
                    //    //get lastest debt
                    //    //lastest Debt = sum (amout)
                    //    debt = db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == order.CustomerId && cf.PaidTargetType == "CUSTOMER").Sum(cf => cf.Amount).Value : 0;
                    //    cashFlowOut.Debt = debt + cashFlowOut.Amount;
                    //    cashFlowOut.CreatedAt = DateTime.Now;
                    //    db.CashFlows.Add(cashFlowOut);
                    //    db.SaveChanges();
                    //}
                    
                    //create action
                    Action action = new Action();
                    action.ActionName = "hủy phiếu";
                    action.ActionType = "DELETE";
                    action.CreatedAt = DateTime.Now;
                    action.EmployeeId = cancelCash.EmployeeId;
                    action.Result = 0;
                    action.ShopId = ShopId;
                    action.TargetId = cash.CashId;
                    action.TargetType = "CASH";
                    db.Actions.Add(action);
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                else
                {
                    def.meta = new Meta(404, "Not found");
                    return Ok(def);
                }
            }
            catch (Exception ex)
            {
                def.meta = new Meta(500, "Internal server error");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/shops/{id}/transfers")]
        public IHttpActionResult GetTransfers(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from o in db.Transfers
                              where o.ShopId == id
                              orderby o.CreatedAt descending
                              select new SaleManager.Data.ShopTransfer
                              {
                                  TransferId = o.TransferId,
                                  Code = o.Code,
                                  Total = o.Total,
                                  Status = o.Status,
                                  CreatedAt = o.CreatedAt,
                                  Note = o.Note,
                                  fromBranch = new SaleManager.Data.Branch()
                                  {
                                      BranchId = o.FromBranchId,
                                      BranchName = o.FromBranch.BranchName
                                  },
                                  toBranch = new SaleManager.Data.Branch()
                                  {
                                      BranchId = o.ToBranchId,
                                      BranchName = o.ToBranch.BranchName
                                  },
                                  employee = new SaleManager.Data.Employee()
                                  {
                                      EmployeeId = o.Employee.EmployeeId,
                                      EmployeeName = o.Employee.EmployeeName
                                  }
                              }).AsEnumerable();
                int count = result.Count();
                if (paging.query != null)
                {
                    result = result.Where(HttpUtility.UrlDecode(paging.query));
                    count = result.Count();
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                var returnData = result.ToList();
                def.meta = new Meta(200, "Success");
                def.data = result;
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpPost]
        [Route("api/shops/{id}/transfers")]
        public IHttpActionResult createTransfer(Data.Transfer transfer)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();

            //check product
            if (transfer.products == null || transfer.products.Count == 0)
            {
                def.meta = new Meta(211, "Invalid parameter");
                return Ok(def);
            }
            else
            {
                //check product
                bool isInvalid = false;
                int count = 0;
                for (int i = 0; i < transfer.products.Count; i++)
                {
                    if (transfer.products[i].Quantity <= 0)
                        count += 1;
                }
                if (count == transfer.products.Count)
                    isInvalid = true;
                if (isInvalid)
                {
                    def.meta = new Meta(211, "Invalid parameter");
                    return Ok(def);
                }
                else
                {
                    //check stock valid
                    count = 0;
                    isInvalid = true;
                    for (int i = 0; i < transfer.products.Count; i++)
                    {
                        var id = transfer.products[i].ProductAttributeId;
                        BranchStock bs = db.BranchStocks.Where(b => b.ProductAttributeId == id && b.BranchId == transfer.FromBranchId).FirstOrDefault();
                        if (bs.Stock >= transfer.products[i].Quantity)
                            count += 1;
                    }
                    if (count == transfer.products.Count)
                        isInvalid = false;
                    if (isInvalid)
                    {
                        def.meta = new Meta(212, "Invalid stock");
                        return Ok(def);
                    }
                }
            }
            Branch toBranch = db.Branches.Find(transfer.ToBranchId);
            Branch fromBranch = db.Branches.Find(transfer.FromBranchId);
            if (toBranch == null || fromBranch == null)
            {
                def.meta = new Meta(213, "Invalid branch");
                return Ok(def);
            }
            //create transfer
            Transfer _transfer = new Transfer();
            _transfer.Code = generateTransferCode();
            _transfer.CreatedAt = DateTime.Now;
            _transfer.FromBranchId = fromBranch.BranchId;
            if (transfer.Note != null && transfer.Note.Trim().Length > 0)
                _transfer.Note = transfer.Note;
            _transfer.ShopId = ShopId;
            _transfer.Status = 2;
            _transfer.ToBranchId = toBranch.BranchId;
            _transfer.Total = transfer.Total;
            _transfer.UpdatedAt = DateTime.Now;
            _transfer.EmployeeId = transfer.EmployeeId;
            db.Transfers.Add(_transfer);
            db.SaveChanges();
            //create note
            if (transfer.Note != null && transfer.Note.Trim().Length > 0)
            {
                Note note = new Note();
                note.Content = transfer.Note;
                note.CreatedAt = DateTime.Now;
                note.ShopId = ShopId;
                note.Status = 0;
                note.TargetId = _transfer.TransferId;
                note.TargetType = "TRANSFER";
                db.Notes.Add(note);
                db.SaveChanges();
            }
            //create transfer product
            for (int i = 0; i < transfer.products.Count; i++)
            {
                var id = transfer.products[i].ProductAttributeId;
                BranchStock bsFrom = db.BranchStocks.Where(b => b.ProductAttributeId == id && b.BranchId == transfer.FromBranchId).FirstOrDefault();
                BranchStock bsTo = db.BranchStocks.Where(b => b.ProductAttributeId == id && b.BranchId == transfer.ToBranchId).FirstOrDefault();
                TransferProduct transferProduct = new TransferProduct();
                transferProduct.CreatedAt = DateTime.Now;
                transferProduct.Price = transfer.products[i].Price;
                transferProduct.ProductAttributeId = transfer.products[i].ProductAttributeId;
                transferProduct.Quantity = transfer.products[i].Quantity;
                transferProduct.TotalPrice = transferProduct.Price * transfer.products[i].Quantity;
                transferProduct.TransferId = _transfer.TransferId;
                transferProduct.UpdatedAt = DateTime.Now;
                db.TransferProducts.Add(transferProduct);
                db.SaveChanges();
                //export
                bsFrom.Stock -= transfer.products[i].Quantity;
                bsFrom.UpdatedAt = DateTime.Now;
                db.Entry(bsFrom).State = EntityState.Modified;
                db.SaveChanges();
                //import
                bsTo.Stock += transfer.products[i].Quantity;
                bsTo.UpdatedAt = DateTime.Now;
                db.Entry(bsTo).State = EntityState.Modified;
                db.SaveChanges();
            }
            //export
            //Exim export = new Exim();
            //export.Code = generateEximCode((int)Const.EximType.EXPORT);
            //export.CreatetAt = DateTime.Now;
            //export.CustomDate = DateTime.Now;
            //export.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
            //export.Discount = 0;
            //export.EmployeeId = transfer.EmployeeId;
            //export.Fee = 0;
            //export.PaymentStatus = (int)Const.PaymentStatus.COMPLETED;
            //export.Price = transfer.Total;
            //export.ShopId = ShopId;
            //export.Status = (int)Const.EximStatus.INIT;
            //export.TargetId = transfer.FromBranchId;
            //export.TargetType = "BRANCH";
            //export.Title = "Chuyển hàng từ " + fromBranch.BranchName + " đến " + toBranch.BranchName;
            //export.Type = (int)Const.EximType.EXPORT;
            //export.UpdatedAt = DateTime.Now;
            //db.Exims.Add(export);
            //db.SaveChanges();
            ////import
            //Exim import = new Exim();
            //import.Code = generateEximCode((int)Const.EximType.IMPORT);
            //import.CreatetAt = DateTime.Now;
            //import.CustomDate = DateTime.Now;
            //import.DeliveryStatus = (int)Const.DeliveryStatus.COMPLETED;
            //import.Discount = 0;
            //import.EmployeeId = transfer.EmployeeId;
            //import.Fee = 0;
            //import.PaymentStatus = (int)Const.PaymentStatus.COMPLETED;
            //import.Price = transfer.Total;
            //import.ShopId = ShopId;
            //import.Status = (int)Const.EximStatus.INIT;
            //import.TargetId = toBranch.BranchId;
            //import.TargetType = "BRANCH";
            //import.Title = "Chuyển hàng từ " + fromBranch.BranchName + " đến " + toBranch.BranchName;
            //import.Type = (int)Const.EximType.IMPORT;
            //import.UpdatedAt = DateTime.Now;
            //db.Exims.Add(import);
            //db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpGet]
        [HttpPost]
        [Route("api/shops/{id}/orderCode")]
        public IHttpActionResult genOrderCode()
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Orders.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopOrderPrefix + code;
            def.data = code;
            def.meta = new Meta(200, "success");
            return Ok(def);
        }


        private string generateProductCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            int count = db.ProductAttributes.Where(o => o.Product.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopProductPrefix + code;
            return code;
        }

        private string generateOrderCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Orders.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopOrderPrefix + code;
            return code;
        }

        private string generateReturnCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Orders.Where(o => o.ShopId == ShopId && o.Type == 1).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopReturnPrefix + code;
            return code;
        }

        private string generateEximCode(int type)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Exims.Where(o => o.ShopId == ShopId && o.Type == type).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            if (type == 0)
                code = shop.ShopImportPrefix + code;
            else
                code = shop.ShopExportPrefix + code;
            return code;
        }

        private string generateCashCode(int type)
        {
            string prefix = "";
            if (type == 0)
                prefix = "PC";
            else
                prefix = "PT";
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Cashes.Where(o => o.ShopId == ShopId && o.CashType.CashTypeCode.Contains(prefix)).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = prefix + code;
            return code;
        }
        private string generateTransferCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Transfers.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopTransferPrefix + code;
            return code;
        }

        private string generateReturnImportCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Exims.Where(o => o.ShopId == ShopId && o.RelatedEximId != null).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;         
            code = "THN" + code;
            return code;
        }
        //[HttpPost]
        //[Route("api/shops/{id}/upload")]
        //public IHttpActionResult Upload(int id)
        //{
        //    string domain = ConfigurationManager.AppSettings["domain"].ToString();
        //    var identity = (ClaimsIdentity)User.Identity;
        //    int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
        //    DefaultResponse def = new DefaultResponse();
        //    try
        //    {
        //        var httpRequest = HttpContext.Current.Request;
        //        List<String> files = new List<string>();
        //        foreach (string file in httpRequest.Files)
        //        {
        //            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

        //            var postedFile = httpRequest.Files[file];
        //            if (postedFile != null && postedFile.ContentLength > 0)
        //            {

        //                int MaxContentLength = 4096 * 4096 * 1; //Size = 16 MB  

        //                IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
        //                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //                var extension = ext.ToLower();
        //                if (!AllowedFileExtensions.Contains(extension))
        //                {
        //                    var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
        //                    def.meta = new Meta(600, message);
        //                    return Ok(def);
        //                }
        //                else if (postedFile.ContentLength > MaxContentLength)
        //                {

        //                    var message = string.Format("Please Upload a file upto 16 mb.");
        //                    def.meta = new Meta(600, message);
        //                    return Ok(def);
        //                }
        //                else
        //                {
        //                    byte[] fileData = null;
        //                    using (var binaryReader = new BinaryReader(postedFile.InputStream))
        //                    {
        //                        fileData = binaryReader.ReadBytes(postedFile.ContentLength);
        //                        var filePath = HttpContext.Current.Server.MapPath("~/Images/" + ShopId + "/" + postedFile.FileName + "_thumb1024_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension);
        //                        Utils.createThumb(1024, filePath, fileData);
        //                        string img = domain + "/images/" + ShopId + "/" + postedFile.FileName + "_thumb1024_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
        //                        string rel = "/images/" + ShopId + "/" + postedFile.FileName + "_thumb1024_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
        //                        files.Add(img);
        //                    }
        //                }
        //            }
        //        }
        //        def.meta = new Meta(200, "Success");
        //        def.data = files;
        //        return Ok(def);
        //    }
        //    catch (Exception ex)
        //    {
        //        def.meta = new Meta(400, "Bad request");
        //        return Ok(def);
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShopExists(int id)
        {
            return db.Shops.Count(e => e.ShopId == id) > 0;
        }
    }
}