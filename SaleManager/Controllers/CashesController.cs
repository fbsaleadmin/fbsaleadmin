﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;
using log4net;
using Newtonsoft.Json;
using SaleManager.Data;

namespace SaleManager.Controllers
{
    [Authorize]
    public class CashesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        private static readonly ILog log = LogMaster.GetLogger("cashes", "cashes");

        public IHttpActionResult PostCash(CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "CUSTOMER")
            {
                Customer customer = db.Customers.Find(cash.PaidTargetId);
                if (customer != null)
                {
                    title = cashType.Name + " cho khách hàng " + customer.CustomerName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }
            else if (cash.PaidTargetType == "VENDOR")
            {
                Vendor vendor = db.Vendors.Find(cash.PaidTargetId);
                if (vendor != null)
                {
                    title = cashType.Name + " cho đối tác " + vendor.VendorName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }
            else if (cash.PaidTargetType == "EMPLOYEE")
            {
                Employee employee = db.Employees.Find(cash.PaidTargetId);
                if (employee != null)
                {
                    title = cashType.Name + " cho nhân viên " + employee.EmployeeName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }
            else if (cash.PaidTargetType == "DELIVERY_VENDOR")
            {
                DeliveryVendor dvendor = db.DeliveryVendors.Find(cash.PaidTargetId);
                if (dvendor != null)
                {
                    title = cashType.Name + " cho khách hàng " + dvendor.VendorName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(212, "invalid parameter");
                return Ok(def);
            }
            Nullable<int> TargetId = null;
            //check target 
            if (cash.TargetType != null && cash.TargetType != "-1")
            {
                //check exist
                if (cash.TargetType == "ORDER")
                {                    
                    Order order = db.Orders.Where(o => o.OrderCode == cash.TargetCode && o.CustomerId == cash.PaidTargetId).FirstOrDefault();
                    //var cashOr= from db.
                    if (order != null)
                        TargetId = order.OrderId;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else if (cash.TargetType == "IMPORT")
                {
                    Exim exim = db.Exims.Where(o => o.Code == cash.TargetCode && o.TargetId == cash.PaidTargetId && o.TargetType == "VENDOR").FirstOrDefault();
                    if (exim != null)
                        TargetId = exim.EximId;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else if (cash.TargetType == "RETURN")
                {
                    Order order = db.Orders.Where(o => o.OrderCode == cash.TargetCode && o.CustomerId == cash.PaidTargetId).FirstOrDefault();
                    if (order != null)
                        TargetId = order.OrderId;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else if (cash.TargetType == "IMPORT_RETURN")
                {
                    Exim exim = db.Exims.Where(o => o.Code == cash.TargetCode && o.TargetId == cash.PaidTargetId && o.TargetType == "VENDOR").FirstOrDefault();
                    if (exim != null)
                        TargetId = exim.EximId;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(213, "invalid parameter");
                    return Ok(def);
                }
            }
            Cash newCash = new Cash();
            newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
            newCash.CashTypeId = cashType.CashTypeId;
            newCash.CreatedAt = DateTime.Now;
            newCash.EmployeeId = cash.EmployeeId;
            if (cash.Note != null && cash.Note.Trim().Length > 0)
                newCash.Note = cash.Note;
            newCash.PaidAmount = cash.PaidAmount;
            newCash.PaidTargetId = cash.PaidTargetId;
            newCash.PaidTargetType = cash.PaidTargetType;
            newCash.PaymentMethod = (int)Const.PaymentType.CASH;
            newCash.Remain = 0;
            newCash.ShopId = ShopId;
            newCash.Status = (int)Const.Status.NORMAL;
            newCash.Title = title;
            newCash.Total = cash.PaidAmount;
            newCash.Type = cashType.Type;
            newCash.UpdatedAt = DateTime.Now;
            newCash.IsManual = true;
            if (TargetId != null)
            {
                newCash.TargetId = TargetId;
                newCash.TargetType = cash.TargetType;
            }
            db.Cashes.Add(newCash);
            db.SaveChanges();
            //cash flow           
            if (cash.IsAddToFlow)
            {
                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = ShopId;
                cashFlow.TargetId = newCash.CashId;
                cashFlow.TargetType = "CASH";
                cashFlow.PaidTargetId = cash.PaidTargetId;
                cashFlow.PaidTargetType = cash.PaidTargetType;
                cashFlow.Type = (int)Const.CashFlowType.PAID;
                if (newCash.Type == (int)Const.CashType.INCOME)
                    cashFlow.Amount = cash.PaidAmount;
                else
                    cashFlow.Amount = 0 - cash.PaidAmount;
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                //var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Select(e=>e.Debt).FirstOrDefault().HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Select(e=>e.Debt).FirstOrDefault().Value : 0;

                cashFlow.Debt = debt + cashFlow.Amount;
                //khach hang no 1.000.000
                // chi + 1.000.000 - (- 200.000)
                // thu + 1.000.000 - (+ 200.000)
                //no khach 1.000.000
                // chi  - 1.000.000 - (- 200.000) = - 800.000
                // thu  - 1.000.000 - (+ 200.000) = - 1.200.000
                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();
            }

            if (newCash.Note != null && newCash.Note.Trim().Length > 0)
            {
                Note note = new Note();
                note.Content = newCash.Note;
                note.CreatedAt = DateTime.Now;
                note.ShopId = ShopId;
                note.Status = 0;
                note.TargetId = newCash.CashId;
                note.TargetType = "CASH";
                db.Notes.Add(note);
                db.SaveChanges();
            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        public IHttpActionResult PutOrder(int id, CashDTO cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();

            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != cash.CashId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Cash current = db.Cashes.Where(s => s.CashId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                string lastNote = current.Note != null ? current.Note.Trim() : "";
                //current.DeliveryStatus = order.DeliveryStatus;
                if (cash.Note != null && cash.Note.Trim().Length > 0 && !lastNote.Equals(cash.Note.Trim()))
                    current.Note = cash.Note;
                if (cash.Note != null && cash.Note.Trim().Length > 0 && !lastNote.Equals(cash.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = cash.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.CashId;
                    note.TargetType = "CASH";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        [HttpPost]
        [Route("api/cashes/{id}/createcash-customer")]
        public IHttpActionResult createCashFromCustomer(int id,CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "CUSTOMER")
            {
                Customer customer = db.Customers.Find(cash.PaidTargetId);
                if (customer != null)
                {
                    title = cashType.Name + " cho khách hàng " + customer.CustomerName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }
            
            IEnumerable<int> TargetId=null;
            //if (cashType.Type == (int)Const.CashType.INCOME) { 

            //}
            //check target 
            if (cash.TargetType != null && cash.TargetType != "-1")
            {
                //check exist
                if (cash.TargetType == "ORDER")
                {
                    var order = db.Orders.Where(o => o.CustomerId == cash.PaidTargetId && o.Type==0).Select(o=>o.OrderId).ToList();
                    if (order != null)
                        TargetId = order;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else if (cash.TargetType == "RETURN")
                {
                    var order = db.Orders.Where(o => o.CustomerId == cash.PaidTargetId && o.Type == 1).Select(o => o.OrderId).ToList();
                    if (order != null)
                        TargetId = order;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(213, "invalid parameter");
                    return Ok(def);
                }
            }

            foreach (var item in TargetId)
            {              
                var cashPaid = db.Cashes.Where(o => o.TargetId == item && o.TargetType==cash.TargetType).Select(o => o.PaidAmount).Sum().HasValue ? db.Cashes.Where(o => o.TargetId == item && o.TargetType==cash.TargetType).Select(o => o.PaidAmount).Sum().Value : 0;
                var orderPaid = db.Orders.Where(o => o.OrderId == item).Select(o => o.TotalPrice).Sum().HasValue ? db.Orders.Where(o => o.OrderId == item).Select(o => o.TotalPrice).Sum().Value : 0;
                decimal paidPrice = orderPaid - cashPaid;
                decimal paidPriceNew = 0;
                if (paidPrice > 0)
                {
                    if (paidPrice <= cash.PaidAmount)
                    {
                        paidPriceNew = paidPrice;
                    }
                    else
                    {
                        paidPriceNew = (decimal)cash.PaidAmount;
                    }
                    cash.PaidAmount = cash.PaidAmount - paidPriceNew;
                }

                if (paidPriceNew > 0)
                {
                    Cash newCash = new Cash();
                    newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
                    newCash.CashTypeId = cashType.CashTypeId;
                    newCash.CreatedAt = DateTime.Now;
                    newCash.EmployeeId = cash.EmployeeId;
                    if (cash.Note != null && cash.Note.Trim().Length > 0)
                        newCash.Note = cash.Note;
                    newCash.PaidAmount = paidPriceNew;
                    newCash.PaidTargetId = cash.PaidTargetId;
                    newCash.PaidTargetType = cash.PaidTargetType;
                    newCash.PaymentMethod = (int)Const.PaymentType.CASH;
                    newCash.Remain = 0;
                    newCash.ShopId = id;
                    newCash.Status = (int)Const.Status.NORMAL;
                    newCash.Title = title;
                    newCash.Total = paidPriceNew;
                    newCash.Type = cashType.Type;
                    newCash.UpdatedAt = DateTime.Now;
                    newCash.IsManual = false;
                    if (TargetId != null)
                    {
                        newCash.TargetId = item;
                        newCash.TargetType = cash.TargetType;
                    }
                    db.Cashes.Add(newCash);
                    db.SaveChanges();
                    //cash flow           

                    CashFlow cashFlow = new CashFlow();
                    cashFlow.ShopId = id;
                    cashFlow.TargetId = newCash.CashId;
                    cashFlow.TargetType = "CASH";
                    cashFlow.PaidTargetId = cash.PaidTargetId;
                    cashFlow.PaidTargetType = cash.PaidTargetType;
                    cashFlow.Type = (int)Const.CashFlowType.PAID;
                    if (newCash.Type == (int)Const.CashType.INCOME)
                        cashFlow.Amount = paidPriceNew;
                    else
                        cashFlow.Amount = 0 - paidPriceNew;
                    //get lastest debt
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                    cashFlow.Debt = debt + cashFlow.Amount;

                    cashFlow.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlow);
                    db.SaveChanges();

                    if (newCash.Note != null && newCash.Note.Trim().Length > 0)
                    {
                        Note note = new Note();
                        note.Content = newCash.Note;
                        note.CreatedAt = DateTime.Now;
                        note.ShopId = id;
                        note.Status = 0;
                        note.TargetId = newCash.CashId;
                        note.TargetType = "CASH";
                        db.Notes.Add(note);
                        db.SaveChanges();
                    }
                }

                if (cash.PaidAmount <= 0) break;

            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        [HttpPost]
        [Route("api/cashes/{id}/createcash-order")]
        public IHttpActionResult createCashFromOrder(int id, CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "CUSTOMER")
            {
                Customer customer = db.Customers.Find(cash.PaidTargetId);
                if (customer != null)
                {
                    title = cashType.Name + " cho khách hàng " + customer.CustomerName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }

            if (cash.PaidAmount > 0)
            {
                Cash newCash = new Cash();
                newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
                newCash.CashTypeId = cashType.CashTypeId;
                newCash.CreatedAt = DateTime.Now;
                newCash.EmployeeId = cash.EmployeeId;
                if (cash.Note != null && cash.Note.Trim().Length > 0)
                    newCash.Note = cash.Note;
                newCash.PaidAmount = cash.PaidAmount;
                newCash.PaidTargetId = cash.PaidTargetId;
                newCash.PaidTargetType = cash.PaidTargetType;
                newCash.PaymentMethod = (int)Const.PaymentType.CASH;
                newCash.Remain = 0;
                newCash.ShopId = id;
                newCash.Status = (int)Const.Status.NORMAL;
                newCash.Title = title;
                newCash.Total = cash.PaidAmount;
                newCash.Type = cashType.Type;
                newCash.UpdatedAt = DateTime.Now;
                newCash.IsManual = false;
                if (cash.TargetId != null)
                {
                    newCash.TargetId = cash.TargetId;
                    newCash.TargetType = cash.TargetType;
                }
                db.Cashes.Add(newCash);
                db.SaveChanges();
                //cash flow           

                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = id;
                cashFlow.TargetId = newCash.CashId;
                cashFlow.TargetType = "CASH";
                cashFlow.PaidTargetId = cash.PaidTargetId;
                cashFlow.PaidTargetType = cash.PaidTargetType;
                cashFlow.Type = (int)Const.CashFlowType.PAID;
                if (newCash.Type == (int)Const.CashType.INCOME)
                    cashFlow.Amount = cash.PaidAmount;
                else
                    cashFlow.Amount = 0 - cash.PaidAmount;
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                cashFlow.Debt = debt + cash.PaidAmount;

                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();

                if (newCash.Note != null && newCash.Note.Trim().Length > 0)
                {
                    Note note = new Note();
                    note.Content = newCash.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = id;
                    note.Status = 0;
                    note.TargetId = newCash.CashId;
                    note.TargetType = "CASH";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        [HttpPost]
        [Route("api/cashes/{id}/createcash-customer-return")]
        public IHttpActionResult createCashFromCustomerReturn(int id, CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "CUSTOMER")
            {
                Customer customer = db.Customers.Find(cash.PaidTargetId);
                if (customer != null)
                {
                    title = cashType.Name + " cho khách hàng " + customer.CustomerName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }

            if (cash.PaidAmount > 0)
            {
                Cash newCash = new Cash();
                newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
                newCash.CashTypeId = cashType.CashTypeId;
                newCash.CreatedAt = DateTime.Now;
                newCash.EmployeeId = cash.EmployeeId;
                if (cash.Note != null && cash.Note.Trim().Length > 0)
                    newCash.Note = cash.Note;
                newCash.PaidAmount = cash.PaidAmount;
                newCash.PaidTargetId = cash.PaidTargetId;
                newCash.PaidTargetType = cash.PaidTargetType;
                newCash.PaymentMethod = (int)Const.PaymentType.CASH;
                newCash.Remain = 0;
                newCash.ShopId = id;
                newCash.Status = (int)Const.Status.NORMAL;
                newCash.Title = title;
                newCash.Total = cash.PaidAmount;
                newCash.Type = cashType.Type;
                newCash.UpdatedAt = DateTime.Now;
                newCash.IsManual = false;
                if (cash.TargetId != null)
                {
                    newCash.TargetId = cash.TargetId;
                    newCash.TargetType = cash.TargetType;
                }
                db.Cashes.Add(newCash);
                db.SaveChanges();
                //cash flow           

                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = id;
                cashFlow.TargetId = newCash.CashId;
                cashFlow.TargetType = "CASH";
                cashFlow.PaidTargetId = cash.PaidTargetId;
                cashFlow.PaidTargetType = cash.PaidTargetType;
                cashFlow.Type = (int)Const.CashFlowType.PAID;
                //if (newCash.Type == (int)Const.CashType.INCOME)
                //    cashFlow.Amount = cash.PaidAmount;
                //else
                cashFlow.Amount = 0 - cash.PaidAmount;
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                cashFlow.Debt = debt + cashFlow.Amount;

                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();

                if (newCash.Note != null && newCash.Note.Trim().Length > 0)
                {
                    Note note = new Note();
                    note.Content = newCash.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = id;
                    note.Status = 0;
                    note.TargetId = newCash.CashId;
                    note.TargetType = "CASH";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        [HttpPost]
        [Route("api/cashes/{id}/createcash-vendor")]
        public IHttpActionResult createCashFromVendor(int id, CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            //int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "VENDOR")
            {
                Vendor vendor = db.Vendors.Find(cash.PaidTargetId);
                if (vendor != null)
                {
                    title = cashType.Name + " cho nhà cung cấp " + vendor.VendorName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }
            else
            {
                def.meta = new Meta(212, "invalid parameter - invalid paid target");
                return Ok(def);
            }

            IEnumerable<int> TargetId = null;

            //check target 
            if (cash.TargetType != null && cash.TargetType != "-1")
            {
                //check exist
                if (cash.TargetType == "IMPORT")
                {
                    var import = db.Exims.Where(e => e.TargetId == cash.PaidTargetId && e.TargetType=="VENDOR" && e.Type == (int)Const.EximType.IMPORT).Select(e => e.EximId).ToList();
                    if (import != null)
                        TargetId = import;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else if (cash.TargetType == "IMPORT_RETURN")
                {
                    var import = db.Exims.Where(e => e.TargetId == cash.PaidTargetId && e.TargetType == "VENDOR" && e.Type == (int)Const.EximType.EXPORT).Select(e => e.EximId).ToList();
                    if (import != null)
                        TargetId = import;
                    else
                    {
                        def.meta = new Meta(213, "invalid parameter - invalid target");
                        return Ok(def);
                    }
                }
                else
                {
                    def.meta = new Meta(213, "invalid parameter");
                    return Ok(def);
                }
            }

            foreach (var item in TargetId)
            {
                var cashPaid = db.Cashes.Where(o => o.TargetId == item && o.TargetType == cash.TargetType).Select(o => o.PaidAmount).Sum().HasValue ? db.Cashes.Where(o => o.TargetId == item && o.TargetType == cash.TargetType).Select(o => o.PaidAmount).Sum().Value : 0;
                //var orderPaid = db.Orders.Where(o => o.OrderId == item).Select(o => o.TotalPrice).Sum().HasValue ? db.Orders.Where(o => o.OrderId == item).Select(o => o.TotalPrice).Sum().Value : 0;
                var importPaid = db.Exims.Where(o => o.EximId == item).Select(o => o.TotalPrice).Sum().HasValue ? db.Exims.Where(o => o.EximId == item).Select(o => o.TotalPrice).Sum().Value : 0;
                decimal paidPrice = importPaid - cashPaid;
                decimal paidPriceNew = 0;
                if (paidPrice > 0)
                {
                    if (paidPrice <= cash.PaidAmount)
                    {
                        paidPriceNew = paidPrice;
                    }
                    else
                    {
                        paidPriceNew = (decimal)cash.PaidAmount;
                    }
                    cash.PaidAmount = cash.PaidAmount - paidPriceNew;
                }

                if (paidPriceNew > 0)
                {
                    Cash newCash = new Cash();
                    newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
                    newCash.CashTypeId = cashType.CashTypeId;
                    newCash.CreatedAt = DateTime.Now;
                    newCash.EmployeeId = cash.EmployeeId;
                    if (cash.Note != null && cash.Note.Trim().Length > 0)
                        newCash.Note = cash.Note;
                    newCash.PaidAmount = paidPriceNew;
                    newCash.PaidTargetId = cash.PaidTargetId;
                    newCash.PaidTargetType = cash.PaidTargetType;
                    newCash.PaymentMethod = (int)Const.PaymentType.CASH;
                    newCash.Remain = 0;
                    newCash.ShopId = id;
                    newCash.Status = (int)Const.Status.NORMAL;
                    newCash.Title = title;
                    newCash.Total = paidPriceNew;
                    newCash.Type = cashType.Type;
                    newCash.UpdatedAt = DateTime.Now;
                    newCash.IsManual = false;
                    if (TargetId != null)
                    {
                        newCash.TargetId = item;
                        newCash.TargetType = cash.TargetType;
                    }
                    db.Cashes.Add(newCash);
                    db.SaveChanges();
                    //cash flow           

                    CashFlow cashFlow = new CashFlow();
                    cashFlow.ShopId = id;
                    cashFlow.TargetId = newCash.CashId;
                    cashFlow.TargetType = "CASH";
                    cashFlow.PaidTargetId = cash.PaidTargetId;
                    cashFlow.PaidTargetType = cash.PaidTargetType;
                    cashFlow.Type = (int)Const.CashFlowType.PAID;
                    if (newCash.Type == (int)Const.CashType.INCOME)
                        cashFlow.Amount = paidPriceNew;
                    else
                        cashFlow.Amount = 0 - paidPriceNew;
                    //get lastest debt
                    //lastest Debt = sum (amout)
                    var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                    cashFlow.Debt = debt + cashFlow.Amount;

                    cashFlow.CreatedAt = DateTime.Now;
                    db.CashFlows.Add(cashFlow);
                    db.SaveChanges();

                    if (newCash.Note != null && newCash.Note.Trim().Length > 0)
                    {
                        Note note = new Note();
                        note.Content = newCash.Note;
                        note.CreatedAt = DateTime.Now;
                        note.ShopId = id;
                        note.Status = 0;
                        note.TargetId = newCash.CashId;
                        note.TargetType = "CASH";
                        db.Notes.Add(note);
                        db.SaveChanges();
                    }
                }

                if (cash.PaidAmount <= 0) break;

            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        [HttpPost]
        [Route("api/cashes/{id}/createcash-exims")]
        public IHttpActionResult createCashFromExim(int id, CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "VENDOR")
            {
                Vendor vendor = db.Vendors.Find(cash.PaidTargetId);
                if (vendor != null)
                {
                    title = cashType.Name + " cho nhà cung cấp " + vendor.VendorName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }

            if (cash.PaidAmount > 0)
            {
                Cash newCash = new Cash();
                newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
                newCash.CashTypeId = cashType.CashTypeId;
                newCash.CreatedAt = DateTime.Now;
                newCash.EmployeeId = cash.EmployeeId;
                if (cash.Note != null && cash.Note.Trim().Length > 0)
                    newCash.Note = cash.Note;
                newCash.PaidAmount = cash.PaidAmount;
                newCash.PaidTargetId = cash.PaidTargetId;
                newCash.PaidTargetType = cash.PaidTargetType;
                newCash.PaymentMethod = (int)Const.PaymentType.CASH;
                newCash.Remain = 0;
                newCash.ShopId = id;
                newCash.Status = (int)Const.Status.NORMAL;
                newCash.Title = title;
                newCash.Total = cash.PaidAmount;
                newCash.Type = cashType.Type;
                newCash.UpdatedAt = DateTime.Now;
                newCash.IsManual = false;
                if (cash.TargetId != null)
                {
                    newCash.TargetId = cash.TargetId;
                    newCash.TargetType = cash.TargetType;
                }
                db.Cashes.Add(newCash);
                db.SaveChanges();
                //cash flow           

                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = id;
                cashFlow.TargetId = newCash.CashId;
                cashFlow.TargetType = "CASH";
                cashFlow.PaidTargetId = cash.PaidTargetId;
                cashFlow.PaidTargetType = cash.PaidTargetType;
                cashFlow.Type = (int)Const.CashFlowType.PAID;
                if (newCash.Type == (int)Const.CashType.INCOME)
                    cashFlow.Amount = cash.PaidAmount;
                else
                    cashFlow.Amount = 0 - cash.PaidAmount;
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                //var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).OrderByDescending(cf => cf.CashFlowId).Select(cf => cf.Debt).FirstOrDefault().HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).OrderByDescending(cf => cf.CashFlowId).Select(cf => cf.Debt).FirstOrDefault().Value : 0;
                cashFlow.Debt = debt - cash.PaidAmount;

                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();

                if (newCash.Note != null && newCash.Note.Trim().Length > 0)
                {
                    Note note = new Note();
                    note.Content = newCash.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = id;
                    note.Status = 0;
                    note.TargetId = newCash.CashId;
                    note.TargetType = "CASH";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        [HttpPost]
        [Route("api/cashes/{id}/createcash-vendor-return")]
        public IHttpActionResult createCashFromVendorReturn(int id, CreateCash cash)
        {
            var identity = (ClaimsIdentity)User.Identity;
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            CashType cashType = db.CashTypes.Where(ct => ct.CashTypeId == cash.CashTypeId).FirstOrDefault();
            if (cashType == null)
            {
                def.meta = new Meta(211, "invalid parameter - invalid cash type");
                return Ok(def);
            }
            //find target
            string title = "";
            if (cash.PaidTargetType == "VENDOR")
            {
                Vendor vendor = db.Vendors.Find(cash.PaidTargetId);
                if (vendor != null)
                {
                    title = cashType.Name + " cho nhà cung cấp " + vendor.VendorName;
                }
                else
                {
                    def.meta = new Meta(212, "invalid parameter - invalid paid target");
                    return Ok(def);
                }
            }

            if (cash.PaidAmount > 0)
            {
                Cash newCash = new Cash();
                newCash.CashCode = generateCashCode(cashType.CashTypeId, cashType.CashTypeCode);
                newCash.CashTypeId = cashType.CashTypeId;
                newCash.CreatedAt = DateTime.Now;
                newCash.EmployeeId = cash.EmployeeId;
                if (cash.Note != null && cash.Note.Trim().Length > 0)
                    newCash.Note = cash.Note;
                newCash.PaidAmount = cash.PaidAmount;
                newCash.PaidTargetId = cash.PaidTargetId;
                newCash.PaidTargetType = cash.PaidTargetType;
                newCash.PaymentMethod = (int)Const.PaymentType.CASH;
                newCash.Remain = 0;
                newCash.ShopId = id;
                newCash.Status = (int)Const.Status.NORMAL;
                newCash.Title = title;
                newCash.Total = cash.PaidAmount;
                newCash.Type = cashType.Type;
                newCash.UpdatedAt = DateTime.Now;
                newCash.IsManual = false;
                if (cash.TargetId != null)
                {
                    newCash.TargetId = cash.TargetId;
                    newCash.TargetType = cash.TargetType;
                }
                db.Cashes.Add(newCash);
                db.SaveChanges();
                //cash flow           

                CashFlow cashFlow = new CashFlow();
                cashFlow.ShopId = id;
                cashFlow.TargetId = newCash.CashId;
                cashFlow.TargetType = "CASH";
                cashFlow.PaidTargetId = cash.PaidTargetId;
                cashFlow.PaidTargetType = cash.PaidTargetType;
                cashFlow.Type = (int)Const.CashFlowType.PAID;
                cashFlow.Amount = cash.PaidAmount;
                //get lastest debt
                //lastest Debt = sum (amout)
                var debt = db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).HasValue ? db.CashFlows.Where(cf => cf.PaidTargetId == cash.PaidTargetId && cf.PaidTargetType == cash.PaidTargetType).Sum(cf => cf.Amount).Value : 0;
                cashFlow.Debt = debt + cashFlow.Amount;

                cashFlow.CreatedAt = DateTime.Now;
                db.CashFlows.Add(cashFlow);
                db.SaveChanges();

                if (newCash.Note != null && newCash.Note.Trim().Length > 0)
                {
                    Note note = new Note();
                    note.Content = newCash.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = id;
                    note.Status = 0;
                    note.TargetId = newCash.CashId;
                    note.TargetType = "CASH";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }
            }

            def.meta = new Meta(200, "Success");

            return Ok(def);
        }

        private string generateCashCode(int id, string prefix)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Cashes.Where(o => o.ShopId == ShopId && o.CashTypeId == id).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = prefix + code;
            return code;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CashExists(int id)
        {
            return db.Cashes.Count(e => e.CashId == id) > 0;
        }
    }
}