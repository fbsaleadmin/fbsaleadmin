﻿using SaleManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;


namespace SaleManager.Controllers
{



    //[Authorize]
    public class StatsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
        // GET: api/StatsMessages/
        // Trả về số lượng trả lời, người trả lời
        //type = 0 -> messeage
        //type = 1 -> comment       
        [HttpGet]
        [Route("api/stats/conversation")]
        public IHttpActionResult GetStatsMessage([FromUri]string startDate, [FromUri]string endDate, [FromUri] int shopID, [FromUri] int typeMess)
        {
            DefaultResponse def = new DefaultResponse(); ;
            int count = db.Conversations.Where(c => c.ShopId == shopID).Count();
            return Ok(count);
        }

        [HttpGet]
        [Route("api/stats/conversation")]
        public IHttpActionResult GetStatsMessage([FromUri] int shopID, [FromUri]string startDate, [FromUri]string endDate)
        {
            DateTime dStart = DateTime.ParseExact(startDate, "yyyy-MM-dd", null);
            DateTime dEnd = DateTime.ParseExact(endDate, "yyyy-MM-dd", null);
            Stats stat = new Stats();
            int countTotal = db.Conversations.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd).Count();
            int countMess = db.Conversations.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd && c.Type == 0).Count();
            int countComm = db.Conversations.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd && c.Type == 1).Count();
            int countPost = db.Posts.Where(c => c.ShopId == shopID && dStart < c.CreatedDate && c.CreatedDate < dEnd).Count();

            //Đếm số post có bình luận (được nhân)
            int countPostHaveComment = (from c in db.Conversations
                                        join com in db.Comments on c.ConversationId equals com.ConversationId
                                        where c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd
                                        select com.ConversationId).Distinct().Count();

            //Đếm số post có trả lời
            int countPostHaveReply = (from c in db.Conversations
                                      join com in db.Comments on c.ConversationId equals com.ConversationId
                                      where c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd
                                      join rep in db.Replies on com.CommentId equals rep.CommentId
                                      select com.CommentId).Distinct().Count();

            //Đếm số mess gửi đến (đc nhận)
            int countMessReceive = (from c in db.Conversations
                                    join com in db.Messages on c.ConversationId equals com.ConversationId
                                    where c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd
                                    select com.ConversationId).Distinct().Count();

            //Đếm số mess gửi đi(đc nhận)
            int countMessReply = (from c in db.Conversations
                                  join mess in db.Messages on c.ConversationId equals mess.ConversationId
                                  where c.ShopId == shopID && mess.EmployeeId != null && dStart < c.CreatedAt && c.CreatedAt < dEnd
                                  select mess.SenderId).Distinct().Count();


            stat.comment = countComm;
            stat.total = countTotal;
            stat.mess = countMess;
            stat.post = countPost;
            stat.postHaveComment = countPostHaveComment;
            stat.postHaveReply = countPostHaveReply;
            stat.messReceive = countMessReceive;
            stat.messReply = countMessReply;
            return Ok(stat);
        }


        [HttpGet]
        [Route("api/stats/order")]
        public IHttpActionResult GetStatsOrder([FromUri] int shopID, [FromUri]string startDate, [FromUri]string endDate)
        {
            DateTime dStart = DateTime.ParseExact(startDate, "yyyy-MM-dd", null);
            DateTime dEnd = DateTime.ParseExact(endDate, "yyyy-MM-dd", null);
            OrderStats stat = new OrderStats();
            var total = db.Orders.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd).Count();
            var totalProcess = db.Orders.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd).Count();
            var totalMoney = db.Orders.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd).Sum(x => x.Paid);
            var totalReceive = db.Orders.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd).Sum(x => x.CustomerPaid);
            var totalLost = totalMoney - totalReceive;
            var countPost = db.Posts.Where(c => c.ShopId == shopID && dStart < c.CreatedDate && c.CreatedDate < dEnd).Count();
            int totalFinish = db.Orders.Where(c => c.ShopId == shopID && dStart < c.CreatedAt && c.CreatedAt < dEnd && c.CustomerPaid == c.Paid && c.OrderStatus == 1).Count();


            totalLost = 0;
            stat.total = total;
            stat.totalMoney = totalMoney == null ? 0 : (decimal)totalMoney;
            stat.totalReceive = totalReceive == null ? 0 : (decimal)totalReceive;
            if (totalLost != null)
                stat.totalLost = (decimal)totalLost;
            stat.totalProcess = totalProcess;
            stat.totalFinish = totalFinish;

            return Ok(stat);
        }
    }
}
