﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.IO;
using System.Drawing;
using System.Web;
using System.Security.Claims;
using System.Configuration;

namespace SaleManager.Controllers
{
    
    public class ProductsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/products/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Product> products = db.Products;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    products = products.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    products = products.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    products = products.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    products = products.OrderBy("ProductId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }              
                def.data = products.Select(p => new ProductDTO()
                {
                    AllowDelivery = p.AllowDelivery,
                    AllowOrder = p.AllowOrder,
                    AllowSell = p.AllowSell,
                    AvailableBranchIds = p.AvailableBranchIds,
                    CategoryId = p.CategoryId,
                    CreatedAt = p.CreatedAt,
                    ManufactureId = p.ManufactureId,
                    ProductDesc = p.ProductDesc,
                    ProductId = p.ProductId,
                    ProductName = p.ProductName,
                    ShopId = p.ShopId,
                    Tag = p.Tag,
                    UpdatedAt = p.UpdatedAt,                   
                    Status = p.Status,
                    MinStock = p.MinStock,
                    StockLimit = p.StockLimit
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Products/5      
        public IHttpActionResult GetProduct(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            ProductDTO product = db.Products.Where(p => p.ProductId == id).Select(p => new ProductDTO()
            {
                AllowDelivery = p.AllowDelivery,
                AllowOrder = p.AllowOrder,
                AllowSell = p.AllowSell,
                AvailableBranchIds = p.AvailableBranchIds,
                CategoryId = p.CategoryId,
                CreatedAt = p.CreatedAt,
                ManufactureId = p.ManufactureId,
                ProductDesc = p.ProductDesc,
                ProductId = p.ProductId,
                ProductName = p.ProductName,
                ShopId = p.ShopId,
                Tag = p.Tag,
                UpdatedAt = p.UpdatedAt,              
                Status = p.Status,
                MinStock = p.MinStock,
                StockLimit = p.StockLimit
            }).FirstOrDefault();
            if (product == null || product.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = product;
            return Ok(def);
        }

        [Route("api/products/{id}/productattributes")]
        public IHttpActionResult GetProductAttribute(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Product product = db.Products.Find(id);
            if (product == null || product.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            var productAttributes = db.ProductAttributes.Where(pa => pa.ProductId == id).ToList();
            def.meta = new Meta(200, "Success");
            def.data = productAttributes;
            return Ok(def);
        }

        //upload

       

        // PUT: api/Products/5      
        public IHttpActionResult PutProduct(int id, ProductDTO product)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != product.ProductId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Product current = db.Products.Where(s => s.ProductId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.AllowDelivery = product.AllowDelivery;
                current.AllowOrder = product.AllowOrder;
                current.AllowSell = product.AllowSell;
                current.AvailableBranchIds = product.AvailableBranchIds;
                current.CategoryId = product.CategoryId;
                current.ManufactureId = product.ManufactureId;
                current.ProductDesc = product.ProductDesc;
                current.ProductName = product.ProductName;
                current.Tag = product.Tag;
                current.UpdatedAt = DateTime.Now;              
                current.Status = product.Status;
                current.MinStock = product.MinStock;
                current.StockLimit = product.StockLimit;

                db.Entry(current).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Products     
        public IHttpActionResult PostProduct(ProductDTO product)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(cl => cl.Type == "ShopId").Select(cl => cl.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Product c = new Product();
            c.AllowDelivery = product.AllowDelivery;
            c.AllowOrder = product.AllowOrder;
            c.AllowSell = product.AllowSell;
            c.AvailableBranchIds = product.AvailableBranchIds;
            c.CategoryId = product.CategoryId;
            c.CreatedAt = DateTime.Now;
            c.ManufactureId = product.ManufactureId;
            c.ProductDesc = product.ProductDesc;
            c.ShopId = ShopId;
            c.Tag = product.Tag;
            c.UpdatedAt = DateTime.Now;
            c.Status = 0;
            c.MinStock = product.MinStock;
            c.StockLimit = product.StockLimit;
         
            db.Products.Add(c);
            db.SaveChanges();
            product.ProductId = c.ProductId;

            def.meta = new Meta(200, "Success");
            def.data = product;

            return Ok(def);
        }

        public IHttpActionResult DeleteProduct(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Product product = db.Products.Find(id);
            if (product == null || product.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            //db.Products.Remove(product);
            product.Status = 99;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.ProductId == id) > 0;
        }
    }
}