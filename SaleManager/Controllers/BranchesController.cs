﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class BranchesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/branches/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Branch> branches = db.Branches;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    branches = branches.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    branches = branches.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    branches = branches.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    branches = branches.OrderBy("BranchId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = branches.Select(b => new BranchDTO()
                {
                    Address = b.Address,
                    BranchId = b.BranchId,
                    BranchName = b.BranchName,
                    CreatedAt = b.CreatedAt,
                    District = b.District,
                    IsMainBranch = b.IsMainBranch,
                    Phone = b.Phone,
                    Province = b.Province,
                    ShopId = b.ShopId,
                    Status = b.Status,
                    UpdatedAt = b.UpdatedAt
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Branches/5     
        public IHttpActionResult GetBranch(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            BranchDTO branch = db.Branches.Where(b => b.BranchId == id).Select(b => new BranchDTO()
            {
                Address = b.Address,
                BranchId = b.BranchId,
                BranchName = b.BranchName,
                CreatedAt = b.CreatedAt,
                District = b.District,
                IsMainBranch = b.IsMainBranch,
                Phone = b.Phone,
                Province = b.Province,
                ShopId = b.ShopId,
                Status = b.Status,
                UpdatedAt = b.UpdatedAt
            }).FirstOrDefault();
            if (branch == null || branch.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = branch;
            return Ok(def);
        }

        // PUT: api/Branches/5       
        public IHttpActionResult PutBranch(int id, BranchDTO branch)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != branch.BranchId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            Branch current = db.Branches.Where(s => s.BranchId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Address = branch.Address;
                current.BranchName = branch.BranchName;
                current.Phone = branch.Phone;
                current.UpdatedAt = DateTime.Now;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/Branches     
        public IHttpActionResult PostBranch(BranchDTO branch)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Branch b = new Branch();
            b.Address = branch.Address;
            b.BranchName = branch.BranchName;
            b.CreatedAt = DateTime.Now;
            b.District = branch.District;
            b.IsMainBranch = branch.IsMainBranch;
            b.Phone = branch.Phone;
            b.Province = branch.Province;
            b.ShopId = ShopId;
            b.Status = branch.Status;
            b.UpdatedAt = DateTime.Now;
            db.Branches.Add(b);
            db.SaveChanges();
            branch.BranchId = b.BranchId;
            //check product each branch
            var stocks = (from bs in db.BranchStocks
                          join _b in db.Branches on bs.BranchId equals _b.BranchId
                          where _b.IsMainBranch == true && _b.ShopId == ShopId
                          select bs).ToList();
            if (stocks.Count != null && stocks.Count > 0)
            {
                for (int i = 0; i < stocks.Count; i++)
                {
                    BranchStock newStock = new BranchStock();
                    newStock.BranchId = branch.BranchId;
                    newStock.ProductAttributeId = stocks[i].ProductAttributeId;
                    newStock.Stock = 0;
                    newStock.UpdatedAt = DateTime.Now;
                    db.BranchStocks.Add(newStock);
                    db.SaveChanges();
                }
            }

            def.meta = new Meta(200, "Success");
            def.data = branch;

            return Ok(def);
        }

        // DELETE: api/Branches/5     
        public IHttpActionResult DeleteBranch(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Branch branch = db.Branches.Find(id);
            if (branch == null || branch.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            //check stock valid
            var stocks = (from bs in db.BranchStocks
                          join _b in db.Branches on bs.BranchId equals _b.BranchId
                          where _b.BranchId == id
                          select bs).ToList();
            bool isValid = true;
            if (stocks != null && stocks.Count > 0)
            {
                for (int i = 0; i < stocks.Count; i++)
                {
                    if (stocks[i].Stock > 0)
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            if (isValid)
            {
                branch.Status = 99;
                //delete branch stock
                db.BranchStocks.RemoveRange(stocks);
                db.SaveChanges();
                def.meta = new Meta(200, "Success");
            }
            else
            {
                def.meta = new Meta(222, "Stock invalid");
            }
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BranchExists(int id)
        {
            return db.Branches.Count(e => e.BranchId == id) > 0;
        }
    }
}