﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ActionsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/actions/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Action> actions = db.Actions;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    actions = actions.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    actions = actions.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    actions = actions.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    actions = actions.OrderBy("ActionId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = actions.Select(a => new ActionDTO()
                {
                    ActionId = a.ActionId,
                    ActionName = a.ActionName,
                    ActionType = a.ActionType,
                    CreatedAt = a.CreatedAt,
                    EmployeeId = a.EmployeeId,
                    Result = a.Result,
                    ShopId = a.ShopId,
                    TargetId = a.TargetId,
                    TargetType = a.TargetType
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }


        [Route("api/actions/geteximsaction")]
        public IHttpActionResult geteximsaction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from e in db.Exims
                          join em in db.Employees on e.EmployeeId equals em.EmployeeId
                          join b in db.Branches on e.BranchId equals b.BranchId
                          join vd in db.Vendors on e.TargetId equals vd.VendorId
                          where e.ShopId == ShopId && e.Status != (int)Const.EximStatus.DELETED && e.Status != (int)Const.EximStatus.CANCELED
                          && e.Type == (int)Const.EximType.IMPORT && e.TargetType == "VENDOR"
                          && e.EximId == id
                          select new SaleManager.Data.Exim
                          {
                              Code = e.Code,
                              CreatetAt = e.CreatetAt,
                              DeliveryStatus = e.DeliveryStatus,
                              employee = new Data.Employee()
                              {
                                  EmployeeId = em.EmployeeId,
                                  EmployeeName = em.EmployeeName
                              },
                              EximId = e.EximId,
                              PaymentStatus = e.PaymentStatus,
                              Price = e.Price,
                              CustomerPaid = 0,
                              Discount = e.Discount,
                              Fee = e.Fee,
                              Status = e.Status,
                              TargetId = e.TargetId,
                              TargetType = e.TargetType,
                              Title = e.Title,
                              TotalPrice = e.TotalPrice,
                              Type = e.Type,
                              UpdatedAt = e.UpdatedAt,
                              target = new Data.Target()
                              {
                                  TargetCode = vd.VendorCode,
                                  TargetId = vd.VendorId,
                                  TargetName = vd.VendorName,
                                  SearchQuery = vd.SearchQuery
                              },
                              branch = new Data.Branch()
                              {
                                  Address = b.Address,
                                  BranchId = b.BranchId,
                                  BranchName = b.BranchName,
                                  Phone = b.Phone
                              }
                          }).FirstOrDefault();
            if (result != null)
            {
                int paidTargetId = (int)result.TargetId;
                int EximId = (int)result.EximId;

                result.CustomerPaid = db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == paidTargetId).Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == paidTargetId).Select(e => e.PaidAmount).Sum().Value : 0;
                result.TotalReturnProduct = db.EximProducts.Where(c => c.Exim.RelatedEximId == EximId).Select(e => e.Quantity).Sum().HasValue ? db.EximProducts.Where(c => c.Exim.RelatedEximId == EximId).Select(e => e.Quantity).Sum().Value : 0;
                //returnData[i].ReturnPaid = db.Exims.Where(c => c.RelatedEximId == EximId).Select(e => e.Price).Sum().HasValue ? db.Exims.Where(c => c.RelatedEximId == EximId).Select(e => e.Price).Sum().Value : 0;
                var returnpaid = from ex in db.Exims
                                 join ca in db.Cashes on ex.EximId equals ca.TargetId
                                 where ex.RelatedEximId == EximId && ex.Status != (int)Const.EximStatus.DELETED
                                 group ca by new
                                 {
                                     ca.TargetId
                                 } into gca
                                 select new
                                 {
                                     TargetId = gca.Key,
                                     Remain = gca.FirstOrDefault().Remain,
                                 };

                result.ReturnPaid = returnpaid.Sum(e => e.Remain);


                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }


        [Route("api/actions/getcusreturnsaction")]
        public IHttpActionResult getcusreturnsaction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from o in db.Orders
                          where o.ShopId == ShopId && o.Type == (int)Const.OrderType.RETURN
                          && o.OrderId == id
                          select new SaleManager.Data.Return
                          {
                              ReturnId = o.OrderId,
                              Code = o.OrderCode,
                              Price = o.OrderPrice,
                              Fee = o.Fee,
                              Status = o.OrderStatus,
                              CreatedAt = o.CreatedAt,
                              Note = o.Note,
                              TotalPrice = o.TotalPrice,
                              Paid = o.Paid,
                              branch = new SaleManager.Data.Branch()
                              {
                                  BranchId = o.BranchId,
                                  BranchName = o.Branch.BranchName
                              },
                              customer = new SaleManager.Data.Customer()
                              {
                                  CustomerCode = o.Customer.CustomerCode,
                                  CustomerId = o.Customer.CustomerId,
                                  CustomerName = o.Customer.CustomerName,
                                  FacebookUserId = o.Customer.FacebookUserId
                              },
                              employee = new SaleManager.Data.Employee()
                              {
                                  EmployeeId = o.Employee.EmployeeId,
                                  EmployeeName = o.Employee.EmployeeName
                              },
                              ProductCount = o.OrderProducts.Count()
                          }).FirstOrDefault();

            if (result != null)
            {
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [Route("api/actions/getimport_returnaction")]
        public IHttpActionResult getimport_returnaction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from o in db.Exims
                          where o.ShopId == ShopId && o.Type == (int)Const.EximType.EXPORT && o.TargetType == "VENDOR"
                         && o.EximId == id
                          select new SaleManager.Data.ImportReturn
                          {
                              EximId = o.EximId,
                              Code = o.Code,
                              Status = o.Status,
                              CreatedAt = o.CreatetAt,
                              TotalPrice = o.TotalPrice,
                              branch = new SaleManager.Data.Branch()
                              {
                                  BranchId = o.BranchId,
                                  BranchName = o.Branch.BranchName
                              },
                              employee = new SaleManager.Data.Employee()
                              {
                                  EmployeeId = o.Employee.EmployeeId,
                                  EmployeeName = o.Employee.EmployeeName
                              },
                              ProductCount = o.EximProducts.Count(),
                              TargetId = o.TargetId
                          }).FirstOrDefault();


            if (result != null)
            {
                //target
                var targetId = result.TargetId;
                if (targetId != null)
                {
                    result.vendor = db.Vendors.Where(d => d.VendorId == targetId).Select(d => new SaleManager.Data.Vendor()
                    {
                        VendorId = d.VendorId,
                        VendorCode = d.VendorCode,
                        VendorName = d.VendorName
                    }).FirstOrDefault();
                }
                //paid amout
                var EximId = result.EximId;
                result.PaidAmount = db.Cashes.Where(c => c.TargetId == EximId && c.PaidTargetId == targetId).Sum(c => c.PaidAmount).Value;

                //
                def.meta = new Meta(200, "Success");
                def.data = result;

                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [Route("api/actions/getorderaction")]
        public IHttpActionResult getorderaction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from o in db.Orders
                          where o.ShopId == ShopId && o.Type == (int)Const.OrderType.SALE
                         && o.OrderId == id
                          select new SaleManager.Data.Order
                          {
                              CreatedAt = o.CreatedAt,
                              CustomerPaid = o.CustomerPaid,
                              DeliveryStatus = o.DeliveryStatus,
                              Discount = o.DiscountPrice,
                              Note = o.Note,
                              OrderAddress = o.OrderAddress,
                              OrderCode = o.OrderCode,
                              OrderId = o.OrderId,
                              OrderPhone = o.OrderPhone,
                              PaymentStatus = o.PaymentStatus,
                              OrderStatus = o.OrderStatus,
                              TotalPrice = o.TotalPrice,
                              //CashesPaid=o.TotalPrice-o.CustomerPaid,
                              //ReturnPaid=0,
                              OrderPrice = o.OrderPrice,
                              DeliveryPrice = o.DeliveryPrice,
                              MoneyCollect = o.MoneyCollect,
                              branch = new SaleManager.Data.Branch()
                              {
                                  BranchId = o.BranchId,
                                  BranchName = o.Branch.BranchName
                              },
                              customer = new SaleManager.Data.Customer()
                              {
                                  CustomerCode = o.Customer.CustomerCode,
                                  CustomerId = o.Customer.CustomerId,
                                  CustomerName = o.Customer.CustomerName,
                                  FacebookUserId = o.Customer.FacebookUserId
                              },
                              employee = new SaleManager.Data.Employee()
                              {
                                  EmployeeId = o.Employee.EmployeeId,
                                  EmployeeName = o.Employee.EmployeeName
                              },
                              saleEmployee = new SaleManager.Data.Employee()
                              {
                                  EmployeeId = o.SaleEmployee.EmployeeId,
                                  EmployeeName = o.SaleEmployee.EmployeeName
                              },
                              ProductCount = o.OrderProducts.Count()
                          }).FirstOrDefault();



            if (result != null)
            {



                var orderId = result.OrderId;
                result.ReturnPaid = db.Orders.Where(e => e.RelatedOrderId == orderId).Select(e => e.OrderPrice).Sum().HasValue ? db.Orders.Where(e => e.RelatedOrderId == orderId).Select(e => e.OrderPrice).Sum().Value : 0;
                result.CashesPaid = db.Cashes.Where(e => e.TargetId == orderId && e.TargetType == "ORDER").Select(e => e.PaidAmount).Sum().HasValue ? db.Cashes.Where(e => e.TargetId == orderId && e.TargetType == "ORDER").Select(e => e.PaidAmount).Sum().Value : result.CustomerPaid;

                var deliveryVendorId = result.DeliveryVendorId;
                if (deliveryVendorId != null)
                {
                    result.deliveryVendor = db.DeliveryVendors.Where(d => d.DeliveryVendorId == deliveryVendorId).Select(d => new SaleManager.Data.DeliveryVendor()
                    {
                        DeliveryVendorId = d.DeliveryVendorId,
                        VendorCode = d.VendorCode,
                        VendorName = d.VendorName
                    }).FirstOrDefault();
                }

                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [Route("api/actions/getvendoraction")]
        public IHttpActionResult getcendoraction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from v in db.Vendors
                              //join p in db.Provinces on v.ProvinceId equals p.ProvinceId
                              //join d in db.Districts on v.DistrictId equals d.DistrictId
                          from d in db.Districts.Where(d => d.DistrictId == v.DistrictId).DefaultIfEmpty()
                          from p in db.Provinces.Where(p => p.ProvinceId == v.ProvinceId).DefaultIfEmpty()
                          where v.ShopId == ShopId
                         && v.VendorId == id
                          select new SaleManager.Models.ShopVendor
                          {
                              Address = v.Address,
                              Company = v.Company,
                              CreatedAt = v.CreatedAt,
                              District = new ShopDistrict()
                              {
                                  DistrictId = d != null ? d.DistrictId : -1,
                                  Name = d != null ? d.Name : "",
                                  ProvinceId = d != null ? d.ProvinceId : -1
                              },
                              Email = v.Email,
                              Note = v.Note,
                              Phone = v.Phone,
                              Province = new ShopProvince()
                              {
                                  ProvinceId = p != null ? p.ProvinceId : -1,
                                  Name = p != null ? p.Name : ""
                              },
                              ShopId = v.ShopId,
                              VendorCode = v.VendorCode,
                              VendorId = v.VendorId,
                              VendorName = v.VendorName,
                              UpdatedAt = v.UpdatedAt,
                              SearchQuery = v.SearchQuery
                          }
                         ).FirstOrDefault();
            if (result != null)
            {
                int VendorId = (int)result.VendorId;
                result.TotalOrder = db.Exims.Where(ip => ip.TargetType == "VENDOR" && ip.TargetId == VendorId && ip.Type == 0).Count();
                result.TotalOrderPrice = db.Exims.Where(ip => ip.TargetType == "VENDOR" && ip.TargetId == VendorId && ip.Type == 0).Sum(c => c.TotalPrice).HasValue ? db.Exims.Where(ip => ip.TargetType == "VENDOR" && ip.TargetId == VendorId && ip.Type == 0).Sum(c => c.TotalPrice).Value : 0;
                result.TotalPaid = db.Cashes.Where(c => c.PaidTargetType == "VENDOR" && c.PaidTargetId == VendorId && c.CashType.Type == 0).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() != null ? db.Cashes.Where(c => c.PaidTargetType == "VENDOR" && c.PaidTargetId == VendorId && c.CashType.Type == 0).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() : 0;
                result.TotalDebt = db.CashFlows.Where(cf => cf.PaidTargetId == VendorId && cf.PaidTargetType == "VENDOR").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault() != null ? db.CashFlows.Where(cf => cf.PaidTargetId == VendorId && cf.PaidTargetType == "VENDOR").OrderByDescending(cf => cf.CreatedAt).FirstOrDefault().Debt : 0;
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [Route("api/actions/getdeliveryvendoraction")]
        public IHttpActionResult getdeliveryvendoraction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from v in db.DeliveryVendors
                              //join d in db.Districts on v.DistrictId equals d.DistrictId
                              //join p in db.Provinces on v.ProvinceId equals p.ProvinceId
                          from d in db.Districts.Where(d => d.DistrictId == v.DistrictId).DefaultIfEmpty()
                          from p in db.Provinces.Where(p => p.ProvinceId == v.ProvinceId).DefaultIfEmpty()
                          where v.ShopId == ShopId && v.DeliveryVendorId == id
                          select new SaleManager.Models.ShopDeliveryVendor
                          {
                              Address = v.Address,
                              CreatedAt = v.CreatedAt,
                              District = new ShopDistrict()
                              {
                                  DistrictId = d != null ? d.DistrictId : -1,
                                  Name = d != null ? d.Name : "",
                                  ProvinceId = d != null ? d.ProvinceId : -1
                              },
                              Phone = v.Phone,
                              Province = new ShopProvince()
                              {
                                  ProvinceId = p != null ? p.ProvinceId : -1,
                                  Name = p != null ? p.Name : ""
                              },
                              ShopId = v.ShopId,
                              VendorCode = v.VendorCode,
                              DeliveryVendorId = v.DeliveryVendorId,
                              VendorName = v.VendorName,
                              UpdatedAt = v.UpdatedAt,
                              SearchQuery = v.SearchQuery,
                              Note = v.Note
                          }
                          ).FirstOrDefault();

            if (result != null)
            {



                int DeliveryVendorId = (int)result.DeliveryVendorId;
                result.TotalDebt = db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(d => d.Total).DefaultIfEmpty().Sum() != null ? (double)db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(d => d.Total).DefaultIfEmpty().Sum() : 0;
                result.TotalPaid = db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() != null ? (double)db.Cashes.Where(c => c.PaidTargetType == "DELIVERY_VENDOR" && c.PaidTargetId == DeliveryVendorId && c.CashType.Type == (int)Const.CashType.INCOME).Select(c => c.PaidAmount).DefaultIfEmpty().Sum() : 0;

                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [Route("api/actions/getproductaction")]
        public IHttpActionResult getproductaction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            var result = (from p in db.Products
                          join pa in db.ProductAttributes on p.ProductId equals pa.ProductId
                          where p.ShopId == ShopId && pa.ProductAttributeId == id
                          select new ShopProduct
                          {
                              category = new Models.Category() { CategoryId = p.Category.CategoryId, CategoryName = p.Category.CategoryName, CategoryParentId = p.Category.CategoryParentId },
                              ProductAttributeId = pa.ProductAttributeId,
                              ProductCode = pa.ProductCode,
                              ProductId = p.ProductId,
                              ShopId = p.ShopId,
                              SubName = pa.SubName,
                              AllowDelivery = p.AllowDelivery,
                              AllowOrder = p.AllowOrder,
                              AllowSell = p.AllowSell,
                              Code = pa.Code,
                              Discount = pa.Discount,
                              images = p.ProductImages.Select(i => new Data.PI() { Alt = i.Alt, Path = i.Path, ProductImageId = i.ProductImageId, RelativePath = i.RelativePath }).ToList(),
                              NetWeight = pa.NetWeight,
                              PAStatus = pa.Status,
                              ProductDesc = p.ProductDesc,
                              ProductName = p.ProductName,
                              PStatus = p.Status,
                              Tag = p.Tag,
                              ManufactureId = p.ManufactureId,
                              MinStock = p.MinStock,
                              StockLimit = p.StockLimit,
                              SearchQuery = pa.SearchQuery,
                              CreatedAt = pa.CreatedAt,
                              UpdatedAt = pa.UpdatedAt
                          }).FirstOrDefault();




            int PaId = (int)result.ProductAttributeId;
            result.stock = db.BranchStocks.Join(db.Branches, stock => stock.BranchId, branc => branc.BranchId,
                    (stock, branc) => new { Stock = stock, Branc = branc })
                    .Where(b => b.Stock.ProductAttributeId == PaId && b.Branc.Status != (int)Const.ProductStatus.DELETED).Select(b => new ShopProductStock()
                    {
                        BranchId = b.Stock.BranchId,
                        Stock = b.Stock.Stock,
                        BranchName = b.Branc.BranchName
                    }).ToList();

            result.prices = (from pp in db.ProductPrices
                             join pu in db.ProductUnits on pp.ProductUnitId equals pu.ProductUnitId
                             where pp.ProductAttributeId == PaId && pp.IsCurrent == true && pu.Status != (int)Const.ProductStatus.DELETED
                             orderby pp.SalePrice descending
                             select new ShopProductPrice()
                             {
                                 ProductPriceId = pp.ProductPriceId,
                                 Unit = pp.ProductUnit.Unit,
                                 Quantity = pp.ProductUnit.Quantity,
                                 OriginPrice = pp.OriginPrice,
                                 AverageImportPrice = pp.OriginPrice,
                                 SalePrice = pp.SalePrice,
                                 ProductUnitId = pp.ProductUnitId
                             }).ToList();

            foreach (var itemP in result.prices)
            {
                var importPrice = (from ep in db.EximProducts
                                   where ep.ProductAttributeId == PaId && ep.ProductUnitId == itemP.ProductUnitId
                                   && ep.Exim.Type == (int)Const.EximType.IMPORT && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                   && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                   orderby ep.CreatedAt descending
                                   select new
                                   {
                                       Price = ep.Price,
                                       Quantity = ep.Quantity
                                   }).AsEnumerable();

                var exportPrice = (from ep in db.EximProducts
                                   where ep.ProductAttributeId == PaId && ep.ProductUnitId == itemP.ProductUnitId
                                   && ep.Exim.TargetType == "VENDOR" && ep.Exim.Type == (int)Const.EximType.EXPORT && ep.Exim.Status != (int)Const.EximStatus.TEMP
                                   && ep.Exim.Status != (int)Const.EximStatus.DELETED && ep.Exim.Status != (int)Const.EximStatus.CANCELED
                                   orderby ep.CreatedAt descending
                                   select new
                                   {
                                       Price = ep.Price,
                                       Quantity = ep.Quantity
                                   }).AsEnumerable();

                importPrice = importPrice.ToList();
                if ((importPrice == null || importPrice.Count() == 0))
                {
                    itemP.AverageImportPrice = 0;
                }
                else
                {
                    itemP.OriginPrice = importPrice.FirstOrDefault().Price;
                    double sumImportPrice = 0;
                    int countImportPrice = 0;
                    foreach (var itemIP in importPrice)
                    {
                        if (itemIP.Price != null)
                        {
                            int quantityExport = exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().HasValue ? exportPrice.Where(e => e.Price == itemIP.Price).Select(e => e.Quantity).Sum().Value : 0;
                            int quantity = (int)itemIP.Quantity - quantityExport;
                            countImportPrice += quantity;//(int)itemIP.Quantity;
                            sumImportPrice += (double)(itemIP.Price * quantity);
                        }
                    }
                    if (countImportPrice != 0)
                    {
                        itemP.AverageImportPrice = Math.Round((decimal)(sumImportPrice / (int)countImportPrice), 0);
                    }
                    else
                    {
                        itemP.AverageImportPrice = 0;
                    }
                }
            }

            decimal originPrice = 0;
            foreach (var itemP in result.prices)
            {
                if (itemP.Quantity == 1)
                {
                    originPrice = (decimal)itemP.OriginPrice;
                }
            }

            result.SumStock = (int)result.stock.Select(s => s.Stock).Sum();
            result.StockPrice = result.SumStock * originPrice;

            if (result != null)
            {
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Actions/5       
        public IHttpActionResult GetAction(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ActionDTO action = db.Actions.Where(a => a.ActionId == id).Select(a => new ActionDTO()
            {
                ActionId = a.ActionId,
                ActionName = a.ActionName,
                ActionType = a.ActionType,
                CreatedAt = a.CreatedAt,
                EmployeeId = a.EmployeeId,
                Result = a.Result,
                ShopId = a.ShopId,
                TargetId = a.TargetId,
                TargetType = a.TargetType
            }).FirstOrDefault();
            //check if claim not correct          
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            if (action == null || !action.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = action;
            return Ok(def);
        }

        // PUT: api/Actions/5        
        public IHttpActionResult PutAction(int id, ActionDTO action)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != action.ActionId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            Action current = db.Actions.Where(s => s.ActionId == id).FirstOrDefault();
            if (current == null || !current.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.ActionId = (int)action.ActionId;
                current.ActionName = action.ActionName;
                current.ActionType = action.ActionType;
                current.EmployeeId = action.EmployeeId;
                current.Result = action.Result;
                current.ShopId = action.ShopId;
                current.TargetId = action.TargetId;
                current.TargetType = action.TargetType;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActionExists(int id)
        {
            return db.Actions.Count(e => e.ActionId == id) > 0;
        }
    }
}