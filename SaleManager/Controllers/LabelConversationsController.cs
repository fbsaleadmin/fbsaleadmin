﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class LabelConversationsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities(); 

        // POST: api/LabelConversations      
        public IHttpActionResult PostLabelConversation(LabelConversationDTO labelConversation)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            //check exist
            var countExist = db.LabelConversations.Where(lc => lc.LabelId == labelConversation.LabelId && lc.ConversationId == labelConversation.ConversationId).Count();
            if (countExist > 0)
            {
                def.meta = new Meta(200, "Success");
                return Ok(def);
            }
            LabelConversation c = new LabelConversation();
            c.ConversationId = labelConversation.ConversationId;
            c.LabelId = labelConversation.LabelId;           
            db.LabelConversations.Add(c);
            db.SaveChanges();
            labelConversation.LabelConversationId = c.LabelConversationId;

            def.meta = new Meta(200, "Success");
            def.data = labelConversation;

            return Ok(def);
        }

        // DELETE: api/LabelConversations/5   
        public IHttpActionResult DeleteLabelConversation(int id)
        {
            DefaultResponse def = new DefaultResponse();
            LabelConversation labelConversation = db.LabelConversations.Find(id);
            if (labelConversation == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            //delete message belong to conversation          
            db.LabelConversations.Remove(labelConversation);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LabelConversationExists(int id)
        {
            return db.LabelConversations.Count(e => e.LabelConversationId == id) > 0;
        }
    }
}