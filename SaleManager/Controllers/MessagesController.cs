﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class MessagesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/Messages
        //public IHttpActionResult GetMessages()
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    def.meta = new Meta(200, "Success");
        //    def.data = db.Messages.Select(m => new MessageDTO() { 
        //        Content = m.Content,
        //        ConversationId = m.ConversationId,
        //        CreatedAt = m.CreatedAt,
        //        FacebookMessageId = m.FacebookMessageId,
        //        FacebookStatus = m.FacebookStatus,
        //        IsBotMessage = m.IsBotMessage,
        //        MessageId = m.MessageId,
        //        RecipientId = m.RecipientId,
        //        RecipientName = m.RecipientName,
        //        SenderId = m.SenderId,
        //        SenderName = m.SenderName,
        //        Sequence = m.Sequence,
        //        Status = m.Status,
        //        Subject = m.Subject,
        //        UpdatedAt = m.UpdatedAt,
        //        EmployeeId = m.EmployeeId,
        //        HasAttachment = m.HasAttachment
        //    });
        //    return Ok(def);
        //}

        [Route("api/messages/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {  
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Message> messages = db.Messages;
                if (paging.query != null)
                {
                    paging.query = HttpUtility.UrlDecode(paging.query);
                    messages = messages.Where(paging.query);
                }
                else
                {
                    def.meta = new Meta(404, "Not found");
                    return Ok(def);
                }
                int count = messages.Count();
                if (paging.order_by != null)
                {
                    messages = messages.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    messages = messages.OrderBy("UpdatedAt desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                var returnResult = messages.Select(m => new ShopMessage()
                {
                    Content = m.Content,
                    ConversationId = m.ConversationId,
                    CreatedAt = m.CreatedAt,
                    FacebookMessageId = m.FacebookMessageId,
                    FacebookStatus = m.FacebookStatus,
                    IsBotMessage = m.IsBotMessage,
                    MessageId = m.MessageId,
                    RecipientId = m.RecipientId,
                    RecipientName = m.RecipientName,
                    SenderId = m.SenderId,
                    SenderName = m.SenderName,
                    Sequence = m.Sequence,
                    Status = m.Status,
                    Subject = m.Subject,
                    UpdatedAt = m.UpdatedAt,
                    EmployeeId = m.EmployeeId,
                    HasAttachment = m.HasAttachment,
                    attachments = db.Attachments.Where(a => a.MessageId == m.MessageId).Select(a => new ShopAttachment()
                    {
                        AttachmentId = a.AttachmentId,
                        CreatedAt = a.CreatedAt,
                        Type = a.Type,
                        Url = a.Url,
                        OwnUrl = a.OwnUrl
                    }).ToList()
                }).ToList();
                for (int i = 0; i < returnResult.Count; i++)
                {                   
                    returnResult[i].UnixTimestamp = Utils.DateTimeToUnixTimeStamp(returnResult[i].UpdatedAt.Value);                    
                }
                def.data = returnResult;
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Messages/5     
        public IHttpActionResult GetMessage(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ShopMessage message = db.Messages.Where(c => c.MessageId == id).Select(m => new ShopMessage()
            {
                Content = m.Content,
                ConversationId = m.ConversationId,
                CreatedAt = m.CreatedAt,
                FacebookMessageId = m.FacebookMessageId,
                FacebookStatus = m.FacebookStatus,
                IsBotMessage = m.IsBotMessage,
                MessageId = m.MessageId,
                RecipientId = m.RecipientId,
                RecipientName = m.RecipientName,
                SenderId = m.SenderId,
                SenderName = m.SenderName,
                Sequence = m.Sequence,
                Status = m.Status,
                Subject = m.Subject,
                UpdatedAt = m.UpdatedAt,
                EmployeeId = m.EmployeeId,
                HasAttachment = m.HasAttachment,
                attachments = db.Attachments.Where(a => a.MessageId == m.MessageId).Select(a => new ShopAttachment()
                {
                    AttachmentId = a.AttachmentId,
                    CreatedAt = a.CreatedAt,
                    Type = a.Type,
                    Url = a.Url,
                    OwnUrl = a.OwnUrl
                }).ToList()
            }).FirstOrDefault();
            if (message == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = message;
            return Ok(def);
        }

        // PUT: api/Messages/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMessage(int id, MessageDTO message)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != message.MessageId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Message current = db.Messages.Where(c => c.MessageId == id).FirstOrDefault();
            if (current == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Content = message.Content;
                current.FacebookStatus = message.FacebookStatus;
                current.IsBotMessage = message.IsBotMessage;
                current.RecipientName = message.RecipientName;
                current.SenderName = message.SenderName;
                current.Sequence = message.Sequence;
                current.Status = message.Status;
                current.Subject = message.Subject;
                current.UpdatedAt = DateTime.Now;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            } 
        }

        // POST: api/Messages
        [ResponseType(typeof(Message))]
        public IHttpActionResult PostMessage(MessageDTO message)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Message c = new Message();
            c.Content = message.Content;
            c.ConversationId = message.ConversationId;
            c.CreatedAt = DateTime.Now;
            c.FacebookMessageId = message.FacebookMessageId;
            c.FacebookStatus = message.FacebookStatus;
            c.IsBotMessage = message.IsBotMessage;
            c.RecipientId = message.RecipientId;
            c.RecipientName = message.RecipientName;
            c.SenderId = message.SenderId;
            c.SenderName = message.SenderName;
            c.Sequence = message.Sequence;
            c.Status = message.Status;
            c.Subject = message.Subject;
            c.UpdatedAt = DateTime.Now;
            c.EmployeeId = message.EmployeeId;
            c.HasAttachment = message.HasAttachment;
            db.Messages.Add(c);
            db.SaveChanges();
            message.MessageId = c.MessageId;

            def.meta = new Meta(200, "Success");
            def.data = message;

            return Ok(def);
        }

        // DELETE: api/Messages/5
        [ResponseType(typeof(Message))]
        public IHttpActionResult DeleteMessage(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            //delete message belong to conversation
            db.Attachments.RemoveRange(message.Attachments);
            db.Messages.Remove(message);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MessageExists(int id)
        {
            return db.Messages.Count(e => e.MessageId == id) > 0;
        }
    }
}