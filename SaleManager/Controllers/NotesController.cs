﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    public class NotesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/notes/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Note> notes = db.Notes;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    notes = notes.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    notes = notes.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    notes = notes.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    notes = notes.OrderBy("NoteId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = notes.Select(c => new NoteDTO()
                {
                    Content = c.Content,
                    CreatedAt = c.CreatedAt,
                    NoteId = c.NoteId,
                    ShopId = c.ShopId,
                    Status = c.Status,
                    TargetId = c.TargetId,
                    TargetType = c.TargetType
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Notes/5      
        public IHttpActionResult GetNote(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            NoteDTO note = db.Notes.Where(c => c.NoteId == id).Select(c => new NoteDTO()
            {
                Content = c.Content,
                CreatedAt = c.CreatedAt,
                NoteId = c.NoteId,
                ShopId = c.ShopId,
                Status = c.Status,
                TargetId = c.TargetId,
                TargetType = c.TargetType
            }).FirstOrDefault();
            if (note == null || note.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = note;
            return Ok(def);
        }

        // PUT: api/Notes/5        
        public IHttpActionResult PutNote(int id, NoteDTO note)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != note.NoteId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Note current = db.Notes.Where(s => s.NoteId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Content = note.Content;
                current.Status = note.Status;
                current.TargetId = note.TargetId;
                current.TargetType = note.TargetType;                
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/Notes     
        public IHttpActionResult PostNote(NoteDTO note)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Note n = new Note();
            n.Content = note.Content;
            n.CreatedAt = DateTime.Now;
            n.ShopId = ShopId;
            n.Status = note.Status;
            n.TargetId = note.TargetId;
            n.TargetType = note.TargetType;            
            
            db.Notes.Add(n);
            db.SaveChanges();
            note.NoteId = n.NoteId;

            def.meta = new Meta(200, "Success");
            def.data = note;

            return Ok(def);
        }

        // DELETE: api/Notes/5     
        public IHttpActionResult DeleteNote(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Note note = db.Notes.Find(id);
            if (note == null || note.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            note.Status = 99;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoteExists(int id)
        {
            return db.Notes.Count(e => e.NoteId == id) > 0;
        }
    }
}