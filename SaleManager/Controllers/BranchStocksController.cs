﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class BranchStocksController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        //[Route("api/branchestocks/GetByPage")]
        //public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (paging != null)
        //    {
        //        def.meta = new Meta(200, "Success");
        //        IQueryable<BranchStock> branchStocks;
        //        if (paging.order_by != null)
        //        {
        //            branchStocks = db.BranchStocks.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        else
        //        {
        //            branchStocks = db.BranchStocks.OrderBy("BranchStockId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        if (paging.query != null)
        //            branchStocks = branchStocks.Where(HttpUtility.UrlDecode(paging.query));
        //        def.data = branchStocks.Select(b => new BranchStockDTO()
        //        {
        //            BranchId = b.BranchId,
        //            BranchStockId = b.BranchStockId,
        //            ProductAttributeId = b.ProductAttributeId,
        //            Stock = b.Stock,
        //            StockLimit = b.StockLimit,
        //            UpdatedAt = b.UpdatedAt
        //        });
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //}

        //// GET: api/BranchStocks/5      
        //public IHttpActionResult GetBranchStock(int id)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    BranchStockDTO branchStocks = db.BranchStocks.Where(b => b.BranchStockId == id).Select(b => new BranchStockDTO()
        //    {
        //        BranchId = b.BranchId,
        //        BranchStockId = b.BranchStockId,
        //        ProductAttributeId = b.ProductAttributeId,
        //        Stock = b.Stock,
        //        StockLimit = b.StockLimit,
        //        UpdatedAt = b.UpdatedAt
        //    }).FirstOrDefault();
        //    if (branchStocks == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    def.meta = new Meta(200, "Success");
        //    def.data = branchStocks;
        //    return Ok(def);
        //}

        //// PUT: api/BranchStocks/5      
        //public IHttpActionResult PutBranchStock(int id, BranchStockDTO branchStock)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    if (id != branchStock.BranchStockId)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //    BranchStock current = db.BranchStocks.Where(s => s.BranchStockId == id).FirstOrDefault();
        //    if (current == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        current.Stock = branchStock.Stock;
        //        current.StockLimit = branchStock.StockLimit;
        //        current.UpdatedAt = DateTime.Now;               
        //        db.Entry(current).State = EntityState.Modified;
        //        try
        //        {
        //            db.SaveChanges();
        //            def.meta = new Meta(200, "Success");
        //            return Ok(def);
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            def.meta = new Meta(500, "Internal Server Error");
        //            return Ok(def);
        //        }
        //    }  
        //}

        //// POST: api/BranchStocks       
        //public IHttpActionResult PostBranchStock(BranchStockDTO branchStock)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    if (!ModelState.IsValid)
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }

        //    BranchStock bs = new BranchStock();
        //    bs.BranchId = branchStock.BranchId;
        //    bs.ProductAttributeId = branchStock.ProductAttributeId;
        //    bs.Stock = branchStock.Stock;
        //    bs.StockLimit = branchStock.StockLimit;
        //    bs.UpdatedAt = DateTime.Now;
        
        //    db.BranchStocks.Add(bs);
        //    db.SaveChanges();
        //    branchStock.BranchStockId = bs.BranchStockId;

        //    def.meta = new Meta(200, "Success");
        //    def.data = branchStock;

        //    return Ok(def);
        //}

        //// DELETE: api/BranchStocks/5     
        //public IHttpActionResult DeleteBranchStock(int id)
        //{
        //    DefaultResponse def = new DefaultResponse();
        //    BranchStock branchStock = db.BranchStocks.Find(id);
        //    if (branchStock == null)
        //    {
        //        def.meta = new Meta(404, "Not Found");
        //        return Ok(def);
        //    }

        //    db.BranchStocks.Remove(branchStock);
        //    db.SaveChanges();

        //    def.meta = new Meta(200, "Success");
        //    return Ok(def);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BranchStockExists(int id)
        {
            return db.BranchStocks.Count(e => e.BranchStockId == id) > 0;
        }
    }
}