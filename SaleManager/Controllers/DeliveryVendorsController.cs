﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class DeliveryVendorsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/deliveryvendors/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<DeliveryVendor> deliveryVendors = db.DeliveryVendors;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    deliveryVendors = deliveryVendors.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    deliveryVendors = deliveryVendors.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    deliveryVendors = deliveryVendors.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    deliveryVendors = deliveryVendors.OrderBy("DeliveryVendorId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.data = deliveryVendors.Select(d => new DeliveryVendorDTO()
                {
                    Address = d.Address,
                    CreatedAt = d.CreatedAt,
                    DeliveryVendorId = d.DeliveryVendorId,
                    DistrictId = d.DistrictId,
                    Phone = d.Phone,
                    ProvinceId = d.ProvinceId,
                    Status = d.Status,
                    UpdatedAt = d.UpdatedAt,
                    VendorName = d.VendorName,
                    ShopId = d.ShopId,
                    VendorCode = d.VendorCode,
                    Note = d.Note,
                    SearchQuery = d.SearchQuery
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/DeliveryVendors/5       
        public IHttpActionResult GetDeliveryVendor(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            DeliveryVendorDTO deliveryVendor = db.DeliveryVendors.Where(o => o.DeliveryVendorId == id).Select(d => new DeliveryVendorDTO()
            {
                Address = d.Address,
                CreatedAt = d.CreatedAt,
                DeliveryVendorId = d.DeliveryVendorId,
                DistrictId = d.DistrictId,
                Phone = d.Phone,
                ProvinceId = d.ProvinceId,
                Status = d.Status,
                UpdatedAt = d.UpdatedAt,
                VendorName = d.VendorName,
                ShopId = d.ShopId,
                VendorCode = d.VendorCode,
                Note = d.Note,
                SearchQuery = d.SearchQuery
            }).FirstOrDefault();
            if (deliveryVendor == null || deliveryVendor.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = deliveryVendor;
            return Ok(def);
        }

        // PUT: api/DeliveryVendors/5        
        public IHttpActionResult PutDeliveryVendor(int id, DeliveryVendorDTO deliveryVendor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != deliveryVendor.DeliveryVendorId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            DeliveryVendor current = db.DeliveryVendors.Where(s => s.DeliveryVendorId == id).FirstOrDefault();
            string lastNote = current.Note != null ? current.Note.Trim() : "";
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.VendorName = deliveryVendor.VendorName;
                current.Address = deliveryVendor.Address;
                current.DistrictId = deliveryVendor.DistrictId;
                current.Phone = deliveryVendor.Phone;
                current.ProvinceId = deliveryVendor.ProvinceId;
                current.UpdatedAt = DateTime.Now;
                deliveryVendor.UpdatedAt = DateTime.Now;
                //if (current.Note != null && current.Note.Trim().Length > 0 && !lastNote.Equals(current.Note.Trim()))
                current.Note = deliveryVendor.Note;

                current.SearchQuery = Utils.unsignString(current.VendorName + " " + current.VendorCode);
                if (current.Note != null && current.Note.Trim().Length > 0 && !lastNote.Equals(current.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = current.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.DeliveryVendorId;
                    note.TargetType = "DELIVERY_VENDOR";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        // POST: api/DeliveryVendors       
        public IHttpActionResult PostDeliveryVendor(DeliveryVendorDTO deliveryVendor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            DeliveryVendor d = new DeliveryVendor();
            d.Address = deliveryVendor.Address;
            d.CreatedAt = DateTime.Now;
            d.DistrictId = deliveryVendor.DistrictId;
            d.Phone = deliveryVendor.Phone;
            d.ProvinceId = deliveryVendor.ProvinceId;
            d.Status = 0;
            d.UpdatedAt = DateTime.Now;
            d.VendorName = deliveryVendor.VendorName;
            d.ShopId = ShopId;
            d.Note = deliveryVendor.Note != null ? deliveryVendor.Note : "";
            string code = generateCode();
            if (code != deliveryVendor.VendorCode)
                d.VendorCode = deliveryVendor.VendorCode;
            else
                d.VendorCode = code;
            d.SearchQuery = Utils.unsignString(d.VendorName + " " + d.VendorCode);
            db.DeliveryVendors.Add(d);
            db.SaveChanges();
            deliveryVendor.DeliveryVendorId = d.DeliveryVendorId;

            if (d.Note != null && d.Note.Trim().Length > 0)
            {
                Note note = new Note();
                note.Content = d.Note;
                note.CreatedAt = DateTime.Now;
                note.ShopId = ShopId;
                note.Status = 0;
                note.TargetId = d.DeliveryVendorId;
                note.TargetType = "DELIVERY_VENDOR";
                db.Notes.Add(note);
                db.SaveChanges();
            }

            def.meta = new Meta(200, "Success");
            def.data = deliveryVendor;

            return Ok(def);
        }

        // DELETE: api/DeliveryVendors/5        
        public IHttpActionResult DeleteDeliveryVendor(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int EmployeeId = Int32.Parse(identity.Claims.Where(c => c.Type == "EmployeeId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            DeliveryVendor deliveryVendor = db.DeliveryVendors.Find(id);
            if (deliveryVendor == null || deliveryVendor.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            deliveryVendor.Status = 99;
            Action action = new Action();
            action.ActionName = "xóa nhà cung cấp vận chuyển";
            action.ActionType = "DELETE";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = deliveryVendor.DeliveryVendorId;
            action.TargetType = "DELIVERY_VENDOR";
            db.Actions.Add(action);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        private string generateCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.DeliveryVendors.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopDVendorPrefix + code;
            return code;
        }

        [HttpGet]
        [Route("api/deliveryvendors/getVendorCode")]
        public IHttpActionResult getVendorCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            int count = db.DeliveryVendors.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopDVendorPrefix + code;
            def.meta = new Meta(200, "Success");
            def.data = code;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/deliveryvendors/{id}/cashes")]
        public IHttpActionResult getCashes(int id, [FromUri] FilteredPagination paging)
        {

            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            try
            {
                var result = (from c in db.Cashes
                              where c.PaidTargetId == id && c.PaidTargetType == "DELIVERY_VENDOR"
                              orderby c.CreatedAt descending
                              select new SaleManager.Data.VendorCash
                              {
                                  CashCode = c.CashCode,
                                  CashId = c.CashId,
                                  cashType = new Data.CashType()
                                  {
                                      CashTypeId = c.CashType.CashTypeId,
                                      Name = c.CashType.Name,
                                      Type = c.CashType.Type
                                  },
                                  employee = new Data.Employee()
                                  {
                                      EmployeeId = c.Employee.EmployeeId,
                                      EmployeeName = c.Employee.EmployeeName
                                  },
                                  PaidAmount = c.PaidAmount,
                                  Title = c.Title,
                                  Total = c.Total,
                                  Status = c.Status
                              }
                         ).AsEnumerable();
                int count = result.Count();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            catch (Exception e)
            {
                def.meta = new Meta(500, "ERRROR");
                def.data = e;
                return Ok(def);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DeliveryVendorExists(int id)
        {
            return db.DeliveryVendors.Count(e => e.DeliveryVendorId == id) > 0;
        }
    }
}