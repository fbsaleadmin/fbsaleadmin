﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class LabelsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
  
        [Route("api/labels/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Label> labels = db.Labels;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    labels = labels.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    labels = labels.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    labels = labels.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    labels = labels.OrderBy("LabelId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }          
                def.data = labels.Select(a => new LabelDTO()
                {
                    CreatedAt = a.CreatedAt,
                    LabelColor = a.LabelColor,
                    LabelId = a.LabelId,
                    LabelName = a.LabelName,
                    ShopId = a.ShopId,
                    Status = a.Status,
                    UpdatedAt = a.UpdatedAt
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Labels/5
        [ResponseType(typeof(Label))]
        public IHttpActionResult GetLabel(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            LabelDTO label = db.Labels.Where(a => a.LabelId == id).Select(a => new LabelDTO()
            {
                CreatedAt = a.CreatedAt,
                LabelColor = a.LabelColor,
                LabelId = a.LabelId,
                LabelName = a.LabelName,
                ShopId = a.ShopId,
                Status = a.Status,
                UpdatedAt = a.UpdatedAt
            }).FirstOrDefault();
            if (label == null || label.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = label;
            return Ok(def);
        }

        // PUT: api/Labels/5      
        public IHttpActionResult PutLabel(int id, LabelDTO label)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != label.LabelId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Label current = db.Labels.Where(a => a.LabelId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.LabelName = label.LabelName;
                current.LabelColor = label.LabelColor;
                current.UpdatedAt = DateTime.Now;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            } 
        }

        // POST: api/Labels      
        public IHttpActionResult PostLabel(LabelDTO label)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Label a = new Label();
            a.CreatedAt = DateTime.Now;
            a.LabelColor = label.LabelColor;
            a.LabelName = label.LabelName;
            a.ShopId = ShopId;
            a.Status = 0;
            a.UpdatedAt = DateTime.Now;
            db.Labels.Add(a);
            db.SaveChanges();
            label.LabelId = a.LabelId;
            label.CreatedAt = a.CreatedAt;
            label.UpdatedAt = a.CreatedAt;
            label.Status = a.Status;

            def.meta = new Meta(200, "Success");
            def.data = label;

            return Ok(def);
        }

        // DELETE: api/Labels/5      
        public IHttpActionResult DeleteLabel(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Label label = db.Labels.Find(id);
            if (label == null || label.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            //remove label conversation
            db.LabelConversations.RemoveRange(label.LabelConversations);
            db.Labels.Remove(label);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LabelExists(int id)
        {
            return db.Labels.Count(e => e.LabelId == id) > 0;
        }
    }
}