﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ProvincesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // GET: api/Provinces
        public IHttpActionResult GetProvinces()
        {
            DefaultResponse def = new DefaultResponse();
            def.meta = new Meta(200, "Success");
            def.data = db.Provinces.Select(p => new ProvinceDTO()
            {
                Code = p.Code,
                Name = p.Name,
                Priority = p.Priority,
                ProvinceId = p.ProvinceId
            });
            return Ok(def);
        }

        [Route("api/provinces/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Province> provinces;
                if (paging.order_by != null)
                {
                    provinces = db.Provinces.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    provinces = db.Provinces.OrderBy("ProvinceId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                if (paging.query != null)
                    provinces = provinces.Where(HttpUtility.UrlDecode(paging.query));
                def.data = provinces.Select(p => new ProvinceDTO()
                {
                    Code = p.Code,
                    Name = p.Name,
                    Priority = p.Priority,
                    ProvinceId = p.ProvinceId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Provinces/5      
        public IHttpActionResult GetProvince(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Province province = db.Provinces.Find(id);
            if (province == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = province;
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProvinceExists(int id)
        {
            return db.Provinces.Count(e => e.ProvinceId == id) > 0;
        }
    }
}