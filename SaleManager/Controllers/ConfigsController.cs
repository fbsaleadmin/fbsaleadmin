﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ConfigsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [HttpGet]
        [Route("api/configs/{id}")]
        public IHttpActionResult GetConfig(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            ConfigDTO config = db.Configs.Where(a => a.ShopId == ShopId).Select(a => new ConfigDTO()
            {
                ConfigId = a.ConfigId,
                AutoCreateOrder = a.AutoCreateOrder,
                AutoHideComment = a.AutoHideComment,
                NewCommentLike = a.NewCommentLike,
                NewMessageNotification = a.NewMessageNotification,
                NewMessagePriority = a.NewMessagePriority,
                NotificationSound = a.NotificationSound,
                AllowBotMessage = a.AllowBotMessage,
                AutoHidePhoneComment = a.AutoHidePhoneComment,
                ShopId = a.ShopId
            }).FirstOrDefault();
            if (config == null || config.ShopId != id)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = config;
            return Ok(def);
        }

        [Route("api/configs/{id}")]
        // PUT: api/Configs/5      
        public IHttpActionResult PutConfig(int id,ConfigDTO config)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Config current = db.Configs.Where(s => s.ShopId == ShopId).FirstOrDefault();
            if (current == null || current.ShopId != id)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.AutoCreateOrder = config.AutoCreateOrder;
                current.AutoHideComment = config.AutoHideComment;
                current.NewCommentLike = config.NewCommentLike;
                current.NewMessageNotification = config.NewMessageNotification;
                current.NewMessagePriority = config.NewMessagePriority;
                current.NotificationSound = config.NotificationSound;
                current.AllowBotMessage = config.AllowBotMessage;
                current.AutoHidePhoneComment = config.AutoHidePhoneComment;
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConfigExists(int id)
        {
            return db.Configs.Count(e => e.ConfigId == id) > 0;
        }
    }
}