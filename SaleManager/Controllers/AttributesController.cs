﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class AttributesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/attributes/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Attribute> attributes = db.Attributes;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    attributes = attributes.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    attributes = attributes.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    attributes = attributes.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    attributes = attributes.OrderBy("AttributeId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }              
                def.data = attributes.Select(a => new AttributeDTO()
                {
                    AttributeName = a.AttributeName,
                    AttributeId = a.AttributeId,
                    CreatedAt = a.CreatedAt,
                    IsCustom = a.IsCustom,
                    ShopId = a.ShopId,
                    UpdatedAt = a.UpdatedAt
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Attributes/5    
        public IHttpActionResult GetAttribute(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;         
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            AttributeDTO attribute = db.Attributes.Where(a => a.AttributeId == id).Select(a => new AttributeDTO()
            {
                AttributeName = a.AttributeName,
                AttributeId = a.AttributeId,
                CreatedAt = a.CreatedAt,
                IsCustom = a.IsCustom,
                ShopId = a.ShopId,
                UpdatedAt = a.UpdatedAt
            }).FirstOrDefault();
            if (attribute == null || !attribute.ShopId.ToString().Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = attribute;
            return Ok(def);
        }

        // PUT: api/Attributes/5     
        public IHttpActionResult PutAttribute(int id, AttributeDTO attribute)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != attribute.AttributeId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            Attribute current = db.Attributes.Where(s => s.AttributeId == id).FirstOrDefault();
            if (current == null || !current.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.AttributeName = attribute.AttributeName;
                current.UpdatedAt = DateTime.Now;                   
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Attributes       
        public IHttpActionResult PostAttribute(AttributeDTO attribute)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Attribute att = new Attribute();
            att.AttributeName = attribute.AttributeName;
            att.CreatedAt = DateTime.Now;
            att.IsCustom = true;
            att.ShopId = Int32.Parse(ShopId);
            att.UpdatedAt = DateTime.Now;
            db.Attributes.Add(att);
            db.SaveChanges();
            attribute.AttributeId = att.AttributeId;

            def.meta = new Meta(200, "Success");
            def.data = attribute;

            return Ok(def);
        }

        // DELETE: api/Attributes/5      
        public IHttpActionResult DeleteAttribute(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            Attribute attribute = db.Attributes.Find(id);
            if (attribute == null || !attribute.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.Attributes.Remove(attribute);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AttributeExists(int id)
        {
            return db.Attributes.Count(e => e.AttributeId == id) > 0;
        }
    }
}