﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ConversationsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();    

        //[Route("api/conversations/GetByPage")]
        //public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        //{
        //    var identity = (ClaimsIdentity)User.Identity;
        //    int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
        //    DefaultResponse def = new DefaultResponse();
        //    if (paging != null)
        //    {
        //        def.meta = new Meta(200, "Success");
        //        IQueryable<Conversation> conversations = db.Conversations;

        //        if (paging.query != null)
        //        {
        //            paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
        //            conversations = conversations.Where(paging.query);
        //        }
        //        else
        //        {
        //            paging.query = "ShopId=" + ShopId;
        //            conversations = conversations.Where(paging.query);
        //        }
        //        if (paging.order_by != null)
        //        {
        //            conversations = conversations.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }
        //        else
        //        {
        //            conversations = conversations.OrderBy("UpdatedAt desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
        //        }             
        //        def.data = conversations.Select(c => new ShopConversation()
        //        {
        //            ConversationId = c.ConversationId,
        //            CreatedAt = c.CreatedAt,
        //            CustomerId = c.CustomerId,
        //            FacebookUserId = c.FacebookUserId,                  
        //            Name = c.Name,
        //            Note = c.Note,
        //            PageId = c.PageId,
        //            page = db.Pages.Where(p => p.PageId == c.PageId).Select(p =>  new ShopPage(){
        //                PageFacebookId = p.PageFacebookId,
        //                PageId = p.PageId,
        //                PageName = p.PageName
        //            }).FirstOrDefault(),
        //            ShopId = c.ShopId,
        //            Status = c.Status,
        //            Tag = c.Tag,
        //            UpdatedAt = c.UpdatedAt,
        //            LastMessage = c.LastMessage,
        //            FacebookPostId = c.FacebookPostId,
        //            Type = c.Type,                    
        //            FBUserId = c.FBUserId,
        //            UserAvatar = c.UserAvatar,
        //            LastMessageTime = c.LastMessageTime,
        //            lables = db.LabelConversations.Where(lc => lc.ConversationId == c.ConversationId).Select(lc => new ShopLabel() { 
        //                LabelId = (int)lc.LabelId,
        //                LabelColor = lc.Label.LabelColor,
        //                LabelConversationId = lc.LabelConversationId,
        //                LabelName = lc.Label.LabelName
        //            }).ToList()
        //        });
        //        return Ok(def);
        //    }
        //    else
        //    {
        //        def.meta = new Meta(400, "Bad Request");
        //        return Ok(def);
        //    }
        //}

        [Route("api/conversations/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] int page,[FromUri]int page_size,[FromUri] string order_by = null,[FromUri]string query = null,[FromUri] string label=null,[FromUri] int hasPhone = 0,
            [FromUri] string post_id = null,[FromUri] int type = -1,[FromUri] string fbid = null,[FromUri] string date = "",[FromUri] int status = 100, [FromUri] int blocked = 0,[FromUri] int page_id = -1)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            List<string> lables = null;
            if (label != null && label.IndexOf(',') > 0)
            {
                string[] _label = label.Split(',');
                lables = new List<string>(_label);
            }
            else
            {
                if (label != null && label.Trim().Length > 0)
                    lables = new List<string>(new string[] { label });               
            }
            //check customer
            int customerId = -1;
            Customer customer = db.Customers.Where(c => c.FacebookUserId == fbid && fbid != null).FirstOrDefault();
            if (customer != null)
                customerId = customer.CustomerId;
            //check date
            Nullable<DateTime> startDate = null;
            Nullable<DateTime> endDate = null;
            try
            {
                if (date != "")
                {
                    DateTime searchDate = DateTime.ParseExact(date, "ddMMyyyy", null);
                    startDate = new DateTime(searchDate.Year, searchDate.Month, searchDate.Day, 0, 0, 0);
                    endDate = new DateTime(searchDate.Year, searchDate.Month, searchDate.Day, 23, 59, 59);
                }
            }
            catch (Exception ex)
            {
                startDate = null;
                endDate = null;
            }           
            //aa
            var result = db.Conversations.Where(c => c.ShopId == ShopId).AsQueryable();
            if (status != 100)
                result = result.Where(c => c.Status == status).AsQueryable();
            if (query != null)
                result = result.Where(c => c.SearchQuery.Contains(query)).AsQueryable();
            if (lables != null && lables.Count > 0)
                result = result.Where(c => c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString()))).AsQueryable();
            if (post_id != null)
                result = result.Where(c => c.FacebookPostId == post_id).AsQueryable();
            if (hasPhone == 1)
                result = result.Where(c => c.Phone != null).AsQueryable();
            if (customerId != -1)
                result = result.Where(c => c.CustomerId == customerId).AsQueryable();
            if (type != -1)
                result = result.Where(c => c.Type == type).AsQueryable();
            if (startDate != null)
                result = result.Where(c => c.UpdatedAt >= startDate && c.UpdatedAt <= endDate).AsQueryable();
            if (page_id != -1)
                result = result.Where(c => c.PageId == page_id).AsQueryable();
            if (blocked == 1)
            {
                var blocks = db.BlockLists.Select(c => c.FacebookUserId).ToList();
                result = result.Where(c => blocks.Contains(c.FBUserId)).AsQueryable();
            }
            //if (query != null)
            //{
            //    if (lables != null && lables.Count >0)
            //    {
            //        if (post_id != null)
            //            if (type != -1)
            //                if (hasPhone == 0) 
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.FacebookPostId == post_id && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //        else
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.SearchQuery.Contains(query) && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //        }                        
            //    }
            //    else
            //    {
            //        if (post_id != null)
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.SearchQuery.Contains(query) && c.FacebookPostId == post_id);
            //        }
            //        else
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.SearchQuery.Contains(query));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.SearchQuery.Contains(query));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.SearchQuery.Contains(query));
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.SearchQuery.Contains(query));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.SearchQuery.Contains(query));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.SearchQuery.Contains(query));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.SearchQuery.Contains(query));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.SearchQuery.Contains(query));
            //        }
            //    }
            //}
            //else
            //{
            //    if (lables != null && lables.Count >0)
            //    {
            //        if (post_id != null)
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.FacebookPostId == post_id && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //        }
            //        else
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));

            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.LabelConversations.Any(lc => lables.Contains(lc.LabelId.ToString())));
            //        }
            //    }
            //    else
            //    {
            //        if (post_id != null)
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type && c.FacebookPostId == post_id);
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type && c.FacebookPostId == post_id);
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.FacebookPostId == post_id);
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.FacebookPostId == post_id);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.FacebookPostId == post_id);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.FacebookPostId == post_id);
            //        }
            //        else
            //        {
            //            if (type != -1)
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Type == type);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Type == type);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Type == type);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Type == type);
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null && c.Type == type);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null && c.Type == type);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null && c.Type == type);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null && c.Type == type);
            //            else
            //                if (hasPhone == 0)
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId);
            //                else
            //                    if (customerId == -1)
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.Phone != null);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.Phone != null);
            //                    else
            //                        if (startDate == null)
            //                            result = result.Where(c => c.ShopId == ShopId && c.CustomerId == customerId && c.Phone != null);
            //                        else
            //                            result = result.Where(c => c.ShopId == ShopId && c.UpdatedAt >= startDate && c.UpdatedAt <= endDate && c.CustomerId == customerId && c.Phone != null);
            //        }
            //    }
            //}
            if (order_by != null)
                result = result.OrderBy(order_by).Skip((page - 1) * page_size).Take(page_size);
            else
                result = result.OrderBy("UpdatedAt desc").Skip((page - 1) * page_size).Take(page_size);
            //new query
            //end new query
            var returnResult = result.Select(c => new ShopConversation()
            {
                ConversationId = c.ConversationId,
                CreatedAt = c.CreatedAt,
                CustomerId = c.CustomerId,
                FacebookUserId = c.FacebookUserId,
                Name = c.Name,
                Note = c.Note,
                PageId = c.PageId,
                page = db.Pages.Where(p => p.PageId == c.PageId).Select(p => new ShopPage()
                {
                    PageFacebookId = p.PageFacebookId,
                    PageId = p.PageId,
                    PageName = p.PageName
                }).FirstOrDefault(),
                ShopId = c.ShopId,
                Status = c.Status,
                Tag = c.Tag,
                UpdatedAt = c.UpdatedAt,//
                LastMessage = c.LastMessage,
                FacebookPostId = c.FacebookPostId,
                Phone = c.Phone,
                Type = c.Type,
                FBUserId = c.FBUserId,
                UserAvatar = c.UserAvatar,
                LastMessageTime = c.LastMessageTime,
                SearchQuery = c.SearchQuery,
                FacebookCommentId = c.FacebookCommentId,
                TotalConversation = db.Conversations.Where(cv => cv.FBUserId == c.FBUserId).Count(),
                IsPMSent = c.IsPMSent,                
                lables = db.LabelConversations.Where(lc => lc.ConversationId == c.ConversationId).Select(lc => new ShopLabel()
                {
                    LabelId = (int)lc.LabelId,
                    LabelColor = lc.Label.LabelColor,
                    LabelConversationId = lc.LabelConversationId,
                    LabelName = lc.Label.LabelName
                }).ToList(),
                CommentId = db.Comments.Where(cm => cm.FacebookCommentId == c.FacebookCommentId).FirstOrDefault() != null ? db.Comments.Where(cm => cm.FacebookCommentId == c.FacebookCommentId).FirstOrDefault().CommentId : -1
            }).ToList();
            //get time stemp
            for (int i = 0; i < returnResult.Count;i++ )
            {
                string userId = returnResult[i].FBUserId;
                returnResult[i].LastUnixTimestamp = Utils.DateTimeToUnixTimeStamp(returnResult[i].LastMessageTime.Value);
                returnResult[i].IsUserBlocked = blocked == 0 ? false : true;
            }
            def.data = returnResult;
            def.meta = new Meta(200, "Success");           
            return Ok(def);
        }

        // GET: api/Conversations/5        
        public IHttpActionResult GetConversation(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            ShopConversation conversation = db.Conversations.Where(c => c.ConversationId == id).Select(c => new ShopConversation()
            {
                ConversationId = c.ConversationId,
                CreatedAt = c.CreatedAt,
                CustomerId = c.CustomerId,
                FacebookUserId = c.FacebookUserId,               
                Name = c.Name,
                Note = c.Note,
                PageId = c.PageId,
                page = db.Pages.Where(p => p.PageId == c.PageId).Select(p => new ShopPage()
                {
                    PageFacebookId = p.PageFacebookId,
                    PageId = p.PageId,
                    PageName = p.PageName
                }).FirstOrDefault(),
                ShopId = c.ShopId,
                Status = c.Status,
                Tag = c.Tag,
                UpdatedAt = c.UpdatedAt,
                LastMessage = c.LastMessage,
                FacebookPostId = c.FacebookPostId,
                Type = c.Type,
                FBUserId = c.FBUserId,
                UserAvatar = c.UserAvatar,
                LastMessageTime = c.LastMessageTime,
                SearchQuery = c.SearchQuery,
                Phone = c.Phone,
                FacebookCommentId = c.FacebookCommentId,
                lables = db.LabelConversations.Where(lc => lc.ConversationId == c.ConversationId).Select(lc => new ShopLabel() {
                        LabelId = (int)lc.LabelId,
                        LabelColor = lc.Label.LabelColor,
                        LabelConversationId = lc.LabelConversationId,
                        LabelName = lc.Label.LabelName
                    }).ToList()
            }).FirstOrDefault();
            if (conversation == null || conversation.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = conversation;
            return Ok(def);
        }

        // PUT: api/Conversations/5       
        public IHttpActionResult PutConversation(int id, ConversationDTO conversation)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != conversation.ConversationId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Conversation current = db.Conversations.Where(c => c.ConversationId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {              
                current.Name = conversation.Name;
                current.Note = conversation.Note;
                current.Status = conversation.Status;
                current.Tag = conversation.Tag;
                current.UpdatedAt = DateTime.Now;
                current.LastMessage = conversation.LastMessage;
                current.SearchQuery = Utils.unsignString(current.Name + " " + current.LastMessage);
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            } 
        }

        // POST: api/Conversations      
        public IHttpActionResult PostConversation(ConversationDTO conversation)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(cl => cl.Type == "ShopId").Select(cl => cl.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Conversation c = new Conversation();
            c.CreatedAt = DateTime.Now;
            c.CustomerId = conversation.CustomerId;
            c.FacebookUserId = conversation.FacebookUserId;          
            c.Name = conversation.Name;
            c.Note = conversation.Note;
            c.PageId = conversation.PageId;
            c.ShopId = ShopId;
            c.Status = conversation.Status;
            c.Tag = conversation.Tag;
            c.UpdatedAt = DateTime.Now;
            c.LastMessage = "";
            c.FacebookPostId = conversation.FacebookPostId;
            c.Type = conversation.Type;
            c.FBUserId = conversation.FBUserId;
            c.UserAvatar = conversation.UserAvatar;
            c.LastMessageTime = conversation.LastMessageTime;
            c.SearchQuery = Utils.unsignString(c.Name + " " + c.LastMessage);
            db.Conversations.Add(c);
            db.SaveChanges();
            conversation.ConversationId = c.ConversationId;

            def.meta = new Meta(200, "Success");
            def.data = conversation;

            return Ok(def);
        }

        // DELETE: api/Conversations/5       
        public IHttpActionResult DeleteConversation(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Conversation conversation = db.Conversations.Find(id);            
            if (conversation == null || conversation.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            //delete message belong to conversation
            conversation.Status = 99;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        //[HttpPost]
        //[Route("api/conversations/{id}/labels")]
        //public IHttpActionResult PostConversation(LabelConversationDTO label)
        //{
        //    DefaultResponse def = new DefaultResponse();

        //    def.meta = new Meta(200, "Success");
        //    return Ok(def);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConversationExists(int id)
        {
            return db.Conversations.Count(e => e.ConversationId == id) > 0;
        }
    }
}