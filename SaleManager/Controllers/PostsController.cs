﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class PostsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/posts/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;         
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Post> posts = db.Posts;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    posts = posts.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    posts = posts.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    posts = posts.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    posts = posts.OrderBy("PostId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                
                def.data = posts.Select(p => new PostDTO()
                {
                    Content = p.Content,
                    CreatedDate = p.CreatedDate,
                    FacebookPostId = p.FacebookPostId,
                    FacebookPostUrl = p.FacebookPostUrl,
                    PageId = p.PageId,
                    PostId = p.PostId,
                    ShopId = p.ShopId,
                    Status = p.Status,
                    Title = p.Title,
                    UpdatedAt = p.UpdatedAt,
                    Image = p.Image,
                    ShowComment = p.ShowComment
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Posts/5        
        public IHttpActionResult GetPost(int id)
        {
            DefaultResponse def = new DefaultResponse();
            PostDTO post = db.Posts.Where(p => p.PostId == id).Select(p => new PostDTO()
            {
                Content = p.Content,
                CreatedDate = p.CreatedDate,
                FacebookPostId = p.FacebookPostId,
                FacebookPostUrl = p.FacebookPostUrl,
                PageId = p.PageId,
                PostId = p.PostId,
                ShopId = p.ShopId,
                Status = p.Status,
                Title = p.Title,
                UpdatedAt = p.UpdatedAt,
                Image = p.Image
            }).FirstOrDefault();
            //check claim
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            if (post == null || !post.ShopId.ToString().Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = post;
            return Ok(def);
        }
                // PUT: api/Posts/5        
        public IHttpActionResult PutPost(int id, PostDTO post)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != post.PostId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            Post current = db.Posts.Where(p => p.PostId == id).FirstOrDefault();
            if (current == null || !current.ShopId.ToString().Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.Content = post.Content;
                current.Status = post.Status;
                current.Title = post.Title;
                current.Image = post.Image;
                current.UpdatedAt = DateTime.Now;               
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            } 
        }

        // POST: api/Posts       
        public IHttpActionResult PostPost(PostDTO post)
        {
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            Post p = new Post();
            p.Content = post.Content;
            p.CreatedDate = DateTime.Now;
            p.FacebookPostId = post.FacebookPostId;
            p.FacebookPostUrl = post.FacebookPostUrl;
            p.PageId = post.PageId;
            p.ShopId = Int32.Parse(ShopId);
            p.Status = post.Status;
            p.Title = post.Title;
            p.UpdatedAt = DateTime.Now;
            p.Image = post.Image;
            db.Posts.Add(p);
            db.SaveChanges();
            post.PostId = p.PostId;

            def.meta = new Meta(200, "Success");
            def.data = post;

            return Ok(def);
        }

        // DELETE: api/Posts/5      
        public IHttpActionResult DeletePost(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Post post = db.Posts.Find(id);
            var identity = (ClaimsIdentity)User.Identity;
            string ShopId = identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault();
            if (post == null || !post.ShopId.Equals(ShopId))
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            //delete all replies belong to comment  
            db.Posts.Remove(post);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        // SETTING: api/setting/5
        [HttpPost]
        [Route("api/posts/{id}/settings")]
        public IHttpActionResult SettingPost(int id)
        {
            DefaultResponse def = new DefaultResponse();
            Post post = db.Posts.Find(id);
            int status = 0;
            if (post == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                if (post.ShowComment == 0)
                {
                    post.ShowComment = 1;
                    status = 1;
                }
                else
                {
                    post.ShowComment = 0;
                }

                post.UpdatedAt = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    def.data = status;
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }
        }

        [HttpGet]
        [Route("api/posts/find/{fbid}")]
        public IHttpActionResult SettingPost(string fbid)
        {
            DefaultResponse def = new DefaultResponse();
            PostDTO post = db.Posts.Where(p => p.FacebookPostId == fbid).Select(p => new PostDTO() { 
                Content = p.Content,
                CreatedDate = p.CreatedDate,
                FacebookPostId = p.FacebookPostId,
                FacebookPostUrl = p.FacebookPostUrl,
                Image = p.Image,
                PageId = p.PageId,
                PostId = p.PostId,
                ShopId = p.ShopId,
                ShowComment = p.ShowComment,
                Status = p.Status,
                Title = p.Title,
                UpdatedAt = p.UpdatedAt
            }).FirstOrDefault();
            if (post == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(200, "success");
                def.data = post;
                return Ok(def);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PostExists(int id)
        {
            return db.Posts.Count(e => e.PostId == id) > 0;
        }
    }
}