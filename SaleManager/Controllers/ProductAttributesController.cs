﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using SaleManager.Data;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ProductAttributesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // DELETE: api/ProductAttributes/5     
        public IHttpActionResult DeleteProductAttribute(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int EmployeeId = Int32.Parse(identity.Claims.Where(c => c.Type == "EmployeeId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            ProductAttribute productAttribute = db.ProductAttributes.Find(id);
            if (productAttribute == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            productAttribute.Status = 99;
            
            Action action = new Action();
            action.ActionName = "xóa sản phẩm";
            action.ActionType = "DELETE";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = productAttribute.ProductAttributeId;
            action.TargetType = "PRODUCT_ATTRIBUTE";
            db.Actions.Add(action);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpPost]
        [Route("api/productattributes/{id}/notallow")]
        public IHttpActionResult NotAllow(int id)
        {
            DefaultResponse def = new DefaultResponse();
            ProductAttribute productAttribute = db.ProductAttributes.Find(id);
            if (productAttribute == null)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            if (productAttribute.Status != 98)
                productAttribute.Status = 98;
            else
                productAttribute.Status = 0;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpGet]
        [Route("api/productattributes/getProductCode/{id}")]
        public IHttpActionResult getProductCode(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());            
            int count = db.ProductAttributes.Where(o => o.Product.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopProductPrefix + code;            
            def.meta = new Meta(200, "Success");
            def.data = code;
            return Ok(def);
        }

        //import
        [HttpGet]
        [Route("api/productattributes/{id}/imports")]
        public IHttpActionResult GetPAImport(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from ip in db.EximProducts
                              join e in db.Exims on ip.EximId equals e.EximId
                              join pa in db.ProductAttributes on ip.ProductAttributeId equals pa.ProductAttributeId 
                              where ip.ProductAttributeId == id && e.Status != (int) Const.EximStatus.DELETED && e.Status != (int) Const.EximStatus.CANCELED
                              && ip.Exim.Type == (int)Const.EximType.IMPORT && e.Status != (int)Const.EximStatus.TEMP
                              orderby ip.CreatedAt descending
                              select new ProductAttributeImport
                              {
                                  CreatedAt = (DateTime)ip.CreatedAt,
                                  DeliveryStatus = (int)ip.Exim.DeliveryStatus,
                                  ImportPrice = (decimal)ip.Price,
                                  LastStock = (int)ip.LastStock,
                                  PaymentStatus = (int)ip.Exim.PaymentStatus,
                                  PreviousStock = (int)ip.PreviousStock,
                                  ProductAttributeId = (int)ip.ProductAttributeId,
                                  Quantity = (int)ip.Quantity,
                                  ImportCode = ip.Exim.Code,
                                  vendor = db.Vendors.Where(v => v.VendorId == ip.Exim.TargetId).Select(v => new SaleManager.Data.Vendor()
                                  {
                                      VendorId = v.VendorId,
                                      VendorName = v.VendorName,
                                      VendorCode = v.VendorCode
                                  }).FirstOrDefault()
                              }).AsEnumerable();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/productattributes/{id}/{uid}/prices-imports")]
        public IHttpActionResult GetPAPriceExport(int id, int uid,[FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from pp in db.ProductPrices
                             join pu in db.ProductUnits on pp.ProductUnitId equals pu.ProductUnitId
                             where pp.ProductAttributeId == id && pp.ProductUnitId == uid
                             select new
                             {
                                 CreatedAt = pp.FromDate,
                                 SalePrice = pp.SalePrice,
                                 ProductUnitName = pu.Unit
                             }).AsEnumerable();

                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/productattributes/{id}/orders")]
        public IHttpActionResult GetPAOrder(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from o in db.OrderProducts
                              join pa in db.ProductAttributes on o.ProductAttributeId equals pa.ProductAttributeId
                              where o.ProductAttributeId == id
                              orderby o.CreatedAt descending
                              select new ProductAttributeOrder
                              {
                                  CreatedAt = (DateTime)o.CreatedAt,
                                  branch = db.Branches.Where(b => b.BranchId == o.Order.BranchId).Select(b => new SaleManager.Data.Branch()
                                  {
                                      BranchId = b.BranchId,
                                      BranchName = b.BranchName
                                  }).FirstOrDefault(),
                                  customer = db.Customers.Where(c => c.CustomerId == o.Order.CustomerId).Select(c => new SaleManager.Data.Customer()
                                  {
                                      CustomerCode = c.CustomerCode,
                                      CustomerId = c.CustomerId,
                                      CustomerName = c.CustomerName,
                                      FacebookUserId = c.FacebookUserId
                                  }).FirstOrDefault(),
                                  DeliveryStatus = (int)o.Order.DeliveryStatus,
                                  OrderCode = o.Order.OrderCode,
                                  PaymentStatus = (int)o.Order.PaymentStatus,
                                  Price = (decimal)o.Price,
                                  ProductAttributeId = pa.ProductAttributeId,
                                  Quantity = (int)o.Quantity,
                                  unit = db.ProductUnits.Where(u => u.ProductUnitId == o.ProductUnitId).Select(u => new SaleManager.Data.Unit()
                                  {
                                      Name = u.Unit,
                                      ProductUnitId = u.ProductUnitId
                                  }).FirstOrDefault()
                              }).AsEnumerable();
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                def.meta = new Meta(200, "Success");
                def.data = result;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/productattributes/{id}/exims")]
        public IHttpActionResult GetPAExims(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from ip in db.EximProducts
                              join e in db.Exims on ip.EximId equals e.EximId
                              join pa in db.ProductAttributes on ip.ProductAttributeId equals pa.ProductAttributeId
                              where ip.ProductAttributeId == id && e.Status != (int)Const.EximStatus.DELETED 
                              && e.Status != (int)Const.EximStatus.CANCELED && e.Status != (int)Const.EximStatus.TEMP
                              orderby ip.CreatedAt descending
                              select new ProductAttributeExims
                              {
                                  CreatedAt = (DateTime)ip.CreatedAt,
                                  DeliveryStatus = (int)ip.Exim.DeliveryStatus,
                                  EximsPrice = (decimal)ip.Price,
                                  LastStock = (int)ip.LastStock,
                                  PaymentStatus = (int)ip.Exim.PaymentStatus,
                                  PreviousStock = (int)ip.PreviousStock,
                                  ProductAttributeId = (int)ip.ProductAttributeId,
                                  Quantity = (int)ip.Quantity,
                                  TargetId=e.TargetId,
                                  Type=e.Type,
                                  TargetType=e.TargetType,
                                  EximsCode = ip.Exim.Code,
                                  branch = db.Branches.Where(b => b.BranchId == e.BranchId).Select(b => new SaleManager.Data.Branch()
                                  {
                                      BranchId = b.BranchId,
                                      BranchName = b.BranchName
                                  }).FirstOrDefault(),
                              }).AsEnumerable();

                var returnData = result.ToList();
                for (int i = 0; i < returnData.Count; i++)
                {
                    int paidTargetId = (int)returnData[i].TargetId;
                    switch (returnData[i].TargetType)
                    {
                        case "CUSTOMER":
                            returnData[i].target = db.Customers.Where(c => c.CustomerId == paidTargetId).Select(c => new Target()
                            {
                                TargetId = c.CustomerId,
                                TargetName = c.CustomerName,
                                TargetCode = c.CustomerCode
                            }).FirstOrDefault();
                            break;
                        case "VENDOR":
                            returnData[i].target = db.Vendors.Where(c => c.VendorId == paidTargetId).Select(c => new Target()
                            {
                                TargetId = c.VendorId,
                                TargetName = c.VendorName,
                                TargetCode = c.VendorCode
                            }).FirstOrDefault();
                            break;
                        default:
                            returnData[i].target = null;
                            break;
                    }
                }
                result = returnData.AsEnumerable();
                int count=0;
                if (result != null)
                {
                    count = result.Count();
                    result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                def.meta = new Meta(200, "Success");
                def.data = result.OrderByDescending(e=>e.CreatedAt).ToList();
                def.metadata = new Metadata(count);
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        [HttpGet]
        [Route("api/productattributes/{id}/attributes")]
        public IHttpActionResult GetAttributes(int id)
        {
            DefaultResponse def = new DefaultResponse();
            var result = (from pa in db.ProductAttributes
                          join ma in db.AttributeMaps on pa.ProductAttributeId equals ma.ProductAttributeId
                          join a in db.Attributes on ma.AttributeId equals a.AttributeId
                          where pa.ProductAttributeId == id                          
                          select new PAAtrribute
                          {
                              AttributeId = a.AttributeId,
                              AttributeMapId = ma.AttributeMapId,
                              AttributeName = a.AttributeName,
                              AttributeValue = ma.Value
                          }).AsEnumerable();
            def.meta = new Meta(200, "Success");
            def.data = result;
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductAttributeExists(int id)
        {
            return db.ProductAttributes.Count(e => e.ProductAttributeId == id) > 0;
        }
    }
}