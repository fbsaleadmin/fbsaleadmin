﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class CashTypesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        // POST: api/CashTypes       
        public IHttpActionResult PostCashType(CashTypeDTO cashType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            //check exist
            var count = db.CashTypes.Where(ctype => ctype.CashTypeCode == cashType.CashTypeCode).Count();
            if (count > 0)
            {
                def.meta = new Meta(211, "Duplicate code");
                return Ok(def);
            }

            CashType ct = new CashType();
            ct.CreatedAt = DateTime.Now;
            ct.DefaultValue = 0;
            ct.IsCustom = true;
            ct.Name = cashType.Name;
            ct.ShopId = ShopId;
            ct.Type = cashType.Type;
            ct.UpdatedAt = DateTime.Now;
            ct.CashTypeCode = cashType.CashTypeCode;
            db.CashTypes.Add(ct);
            db.SaveChanges();
            cashType.CashTypeId = ct.CashTypeId;

            def.meta = new Meta(200, "Success");
            def.data = cashType;

            return Ok(def);
        }

        // DELETE: api/CashTypes/5       
        public IHttpActionResult DeleteCashType(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            CashType cashType = db.CashTypes.Find(id);
            if (cashType == null || cashType.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            cashType.Type = 99;
            db.CashTypes.Remove(cashType);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CashTypeExists(int id)
        {
            return db.CashTypes.Count(e => e.CashTypeId == id) > 0;
        }
    }
}