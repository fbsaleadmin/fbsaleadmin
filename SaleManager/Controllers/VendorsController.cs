﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;
using SaleManager.Data;

namespace SaleManager.Controllers
{
    [Authorize]
    public class VendorsController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/vendors/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Vendor> vendors = db.Vendors;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    vendors = vendors.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    vendors = vendors.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    vendors = vendors.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    vendors = vendors.OrderBy("VendorId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }               
                def.data = vendors.Select(v => new VendorDTO()
                {
                    Address = v.Address,
                    Company = v.Company,
                    CreatedAt = v.CreatedAt,
                    DistrictId = v.DistrictId,
                    Email = v.Email,
                    Note = v.Note,
                    ProvinceId = v.ProvinceId,
                    ShopId = v.ShopId,
                    UpdatedAt = v.UpdatedAt,
                    VendorCode = v.VendorCode,
                    VendorId = v.VendorId,
                    VendorName = v.VendorName,
                    Phone = v.Phone,
                    Status = v.Status,
                    SearchQuery = v.SearchQuery
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Vendors/5        
        public IHttpActionResult GetVendor(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            VendorDTO employee = db.Vendors.Where(v => v.VendorId == id).Select(v => new VendorDTO()
            {
                Address = v.Address,
                Company = v.Company,
                CreatedAt = v.CreatedAt,
                DistrictId = v.DistrictId,
                Email = v.Email,
                Note = v.Note,
                ProvinceId = v.ProvinceId,
                ShopId = v.ShopId,
                UpdatedAt = v.UpdatedAt,
                VendorCode = v.VendorCode,
                VendorId = v.VendorId,
                VendorName = v.VendorName,
                Phone = v.Phone,
                Status = v.Status,
                SearchQuery = v.SearchQuery
            }).FirstOrDefault();
            if (employee == null || employee.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = employee;
            return Ok(def);
        }

        // PUT: api/Vendors/5      
        public IHttpActionResult PutVendor(int id, VendorDTO vendor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != vendor.VendorId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Vendor current = db.Vendors.Where(s => s.VendorId == id).FirstOrDefault();
          
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                string lastNote = current.Note != null ? current.Note.Trim() : "";
                current.Address = vendor.Address;
                current.Company = vendor.Company;
                current.DistrictId = vendor.DistrictId;
                current.Email = vendor.Email;
                if (vendor.Note != null && vendor.Note.Trim().Length > 0 && !lastNote.Equals(vendor.Note.Trim()))
                    current.Note = vendor.Note;
                current.ProvinceId = vendor.ProvinceId;
                current.UpdatedAt = DateTime.Now;
                current.VendorCode = vendor.VendorCode;
                current.VendorName = vendor.VendorName;
                current.Phone = vendor.Phone;           
                current.SearchQuery = Utils.unsignString(current.VendorName + " " + current.VendorCode);
                //check if note changed
                if (vendor.Note != null && vendor.Note.Trim().Length > 0 && !lastNote.Equals(vendor.Note.Trim()))
                {
                    Note note = new Note();
                    note.Content = current.Note;
                    note.CreatedAt = DateTime.Now;
                    note.ShopId = ShopId;
                    note.Status = 0;
                    note.TargetId = current.VendorId;
                    note.TargetType = "VENDOR";
                    db.Notes.Add(note);
                    db.SaveChanges();
                }

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Vendors   
        public IHttpActionResult PostVendor(VendorDTO vendor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Vendor v = new Vendor();
            v.Address = vendor.Address;
            v.Company = vendor.Company;
            v.CreatedAt = DateTime.Now;
            v.DistrictId = vendor.DistrictId;
            v.Email = vendor.Email;
            if (v.Note != null && v.Note.Trim().Length > 0)
                v.Note = vendor.Note;
            v.ProvinceId = vendor.ProvinceId;
            v.ShopId = ShopId;
            v.UpdatedAt = DateTime.Now;            
            v.VendorName = vendor.VendorName;
            v.Phone = vendor.Phone;
            v.Status = 0;
            string code = generateCode();
            if (code != vendor.VendorCode)
                v.VendorCode = vendor.VendorCode;
            else
                v.VendorCode = code;
            v.SearchQuery = Utils.unsignString(v.VendorName + " " + v.VendorCode);
            db.Vendors.Add(v);
            db.SaveChanges();
            vendor.VendorId = v.VendorId;
            if (v.Note != null && v.Note.Trim().Length > 0)
            {
                Note note = new Note();
                note.Content = v.Note;
                note.CreatedAt = DateTime.Now;
                note.ShopId = ShopId;
                note.Status = 0;
                note.TargetId = v.VendorId;
                note.TargetType = "VENDOR";
                db.Notes.Add(note);
                db.SaveChanges();
            }

            def.meta = new Meta(200, "Success");
            def.data = vendor;

            return Ok(def);
        }

        // DELETE: api/Vendors/5        
        public IHttpActionResult DeleteVendor(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int EmployeeId = Int32.Parse(identity.Claims.Where(c => c.Type == "EmployeeId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Vendor vendor = db.Vendors.Find(id);
            if (vendor == null || vendor.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            vendor.Status = 99;
            Action action = new Action();
            action.ActionName = "xóa nhà cung cấp";
            action.ActionType = "DELETE";
            action.CreatedAt = DateTime.Now;
            action.EmployeeId = EmployeeId;
            action.Result = 0;
            action.ShopId = ShopId;
            action.TargetId = vendor.VendorId;
            action.TargetType = "VENDOR";
            db.Actions.Add(action);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        [HttpGet]
        [Route("api/vendors/{id}/cashes")]
        public IHttpActionResult getCashes(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from c in db.Cashes
                          where c.PaidTargetId == id && c.PaidTargetType == "VENDOR"
                          orderby c.CreatedAt descending
                          select new SaleManager.Data.VendorCash
                          {
                              CashCode = c.CashCode,
                              CashId = c.CashId,
                              cashType = new Data.CashType()
                              {
                                  CashTypeId = c.CashType.CashTypeId,
                                  Name = c.CashType.Name,
                                  Type = c.CashType.Type
                              },
                              employee = new Data.Employee()
                              {
                                  EmployeeId = c.Employee.EmployeeId,
                                  EmployeeName = c.Employee.EmployeeName
                              },
                              PaidAmount = c.PaidAmount,
                              Title = c.Title,
                              Total = c.Total,
                              Status = c.Status
                          }
                         ).AsEnumerable();
            int count = result != null ? result.Count() : 0;
            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(count);
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/vendors/{id}/exims")]
        public IHttpActionResult getExims(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            var result = (from e in db.Exims
                          join em in db.Employees on e.EmployeeId equals em.EmployeeId
                          where e.TargetId == id && e.TargetType == "VENDOR"
                          orderby e.CreatetAt descending
                          select new SaleManager.Data.Exim
                          {
                              Code = e.Code,
                              CreatetAt = e.CreatetAt,
                              DeliveryStatus = e.DeliveryStatus,
                              employee = new Data.Employee()
                              {
                                  EmployeeId = em.EmployeeId,
                                  EmployeeName = em.EmployeeName
                              },
                              EximId = e.EximId,
                              PaymentStatus = e.PaymentStatus,
                              Price = e.Price,
                              Discount = e.Discount,
                              Fee = e.Fee,
                              Status = e.Status,
                              TargetId = e.TargetId,
                              TargetType = e.TargetType,
                              Title = e.Title,
                              Type = e.Type,
                              UpdatedAt = e.UpdatedAt
                          }).AsEnumerable();
            int count = result != null ? result.Count() : 0;
            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(count);
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/vendors/{id}/eximproduct")]
        public IHttpActionResult getEximProduct(int id, [FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());

            DefaultResponse def = new DefaultResponse();
            var result = (from e in db.Exims
                          join ep in db.EximProducts on e.EximId equals ep.EximId
                          where e.TargetId == id && e.TargetType == "VENDOR"
                          orderby e.CreatetAt descending
                          group e by new 
                          { 
                                e.EximId
                          }into gep
                          select new 
                          {
                              Code = gep.FirstOrDefault().Code,
                              CreatetAt = gep.FirstOrDefault().CreatetAt,
                              TotalProduct= gep.Sum(e=>e.EximProducts.FirstOrDefault().Quantity),
                              Price = gep.FirstOrDefault().Price,
                              Discount = gep.FirstOrDefault().Discount
                          }).AsEnumerable();

            int count = result != null ? result.Count() : 0;
            result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
            def.meta = new Meta(200, "Success");
            def.metadata = new Metadata(count);
            def.data = result;
            return Ok(def);
        }

        [HttpGet]
        [Route("api/vendors/{id}/cashflows")]
        public IHttpActionResult GetCashFlows(int id, [FromUri] FilteredPagination paging)
        {
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                var result = (from cf in db.CashFlows
                              where cf.PaidTargetId == id && cf.PaidTargetType == "VENDOR"
                              orderby cf.CreatedAt descending
                              select new Data.CashFlow
                              {
                                  Amount = cf.Amount,
                                  CreatedAt = cf.CreatedAt,
                                  Debt = cf.Debt,
                                  Type = cf.Type,
                                  TargetId = cf.TargetId,
                                  TargetType = cf.TargetType
                              }).AsEnumerable();
                int count = result != null ? result.Count() : 0;
                result = result.Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                //target
                var returnData = result.ToList();
                //select target
                for (int i = 0; i < returnData.Count; i++)
                {
                    int targetId = (int)returnData[i].TargetId;
                    switch (returnData[i].TargetType)
                    {
                        case "IMPORT":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId).Select(c => new Target()
                            {
                                TargetCode = c.Code,
                                TargetId = c.EximId
                            }).FirstOrDefault();
                            break;
                        case "CASH":
                            returnData[i].target = db.Cashes.Where(c => c.CashId == targetId).Select(c => new Target()
                            {
                                TargetId = c.CashId,
                                TargetCode = c.CashCode
                            }).FirstOrDefault();
                            break;
                        case "CANCELED":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId).Select(c => new Target()
                            {
                                TargetCode = c.Code,
                                TargetId = c.EximId
                            }).FirstOrDefault();
                            break;
                        case "IMPORT_RETURN":
                            returnData[i].target = db.Exims.Where(c => c.EximId == targetId).Select(c => new Target()
                            {
                                TargetCode = c.Code,
                                TargetId = c.EximId
                            }).FirstOrDefault();
                            break;
                        default:
                            returnData[i].target = null;
                            break;
                    }
                }
                def.meta = new Meta(200, "Success");
                def.metadata = new Metadata(count);
                def.data = returnData;
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        private string generateCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            int count = db.Vendors.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopVendorPrefix + code;
            return code;
        }

        [HttpGet]
        [Route("api/vendors/getVendorCode")]
        public IHttpActionResult getVendorCode()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            int count = db.Vendors.Where(o => o.ShopId == ShopId).Count() + 1;
            Shop shop = db.Shops.Find(ShopId);
            string code = "";
            if (count >= 100000)
                code = count + "";
            else if (count >= 10000 && count < 100000)
                code = "0" + count;
            else if (count >= 1000 && count < 10000)
                code = "00" + count;
            else if (count >= 100 && count < 1000)
                code = "000" + count;
            else if (count >= 10 && count < 100)
                code = "0000" + count;
            else
                code = "00000" + count;
            code = shop.ShopVendorPrefix + code;
            def.meta = new Meta(200, "Success");
            def.data = code;
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VendorExists(int id)
        {
            return db.Vendors.Count(e => e.VendorId == id) > 0;
        }
    }
}