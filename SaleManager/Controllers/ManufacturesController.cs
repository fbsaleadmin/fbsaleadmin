﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class ManufacturesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        [Route("api/manufactures/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Manufacture> manufactures = db.Manufactures;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    manufactures = manufactures.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    manufactures = manufactures.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    manufactures = manufactures.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    manufactures = manufactures.OrderBy("ManufactureId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }              
                def.data = manufactures.Select(m => new ManufactureDTO()
                {
                    ManufactureId = m.ManufactureId,
                    Name = m.Name,
                    Origin = m.Origin,
                    ShopId = m.ShopId
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }

        // GET: api/Manufactures/5       
        public IHttpActionResult GetManufacture(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            ManufactureDTO manufacture = db.Manufactures.Where(m => m.ManufactureId == id).Select(m => new ManufactureDTO()
            {
                ManufactureId = m.ManufactureId,
                Name = m.Name,
                Origin = m.Origin,
                ShopId = m.ShopId
            }).FirstOrDefault();
            if (manufacture == null || manufacture.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = manufacture;
            return Ok(def);
        }
     
        public IHttpActionResult PutManufacture(int id, ManufactureDTO manufacture)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != manufacture.ManufactureId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Manufacture current = db.Manufactures.Where(s => s.ManufactureId == id).FirstOrDefault();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {

                current.Name = manufacture.Name;
                current.Origin = manufacture.Origin;

                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Manufactures        
        public IHttpActionResult PostManufacture(ManufactureDTO manufacture)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Manufacture m = new Manufacture();
            m.Name = manufacture.Name;
            m.Origin = manufacture.Origin;
            m.ShopId = ShopId;
        
            db.Manufactures.Add(m);
            db.SaveChanges();
            manufacture.ManufactureId = m.ManufactureId;

            def.meta = new Meta(200, "Success");
            def.data = manufacture;

            return Ok(def);
        }

        // DELETE: api/Manufactures/5      
        public IHttpActionResult DeleteManufacture(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Manufacture manufacture = db.Manufactures.Find(id);
            if (manufacture == null || manufacture.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            db.Manufactures.Remove(manufacture);
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ManufactureExists(int id)
        {
            return db.Manufactures.Count(e => e.ManufactureId == id) > 0;
        }
    }
}