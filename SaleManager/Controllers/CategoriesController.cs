﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SaleManager;
using SaleManager.Models;
using System.Web;
using System.Security.Claims;

namespace SaleManager.Controllers
{
    [Authorize]
    public class CategoriesController : ApiController
    {
        private SalesManagerEntities db = new SalesManagerEntities();
     
        [Route("api/categories/GetByPage")]
        public IHttpActionResult GetByPage([FromUri] FilteredPagination paging)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (paging != null)
            {
                def.meta = new Meta(200, "Success");
                IQueryable<Category> categories = db.Categories;
                if (paging.query != null)
                {
                    paging.query = "ShopId=" + ShopId + " and " + HttpUtility.UrlDecode(paging.query);
                    categories = categories.Where(paging.query);
                }
                else
                {
                    paging.query = "ShopId=" + ShopId;
                    categories = categories.Where(paging.query);
                }
                if (paging.order_by != null)
                {
                    categories = categories.OrderBy(paging.order_by).Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }
                else
                {
                    categories = categories.OrderBy("CategoryId desc").Skip((paging.page - 1) * paging.page_size).Take(paging.page_size);
                }               
                def.data = categories.Select(c => new CategoryDTO()
                {
                    AvailableBranchIds = c.AvailableBranchIds,
                    CategoryDesc = c.CategoryDesc,
                    CategoryId = c.CategoryId,
                    CategoryName = c.CategoryName,
                    CategoryParentId = c.CategoryParentId,
                    CreatedAt = c.CreatedAt,
                    ShopId = c.ShopId,
                    Status = c.Status,
                    UpdatedAt = c.UpdatedAt
                });
                return Ok(def);
            }
            else
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
        }


        // GET: api/Categories/5       
        public IHttpActionResult GetCategory(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            CategoryDTO category = db.Categories.Where(c => c.CategoryId == id).Select(c => new CategoryDTO()
            {
                AvailableBranchIds = c.AvailableBranchIds,
                CategoryDesc = c.CategoryDesc,
                CategoryId = c.CategoryId,
                CategoryName = c.CategoryName,
                CategoryParentId = c.CategoryParentId,
                CreatedAt = c.CreatedAt,
                ShopId = c.ShopId,
                Status = c.Status,
                UpdatedAt = c.UpdatedAt,
            }).FirstOrDefault();
            if (category == null || category.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            def.meta = new Meta(200, "Success");
            def.data = category;
            return Ok(def);
        }

        // PUT: api/Categories/5       
        public IHttpActionResult PutCategory(int id, CategoryDTO category)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            if (id != category.CategoryId)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }
            Category current = db.Categories.Where(s => s.CategoryId == id).FirstOrDefault();
            int countChil = db.Categories.Where(s => s.CategoryParentId == category.CategoryId).Count();
            if (current == null || current.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }
            else
            {
                current.AvailableBranchIds = category.AvailableBranchIds;
                current.CategoryDesc = category.CategoryDesc;
                current.CategoryName = category.CategoryName;
                current.CategoryParentId = category.CategoryParentId;
                current.Status = category.Status;
                current.UpdatedAt = DateTime.Now;   
                       
                db.Entry(current).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    def.meta = new Meta(200, "Success");
                    return Ok(def);
                }
                catch (DbUpdateConcurrencyException)
                {
                    def.meta = new Meta(500, "Internal Server Error");
                    return Ok(def);
                }
            }  
        }

        // POST: api/Categories       
        public IHttpActionResult PostCategory(CategoryDTO category)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            if (!ModelState.IsValid)
            {
                def.meta = new Meta(400, "Bad Request");
                return Ok(def);
            }

            Category cate = new Category();
            cate.AvailableBranchIds = category.AvailableBranchIds;
            cate.CategoryDesc = category.CategoryDesc;
            cate.CategoryName = category.CategoryName;
            cate.CategoryParentId = category.CategoryParentId;
            cate.CreatedAt = DateTime.Now;
            cate.ShopId = ShopId;
            cate.Status = category.Status;
            cate.UpdatedAt = DateTime.Now;

            db.Categories.Add(cate);
            db.SaveChanges();
            category.CategoryId = cate.CategoryId;
            category.CreatedAt = cate.CreatedAt;
            category.UpdatedAt = cate.UpdatedAt;

            def.meta = new Meta(200, "Success");
            def.data = category;

            return Ok(def);
        }

        // DELETE: api/Categories/5        
        public IHttpActionResult DeleteCategory(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            int ShopId = Int32.Parse(identity.Claims.Where(c => c.Type == "ShopId").Select(c => c.Value).SingleOrDefault());
            DefaultResponse def = new DefaultResponse();
            Category category = db.Categories.Find(id);
           
            if (category == null || category.ShopId != ShopId)
            {
                def.meta = new Meta(404, "Not Found");
                return Ok(def);
            }

            if (category.CategoryParentId == 0)
            {
                var cate = db.Categories.Where(e => e.CategoryParentId == id).ToList();
                if (cate != null)
                {
                    foreach (var cat in cate)
                    {
                        var product = db.Products.Where(e => e.CategoryId == cat.CategoryId).ToList();
                        if (product != null)
                        {
                            foreach (var pd in product)
                            {
                                var pda = db.ProductAttributes.Where(e => e.ProductId == pd.ProductId);
                                if (pda != null)
                                {
                                    foreach (var pa in pda)
                                    {
                                        pa.Status = 99;
                                        db.Entry(pa).State = EntityState.Modified;
                                    }
                                }

                                pd.Status = 99;
                                db.Entry(pd).State = EntityState.Modified;
                            }
                        }
                        cat.Status = 99;
                        db.Entry(category).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            else
            {
                var product = db.Products.Where(e => e.CategoryId == category.CategoryId).ToList();
                if (product != null)
                {
                    foreach (var pd in product)
                    {
                        var pda = db.ProductAttributes.Where(e => e.ProductId == pd.ProductId);
                        if (pda != null)
                        {
                            foreach (var pa in pda)
                            {
                                pa.Status = 99;
                                db.Entry(pa).State = EntityState.Modified;
                            }
                        }

                        pd.Status = 99;
                        db.Entry(pd).State = EntityState.Modified;
                    }
                }
            }

            category.Status = 99;
            db.Entry(category).State = EntityState.Modified;
            db.SaveChanges();

            def.meta = new Meta(200, "Success");
            return Ok(def);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.CategoryId == id) > 0;
        }
    }
}