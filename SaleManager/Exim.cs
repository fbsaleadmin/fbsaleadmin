//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaleManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Exim
    {
        public Exim()
        {
            this.EximProducts = new HashSet<EximProduct>();
        }
    
        public int EximId { get; set; }
        public string Code { get; set; }
        public Nullable<int> ShopId { get; set; }
        public Nullable<int> TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Title { get; set; }
        public Nullable<int> PaymentStatus { get; set; }
        public Nullable<int> DeliveryStatus { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<System.DateTime> CreatetAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<System.DateTime> CustomDate { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> RelatedEximId { get; set; }
    
        public virtual ICollection<EximProduct> EximProducts { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
