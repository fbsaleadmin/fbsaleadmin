﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Security.Claims;
using SaleManager;


//old
//[assembly: OwinStartup(typeof(SaleManager.Startup))]

//namespace SaleManager
//{
//    public partial class Startup
//    {
//        public void Configuration(IAppBuilder app)
//        {
//            ConfigureAuth(app);
//        }
//    }
//}


//new
[assembly: OwinStartup(typeof(SaleManager.Startup))]
namespace SaleManager
{
    public class Startup
    {
        private SalesManagerEntities db;
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            //other configurations

            ConfigureOAuth(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/security/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(24),
                Provider = new AuthorizationServerProvider()
            };

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }

    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private SalesManagerEntities db = new SalesManagerEntities();

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            //var form = await context.Request.ReadFormAsync();
            ////form shopid
            //string ShopId = form["shop_id"].ToString();
            
            try
            {
                //retrieve your user from database. ex:
                //var user = await userService.Authenticate(context.UserName, context.Password);
                db = new SalesManagerEntities();
                string UserName = context.UserName;
                string Password = context.Password.ToLower();                
                var user = db.Employees.Where(e => e.Email == UserName && e.Password == Password).FirstOrDefault();
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                identity.AddClaim(new Claim(ClaimTypes.Name, user.EmployeeName));
                identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
                identity.AddClaim(new Claim("UserId", user.UserId != null ? user.UserId.ToString() : ""));
                //find shop id 
                identity.AddClaim(new Claim("ShopId", user.ShopId.ToString()));
                identity.AddClaim(new Claim("EmployeeId", user.EmployeeId.ToString()));

                //roles example
                //var rolesTechnicalNamesUser = new List<string>();

                //if (user.Roles != null)
                //{
                //    rolesTechnicalNamesUser = user.Roles.Select(x => x.TechnicalName).ToList();

                //    foreach (var role in user.Roles)
                //        identity.AddClaim(new Claim(ClaimTypes.Role, role.TechnicalName));
                //}

                //var principal = new GenericPrincipal(identity, rolesTechnicalNamesUser.ToArray());

                //Thread.CurrentPrincipal = principal;

                context.Validated(identity);
            }
            catch (Exception ex)
            {

                context.SetError("invalid_grant", ex.Message + "-" + ex.StackTrace); 
            }
        }
    }
}