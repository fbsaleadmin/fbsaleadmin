﻿var myApp = angular.module('FSaleApp', ['ngPrint', 'ngJsonExportExcel', 'ngDialog', 'ngMd5', 'ngRoute', 'wu.masonry', 'ui.bootstrap', 'ngFileUpload', 'ui.bootstrap.datetimepicker', 'angular-loading-bar', 'yaru22.angular-timeago', 'chart.js', 'ngMaterial']);
myApp.value('config', { domain: 'https://apiv1.fb.sale/' });
myApp.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}])

myApp.filter('iif', function () {
    return function (input, trueValue, falseValue) {
        return input ? trueValue : falseValue;
    };
});

myApp.config(function (timeAgoSettings) {
    timeAgoSettings.strings.vi_VN = {
        prefixAgo: null,
        prefixFromNow: null,
        suffixAgo: 'trước',
        suffixFromNow: 'từ bây giờ',
        seconds: 'nhỏ hơn 1 phút',
        minute: 'khoảng 1 phút',
        minutes: '%d phút',
        hour: 'khoảng 1 giờ',
        hours: 'khoảng %d giờ',
        day: 'một ngày',
        days: '%d ngày',
        month: 'khoảng 1 tháng',
        months: '%d tháng',
        year: 'khoảng 1 năm',
        years: '%d năm',
        numbers: []
    };
});

var _fileReader = function ($q, $log) {

    var onLoad = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };

    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };

    var onProgress = function (reader, scope) {
        return function (event) {
            scope.$broadcast("fileProgress",
                {
                    total: event.total,
                    loaded: event.loaded
                });
        };
    };

    var getReader = function (deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readAsDataURL = function (file, scope) {
        var deferred = $q.defer();

        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);

        return deferred.promise;
    };

    return {
        readAsDataUrl: readAsDataURL
    };
};

myApp.factory("fileReader",
               ["$q", "$log", _fileReader]);

myApp.directive('fb', ['$FB', function ($FB) {
    return {
        restrict: "E",
        replace: true,
        template: "<div id='fb-root'></div>",
        compile: function (tElem, tAttrs) {
            return {
                post: function (scope, iElem, iAttrs, controller) {
                    var fbAppId = iAttrs.appId || '';

                    var fb_params = {
                        appId: iAttrs.appId || "",
                        cookie: iAttrs.cookie || true,
                        status: iAttrs.status || true,
                        xfbml: iAttrs.xfbml || true
                    };

                    // Setup the post-load callback
                    window.fbAsyncInit = function () {
                        $FB._init(fb_params);

                        if ('fbInit' in iAttrs) {
                            iAttrs.fbInit();
                        }
                    };

                    (function (d, s, id, fbAppId) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id; js.async = true;
                        js.src = "//connect.facebook.net/en_US/all.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk', fbAppId));
                }
            }
        }
    };
}]);

myApp.factory('$FB', ['$rootScope', function ($rootScope) {

    var fbLoaded = false;

    // Our own customisations
    var _fb = {
        loaded: fbLoaded,
        _init: function (params) {
            if (window.FB) {
                // FIXME: Ugly hack to maintain both window.FB
                // and our AngularJS-wrapped $FB with our customisations
                angular.extend(window.FB, _fb);
                angular.extend(_fb, window.FB);

                // Set the flag
                _fb.loaded = true;

                // Initialise FB SDK
                window.FB.init(params);

                if (!$rootScope.$$phase) {
                    $rootScope.$apply();
                }
            }
        }
    }

    return _fb;
}]);

myApp.directive('attributeset', function () {
    return {
        restrict: 'E',
        replace: false,

        scope: {
            attributes: "="
        },
        templateUrl: '/Scripts/directive/attribute.html',
        link: function (scope, element, attrs) {
            scope.attrs = { "data": [] };
            scope.internalControl = scope.control || {};
            scope.addAttr = function () {
                //check exist
                var isDup = false;
                for (var i = 0; i < scope.attrs.data.length; i++) {
                    if (scope.attrs.data[i].AttributeId == parseInt(scope.newAttr.AttributeId) || scope.newAttr.AttributeId == "-1")
                        isDup = true;
                }
                if (isDup)
                    return;
                if (!scope.newAttr.Value || /^\s*$/.test(scope.newAttr.Value))
                    return;
                scope.attrs.data.push(scope.newAttr);
                scope.newAttr = { "AttributeId": "-1", "Value": "" };
            }
            scope.deleteAttr = function (id) {
                //check exist                
                for (var i = 0; i < scope.attrs.data.length; i++) {
                    if (scope.attrs.data[i].AttributeId == id)
                        scope.attrs.data.splice(i, 1);
                }
            }
            scope.findName = function (id) {
                var name = "";
                for (var i = 0; i < scope.attributes.length; i++) {
                    if (scope.attributes[i].AttributeId == parseInt(id))
                        name = scope.attributes[i].AttributeName;
                }
                return name;
            }
            scope.Delete = function (e) {
                //remove element and also destoy the scope that element
                element.remove();
                scope.$destroy();
            }
        }
    };
});

myApp.directive('productunit', function () {
    return {
        restrict: 'E',
        replace: false,
        scope: {
        },
        templateUrl: '/Scripts/directive/unit.html',
        link: function (scope, element, attrs) {
            scope.Delete = function (e) {
                //remove element and also destoy the scope that element
                element.remove();
                scope.$destroy();
            }
        }
    };
});

myApp.directive('productimage', ['fileReader', function (fileReader) {
    return {
        restrict: 'E',
        replace: false,
        scope: {
            files: "="
        },
        templateUrl: '/Scripts/directive/productImage.html',
        link: function (scope, element, attrs) {
            scope.Delete = function (e, name) {
                if (scope.files == undefined || scope.files.length > 0) {
                    var index = 0;
                    for (var i = 0; i < scope.files.length; i++) {
                        if (name == scope.files[i].name)
                            index = i;
                    }
                    //remove
                    scope.files.splice(index, 1);
                }
                console.log(name);
            }

            scope.loadImage = function (file) {
                fileReader.readAsDataUrl(file, scope)
                      .then(function (result) {
                          file.base64String = result;
                      });
            }
        }
    };
}]);

myApp.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue, "", 0)
            });

            elem.bind('blur', function (event) {
                var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber, "", 0));
            });
        }
    };
}]);

myApp.directive("ngFileSelect", function () {

    return {
        link: function ($scope, el) {

            el.bind("change", function (e) {
                //check file exists
                var currentFiles = $scope.files;
                //check ext                
                var selectedFiles = (e.srcElement || e.target).files;
                var isValid = true;
                for (var i = 0; i < selectedFiles.length; i++) {
                    var ext = selectedFiles[i].name.split('.').pop();
                    if ("jpg|png|gif|JPG|PNG|GIF".indexOf(ext) == -1)
                        isValid = false;
                }
                if (!isValid)
                    return;
                if (currentFiles != undefined && currentFiles.length > 0) {
                    if (selectedFiles != undefined && selectedFiles.length > 0) {
                        for (var i = 0; i < selectedFiles.length; i++) {
                            var isDup = false;
                            for (var j = 0; j < currentFiles.length; j++) {
                                if (selectedFiles[i].name == currentFiles[j].name) {
                                    isDup = true;
                                    break;
                                }
                            }
                            if (!isDup) {
                                $scope.files.push(selectedFiles[i]);
                                //$scope.getFile(selectedFiles[i]);
                                $scope.$apply();
                            }
                        }
                    }
                }
                else {
                    var files = (e.srcElement || e.target).files;
                    $scope.files = new Array();
                    for (var i = 0; i < files.length; i++) {
                        //$scope.getFile(files[i]);
                        $scope.files.push(files[i]);
                        $scope.$apply();
                    }
                }
            })

        }

    }
})

myApp.directive('inputEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.inputEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//Xuất excel
myApp.factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return {
        report: function (tableId, worksheetName) {
            var table = $(tableId),
                ctx = { worksheet: worksheetName, table: table.html() },
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
});

//Export Table
myApp.directive('exportTable', function () {
    var link = function ($scope, elm, attr) {
        $scope.$on('export-pdf', function (e, d) {
            elm.tableExport({ type: 'pdf', escape: false });
        });
        $scope.$on('export-excel', function (e, d) {
            elm.tableExport({ type: 'excel', escape: false });
        });
        $scope.$on('export-doc', function (e, d) {
            elm.tableExport({ type: 'doc', escape: false });
        });
        $scope.$on('export-csv', function (e, d) {
            elm.tableExport({ type: 'csv', escape: false });
        });
    }
    return {
        restrict: 'C',
        link: link
    }
});

//Đinh dạng giá
myApp.filter("displayprice", function () {
    return function (input) {
        //console.log("displayprice: " + input);
        input = (typeof input === 'undefined' || input === '') ? "" : input + "";
        var comma = ".";
        var num = parseInt(input) ? parseInt(input.replace(/\./g, '')) : input;
        //var num = parseInt(input) ? parseInt(input.replace(/[^\d|\-+|\.+]/g, '')) : input;

        //console.log("displayprice2: " + input);
        var nums = 0;
        if (num >= 0) {
            nums = num;
        }
        else {
            nums = 0 - num;
        }
        nums = nums + "";

        var str = "";

        var k = (nums.length % 3);
        if (k > 0) {
            str += nums.substring(0, k) + comma;
        }

        while (k < nums.length) {

            str += nums.substring(k, k + 3) + comma;
            k = k + 3;
        }
        if (num >= 0) {
            str = str.substring(0, str.length - 1);
        }
        else {
            str = "-" + str.substring(0, str.length - 1);
        }
        return str;
    }
});

myApp.filter("formatPrice", function () {
    return function (price, digits, thoSeperator, decSeperator, bdisplayprice) {
        var i;
        price = (typeof price === "undefined") ? 0 : price;
        digits = (typeof digits === "undefined") ? 0 : digits;
        bdisplayprice = (typeof bdisplayprice === "undefined") ? true : bdisplayprice;
        thoSeperator = (typeof thoSeperator === "undefined") ? "." : thoSeperator;
        decSeperator = (typeof decSeperator === "undefined") ? "," : decSeperator;
        price = (typeof price === "") ? "0" : price;

        if (price != 0) {
            var prices = 0 - price;
            if (price > 0) {
                prices = price;
            }
            prices = prices + "";
            var _temp = prices.split('.');
            var dig = (typeof _temp[1] === "undefined") ? "00" : _temp[1];
            if (bdisplayprice && parseInt(dig, 10) === 0) {
                dig = "";
            } else {
                dig = dig + "";
                if (dig.length > digits) {
                    dig = (Math.round(parseFloat("0." + dig) * Math.pow(10, digits))) + "";
                }
                for (i = dig.length; i < digits; i++) {
                    dig += "0";
                }
            }
            var num = _temp[0];
            var s = "",
                ii = 0;
            for (i = num.length - 1; i > -1; i--) {
                s = ((ii++ % 3 === 2) ? ((i > 0) ? thoSeperator : "") : "") + num.substr(i, 1) + s;
            }
        }
        else {
            s = 0;
        }

        if (price < 0) {
            s = '- ' + s;;
        }
        if (dig > 0) {
            return s + decSeperator + dig;
        }
        else {
            return s;
        }
    }
});

//Đinh dạng độ dài chuỗi
myApp.filter("displaystring", function () {
    return function (input, k) {
        input = (typeof input === 'undefined' || input === '') ? "" : input + "";
        var str = input.length > k ? input.substring(0, input.substring(0, k).lastIndexOf(' ')) + "..." : input
        return str;
    }
});

// chuyen chu hoa thanh chu thuong va chuyen tieng viet khong dau thanh co dau
myApp.formatSpecialchar = function (ystring) {
    if (ystring) {
        ystring = ystring.toLowerCase();
        ystring = ystring.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
    }
    else {
        ystring = '';
    }
    return ystring;
}

myApp.controller('LoginController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', function LoginController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog) {
    $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    $scope.phoneFormat = /^[0-9]{10,11}$/;
    $scope.isCheckEmail = false;
    $scope.isUniqueCheck = false;

    //user
    $scope.checkExistEmail = function () {
        if ($scope.email != undefined) {
            $http.get("/api/app/checkEmailExist" + "?email=" + $scope.email).success(function (data, status, headers) {
                if (data.meta.error_code == 200) {
                    $scope.isCheckEmail = true;
                }
                else
                    $scope.isCheckEmail = false;
            }).error(function (data, status, headers, config) {
                $scope.isCheckEmail = false;
            });
        }
        else
            $scope.isCheckEmail = false;
    };

    $scope.changeTab = function () {
        //active register
        var tabRegister = angular.element(document.querySelector('#tabRegister'));
        tabRegister.addClass('active');
        var profile = angular.element(document.querySelector('#profile'));
        profile.addClass('active');
        var tabLogin = angular.element(document.querySelector('#tabLogin'));
        tabLogin.removeClass('active');
        var home = angular.element(document.querySelector('#home'));
        home.removeClass('active');
    }

    $scope.loginFacebook = function () {
        cfpLoadingBar.start();
        FB.login(function (response) {

            if (response.authResponse) {
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID

                FB.api('/me?fields=email,name,id', function (response) {
                    user_email = response.email; //get user email
                    var apiUrl = '/api/app/loginfacebook';

                    var login = {
                        email: user_email,
                        id: user_id,
                        token: access_token,
                        name: response.name
                    };

                    var post = $http({
                        method: "POST",
                        url: apiUrl,
                        data: login
                    });

                    post.success(function successCallback(data, status, headers, config) {
                        // success      
                        cfpLoadingBar.complete();
                        if (data.meta.error_code == 200) {
                            //redirect
                            cfpLoadingBar.complete();
                            $window.location.href = '/cmsadmin.html';
                        }
                        else if (data.meta.error_code == 500) {
                            //redirect
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Đăng ký thất bại, email hoặc tài khoản facebook đã tồn tại!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                        else if (data.meta.error_code == 404) {
                            //redirect
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                        else if (data.meta.error_code == 400) {
                            //redirect
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Lỗi trong quá trình xử lý, vui lòng thử lại sau!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                        else {
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                    })
                    .error(function (data, status, headers, config) { // optional
                        $mdDialog.show(
                              $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title('Thông tin')
                                .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                                .ok('Đóng')
                                .fullscreen(true)
                            );
                    });
                });

            } else {
                //user hit cancel button

            }
        }, {
            scope: 'email,manage_pages,pages_messaging,pages_messaging_subscriptions,pages_messaging_phone_number,pages_show_list,publish_pages,read_page_mailboxes'
            //scope: 'email'
        });
    }

    $scope.login = function () {

        cfpLoadingBar.start();
        var apiUrl = '/api/app/login';

        var email = angular.element(document.querySelector('#txtLoginEmail')).val();
        var password = angular.element(document.querySelector('#txtLoginPassword')).val();

        if (email == undefined || password == undefined || email == "" || password == "") {
            $mdDialog.show(
                $mdDialog.alert()
                  .clickOutsideToClose(true)
                  .title('Thông tin')
                  .textContent('Vui lòng điền đẩy đủ thông tin!')
                  .ok('Đóng')
                  .fullscreen(true)
              );
            return;
        }

        var login = {
            email: email,
            password: md5.createHash(password || '')
        };

        var post = $http({
            method: "POST",
            url: apiUrl,
            data: login
        });

        post.success(function successCallback(data, status, headers, config) {
            // success      
            if (data.meta.error_code == 200) {
                //redirect
                cfpLoadingBar.complete();
                $window.location.href = '/cmsadmin.html';
            }
            else if (data.meta.error_code == 404) {
                //redirect
                cfpLoadingBar.complete();
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
            }
            else if (data.meta.error_code == 400) {
                //redirect
                cfpLoadingBar.complete();
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Lỗi trong quá trình xử lý, vui lòng thử lại sau!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
            }
            else {
                cfpLoadingBar.complete();
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                    .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
        });
    }

    //shop
    //$scope.checkUniqueName = function () {
    //    if ($scope.uniqueName != undefined) {
    //        $http.get("/api/app/checkUniqueName" + "?name=" + $scope.uniqueName).success(function (data, status, headers) {
    //            if (data.meta.error_code == 200) {
    //                $scope.isUniqueCheck = true;
    //            }
    //            else
    //                $scope.isUniqueCheck = false;
    //        }).error(function (data, status, headers, config) {
    //            $scope.isUniqueCheck = false;
    //        });
    //    }
    //    else
    //        $scope.isUniqueCheck = false;
    //};

    //$scope.createShop = function () {        
    //    cfpLoadingBar.start();
    //    if ($scope.frmCreateShop.$valid && $scope.isUniqueCheck) {
    //        var shop = {
    //            ShopName: $scope.shopName,
    //            Address: $scope.shopAddress,
    //            Phone: $scope.shopPhone,
    //            ShopUniqueName: $scope.uniqueName
    //        };

    //        var post = $http({
    //            method: "POST",
    //            url: "/api/app/createShop",
    //            data: shop
    //        });

    //        post.success(function successCallback(data, status, headers, config) {
    //            // success                
    //            if (data.meta.error_code == 200) {                  
    //                //var IsTokenValid = $scope.IsTokenValid == "1" || $scope.IsTokenValid == 1 ? true : false;
    //                //if (IsTokenValid)
    //                //    angular.element(document.querySelector('#pageModal')).modal({ backdrop: 'static', keyboard: true })
    //                //else
    //                //    $scope.goHome();
    //                $window.location.href = '/link-page.html';
    //            }
    //            else {
    //                $mdDialog.show(
    //                  $mdDialog.alert()
    //                    .clickOutsideToClose(true)
    //                    .title('Thông tin')
    //                    .textContent('Tạo cửa hàng thất bại, vui lòng thử lại sau!')
    //                    .ok('Đóng')
    //                    .fullscreen(true)
    //                );
    //            }
    //        })
    //                .error(function (data, status, headers, config) { // optional
    //                    $mdDialog.show(
    //                      $mdDialog.alert()
    //                        .clickOutsideToClose(true)
    //                        .title('Thông tin')
    //                        .textContent('Tạo cửa hàng thất bại, vui lòng thử lại sau!')
    //                        .ok('Đóng')
    //                        .fullscreen(true)
    //                    );
    //                });
    //    }
    //} 
}]);

myApp.controller('LandingPageController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', '$FB', '$filter', '$compile', function LandingPageController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast, $FB, $filter, $compile) {

    $scope.countLinked = 0;
    $scope.loggedFacebookUserId = "";

    $scope.init = function () {
        if ($scope.IsLogged == 1) {
            cfpLoadingBar.complete();
            $scope.getPages();
            angular.element(document.querySelector('#linkPageModal')).modal('toggle');
        }
        else {
            $scope.Group = "";
        }
    }

    $scope.$watch(function () { return $FB.loaded }, function () {
        if ($FB.loaded) {
            if ($scope.IsLogged == 1) {
                cfpLoadingBar.complete();
                $scope.getPages();
                angular.element(document.querySelector('#linkPageModal')).modal('toggle');
            }
            else {
                $scope.Group = "";
            }
        }
    });

    $scope.loginFacebook = function () {
        cfpLoadingBar.start();
        $FB.login(function (response) {

            if (response.authResponse) {
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID
                $scope.loggedFacebookUserId = response.authResponse.userID;
                angular.element(document.querySelector('#loginModal')).modal('toggle');
                $FB.api('/me?fields=email,name,id', function (response) {
                    user_email = response.email; //get user email

                    var apiUrl = '/api/app/loginfacebook';

                    var login = {
                        email: user_email,
                        id: user_id,
                        token: access_token,
                        name: response.name
                    };

                    var post = $http({
                        method: "POST",
                        url: apiUrl,
                        data: login
                    });

                    post.success(function successCallback(data, status, headers, config) {
                        // success      
                        cfpLoadingBar.complete();
                        ///Shared/BoxLogin
                        if (data.meta.error_code == 200) {

                            $("#boxLogin").load("/Shared/BoxLogin", function (data) {
                                var target = $compile(data)($scope);
                                $("#boxLogin").html(target);
                            });
                            if (data.meta.error_code == 200) {
                                //redirect                         
                                cfpLoadingBar.complete();
                                $scope.goSelectPage();
                            }
                        }
                        else if (data.meta.error_code == 500) {
                            //redirect
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Đăng ký thất bại, email hoặc tài khoản facebook đã tồn tại!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                        else if (data.meta.error_code == 404) {
                            //redirect
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                        else if (data.meta.error_code == 400) {
                            //redirect
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Lỗi trong quá trình xử lý, vui lòng thử lại sau!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                        else {
                            cfpLoadingBar.complete();
                            $mdDialog.show(
                             $mdDialog.alert()
                               .clickOutsideToClose(true)
                               .title('Thông tin')
                               .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                               .ok('Đóng')
                               .fullscreen(true)
                           );
                        }
                    })
                    .error(function (data, status, headers, config) { // optional
                        $mdDialog.show(
                              $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title('Thông tin')
                                .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                                .ok('Đóng')
                                .fullscreen(true)
                            );
                    });
                });

            } else {
                //user hit cancel button

            }
        }, {
            scope: 'email,manage_pages,pages_messaging,pages_messaging_subscriptions,pages_messaging_phone_number,pages_show_list,publish_pages,read_page_mailboxes'
            //scope: 'email'
        });
    }

    $scope.login = function () {
        cfpLoadingBar.start();
        var apiUrl = '/api/app/login';

        var email = angular.element(document.querySelector('#txtLoginEmail')).val();
        var password = angular.element(document.querySelector('#txtLoginPassword')).val();

        if (email == undefined || password == undefined || email == "" || password == "") {
            $mdDialog.show(
                $mdDialog.alert()
                  .clickOutsideToClose(true)
                  .title('Thông tin')
                  .textContent('Vui lòng điền đẩy đủ thông tin!')
                  .ok('Đóng')
                  .fullscreen(true)
              );
            return;
        }

        var login = {
            email: email,
            password: md5.createHash(password || '')
        };

        var post = $http({
            method: "POST",
            url: apiUrl,
            data: login
        });

        post.success(function successCallback(data, status, headers, config) {
            // success      
            if (data.meta.error_code == 200) {

                $("#boxLogin").load("/Shared/BoxLogin", function (data) {
                    var target = $compile(data)($scope);
                    $("#boxLogin").html(target);
                });
                if (data.meta.error_code == 200) {
                    //redirect                  
                    cfpLoadingBar.complete();
                    $scope.goSelectPage();
                }
            }
            else if (data.meta.error_code == 404) {
                //redirect
                cfpLoadingBar.complete();
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
            }
            else if (data.meta.error_code == 400) {
                //redirect
                cfpLoadingBar.complete();
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Lỗi trong quá trình xử lý, vui lòng thử lại sau!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
            }
            else {
                cfpLoadingBar.complete();
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                    .textContent('Đăng nhập thất bại, vui lòng kiểm tra lại email và mật khẩu!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
        });
    }

    $scope.goSelectPage = function () {
        $window.location.href = '/cmsadmin.html';
    }
}]);

myApp.controller('SelectPageController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', '$FB', '$filter', '$compile', '$interval', function SelectPageController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast, $FB, $filter, $compile, $interval) {
    $scope.progressDownload = 0;
    $scope.progress = 0;
    $scope.countLinked = 0;
    $scope.loggedFacebookUserId = "";

    $scope.init = function () {
        $scope.getPages();
    }

    $scope.$watch(function () { return $FB.loaded }, function () {
        if ($FB.loaded) {
            if ($scope.IsLogged == 1) {
                cfpLoadingBar.complete();
                $scope.getPages();
            }
            else {
                $scope.Group = "";
            }
        }
    });

    $scope.getPages = function () {
        $http.get("/api/app/getPages").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.pages = data.data;
                if ($scope.pages == undefined)
                    $scope.pages = new Array();
                else {
                    for (var i = 0; i < $scope.pages.length; i++) {
                        $scope.pages[i].isAdded = true;
                        if ($scope.pages[i].isSubscribed)
                            $scope.countLinked += 1;
                    }
                }
                if ($scope.Group == 'Admin') {
                    //get page by token if valid

                    $FB.getLoginStatus(function (response) {
                        if (response.status === 'connected') {
                            $scope.loggedFacebookUserId = response.authResponse.userID;
                            if ($scope.loggedFacebookUserId == $scope.FacebookUserId) {
                                $FB.api(
                             '/me/accounts',
                             'GET',
                             {},
                             function (response) {
                                 if (response != undefined) {
                                     if (response.data != undefined) {
                                         for (var i = 0; i < response.data.length; i++) {
                                             var isDup = false;
                                             for (var j = 0; j < $scope.pages.length; j++) {
                                                 if (response.data[i].id == $scope.pages[j].id) {
                                                     isDup = true;
                                                     break;
                                                 }
                                             }
                                             if (!isDup) {
                                                 response.data[i].isAdded = false;
                                                 response.data[i].isSubscribed = false;
                                                 $scope.pages.push(response.data[i]);
                                                 $scope.$apply();
                                             }
                                         }
                                         //check facebook page removed
                                         for (var i = 0; i < $scope.pages.length; i++) {
                                             var isExist = false;
                                             for (var j = 0; j < response.data.length; j++) {
                                                 console.log($scope.pages[i].id + ":" + response.data[j].id);
                                                 if (response.data[j].id == $scope.pages[i].id) {
                                                     isExist = true;
                                                     break;
                                                 }
                                             }
                                             if (!isExist) {
                                                 //page has been removed
                                                 console.log($scope.pages[i]);
                                                 $scope.pages[i].isPageRemoved = true;
                                             }
                                         }
                                     }
                                     else
                                         $scope.showLogin = true;
                                 }
                                 else {
                                     //required login
                                     $scope.showLogin = true;
                                 }
                             }
                           );
                            }
                        } else if (response.status === 'not_authorized') {
                            // the user is logged in to Facebook, 
                            // but has not authenticated your app
                        } else {
                            // the user isn't logged in to Facebook.
                        }
                    });
                }

            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onLinkChanged = function (page) {
        console.log(page);
        if (!page.isSubscribed) {
            $scope.unlinkFanPage(page);
        }
        else {
            $scope.linkFanPage(page, true);
        }
    }

    var processInterval;

    $scope.increaseProgress = function () {
        if ($scope.progress < 99)
            $scope.progress += 1;
    }

    $scope.increaseProgressDownload = function () {
        if ($scope.progressDownload < 99)
            $scope.progressDownload += 1;
    }

    $scope.activePage = function () {
        $scope.linkFanPage($scope.selectedPage, false);
    }

    $scope.linkFanPage = function (page, isAdded) {
        //test
        $scope.progress = 0;
        processInterval = $interval($scope.increaseProgress, 200);
        var p = {
            id: page.id,
            page_name: page.name,
            access_token: page.access_token
        };

        var post = $http({
            method: "POST",
            url: "/api/app/linkPage",
            data: p
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            $scope.progress = 100;
            $interval.cancel(processInterval);
            if (data.meta.error_code == 200) {
                page.isSubscribed = true;
                page.PageId = data.data.PageId;
                //run download content
                if (!isAdded && $scope.selectedTime != "0") {
                    $scope.showDownloadContent();
                }
                else {
                    $mdToast.show($mdToast.simple()
                     .textContent('Liên kết page thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else if (data.meta.error_code == 212) {
                page.isSubscribed = false;
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Page này đã được liên kết với một cửa hàng khác, vui lòng chọn page khác!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else if (data.meta.error_code == 500) {
                page.isSubscribed = false;
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Page này đã được liên kết với một cửa hàng khác, vui lòng chọn page khác!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                page.isSubscribed = false;
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Có lỗi xảy ra vui lòng thử lại sau!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
            }
        })
                    .error(function (data, status, headers, config) {
                        page.isSubscribed = false;
                        $interval.cancel(processInterval);
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Có lỗi xảy ra vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                        page.isLinking = false;
                    });

    }

    $scope.unlinkFanPage = function (page) {
        page.isLinking = true;
        var post = $http({
            method: "POST",
            url: "/api/app/unlinkPage",
            data: page
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                page.isSubscribed = false;
                $mdToast.show($mdToast.simple()
                     .textContent('Hủy liên kết page thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Hủy liên kêt page thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            page.isLinking = false;
        })
            .error(function (data, status, headers, config) {
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Có lỗi xảy ra vui lòng thử lại sau!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
                page.isLinking = false;
            });
    }

    $scope.goHome = function () {
        $window.location.href = '/home.html';
    }

    $scope.goChat = function () {
        var countSelected = 0;
        for (var i = 0; i < $scope.pages.length; i++) {
            if ($scope.pages[i].isSelected)
                countSelected += 1;
        }
        if (countSelected > 0)
            $window.location.href = '/chat.html';
        else {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng chọn ít nhất một page đã liên kết để tiếp tục!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
        }
    }

    $scope.showDownloadContent = function (page) {
        $scope.selectedPage = page;
        angular.element(document.querySelector('#downloadContentModal')).modal('toggle');
    }

    $scope.downloadContent = function () {
        //fake progress
        $scope.progressDownload = 0;
        processInterval = $interval($scope.increaseProgressDownload, 300);
        $scope.selectedPage.isDownloading = true;
        var since = "";
        var today = new Date();
        if ($scope.selectedTime == "1") {
            today.setDate(today.getDate() - 1)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "2") {
            today.setDate(today.getDate() - 2)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "3") {
            today.setDate(today.getDate() - 3)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "5") {
            today.setDate(today.getDate() - 5)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "7") {
            today.setDate(today.getDate() - 7)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "20") {
            today.setDate(today.getDate() - 20)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        var download = {
            page_id: $scope.selectedPage.PageId,
            since: since
        }
        var post = $http({
            method: "POST",
            url: "/api/app/downloadContent",
            data: download
        });

        post.success(function successCallback(data, status, headers, config) {
            // success      
            if (data.meta.error_code == 200) {
                $scope.progress = 100;
                $interval.cancel(processInterval);
                $scope.selectedPage.isDownloading = false;
                angular.element(document.querySelector('#downloadContentModal')).modal('toggle')
                $mdToast.show($mdToast.simple()
                       .textContent('Tải dữ liệu thành công!')
                       .position('fixed bottom right')
                       .hideDelay(3000));
            }
            else {
                $scope.selectedPage.isDownloading = false;
                $mdToast.show($mdToast.simple()
                       .textContent('Tải dữ liệu thất bại!')
                       .position('fixed bottom right')
                       .hideDelay(3000));
            }
        })
                    .error(function (data, status, headers, config) {
                        $interval.cancel(processInterval);
                        $scope.selectedPage.isDownloading = false;
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Tải dữ liệu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                    });
    }

    $scope.onSelectedAllChanged = function () {
        for (var i = 0; i < $scope.pages.length; i++) {
            if ($scope.pages[i].isSubscribed)
                $scope.pages[i].isSelected = $scope.isAllSelected;
        }
    }


}]);

myApp.controller('LogonController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', function LogonController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog) {

    $scope.init = function () {
        // check if there is query in url
        // and fire search in case its value is not empty
        //get shop list       
        cfpLoadingBar.start();
        $http.get("/api/app/getShopList").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.shops = data.data;
                $scope.totalShop = $scope.shops.length;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.getShopStats = function (shop) {
        shop.stats = {}
        $http.get("/api/app/getShopStats?id=" + shop.ShopId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                shop.stats = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.redirectToHome = function (shop) {
        var params = { "ShopId": shop.ShopId, "ShopName": shop.ShopName };
        var post = $http({
            method: "POST",
            url: "/api/app/RedirectToHome",
            data: params
        });

        post.success(function successCallback(data, status, headers, config) {
            $window.location.href = '/shop.html';
        })
            .error(function (data, status, headers, config) {
            });
    }
}]);

myApp.controller('ShopController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', function ShopController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog) {

    $scope.today = new Date();

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.getShopDetailStats();
    }

    $scope.getShopDetailStats = function () {
        $http.get("/api/app/getShopDetailStats").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.detailStats = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }
}]);

myApp.controller('CategoryController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CategoryController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
    }

    var newCategory = {};

    $scope.initCategory = function () {
        $http.get("/api/app/getCategories").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.categories = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.createCategory = function () {

        if ($scope.newCategory.CategoryName == undefined || $scope.newCategory.CategoryName.trim().length == 0) {
            return;
        }

        cfpLoadingBar.start();
        for (var i = 0; i < $scope.categories.length; i++) {
            if (($scope.categories[i].CategoryName == $scope.newCategory.CategoryName) && ($scope.categories[i].CategoryParentId == $scope.newCategory.CategoryParentId)) {
                cfpLoadingBar.complete();
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Danh mục đã tồn tại, vui lòng thử lại!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
                return;
            }
        }

        var post = $http({
            method: "POST",
            url: "/api/app/createCategory",
            data: $scope.newCategory
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                angular.element(document.querySelector('#cateModal')).modal('toggle')
                $mdToast.show($mdToast.simple()
                   .textContent('Thêm danh mục thành công!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
                $scope.newCategory = { "CategoryParentId": "-1" };
                $scope.initCategory();
            }
            else {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Tạo danh mục thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Tạo danh mục thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                });
    }

    $scope.editCategory = function (category) {
        $scope.editingCategory = angular.copy(category);
        angular.element(document.querySelector('#editModal')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.saveCategory = function () {
        if ($scope.editingCategory.CategoryName == undefined || $scope.editingCategory.CategoryName.trim().length == 0) {
            return;
        }

        if ($scope.editingCategory.CategoryId == $scope.editingCategory.CategoryParentId) {
            $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Danh mục cha không phù hợp!')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
        }
        else if ($scope.editingCategory.children && $scope.editingCategory.children.length != 0) {
            $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thể gộp danh mục cha !')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
        }
        else {

            cfpLoadingBar.start();
            var post = $http({
                method: "POST",
                url: "/api/app/editCategory",
                data: $scope.editingCategory
            });

            post.success(function successCallback(data, status, headers, config) {
                // success                
                cfpLoadingBar.complete();
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editModal')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                       .textContent('Cập nhật danh mục thành công!')
                       .position('fixed bottom right')
                       .hideDelay(3000));
                    $scope.editingCategory = {};
                    $scope.initCategory();
                }
                else {
                    $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Cập nhật danh mục thất bại, vui lòng thử lại sau!')
                           .ok('Đóng')
                           .fullscreen(true)
                       );
                }
            })
                    .error(function (data, status, headers, config) { // optional
                        cfpLoadingBar.complete();
                        $mdDialog.show(
                          $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Thông tin')
                            .textContent('Cập nhật danh mục thất bại, vui lòng thử lại sau!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
                    });
        }
    }

    $scope.showConfirmDeleteCategory = function (category) {
        angular.element(document.querySelector('#editModal')).modal('toggle')
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa danh mục này?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteCategory(category);
        }, function () {
        });
    }

    $scope.deleteCategory = function (category) {
        var count = 0;

        if (category.CategoryParentId == -1) {
            if (category.StockCount > 0) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Không thể xóa danh mục! Danh mục con trong danh mục bạn xóa hiện còn sản phẩm!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                return;
            }
        }
        else if (category.StockCount > 0) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Không thể xóa danh mục! Danh mục này hiện còn sản phẩm!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/deleteCategory/" + $scope.editingCategory.CategoryId
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $mdToast.show($mdToast.simple()
                    .textContent('Xóa danh mục thành công!')
                    .position('fixed bottom right')
                    .hideDelay(3000));
                $scope.editingCategory = null;
                $scope.initCategory();
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa danh mục thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa danh mục thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }
}]);

myApp.controller('ProductController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'fileReader', 'Excel', '$timeout', '$compile', 'Upload', 'cfpLoadingBar', '$mdDialog', '$mdToast', '$sce', function ProductController($scope, $http, config, ngDialog, md5, $window, fileReader, Excel, $timeout, $compile, Upload, cfpLoadingBar, $mdDialog, $mdToast, $sce) {
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.domainImg = config.domain;
    $scope.isLoading = false;
    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.loadProducts();
        $scope.loadCategories();
        $scope.loadManufactures();
        $scope.loadAttributes();
    }

    $scope.newProduct = {}
    $scope.editProduct = {}
    $scope.newManufacture = {}
    $scope.newAttribute = {}
    $scope.editProductNewAttribute = {}

    $scope.isEditing = false;

    $scope.loadProducts = function () {
        $http.get("/api/app/getProducts?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.Count;
                $scope.metadata = data.metadata;
                $scope.products = data.data;


                //calculate stock
                for (var i = 0; i < $scope.products.length; i++) {
                    var count = 0;
                    var tooltip = "Cảnh báo tồn kho! \n Danh sách hàng tồn kho tại các chi nhánh: \n ";
                    if ($scope.products[i].stock != null) {
                        for (var j = 0; j < $scope.products[i].stock.length; j++) {
                            count += $scope.products[i].stock[j].Stock;
                            tooltip += $scope.products[i].stock[j].BranchName + " : " + $scope.products[i].stock[j].Stock + " \n ";
                        }
                    }

                    $scope.products[i].stock_count = count;
                    $scope.products[i].tooltip = $sce.trustAsHtml(tooltip);

                    if ($scope.products[i].stock_count >= $scope.products[i].MinStock && $scope.products[i].stock_count <= $scope.products[i].StockLimit)
                        $scope.products[i].isInvalidQuantity = false;
                    else
                        $scope.products[i].isInvalidQuantity = true;
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadProducts();
    }

    $scope.onPageChange = function () {
        $scope.loadProducts();
    }

    $scope.onQueryChange = function () {
        var query = "";
        console.log($scope.q);
        if ($scope.q.category != '-1') {
            query = "category.CategoryId == " + $scope.q.category;
        }
        if ($scope.q.allowsell != '-1') {
            if (query != "") {
                switch ($scope.q.allowsell) {
                    case '0':
                        query += " and PAStatus != 98 and PAStatus != 99";
                        break;
                    case '1':
                        query += " and PAStatus == 98";
                        break;
                    case '2':

                        query += " and SumStock!=0";
                        break;
                    case '3':
                        query += " and SumStock==0";
                        break;
                    default:
                        break;

                }
            }
            else {
                switch ($scope.q.allowsell) {
                    case '0':
                        query += " PAStatus != 98 and PAStatus != 99";
                        break;
                    case '1':
                        query += " PAStatus == 98";
                        break;
                    case '2':

                        query += "SumStock!=0";
                        break;
                    case '3':
                        query += "SumStock==0";
                        break;
                    default:
                        break;
                }
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }

        $scope.query = query;
        $scope.loadProducts();
    }

    $scope.getFirstImage = function (images) {
        if (images != undefined && images.length > 0)
            return images[0].Path;
        else
            return "";
    }

    $scope.sumStock = function (stocks) {
        var count = 0;
        if (stocks != null) {
            for (var i = 0; i < stocks.length; i++) {
                count += stocks[i].Stock;
            }
        }
        return count;
    }

    $scope.expandDetail = function (product) {
        for (var i = 0; i < $scope.products.length; i++) {
            if (product.ProductAttributeId != $scope.products[i].ProductAttributeId)
                $scope.products[i].isExpand = false;
        }
        product.isExpand = !product.isExpand;
    }

    $scope.getFile = function (file) {
        //add
        var output;
        if (!$scope.isEditing)
            output = document.getElementById("result");
        else
            output = document.getElementById("resultEdit");
        fileReader.readAsDataUrl(file, $scope)
                      .then(function (result) {
                          //add
                          var div = document.createElement("div");
                          var strHtml = "";
                          div.innerHTML = "<img class='thumbnail' src='" + result + "'" + "/>";

                          output.insertBefore(div, null);
                      });
    };

    $scope.findOriginPrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            var index = -1;
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            price = productPrice[index].OriginPrice
        }
        return price;
    }

    $scope.findAverageImportPrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            var index = -1;
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            price = productPrice[index].AverageImportPrice
        }
        return price;
    }

    $scope.findSalePrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            var index = -1;
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            price = productPrice[index].SalePrice;
        }
        return price;
    }

    $scope.loadCategories = function () {
        $http.get("/api/app/getCategories").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.categories = data.data;
                var loc = [];
                $scope.categories.forEach(function (con) {
                    con.Class = 'opparent';
                    loc.push(con);
                    con.children.forEach(function (thocon) {
                        thocon.Class = 'opchil';
                        thocon.CategoryName = '\xa0\xa0\xa0' + thocon.CategoryName;
                        loc.push(thocon);
                    })
                });
                $scope.categoriallchil = loc;
                console.log($scope.categoriallchil);
            }
        }).error(function (data, status, headers, config) {

        });
    }
    $scope.onLoadchil = function (categoriespr) {

    }

    $scope.loadManufactures = function () {
        $http.get("/api/app/getManufactures").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.manufactures = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadAttributes = function () {
        $http.get("/api/app/getAttributes").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.attributes = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.openCreateProductDialog = function () {
        //var output = document.getElementById("result");
        //output.innerHTML = "";
        $scope.files = new Array();
        $http.get("/api/app/getProductCode").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.newProduct.ProductCode = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
        angular.element(document.querySelector('#addProduct')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addProduct')).modal('toggle');
        $scope.newProduct = {};
    }

    $scope.createCategory = function () {
        if ($scope.newCategory.CategoryName == undefined || $scope.newCategory.CategoryName == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Nội dung tên nhóm trống, vui lòng nhập dữ liệu!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }
        //if ($scope.newCategory.CategoryParentId == (-1)) {
        //    $mdDialog.show(
        //         $mdDialog.alert()
        //           .clickOutsideToClose(true)
        //           .title('Thông tin')
        //           .textContent('Nội dung nhóm cha trống, vui lòng nhập dữ liệu!')
        //           .ok('Đóng')
        //           .fullscreen(true)
        //       );
        //    return;
        //}

        cfpLoadingBar.start();
        for (var i = 0; i < $scope.categories.length; i++) {
            if (($scope.categories[i].CategoryName == $scope.newCategory.CategoryName)
                && ($scope.categories[i].CategoryParentId == $scope.newCategory.CategoryParentId)) {
                cfpLoadingBar.complete();
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Nhóm đã tồn tại, vui lòng thử lại!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
                return;
            }

            var cate = $scope.categories[i].children;

            for (var j = 0; j < cate.length; j++) {
                if ((cate[j].CategoryName == $scope.newCategory.CategoryName) && (cate[j].CategoryParentId == $scope.newCategory.CategoryParentId)) {
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Nhóm đã tồn tại, vui lòng thử lại!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                    return;
                }
            }

        }

        var post = $http({
            method: "POST",
            url: "/api/app/createCategory",
            data: $scope.newCategory
        });

        post.success(function successCallback(data, status, headers, config) {
            // success               
            cfpLoadingBar.complete();
            if (data.meta && data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#addCategory')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm nhóm thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.loadCategories();
                    $scope.newCategory = {};
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhóm thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo nhóm thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.createManufacture = function () {
        if ($scope.newManufacture.Name == undefined || $scope.newManufacture.Name == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Nội dung tên thương hiệu, vui lòng nhập dữ liệu!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }
        if ($scope.newManufacture.Origin == undefined) {
            $scope.newManufacture.Origin == '';
        }

        cfpLoadingBar.start();
        for (var i = 0; i < $scope.manufactures.length; i++) {
            if (($scope.manufactures[i].Name == $scope.newManufacture.Name) && ($scope.manufactures[i].Origin == $scope.newManufacture.Origin)) {
                cfpLoadingBar.complete();
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Thương hiệu đã tồn tại, vui lòng thử lại!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
                return;
            }
        }

        var post = $http({
            method: "POST",
            url: "/api/app/createManufacture",
            data: $scope.newManufacture
        });

        post.success(function successCallback(data, status, headers, config) {
            // success               
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#createManufacture')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm thương hiệu thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.loadManufactures();
                    $scope.newManufacture = {};
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo thương hiệu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo thương hiệu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.createAttribute = function () {
        if ($scope.newAttribute.AttributeName == undefined || $scope.newAttribute.AttributeName == '') {
            return;
        }

        cfpLoadingBar.start();

        for (var i = 0; i < $scope.attributes.length; i++) {
            if ($scope.attributes[i].AttributeName.toLowerCase() == $scope.newAttribute.AttributeName.toLowerCase()) {
                console.log($scope.attributes[i].AttributeName.trim().toLowerCase());
                cfpLoadingBar.complete();
                angular.element(document.querySelector('#createAttribute')).modal('toggle')
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Thuộc tính đã tồn tại, vui lòng thử lại!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
                $scope.newAttribute = {};
                return;
            }
        }

        var post = $http({
            method: "POST",
            url: "/api/app/createAttribute",
            data: $scope.newAttribute
        });

        post.success(function successCallback(data, status, headers, config) {
            // success     
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#createAttribute')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Tạo thuộc tính thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.loadAttributes();
                    $scope.newAttribute = {};
                }
            }
            else {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Tạo thuộc tính thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo thuộc tính thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.addSet = function (ev, attrs) {
        var compiledDirective = $compile('<attributeset attributes="attributes"></attributeset>');
        var directiveElement = compiledDirective($scope);
        angular.element("#attributeSets").prepend(directiveElement);
    };

    $scope.newUnitNew = {};
    $scope.addUnitNew = function (ev, attrs) {

        $scope.newProduct.OriginPrice = $scope.newProduct.OriginPrice;
        if ($scope.newProduct.units == undefined)
            $scope.newProduct.units = [];

        console.log($scope.newProduct.units);

        if ($scope.newUnitNew.Name == "" || $scope.newUnitNew.Name == undefined) {
            $mdToast.show($mdToast.simple()
                     .textContent('Bạn vui lòng nhập tên đơn vị!')
                     .position('fixed bottom right')
                     .hideDelay(6000));
            return;
        }
        else if ($scope.newUnitNew.Value == "" || $scope.newUnitNew.Value == undefined) {
            $mdToast.show($mdToast.simple()
                     .textContent('Bạn vui lòng nhập số lượng của đơn vị!')
                     .position('fixed bottom right')
                     .hideDelay(6000));
            return;
        }
        else if ($scope.newUnitNew.Price == "" || $scope.newUnitNew.Price == undefined) {
            $mdToast.show($mdToast.simple()
                     .textContent('Bạn vui lòng nhập giá bán của đơn vị!')
                     .position('fixed bottom right')
                     .hideDelay(6000));
            return;
        }
        else {
            var k = true;
            if ($scope.newUnitNew.Name == $scope.newProduct.BaseUnit) {
                k = false;
            }
            for (var i = 0; i < $scope.newProduct.units.length; i++) {
                console.log($scope.newProduct.BaseUnit + "-" + $scope.newUnitNew.Name);
                if ($scope.newProduct.units[i].Name == $scope.newUnitNew.Name && $scope.newProduct.units[i].Value == $scope.newUnitNew.Value && $scope.newProduct.units[i].status != "DELETE") {
                    k = false;
                    break;
                }
            }

            if (k == true) {
                $scope.newProduct.units.push($scope.newUnitNew);
            }
        }
        $scope.newUnitNew = {};

    };

    var getAttributeSet = function () {
        var Attrs = [];
        var ChildHeads = [$scope.$$childHead];
        var currentScope;
        while (ChildHeads.length) {
            currentScope = ChildHeads.shift();
            while (currentScope) {

                if (currentScope.attrs !== undefined) {
                    Attrs.push(currentScope.attrs.data);
                }

                currentScope = currentScope.$$nextSibling;
            }
        }
        return Attrs;
    }

    var getProductUnit = function () {
        var units = [];
        var ChildHeads = [$scope.$$childHead];
        var currentScope;
        while (ChildHeads.length) {
            currentScope = ChildHeads.shift();
            while (currentScope) {

                if (currentScope.newUnit !== undefined && currentScope.newUnit.Name != undefined && currentScope.newUnit.Value != undefined && currentScope.newUnit.Price != undefined) {
                    units.push(currentScope.newUnit);
                }

                currentScope = currentScope.$$nextSibling;
            }
        }
        return units;
    }

    $scope.createProduct = function () {

        if ($scope.newProduct.ProductName == undefined || $scope.newProduct.ProductName == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng nhập tên sản phẩm!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.newProduct.CategoryId == undefined || $scope.newProduct.CategoryId == '-1' || $scope.newProduct.CategoryId == -1) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng chọn nhóm hàng!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.newProduct.ManufactureId == undefined || $scope.newProduct.ManufactureId == '-1' || $scope.newProduct.ManufactureId == -1) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng chọn nhãn hiệu!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.newProduct.SalePrice == undefined || $scope.newProduct.SalePrice == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng nhập giá bán!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        var domain = config.domain;
        if ($scope.files == undefined || $scope.files.length == 0) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng chọn ảnh sản phẩm')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.newProduct.BaseUnit == undefined || $scope.newProduct.BaseUnit == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn chưa điền Đơn vị cơ bản trong phần Đơn vị tính')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        $scope.newProduct.OriginPrice = 0;

        if ($scope.newProduct.units != undefined && $scope.newProduct.units != null) {
            if ($scope.newProduct.units.length > 0) {
                for (var i = 0; i < $scope.newProduct.units.length; i++) {
                    $scope.newProduct.units[i].Price = isNaN($scope.newProduct.units[i].Price) ? parseInt($scope.newProduct.units[i].Price.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newProduct.units[i].Price;
                }
            }
        }

        $scope.newProduct.attributeSets = getAttributeSet();
        //remvove invalid character
        $scope.newProduct.SalePrice = isNaN($scope.newProduct.SalePrice) ? parseInt($scope.newProduct.SalePrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newProduct.SalePrice;
        $scope.newProduct.OriginPrice = isNaN($scope.newProduct.OriginPrice) ? parseInt($scope.newProduct.OriginPrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newProduct.OriginPrice;

        cfpLoadingBar.start();
        if (!$scope.isLoading) {
            $scope.isLoading = true;

            var createProduct = angular.element(document.querySelector('#createProduct'));
            createProduct.attr('disabled', 'disabled');


            var post = $http({
                method: "POST",
                url: "/api/app/createProduct",
                data: $scope.newProduct
            });
            post.success(function successCallback(data, status, headers, config) {

                // success                
                $scope.isLoading = false;
                cfpLoadingBar.complete();
                if (data.meta.error_code == 200) {
                    if (data.meta.error_code == 200) {
                        //upload                  
                        var statusUploadImg = true;
                        if ($scope.files && $scope.files.length) {
                            for (var i = 0; i < $scope.files.length; i++) {
                                Upload.upload({
                                    url: domain + "/api/uploadProduct/" + data.data,
                                    data: { file: $scope.files[i] }
                                }).then(function (resp) {
                                    if (statusUploadImg) {
                                        angular.element(document.querySelector('#addProduct')).modal('toggle');
                                        $scope.newProduct = { "CategoryId ": "-1", "ManufactureId": "-1" };
                                        $scope.newManufacture = {};
                                        $scope.newAttribute = {};
                                        $scope.files = new Array();
                                        $mdToast.show($mdToast.simple()
                                     .textContent('Thêm sản phẩm thành công!')
                                     .position('fixed bottom right')
                                     .hideDelay(3000));
                                        $timeout(createProduct.removeAttr('disabled'), 1000);
                                        //new set
                                        var compiledDirective = $compile('<attributeset attributes="attributes"></attributeset>');
                                        var directiveElement = compiledDirective($scope);
                                        angular.element("#attributeSets").html(directiveElement);
                                        var addSetButtonHtml = "<button type='button' class='btn btn-secondary' ng-click='addSet();'>Thêm nhóm thuộc tính <span class='ion-ios-plus-empty'></span></button>";
                                        var target = $compile(addSetButtonHtml)($scope);
                                        $("#attributeSets").append(target);
                                        var output = document.getElementById("result");
                                        if (output != undefined)
                                            output.innerHTML = "";
                                        $scope.loadProducts();
                                        statusUploadImg = false;
                                    }

                                }, function (resp) {
                                    if (statusUploadImg) {
                                        angular.element(document.querySelector('#addProduct')).modal('toggle');
                                        $timeout(createProduct.removeAttr('disabled'), 1000);
                                        $scope.newProduct = { "CategoryId ": "-1", "ManufactureId": "-1" };
                                        $scope.newManufacture = {};
                                        $scope.newAttribute = {};
                                        $scope.files = new Array();
                                        $mdToast.show($mdToast.simple()
                                     .textContent('Thêm sản phẩm thành công!')
                                     .position('fixed bottom right')
                                     .hideDelay(3000));
                                        //new set
                                        var compiledDirective = $compile('<attributeset attributes="attributes"></attributeset>');
                                        var directiveElement = compiledDirective($scope);
                                        angular.element("#attributeSets").html(directiveElement);
                                        var addSetButtonHtml = "<button type='button' class='btn btn-secondary' ng-click='addSet();'>Thêm nhóm thuộc tính <span class='ion-ios-plus-empty'></span></button>";
                                        var target = $compile(addSetButtonHtml)($scope);
                                        $("#attributeSets").append(target);

                                        var output = document.getElementById("result");
                                        if (output != undefined)
                                            output.innerHTML = "";
                                        $scope.loadProducts();
                                        statusUploadImg = false;
                                    }
                                    console.log('Error status: ' + resp.status);
                                }, function (evt) {
                                });
                            }
                        }
                        else {
                            if (statusUploadImg) {
                                angular.element(document.querySelector('#addProduct')).modal('toggle');
                                $timeout(createProduct.removeAttr('disabled'), 1000);
                                $scope.newProduct = { "CategoryId ": "-1", "ManufactureId": "-1" };
                                $scope.newManufacture = {};
                                $scope.newAttribute = {};
                                $scope.files = new Array();
                                $mdToast.show($mdToast.simple()
                             .textContent('Thêm sản phẩm thành công!')
                             .position('fixed bottom right')
                             .hideDelay(3000));
                                //new set
                                var compiledDirective = $compile('<attributeset attributes="attributes"></attributeset>');
                                var directiveElement = compiledDirective($scope);
                                angular.element("#attributeSets").html(directiveElement);

                                var output = document.getElementById("result");
                                if (output != undefined)
                                    output.innerHTML = "";
                                $scope.loadProducts();
                                statusUploadImg = false;
                            }
                        }
                    }
                }
                else {
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo sản phẩm thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                }
            }).
                error(function (data, status, headers, config) { // optional
                    $timeout(createProduct.removeAttr('disabled'), 1000);
                    $scope.isLoading = false;
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                   $mdDialog.alert()
                     .clickOutsideToClose(true)
                     .title('Thông tin')
                     .textContent('Tạo sản phẩm thất bại, vui lòng thử lại sau!')
                     .ok('Đóng')
                     .fullscreen(true)
                 );
                });
        }
    }

    $scope.loadExims = function (product, pagenumber) {
        console.log(pagenumber);
        $http.get("/api/app/getProductExims?id=" + product.ProductAttributeId + "&page=" + pagenumber + "&page_size=5").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                product.exims = data.data;
                $scope.itemExims_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {

        });
    }



    $scope.findPaymentStatus = function (status) {
        if (status == 0)
            return "Chưa thanh toán";
        else if (status == 1)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa vận chuyển";
        else if (status == 1)
            return "Đang vận chuyển";
        else
            return "Đã nhập kho"
    }

    $scope.findOrderDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else if (status == 3)
            return "Không giao được hàng";
        else
            return "Đã giao hàng"
    }

    $scope.findBaseUnit = function (productPrice) {
        var unit = "";
        if (productPrice != null) {
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            unit = productPrice[index].Unit;
        }
        return unit;
    }
    //undefined

    $scope.newUnit = {};
    $scope.openEditProductDialog = function (productAttribute) {
        $scope.isEditing = true;
        $scope.files = new Array();
        $scope.editProduct = angular.copy(productAttribute);
        $scope.editProduct.CategoryId = $scope.editProduct.category.CategoryId + "";
        $scope.editProduct.ManufactureId = $scope.editProduct.ManufactureId + "";
        $scope.editProduct.SalePrice = $scope.findSalePrice($scope.editProduct.prices);
        $scope.editProduct.OriginPrice = $scope.findOriginPrice($scope.editProduct.prices);
        $scope.editProduct.BaseUnit = $scope.findBaseUnit($scope.editProduct.prices)

        //load attribute
        $http.get("/api/app/getPAAttributes?id=" + $scope.editProduct.ProductAttributeId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.editProduct.attrs = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        angular.element(document.querySelector('#editProduct')).modal({ backdrop: 'static', keyboard: true });
    }

    $scope.addUnit = function () {
        $scope.editProduct.OriginPrice = $scope.editProduct.OriginPrice;
        if ($scope.editProduct.prices == undefined)
            $scope.editProduct.prices = [];

        console.log($scope.editProduct.prices);

        if ($scope.newUnit.Unit == "" || $scope.newUnit.Unit == undefined) {
            $mdToast.show($mdToast.simple()
                     .textContent('Bạn vui lòng nhập tên đơn vị!')
                     .position('fixed bottom right')
                     .hideDelay(6000));
            return;
        }
        else if ($scope.newUnit.Quantity == "" || $scope.newUnit.Quantity == undefined) {
            $mdToast.show($mdToast.simple()
                     .textContent('Bạn vui lòng nhập số lượng của đơn vị!')
                     .position('fixed bottom right')
                     .hideDelay(6000));
            return;
        }
        else if ($scope.newUnit.SalePrice == "" || $scope.newUnit.SalePrice == undefined) {
            $mdToast.show($mdToast.simple()
                     .textContent('Bạn vui lòng nhập giá bán của đơn vị!')
                     .position('fixed bottom right')
                     .hideDelay(6000));
            return;
        }
        else {
            var k = true;
            for (var i = 0; i < $scope.editProduct.prices.length; i++) {
                if ($scope.editProduct.prices[i].Unit == $scope.newUnit.Unit && $scope.editProduct.prices[i].Quantity == $scope.newUnit.Quantity && $scope.editProduct.prices[i].status != "DELETE") {
                    k = false;
                    break;
                }
            }

            if (k == true) {
                $scope.editProduct.prices.push($scope.newUnit);
            }
        }
        $scope.newUnit = {};
    }

    $scope.deleteEditProductUnit = function (price) {
        price.Status = 'DELETE';
    }

    $scope.deleteEditProductAttribute = function (attr) {
        attr.status = 'DELETE';
    }

    $scope.findName = function (id) {
        var name = "";
        for (var i = 0; i < $scope.attributes.length; i++) {
            if ($scope.attributes[i].AttributeId == parseInt(id))
                name = $scope.attributes[i].AttributeName;
        }
        return name;
    }

    $scope.addEditProductAttribute = function () {
        var isDup = false;
        var indexDup = -1;
        for (var i = 0; i < $scope.editProduct.attrs.length; i++) {
            if (($scope.editProduct.attrs[i].AttributeId == parseInt($scope.editProductNewAttribute.AttributeId) || $scope.editProductNewAttribute.AttributeId == "-1")) {
                isDup = true;
                indexDup = i;
            }
        }

        if (!$scope.editProductNewAttribute.AttributeValue || /^\s*$/.test($scope.editProductNewAttribute.AttributeValue))
            return;
        if (isDup && $scope.editProduct.attrs[indexDup].status != 'DELETE')
            return;
        else {
            if (indexDup == -1 || $scope.editProduct.attrs[indexDup].status != 'DELETE') {
                $scope.editProductNewAttribute.status = "NEW";
                $scope.editProductNewAttribute.AttributeName = $scope.findName($scope.editProductNewAttribute.AttributeId);
                $scope.editProduct.attrs.push($scope.editProductNewAttribute);
                $scope.editProductNewAttribute = { AttributeId: "-1" };
            }
            else {
                //update
                $scope.editProduct.attrs[indexDup].status = 'UPDATE';
                $scope.editProduct.attrs[indexDup].AttributeValue = $scope.editProductNewAttribute.AttributeValue;
                $scope.editProductNewAttribute = { AttributeId: "-1" };
            }
        }
    }

    $scope.deleteProductImage = function (image) {
        var index = -1;
        for (var i = 0; i < $scope.editProduct.images.length; i++) {
            console.log(image.RelativePath + "-" + $scope.editProduct.images[i].RelativePath);
            if (image.RelativePath == $scope.editProduct.images[i].RelativePath)
                index = i;
        }
        if (index > -1) {
            if ($scope.editProduct.deletedImages == undefined) {
                $scope.editProduct.deletedImages = new Array();
            }
            $scope.editProduct.deletedImages.push(image);
            $scope.editProduct.images.splice(index, 1);
        }
    }

    $scope.updateProduct = function () {
        var domain = config.domain;

        if ($scope.editProduct.ProductName == undefined || $scope.editProduct.ProductName == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng nhập tên sản phẩm!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.editProduct.CategoryId == undefined || $scope.editProduct.CategoryId == '-1' || $scope.editProduct.CategoryId == -1) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng chọn nhóm hàng!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.editProduct.ManufactureId == undefined || $scope.editProduct.ManufactureId == '-1' || $scope.editProduct.ManufactureId == -1) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng chọn nhãn hiệu!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.editProduct.SalePrice == undefined || $scope.editProduct.SalePrice == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng nhập giá bán!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.editProduct.BaseUnit == undefined || $scope.editProduct.BaseUnit == '') {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn chưa điền Đơn vị cơ bản trong phần Đơn vị tính')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        $scope.editProduct.CategoryId = parseInt($scope.editProduct.CategoryId);
        $scope.editProduct.ManufactureId = parseInt($scope.editProduct.ManufactureId);
        $scope.editProduct.MinStock = parseInt($scope.editProduct.MinStock);
        $scope.editProduct.StockLimit = parseInt($scope.editProduct.StockLimit);

        if ($scope.editProduct.prices != undefined) {
            if ($scope.editProduct.prices.length > 0) {
                for (var i = 0; i < $scope.editProduct.prices.length; i++) {
                    $scope.editProduct.prices[i].SalePrice = isNaN($scope.editProduct.prices[i].SalePrice) ? parseInt($scope.editProduct.prices[i].SalePrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.editProduct.prices[i].SalePrice;
                }
            }
        }
        $scope.editProduct.SalePrice = isNaN($scope.editProduct.SalePrice) ? parseInt($scope.editProduct.SalePrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.editProduct.SalePrice;
        $scope.editProduct.OriginPrice = isNaN($scope.editProduct.OriginPrice) ? parseInt($scope.editProduct.OriginPrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.editProduct.OriginPrice;
        cfpLoadingBar.start();
        //upload
        if ($scope.files && $scope.files.length) {
            for (var i = 0; i < $scope.files.length; i++) {
                Upload.upload({
                    url: domain + "/api/uploadProduct/" + $scope.editProduct.ProductId,
                    data: { file: $scope.files[i] }
                }).then(function (resp) {
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                });
            }
        }

        var post = $http({
            method: "POST",
            url: "/api/app/updateProduct",
            data: $scope.editProduct
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                angular.element(document.querySelector('#editProduct')).modal('toggle')
                $scope.loadProducts();
                $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật sản phẩm thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật sản phẩm thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Cập nhật sản phẩm thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.showConfirmDeleteProduct = function (product) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa sản phẩm ' + product.ProductCode + ' hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteProduct(product);
        }, function () {
        });
    }

    $scope.deleteProduct = function (product) {

        var count = 0;
        if (product.stock != null) {
            for (var j = 0; j < product.stock.length; j++) {
                count += product.stock[j].Stock;
            }
        }

        console.log(count);

        if (count > 0) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Sản phẩm vẫn còn hàng tồn kho, bạn không thể xóa!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/deleteProduct",
            data: product
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                product.isExpand = false;
                product.PAStatus = 99;
                $scope.loadProducts();
                $mdToast.show($mdToast.simple()
                     .textContent('Xóa sản phẩm thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Xóa sản phẩm thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );

            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Xóa sản phẩm thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );

        });
    }

    $scope.stopProduct = function (product) {
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/stopProduct",
            data: product
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (product.PAStatus != 98)
                    product.PAStatus = 98;
                else
                    product.PAStatus = 0;
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Ngừng kinh doanh sản phẩm thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Ngừng kinh doanh sản phẩm thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );

        });
    }

    //Export
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.report(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    }

    //$scope.name = 'World';
    $scope.exportAction = function (option) {
        switch (option) {
            case 'pdf': $scope.$broadcast('export-pdf', {});
                break;
            case 'excel': $scope.$broadcast('export-excel', {});
                break;
            case 'doc': $scope.$broadcast('export-doc', {});
                break;
            case 'csv': $scope.$broadcast('export-csv', {});
                break;
            default: console.log('no event caught');
        }
    }

    $scope.exportAction2 = function () {
        //$http.get("api/export/22/product")
        //.then(function (response) {
        //    $scope.myWelcome = response.data;
        //});
        $http.get("/api/app/exportExcel").success(function (data, status, headers) {
            //if (data.meta.error_code == 200) {
            //    $scope.categories = data.data;
            //}
        }).error(function (data, status, headers, config) {

        });
    }

}]);

myApp.controller('CustomerController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CustomerController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.customer = {};
    $scope.import = {};
    $scope.import_return = {};
    $scope.order = {};
    $scope.product_attribute = {};
    $scope.return = {};
    $scope.vendor = {};
    $scope.delivery_cendor = {};

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DOBPopup = {
        opened: false
    };

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openPopupDOB = function () {
        $scope.DOBPopup.opened = true;
    }

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.loadCustomers();
        $scope.loadProvinces();
        $scope.loadCustomerGroups();
    }

    $scope.loadCustomers = function () {
        $http.get("/api/app/getCustomers?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.Count;
                $scope.customers = data.data;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadCustomers();
    }

    $scope.onPageChange = function () {
        $scope.loadCustomers();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.group != '-1') {
            query = "customerGroup.CustomerGroupId = " + $scope.q.group;
        }
        if ($scope.q.province != '-1') {
            if (query != "")
                query = " and province.ProvinceId = " + $scope.q.province;
            else
                query = "province.ProvinceId = " + $scope.q.province;
        }
        if ($scope.q.type != '-1') {
            if (query != "")
                query = " and Type = " + $scope.q.type;
            else
                query = "Type = " + $scope.q.type;
        }
        else {
            if (query == "")
                query = "1=1";
        }

        $scope.query = query;
        $scope.loadCustomers();
    }

    $scope.onOrderChange = function () {

    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadDistricts = function (province) {
        console.log(province);
        if (!$scope.isEditing) {
            $scope.newCustomer.Province.Name = province.Name;
            $scope.newCustomer.Province.ProvinceId = province.ProvinceId;
            $scope.newCustomer.District.Name = "Quận / Huyện";
            $scope.newCustomer.District.DistrictId = -1;
        }
        else {
            $scope.editCustomer.Province.Name = province.Name;
            $scope.editCustomer.Province.ProvinceId = province.ProvinceId;
            $scope.editCustomer.District.Name = "Quận / Huyện";
            $scope.editCustomer.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newCustomer.District.Name = district.Name;
            $scope.newCustomer.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editCustomer.District.Name = district.Name;
            $scope.editCustomer.District.DistrictId = district.DistrictId;
        }
    }

    $scope.loadCustomerGroups = function () {
        $http.get("/api/app/getCustomerGroups").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.customerGroups = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectGroup = function (group) {
        $scope.newCustomer.CustomerGroup.Id = group.CustomerGroupId;
        $scope.newCustomer.CustomerGroup.Name = group.Name;
    }

    $scope.createCustomerGroup = function () {
        var groupName = angular.element(document.querySelector('#txtGroupName')).val();

        var group = {
            Name: groupName
        };
        var post = $http({
            method: "POST",
            url: "/api/app/createCustomerGroup",
            data: group
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#groupModal')).modal('toggle')
                    $scope.loadCustomerGroups();
                    $mdToast.show($mdToast.simple()
                     .textContent('Tạo nhóm khách hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    if (!$scope.isEditing) {
                        $scope.newCustomer.CustomerGroup.Id = data.data.CustomerGroupId + "";
                        $scope.newCustomer.CustomerGroup.Name = data.data.Name;
                    }
                    else {
                        $scope.editCustomer.CustomerGroup.Id = data.data.CustomerGroupId + "";
                        $scope.editCustomer.CustomerGroup.Name = data.data.Name;
                    }
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhóm khách hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhóm khách hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.openCreateCustomerDialog = function () {
        //
        $scope.isEditing = false;
        $http.get("/api/app/getCustomerCode").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.newCustomer.CustomerCode = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        //$scope.loadCustomerGroups();
        $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" }, "CustomerGroupId": '1' };

        angular.element(document.querySelector('#addCustomer')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addCustomer')).modal('toggle');
        $scope.newCustomer = {};
        $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" }, "CustomerGroupId": '1' };
        $scope.newCustomer.Phone = '';
        $scope.newCustomer.Email = '';
        $scope.newCustomer.TaxID = '';
        $scope.newCustomer.ID = '';
    }

    $scope.openEditCustomerDialog = function (customer) {
        //
        $scope.isEditing = true;
        $scope.editCustomer = angular.copy(customer);
        $scope.originEditCustomer = angular.copy(customer);
        if ($scope.editCustomer.Province != null) {
            $scope.editCustomer.Province.ProvinceId = $scope.editCustomer.Province.ProvinceId + '';
            $scope.editCustomer.District.DistrictId = $scope.editCustomer.District.DistrictId + '';
            if ($scope.editCustomer.Province.ProvinceId != '-1') {
                $http.get("/api/app/getDistricts?id=" + $scope.editCustomer.Province.ProvinceId).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.districts = data.data.Districts;
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else {
                $scope.editCustomer.Province = { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 };
                $scope.editCustomer.District = { "DistrictId": -1, "Name": "Quận / Huyện" }
            }
        }
        else {
            $scope.editCustomer.Province = { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 };
            $scope.editCustomer.District = { "DistrictId": -1, "Name": "Quận / Huyện" }
        }

        $scope.editCustomer.Type = $scope.editCustomer.Type + "";
        $scope.editCustomer.Gender = $scope.editCustomer.Gender == true ? "1" : "0";
        if ($scope.editCustomer.DOB != undefined)
            $scope.editCustomer.DOB = new Date($scope.editCustomer.DOB);
        //group
        $scope.editCustomer.customerGroup.CustomerGroupId = $scope.editCustomer.customerGroup.CustomerGroupId + "";
        angular.element(document.querySelector('#editCustomer')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.createCustomer = function () {

        if (!$scope.frmAddCustomer.$valid) {
            if ($scope.frmAddCustomer.$error.required && $scope.frmAddCustomer.$error.required[0].$name == "addCustomerName") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập tên khách hàng!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmAddCustomer.$error.pattern && $scope.frmAddCustomer.$error.pattern[0].$name == 'txtTaxID') {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng mã số thuế!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmAddCustomer.$error.pattern && $scope.frmAddCustomer.$error.pattern[0].$name == 'txtID') {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số chứng minh thư!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmAddCustomer.$error.required && $scope.frmAddCustomer.$error.required[0].$name == 'txtPhone') {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số điện thoại!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmAddCustomer.$error.email) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng email!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thành công ! Vui lòng kiểm tra lại thông tin')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            }
        }

        for (var i = 0; i < $scope.customers.length; i++) {
            if ($scope.customers[i].Phone == $scope.newCustomer.Phone) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số điện thoại này đã được sử dụng cho khách hàng ' + $scope.customers[i].CustomerName + ', bạn vui lòng sử dụng số điện thoại khác!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }


        cfpLoadingBar.start();
        $scope.newCustomer.CustomerGroupId = $scope.newCustomer.CustomerGroupId ? $scope.newCustomer.CustomerGroupId : 1;
        $scope.newCustomer.CustomerGroupId = $scope.newCustomer.CustomerGroupId == -1 ? 1 : $scope.newCustomer.CustomerGroupId;
        $scope.newCustomer.Type = parseInt($scope.newCustomer.Type);
        $scope.newCustomer.Gender = parseInt($scope.newCustomer.Gender);
        $scope.newCustomer.DistrictId = parseInt($scope.newCustomer.District.DistrictId);
        $scope.newCustomer.ProvinceId = parseInt($scope.newCustomer.Province.ProvinceId);

        var post = $http({
            method: "POST",
            url: "/api/app/createCustomer",
            data: $scope.newCustomer
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#addCustomer')).modal('toggle');
                    $scope.loadCustomers();
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm khách hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    //$scope.newCustomer;
                    $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" }, "CustomerGroupId": '1' };
                }
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                $mdDialog.alert()
                  .clickOutsideToClose(true)
                  .title('Thông tin')
                  .textContent('Tạo khách hàng thất bại, số điện thoại đã tồn tại!')
                  .ok('Đóng')
                  .fullscreen(true)
              );
                return;

            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo khách hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Tạo khách hàng thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
                });
    }

    $scope.expandDetail = function (customer) {
        for (var i = 0; i < $scope.customers.length; i++) {
            if (customer.CustomerId != $scope.customers[i].CustomerId)
                $scope.customers[i].isExpand = false;
        }
        customer.isExpand = !customer.isExpand;
    }

    $scope.cancelEdit = function () {
        $scope.editCustomer = $scope.originEditCustomer;
    }

    $scope.updateCustomer = function () {
        if (!$scope.frmEditCustomer.$valid) {
            if ($scope.frmEditCustomer.$error.required && $scope.frmEditCustomer.$error.required[0].$name == "editCustomerName") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập tên khách hàng!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmEditCustomer.$error.pattern && $scope.frmEditCustomer.$error.pattern[0].$name == 'txtTaxID') {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng mã số thuế!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmEditCustomer.$error.pattern && $scope.frmEditCustomer.$error.pattern[0].$name == 'txtID') {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số chứng minh thư!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if (($scope.frmEditCustomer.$error.pattern && $scope.frmEditCustomer.$error.pattern[0].$name == 'txtPhone') || ($scope.frmEditCustomer.$error.required && $scope.frmEditCustomer.$error.required[0].$name == 'txtPhone')) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số điện thoại!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmEditCustomer.$error.email) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng email!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thành công ! Vui lòng kiểm tra lại thông tin')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            }
        }

        for (var i = 0; i < $scope.customers.length; i++) {
            if ($scope.customers[i].Phone == $scope.editCustomer.Phone && $scope.customers[i].CustomerCode != $scope.editCustomer.CustomerCode) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số điện thoại này đã được sử dụng cho khách hàng ' + $scope.customers[i].CustomerName + ', bạn vui lòng sử dụng số điện thoại khác!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }

        cfpLoadingBar.start();
        $scope.editCustomer.CustomerGroupId = parseInt($scope.editCustomer.customerGroup.CustomerGroupId);
        $scope.editCustomer.Type = parseInt($scope.editCustomer.Type);
        $scope.editCustomer.Gender = parseInt($scope.editCustomer.Gender);
        $scope.editCustomer.DistrictId = parseInt($scope.editCustomer.District.DistrictId);
        $scope.editCustomer.ProvinceId = parseInt($scope.editCustomer.Province.ProvinceId);

        var post = $http({
            method: "POST",
            url: "/api/app/editCustomer",
            data: $scope.editCustomer
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editCustomer')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật khách hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.editCustomer = {};
                    $scope.loadCustomers();
                }
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                $mdDialog.alert()
                  .clickOutsideToClose(true)
                  .title('Thông tin')
                  .textContent('Tạo khách hàng thất bại, số điện thoại đã tồn tại!')
                  .ok('Đóng')
                  .fullscreen(true)
              );
                return;

            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo khách hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo khách hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.showConfirmDeleteCustomer = function (customer) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa khách hàng ' + customer.CustomerName + ' hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteCustomer(customer);
        }, function () {
        });
    }

    $scope.deleteCustomer = function (customer) {

        if (customer.TotalDebt != 0) {
            $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Công nợ của khách hàng vẫn còn, bạn không thể xóa!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            return;
        }

        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/deleteCustomer",
            data: customer
        });

        post.success(function successCallback(data, status, headers, config) {
            // success         
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.loadCustomers();
                    $mdToast.show($mdToast.simple()
                     .textContent('Xóa khách hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Xóa khách hàng thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Xóa khách hàng thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.loadOrder = function (customer, pagenumber) {
        //console.log(product);
        $http.get("/api/app/getCustomerOrder?id=" + customer.CustomerId + "&page=" + pagenumber + "&page_size=5").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                customer.orders = data.data;
                $scope.itemorder_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadCashFlows = function (customer, pagenumber) {
        $http.get("/api/app/getCustomerCashFlows?id=" + customer.CustomerId + "&page=" + pagenumber + "&page_size=5").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                customer.cashflows = data.data;
                $scope.itemcashflow_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadCustomerEximsProduct = function (customer) {
        $http.get("/api/app/loadCustomerEximsProduct?id=" + customer.CustomerId + "&page=1&page_size=20").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                customer.eximsproducts = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.findType = function (status) {
        if (status == 0)
            return "Thanh toán";
        else if (status == 1)
            return "Bán hàng";
        else if (status == 2 || status == 4)
            return "Trả hàng";
        else if (status == 3)
            return "Nhập hàng";
        else if (status == 5)
            return "Hủy phiếu";
    }

    $scope.findPaymentStatus = function (status) {
        if (status == 0)
            return "Chưa thanh toán";
        else if (status == 1)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa vận chuyển";
        else if (status == 1)
            return "Đang vận chuyển";
        else
            return "Đã nhập kho"
    }

    $scope.findOrderDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else if (status == 3)
            return "Không giao được hàng";
        else
            return "Đã giao hàng"
    }

    //cash
    $scope.openCreateCash = function (customer) {
        //
        $scope.Type = 1;
        $scope.cus = angular.copy(customer);;
        console.log($scope.Type);
        $http.get("/api/app/getCashTypes?type=" + $scope.Type).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.cashTypes = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        angular.element(document.querySelector('#addCashIn')).modal({ backdrop: 'static', keyboard: true });
        $scope.newCashIn = { "CashTypeId": "-1" };
        $scope.isCashInOpen = true;

        $scope.newCashIn.PaidTargetType = "CUSTOMER";
        $scope.newCashIn.PaidTargetId = customer.CustomerId;
        $scope.newCashIn.TargetName = customer.CustomerName;
        $scope.newCashIn.TotalDebt = customer.TotalDebt;
        $scope.InfoCustomer = "" + customer.CustomerName + " - " + customer.Phone;

        if (customer.TotalDebt <= 0) {
            var typeCash = angular.element(document.querySelector('#TypeIn'));
            typeCash.prop('checked', true);
            $scope.newCashIn.TargetType = "ORDER";
            $scope.newCashIn.TargetTypeName = "Phiếu mua hàng";
            $scope.newCashIn.CashTypeName = "thu";
            $scope.newCashIn.PaidAmount = (0 - customer.TotalDebt) + "";
            $scope.newCashIn.Note = "Thu tiền từ khách hàng " + $scope.InfoCustomer;
        }
        else {
            var typeCash = angular.element(document.querySelector('#TypeOut'));
            typeCash.prop('checked', true);
            $scope.newCashIn.TargetType = "RETURN";
            $scope.newCashIn.TargetTypeName = "Phiếu trả hàng";
            $scope.newCashIn.CashTypeName = "chi";
            $scope.newCashIn.Note = "Thanh toán cho khách hàng " + $scope.InfoCustomer;
            $scope.newCashIn.PaidAmount = customer.TotalDebt + "";
        }
    }

    $scope.selectCashType = function (type) {
        $scope.Type = type;
        console.log($scope.Type);
        $http.get("/api/app/getCashTypes?type=" + $scope.Type).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.cashTypes = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
        if ($scope.Type == 1) {
            $scope.newCashIn.TargetType = "ORDER";
            $scope.newCashIn.TargetTypeName = "Phiếu mua hàng";
            $scope.newCashIn.CashTypeName = "thu";
            $scope.newCashIn.Note = "Thu tiền từ khách hàng " + $scope.InfoCustomer;
            $scope.newCashIn.PaidAmount = (0 - $scope.newCashIn.TotalDebt) + "";
        }
        else if ($scope.Type == 0) {
            $scope.newCashIn.TargetType = "RETURN";
            $scope.newCashIn.TargetTypeName = "Phiếu trả hàng";
            $scope.newCashIn.CashTypeName = "chi";
            $scope.newCashIn.Note = "Thanh toán cho khách hàng " + $scope.InfoCustomer;
            $scope.newCashIn.PaidAmount = $scope.newCashIn.TotalDebt + "";
        }
    }

    $scope.createCashType = function () {
        //$scope.newCashType.Type = 1;
        var post = $http({
            method: "POST",
            url: "/api/app/createCashType",
            data: $scope.newCashType
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                var id = data.data.CashTypeId;
                angular.element(document.querySelector('#addCashType')).modal('toggle');
                $http.get("/api/app/getCashTypes?type=" + $scope.type).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.cashTypes = data.data;
                        $scope.newCashIn.CashTypeId = id + '';
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mã phiếu đã được sử dụng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.createCash = function () {

        if ($scope.newCashIn.CashTypeId <= 0 || $scope.newCashIn.CashTypeId == undefined) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng lựa chọn loại phiếu')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }
        $scope.newCashIn.PaidAmount = $scope.newCashIn.PaidAmount + "";
        if ($scope.newCashIn.PaidAmount != undefined && $scope.newCashIn.PaidAmount != '') {
            if (!isNaN(parseInt($scope.newCashIn.PaidAmount))) {
                $scope.newCashIn.PaidAmount = parseInt($scope.newCashIn.PaidAmount) ? parseInt($scope.newCashIn.PaidAmount.replace(/\./g, '')) : $scope.newCashIn.PaidAmount;
            }
            else {
                $scope.newCashIn.PaidAmount = isNaN($scope.newCashIn.PaidAmount) ? parseInt($scope.newCashIn.PaidAmount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newCashIn.PaidAmount;
            }
        }

        if (isNaN($scope.newCashIn.PaidAmount) || $scope.newCashIn.PaidAmount <= 0) {
            $mdDialog.show(
                 $mdDialog.alert()
                                   .clickOutsideToClose(true)
                                   .title('Thông tin')
                                   .textContent('Vui lòng nhập số tiền')
                                   .ok('Đóng')
                                   .fullscreen(true)
                               );
            return;
        }

        //console.log($scope.Type);
        //console.log($scope.newCashIn.PaidAmount + "-" + $scope.newCashIn.TotalDebt);

        if ($scope.Type == 1) {
            if ($scope.newCashIn.PaidAmount > (0 - $scope.newCashIn.TotalDebt)) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số tiền bạn thu lớn hơn công nợ của khách hàng ' + $scope.newCashIn.TargetName + ', bạn vui lòng điền lại số tiền!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }
        else {
            if ($scope.newCashIn.PaidAmount > $scope.newCashIn.TotalDebt) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số tiền bạn trả lớn hơn công nợ của khách hàng ' + $scope.newCashIn.TargetName + ', bạn vui lòng điền lại số tiền!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }
        //}

        var post = $http({
            method: "POST",
            url: "/api/app/createCashFromCustomer",
            data: $scope.newCashIn
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                angular.element(document.querySelector('#addCashIn')).modal('toggle');
                $scope.newCashIn = { "PaidTargetType": "CUSTOMER", "CashTypeId": "-1" };
                $scope.Type == 1;
                //load cash
                //$scope.loadCustomers();
                //$scope.loadCashFlows($scope.cus);
                //var EmployeeId = angular.element(document.querySelector('#EmployeeId'));
                //$scope.loadCustomers();

                cfpLoadingBar.complete();

                $mdToast.show($mdToast.simple()
                   .textContent('Tạo phiếu thành công!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

}]);

myApp.controller('ProductPriceController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', function ProductPriceController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $filter, $mdDialog, $mdToast) {
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.isLoading = false;
    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.loadProductPrices();
        $scope.loadCategories();
    }

    $scope.loadCategories = function () {
        $http.get("/api/app/getCategories").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.categories = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadProductPrices = function () {
        $http.get("/api/app/getProductPrices?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.item_count;
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i].isPopoverOpen = false;
                    data.data[i].isPopoverPriceOpen = false;
                    data.data[i].activePriceIndex = 0;
                }
                $scope.productPrices = data.data;

            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $http.get("/api/app/getProductPrices?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.item_count;
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i].isPopoverOpen = false;
                    data.data[i].isPopoverPriceOpen = false;
                    data.data[i].activePriceIndex = 0;
                }
                $scope.productPrices = data.data;

            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageChange = function () {
        $http.get("/api/app/getProductPrices?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.item_count;
                for (var i = 0; i < data.data.length; i++) {
                    data.data[i].isPopoverOpen = false;
                    data.data[i].isPopoverPriceOpen = false;
                    data.data[i].activePriceIndex = 0;
                }
                $scope.productPrices = data.data;

            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onQueryChange = function () {
        var query = "";
        var txtSearch = myApp.formatSpecialchar($scope.q.txtSearch);
        if ($scope.q.category != '-1') {
            query = "category.CategoryId == " + $scope.q.category;
        }
        if ($scope.q.allowsell != '-1') {
            if (query != "") {
                if ($scope.q.allowsell == '0') {
                    query += " and PAStatus != 98 and PAStatus != 99";
                }
                else {
                    query += " and PAStatus = 98";
                }
            }
            else {
                if ($scope.q.allowsell == '0') {
                    query += "PAStatus != 98 and PAStatus != 99";
                }
                else {
                    query += "PAStatus == 98";
                }
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }

        query += ' and SearchQuery.Contains(\"' + txtSearch + '\")';

        $scope.query = query;
        $scope.loadProductPrices();
    }

    $scope.sumStock = function (stocks) {
        var count = 0;
        if (stocks != null) {
            for (var i = 0; i < stocks.length; i++) {
                count += stocks[i].Stock;
            }
        }
        return count;
    }

    $scope.findOriginPrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            price = productPrice.prices[productPrice.activePriceIndex].OriginPrice;
        }
        return price;
    }

    $scope.findAverageImportPrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            price = productPrice.prices[productPrice.activePriceIndex].AverageImportPrice;
        }
        return price;
    }

    $scope.findSalePrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            price = productPrice.prices[productPrice.activePriceIndex].SalePrice;
        }
        return price;
    }

    $scope.findSalePriceWithFormat = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            //price = $filter("currency")(productPrice.prices[productPrice.activePriceIndex].SalePrice, "", 0);
            price = productPrice.prices[productPrice.activePriceIndex].SalePrice;
        }
        return price;
    }

    $scope.findBaseUnit = function (productPrice) {
        var unit = "";
        if (productPrice != null) {
            unit = productPrice.prices[productPrice.activePriceIndex].unit.Unit;
            console.log(unit);
        }
        return unit;
    }

    $scope.findBaseUnitId = function (productPrice) {
        var unitId = 0;
        if (productPrice != null) {
            unitId = productPrice.prices[productPrice.activePriceIndex].unit.ProductUnitId;
        }
        return unitId;
    }

    $scope.changeUnit = function (productPrice, index) {
        productPrice.activePriceIndex = index;
        productPrice.isPopoverOpen = false;
        $scope.itemInit(productPrice);
    }

    $scope.itemInit = function (productPrice) {
        productPrice.operator = "+";
        productPrice.operatorValue = "VND";
        productPrice.changeValue = 0;
        productPrice.newPrice = $scope.findSalePrice(productPrice);
        productPrice.originalPrice = $scope.findSalePrice(productPrice);
    }

    $scope.changeOperator = function (productPrice, i) {
        if (i == 0) {
            productPrice.operator = "+";
        }
        else
            productPrice.operator = "-";
        $scope.changePrice(productPrice);
    }

    $scope.changeOperatorValue = function (productPrice, i) {
        if (i == 0)
            productPrice.operatorValue = "VND";
        else
            productPrice.operatorValue = "PER";
        $scope.changePrice(productPrice);
    }

    $scope.updatePrice = function (productPrice) {
        cfpLoadingBar.start();
        productPrice.isPopoverPriceOpen = false;
        var ProductPriceId = productPrice.prices[productPrice.activePriceIndex].ProductPriceId;
        var newPrice = {
            ProductId: productPrice.ProductId,
            ProductPriceId: ProductPriceId,
            NewPrice: productPrice.newPrice
        }

        //update
        var post = $http({
            method: "POST",
            url: "/api/app/updatePrice",
            data: newPrice
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.loadProductPrices();
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật giá thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Cập nhật giá thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );

            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật giá thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

                });
    }

    $scope.changeOption = function (productPrice) {
        if (productPrice.priceSelect == '0') {
            productPrice.newPrice = $scope.findSalePrice(productPrice);
            productPrice.originalPrice = $scope.findSalePrice(productPrice);
        }
        else if (productPrice.priceSelect == '1') {
            productPrice.newPrice = $scope.findAverageImportPrice(productPrice);
            productPrice.originalPrice = $scope.findAverageImportPrice(productPrice);
        }
        else {
            productPrice.originalPrice = $scope.findOriginPrice(productPrice);
            productPrice.newPrice = $scope.findOriginPrice(productPrice);
        }
        $scope.changePrice(productPrice);
        console.log(productPrice);
    }

    $scope.changePrice = function (productPrice) {
        if (productPrice.changeValue == undefined)
            return;
        if (productPrice.changeValue == NaN)
            productPrice.changeValue = 0;
        if (productPrice.operator == "+") {
            if (productPrice.operatorValue == "VND")
                productPrice.newPrice = productPrice.originalPrice + parseFloat(productPrice.changeValue);
            else {
                productPrice.newPrice = productPrice.originalPrice + productPrice.originalPrice * parseFloat(productPrice.changeValue) / 100;
            }
        }
        else {
            if (productPrice.operatorValue == "VND")
                productPrice.newPrice = productPrice.originalPrice - parseFloat(productPrice.changeValue);
            else {
                productPrice.newPrice = productPrice.originalPrice - productPrice.originalPrice * parseFloat(productPrice.changeValue) / 100;
            }
        }

    }

    $scope.loadImport = function (productPrice) {
        console.log($scope.findBaseUnitId(productPrice));
        $http.get("/api/app/getProductPricesImport?id=" + productPrice.ProductAttributeId + "&uid=" + $scope.findBaseUnitId(productPrice)).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                productPrice.imports = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        for (var i = 0; i < $scope.productPrices.length; i++) {
            if (productPrice.ProductAttributeId != $scope.productPrices[i].ProductAttributeId)
                $scope.productPrices[i].isExpand = false;
        }
        productPrice.isExpand = !productPrice.isExpand;
    }
}]);

myApp.controller('VendorController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function VendorController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = { "totaldebt": "0", "totalpaid": "0" };
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.newVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.loadVendors();
        $scope.loadProvinces();
    }

    $scope.loadVendors = function () {
        $http.get("/api/app/getVendors?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.Count;
                $scope.vendors = data.data;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadVendors();
    }

    $scope.onPageChange = function () {
        $scope.loadVendors();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.province != '-1') {
            query = "province.ProvinceId = " + $scope.q.province;
        }
        else {
            query = "1=1";
        }

        $scope.query = query;
        $scope.loadVendors();
    }

    $scope.onOrderChange = function () {

        if ($scope.lo.totaldebt != $scope.o.totaldebt) {
            if ($scope.o.totaldebt == 1)
                $scope.orderby = "TotalDebt desc";
            else
                $scope.orderby = "TotalDebt asc";
            //set lo
            $scope.o.totalpaid = "0";
            $scope.lo.totalpaid = "0";
            $scope.lo.totaldebt = $scope.o.totaldebt;
        }
        if ($scope.lo.totalpaid != $scope.o.totalpaid) {
            if ($scope.o.totalpaid == 1)
                $scope.orderby = "TotalPaid desc";
            else
                $scope.orderby = "TotalPaid asc";
            //set lo
            $scope.o.totaldebt = "0";
            $scope.lo.totaldebt = "0";
            $scope.lo.totalpaid = $scope.o.totalpaid;
        }
        $scope.loadVendors();
    }

    $scope.expandDetail = function (vendor) {
        for (var i = 0; i < $scope.vendors.length; i++) {
            if (vendor.VendorId != $scope.vendors[i].VendorId) {
                $scope.vendors[i].isExpand = false;
            }
        }
        vendor.isExpand = !vendor.isExpand;
    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadDistricts = function (province) {
        if (!$scope.isEditing) {
            $scope.newVendor.Province.Name = province.Name;
            $scope.newVendor.Province.ProvinceId = province.ProvinceId;
            $scope.newVendor.District.Name = "Quận / Huyện";
            $scope.newVendor.District.DistrictId = -1;
        }
        else {
            $scope.editVendor.Province.Name = province.Name;
            $scope.editVendor.Province.ProvinceId = province.ProvinceId;
            $scope.editVendor.District.Name = "Quận / Huyện";
            $scope.editVendor.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newVendor.District.Name = district.Name;
            $scope.newVendor.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editVendor.District.Name = district.Name;
            $scope.editVendor.District.DistrictId = district.DistrictId;
        }
    }

    $scope.openCreateVendorDialog = function () {
        //
        $http.get("/api/app/getVendorCode").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.newVendor.VendorCode = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
        $scope.newVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
        angular.element(document.querySelector('#addVendor')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addVendor')).modal('toggle');
        $scope.newVendor = {};
        $scope.newVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
        $scope.newVendor.Phone = '';
        $scope.newVendor.Email = '';
    }

    $scope.createVendor = function () {

        if (!$scope.frmAddVendor.$valid) {

            if ($scope.frmAddVendor.$error.required && $scope.frmAddVendor.$error.required[0].$name == "vendorname") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập tên nhà cung cấp!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmAddVendor.$error.required && $scope.frmAddVendor.$error.required[0].$name == "vendorphone") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số điện thoại !')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            } else if ($scope.frmAddVendor.$error.email) {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Vui lòng nhập đúng định dạng email !')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            } else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thành công ! Vui lòng kiểm tra lại thông tin')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            }
        }

        $scope.newVendor.DistrictId = parseInt($scope.newVendor.District.DistrictId);
        $scope.newVendor.ProvinceId = parseInt($scope.newVendor.Province.ProvinceId);

        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/createVendor",
            data: $scope.newVendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success              
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#addVendor')).modal('toggle')
                    $scope.newVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.page = 1;
                    $scope.newVendor = {};
                    $scope.newVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
                    $scope.loadVendors();
                }
            }
            else {
                $scope.newVendor.DistrictId = $scope.newVendor.District.DistrictId + '';
                $scope.newVendor.ProvinceId = $scope.newVendor.Province.ProvinceId + '';
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $scope.newVendor.DistrictId = $scope.newVendor.District.DistrictId + '';
                    $scope.newVendor.ProvinceId = $scope.newVendor.Province.ProvinceId + '';
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

                });
    }

    $scope.openEditVendorDialog = function (vendor) {
        //
        $scope.isEditing = true;
        $scope.editVendor = angular.copy(vendor);
        $scope.originEditVendor = angular.copy(vendor);
        if ($scope.editVendor.Province != null) {
            $scope.editVendor.Province.ProvinceId = $scope.editVendor.Province.ProvinceId + '';
            $scope.editVendor.District.DistrictId = $scope.editVendor.District.DistrictId + '';
            if ($scope.editVendor.Province.ProvinceId != '-1') {
                $http.get("/api/app/getDistricts?id=" + $scope.editVendor.Province.ProvinceId).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.districts = data.data.Districts;
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else {
                $scope.editVendor.Province.Name = "Tỉnh / Thành Phố";
                $scope.editVendor.District.Name = "Quận / Huyện";
            }
        }
        else {
            $scope.editVendor.Province = { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 };
            $scope.editVendor.District = { "DistrictId": -1, "Name": "Quận / Huyện" }
        }
        angular.element(document.querySelector('#editVendor')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.updateVendor = function () {
        if (!$scope.frmEditVendor.$valid) {
            if ($scope.frmEditVendor.$error.required && $scope.frmEditVendor.$error.required[0].$name == "vendorname") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập tên nhà cung cấp!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmEditVendor.$error.required && $scope.frmEditVendor.$error.required[0].$name == "vendorphone") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số điện thoại !')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            } else if ($scope.frmEditVendor.$error.email) {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Vui lòng nhập đúng định dạng email !')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            } else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thành công ! Vui lòng kiểm tra lại thông tin')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            }
        }
        cfpLoadingBar.start();
        $scope.editVendor.DistrictId = parseInt($scope.editVendor.District.DistrictId);
        $scope.editVendor.ProvinceId = parseInt($scope.editVendor.Province.ProvinceId);

        var post = $http({
            method: "POST",
            url: "/api/app/editVendor",
            data: $scope.editVendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success         
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editVendor')).modal('toggle')
                    $scope.editVendor = {};
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.loadVendors();
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Câp nhật đối tác thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật đối tác thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

                });
    }

    $scope.showConfirmDeleteVendor = function (vendor) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa nhà cung cấp ' + vendor.VendorName + ' hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteVendor(vendor);
        }, function () {
        });
    }

    $scope.deleteVendor = function (vendor) {
        if (vendor.TotalDebt != 0) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Công nợ của nhà cung cấp vẫn còn, bạn không thể xóa!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        var post = $http({
            method: "POST",
            url: "/api/app/deleteVendor",
            data: vendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.loadVendors();
                    $mdToast.show($mdToast.simple()
                     .textContent('Xóa nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa đối tác thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa đối tác thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

                });
    }

    $scope.loadVendorCashes = function (vendor) {
        $http.get("/api/app/getVendorCashes?id=" + vendor.VendorId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.cashes = data.data;

                for (var i = 0; i < data.data.length; i++) {
                    vendor.TotalOrderPrice = vendor.TotalOrderPrice + vendor.cashes.Total;
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadVendorExims = function (vendor) {
        $http.get("/api/app/loadVendorExims?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.exims = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadCashFlows = function (vendor, pagesize) {
        $http.get("/api/app/getVendorCashFlows?id=" + vendor.VendorId + "&page=" + pagesize + "&page_size=5").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                vendor.cashflows = data.data;
                $scope.itemcashflow_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadVendorEximsProduct = function (vendor, numberpage) {
        $http.get("/api/app/loadVendorEximsProduct?id=" + vendor.VendorId + "&page=" + numberpage + "&page_size=5").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.eximsproducts = data.data;
                $scope.EximsProductpage = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findType = function (status) {
        if (status == 0)
            return "Thanh toán";
        else if (status == 1)
            return "Bán hàng";
        else if (status == 2 || status == 4)
            return "Trả hàng";
        else if (status == 3)
            return "Nhập hàng";
        else if (status == 5)
            return "Hủy phiếu";
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.findPaymentStatus = function (cash) {
        if (cash.PaidAmount == 0)
            return "Chưa thanh toán";
        else if (cash.PaidAmount < cash.Total)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else
            return "Đã giao hàng"
    }
}]);

myApp.controller('DeliveryVendorController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function DeliveryVendorController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = { "totaldebt": "0", "totalpaid": "0" };
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.newDeliveryVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.loadVendors();
        $scope.loadProvinces();
    }

    //$scope.newDeliveryVendor = {};

    $scope.loadVendors = function () {
        $http.get("/api/app/getDeliveryVendors?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.item_count;
                $scope.vendors = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadVendors();
    }

    $scope.onPageChange = function () {
        $scope.loadVendors();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.province != '-1') {
            query = "province.ProvinceId = " + $scope.q.province;
        }
        else {
            query = "1=1";
        }

        $scope.query = query;
        $scope.loadVendors();
    }

    $scope.onOrderChange = function () {

        if ($scope.lo.totaldebt != $scope.o.totaldebt) {
            if ($scope.o.totaldebt == 1)
                $scope.orderby = "TotalDebt desc";
            else
                $scope.orderby = "TotalDebt asc";
            //set lo
            $scope.o.totalpaid = "0";
            $scope.lo.totalpaid = "0";
            $scope.lo.totaldebt = $scope.o.totaldebt;
        }
        if ($scope.lo.totalpaid != $scope.o.totalpaid) {
            if ($scope.o.totalpaid == 1)
                $scope.orderby = "TotalPaid desc";
            else
                $scope.orderby = "TotalPaid asc";
            //set lo
            $scope.o.totaldebt = "0";
            $scope.lo.totaldebt = "0";
            $scope.lo.totalpaid = $scope.o.totalpaid;
        }
        $scope.loadVendors();
    }

    $scope.expandDetail = function (vendor) {
        for (var i = 0; i < $scope.vendors.length; i++) {
            if (vendor.DeliveryVendorId != $scope.vendors[i].DeliveryVendorId)
                $scope.vendors[i].isExpand = false;
        }
        vendor.isExpand = !vendor.isExpand;
    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadDistricts = function (province) {
        if (!$scope.isEditing) {
            $scope.newDeliveryVendor.Province.Name = province.Name;
            $scope.newDeliveryVendor.Province.ProvinceId = province.ProvinceId;
            $scope.newDeliveryVendor.District.Name = "Quận / Huyện";
            $scope.newDeliveryVendor.District.DistrictId = -1;
        }
        else {
            $scope.editDeliveryVendor.Province.Name = province.Name;
            $scope.editDeliveryVendor.Province.ProvinceId = province.ProvinceId;
            $scope.editDeliveryVendor.District.Name = "Quận / Huyện";
            $scope.editDeliveryVendor.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newDeliveryVendor.District.Name = district.Name;
            $scope.newDeliveryVendor.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editDeliveryVendor.District.Name = district.Name;
            $scope.editDeliveryVendor.District.DistrictId = district.DistrictId;
        }
    }

    $scope.openCreateDeliveryVendorDialog = function () {
        //
        $http.get("/api/app/getDeliveryVendorCode").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.newDeliveryVendor.VendorCode = data.data;
            }
        })
        .error(function (data, status, headers, config) {
        });
        $scope.newDeliveryVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
        angular.element(document.querySelector('#addDeliveryVendor')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addDeliveryVendor')).modal('toggle');
        $scope.newDeliveryVendor = {};
        $scope.newDeliveryVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
        $scope.newDeliveryVendor.Phone = '';
    }

    $scope.createDeliveryVendor = function () {
        if (!$scope.frmAddDeliveryVendor.$valid) {
            console.log($scope.frmAddDeliveryVendor);
            if ($scope.frmAddDeliveryVendor.$error.required && $scope.frmAddDeliveryVendor.$error.required[0].$name == "DeliveryName") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập tên nhà cung cấp!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmAddDeliveryVendor.$error.required && $scope.frmAddDeliveryVendor.$error.required[0].$name == "txtPhone") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số điện thoại !')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thành công ! Vui lòng kiểm tra lại thông tin')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            }
        }
        cfpLoadingBar.start();
        $scope.newDeliveryVendor.DistrictId = parseInt($scope.newDeliveryVendor.District.DistrictId);
        $scope.newDeliveryVendor.ProvinceId = parseInt($scope.newDeliveryVendor.Province.ProvinceId);
        var post = $http({
            method: "POST",
            url: "/api/app/createDeliveryVendor",
            data: $scope.newDeliveryVendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#addDeliveryVendor')).modal('toggle')
                    $scope.newDeliveryVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.page = 1;
                    $scope.newDeliveryVendor = {};
                    $scope.newDeliveryVendor = { "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
                    $scope.loadVendors();
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.openEditDeliveryVendorDialog = function (vendor) {
        $scope.isEditing = true;
        $scope.editDeliveryVendor = angular.copy(vendor);
        $scope.originEditVendor = angular.copy(vendor);
        if ($scope.editDeliveryVendor.Province != null) {
            $scope.editDeliveryVendor.Province.ProvinceId = $scope.editDeliveryVendor.Province.ProvinceId + '';
            $scope.editDeliveryVendor.District.DistrictId = $scope.editDeliveryVendor.District.DistrictId + '';
            if ($scope.editDeliveryVendor.Province.ProvinceId != '-1') {
                $http.get("/api/app/getDistricts?id=" + $scope.editDeliveryVendor.Province.ProvinceId).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.districts = data.data.Districts;
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else {
                $scope.editDeliveryVendor.Province.Name = "Tỉnh / Thành Phố";
                $scope.editDeliveryVendor.District.Name = "Quận / Huyện";
            }
        }
        else {
            $scope.editDeliveryVendor.Province = { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 };
            $scope.editDeliveryVendor.District = { "DistrictId": -1, "Name": "Quận / Huyện" }
        }
        angular.element(document.querySelector('#editDeliveryVendor')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.updateDeliveryVendor = function () {
        if (!$scope.frmEditDeliveryVendor.$valid) {
            console.log($scope.frmEditDeliveryVendor);
            if ($scope.frmEditDeliveryVendor.$error.required && $scope.frmEditDeliveryVendor.$error.required[0].$name == "DeliveryName") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập tên nhà cung cấp!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else if ($scope.frmEditDeliveryVendor.$error.required && $scope.frmEditDeliveryVendor.$error.required[0].$name == "txtPhone") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập đúng định dạng số điện thoại !')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
            else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Không thành công ! Vui lòng kiểm tra lại thông tin')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
                return;
            }
        }
        cfpLoadingBar.start();
        $scope.editDeliveryVendor.DistrictId = parseInt($scope.editDeliveryVendor.District.DistrictId);
        $scope.editDeliveryVendor.ProvinceId = parseInt($scope.editDeliveryVendor.Province.ProvinceId);

        var post = $http({
            method: "POST",
            url: "/api/app/editDeliveryVendor",
            data: $scope.editDeliveryVendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editDeliveryVendor')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.editDeliveryVendor = {};
                    $scope.loadVendors();
                }
            }
            else {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Cập nhật nhà cung cấp thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.showConfirmDeleteDeliveryVendor = function (vendor) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa nhà cung cấp ' + vendor.VendorName + ' hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteDeliveryVendor(vendor);
        }, function () {
        });
    }

    $scope.deleteDeliveryVendor = function (vendor) {
        var post = $http({
            method: "POST",
            url: "/api/app/deleteDeliveryVendor",
            data: vendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.loadVendors();
                    $mdToast.show($mdToast.simple()
                     .textContent('Xóa nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.loadVendorCashes = function (vendor, pagenumber) {
        $http.get("/api/app/getDVendorCashes?id=" + vendor.DeliveryVendorId + "&page=" + pagenumber + "&page_size=5").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.cashes = data.data;
                $scope.itemVendorCashes_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.findPaymentStatus = function (cash) {
        if (cash.PaidAmount == 0)
            return "Chưa thanh toán";
        else if (cash.PaidAmount < cash.Total)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }
}]);

myApp.controller('ImportController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function ImportController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 15;
    $scope.newVendor = {};
    $scope.isDone = false;

    $scope.init = function () {
        $scope.start();
        $scope.loadEmployees();
        $scope.loadBranches();
    }

    $scope.start = function () {
        cfpLoadingBar.start();
    };

    $scope.complete = function () {
        cfpLoadingBar.complete();
    }

    $scope.loadEmployees = function () {
        $http.get("/api/app/getEmployees?page=1&page_size=100&query=1=1").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.employees = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.removeSelectedProduct = function (product) {
        console.log(product);
        var index = -1;
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].ProductAttributeId == product.ProductAttributeId) {
                index = i;
                break;
            }
        }
        $scope.selectedProducts.splice(index, 1);
        //$scope.calculateOrderPrice();
    }

    $scope.loadBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.branches = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectedProducts = new Array();
    $scope.selectedVendor = { VendorName: 'Nhà cung cấp chưa xác định' };

    var isLoading = false;

    $scope.getSuggestProducts = function (val) {
        val = myApp.formatSpecialchar(val)
        return $http.get('/api/app/getProducts', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                query: 'SearchQuery.contains(\"' + val + '\")',
                order_by: ''
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.sumStock = function (stocks) {
        var count = 0;
        if (stocks != null) {
            for (var i = 0; i < stocks.length; i++) {
                count += stocks[i].Stock;
            }
        }
        return count;
    }

    $scope.selectProduct = function (product) {
        //check exist
        console.log(product.PStatus + " -" + product.ProductAttributeId);
        if (product.PAStatus == 98) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Sản phẩm bạn chọn đã "Ngừng kinh doanh", vui lòng chọn lại hoặc chuyển trạng thái sản phẩm sang "Đang kinh doanh" !')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return
        }
        if ($scope.selectedProducts != undefined) {
            var isDup = false;
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                if ($scope.selectedProducts[i].ProductAttributeId == product.ProductAttributeId)
                    isDup = true;
            }
            if (!isDup)
                $scope.selectedProducts.push(product);
        }
        else
            $scope.selectedProducts.push(product);
    }

    $scope.initProduct = function (product) {
        product.originPrice = $scope.findOriginPrice(product.prices);
        product.quantity = "1";
        product.discount = "0";
        product.total = (parseInt(product.originPrice) - parseInt(product.discount)) * parseInt(product.quantity);
    }

    $scope.findOriginPrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            var index = -1;
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            price = productPrice[index].OriginPrice
        }
        return price;
    }

    $scope.onPriceChange = function (product, k) {

        if (k == 1) {
            console.log("originPrice: " + product.originPrice);
            if (product.originPrice != undefined && product.originPrice != '') {
                product.originPrice = parseInt(product.originPrice) ? parseInt(product.originPrice.replace(/\./g, '')) : product.originPrice;
            }
            else {
                product.originPrice = '';
            }
        }
        else if (k == 2) {
            if (product.discount != undefined && product.discount != '') {
                product.discount = parseInt(product.discount) ? parseInt(product.discount.replace(/\./g, '')) : product.discount;
            }
            else {
                product.discount = '';
            }
        }
        else {
            if (product.quantity != undefined && product.quantity != '') {
                product.quantity = isNaN(product.quantity) ? parseInt(product.quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.quantity;
            }
            else {
                product.quantity = '';
            }
        }

        product.total = (product.originPrice - product.discount) * product.quantity;
        if (isNaN(parseInt(product.total))) product.total = 0;
    }

    $scope.onDiscountPriceChange = function () {
        if ($scope.newImport.discount != undefined && $scope.newImport.discount != '') {
            $scope.newImport.discount = parseInt($scope.newImport.discount) ? parseInt($scope.newImport.discount.replace(/\./g, '')) : $scope.newImport.discount;
        }
        else {
            $scope.newImport.discount = '';
        }
    }

    $scope.onPaidPriceChange = function () {
        if ($scope.newImport.paid != undefined && $scope.newImport.paid != '') {
            $scope.newImport.paid = parseInt($scope.newImport.paid) ? parseInt($scope.newImport.paid.replace(/\./g, '')) : $scope.newImport.paid;
        }
        else {
            $scope.newImport.paid = '';
        }
    }

    $scope.openCreateVendorDialog = function () {
        //
        $http.get("/api/app/getVendorCode").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.newVendor.VendorCode = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
        angular.element(document.querySelector('#addVendor')).modal({ backdrop: 'static', keyboard: true })
        $scope.newVendor = {};
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addVendor')).modal('toggle');
        $scope.newVendor = {};
        $scope.newVendor.Phone = '';
    }

    $scope.createVendor = function () {
        if (!$scope.frmAddVendor.$valid) {
            return;
        }
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/createVendor",
            data: $scope.newVendor
        });

        post.success(function successCallback(data, status, headers, config) {
            // success     
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#addVendor')).modal('toggle')
                    $scope.newVendor = {};
                    $scope.selectedVendor = data.data;
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm nhà cung cấp thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo nhà cung cấp thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo nhà cung cấp thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.getSuggestVendors = function (val) {
        val = myApp.formatSpecialchar(val)
        return $http.get('/api/app/getVendors', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                query: 'SearchQuery.contains(\"' + val + '\")',
                order_by: ''
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.selectVendor = function (vendor) {
        $scope.selectedVendor = vendor;
    }

    $scope.calculateQuatity = function () {
        var quantity = 0;
        if ($scope.selectedProducts != undefined && $scope.selectedProducts.length > 0) {
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                if ($scope.selectedProducts[i].quantity != undefined && $scope.selectedProducts[i].quantity != '') {
                    quantity += parseInt($scope.selectedProducts[i].quantity);
                }
            }
        }

        return quantity;
    }

    $scope.calculateTotal = function () {
        var total = 0;
        if ($scope.selectedProducts != undefined && $scope.selectedProducts.length > 0) {
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                if ($scope.selectedProducts[i].total != undefined && $scope.selectedProducts[i].total != '') {
                    total += parseInt($scope.selectedProducts[i].total);
                }
            }
        }
        if (isNaN(parseInt(total))) total = 0;
        return total;
    }

    $scope.createImport = function (temp) {
        if ($scope.isDone) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo phiếu nhập hàng thành công, vui lòng kiểm tra lại trong mục quản lý nhập / xuất hàng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }
        if (!$scope.frmImport.$valid) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Vui lòng nhập đầy đủ thông tin!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }
        if ($scope.calculateQuatity() <= 0) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Vui lòng nhập số lượng nhập hàng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        var importProducts = new Array();
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            var product = {};
            product.ProductAttributeId = $scope.selectedProducts[i].ProductAttributeId;
            product.Quantity = parseInt($scope.selectedProducts[i].quantity);
            product.OriginPrice = isNaN($scope.selectedProducts[i].originPrice) ? parseInt($scope.selectedProducts[i].originPrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.selectedProducts[i].originPrice;
            product.Discount = parseFloat($scope.selectedProducts[i].discount);
            importProducts.push(product);
        }
        //check vendor
        if ($scope.selectedVendor == undefined || $scope.selectedVendor.VendorId == undefined) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng chọn nhà cung cấp!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }

        //check price paid
        if ($scope.newImport.paid > ($scope.calculateTotal() - $scope.newImport.discount)) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số tiền trả nhà cung cấp đang lớn hơn tổng số tiền của đơn nhập hàng!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }

        $scope.newImport.selectedProducts = importProducts;
        $scope.newImport.selectedVendorId = $scope.selectedVendor.VendorId;
        $scope.newImport.VendorName = $scope.selectedVendor.VendorName;
        $scope.newImport.paid = parseFloat($scope.newImport.paid);
        $scope.newImport.PaymentType = parseFloat($scope.newImport.PaymentType);
        //remove invalid
        $scope.newImport.discount = isNaN($scope.newImport.discount) ? parseInt($scope.newImport.discount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newImport.discount;
        $scope.newImport.paid = isNaN($scope.newImport.paid) ? parseInt($scope.newImport.paid.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newImport.paid;
        $scope.newImport.isTemp = temp;
        var post = $http({
            method: "POST",
            url: "/api/app/createImport",
            data: $scope.newImport
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                $scope.isDone = true;
                if (data.meta.error_code == 200) {
                    //success
                    //$scope.newImport = { "BranchId": "-1", "PaymentType": "0", "discount": 0, "paid": 0, "PaymentType": "0" }; comment by phungTheNhan
                    //confirm
                    var confirm = $mdDialog.confirm()
                      .title('Thông báo')
                      .textContent('Tạo phiếu nhập hàng thành công, vui lòng kiểm tra lại trong mục quản lý nhập / xuất hàng!')
                      .ok('Làm mới!')
                      .cancel('Về trang quản lý');

                    $mdDialog.show(confirm).then(function () {
                        $window.location.href = '/imports.html';
                    }, function () {
                        $window.location.href = '/exims.html';
                    });
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo phiếu nhập hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo phiếu nhập hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

}]);

myApp.controller('CashController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CashController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = { "totaldebt": "0", "totalpaid": "0" };
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptionsStart = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.dateOptionsEnd = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0) {
            $scope.DatePopup.startDate = true;
        }
        else {
            $scope.DatePopup.endDate = true;
        }
    }

    $scope.init = function () {
        cfpLoadingBar.start();
        $scope.loadCashes();
    }

    $scope.loadCashes = function () {
        $http.get("/api/app/getCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.cashes = data.data;
                $scope.item_count = data.metadata.item_count;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $http.get("/api/app/getCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.cashes = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageChange = function () {
        $http.get("/api/app/getCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.cashes = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.type != '-1') {
            query = "Type = " + $scope.q.type;
        }
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(year, month, date),
                startingDay: 1
            };
        }
        else {
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(1900, 1, 1),
                startingDay: 1
            };
        }
        if ($scope.q.EndDate != undefined) {
            var year = $scope.q.EndDate.getFullYear();
            var month = $scope.q.EndDate.getMonth();
            var date = $scope.q.EndDate.getDate();
            if (query != "") {
                query += " and CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(year, month, date),
                startingDay: 1
            };
        }

        else {
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(),
                startingDay: 1
            };
        }
        if (query == "")
            query = "1=1";
        $scope.query = query;
        $scope.loadCashes();
    }

    $scope.onOrderChange = function () {
        $scope.loadCashes();
    }

    $scope.expandDetail = function (cash) {
        for (var i = 0; i < $scope.cashes.length; i++) {
            if (cash.CashId != $scope.cashes[i].CashId)
                $scope.cashes[i].isExpand = false;
        }
        cash.isExpand = !cash.isExpand;
    }

    $scope.getMessage = function (cash) {
        var message = "";
        if (cash.target != undefined) {
            if (cash.TargetType == "IMPORT") {
                message = "Phiếu thu tự động được gắn với phiếu nhập " + cash.target.TargetCode;
            }
            else if (cash.TargetType == "EXPORT") {
                message = "Phiếu thu tự động được gắn với phiếu xuất " + cash.target.TargetCode;
            }
            else if (cash.TargetType == "ORDER") {
                message = "Phiếu thu tự động được gắn với hóa đơn " + cash.target.TargetCode;
            }
        }
        return message;
    }

    $scope.findType = function (cash) {
        var type = "";
        if (cash.cashType == null || cash.cashType == undefined) {
            if (cash.Type == 1)
                return "Phiếu thu";
            else
                return "Phiếu chi";
        }
        else
            return cash.cashType.Name;
    }

    $scope.updateCash = function (cash) {
        cfpLoadingBar.start();

        var post = $http({
            method: "POST",
            url: "/api/app/updateCash",
            data: cash
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.loadCashes();
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật phiếu thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                order.DeliveryStatus = baseDeliveryStatus;
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            order.DeliveryStatus = baseDeliveryStatus;
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Cập nhật phiếu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.openCreateCash = function (type) {
        //
        $scope.isEditing = false;
        $scope.type = type;
        $http.get("/api/app/getCashTypes?type=" + type).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.cashTypes = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        if (type == 1) {
            angular.element(document.querySelector('#addCashIn')).modal({ backdrop: 'static', keyboard: true });
            $scope.newCashIn = { "PaidTargetType": "-1", "CashTypeId": "-1", "TargetType": "-1" };
            $scope.isCashInOpen = true;
            $scope.isCashOutOpen = false;
        }
        else {
            angular.element(document.querySelector('#addCashOut')).modal({ backdrop: 'static', keyboard: true });
            $scope.newCashOut = { "PaidTargetType": "-1", "CashTypeId": "-1", "TargetType": "-1" };
            $scope.isCashInOpen = false;
            $scope.isCashOutOpen = true;
        }
    }

    $scope.createCashType = function () {
        var post = $http({
            method: "POST",
            url: "/api/app/createCashType",
            data: $scope.newCashType
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                $scope.newCashType = { "Type": 1 };
                var id = data.data.CashTypeId;
                angular.element(document.querySelector('#addCashType')).modal('toggle');
                $http.get("/api/app/getCashTypes?type=" + $scope.type).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.cashTypes = data.data;
                        if ($scope.isCashInOpen)
                            $scope.newCashIn.CashTypeId = id + '';
                        else
                            $scope.newCashOut.CashTypeId = id + '';
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mã phiếu đã được sử dụng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.onTypeChange = function () {
        $scope.newCashIn.selectedTarget = undefined;
        $scope.newCashOut.selectedTarget = undefined;
    }

    $scope.onTargetTypeChange = function () {
        if ($scope.newCashIn.TargetType == -1)
            $scope.newCashIn.TargetCode = undefined;
        if ($scope.newCashOut.TargetType == -1)
            $scope.newCashOut.TargetCode = undefined;
    }

    $scope.selectTarget = function (target) {
        $scope.newCashIn.selectedTarget = target;
        $scope.newCashOut.selectedTarget = target;
        $scope.findName();
    }

    $scope.findName = function () {
        if ($scope.isCashInOpen) {
            if ($scope.newCashIn.selectedTarget != undefined) {
                if ($scope.newCashIn.PaidTargetType == "CUSTOMER")
                    $scope.newCashIn.selectedTarget.Name = $scope.newCashIn.selectedTarget.CustomerName;
                else if ($scope.newCashIn.PaidTargetType == "VENDOR")
                    $scope.newCashIn.selectedTarget.Name = $scope.newCashIn.selectedTarget.VendorName;
                else if ($scope.newCashIn.PaidTargetType == "EMPLOYEE")
                    $scope.newCashIn.selectedTarget.Name = $scope.newCashIn.selectedTarget.EmployeeName;
                else if ($scope.newCashIn.PaidTargetType == "DELIVERY_VENDOR")
                    //$scope.newCashIn.selectedTarget.$scope.newCashIn = $scope.newCashIn.selectedTarget.VendorName;
                    $scope.newCashIn.selectedTarget.Name = $scope.newCashIn.selectedTarget.VendorName;
            }
        }
        else {
            if ($scope.newCashOut.selectedTarget != undefined) {
                if ($scope.newCashOut.PaidTargetType == "CUSTOMER")
                    $scope.newCashOut.selectedTarget.Name = $scope.newCashOut.selectedTarget.CustomerName;
                else if ($scope.newCashOut.PaidTargetType == "VENDOR")
                    $scope.newCashOut.selectedTarget.Name = $scope.newCashOut.selectedTarget.VendorName;
                else if ($scope.newCashOut.PaidTargetType == "EMPLOYEE")
                    $scope.newCashOut.selectedTarget.Name = $scope.newCashOut.selectedTarget.EmployeeName;
                else if ($scope.newCashOut.PaidTargetType == "DELIVERY_VENDOR")
                    //$scope.newCashOut.selectedTarget.$scope.newCashOut = $scope.newCashOut.selectedTarget.VendorName;
                    $scope.newCashOut.selectedTarget.Name = $scope.newCashOut.selectedTarget.VendorName;
            }
        }
    }

    $scope.getSuggestCustomers = function (val) {
        val = myApp.formatSpecialchar(val);
        return $http.get('/api/app/getCustomers', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                order_by: '',
                query: 'SearchQuery.contains(\"' + val + '\")'
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getSuggestVendors = function (val) {
        val = myApp.formatSpecialchar(val)
        return $http.get('/api/app/getVendors', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                order_by: '',
                query: 'SearchQuery.contains(\"' + val + '\")'
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getSuggestEmployees = function (val) {
        return $http.get('/api/app/getEmployees', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                query: 'EmployeeName.contains(\"' + val + '\")'
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getSuggestDVendors = function (val) {
        val = myApp.formatSpecialchar(val)
        return $http.get('/api/app/getDeliveryVendors', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                order_by: '',
                query: 'SearchQuery.contains(\"' + val + '\")'
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.createCash = function (type) {
        var data;
        if (type == 1)
            data = $scope.newCashIn;
        else
            data = $scope.newCashOut;
        if (data.selectedTarget != undefined) {
            if (data.PaidTargetType == "CUSTOMER")
                data.PaidTargetId = data.selectedTarget.CustomerId;
            else if (data.PaidTargetType == "VENDOR")
                data.PaidTargetId = data.selectedTarget.VendorId;
            else if (data.PaidTargetType == "EMPLOYEE")
                data.PaidTargetId = data.selectedTarget.EmployeeId;
            else if (data.PaidTargetType == "DELIVERY_VENDOR")
                data.PaidTargetId = data.selectedTarget.DeliveryVendorId;
        }
        else {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng lựa chọn đối tượng')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }

        if (data.CashTypeId <= 0 || data.CashTypeId == undefined) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng lựa chọn loại phiếu')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }

        //check if target type != -1
        if (data.TargetType != -1) {
            if (data.TargetCode == undefined || data.TargetCode == "") {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng nhập mã phiếu!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }

        data.PaidAmount = isNaN(data.PaidAmount) ? parseInt(data.PaidAmount.replace(/[^\d|\-+|\.+]/g, '')) : data.PaidAmount;
        if (isNaN(data.PaidAmount) || data.PaidAmount <= 0) {
            $mdDialog.show(
                                 $mdDialog.alert()
                                   .clickOutsideToClose(true)
                                   .title('Thông tin')
                                   .textContent('Vui lòng nhập số tiền')
                                   .ok('Đóng')
                                   .fullscreen(true)
                               );
            return;
        }

        var post = $http({
            method: "POST",
            url: "/api/app/createCash",
            data: data
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                if (type == 1) {
                    angular.element(document.querySelector('#addCashIn')).modal('toggle');
                    $scope.newCashIn = { "PaidTargetType": "-1", "CashTypeId": "-1", "TargetType": "-1" };
                }
                else {
                    angular.element(document.querySelector('#addCashOut')).modal('toggle');
                    $scope.newCashOut = { "PaidTargetType": "-1", "CashTypeId": "-1", "TargetType": "-1" };
                }
                cfpLoadingBar.complete();
                $scope.loadCashes();
                $mdToast.show($mdToast.simple()
                   .textContent('Tạo phiếu thành công!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
            }
            else if (data.meta.error_code == 213) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mã phiếu không hợp lệ!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.findCashType = function (cash) {
        if (cash.cashType == undefined || cash.cashType == null) {
            if (cash.CashCode.indexOf("PCPN") >= 0)
                return "Phiếu chi phiếu nhập";
            else if (cash.CashCode.indexOf("PCTH") >= 0)
                return "Phiếu chi trả hàng";
            else if (cash.CashCode.indexOf("PTHD") >= 0)
                return "Phiếu thu hóa đơn";
        }
        else
            return cash.cashType.Name;
    }

    $scope.showConfirmCancelCashes = function (cash) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn hủy phiếu này không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.cancelCashes(cash);
        }, function () {
        });
    }

    $scope.cancelCashes = function (cash) {
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/cancelCashes",
            data: cash
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                     .textContent('Hủy phiếu thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    cash.isExpand = false;
                    $scope.loadCashes();
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Hủy phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Hủy phiếu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

}]);

myApp.controller('EximController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function EximController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.init = function () {
        cfpLoadingBar.start();
        $scope.loadExims();
    }

    $scope.loadExims = function () {
        $http.get("/api/app/getExims?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                console.log(data.data);
                $scope.item_count = data.metadata.item_count;
                $scope.exims = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadExims();
    }

    $scope.onPageChange = function () {
        $scope.loadExims();
    }

    $scope.onQueryChange = function () {
        var query = "";
        //if ($scope.q.type != '-1') {
        //    query = "Type = " + $scope.q.type;
        //}
        if ($scope.q.paymentStatus != '-1') {
            if (query != "") {
                query += " and PaymentStatus =" + $scope.q.paymentStatus;
            }
            else {
                query += "PaymentStatus =" + $scope.q.paymentStatus;
            }
        }
        if ($scope.q.deliveryStatus != '-1') {
            if (query != "") {
                query += " and DeliveryStatus =" + $scope.q.deliveryStatus;
            }
            else {
                query += "DeliveryStatus =" + $scope.q.deliveryStatus;
            }
        }


        else {
            if (query == "")
                query = "1=1";
        }

        if ($scope.q.txtSearch != '' && $scope.q.txtSearch != undefined) {
            var txtSearch = myApp.formatSpecialchar($scope.q.txtSearch);
            query += ' and (Code.Contains(\"' + txtSearch + '\") OR target.SearchQuery.Contains(\"' + txtSearch + '\")) ';
        }

        $scope.query = query;
        $scope.loadExims();
    }
    $scope.filterPaystatus = function (startus) {
        switch (startus) {
            case 0:
                return "Chưa thanh toán";
                break;
            case 1:
                return "Chưa thanh toán hết";
                break;
            case 2:
                return "Đã thanh toán";
                break;

            default:
                break;
        }

    }

    $scope.loadProducts = function (exim) {
        $http.get("/api/app/getEximProducts?page=" + $scope.page + "&page_size=" + $scope.page_size + "&id=" + exim.EximId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                exim.products = data.data;

                //check return product status
                var quantityTotal = 0;
                for (var i = 0; i < exim.products.length; i++) {
                    quantityTotal += exim.products[i].Quantity;
                }
                if (quantityTotal > exim.TotalReturnProduct) {
                    $scope.StatusProduct = 1;
                }
                else {
                    $scope.StatusProduct = 0;
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.expandDetail = function (exim) {
        for (var i = 0; i < $scope.exims.length; i++) {
            if (exim.EximId != $scope.exims[i].EximId)
                $scope.exims[i].isExpand = false;
        }
        if (!exim.isExpand)
            $scope.loadProducts(exim);
        exim.isExpand = !exim.isExpand;
    }

    $scope.findPaymentStatus = function (status) {
        if (status == 0)
            return "Chưa thanh toán";
        else if (status == 1)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else
            return "Đã giao hàng"
    }

    $scope.showConfirmCompleteImport = function (exim) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn hoàn thành hóa đơn này không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.complete(exim);
        }, function () {
        });
    }

    $scope.complete = function (exim) {
        var post = $http({
            method: "POST",
            url: "/api/app/completeImport",
            data: exim
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                $scope.loadExims();
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                    .textContent('Cập nhật phiếu thành công!')
                    .position('fixed bottom right')
                    .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật phiếu nhập hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật phiếu nhập hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.showConfirmCancelImport = function (exim) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn hủy hóa đơn này không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.cancel(exim);
        }, function () {
        });
    }

    $scope.cancel = function (exim) {
        var post = $http({
            method: "POST",
            url: "/api/app/cancelImport",
            data: exim
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                $scope.loadExims();
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                    .textContent('Hủy phiếu nhập thành công!')
                    .position('fixed bottom right')
                    .hideDelay(3000));
                }

            }
            else if (data.meta.error_code == 212) {

                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Bạn nhập thiếu thông tin, vui lòng kiểm tra lại!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Hủy phiếu nhập hàng thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.loadCashes = function (exim, pagenumber) {
        $http.get("/api/app/getEximCashes?page=" + pagenumber + "&page_size=5&id=" + exim.EximId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                exim.cashes = data.data;
                $scope.itemexim_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    //cash
    $scope.openCreateCash = function (exim) {
        //
        $scope.type = 0;
        $http.get("/api/app/getCashTypes?type=" + $scope.type).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.cashTypes = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        angular.element(document.querySelector('#addCashIn')).modal({ backdrop: 'static', keyboard: true });
        $scope.newCashIn = { "PaidTargetType": "-1", "CashTypeId": "-1", "TargetType": "-1" };
        $scope.isCashInOpen = true;

        $scope.newCashIn.PaidTargetType = "VENDOR";
        $scope.newCashIn.PaidTargetId = exim.TargetId;
        $scope.newCashIn.TargetName = exim.target.TargetName;
        $scope.newCashIn.TargetId = exim.EximId;
        $scope.newCashIn.TargetType = "IMPORT";
        $scope.newCashIn.PaidAmount = (exim.Price - exim.Discount - exim.CustomerPaid - exim.ReturnPaid) + "";
        $scope.newCashIn.Note = "Thanh toán cho nhà cung cấp " + exim.target.TargetName + "-" + exim.target.TargetCode;
    }

    $scope.createCashType = function () {
        $scope.newCashType.Type = 0;
        var post = $http({
            method: "POST",
            url: "/api/app/createCashType",
            data: $scope.newCashType
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                var id = data.data.CashTypeId;
                angular.element(document.querySelector('#addCashType')).modal('toggle');
                $http.get("/api/app/getCashTypes?type=" + $scope.type).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.cashTypes = data.data;
                        if ($scope.isCashInOpen)
                            $scope.newCashIn.CashTypeId = id + '';
                        else
                            $scope.newCashOut.CashTypeId = id + '';
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mã phiếu đã được sử dụng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.createCash = function () {

        if ($scope.newCashIn.CashTypeId <= 0 || $scope.newCashIn.CashTypeId == undefined) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng lựa chọn loại phiếu')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }

        if ($scope.newCashIn.PaidAmount != undefined && $scope.newCashIn.PaidAmount != '') {
            if (!isNaN(parseInt($scope.newCashIn.PaidAmount))) {
                $scope.newCashIn.PaidAmount = parseInt($scope.newCashIn.PaidAmount) ? parseInt($scope.newCashIn.PaidAmount.replace(/\./g, '')) : $scope.newCashIn.PaidAmount;
            }
            else {
                $scope.newCashIn.PaidAmount = isNaN($scope.newCashIn.PaidAmount) ? parseInt($scope.newCashIn.PaidAmount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newCashIn.PaidAmount;
            }
        }

        if (isNaN($scope.newCashIn.PaidAmount) || $scope.newCashIn.PaidAmount <= 0) {
            $mdDialog.show(
                 $mdDialog.alert()
                                   .clickOutsideToClose(true)
                                   .title('Thông tin')
                                   .textContent('Vui lòng nhập số tiền')
                                   .ok('Đóng')
                                   .fullscreen(true)
                               );
            return;
        }

        for (var i = 0; i < $scope.exims.length; i++) {
            if ($scope.newCashIn.PaidAmount > ($scope.exims[i].Price - $scope.exims[i].Discount - $scope.exims[i].CustomerPaid - $scope.exims.ReturnPaid) && $scope.exims[i].EximId == $scope.newCashIn.TargetId) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số tiền bạn thu lớn hơn công nợ của đơn hàng ' + $scope.exims[i].OrderCode + ', mời bạn điền lại số tiền!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }

        var post = $http({
            method: "POST",
            url: "/api/app/createCashFromExim",
            data: $scope.newCashIn
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                angular.element(document.querySelector('#addCashIn')).modal('toggle');
                $scope.newCashIn = { "PaidTargetType": "VENDOR", "CashTypeId": "-1" };

                cfpLoadingBar.complete();
                $scope.loadExims();
                $mdToast.show($mdToast.simple()
                   .textContent('Tạo phiếu thành công!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo loại thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }
}]);

myApp.controller('DebtController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CashController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        cfpLoadingBar.start();
        $scope.loadCashes();
    }

    $scope.loadCashes = function () {
        $http.get("/api/app/getCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.cashes = data.data;
                $scope.item_count = data.metadata.item_count;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $http.get("/api/app/getCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.cashes = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageChange = function () {
        $http.get("/api/app/getCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.cashes = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.type != '-1') {
            query = "cashType.Type = " + $scope.q.type;
        }
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
        }
        if ($scope.q.EndDate != undefined) {
            var year = $scope.q.EndDate.getFullYear();
            var month = $scope.q.EndDate.getMonth();
            var date = $scope.q.EndDate.getDate();
            if (query != "") {
                query += " and CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }
        $scope.query = query;
        $scope.loadCashes();
    }

    $scope.onOrderChange = function () {
        $scope.loadCashes();
    }

    $scope.expandDetail = function (cash) {
        for (var i = 0; i < $scope.cashes.length; i++) {
            if (cash.CashId != $scope.cashes[i].CashId)
                $scope.cashes[i].isExpand = false;
        }
        cash.isExpand = !cash.isExpand;
    }

    $scope.getDebtType = function (cash) {
        var message = "";
        if (cash.TargetType == "ORDER") {
            message = "Phiếu bán hàng";
        }
        else if (cash.TargetType == "DELIVERY_VENDOR") {
            message = "Phiếu chuyển hàng";
        }
        else if (cash.TargetType == "EXIM") {
            if (cash.target.Type == 0) {
                message = "Phiếu nhập hàng";
            }
            else {
                message = "Phiếu xuất hàng";
            }
        }
        return message;
    }
}]);

myApp.controller('OrderController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CashController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.dateOptionsEnd = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.dateOptionsStart = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        cfpLoadingBar.start();
        var favoriteCookie = $window.sessionStorage.getItem("Order");
        if (favoriteCookie == 1) {
            var query = "";

            var year = $scope.dt.getFullYear();
            var month = $scope.dt.getMonth();
            var date = $scope.dt.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            $scope.query = query;

            $window.sessionStorage.setItem("Order", 2);
        }

        $scope.loadOrders();
    }

    $scope.openOrderDate = function () {
        $window.sessionStorage.setItem("Order", 1);
        $window.location.href = '/orders.html';
    }

    $scope.loadOrders = function () {
        $http.get("/api/app/getOrders?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.orders = data.data;
                $scope.item_count = data.metadata.Count;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadOrders();
    }

    $scope.onPageChange = function () {
        $scope.loadOrders();
    }

    $scope.onQueryChange = function () {

        var query = "";
        if ($scope.q.StartDate) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(year, month, date),
                startingDay: 1
            };
        }
        else {
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(1900, 1, 1),
                startingDay: 1
            };
        }
        if ($scope.q.EndDate) {
            var year = $scope.q.EndDate.getFullYear();
            var month = $scope.q.EndDate.getMonth();
            var date = $scope.q.EndDate.getDate();
            if (query != "") {
                query += " and CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }

            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(year, month, date),
                startingDay: 1
            };
        }
        else {
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(),
                startingDay: 1
            };
        }
        if (query == "") {
            query = "1=1";
        }
        switch ($scope.q.allowsell) {
            case '-1':

                break;
            case '0':
                query += " and DeliveryStatus == 4";
                break;
            case '1':
                query += " and DeliveryStatus == 2";
                break;
            case '2':
                query += " and DeliveryStatus == 3";
                break;
            default:
                break;
        }
        var txtSearch = $scope.q.txtSearch ? $scope.q.txtSearch : '';
        query += ' and( customer.CustomerName.Contains(\"' + txtSearch + '\") or OrderCode.Contains(\"' + txtSearch + '\"))';
        $scope.query = query;
        $scope.loadOrders();
    }

    $scope.onOrderChange = function () {
        $scope.loadOrders();
    }

    $scope.expandDetail = function (order) {
        for (var i = 0; i < $scope.orders.length; i++) {
            if (order.OrderId != $scope.orders[i].OrderId)
                $scope.orders[i].isExpand = false;
        }
        if (!order.isExpand)
            $scope.loadOrderProducts(order);
        order.isExpand = !order.isExpand;
    }

    $scope.findPaymentStatus = function (status) {
        if (status == 0)
            return "Chưa thanh toán";
        else if (status == 1)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findOrderDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else if (status == 3)
            return "Không giao được hàng";
        else
            return "Đã giao hàng"
    }

    $scope.loadOrderProducts = function (order) {
        $http.get("/api/app/getOrderProducts?id=" + order.OrderId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                order.products = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadOrderCashes = function (order, pagenumber) {
        $http.get("/api/app/getOrderCashes?id=" + order.OrderId + "&page=" + pagenumber + "&page_size=5").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                console.log(data.metadata.item_count);
                order.cashes = data.data;
                $scope.itemorder_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.updateOrder = function (order) {
        cfpLoadingBar.start();
        var baseDeliveryStatus = order.DeliveryStatus;
        order.DeliveryStatus = parseInt(order.NewDeliveryStatus);

        var post = $http({
            method: "POST",
            url: "/api/app/updateOrder",
            data: order
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.loadOrders();
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật đơn hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                order.DeliveryStatus = baseDeliveryStatus;
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật đơn hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    order.DeliveryStatus = baseDeliveryStatus;
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật đơn hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.showConfirmCancelOrder = function (order) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn hủy hóa đơn này không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.cancelOrder(order);
        }, function () {
        });
    }

    $scope.cancelOrder = function (order) {
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/cancelOrder",
            data: order
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                     .textContent('Hủy đơn hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    order.isExpand = false;
                    $scope.loadOrders();
                }
            }
            else if (data.meta.error_code == 201) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Đơn đang đã thành công, vui lòng sử dụng chức năng trả hàng')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            }
            else if (data.meta.error_code == 202) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Đơn hàng đã/đang được chuyển, vui lòng sử dụng chức năng trả hàng')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Hủy đơn hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Hủy đơn hàng thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    //cash
    $scope.openCreateCash = function (order) {
        //
        $scope.type = 1;
        $http.get("/api/app/getCashTypes?type=" + $scope.type).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.cashTypes = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        angular.element(document.querySelector('#addCashIn')).modal({ backdrop: 'static', keyboard: true });
        $scope.newCashIn = { "PaidTargetType": "-1", "CashTypeId": "-1", "TargetType": "-1" };
        $scope.isCashInOpen = true;

        $scope.newCashIn.PaidTargetType = "CUSTOMER";
        $scope.newCashIn.PaidTargetId = order.customer.CustomerId;
        $scope.newCashIn.TargetName = order.customer.CustomerName;
        $scope.newCashIn.TargetId = order.OrderId;
        $scope.newCashIn.TargetType = "ORDER";
        $scope.newCashIn.PaidAmount = (order.TotalPrice - order.CashesPaid - order.ReturnPaid) + "";
        $scope.newCashIn.Note = "Thanh toán cho khách hàng " + order.customer.CustomerName + "-" + order.customer.CustomerCode;
    }

    $scope.createCashType = function () {
        $scope.newCashType.Type = 1;
        var post = $http({
            method: "POST",
            url: "/api/app/createCashType",
            data: $scope.newCashType
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                var id = data.data.CashTypeId;
                angular.element(document.querySelector('#addCashType')).modal('toggle');
                $http.get("/api/app/getCashTypes?type=" + $scope.type).success(function (data, status, headers) {
                    if (data.meta.error_code == 200) {
                        $scope.cashTypes = data.data;
                        if ($scope.isCashInOpen)
                            $scope.newCashIn.CashTypeId = id + '';
                        else
                            $scope.newCashOut.CashTypeId = id + '';
                    }
                }).error(function (data, status, headers, config) {

                });
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mã phiếu đã được sử dụng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo loại phiếu thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.createCash = function () {

        if ($scope.newCashIn.CashTypeId <= 0 || $scope.newCashIn.CashTypeId == undefined) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Vui lòng lựa chọn loại phiếu')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            return;
        }

        if ($scope.newCashIn.PaidAmount != undefined && $scope.newCashIn.PaidAmount != '') {
            if (!isNaN(parseInt($scope.newCashIn.PaidAmount))) {
                $scope.newCashIn.PaidAmount = parseInt($scope.newCashIn.PaidAmount) ? parseInt($scope.newCashIn.PaidAmount.replace(/\./g, '')) : $scope.newCashIn.PaidAmount;
            }
            else {
                $scope.newCashIn.PaidAmount = isNaN($scope.newCashIn.PaidAmount) ? parseInt($scope.newCashIn.PaidAmount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.newCashIn.PaidAmount;
            }
        }

        if (isNaN($scope.newCashIn.PaidAmount) || $scope.newCashIn.PaidAmount <= 0) {
            $mdDialog.show(
                 $mdDialog.alert()
                                   .clickOutsideToClose(true)
                                   .title('Thông tin')
                                   .textContent('Vui lòng nhập số tiền')
                                   .ok('Đóng')
                                   .fullscreen(true)
                               );
            return;
        }

        for (var i = 0; i < $scope.orders.length; i++) {
            if ($scope.newCashIn.PaidAmount > ($scope.orders[i].TotalPrice - $scope.orders[i].CashesPaid - $scope.orders[i].ReturnPaid) && $scope.orders[i].OrderId == $scope.newCashIn.TargetId) {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Số tiền bạn thu lớn hơn công nợ của đơn hàng ' + $scope.orders[i].OrderCode + ', mời bạn điền lại số tiền!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                return;
            }
        }

        var post = $http({
            method: "POST",
            url: "/api/app/createCashFromOrder",
            data: $scope.newCashIn
        });
        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                angular.element(document.querySelector('#addCashIn')).modal('toggle');
                $scope.newCashIn = { "PaidTargetType": "CUSTOMER", "CashTypeId": "-1" };

                cfpLoadingBar.complete();
                $scope.loadOrders();
                $mdToast.show($mdToast.simple()
                   .textContent('Tạo phiếu thành công!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) {
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo loại thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }
}]);

myApp.controller('ReturnController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function ImportController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 5;
    $scope.isDone = false;

    $scope.init = function () {
        $scope.start();
        $scope.loadEmployees();
        $scope.loadBranches();
        var orderCode = $window.sessionStorage.getItem("OrderCode");
        if (orderCode != "") {
            $scope.orderCode = orderCode;
            $window.sessionStorage.setItem("OrderCode", "");
            $scope.loadOrder();
        }
    }

    $scope.openReturn = function (order) {
        $window.sessionStorage.setItem("OrderCode", order.OrderCode);
        $window.location.href = '/returns.html';
    }

    $scope.start = function () {
        cfpLoadingBar.start();
    };

    $scope.complete = function () {
        cfpLoadingBar.complete();
    }

    $scope.loadEmployees = function () {
        $http.get("/api/app/getEmployees?page=1&page_size=100&query=1=1").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.employees = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.branches = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadOrder = function () {
        $http.get("/api/app/getReturnOrder?code=" + $scope.orderCode).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.order = data.data;
                $scope.order.PaymentType = '0';
                $scope.order.employee.EmployeeId = $scope.order.employee.EmployeeId + "";
                $scope.order.branch.BranchId = $scope.order.branch.BranchId + "";
                $scope.order.fee = 0;
                $scope.order.TotalPriceOld = $scope.order.TotalPrice;
                //init product
                $scope.order.TotalPrice = 0;
                for (var i = 0; i < $scope.order.products.length; i++) {
                    $scope.order.products[i].MaxQuantity = angular.copy($scope.order.products[i].Quantity);
                    $scope.order.TotalPrice += $scope.order.products[i].MaxQuantity * $scope.order.products[i].Price;
                    //console.log($scope.order.products[i]);
                }
                $scope.order.paid = ($scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee) < 0 ? 0 : $scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee;
            }
            else {
                $mdToast.show($mdToast.simple()
                   .textContent('Không tìm thấy đơn hàng!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.onPriceChanged = function (product, k) {
        if (k == 1) {
            product.Price = parseInt(product.Price) ? parseInt(product.Price.replace(/\./g, '')) : product.Price;
        }
        product.Price = isNaN(product.Price) ? parseInt(product.Price.replace(/[^\d|\-+|\.+]/g, '')) : product.Price;
        product.Quantity = isNaN(product.Quantity) ? parseInt(product.Quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.Quantity;
        product.total = product.Price * product.Quantity;
        $scope.order.TotalPrice = 0;
        //total price
        for (var i = 0; i < $scope.order.products.length; i++) {
            $scope.order.TotalPrice += $scope.order.products[i].total;
        }

        $scope.order.paid = ($scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee) < 0 ? 0 : $scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee;
    }

    $scope.onPriceChangedOrder = function (order) {
        //total price
        $scope.order.TotalPrice = 0;
        for (var i = 0; i < $scope.order.products.length; i++) {
            $scope.order.TotalPrice += $scope.order.products[i].total;
        }
        $scope.order.fee = parseInt($scope.order.fee) ? parseInt($scope.order.fee.replace(/\./g, '')) : $scope.order.fee;
        $scope.order.paid = ($scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee) < 0 ? 0 : $scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee;
    }

    $scope.returnOrder = function () {
        var checklength = true;
        $scope.order.products.forEach(function (productlo) {
            if (productlo.MaxQuantity < productlo.Quantity) {
                checklength = false;
                return;
            }
        });

        if (!checklength) {
            $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Số hàng trả lớn hơn đơn hàng ! vui lòng kiểm tra lại')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            return
        }
        if (!$scope.frmReturn.$valid) {
            $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Vui lòng nhập đủ thông tin!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );

            return;
        }

        $scope.order.paid = $scope.order.paid + "";
        if ($scope.order.paid != undefined && $scope.order.paid != '') {
            if (!isNaN(parseInt($scope.order.paid))) {
                $scope.order.paid = parseInt($scope.order.paid) ? parseInt($scope.order.paid.replace(/\./g, '')) : $scope.order.paid;
            }
            else {
                $scope.order.paid = isNaN($scope.order.paid) ? parseInt($scope.order.paid.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.paid;
            }
        }

        $scope.order.paids = ($scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee) < 0 ? 0 : ($scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee);
        if ($scope.order.paid > $scope.order.paids) {
            $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Số tiền trả khách lớn hơn tổng tiền cần phải trả! Yêu cầu nhập đúng số tiền trả khách!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );

            return;
        }

        $scope.order.fee = isNaN($scope.order.fee) ? parseInt($scope.order.fee.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.fee;
        $scope.order.TotalPaid = $scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee < 0 ? 0 : $scope.order.TotalPrice - ($scope.order.TotalPriceOld - $scope.order.CustomerPaid) - $scope.order.fee;
        $scope.order.TotalCustomerCashes = $scope.order.TotalPriceOld - $scope.order.CustomerPaid;

        $scope.order.PaymentType = parseFloat($scope.order.PaymentType);
        $scope.order.EmployeeId = parseInt($scope.order.employee.EmployeeId);
        $scope.order.CustomerId = parseInt($scope.order.customer.CustomerId);
        $scope.order.BranchId = parseInt($scope.order.branch.BranchId);

        var post = $http({
            method: "POST",
            url: "/api/app/returnOrder",
            data: $scope.order
        });

        post.success(function successCallback(data, status, headers, config) {
            // success           
            $scope.isDone = true;
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    //success
                    var confirm = $mdDialog.confirm()
                      .title('Thông báo')
                      .textContent('Trả hàng thành công, vui lòng kiểm tra lại trong mục quản lý nhập / xuất hàng!')
                      .ok('Làm mới!')
                      .cancel('Về trang quản lý');

                    $mdDialog.show(confirm).then(function () {
                        $window.location.href = '/returns.html';
                    }, function () {
                        $window.location.href = '/customer-returns.html';
                    });
                }
            }
            else if (data.meta.error_code == 212) {
                //success
                $scope.order.PaymentType = $scope.order.PaymentType + '';
                $scope.order.EmployeeId = $scope.order.employee.EmployeeId + '';
                $scope.order.CustomerId = $scope.order.customer.CustomerId + '';
                $scope.order.BranchId = $scope.order.branch.BranchId + '';
                $mdDialog.show(
                   $mdDialog.alert()
                     .clickOutsideToClose(true)
                     .title('Thông tin')
                     .textContent('Đơn hàng chưa thành công, vui lòng chọn đơn hàng khác!')
                     .ok('Đóng')
                     .fullscreen(true)
                 );
            }
            else {
                $scope.order.PaymentType = $scope.order.PaymentType + '';
                $scope.order.EmployeeId = $scope.order.employee.EmployeeId + '';
                $scope.order.CustomerId = $scope.order.customer.CustomerId + '';
                $scope.order.BranchId = $scope.order.branch.BranchId + '';
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Trả hàng thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
        })
        .error(function (data, status, headers, config) {
            $scope.order.PaymentType = $scope.order.PaymentType + '';
            $scope.order.EmployeeId = $scope.order.employee.EmployeeId + '';
            $scope.order.CustomerId = $scope.order.customer.CustomerId + '';
            $scope.order.BranchId = $scope.order.branch.BranchId + '';
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Trả hàng thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }
}]);

myApp.controller('ImportReturnController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function ImportReturnController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 5;
    $scope.isDone = false;

    $scope.init = function () {
        $scope.start();
        $scope.loadEmployees();
        $scope.loadBranches();
        if ($scope.EximCode != undefined) {
            $scope.loadExim();
        }
    }

    $scope.start = function () {
        cfpLoadingBar.start();
    };

    $scope.complete = function () {
        cfpLoadingBar.complete();
    }

    $scope.loadEmployees = function () {
        $http.get("/api/app/getEmployees?page=1&page_size=100&query=1=1").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.employees = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.branches = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadExim = function () {
        $http.get("/api/app/getReturnImport?code=" + $scope.EximCode).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.return = data.data;
                $scope.return.PaidAmount = 0;
                $scope.return.EmployeeId = $scope.EmployeeId + '';
                $scope.return.BranchId = $scope.BranchId + '';
                $scope.return.PaymentType = '0';
                $scope.return.fee = 0;
                $scope.return.TotalPrice = 0;
                //cal product total price
                for (var i = 0; i < $scope.return.products.length; i++) {
                    $scope.return.TotalPrice += $scope.return.products[i].Price * $scope.return.products[i].Quantity;
                }
                $scope.return.ReturnAmount = $scope.return.TotalPrice;
            }
            else if (data.meta.error_code == 200) {
                $mdToast.show($mdToast.simple()
                   .textContent('Không tìm thấy hóa đơn nhập!')
                   .position('fixed bottom right')
                   .hideDelay(3000));
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.onPriceChanged = function (product, k) {
        if (k == 1) {
            if (product.ReturnPrice != undefined && product.ReturnPrice != '') {
                product.ReturnPrice = parseInt(product.ReturnPrice) ? parseInt(product.ReturnPrice.replace(/\./g, '')) : product.ReturnPrice;
                product.ReturnPrice = isNaN(product.ReturnPrice) ? parseInt(product.ReturnPrice.replace(/[^\d|\-+|\.+]/g, '')) : product.ReturnPrice;
            }
        }
        product.Quantity = isNaN(product.Quantity) ? parseInt(product.Quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.Quantity;
        product.total = product.ReturnPrice * product.Quantity;
        $scope.return.TotalPrice = 0;
        //total price
        for (var i = 0; i < $scope.return.products.length; i++) {
            $scope.return.TotalPrice += $scope.return.products[i].total;
        }
        $scope.return.ReturnAmount = $scope.return.TotalPrice;
    }

    $scope.onPaidAmountPriceChanged = function () {
        //if (k == 1) {
        if ($scope.return.PaidAmount != undefined && $scope.return.PaidAmount != '') {
            //if (!isNaN(parseInt($scope.return.PaidAmount))) {
            $scope.return.PaidAmount = parseInt($scope.return.PaidAmount) ? parseInt($scope.return.PaidAmount.replace(/\./g, '')) : $scope.return.PaidAmount;
            //}
            //else {
            $scope.return.PaidAmount = isNaN($scope.return.PaidAmount) ? parseInt($scope.return.PaidAmount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.return.PaidAmount;
            //}
        }
    }

    $scope.returnImport = function () {
        //check status
        if ($scope.return.Status == 10) {
            $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Phiếu nhập hiện tại là phiếu tạm, vui lòng chọn phiếu nhập khác!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
        }
        var returnImport = {};
        console.log("TH:" + $scope.return.PaidAmount);
        if ($scope.return.PaidAmount != undefined && $scope.return.PaidAmount != '') {
            //if (!isNaN(parseInt($scope.return.PaidAmount))) {
            //returnImport.Paid = parseInt($scope.return.PaidAmount) ? parseInt($scope.return.PaidAmount.replace(/\./g, '')) : $scope.return.PaidAmount;
            //}
            //else {
            returnImport.Paid = isNaN($scope.return.PaidAmount) ? parseInt($scope.return.PaidAmount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.return.PaidAmount;
            //}
        }
        else {
            returnImport.Paid = 0;
        }


        returnImport.PaymentMethod = parseInt($scope.return.PaymentType);
        returnImport.BranchId = parseInt($scope.return.BranchId);
        returnImport.EximId = parseInt($scope.return.EximId);
        returnImport.VendorName = parseInt($scope.return.target.TargetName);
        returnImport.EmployeeId = parseInt($scope.return.EmployeeId);
        returnImport.Note = $scope.return.Note;
        //product
        var products = new Array();
        for (var i = 0; i < $scope.return.products.length; i++) {
            var product = {};
            product.ProductAttributeId = $scope.return.products[i].ProductAttributeId;
            product.ReturnPrice = $scope.return.products[i].ReturnPrice;
            product.Quantity = $scope.return.products[i].Quantity;
            products.push(product);
        }
        returnImport.products = products;
        var post = $http({
            method: "POST",
            url: "/api/app/returnImport",
            data: returnImport
        });

        post.success(function successCallback(data, status, headers, config) {
            // success           
            $scope.isDone = true;
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    //success
                    var confirm = $mdDialog.confirm()
                      .title('Thông báo')
                      .textContent('Trả hàng nhập thành công, vui lòng kiểm tra lại trong mục quản lý trả hàng nhập!')
                      .ok('Về trang quản lý nhập hàng')
                      .cancel('Về trang quản lý trả hàng nhập');

                    $mdDialog.show(confirm).then(function () {
                        $window.location.href = '/exims.html';
                    }, function () {
                        $window.location.href = '/vendor-returns.html';
                    });
                }
            }
            else if (data.meta.error_code == 211) {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Bạn chưa nhập đủ dữ liệu!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
            else if (data.meta.error_code == 212) {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Kho hàng bạn chọn không đủ hàng để trả nhà cung cấp!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
        })
                .error(function (data, status, headers, config) { // optional                   
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Trả hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }
}]);

myApp.controller('CustomerReturnController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CustomerReturnController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptionsStart = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.dateOptionsEnd = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };
    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        cfpLoadingBar.start();
        var favoriteCookie = $window.sessionStorage.getItem("CustomerReturn");
        if (favoriteCookie == 1) {
            var query = "";

            var year = $scope.dt.getFullYear();
            var month = $scope.dt.getMonth();
            var date = $scope.dt.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            $scope.query = query;

            $window.sessionStorage.setItem("CustomerReturn", 2);
        }
        $scope.loadReturns();
    }

    $scope.openCustomerReturnDate = function () {
        $window.sessionStorage.setItem("CustomerReturn", 1);
        $window.location.href = '/customer-returns.html';
    }

    $scope.loadReturns = function () {
        $http.get("/api/app/getReturns?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.returns = data.data;
                $scope.item_count = data.metadata.item_count;
                console.log($scope.returns);
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadReturns();
    }

    $scope.onPageChange = function () {
        $scope.loadReturns();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            } $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(year, month, date),
                startingDay: 1
            };
        }
        else {
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(1900, 1, 1),
                startingDay: 1
            };
        }
        if ($scope.q.EndDate != undefined) {
            var year = $scope.q.EndDate.getFullYear();
            var month = $scope.q.EndDate.getMonth();
            var date = $scope.q.EndDate.getDate();
            if (query != "") {
                query += " and CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(year, month, date),
                startingDay: 1
            };
        }

        else {
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(),
                startingDay: 1
            };
        }
        if (query == "") {
            query = "1=1";
        }

        $scope.query = query;
        $scope.loadReturns();
    }

    $scope.onOrderChange = function () {
        $scope.loadReturns();
    }

    $scope.expandDetail = function (_return) {
        for (var i = 0; i < $scope.returns.length; i++) {
            if (_return.ReturnId != $scope.returns[i].ReturnId)
                $scope.returns[i].isExpand = false;
        }
        if (!_return.isExpand)
            $scope.loadReturnProducts(_return);
        _return.isExpand = !_return.isExpand;
    }

    $scope.findPaymentStatus = function (status) {
        if (status == 0)
            return "Chưa thanh toán";
        else if (status == 1)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.loadReturnProducts = function (_return) {
        $http.get("/api/app/getReturnProducts?id=" + _return.ReturnId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                _return.products = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }
    $scope.loadCashes = function (_return) {
        $http.get("/api/app/getReturnCashes?id=" + _return.ReturnId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                _return.cashes = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.updateReturn = function (_return) {
        cfpLoadingBar.start();

        var post = $http({
            method: "POST",
            url: "/api/app/updateReturn",
            data: _return
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật đơn hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật đơn hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Cập nhật đơn hàng thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }
}]);

myApp.controller('VendorReturnController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CustomerReturnController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptionsStart = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.dateOptionsEnd = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };
    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        cfpLoadingBar.start();
        $scope.loadReturns();
    }

    $scope.loadReturns = function () {
        $http.get("/api/app/getImportReturns?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.returns = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $http.get("/api/app/getImportReturns?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.returns = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageChange = function () {
        $http.get("/api/app/getImportReturns?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.returns = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.StartDate) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            if (query != "") {
                query += " and CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
            }
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(year, month, date),
                startingDay: 1
            };
        }
        else {
            $scope.dateOptionsEnd = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(1900, 1, 1),
                startingDay: 1
            };
        }
        if ($scope.q.EndDate) {
            var year = $scope.q.EndDate.getFullYear();
            var month = $scope.q.EndDate.getMonth();
            var date = $scope.q.EndDate.getDate();
            if (query != "") {
                query += " and CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                minDate: new Date(1900, 1, 1),
                maxDate: new Date(year, month, date),
                startingDay: 1
            };
        }
        else {
            $scope.dateOptionsStart = {
                formatYear: 'yyyy',
                maxDate: new Date(),
                minDate: new Date(1900, 1, 1),
                startingDay: 1
            };

        }
        if (query == "")
            query = "1=1";
        $scope.query = query;
        $scope.loadReturns();
    }

    $scope.onOrderChange = function () {
        $scope.loadReturns();
    }

    $scope.expandDetail = function (_return) {
        console.log($scope.returns);
        console.log(_return);
        for (var i = 0; i < $scope.returns.length; i++) {
            if (_return.EximId != $scope.returns[i].EximId)
                $scope.returns[i].isExpand = false;
        }
        if (!_return.isExpand)
            $scope.loadReturnProducts(_return);
        _return.isExpand = !_return.isExpand;
    }

    $scope.loadReturnProducts = function (_return) {
        $http.get("/api/app/getImportReturnProducts?id=" + _return.EximId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                _return.products = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadCashes = function (_return) {
        $http.get("/api/app/getImportReturnCashes?id=" + _return.EximId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                _return.cashes = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.findPaymentStatus = function (status) {
        if (status == 0)
            return "Chưa thanh toán";
        else if (status == 1)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }
}]);

myApp.controller('EmployeeController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function EmployeeController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.isEditing = false;

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.getEmployees();
        $scope.getBranches();
    }
    $scope.newEmp = {};

    $scope.getEmployees = function () {
        $http.get("/api/app/getShopEmployees").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if ($scope.page == 1) {
                    $scope.employees = data.data;
                }
                else {
                    $scope.employees = $scope.employees.concat(data.data);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.getBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if ($scope.page == 1) {
                    $scope.branches = data.data;
                }
                else {
                    $scope.branches = $scope.branches.concat(data.data);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.openCreateEmployeeDialog = function () {
        $scope.isEditing = false;
        angular.element(document.querySelector('#addEmployee')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addEmployee')).modal('toggle');
        $scope.newEmp = {};
        $scope.newEmp.Email = '';
        $scope.newEmp.Phone = '';
    }

    $scope.openEditEmployeeDialog = function (employee) {
        //
        $scope.isEditing = true;
        $scope.editEmployee = angular.copy(employee);
        $scope.originEmployeeCustomer = angular.copy(employee);
        $scope.editEmployee.BranchId = $scope.editEmployee.branch.BranchId + "";
        angular.element(document.querySelector('#editEmployee')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.createEmployee = function () {
        if (!$scope.frmAddEmployee.$valid) {
            return;
        }

        cfpLoadingBar.start();
        $scope.newEmp.BranchId = parseInt($scope.newEmp.BranchId);
        if ($scope.newEmp.BranchId == -1) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Bạn chưa chọn chi nhánh!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
        }
        else {

            var post = $http({
                method: "POST",
                url: "/api/app/createEmployee",
                data: $scope.newEmp
            });

            post.success(function successCallback(data, status, headers, config) {
                // success                
                cfpLoadingBar.complete();
                if (data.meta.error_code == 200) {
                    if (data.meta.error_code == 200) {
                        angular.element(document.querySelector('#addEmployee')).modal('toggle')
                        $scope.newEmp = { "BranchId": "-1", "Group": "-1" };
                        $mdToast.show($mdToast.simple()
                         .textContent('Thêm nhân viên thành công!')
                         .position('fixed bottom right')
                         .hideDelay(3000));
                        $scope.newEmp = {};
                        $scope.getEmployees();
                    }
                }
                else if (data.meta.error_code == 211) {
                    $mdDialog.show(
                          $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Thông tin')
                            .textContent('Email đã được sử dụng, vui lòng chọn email khác!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
                }
                else {
                    $mdDialog.show(
                          $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Thông tin')
                            .textContent('Tạo nhân viên thất bại, vui lòng thử lại sau!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
                }
            })
                    .error(function (data, status, headers, config) { // optional
                        cfpLoadingBar.complete();
                        $mdDialog.show(
                          $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Thông tin')
                            .textContent('Tạo nhân viên thất bại, vui lòng thử lại sau!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
                    });
        }
    }

    $scope.expandDetail = function (employee) {
        for (var i = 0; i < $scope.employees.length; i++) {
            if (employee.EmployeeId != $scope.employees[i].EmployeeId)
                $scope.employees[i].isExpand = false;
        }
        employee.isExpand = !employee.isExpand;
    }

    $scope.cancelEdit = function () {
        $scope.editCustomer = $scope.originEditCustomer;
    }

    $scope.updateEmployee = function () {
        if (!$scope.frmEditEmployee.$valid) {
            return;
        }

        cfpLoadingBar.start();
        $scope.editEmployee.BranchId = parseInt($scope.editEmployee.BranchId);

        var post = $http({
            method: "POST",
            url: "/api/app/updateEmployee",
            data: $scope.editEmployee
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editEmployee')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật nhân viên thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.editCustomer = {};
                    $scope.getEmployees();
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật nhân viên thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật nhân viên thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.showConfirmDeleteEmployee = function (employee) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa nhân viên ' + employee.EmployeeName + ' hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteEmployee(employee);
        }, function () {
        });
    }

    $scope.deleteEmployee = function (employee) {
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/deleteEmployee",
            data: employee
        });

        post.success(function successCallback(data, status, headers, config) {
            // success         
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.getEmployees();
                    $mdToast.show($mdToast.simple()
                     .textContent('Xóa nhân viên thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                       $mdDialog.alert()
                         .clickOutsideToClose(true)
                         .title('Thông tin')
                         .textContent('Xóa nhân viên thất bại, vui lòng thử lại sau!')
                         .ok('Đóng')
                         .fullscreen(true)
                     );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa nhân viên thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }
}]);

myApp.controller('ActionController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', '$filter', function ActionController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast, $filter) {

    $scope.init = function () {
        //init   
        $scope.getActions();
    }

    $scope.getActions = function () {
        $http.get("/api/app/getActions").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                console.log(data);
                $scope.actions = data.data;
            }
        }).error(function (data, status, headers, config) {
        });
    }

    $scope.findClass = function (action) {
        var c = "";
        if (action.TargetType == "ORDER")
            c = "hoadon";
        else if (action.TargetType == "IMPORT")
            c = "inport"
        else if (action.TargetType == "EXPORT")
            c = "export";
        else
            c = "edit";
        return c;
    }

    $scope.findIconClass = function (action) {
        var c = "";
        if (action.TargetType == "ORDER")
            c = "fa fa-file-text-o";
        else if (action.TargetType == "IMPORT")
            c = "fa fa-file-text-o"
        else if (action.TargetType == "EXPORT")
            c = "fa fa-file-text-o";
        else
            c = "fa fa-pencil-square-o";
        return c;
    }

    $scope.findTitle = function (action) {
        var title = "";
        if (action.TargetType == "ORDER")
            title = "HÓA ĐƠN"
        else if (action.TargetType == "IMPORT")
            title = "NHẬP HÀNG";
        else if (action.TargetType == "EXPORT")
            title = "XUẤT HÀNG";
        else if (action.TargetType == "PRODUCT" || action.TargetType == "PRODUCT_ATTRIBUTE")
            title = "SẢN PHẨM";
        else if (action.TargetType == "CUSTOMER")
            title = "KHÁCH HÀNG";
        else if (action.TargetType == "VENDOR")
            title = "NHÀ CUNG CẤP";
        else if (action.TargetType == "RETURN")
            title = "TRẢ HÀNG";
        else if (action.TargetType == "DELIVERY_VENDOR")
            title = "NHÀ CUNG CẤP VẬN CHUYỂN";
        else if (action.TargetType == "IMPORT_RETURN")
            title = "TRẢ HÀNG NHẬP";
        return title;
    }
    $scope.findMessage = function (action) {
        var a = "";
        if (action.target.Price != undefined && action.target.Price > 0)
            a = "với giá trị " + $filter('currency')(action.target.Price, "", 0) + "đ";
        else
            a = action.target.TargetCode;
        return a;
    }

    $scope.PagerService = function () {
        // service definition
        var service = {};

        service.GetPager = GetPager;

        return service;

        // service implementation
        function GetPager(totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;

            // default page size is 10
            pageSize = pageSize || 10;

            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            var pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }
    }
}]);

myApp.controller('ChartController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', function ChartController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $filter, $mdDialog, $mdToast) {

    $scope.query = "1=1";
    $scope.q = {};

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            $scope.dt = $scope.q.StartDate;
            if (query != "") {
                query += " AND CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }
        $scope.query = query;

        cfpLoadingBar.start();
        $scope.getStats();

    }

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.chart = {};
    $scope.chart.options = {
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return datasetLabel
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return value
                    }
                }
            }]
        }
    }

    $scope.init = function () {
        //init   
        var query = "";
        var year = $scope.dt.getFullYear();
        var month = $scope.dt.getMonth();
        var date = $scope.dt.getDate();

        if (query != "") {
            query += " AND CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
        }
        else {
            query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
        }
        $scope.query = query;
        $scope.getStats();
    }

    $scope.getStats = function () {
        $http.get("/api/app/getTodayStats?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    $scope.chart.labels.push($scope.stats[i].ProductName + "\n" + $scope.stats[i].SubName);
                    $scope.chart.data.push($scope.stats[i].Quantity);
                }
            }
        }).error(function (data, status, headers, config) {
        });
    }
}]);

myApp.controller('SalesController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', '$sce', function SalesController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $filter, $mdDialog, $mdToast, $sce) {

    $scope.page = 1;
    $scope.page_size = 14;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.selectedProducts = new Array();
    $scope.order = { "Total": 0 };

    $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        //var EmployeeId = angular.element(document.querySelector('#EmployeeId'));
        //var BranchId2 = angular.element(document.querySelector('#BranchId'));
        //$scope.order.EmployeeId = EmployeeId.val(); 
        //$scope.order.BranchId2 = BranchId2.val();
        //console.log($scope.order.EmployeeId +"-"+$scope.order.BranchId2);
        $scope.loadProducts();
        $scope.loadCategories();
        $scope.loadEmployees();
        $scope.loadBranches();
        $scope.loadVendors();
        $scope.loadProvinces();
        $scope.order.Notification = "Còn thiếu: ";
        $scope.order.classActive = 'active';
    }

    $scope.loadProducts = function () {
        $http.get("/api/app/getProducts?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.Count;
                $scope.products = data.data;
                //calculate stock
                for (var i = 0; i < $scope.products.length; i++) {
                    var count = 0;
                    var tooltip = "Danh sách hàng tồn kho tại các chi nhánh: \n ";
                    if ($scope.products[i].stock != null) {
                        for (var j = 0; j < $scope.products[i].stock.length; j++) {
                            if ($scope.products[i].stock[j].BranchId == $scope.order.BranchId2) {
                                $scope.products[i].countStock = $scope.products[i].stock[j].Stock;
                            }
                            count += $scope.products[i].stock[j].Stock;
                            tooltip += $scope.products[i].stock[j].BranchName + " : " + $scope.products[i].stock[j].Stock + " \n ";
                        }
                    }

                    $scope.products[i].stock_count = count;
                    $scope.products[i].tooltip = $sce.trustAsHtml(tooltip);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageChange = function () {
        $http.get("/api/app/getProducts?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.item_count = data.metadata.item_count;
                $scope.products = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.category != '-1') {
            query = "category.CategoryId = " + $scope.q.category;
        }
        else {
            query = "1=1";
        }

        if ($scope.q.txtSearch != '' && $scope.q.txtSearch != undefined) {
            var txtSearch = myApp.formatSpecialchar($scope.q.txtSearch);
            query += ' and SearchQuery.Contains(\"' + txtSearch + '\")';
        }

        $scope.query = query;
        $scope.loadProducts();
    }

    $scope.loadCategories = function () {
        $http.get("/api/app/getCategories").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.categories = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.getFirstImage = function (images) {
        if (images != undefined && images.length > 0)
            return images[0].Path;
        else
            return "";
    }

    $scope.selectBranch = function () {
        for (var i = 0; i < $scope.products.length; i++) {
            var count = 0;
            if ($scope.products[i].stock != null) {
                for (var j = 0; j < $scope.products[i].stock.length; j++) {
                    if ($scope.products[i].stock[j].BranchId == $scope.order.BranchId) {
                        $scope.products[i].countStock = $scope.products[i].stock[j].Stock;
                    }
                    count += $scope.products[i].stock[j].Stock;
                }
            }

            for (var k = 0; k < $scope.selectedProducts.length; k++) {
                if ($scope.selectedProducts[k].ProductAttributeId == $scope.products[i].ProductAttributeId) {
                    //isSelected = true;
                    if ($scope.selectedProducts[k].Quantity > $scope.products[i].countStock)
                        $scope.selectedProducts[k].isInvalidQuantity = true;
                    else
                        $scope.selectedProducts[k].isInvalidQuantity = false;
                    break;
                }
            }

            $scope.products[i].stock_count = count;
        }
    }

    $scope.findSalePrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            var index = -1;
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            price = productPrice[index].SalePrice;
        }
        return price;
    }

    $scope.findOriginPrice = function (productPrice) {
        var price = 0;
        if (productPrice != null) {
            var index = -1;
            for (var i = 0; i < productPrice.length; i++) {
                if (productPrice[i].Quantity == 1)
                    index = i;
            }
            price = productPrice[index].OriginPrice;
        }
        return price;
    }

    $scope.selectProduct = function (product) {
        //check exist
        var isSelected = false;
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].ProductAttributeId == product.ProductAttributeId) {
                isSelected = true;
                break;
            }
        }
        if (!isSelected) {
            product.Quantity = 1;
            if (product.Quantity > product.countStock)
                product.isInvalidQuantity = true;
            else
                product.isInvalidQuantity = false;
            $scope.selectedProducts.push(product);
        }
    }

    $scope.goPrevPage = function () {
        if ($scope.page <= 1)
            return;
        $scope.page -= 1;
        $scope.loadProducts();
    }

    $scope.goNextPage = function () {
        if ($scope.page * $scope.page_size >= $scope.item_count)
            return;
        $scope.page += 1;
        $scope.loadProducts();
    }

    $scope.loadEmployees = function () {
        $http.get("/api/app/getEmployees?page=1&page_size=100&query=1=1").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.employees = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.branches = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadVendors = function () {
        $http.get("/api/app/getDeliveryVendors?page=" + 1 + "&page_size=" + 100 + "&query=1=1&order_by=").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.vendors = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.removeSelectedProduct = function (product) {
        console.log(product);
        var index = -1;
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].ProductAttributeId == product.ProductAttributeId) {
                index = i;
                break;
            }
        }
        $scope.selectedProducts.splice(index, 1);
        $scope.calculateOrderPrice();
    }

    $scope.findBaseUnit = function (product) {
        if (product.prices != null) {
            var index = -1;
            for (var i = 0; i < product.prices.length; i++) {
                if (product.prices[i].Quantity == 1)
                    index = i;
            }
            product.ProductPriceId = product.prices[index].ProductPriceId + '';
            product.ProductUnitId = product.prices[index].ProductUnitId + '';
            product.SelectedSalePrice = product.prices[index].SalePrice;
            product.SelectedOriginPrice = product.prices[index].OriginPrice;
            product.Total = product.SelectedSalePrice;
            product.TotalOrigin = product.SelectedOriginPrice;
            $scope.calculateOrderPrice();
        }
    }

    $scope.onUnitChange = function (product) {
        //find index
        product.Quantity = '1';
        //find
        if (product.prices != null) {
            var index = -1;
            for (var i = 0; i < product.prices.length; i++) {
                if (product.prices[i].ProductPriceId == product.ProductPriceId)
                    index = i;
            }
            product.ProductPriceId = product.prices[index].ProductPriceId + '';
            product.ProductUnitId = product.prices[index].ProductUnitId + '';
            product.SelectedSalePrice = product.prices[index].SalePrice;
            product.SelectedOriginPrice = product.prices[index].OriginPrice;
            //recalculate
            $scope.onQuantityChange(product);
        }
    }

    $scope.onQuantityChangeMinus = function (product) {
        product.Quantity = isNaN(product.Quantity) ? parseInt(product.Quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.Quantity;
        product.Quantity = product.Quantity - 1;
        if (product.Quantity < 1) product.Quantity = 1;
        product.SelectedSalePrice = isNaN(product.SelectedSalePrice) ? parseInt(product.SelectedSalePrice.replace(/[^\d|\-+|\.+]/g, '')) : product.SelectedSalePrice;
        product.Total = product.SelectedSalePrice * product.Quantity;
        $scope.calculateOrderPrice();
        //check valid quantity        
        if (product.Quantity > product.countStock)
            product.isInvalidQuantity = true;
        else
            product.isInvalidQuantity = false;
    }

    $scope.onQuantityChange = function (product, k) {
        if (k == 1) {
            if (product.SelectedSalePrice != undefined && product.SelectedSalePrice != '') {
                if (!isNaN(parseInt(product.SelectedSalePrice))) {
                    product.SelectedSalePrice = parseInt(product.SelectedSalePrice) ? parseInt(product.SelectedSalePrice.replace(/\./g, '')) : product.SelectedSalePrice;
                }
                else {
                    product.SelectedSalePrice = isNaN(product.SelectedSalePrice) ? parseInt(product.SelectedSalePrice.replace(/[^\d|\-+|\.+]/g, '')) : product.SelectedSalePrice;
                }
            }
        }
        product.Quantity = isNaN(product.Quantity) ? parseInt(product.Quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.Quantity;
        product.SelectedSalePrice = isNaN(product.SelectedSalePrice) ? parseInt(product.SelectedSalePrice.replace(/[^\d|\-+|\.+]/g, '')) : product.SelectedSalePrice;
        product.Total = product.SelectedSalePrice * product.Quantity;
        $scope.calculateOrderPrice(4);
        //check valid quantity        
        if (product.Quantity > product.countStock || product.Quantity < 1 || product.Quantity == undefined || product.Quantity == '')
            product.isInvalidQuantity = true;
        else
            product.isInvalidQuantity = false;
    }

    $scope.onQuantityChangePlus = function (product) {
        product.Quantity = isNaN(product.Quantity) ? parseInt(product.Quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.Quantity;
        console.log(product.Quantity);
        product.Quantity = parseInt(product.Quantity) + 1;
        console.log(product.Quantity);
        product.SelectedSalePrice = isNaN(product.SelectedSalePrice) ? parseInt(product.SelectedSalePrice.replace(/[^\d|\-+|\.+]/g, '')) : product.SelectedSalePrice;
        product.Total = product.SelectedSalePrice * product.Quantity;
        $scope.calculateOrderPrice();
        //check valid quantity        
        if (product.Quantity > product.countStock)
            product.isInvalidQuantity = true;
        else
            product.isInvalidQuantity = false;
    }

    $scope.calculateOrderPrice = function (k) {
        var total = 0;
        var totalOrigin = 0;
        var Discountcheck = $scope.order.DiscountView.toString();
        console.log(Discountcheck.replace('.', '') + '--' + $scope.order.Total);
        if ($scope.order.discountType == 0 && (parseInt(Discountcheck.replace('.', '')) > parseInt($scope.order.Total))) {
            $scope.order.DiscountView = $scope.order.Total;
        }
        else if ($scope.order.discountType == 1 && (parseInt(Discountcheck.replace('.')) > 100)) {
            $scope.order.DiscountView = 100;
        }
        $scope.order.DiscountView = $scope.order.DiscountView + '';
        if ($scope.selectedProducts.length > 0) {
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                total += $scope.selectedProducts[i].Total;
            }
        }
        $scope.order.Total = total;

        if (k == 1) {
            if ($scope.order.DiscountView != undefined && $scope.order.DiscountView != '') {
                if (!isNaN(parseInt($scope.order.DiscountView))) {
                    $scope.order.DiscountView = parseInt($scope.order.DiscountView) ? parseInt($scope.order.DiscountView.replace(/\./g, '')) : $scope.order.DiscountView;
                }
                else {
                    $scope.order.DiscountView = isNaN($scope.order.DiscountView) ? parseInt($scope.order.DiscountView.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.DiscountView;
                }
            }
        }

        if (k == 2) {
            if ($scope.order.DeliveryPriceView != undefined && $scope.order.DeliveryPriceView != '') {
                if (!isNaN(parseInt($scope.order.DeliveryPriceView))) {
                    $scope.order.DeliveryPriceView = parseInt($scope.order.DeliveryPriceView) ? parseInt($scope.order.DeliveryPriceView.replace(/\./g, '')) : $scope.order.DeliveryPriceView;
                }
                else {
                    $scope.order.DeliveryPriceView = isNaN($scope.order.DeliveryPriceView) ? parseInt($scope.order.DeliveryPriceView.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.DeliveryPriceView;
                }
            }
        }

        if (isNaN(parseInt($scope.order.DeliveryPriceView))) {
            $scope.order.DeliveryPriceView = '';
            $scope.order.DeliveryPrice = 0;
        }
        else {
            $scope.order.DeliveryPriceView = isNaN($scope.order.DeliveryPriceView) ? parseInt($scope.order.DeliveryPriceView.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.DeliveryPriceView;
            $scope.order.DeliveryPrice = $scope.order.DeliveryPriceView;
        }

        if (k == 3) {
            if ($scope.order.CustomerPaid != undefined && $scope.order.CustomerPaid != '') {
                if (!isNaN(parseInt($scope.order.CustomerPaid))) {
                    $scope.order.CustomerPaid = parseInt($scope.order.CustomerPaid) ? parseInt($scope.order.CustomerPaid.replace(/\./g, '')) : $scope.order.CustomerPaid;
                }
                else {
                    $scope.order.CustomerPaid = isNaN($scope.order.CustomerPaid) ? parseInt($scope.order.CustomerPaid.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.CustomerPaid;
                }
            }
        }

        if ($scope.order.discountType == 0) {
            $scope.order.TotalPrice = total - $scope.order.DiscountView + parseInt($scope.order.DeliveryPrice);
            $scope.order.Remain = total - $scope.order.DiscountView + parseInt($scope.order.DeliveryPrice) - $scope.order.CustomerPaid;
            $scope.order.Discount = $scope.order.DiscountView;
        }
        else {
            $scope.order.TotalPrice = total - ($scope.order.DiscountView * total * 0.01) + parseInt($scope.order.DeliveryPrice);
            $scope.order.Remain = total - ($scope.order.DiscountView * total * 0.01) + parseInt($scope.order.DeliveryPrice) - $scope.order.CustomerPaid;
            $scope.order.Discount = $scope.order.DiscountView * total * 0.01;
        }

        if ($scope.order.needCollect) {
            $scope.order.MoneyCollect = $scope.order.Remain;
            $scope.order.Remain = 0;
        }
        else {
            $scope.order.MoneyCollect = 0;
        }

        if ($scope.order.Remain < 0) {
            $scope.order.Notification = "Còn thừa:";
            $scope.order.Remain = Math.abs($scope.order.Remain);
        }
        else {
            $scope.order.Notification = "Còn thiếu:";
        }


    }

    $scope.getSuggestCustomers = function (val) {
        val = myApp.formatSpecialchar(val)
        return $http.get('/api/app/getCustomers', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                order_by: '',
                query: 'SearchQuery.contains(\"' + val + '\")'
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.selectCustomer = function (customer) {
        $scope.selectedCustomer = customer;
    }

    $scope.createOrder = function () {

        var checkValidate = true;
        if ($scope.selectedProducts == undefined || $scope.selectedProducts.length == 0) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Vui lòng chọn sản phẩm cho hóa đơn!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }
        else {
            //check stock
            var isInvalid = false;
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                if ($scope.selectedProducts[i].isInvalidQuantity)
                    isInvalid = true;
            }
            if (isInvalid) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Sản phẩm không đủ số lượng bán hoặc số lượng sản phẩm nhập nhỏ hơn 1!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                return;
            }
        }
        //check customer
        if ($scope.selectedCustomer == undefined) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Vui lòng chọn khách hàng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        //check branch
        if ($scope.order.BranchId == undefined || $scope.order.BranchId == '' || $scope.order.BranchId == -1) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Vui lòng chọn chi nhánh bán hàng!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        //check delivery
        if (parseInt($scope.order.DeliveryPrice) > 0 && ($scope.order.DeliveryVendorId == undefined || $scope.order.DeliveryVendorId == "-1")) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Vui lòng chọn đơn vị vận chuyển!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        //check paid
        $scope.order.TotalPrice = isNaN($scope.order.TotalPrice) ? parseInt($scope.order.TotalPrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.TotalPrice;//parseInt($scope.order.TotalPrice);
        $scope.order.CustomerPaid = parseInt($scope.order.CustomerPaid);

        if ($scope.order.CustomerPaid > $scope.order.TotalPrice) {
            $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Khách hàng trả quá tiền hàng, vui lòng nhập lại!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            return;
        }

        //create
        if (isNaN(parseInt($scope.order.CustomerPaid))) {
            $scope.order.CustomerPaid = 0;
        }
        if (isNaN(parseInt($scope.order.DiscountView))) {
            $scope.order.DiscountPrice = 0;
            $scope.order.Discount = 0;
        }
        if (isNaN(parseInt($scope.order.DeliveryPriceView))) {
            $scope.order.DeliveryPrice = 0;
            $scope.order.DeliveryPriceView = 0;
        }
        $scope.order.CustomerId = $scope.selectedCustomer.CustomerId;
        //$scope.order.SaleEmployeeId = parseInt($scope.order.SaleEmployeeId);
        //$scope.order.BranchId = parseInt($scope.order.BranchId);
        $scope.order.OrderPrice = isNaN($scope.order.Total) ? parseInt($scope.order.Total.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.Total;//parseInt($scope.order.Total);
        $scope.order.DiscountPrice = isNaN($scope.order.Discount) ? parseInt($scope.order.Discount.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.Discount;//parseInt($scope.order.Discount);
        $scope.order.DeliveryPrice = isNaN($scope.order.DeliveryPrice) ? parseInt($scope.order.DeliveryPrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.DeliveryPrice; //parseInt($scope.order.DeliveryPrice);       
        $scope.order.MoneyCollect = isNaN($scope.order.MoneyCollect) ? parseInt($scope.order.MoneyCollect.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.MoneyCollect; //parseInt($scope.order.DeliveryPrice);       
        var address = $scope.selectedCustomer.Address != undefined ? $scope.selectedCustomer.Address : "";
        if ($scope.selectedCustomer.District != undefined && $scope.selectedCustomer.District.Name != "")
            address += " " + $scope.selectedCustomer.District.Name;
        if ($scope.selectedCustomer.Province != undefined && $scope.selectedCustomer.Province.Name != "")
            address += " " + $scope.selectedCustomer.Province.Name;

        $scope.order.Channel = "Web";
        //$scope.order.PaymentChannel = "Direct";
        $scope.order.CustomerPaid = isNaN($scope.order.CustomerPaid) ? parseInt($scope.order.CustomerPaid.replace(/[^\d|\-+|\.+]/g, '')) : $scope.order.CustomerPaid;
        $scope.order.Paid = $scope.order.CustomerPaid;
        //$scope.order.DeliveryVendorId = parseInt($scope.order.DeliveryVendorId);
        //$scope.order.DeliveryStatus = parseInt($scope.order.DeliveryStatus);
        //check price
        var products = new Array();
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            var product = {};
            product.ProductAttributeId = $scope.selectedProducts[i].ProductAttributeId;

            if ($scope.selectedProducts[i].Quantity == undefined || $scope.selectedProducts[i].Quantity == '') {
                $mdDialog.show(
                          $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Thông tin')
                            .textContent('Vui lòng chọn số lượng cho sản phẩm ' + (i + 1) + '!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
                return;
            }
            else {
                product.Quantity = parseInt($scope.selectedProducts[i].Quantity);
            }

            if ($scope.selectedProducts[i].stock != null) {

                for (var j = 0; j < $scope.selectedProducts[i].stock.length; j++) {
                    if ($scope.selectedProducts[i].stock[j].Stock <= 0 && $scope.products[i].stock[j].BranchId == $scope.order.BranchId) {
                        $mdDialog.show(
                          $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Thông tin')
                            .textContent('Sản phẩm ' + $scope.selectedProducts[i].ProductName + ' ' + $scope.selectedProducts[i].SubName + ' hiện không còn hàng tại kho ' + $scope.products[i].stock[j].BranchName + '! Vui lòng chọn chi nhánh khác!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
                        return;
                    }
                }
            }
            $scope.order.OrderAddress = address;
            $scope.order.OrderEmail = $scope.selectedCustomer.Email;
            $scope.order.OrderPhone = $scope.selectedCustomer.Phone;
            $scope.order.DeliveryVendorId = parseInt($scope.order.DeliveryVendorId);
            $scope.order.SaleEmployeeId = parseInt($scope.order.SaleEmployeeId);
            $scope.order.DeliveryStatus = parseInt($scope.order.DeliveryStatus);
            $scope.order.PaymentChannel = "Direct";

            product.Price = isNaN($scope.selectedProducts[i].SelectedSalePrice) ? parseInt($scope.selectedProducts[i].SelectedSalePrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.selectedProducts[i].SelectedSalePrice;//$scope.selectedProducts[i].SelectedSalePrice;
            var OriginPrice = isNaN($scope.selectedProducts[i].SelectedOriginPrice) ? parseInt($scope.selectedProducts[i].SelectedOriginPrice.replace(/[^\d|\-+|\.+]/g, '')) : $scope.selectedProducts[i].SelectedOriginPrice;
            var Quantity = isNaN(product.Quantity) ? parseInt(product.Quantity.replace(/[^\d|\-+|\.+]/g, '')) : product.Quantity;
            product.ProductUnitId = parseInt($scope.selectedProducts[i].ProductUnitId);
            products.push(product);
        }
        $scope.order.products = products;

        var createOrder = angular.element(document.querySelector('#createOrder'));
        createOrder.attr('disabled', 'disabled');


        var post = $http({
            method: "POST",
            url: "/api/app/createOrder",
            data: $scope.order
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                $scope.isDone = true;
                if (data.meta.error_code == 200) {
                    //success        
                    createOrder.removeAttr('disabled');

                    var confirm = $mdDialog.confirm()
                      .title('Thông báo')
                      .textContent('Tạo đơn hàng thành công, vui lòng kiểm tra lại trong mục quản lý hóa đơn!')
                      .ok('Làm mới!')
                      .cancel('Về trang quản lý hóa đơn!');

                    $mdDialog.show(confirm).then(function () {
                        $window.location.href = '/sales.html';
                    }, function () {
                        $window.location.href = '/orders.html';
                    });
                }
            }
            else if (data.meta.error_code == 213) {
                createOrder.removeAttr('disabled');
                $scope.order = { "BranchId": $scope.order.BranchId2 + '', "SaleEmployeeId": $scope.order.EmployeeId + '', "DeliveryStatus": "2", "PaymentChannel": "0" };
                $scope.order = { "Total": $scope.order.Total + '', "DiscountView": $scope.order.DiscountView + '', "DeliveryPriceView": $scope.order.DeliveryPriceView, "DeliveryVendorId": $scope.order.DeliveryPriceView };
                $scope.order = { "CustomerPaid": $scope.order.CustomerPaid + '', "TotalPrice": $scope.order.TotalPrice + '', "Notification": $scope.order.Notification, "Remain": $scope.order.Remain };
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Khách hàng trả quá tiền hàng, vui lòng nhập lại!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            }
            else {
                createOrder.removeAttr('disabled');
                console.log($scope.order.BranchId2 + '-' + $scope.order.EmployeeId);
                $scope.order = { "BranchId": $scope.order.BranchId2 + '', "SaleEmployeeId": $scope.order.EmployeeId + '', "DeliveryStatus": "2", "PaymentChannel": "0" };
                $scope.order = { "Total": $scope.order.Total + '', "DiscountView": $scope.order.DiscountView + '', "DeliveryPriceView": $scope.order.DeliveryPriceView, "DeliveryVendorId": $scope.order.DeliveryPriceView };
                $scope.order = { "CustomerPaid": $scope.order.CustomerPaid + '', "TotalPrice": $scope.order.TotalPrice + '', "Notification": $scope.order.Notification, "Remain": $scope.order.Remain };
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo đơn hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) {
            createOrder.removeAttr('disabled');
            //console.log($scope.order.BranchId2 + '-' + $scope.order.EmployeeId);
            $scope.order = { "BranchId": $scope.order.BranchId2, "SaleEmployeeId": $scope.order.EmployeeId, "DeliveryStatus": "2", "PaymentChannel": "0" };
            $scope.order = { "Total": $scope.order.Total + '', "DiscountView": $scope.order.DiscountView + '', "DeliveryPriceView": $scope.order.DeliveryPriceView, "DeliveryVendorId": $scope.order.DeliveryPriceView };
            $scope.order = { "CustomerPaid": $scope.order.CustomerPaid + '', "TotalPrice": $scope.order.TotalPrice + '', "Notification": $scope.order.Notification, "Remain": $scope.order.Remain };
            $mdDialog.show(
                $mdDialog.alert()
                  .clickOutsideToClose(true)
                  .title('Thông tin')
                  .textContent('Tạo đơn hàng thất bại, vui lòng thử lại sau!')
                  .ok('Đóng')
                  .fullscreen(true)
              );
        });
    }

    $scope.selectMoney = function () {
        $scope.order.discountType = 0;
        $scope.calculateOrderPrice();
    }

    $scope.selectPercent = function () {
        $scope.order.discountType = 1;
        $scope.calculateOrderPrice();
    }

    $scope.onDeliveryVendorChange = function () {
        if ($scope.order.DeliveryVendorId == '-1') {
            if ($scope.order.DeliveryStatus != 2) {
                $mdToast.show($mdToast.simple()
                             .textContent('Đã chuyển trạng thái chuyển hàng -> "Đã giao hàng"!')
                             .position('fixed bottom right')
                             .hideDelay(3000));
            }
            $scope.order.DeliveryStatus = '2';
        }
        else {
            if ($scope.order.DeliveryStatus != 4) {
                $mdToast.show($mdToast.simple()
                             .textContent('Đã chuyển trạng thái chuyển hàng -> "Hàng đã chuẩn bị tại kho"!')
                             .position('fixed bottom right')
                             .hideDelay(3000));
            }
            $scope.order.DeliveryStatus = '4'
        }
    }

    //Thêm khách hàng
    $scope.openCreateCustomerDialog = function () {
        //
        //$scope.isEditing = false;
        $http.get("/api/app/getCustomerCode").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.newCustomer.CustomerCode = data.data;
            }
        }).error(function (data, status, headers, config) {

        });

        $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };

        angular.element(document.querySelector('#addCustomer')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    //load tinh thanh
    $scope.loadDistricts = function (province) {
        if (!$scope.isEditing) {
            $scope.newCustomer.Province.Name = province.Name;
            $scope.newCustomer.Province.ProvinceId = province.ProvinceId;
            $scope.newCustomer.District.Name = "Quận / Huyện";
            $scope.newCustomer.District.DistrictId = -1;
        }
        else {
            $scope.editCustomer.Province.Name = province.Name;
            $scope.editCustomer.Province.ProvinceId = province.ProvinceId;
            $scope.editCustomer.District.Name = "Quận / Huyện";
            $scope.editCustomer.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {

            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newCustomer.District.Name = district.Name;
            $scope.newCustomer.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editCustomer.District.Name = district.Name;
            $scope.editCustomer.District.DistrictId = district.DistrictId;
        }
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addCustomer')).modal('toggle');
        $scope.newCustomer = {};
        $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" } };
        $scope.newCustomer.Phone = '';
        $scope.newCustomer.Email = '';
        $scope.newCustomer.TaxID = '';
        $scope.newCustomer.ID = '';
    }

    $scope.createCustomer = function () {
        //if (!$scope.frmAddCustomer.$valid) {
        //    return;
        //}
        cfpLoadingBar.start();

        if ($scope.newCustomer.CustomerName == undefined || $scope.newCustomer.CustomerName == '') {
            cfpLoadingBar.complete();
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng nhập tên khách hàng!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        if ($scope.newCustomer.Phone == undefined || $scope.newCustomer.Phone == '') {
            cfpLoadingBar.complete();
            $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Thông tin')
                    .textContent('Bạn vui lòng nhập số điện thoại khách hàng!')
                    .ok('Đóng')
                    .fullscreen(true)
                );
            return;
        }

        $scope.newCustomer.CustomerGroupId = 1;
        $scope.newCustomer.Type = parseInt($scope.newCustomer.Type);
        $scope.newCustomer.Gender = parseInt($scope.newCustomer.Gender);
        $scope.newCustomer.DistrictId = parseInt($scope.newCustomer.District.DistrictId);
        $scope.newCustomer.ProvinceId = parseInt($scope.newCustomer.Province.ProvinceId);

        var post = $http({
            method: "POST",
            url: "/api/app/createCustomer",
            data: $scope.newCustomer
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {

                    angular.element(document.querySelector('#addCustomer')).modal('toggle')
                    $scope.newCustomer = {};
                    $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" }, "CustomerGroup": { "Id": '-1' } };
                    $scope.selectCustomer(data.data);
                    $mdToast.show($mdToast.simple()
                     .textContent('Thêm khách hàng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" }, "CustomerGroup": { "Id": '-1' } };
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Tạo khách hàng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
        .error(function (data, status, headers, config) { // optional
            $scope.newCustomer = { "Type": "0", "Gender": "1", "Province": { "Name": "Tỉnh / Thành Phố", "ProvinceId": -1 }, "District": { "DistrictId": -1, "Name": "Quận / Huyện" }, "CustomerGroup": { "Id": '-1' } };
            cfpLoadingBar.complete();
            $mdDialog.show(
               $mdDialog.alert()
                 .clickOutsideToClose(true)
                 .title('Thông tin')
                 .textContent('Tạo khách hàng thất bại, vui lòng thử lại sau!')
                 .ok('Đóng')
                 .fullscreen(true)
             );
        });
    }
}]);

myApp.controller('BranchTransfersController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function BranchTransfersController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        cfpLoadingBar.start();
        $scope.loadTransfers();
    }

    $scope.loadTransfers = function () {
        $http.get("/api/app/getTransfers?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.transfers = data.data;
                $scope.item_count = data.metadata.item_count;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadTransfers();
    }

    $scope.onPageChange = function () {
        $scope.loadTransfers();
    }

    $scope.expandDetail = function (transfer) {
        for (var i = 0; i < $scope.transfers.length; i++) {
            if (transfer.TransferId != $scope.transfers[i].TransferId)
                $scope.transfers[i].isExpand = false;
        }
        if (!transfer.isExpand)
            $scope.loadTransferProducts(transfer);
        transfer.isExpand = !transfer.isExpand;
    }

    $scope.loadTransferProducts = function (transfer) {
        $http.get("/api/app/getTransferProducts?id=" + transfer.TransferId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                transfer.products = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.updateTransfer = function (transfer) {
        cfpLoadingBar.start();

        var post = $http({
            method: "POST",
            url: "/api/app/updateTransfer",
            data: transfer
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật phiếu thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật phiếu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }
}]);

myApp.controller('TransferController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function TransferController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {

    $scope.page = 1;
    $scope.page_size = 15;
    $scope.newExchange = {};
    $scope.isDone = false;

    $scope.init = function () {
        $scope.start();
        $scope.loadEmployees();
        $scope.loadBranches();
    }

    $scope.start = function () {
        cfpLoadingBar.start();
    };

    $scope.complete = function () {
        cfpLoadingBar.complete();
    }

    $scope.loadEmployees = function () {
        $http.get("/api/app/getEmployees?page=1&page_size=100&query=1=1").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.employees = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.branches = data.data;
                $scope.complete();
            }
        }).error(function (data, status, headers, config) {

        });
    }
    $scope.removeSelectedProduct = function (product) {
        console.log(product);
        var index = -1;
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].ProductAttributeId == product.ProductAttributeId) {
                index = i;
                break;
            }
        }
        $scope.selectedProducts.splice(index, 1);
    }

    $scope.selectedProducts = new Array();

    var isLoading = false;

    $scope.getSuggestProducts = function (val) {
        val = myApp.formatSpecialchar(val)
        return $http.get('/api/app/getProducts', {
            params: {
                page: $scope.page,
                page_size: $scope.page_size,
                query: 'SearchQuery.contains(\"' + val + '\")',
                order_by: ''
            }
        }).then(function (response) {
            return response.data.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.sumStock = function (stocks) {
        var count = 0;
        if (stocks != null) {
            for (var i = 0; i < stocks.length; i++) {
                count += stocks[i].Stock;
            }
        }
        return count;
    }

    $scope.selectProduct = function (product) {
        //check exist
        if ($scope.selectedProducts != undefined) {
            var isDup = false;
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                if ($scope.selectedProducts[i].ProductAttributeId == product.ProductAttributeId)
                    isDup = true;
            }
            if (!isDup)
                $scope.selectedProducts.push(product);
        }
        else
            $scope.selectedProducts.push(product);
    }

    $scope.getOriginPrice = function (product) {
        var price = 0;
        if (product.prices != null) {
            var index = -1;
            for (var i = 0; i < product.prices.length; i++) {
                if (product.prices[i].Quantity == 1)
                    index = i;
            }
            price = product.prices[index].OriginPrice
        }
        product.price = price;
    }

    $scope.createTransfer = function () {
        if ($scope.transfer.ToBranchId == '-1' || $scope.transfer.FromBranchId == $scope.transfer.ToBranchId) {
            $mdDialog.show(
             $mdDialog.alert()
               .clickOutsideToClose(true)
               .title('Thông tin')
               .textContent('Vui lòng chọn chi nhánh, chi nhánh nhận và chuyển không được trùng nhau!')
               .ok('Đóng')
               .fullscreen(true)
           );
            return;
        }

        $scope.transfer.EmployeeId = parseInt($scope.transfer.EmployeeId);
        $scope.transfer.FromBranchId = parseInt($scope.transfer.FromBranchId);
        $scope.transfer.ToBranchId = parseInt($scope.transfer.ToBranchId);
        var total = 0;
        var exchangeProducts = new Array();
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            var product = {};
            product.ProductAttributeId = $scope.selectedProducts[i].ProductAttributeId;
            product.Quantity = isNaN($scope.selectedProducts[i].quantity) ? parseInt($scope.selectedProducts[i].quantity.replace(/[^\d|\-+|\.+]/g, '')) : $scope.selectedProducts[i].quantity;
            product.Price = isNaN($scope.selectedProducts[i].price) ? parseInt($scope.selectedProducts[i].price.replace(/[^\d|\-+|\.+]/g, '')) : $scope.selectedProducts[i].price;
            product.TotalPrice = product.Quantity * product.Price;
            total += product.TotalPrice;
            product.ProductId = $scope.selectedProducts[i].ProductId;
            exchangeProducts.push(product);
        }
        $scope.transfer.products = exchangeProducts;
        $scope.transfer.Total = total;

        var post = $http({
            method: "POST",
            url: "/api/app/transferProduct",
            data: $scope.transfer
        });

        post.success(function successCallback(data, status, headers, config) {
            // success           
            $scope.isDone = true;
            $scope.transfer.EmployeeId = $scope.transfer.EmployeeId + '';
            $scope.transfer.FromBranchId = $scope.transfer.FromBranchId + '';
            $scope.transfer.ToBranchId = $scope.transfer.ToBranchId + '';
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    //success
                    $scope.isDone = true;
                    $scope.selectedProducts = new Array();
                    $scope.transfer = {};
                    var confirm = $mdDialog.confirm()
                      .title('Thông báo')
                      .textContent('Chuyển hàng thành công, vui lòng kiểm tra lại trong mục nhập/xuất hàng!')
                      .ok('Làm mới!')
                      .cancel('Về trang quản lý');

                    $mdDialog.show(confirm).then(function () {
                        $window.location.href = '/transfers.html';
                    }, function () {
                        $window.location.href = '/branch-transfers.html';
                    });
                }
            }
            else if (data.meta.error_code == 211) {

                $mdDialog.show(
                        $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Vui lòng chọn hàng cần chuyển!')
                           .ok('Đóng')
                           .fullscreen(true)
                       );
            }
            else if (data.meta.error_code == 212) {
                $mdDialog.show(
                        $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Số hàng trong kho cần chuyển không đáp ứng được số lượng đã chọn!')
                           .ok('Đóng')
                           .fullscreen(true)
                       );
            }
            else {
                $scope.newExchange.EmployeeId = $scope.newExchange.EmployeeId + '';
                $scope.newExchange.FromBranchId = $scope.newExchange.FromBranchId + '';
                $scope.newExchange.ToBranchId = $scope.newExchange.ToBranchId + '';
                $mdDialog.show(
                    $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Chuyển hàng thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    $scope.transfer.EmployeeId = $scope.transfer.EmployeeId + '';
                    $scope.transfer.FromBranchId = $scope.transfer.FromBranchId + '';
                    $scope.transfer.ToBranchId = $scope.transfer.ToBranchId + '';
                    $mdDialog.show(
                    $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Chuyển hàng thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                });
    }

    $scope.findBranchName = function (id) {
        var name = "";
        for (var i = 0; i < $scope.branches.length; i++) {
            if ($scope.branches[i].BranchId == parseInt(id)) {
                name = $scope.branches[i].BranchName;
                break;
            }
        }
        return name;
    }

}]);

myApp.controller('BranchController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function BranchController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.isEditing = false;

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.getBranches();
    }

    $scope.newBranch = {};

    $scope.getBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if ($scope.page == 1) {
                    $scope.branches = data.data;
                }
                else {
                    $scope.branches = $scope.branches.concat(data.data);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.openCreateBranchDialog = function () {
        //
        $scope.isEditing = false;
        angular.element(document.querySelector('#addBranch')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.cancelCreate = function () {
        angular.element(document.querySelector('#addBranch')).modal('toggle');
        $scope.newBranch = {};
    }

    $scope.createBranch = function () {
        if (!$scope.frmAddBranch.$valid) {
            return;
        }

        //if (!$scope.frmAddBranch.$valid)
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/createBranch",
            data: $scope.newBranch
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                //if (data.meta.error_code == 200) {
                angular.element(document.querySelector('#addBranch')).modal('toggle')
                $mdToast.show($mdToast.simple()
                 .textContent('Thêm chi nhánh thành công!')
                 .position('fixed bottom right')
                 .hideDelay(3000));
                $scope.newBranch = {}
                $scope.getBranches();
                //}
            }
            else {
                $mdDialog.show(
                    $mdDialog.alert()
                      .clickOutsideToClose(true)
                      .title('Thông tin')
                      .textContent('Tạo chi nhánh thất bại, vui lòng thử lại sau!')
                      .ok('Đóng')
                      .fullscreen(true)
                  );
            }
        })
        .error(function (data, status, headers, config) { // optional
            cfpLoadingBar.complete();
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Thông tin')
                .textContent('Tạo chi nhánh thất bại, vui lòng thử lại sau!')
                .ok('Đóng')
                .fullscreen(true)
            );
        });
    }

    $scope.expandDetail = function (branch) {
        for (var i = 0; i < $scope.branches.length; i++) {
            if (branch.BranchId != $scope.branches[i].BranchId)
                $scope.branches[i].isExpand = false;
        }
        branch.isExpand = !branch.isExpand;
    }

    $scope.openEditBranchDialog = function (branch) {
        //
        $scope.isEditing = true;
        $scope.editBranch = angular.copy(branch);
        $scope.originBranch = angular.copy(branch);
        angular.element(document.querySelector('#editBranch')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.updateBranch = function () {
        if (!$scope.frmEditBranch.$valid) {
            return;
        }

        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/updateBranch",
            data: $scope.editBranch
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editBranch')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật chi nhánh thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.editBranch = {};
                    $scope.getBranches();
                }
            }
            else {
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Cập nhật chi nhánh thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Cập nhật chi nhánh thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                });
    }

    $scope.showConfirmDeleteBranch = function (branch) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn xóa chi nhánh "' + branch.BranchName + '" hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            $scope.deleteBranch(branch);
        }, function () {
        });
    };

    $scope.deleteBranch = function (branch) {
        cfpLoadingBar.start();
        var post = $http({
            method: "POST",
            url: "/api/app/deleteBranch",
            data: branch
        });

        post.success(function successCallback(data, status, headers, config) {
            // success         
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200 || data.meta.error_code == 222) {
                if (data.meta.error_code == 200) {
                    $scope.getBranches();
                    $mdToast.show($mdToast.simple()
                     .textContent('Xóa chi nhánh thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
                else {
                    //show dialog
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Hàng tồn trong kho vẫn còn, vui lòng chuyển qua kho khác !')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Xóa chi nhánh thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('Thông tin')
                       .textContent('Xóa chi nhánh thất bại, vui lòng thử lại sau!')
                       .ok('Đóng')
                       .fullscreen(true)
                   );
                });
    }
}]);

myApp.controller('PageController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', '$filter', '$FB', function PageController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast, $filter, $FB) {

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        //$scope.getPages();
    }

    $scope.$watch(function () { return $FB.loaded }, function () {
        if ($FB.loaded) {
            $scope.getPages();
        }
    });

    $scope.getPages = function () {
        $http.get("/api/app/getPages").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.pages = data.data;
                if ($scope.pages == undefined)
                    $scope.pages = new Array();
                else {
                    for (var i = 0; i < $scope.pages.length; i++) {
                        if ($scope.pages[i].isSubscribed)
                            $scope.countLinked += 1;
                    }
                }
                if ($scope.Group == 'Admin') {
                    //get page by token if valid                    
                    $FB.getLoginStatus(function (response) {
                        if (response.status === 'connected') {
                            $scope.loggedFacebookUserId = response.authResponse.userID;
                            if ($scope.loggedFacebookUserId == $scope.FacebookUserId) {
                                $FB.api(
                             '/me/accounts',
                             'GET',
                             {},
                             function (response) {
                                 if (response != undefined) {
                                     if (response.data != undefined) {
                                         for (var i = 0; i < response.data.length; i++) {
                                             var isDup = false;
                                             for (var j = 0; j < $scope.pages.length; j++) {
                                                 if (response.data[i].id == $scope.pages[j].id) {
                                                     isDup = true;
                                                     break;
                                                 }
                                             }
                                             if (!isDup) {
                                                 $scope.pages.push(response.data[i]);
                                                 $scope.$apply();
                                             }
                                         }
                                     }
                                     else
                                         $scope.showLogin = true;
                                 }
                                 else {
                                     //required login
                                     $scope.showLogin = true;
                                 }
                             }
                           );
                            }
                        } else if (response.status === 'not_authorized') {
                            // the user is logged in to Facebook, 
                            // but has not authenticated your app
                        } else {
                            // the user isn't logged in to Facebook.
                        }
                    });
                }

            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loginFacebook = function () {
        FB.login(function (response) {

            if (response.authResponse) {
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID
                //update token
                user_email = response.email; //get user email
                var apiUrl = '/api/app/updateToken';

                var updateToken = {
                    email: user_email,
                    id: user_id,
                    token: access_token
                };

                var post = $http({
                    method: "POST",
                    url: apiUrl,
                    data: updateToken
                });

                post.success(function successCallback(data, status, headers, config) {
                    // success     
                    $scope.showLogin = false;
                    if (data.meta.error_code == 200) {
                        //get page 
                        //update token
                        $http.get("/api/app/RefreshPageAndToken").success(function (data, status, headers) {
                            $scope.pages = data.data;
                        }).error(function (data, status, headers, config) {

                        });
                    }
                    else if (data.meta.error_code == 400) {

                    }
                })
                .error(function (data, status, headers, config) { // optional                    
                });
            } else {
                //user hit cancel button
            }
        }, {
            scope: 'email,manage_pages,pages_messaging,pages_messaging_subscriptions,pages_messaging_phone_number,pages_show_list,publish_pages,read_page_mailboxes'
        });
    }

    $scope.linkFanPage = function (event, page) {
        var button = event.currentTarget;
        page.isLinking = true;
        var p = {
            id: page.id,
            page_name: page.name,
            access_token: page.access_token
        };

        var post = $http({
            method: "POST",
            url: "/api/app/linkPage",
            data: p
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            page.isLinking = false;
            if (data.meta.error_code == 200) {
                $scope.getPages();
                $mdToast.show($mdToast.simple()
                     .textContent('Liên kết page thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
            }
            else if (data.meta.error_code == 212) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Page này đã được liên kết với một cửa hàng khác, vui lòng chọn page khác!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else if (data.meta.error_code == 500) {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Page này đã được liên kết với một cửa hàng khác, vui lòng chọn page khác!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Có lỗi xảy ra vui lòng thử lại sau!')
                            .ok('Đóng')
                            .fullscreen(true)
                        );
            }
        })
                    .error(function (data, status, headers, config) {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Có lỗi xảy ra vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                        page.isLinking = false;
                    });

    }

    $scope.unlinkFanPage = function (event, page) {
        page.isLinking = true;
        var post = $http({
            method: "POST",
            url: "/api/app/unlinkPage",
            data: page
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                $scope.getPages();
                $mdToast.show($mdToast.simple()
                     .textContent('Hủy liên kết page thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Hủy liên kêt page thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
            page.isLinking = false;
        })
            .error(function (data, status, headers, config) {
                $mdDialog.show(
                 $mdDialog.alert()
                   .clickOutsideToClose(true)
                   .title('Thông tin')
                   .textContent('Có lỗi xảy ra vui lòng thử lại sau!')
                   .ok('Đóng')
                   .fullscreen(true)
               );
                page.isLinking = false;
            });
    }

    $scope.showDownloadContent = function (page) {
        $scope.selectedPage = page;
        angular.element(document.querySelector('#downloadContentModal')).modal('toggle');
    }

    $scope.downloadContent = function () {
        $scope.selectedPage.isDownloading = true;
        var since = "";
        var today = new Date();
        if ($scope.selectedTime == "1") {
            today.setDate(today.getDate() + 1)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "2") {
            today.setDate(today.getDate() + 2)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "3") {
            today.setDate(today.getDate() + 3)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        else if ($scope.selectedTime == "7") {
            today.setDate(today.getDate() + 7)
            since = $filter('date')(today, "ddMMyyyy0000");
        }
        var download = {
            page_id: $scope.selectedPage.PageId,
            since: since
        }
        var post = $http({
            method: "POST",
            url: "/api/app/downloadContent",
            data: download
        });

        post.success(function successCallback(data, status, headers, config) {
            // success      
            if (data.meta.error_code == 200) {
                $scope.selectedPage.isDownloading = false;
                angular.element(document.querySelector('#downloadContentModal')).modal('toggle')
                $mdToast.show($mdToast.simple()
                       .textContent('Tải dữ liệu thành công!')
                       .position('fixed bottom right')
                       .hideDelay(3000));
            }
            else {
                $scope.selectedPage.isDownloading = false;
                $mdToast.show($mdToast.simple()
                       .textContent('Tải dữ liệu thất bại!')
                       .position('fixed bottom right')
                       .hideDelay(3000));
            }
        })
                    .error(function (data, status, headers, config) {
                        $scope.selectedPage.isDownloading = false;
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(true)
                           .title('Thông tin')
                           .textContent('Tải dữ liệu thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                    });
    }
}]);

myApp.controller('ReportEveryDayController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'Excel', '$timeout', 'cfpLoadingBar', '$mdDialog', '$location', '$filter', function ShopController($scope, $http, config, ngDialog, md5, $window, Excel, $timeout, cfpLoadingBar, $mdDialog, $location, $filter) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.select_page = 1;

    $scope.isLoading = false;

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        cfpLoadingBar.start();
        $scope.loadPageDate();
    }

    $scope.onPageChange = function () {
        cfpLoadingBar.start();
        $scope.loadPageDate();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            $scope.dt = $scope.q.StartDate;
            if (query != "") {
                query += " AND CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }
        $scope.query = query;

        cfpLoadingBar.start();
        $scope.loadPageDate();

    }

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        var query = "";
        var year = $scope.dt.getFullYear();
        var month = $scope.dt.getMonth();
        var date = $scope.dt.getDate();

        if (query != "") {
            query += " AND CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
        }
        else {
            query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0)";
        }
        $scope.query = query;

        $scope.reportDateSale();
        $scope.dateUrl = '/Reports/Date/dateSale.html';

    }

    $scope.loadPageDate = function () {
        if ($scope.select_page == 1) {
            $scope.reportDateSale();
            $scope.dateUrl = '/Reports/Date/dateSale.html';
        }
        else if ($scope.select_page == 2) {
            $scope.reportDateCashes();
            $scope.dateUrl = '/Reports/Date/dateCashes.html';
        }
        else if ($scope.select_page == 3) {
            $scope.reportDateProduct();
            $scope.dateUrl = '/Reports/Date/dateProducts.html';
        }
    }

    //Mối quan tâm
    $scope.dateSale = function () {
        $scope.select_page = 1;
        cfpLoadingBar.start();
        $scope.reportDateSale();
        $scope.dateUrl = '/Reports/Date/dateSale.html';
    }

    $scope.dateCashes2 = function () {
        $scope.select_page = 2;
        cfpLoadingBar.start();
        $scope.reportDateCashes();
        $scope.dateUrl = '/Reports/Date/dateCashes.html';
    }

    $scope.dateProduct2 = function () {
        $scope.select_page = 3;
        cfpLoadingBar.start();
        $scope.reportDateProduct();
        $scope.dateUrl = '/Reports/Date/dateProducts.html';
    }

    $scope.dateTotal = function () {
        $scope.select_page = 4;
        cfpLoadingBar.start();
        $scope.reportDateProduct();
        $scope.dateUrl = '/Reports/Date/dateTotal.html';
    }

    $scope.reportDateSale = function () {
        $http.get("/api/app/reportDateSale?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.datesale = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportDateCashes = function () {
        $http.get("/api/app/reportDateCashes?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.datecashes = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportDateProduct = function () {
        $http.get("/api/app/reportDateProduct?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.dateproduct = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.checkNull = function () {
        if ($scope.item_count == 0) {
            var trNull = angular.element(document.querySelector('#trNull'));
            trNull.removeClass('hide');
            var trMeta = angular.element(document.querySelector('#trMeta'));
            trMeta.addClass('hide');
        }
        else {
            var trNull = angular.element(document.querySelector('#trNull'));
            trNull.addClass('hide');
            var trMeta = angular.element(document.querySelector('#trMeta'));
            trMeta.removeClass('hide');
        }
    }

    //Tạo bản in
    $scope.printDate = function () {
        if ($scope.select_page == 1) {
            $scope.printDateSale();
        }
        else if ($scope.select_page == 2) {
            $scope.printDateCashes();
        }
        else if ($scope.select_page == 3) {
            $scope.printDateProduct();
        }
    }

    $scope.printDateSale = function () {

        tableRows = [];
        var setData = $scope.datesale;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                OrderCode: value.OrderCode,
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                Quantity: value.Quantity,
                OrderPrice: $filter('currency')(value.OrderPrice, "", 0),
                DeliveryPrice: $filter('currency')(value.DeliveryPrice, "", 0),
                DiscountPrice: $filter('currency')(value.DiscountPrice, "", 0),
                Total: $filter('currency')(value.OrderPrice - value.DeliveryPrice + value.DiscountPrice, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.OrderCode,
                item.CreatedAt,
                item.Quantity,
                item.OrderPrice,
                item.DeliveryPrice,
                item.DiscountPrice,
                item.Total,
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo cuối ngày về bán hàng', style: 'headerTile' },
             { text: 'Ngày bán ' + $filter('date')($scope.dt, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã chứng từ', 'Thời gian', 'SL Sản phẩm', 'Giá trị đơn hàng', 'Giảm giá', 'Phí vận chuyển', 'Doanh thu'],
                       [{ text: 'Số hóa đơn: ' + $scope.metadata.Count, colSpan: 3, alignment: 'left' }, {}, {},
                           $scope.metadata.TotalProduct, $filter('currency')($scope.metadata.TotalOrderPrice, "", 0),
                           $filter('currency')($scope.metadata.TotalDeliveryPrice, "", 0), $filter('currency')($scope.metadata.TotalDiscountPrice, "", 0),
                           $filter('currency')($scope.metadata.TotalOrderPrice - $scope.metadata.TotalDeliveryPrice + $scope.metadata.TotalDiscountPrice, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printDateCashes = function () {

        tableRows = [];
        var setData = $scope.datecashes;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                CashCode: value.CashCode,
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                CashTypeName: value.CashTypeName,
                PaidTargetName: value.PaidTargetName,
                PaidAmount: $filter('currency')(value.PaidAmount, "", 0),
                Total: $filter('currency')(value.Total, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.CashCode,
                item.CreatedAt,
                item.CashTypeName,
                item.PaidTargetName,
                item.PaidAmount,
                item.Total,
            ];
        });

        var docDefinition = {

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo cuối ngày về thu chi', style: 'headerTile' },
             { text: 'Ngày bán ' + $filter('date')($scope.dt, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã phiếu thu/chi', 'Thời gian', 'Thu / chi', 'Người nộp/nhận', 'Đã thu / chi', 'Tổng tiền'],
                       [{ text: 'Số phiếu: ' + $scope.metadata.Count, colSpan: 5, alignment: 'left' }, {}, {}, {}, {},
                           $filter('currency')($scope.metadata.TotalCashes, "", 0), $filter('currency')($scope.metadata.TotalPrice, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printDateProduct = function () {

        tableRows = [];
        var setData = $scope.dateproduct;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductFullName: value.ProductFullName,
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                QuantitySale: value.QuantitySale,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0),
                QuantityReturn: value.QuantityReturn,
                TotalPriceReturn: $filter('currency')(value.TotalPriceReturn, "", 0),
                Total: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductFullName,
                item.CreatedAt,
                item.QuantitySale,
                item.TotalPriceSale,
                item.QuantityReturn,
                item.TotalPriceReturn,
                item.Total,
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo cuối ngày về hàng hóa', style: 'headerTile' },
             { text: 'Ngày bán ' + $filter('date')($scope.dt, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Tên hàng', 'Thời gian', 'SL Bán', 'Giá trị bán', 'SL Trả', 'Giá trị trả', 'Doanh thu (thuần)'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 3, alignment: 'left' }, {}, {},
                           $scope.metadata.TotalSale, $filter('currency')($scope.metadata.TotalPriceSale, "", 0),
                           $scope.metadata.TotalReturn, $filter('currency')($scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalPriceSale - $scope.metadata.TotalPriceReturn, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    //$scope.openPDF = function () {                       
    //    songRows = [];
    //    var setSongs = $scope.datesale;       
    //    angular.forEach(setSongs, function (value, key) {
    //        //console.log('OrderCode:' + value.OrderCode + ' CreatedAt' + value.CreatedAt + ' Quantity' + value.Quantity );
    //        songRows.push({ OrderCode: value.OrderCode, CreatedAt: value.CreatedAt, Quantity: value.Quantity });
    //    });

    //    var items = songRows.map(function (item) {
    //        console.log('OrderCode:' + item.OrderCode + ' CreatedAt:' + item.CreatedAt + ' Quantity:' + item.Quantity);
    //        return [(100 + songRows.indexOf(item) + 1).toString().slice(-2) + '.', item.OrderCode, item.CreatedAt, item.Quantity];
    //    });

    //    var docDefinition = 
    //    {       
    //        pageOrientation: 'landscape',

    //        content: 
    //        [
    //             //{ text: setNumber, style: 'firstLine'},
    //             //{ text: setList.EventDate + ' - ' + setList.SetName + ' @ ' + setList.Venue + '\n\n', style: 'secondLine' },                 
    //             {  
    //                 style: 'songRow',
    //                 table: 
    //                 {                                                   
    //                     body: 
    //                     [
    //                       [                             
    //                         { text: '------' },
    //                         { text: '--------------------------------------' },
    //                         { text: '--------------------------------------' },
    //                         { text: '--------------------------------------' },  
    //                       ]
    //                     ].concat(items)
    //                 }
    //             }
    //        ],

    //        styles: {
    //            firstLine: {
    //                fontSize: 32,
    //                bold: true,
    //                alignment: 'center'
    //            },
    //            secondLine: {
    //                fontSize: 15,
    //                bold: true,
    //                alignment: 'center'
    //            },
    //            songRow: {
    //                fontSize: 18,
    //                bold: true,
    //                alignment: 'center',                   
    //            },
    //        }

    //    };//End docDefinition

    //    //Generate PDF
    //    pdfMake.createPdf(docDefinition).open();
    //} //END Generate PDF Function  

    //$scope.exportAction = function (option) {
    //    switch (option) {
    //        case 'pdf': $scope.$broadcast('export-pdf', {});
    //            break;
    //        case 'excel': $scope.$broadcast('export-excel', {});
    //            break;
    //        case 'doc': $scope.$broadcast('export-doc', {});
    //            break;
    //        case 'csv': $scope.$broadcast('export-csv', {});
    //            break;
    //        default: console.log('no event caught');
    //    }
    //}

}]);

myApp.controller('ReportSaleController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'Excel', '$timeout', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', '$location', function ShopController($scope, $http, config, ngDialog, md5, $window, Excel, $timeout, cfpLoadingBar, $filter, $mdDialog, $mdToast, $location) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.select_page = 1;
    $scope.typedate = 2;
    $scope.datetimetext = "tuần này";
    $scope.type_report = 2;

    $scope.isLoading = false;

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadSalePage();
    }

    $scope.onPageChange = function () {
        $scope.loadSalePage();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            $scope.dt = $scope.q.StartDate;
            if (query != "") {
                query += " AND CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }
        $scope.query = query;
        $scope.loadProductPage();
    }

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        //init    
        cfpLoadingBar.start();
        var BranchId = angular.element(document.querySelector('#reportBranchId'));
        $scope.BranchId = BranchId.val();
        $scope.reportSaleTime();
        $scope.loadBranches();
        $scope.saleUrl = '/Reports/Sales/saleTime.html';
    }

    //Lọc theo chi nhánh
    $scope.loadBranches = function () {
        $http.get("/api/app/getBranches").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.branches = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectBranch = function () {

    }

    $scope.loadSalePage = function () {
        if ($scope.select_page == 1) {
            if ($scope.type_report == 1) {
                $scope.reportSaleTimeChart();
                $scope.saleUrl = '/Reports/Sales/saleTimeChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportSaleTime();
                $scope.saleUrl = '/Reports/Sales/saleTime.html';
            }
        }
        else if ($scope.select_page == 2) {
            $scope.reportSaleProfit();
            $scope.saleUrl = '/Reports/Sales/saleProfit.html';
        }
        else if ($scope.select_page == 3) {
            $scope.reportSaleSubPrice();
            $scope.saleUrl = '/Reports/Sales/saleSubPrice.html';
        }
        else if ($scope.select_page == 4) {
            $scope.reportSaleReturnProduct();
            $scope.saleUrl = '/Reports/Sales/saleReturnProduct.html';
        }
        else if ($scope.select_page == 5) {
            $scope.reportSaleEmployees();
            $scope.saleUrl = '/Reports/Sales/saleEmployees.html';
        }
    }

    //Kiểu hiển thị
    $scope.saleChart = function () {
        $scope.type_report = 1;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.addClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.page == 1) {
            $scope.reportSaleTimeChart();
            $scope.saleUrlChart = '/Reports/Sales/saleTimeChart.html';
        }
        else if ($scope.page == 2) {
            $scope.reportSaleProfitChart();
            $scope.saleUrlChart = '/Reports/Sales/saleProfitChart.html';
        }
    }

    $scope.saleDoc = function () {
        $scope.type_report = 2;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');
        cfpLoadingBar.start();
        if ($scope.page == 1) {
            $scope.reportSaleTime();
            $scope.saleUrl = '/Reports/Sales/saleTime.html';
        }
        else if ($scope.page == 2) {
            $scope.reportSaleProfit();
            $scope.saleUrl = '/Reports/Sales/saleProfit.html';
        }
        else if ($scope.page == 3) {
            $scope.reportSaleSubPrice();
            $scope.saleUrl = '/Reports/Sales/saleSubPrice.html';
        }
        else if ($scope.page == 4) {
            $scope.reportSaleReturnProduct();
            $scope.saleUrl = '/Reports/Sales/saleReturnProduct.html';
        }
        else if ($scope.page == 5) {
            $scope.reportSaleEmployees();
            $scope.saleUrl = '/Reports/Sales/saleEmployees.html';
        }
    }

    //Mối quan tâm
    $scope.saleTime = function () {
        $scope.select_page = 1;
        $scope.typedate = 2;
        var saleTime2 = angular.element(document.querySelector('#saleTime2'));
        saleTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.type_report == 1) {
            $scope.reportSaleTimeChart();
            $scope.saleUrlChart = '/Reports/Sales/saleTimeChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportSaleTime();
            $scope.saleUrl = '/Reports/Sales/saleTime.html';
        }
    }

    $scope.saleProfit = function () {
        $scope.select_page = 2;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var saleTime2 = angular.element(document.querySelector('#saleTime2'));
        saleTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        if ($scope.type_report == 1) {
            $scope.reportSaleProfitChart();
            $scope.saleUrlChart = '/Reports/Sales/saleProfitChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportSaleProfit();
            $scope.saleUrl = '/Reports/Sales/saleProfit.html';
        }
    }

    $scope.saleSubPrice = function () {
        $scope.select_page = 3;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        $scope.reportSaleSubPrice();
        var saleTime2 = angular.element(document.querySelector('#saleTime2'));
        saleTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.saleUrl = '/Reports/Sales/saleSubPrice.html';
    }

    $scope.saleReturnProduct = function () {
        $scope.select_page = 4;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        $scope.reportSaleReturnProduct();
        var saleTime2 = angular.element(document.querySelector('#saleTime2'));
        saleTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.saleUrl = '/Reports/Sales/saleReturnProduct.html';
    }

    $scope.saleEmployees = function () {
        $scope.select_page = 5;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var saleTime2 = angular.element(document.querySelector('#saleTime2'));
        saleTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        if ($scope.type_report == 1) {
            $scope.reportSaleEmployees();
            $scope.saleUrlChart = '/Reports/Sales/saleEmployeesChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportSaleEmployees();
            $scope.saleUrl = '/Reports/Sales/saleEmployees.html';
        }
    }

    //Thời gian
    $scope.saleTime1 = function () {
        $scope.typedate = 1;
        $scope.datetimetext = "hôm nay";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime2 = function () {
        $scope.typedate = 2;
        $scope.datetimetext = "tuần này";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime3 = function () {
        $scope.typedate = 3;
        $scope.datetimetext = "tháng này";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime4 = function () {
        $scope.typedate = 4;
        $scope.datetimetext = "quý này";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime5 = function () {
        $scope.typedate = 5;
        $scope.datetimetext = "Năm nay";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime11 = function () {
        $scope.typedate = 6;
        $scope.datetimetext = "hôm qua";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime21 = function () {
        $scope.typedate = 7;
        $scope.datetimetext = "tuần trước";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime22 = function () {
        $scope.typedate = 8;
        $scope.datetimetext = "7 ngày qua";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime31 = function () {
        $scope.typedate = 9;
        $scope.datetimetext = "tháng trước";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime41 = function () {
        $scope.typedate = 10;
        $scope.datetimetext = "quý trước";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    $scope.saleTime51 = function () {
        $scope.typedate = 11;
        $scope.datetimetext = "năm trước";
        cfpLoadingBar.start();
        $scope.loadSalePage();
    }

    //Lấy dữ liệu
    $scope.reportSaleTime = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleTime?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.saletimes = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportSaleTimeChart = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleTimeChart?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    console.log($scope.stats[i].Day + '-' + $scope.stats[i].TotalPrice);
                    $scope.chart.labels.push($scope.stats[i].Day);
                    $scope.chart.data.push($scope.stats[i].TotalPrice);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportSaleProfit = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleProfit?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.saleprofit = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportSaleProfitChart = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleProfitChart?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    console.log($scope.stats[i].Day + '-' + $scope.stats[i].TotalPrice);
                    $scope.chart.labels.push($scope.stats[i].Day);
                    $scope.chart.data.push($scope.stats[i].TotalPrice - $scope.stats[i].TotalOriginPrice);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportSaleReturnProduct = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleReturnProduct?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.salereturnproduct = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportSaleSubPrice = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleSubPrice?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.salesubprice = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportSaleEmployees = function () {
        $scope.formatDate();
        $http.get("/api/app/reportSaleEmployees?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.saleemployees = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    $scope.chart.labels.push($scope.stats[i].EmployeeName);
                    $scope.chart.data.push($scope.stats[i].TotalPrice);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.formatDate = function () {
        var query = "";
        var year = $scope.dt.getFullYear();
        var month = $scope.dt.getMonth();
        var date = $scope.dt.getDate();

        $scope.startdate = new Date();
        $scope.enddate = new Date();

        if ($scope.typedate == 1) {
            $scope.startdate = new Date();
            $scope.enddate = new Date();
        }
        else if ($scope.typedate == 2) {
            var dayOfWeek = $scope.startdate.getDay();
            var startDay = 0;
            if (dayOfWeek == 0) {
                startDay = 6;
            }
            else {
                startDay = dayOfWeek - 1;
            }

            $scope.startdate.setDate(date - startDay);
            $scope.enddate.setDate(date - startDay + 6);
        }
        else if ($scope.typedate == 3) {
            $scope.startdate.setDate(1);
            var monthStart = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth(), 1);
            var monthEnd = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth() + 1, 1);
            var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
            $scope.enddate.setDate(monthLength);
        }
        else if ($scope.typedate == 4) {
            if ($scope.startdate.getMonth() >= 0 && $scope.startdate.getMonth() <= 2) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 0, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 2, 31);
            }
            else if ($scope.startdate.getMonth() >= 3 && $scope.startdate.getMonth() <= 5) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 3, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 5, 30);
            }
            else if ($scope.startdate.getMonth() >= 6 && $scope.startdate.getMonth() <= 8) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 6, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 8, 30);
            }
            else if ($scope.startdate.getMonth() >= 9 && $scope.startdate.getMonth() <= 11) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 9, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 11, 31);
            }
        }
        else if ($scope.typedate == 5) {
            $scope.startdate = new Date($scope.startdate.getFullYear(), 0, 1);
            $scope.enddate = new Date($scope.startdate.getFullYear(), 11, 31);
        }
        else if ($scope.typedate == 6) {
            var date = new Date();
            $scope.startdate.setDate(date.getDate() - 1);
            $scope.enddate.setDate(date.getDate() - 1);
        }
        else if ($scope.typedate == 7) {
            //var date = new Date();
            $scope.startdate.setDate($scope.startdate.getDate() - 7);
            $scope.enddate.setDate($scope.enddate.getDate() - 7);
            var dayOfWeek = $scope.startdate.getDay();
            var startDay = 0;
            if (dayOfWeek == 0) {
                startDay = 6;
            }
            else {
                startDay = dayOfWeek - 1;
            }

            $scope.startdate.setDate($scope.startdate.getDate() - startDay);
            $scope.enddate.setDate($scope.enddate.getDate() - startDay + 6);
        }
        else if ($scope.typedate == 8) {
            var date = new Date();
            $scope.startdate.setDate(date.getDate() - 7);
        }
        else if ($scope.typedate == 9) {
            $scope.startdate.setMonth($scope.startdate.getMonth() - 1);
            $scope.enddate.setMonth($scope.enddate.getMonth() - 1);
            $scope.startdate.setDate(1);
            var monthStart = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth(), 1);
            var monthEnd = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth() + 1, 1);
            var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
            $scope.enddate.setDate(monthLength);
        }
        else if ($scope.typedate == 10) {
            $scope.startdate.setMonth($scope.startdate.getMonth() - 3);
            if ($scope.startdate.getMonth() >= 0 && $scope.startdate.getMonth() <= 2) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 0, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 2, 31);
            }
            else if ($scope.startdate.getMonth() >= 3 && $scope.startdate.getMonth() <= 5) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 3, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 5, 30);
            }
            else if ($scope.startdate.getMonth() >= 6 && $scope.startdate.getMonth() <= 8) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 6, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 8, 30);
            }
            else if ($scope.startdate.getMonth() >= 9 && $scope.startdate.getMonth() <= 11) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 9, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 11, 31);
            }
        }
        else if ($scope.typedate == 11) {
            $scope.startdate = new Date($scope.startdate.getFullYear() - 1, 0, 1);
            $scope.enddate = new Date($scope.enddate.getFullYear() - 1, 11, 31);
        }

        if (query != "") {
            query += " AND CreatedAt >= DateTime(" + $scope.startdate.getFullYear() + "," + ($scope.startdate.getMonth() + 1) + "," + $scope.startdate.getDate() + ",0,0,0) AND CreatedAt <= DateTime(" + $scope.enddate.getFullYear() + "," + ($scope.enddate.getMonth() + 1) + "," + $scope.enddate.getDate() + ",23,59,59)";
        }
        else {
            query += "CreatedAt >= DateTime(" + $scope.startdate.getFullYear() + "," + ($scope.startdate.getMonth() + 1) + "," + $scope.startdate.getDate() + ",0,0,0) AND CreatedAt <= DateTime(" + $scope.enddate.getFullYear() + "," + ($scope.enddate.getMonth() + 1) + "," + $scope.enddate.getDate() + ",23,59,59)";
        }
        $scope.query = query;
        console.log("query=" + $scope.query);
    }

    $scope.checkNull = function () {
        if ($scope.item_count == 0) {
            var trNull = angular.element(document.querySelector('#trNull'));
            trNull.removeClass('hide');
            var trMeta = angular.element(document.querySelector('#trMeta'));
            trMeta.addClass('hide');
        }
        else {
            var trNull = angular.element(document.querySelector('#trNull'));
            trNull.addClass('hide');
            var trMeta = angular.element(document.querySelector('#trMeta'));
            trMeta.removeClass('hide');
        }
    }

    //Tạo bản in
    $scope.printSale = function () {
        if ($scope.select_page == 1) {
            $scope.printSaleTime();
        }
        else if ($scope.select_page == 2) {
            $scope.printSaleProfit();
        }
        else if ($scope.select_page == 3) {
            $scope.printSaleSubPrice();
        }
        else if ($scope.select_page == 4) {
            $scope.printSaleReturnProduct();
        }
        else if ($scope.select_page == 5) {
            $scope.printSaleEmployees();
        }
    }

    $scope.printSaleTime = function () {

        tableRows = [];
        var setData = $scope.saletimes;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                OrderCode: value.OrderCode,
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                CustomerName: value.CustomerName,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0),
                TotalPriceReturn: $filter('currency')(value.TotalPriceReturn, "", 0),
                Total: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.OrderCode,
                item.CreatedAt,
                item.CustomerName,
                item.TotalPriceSale,
                item.TotalPriceReturn,
                item.Total,
            ];
        });

        var docDefinition = {
            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo bán hàng theo thơi gian', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã chứng từ', 'Thời gian', 'Khách hàng', 'Doanh thu', 'Giá trị trả', 'Doanh thu (thuần)'],
                       [{ text: 'Số hóa đơn: ' + $scope.metadata.Count, colSpan: 4, alignment: 'left' }, {}, {}, {},
                           $filter('currency')($scope.metadata.TotalPrice, "", 0), $filter('currency')($scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalPrice - $scope.metadata.TotalPriceReturn, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printSaleProfit = function () {

        tableRows = [];
        var setData = $scope.saleprofit;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                OrderPrice: value.OrderPrice,
                DiscountPrice: $filter('currency')(value.DiscountPrice, "", 0),
                DeliveryPrice: $filter('currency')(value.DeliveryPrice, "", 0),
                TotalPrice: $filter('currency')(value.TotalPrice, "", 0),
                TotalOriginPrice: $filter('currency')(value.TotalOriginPrice, "", 0),
                Total: $filter('currency')(value.TotalPrice - value.TotalOriginPrice, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.CreatedAt,
                item.OrderPrice,
                item.DiscountPrice,
                item.DeliveryPrice,
                item.TotalPrice,
                item.TotalOriginPrice,
                item.Total,
            ];
        });

        var docDefinition = {

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo lợi nhuận theo hóa đơn', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Thơi gian', 'Tổng tiền hàng', 'Giảm giá HD', 'Phí vận chuyển', 'Doanh thu', 'Tổng giá vốn', 'Lợi nhuận'],
                       [{ text: 'Số hóa đơn: ' + $scope.metadata.Count, colSpan: 2, alignment: 'left' }, {},
                           $filter('currency')($scope.metadata.TotalOrderPrice, "", 0), $filter('currency')($scope.metadata.TotalDiscount, "", 0),
                           $filter('currency')($scope.metadata.TotalDelivery, "", 0), $filter('currency')($scope.metadata.TotalPrice, "", 0),
                           $filter('currency')($scope.metadata.TotalOriginPrice, "", 0), $filter('currency')($scope.metadata.TotalPrice - $scope.metadata.TotalOriginPrice, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },

            //footer: {
            //    columns: [
            //      'Left part',
            //      { text: 'Right part', alignment: 'right' }
            //    ],

            //},
        }


        pdfMake.createPdf(docDefinition).open();
        //console.log(urlPDF);
    }

    $scope.printSaleSubPrice = function () {

        tableRows = [];
        var setData = $scope.salesubprice;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                OrderCode: value.OrderCode,
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                EmployeeName: value.EmployeeName,
                CustomerName: value.CustomerName,
                TotalPrice: $filter('currency')(value.TotalPrice, "", 0),
                DiscountPrice: $filter('currency')(value.DiscountPrice, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.OrderCode,
                item.CreatedAt,
                item.EmployeeName,
                item.CustomerName,
                item.TotalPrice,
                item.DiscountPrice,
            ];
        });

        var docDefinition = {

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo tổng hợp giảm giá theo hóa đơn', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hóa đơn', 'Thời gian', 'Nhân viên', 'Khách hàng', 'Giá trị HD', 'Giảm giá HD'],
                       [{ text: 'Số hóa đơn: ' + $scope.metadata.Count, colSpan: 5, alignment: 'left' }, {}, {}, {}, {},
                           $filter('currency')($scope.metadata.TotalPrice, "", 0), $filter('currency')($scope.metadata.TotalPriceReturn, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }


        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printSaleReturnProduct = function () {

        tableRows = [];
        var setData = $scope.salereturnproduct;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                OrderCode: value.OrderCode,
                CreatedAt: $filter('date')(value.CreatedAt, "dd/MM/yyyy"),
                CustomerName: value.CustomerName,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0),
                TotalPriceReturn: $filter('currency')(value.TotalPriceReturn, "", 0),
                Total: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.OrderCode,
                item.CreatedAt,
                item.CustomerName,
                item.TotalPriceSale,
                item.TotalPriceReturn,
                item.Total,
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo trả hàng theo hóa đơn', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã chứng từ', 'Thời gian', 'Khách hàng', 'Doanh thu', 'Giá trị trả', 'Doanh thu (thuần)'],
                       [{ text: 'Số hóa đơn: ' + $scope.metadata.Count, colSpan: 4, alignment: 'left' }, {}, {}, {},
                           $filter('currency')($scope.metadata.TotalPrice, "", 0), $filter('currency')($scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalPrice - $scope.metadata.TotalPriceReturn, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printSaleEmployees = function () {

        tableRows = [];
        var setData = $scope.saleemployees;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                EmployeeName: value.EmployeeName,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0),
                TotalPriceReturn: $filter('currency')(value.TotalPriceReturn, "", 0),
                Total: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.EmployeeName,
                item.TotalPriceSale,
                item.TotalPriceReturn,
                item.Total,
            ];
        });

        var docDefinition = {

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo bán hàng theo nhân viên', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Nhân viên', 'Doanh thu', 'Giá trị trả', 'Doanh thu (thuần)'],
                       [{ text: 'Số nhân viên: ' + $scope.metadata.Count, colSpan: 2, alignment: 'left' }, {},
                           $filter('currency')($scope.metadata.TotalPrice, "", 0), $filter('currency')($scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalPrice - $scope.metadata.TotalPriceReturn, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    //Khởi tạo biểu đồ
    $scope.chart = {};
    $scope.chart.options = {
        type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return $filter("currency")(datasetLabel, "", 0) + "đ"
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return $filter("currency")(value, "", 0) + "đ"
                    }
                }
            }]
        }
    }

}]);

myApp.controller('ReportOrderController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'Excel', '$timeout', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', '$location', function ShopController($scope, $http, config, ngDialog, md5, $window, Excel, $timeout, cfpLoadingBar, $filter, $mdDialog, $mdToast, $location) {

    $scope.today = new Date();
    $scope.page = 1;
    $scope.query = "2";
    $scope.datetimetext = "Tuần này";
    $scope.type_report = 2;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    //$scope.today = function () {
    //    $scope.dt = new Date();
    //};
    //$scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    //Khởi tạo
    $scope.chart = {};
    $scope.chart.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return datasetLabel//$filter("int")(datasetLabel, "", 0)
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return value//$filter("int")(value, "", 0)
                    }
                }
            }]
        }
    }

    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.reportOrderProduct();
        $scope.orderUrl = '/Reports/Order/orderProduct.html';
    }

    $scope.loadOrderPage = function () {
        if ($scope.page == 1) {
            if ($scope.type_report == 1) {
                $scope.reportOrderProductChart();
                $scope.orderUrlChart = '/Reports/Order/orderProductChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportOrderProduct();
                $scope.orderUrl = '/Reports/Order/orderProduct.html';
            }
        }
        else if ($scope.page == 2) {
            if ($scope.type_report == 1) {
                $scope.reportOrderExchangeChart();
                $scope.orderUrlChart = '/Reports/Order/orderExchangeChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportOrderExchange();
                $scope.orderUrl = '/Reports/Order/orderExchange.html';
            }
        }
    }

    //Kiểu hiển thị
    $scope.orderChart = function () {
        $scope.type_report = 1;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.addClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.page == 1) {
            $scope.reportOrderProductChart();
            $scope.orderUrlChart = '/Reports/Order/orderProductChart.html';
        }
        else if ($scope.page == 2) {
            $scope.reportOrderExchangeChart();
            $scope.orderUrlChart = '/Reports/Order/orderExchangeChart.html';
        }
    }

    $scope.orderDoc = function () {
        $scope.type_report = 2;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');
        cfpLoadingBar.start();
        if ($scope.page == 1) {
            $scope.reportOrderProduct();
            $scope.orderUrl = '/Reports/Order/orderProduct.html';
        }
        else if ($scope.page == 2) {
            $scope.reportOrderExchange();
            $scope.orderUrl = '/Reports/Order/orderExchange.html';
        }
    }

    //Mối quan tâm
    $scope.orderProduct = function () {
        $scope.page = 1;
        $scope.query = "2";
        var orderTime2 = angular.element(document.querySelector('#orderTime2'));
        orderTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.type_report == 1) {
            $scope.reportOrderProductChart();
            $scope.orderUrlChart = '/Reports/Order/orderProductChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportOrderProduct();
            $scope.orderUrl = '/Reports/Order/orderProduct.html';
        }
    }

    $scope.orderExchange = function () {
        $scope.page = 2;
        $scope.query = "2";
        cfpLoadingBar.start();
        var orderTime2 = angular.element(document.querySelector('#orderTime2'));
        orderTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        if ($scope.type_report == 1) {
            $scope.reportOrderExchangeChart();
            $scope.orderUrlChart = '/Reports/Order/orderExchangeChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportOrderExchange();
            $scope.orderUrl = '/Reports/Order/orderExchange.html';
        }
    }

    //Thời gian
    $scope.orderTime1 = function () {
        $scope.query = "1";
        $scope.datetimetext = "hôm nay";
        cfpLoadingBar.start();
        $scope.loadOrderPage();
    }

    $scope.orderTime2 = function () {
        $scope.query = "2";
        $scope.datetimetext = "tuần này";
        cfpLoadingBar.start();
        $scope.loadOrderPage();
    }

    $scope.orderTime3 = function () {
        $scope.query = "3";
        $scope.datetimetext = "tháng nay";
        cfpLoadingBar.start();
        $scope.loadOrderPage();
    }

    $scope.orderTime4 = function () {
        $scope.query = "4";
        $scope.datetimetext = "quý nay";
        cfpLoadingBar.start();
        $scope.loadOrderPage();
    }

    $scope.orderTime5 = function () {
        $scope.query = "5";
        $scope.datetimetext = "năm nay";
        cfpLoadingBar.start();
        $scope.loadOrderPage();
    }

    //Lấy dữ liệu
    $scope.reportOrderProduct = function () {
        $http.get("/api/app/reportOrderProduct?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.orderproduct = data.data;
                $scope.dateproduct = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportOrderProductChart = function () {
        $http.get("/api/app/reportOrderProductChart?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                //$scope.saletimeschart = data.data;
                //$scope.datetimechart = data.metadatadate;
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    console.log($scope.stats[i].ProductFullName + '-' + $scope.stats[i].Quantity);
                    $scope.chart.labels.push($scope.stats[i].ProductFullName);
                    $scope.chart.data.push($scope.stats[i].Quantity);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportOrderExchange = function () {
        $http.get("/api/app/reportOrderExchange?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.orderexchange = data.data;
                $scope.dateexchange = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportOrderExchangeChart = function () {
        $http.get("/api/app/reportOrderExchangeChart?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                //$scope.saletimeschart = data.data;
                //$scope.datetimechart = data.metadatadate;
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    console.log($scope.stats[i].CustomerName + '-' + $scope.stats[i].Quantity);
                    $scope.chart.labels.push($scope.stats[i].CustomerName);
                    $scope.chart.data.push($scope.stats[i].Quantity);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.exportAction = function (option) {
        switch (option) {
            case 'pdf': $scope.$broadcast('export-pdf', {});
                break;
            case 'excel': $scope.$broadcast('export-excel', {});
                break;
            case 'doc': $scope.$broadcast('export-doc', {});
                break;
            case 'csv': $scope.$broadcast('export-csv', {});
                break;
            default: console.log('no event caught');
        }
    }

}]);

myApp.controller('ReportProductController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'Excel', '$timeout', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', '$location', function ChartController($scope, $http, config, ngDialog, md5, $window, Excel, $timeout, cfpLoadingBar, $filter, $mdDialog, $mdToast, $location) {

    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.select_page = 1;
    $scope.typedate = 2;
    $scope.datetimetext = "tuần này";
    $scope.type_report = 2;

    $scope.isLoading = false;

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadProductPage();
    }

    $scope.onPageChange = function () {
        $scope.loadProductPage();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.StartDate != undefined) {
            var year = $scope.q.StartDate.getFullYear();
            var month = $scope.q.StartDate.getMonth();
            var date = $scope.q.StartDate.getDate();
            $scope.dt = $scope.q.StartDate;
            if (query != "") {
                query += " AND CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
            else {
                query += "CreatedAt >= DateTime(" + year + "," + (month + 1) + "," + date + ",0,0,0) AND CreatedAt <= DateTime(" + year + "," + (month + 1) + "," + date + ",23,59,59)";
            }
        }
        else {
            if (query == "")
                query = "1=1";
        }
        $scope.query = query;
        $scope.loadProductPage();
    }

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    $scope.init = function () {
        //init   
        cfpLoadingBar.start();
        $scope.reportProductSale();
        $scope.productUrl = '/Reports/Product/productSale.html';
    }

    $scope.loadProductPage = function () {
        if ($scope.select_page == 1) {
            if ($scope.type_report == 1) {
                $scope.reportProductSaleChart1();
                $scope.reportProductSaleChart2();
                $scope.productUrlChart = '/Reports/Product/productSaleChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportProductSale();
                $scope.productUrl = '/Reports/Product/productSale.html';
            }
        }
        else if ($scope.select_page == 2) {
            if ($scope.type_report == 1) {
                $scope.reportProductProfitChart1();
                $scope.reportProductProfitChart2();
                $scope.productUrlChart = '/Reports/Product/productProfitChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportProductProfit();
                $scope.productUrl = '/Reports/Product/productProfit.html';
            }
        }
        else if ($scope.select_page == 3) {
            $scope.reportProductExims();
            $scope.productUrl = '/Reports/Product/productExims.html';
        }
        else if ($scope.select_page == 4) {
            $scope.reportProductEximDel();
            $scope.productUrl = '/Reports/Product/productEximDel.html';
        }
        else if ($scope.select_page == 5) {
            $scope.reportProductEmployees();
            $scope.productUrl = '/Reports/Product/productEmployees.html';
        }
        else if ($scope.select_page == 6) {
            $scope.reportProductCustomer();
            $scope.productUrl = '/Reports/Product/productCustomer.html';
        }
        else if ($scope.select_page == 7) {
            $scope.reportProductVendor();
            $scope.productUrl = '/Reports/Product/productVendor.html';
        }
    }

    //Kiểu hiển thị
    $scope.productChart = function () {
        $scope.type_report = 1;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.addClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.select_page == 1) {
            $scope.reportProductSaleChart1();
            $scope.reportProductSaleChart2();
            $scope.productUrlChart = '/Reports/Product/productSaleChart.html';
        }
        else if ($scope.select_page == 2) {
            $scope.reportProductProfitChart1();
            $scope.reportProductProfitChart2();
            $scope.productUrlChart = '/Reports/Product/productProfitChart.html';
        }
    }

    $scope.productDoc = function () {
        $scope.type_report = 2;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');
        cfpLoadingBar.start();
        if ($scope.select_page == 1) {
            $scope.reportProductSale();
            $scope.productUrl = '/Reports/Product/productSale.html';
        }
        else if ($scope.select_page == 2) {
            $scope.reportProductProfit();
            $scope.productUrl = '/Reports/Product/productProfit.html';
        }
        else if ($scope.select_page == 3) {
            $scope.reportProductExims();
            $scope.productUrl = '/Reports/Product/productExim.html';
        }
        else if ($scope.select_page == 4) {
            $scope.reportProductEximDel();
            $scope.productUrl = '/Reports/Product/productEximDel.html';
        }
        else if ($scope.select_page == 5) {
            $scope.reportProductEmployees();
            $scope.productUrl = '/Reports/Product/productEmployees.html';
        }
        else if ($scope.select_page == 6) {
            $scope.reportProductCustomer();
            $scope.productUrl = '/Reports/Product/productCustomer.html';
        }
        else if ($scope.select_page == 7) {
            $scope.reportProductVendor();
            $scope.productUrl = '/Reports/Product/productVendor.html';
        }
    }

    //Mối quan tâm
    $scope.productSale = function () {
        $scope.select_page = 1;
        $scope.typedate = 2;
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.type_report == 1) {
            $scope.reportProductSaleChart1();
            $scope.reportProductSaleChart2();
            $scope.productUrlChart = '/Reports/Product/productSaleChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportProductSale();
            $scope.productUrl = '/Reports/Product/productSale.html';
        }
    }

    $scope.productProfit = function () {
        $scope.select_page = 2;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        if ($scope.type_report == 1) {
            $scope.reportProductProfitChart1();
            $scope.reportProductProfitChart2();
            $scope.productUrlChart = '/Reports/Product/productProfitChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportProductProfit();
            $scope.productUrl = '/Reports/Product/productProfit.html';
        }
    }

    $scope.productExims = function () {
        $scope.select_page = 3;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.reportProductExims();
        $scope.productUrl = '/Reports/Product/productExims.html';
    }

    $scope.productEximDel = function () {
        $scope.select_page = 4;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.reportProductEximDel();
        $scope.productUrl = '/Reports/Product/productEximDel.html';
    }

    $scope.productEmployees = function () {
        $scope.select_page = 5;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.reportProductEmployees();
        $scope.productUrl = '/Reports/Product/productEmployees.html';
    }

    $scope.productCustomer = function () {
        $scope.select_page = 6;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.reportProductCustomer();
        $scope.productUrl = '/Reports/Product/productCustomer.html';
    }

    $scope.productVendor = function () {
        $scope.select_page = 7;
        $scope.typedate = 2;
        cfpLoadingBar.start();
        var productTime2 = angular.element(document.querySelector('#productTime2'));
        productTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.addClass('hide');

        var typeReportDoc = angular.element(document.querySelector('#typeReportDoc'));
        typeReportDoc.prop('checked', true);
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');

        $scope.reportProductVendor();
        $scope.productUrl = '/Reports/Product/productVendor.html';
    }

    //Thời gian
    $scope.productTime1 = function () {
        $scope.typedate = 1;
        $scope.datetimetext = "hôm nay";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime11 = function () {
        $scope.typedate = 6;
        $scope.datetimetext = "hôm qua";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime2 = function () {
        $scope.typedate = 2;
        $scope.datetimetext = "tuần này";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime21 = function () {
        $scope.typedate = 7;
        $scope.datetimetext = "tuần trước";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime22 = function () {
        $scope.typedate = 8;
        $scope.datetimetext = "7 ngày qua";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime3 = function () {
        $scope.typedate = 3;
        $scope.datetimetext = "tháng này";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime31 = function () {
        $scope.typedate = 9;
        $scope.datetimetext = "tháng trước";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime4 = function () {
        $scope.typedate = 4;
        $scope.datetimetext = "quý này";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime41 = function () {
        $scope.typedate = 10;
        $scope.datetimetext = "quý trước";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime5 = function () {
        $scope.typedate = 5;
        $scope.datetimetext = "năm nay";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    $scope.productTime51 = function () {
        $scope.typedate = 11;
        $scope.datetimetext = "năm trước";
        cfpLoadingBar.start();
        $scope.loadProductPage();
    }

    //Lấy dữ liệu
    $scope.reportProductSale = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductSale?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.productsale = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductSaleChart1 = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductSaleChart1?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats1 = data.data;
                //get chart data
                $scope.chart1.labels = new Array();
                $scope.chart1.data = new Array();
                for (var i = 0; i < $scope.stats1.length; i++) {
                    $scope.chart1.labels.push($scope.stats1[i].ProductFullName);
                    $scope.chart1.data.push($scope.stats1[i].TotalPrice);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductSaleChart2 = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductSaleChart2?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats2 = data.data;
                //get chart data
                $scope.chart2.labels = new Array();
                $scope.chart2.data = new Array();
                for (var i = 0; i < $scope.stats2.length; i++) {
                    $scope.chart2.labels.push($scope.stats2[i].ProductFullName);
                    $scope.chart2.data.push($scope.stats2[i].Quantity);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductProfit = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductProfit?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.productprofit = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductProfitChart1 = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductProfitChart1?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats1 = data.data;
                //get chart data
                $scope.chart1.labels = new Array();
                $scope.chart1.data = new Array();
                for (var i = 0; i < $scope.stats1.length; i++) {
                    $scope.chart1.labels.push($scope.stats1[i].ProductFullName);
                    $scope.chart1.data.push($scope.stats1[i].TotalProfit);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductProfitChart2 = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductProfitChart2?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats2 = data.data;
                //get chart data
                $scope.chart2.labels = new Array();
                $scope.chart2.data = new Array();
                for (var i = 0; i < $scope.stats2.length; i++) {
                    //console.log($scope.stats[i].ProductFullName + '-' + $scope.stats[i].Quantity);
                    $scope.chart2.labels.push($scope.stats2[i].ProductFullName);
                    $scope.chart2.data.push($scope.stats2[i].Rates);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductExims = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductExims?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.productexims = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductEmployees = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductEmployees?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.productemployees = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductCustomer = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductCustomer?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.productcustomer = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportProductVendor = function () {
        $scope.formatDate();
        $http.get("/api/app/reportProductVendor?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.productvendor = data.data;
                $scope.metadata = data.metadata;
                $scope.datainfo = data.metadatainfo;
                $scope.item_count = data.metadata.Count;
                $scope.checkNull();
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.formatDate = function () {
        var query = "";
        var year = $scope.dt.getFullYear();
        var month = $scope.dt.getMonth();
        var date = $scope.dt.getDate();

        $scope.startdate = new Date();
        $scope.enddate = new Date();

        if ($scope.typedate == 1) {
            $scope.startdate = new Date();
            $scope.enddate = new Date();
        }
        else if ($scope.typedate == 2) {
            var dayOfWeek = $scope.startdate.getDay();
            var startDay = 0;
            if (dayOfWeek == 0) {
                startDay = 6;
            }
            else {
                startDay = dayOfWeek - 1;
            }

            $scope.startdate.setDate(date - startDay);
            $scope.enddate.setDate(date - startDay + 6);
        }
        else if ($scope.typedate == 3) {
            $scope.startdate.setDate(1);
            var monthStart = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth(), 1);
            var monthEnd = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth() + 1, 1);
            var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
            $scope.enddate.setDate(monthLength);
        }
        else if ($scope.typedate == 4) {
            if ($scope.startdate.getMonth() >= 0 && $scope.startdate.getMonth() <= 2) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 0, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 2, 31);
            }
            else if ($scope.startdate.getMonth() >= 3 && $scope.startdate.getMonth() <= 5) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 3, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 5, 30);
            }
            else if ($scope.startdate.getMonth() >= 6 && $scope.startdate.getMonth() <= 8) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 6, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 8, 30);
            }
            else if ($scope.startdate.getMonth() >= 9 && $scope.startdate.getMonth() <= 11) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 9, 1);
                $scope.enddate = new Date($scope.enddate.getFullYear(), 11, 31);
            }
        }
        else if ($scope.typedate == 5) {
            $scope.startdate = new Date($scope.startdate.getFullYear(), 0, 1);
            $scope.enddate = new Date($scope.startdate.getFullYear(), 11, 31);
        }
        else if ($scope.typedate == 6) {
            var date = new Date();
            $scope.startdate.setDate(date.getDate() - 1);
            $scope.enddate.setDate(date.getDate() - 1);
        }
        else if ($scope.typedate == 7) {
            //var date = new Date();
            $scope.startdate.setDate($scope.startdate.getDate() - 7);
            $scope.enddate.setDate($scope.enddate.getDate() - 7);
            var dayOfWeek = $scope.startdate.getDay();
            var startDay = 0;
            if (dayOfWeek == 0) {
                startDay = 6;
            }
            else {
                startDay = dayOfWeek - 1;
            }

            $scope.startdate.setDate($scope.startdate.getDate() - startDay);
            $scope.enddate.setDate($scope.enddate.getDate() - startDay + 6);
        }
        else if ($scope.typedate == 8) {
            var date = new Date();
            $scope.startdate.setDate(date.getDate() - 7);
        }
        else if ($scope.typedate == 9) {
            $scope.startdate.setMonth($scope.startdate.getMonth() - 1);
            $scope.enddate.setMonth($scope.enddate.getMonth() - 1);
            $scope.startdate.setDate(1);
            var monthStart = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth(), 1);
            var monthEnd = new Date($scope.startdate.getFullYear(), $scope.startdate.getMonth() + 1, 1);
            var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
            $scope.enddate.setDate(monthLength);
        }
        else if ($scope.typedate == 10) {
            $scope.startdate.setMonth($scope.startdate.getMonth() - 3);
            if ($scope.startdate.getMonth() >= 0 && $scope.startdate.getMonth() <= 2) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 0, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 2, 31);
            }
            else if ($scope.startdate.getMonth() >= 3 && $scope.startdate.getMonth() <= 5) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 3, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 5, 30);
            }
            else if ($scope.startdate.getMonth() >= 6 && $scope.startdate.getMonth() <= 8) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 6, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 8, 30);
            }
            else if ($scope.startdate.getMonth() >= 9 && $scope.startdate.getMonth() <= 11) {
                $scope.startdate = new Date($scope.startdate.getFullYear(), 9, 1);
                $scope.enddate = new Date($scope.startdate.getFullYear(), 11, 31);
            }
        }
        else if ($scope.typedate == 11) {
            $scope.startdate = new Date($scope.startdate.getFullYear() - 1, 0, 1);
            $scope.enddate = new Date($scope.enddate.getFullYear() - 1, 11, 31);
        }

        if (query != "") {
            query += " AND CreatedAt >= DateTime(" + $scope.startdate.getFullYear() + "," + ($scope.startdate.getMonth() + 1) + "," + $scope.startdate.getDate() + ",0,0,0) AND CreatedAt <= DateTime(" + $scope.enddate.getFullYear() + "," + ($scope.enddate.getMonth() + 1) + "," + $scope.enddate.getDate() + ",23,59,59)";
        }
        else {
            query += "CreatedAt >= DateTime(" + $scope.startdate.getFullYear() + "," + ($scope.startdate.getMonth() + 1) + "," + $scope.startdate.getDate() + ",0,0,0) AND CreatedAt <= DateTime(" + $scope.enddate.getFullYear() + "," + ($scope.enddate.getMonth() + 1) + "," + $scope.enddate.getDate() + ",23,59,59)";
        }
        $scope.query = query;
        console.log("query=" + $scope.query);
    }

    $scope.checkNull = function () {
        if ($scope.item_count == 0) {
            var trNull = angular.element(document.querySelector('#trNull'));
            trNull.removeClass('hide');
            var trMeta = angular.element(document.querySelector('#trMeta'));
            trMeta.addClass('hide');
        }
        else {
            var trNull = angular.element(document.querySelector('#trNull'));
            trNull.addClass('hide');
            var trMeta = angular.element(document.querySelector('#trMeta'));
            trMeta.removeClass('hide');
        }
    }

    //Tạo bản in
    $scope.printProduct = function () {
        if ($scope.select_page == 1) {
            $scope.printProductSale();
        }
        else if ($scope.select_page == 2) {
            $scope.printProductProfit();
        }
        else if ($scope.select_page == 3) {
            $scope.printProductExims();
        }
        else if ($scope.select_page == 4) {
            $scope.printProductEximDel();
        }
        else if ($scope.select_page == 5) {
            $scope.printProductEmployees();
        }
        else if ($scope.select_page == 6) {
            $scope.printProductCustomer();
        }
        else if ($scope.select_page == 7) {
            $scope.printProductVendor();
        }
    }

    $scope.printProductSale = function () {

        tableRows = [];
        var setData = $scope.productsale;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductCode: value.ProductCode,
                ProductFullName: value.ProductFullName,
                QuantitySale: value.QuantitySale,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0),
                QuantityReturn: value.QuantityReturn,
                TotalPriceReturn: $filter('currency')(value.TotalPriceReturn, "", 0),
                Total: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductCode,
                item.ProductFullName,
                item.QuantitySale,
                item.TotalPriceSale,
                item.QuantityReturn,
                item.TotalPriceReturn,
                item.Total,
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo bán hàng theo hàng hóa', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hàng', 'Tên hàng', 'SL Bán', 'Giá trị bán', 'SL Trả', 'Giá trị trả', 'Doanh thu (thuần)'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 3, alignment: 'left' }, {}, {},
                           $scope.metadata.TotalSale, $filter('currency')($scope.metadata.TotalPriceSale, "", 0),
                           $scope.metadata.TotalReturn, $filter('currency')($scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalPriceSale - $scope.metadata.TotalPriceReturn, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printProductProfit = function () {

        tableRows = [];
        var setData = $scope.productprofit;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductCode: value.ProductCode,
                ProductFullName: value.ProductFullName,
                QuantitySale: value.QuantitySale,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0),
                QuantityReturn: value.QuantityReturn,
                TotalPriceReturn: $filter('currency')(value.TotalPriceReturn, "", 0),
                Total: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn, "", 0),
                TotalOriginPrice: $filter('currency')(value.TotalOriginPrice, "", 0),
                TotalProfitPrice: $filter('currency')(value.TotalPriceSale - value.TotalPriceReturn - value.TotalOriginPrice, "", 0),
                TotalRate: $filter('number')((value.TotalPriceSale - value.TotalPriceReturn - value.TotalOriginPrice) * 100 / (value.TotalPriceSale - value.TotalPriceReturn), "", 0) + "%",
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductCode,
                item.ProductFullName,
                item.QuantitySale,
                item.TotalPriceSale,
                item.QuantityReturn,
                item.TotalPriceReturn,
                item.Total,
                item.TotalOriginPrice,
                item.TotalProfitPrice,
                item.TotalRate
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo bán hàng theo hàng hóa', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hàng', 'Tên hàng', 'SL Bán', 'Giá trị bán', 'SL Trả', 'Giá trị trả', 'Doanh thu (thuần)', 'Tổng giá vốn', 'Lợi nhuận', 'Tỷ xuất'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 3, alignment: 'left' }, {}, {},
                           $scope.metadata.TotalSale, $filter('currency')($scope.metadata.TotalPriceSale, "", 0),
                           $scope.metadata.TotalReturn, $filter('currency')($scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalPriceSale - $scope.metadata.TotalPriceReturn, "", 0),
                           $filter('currency')($scope.metadata.TotalOriginPrice, "", 0),
                           $filter('currency')($scope.metadata.TotalPriceSale - $scope.metadata.TotalPriceReturn - $scope.metadata.TotalOriginPrice, "", 0), { text: "%" },
                       ],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printProductExims = function () {

        tableRows = [];
        var setData = $scope.productexims;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductCode: value.ProductCode,
                ProductFullName: value.ProductFullName,
                QuantityStart: value.QuantityInportStart - value.QuantityExportStart,
                TotalPriceStart: $filter('currency')(value.TotalPriceInportStart - value.TotalPriceExportStart, "", 0),
                QuantityInport: value.QuantityInport,
                TotalPriceInport: $filter('currency')(value.TotalPriceInport, "", 0),
                QuantityExport: value.QuantityExport,
                TotalPriceExport: $filter('currency')(value.TotalPriceExport, "", 0),
                QuantityEnd: value.QuantityInportStart - value.QuantityExportStart + value.QuantityInport - value.QuantityExport,
                TotalPriceEnd: $filter('currency')(value.TotalPriceInportStart - value.TotalPriceExportStart + value.TotalPriceInport - value.TotalPriceExport, "", 0),
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductCode,
                item.ProductFullName,
                item.QuantityStart,
                item.TotalPriceStart,
                item.QuantityInport,
                item.TotalPriceInport,
                item.QuantityExport,
                item.TotalPriceExport,
                item.QuantityEnd,
                item.TotalPriceEnd,
            ];
        });

        var docDefinition = {

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo hàng hóa xuất nhập tồn', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hàng', 'Tên hàng', 'Tồn đầu kỳ', 'Giá trị đầu kỳ', 'SL Nhập', 'Giá trị nhập', 'SL Xuất', 'Giá trị xuất', 'Tồn cuối kỳ', 'Giá trị cuối kỳ'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 3, alignment: 'left' }, {}, {},
                           $scope.metadata.TotalStart, $filter('currency')($scope.metadata.TotalPriceStart, "", 0),
                           $scope.metadata.TotalInport, $filter('currency')($scope.metadata.TotalPriceInport, "", 0),
                           $scope.metadata.TotalExport, $filter('currency')($scope.metadata.TotalPriceExport, "", 0),
                           $scope.metadata.TotalEnd, $filter('currency')($scope.metadata.TotalPriceEnd, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printProductEmployees = function () {

        tableRows = [];
        var setData = $scope.productemployees;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductCode: value.ProductCode,
                ProductFullName: value.ProductFullName,
                SaleEmployeeName: value.SaleEmployeeName,
                QuantitySale: value.QuantitySale,
                TotalPriceSale: $filter('currency')(value.TotalPriceSale, "", 0)
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductCode,
                item.ProductFullName,
                item.SaleEmployeeName,
                item.QuantitySale,
                item.TotalPriceSale
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo danh sách nhân viên theo bán hàng', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hàng', 'Tên hàng', 'Tên nhân viên', 'SL Bán', 'Giá trị bán'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 4, alignment: 'left' }, {}, {}, {},
                       $scope.metadata.TotalProduct, $filter('currency')($scope.metadata.TotalPrice, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printProductCustomer = function () {

        tableRows = [];
        var setData = $scope.productcustomer;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductCode: value.ProductCode,
                ProductFullName: value.ProductFullName,
                CustomerName: value.CustomerName,
                QuantityBuy: value.QuantityBuy,
                TotalPriceBuy: $filter('currency')(value.TotalPriceBuy, "", 0)
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductCode,
                item.ProductFullName,
                item.CustomerName,
                item.QuantityBuy,
                item.TotalPriceBuy
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo danh sách khách hàng theo hàng bán', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hàng', 'Tên hàng', 'Tên khách hàng', 'SL Mua', 'Giá trị mua'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 4, alignment: 'left' }, {}, {}, {},
                           $scope.metadata.TotalProduct, $filter('currency')($scope.metadata.TotalPrice, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    $scope.printProductVendor = function () {

        tableRows = [];
        var setData = $scope.productvendor;
        angular.forEach(setData, function (value, key) {
            tableRows.push({
                ProductCode: value.ProductCode,
                ProductFullName: value.ProductFullName,
                VendorName: value.VendorName,
                QuantityProduct: value.QuantityProduct,
                TotalPrice: $filter('currency')(value.TotalPrice, "", 0)
            });
        });

        var items = tableRows.map(function (item) {
            return [
                (100 + tableRows.indexOf(item) + 1).toString().slice(-2) + '.',
                item.ProductCode,
                item.ProductFullName,
                item.VendorName,
                item.QuantityProduct,
                item.TotalPrice
            ];
        });

        var docDefinition = {
            //header: 'simptle tex',

            content: [
             { text: 'Ngày lập: ' + $filter('date')($scope.dt, "dd/MM/yyyy hh:mm:ss"), style: 'header' },
             { text: 'Báo cáo danh sách nhà cung cấp theo hàng nhập', style: 'headerTile' },
             { text: 'Từ ngày: ' + $filter('date')($scope.startdate, "dd/MM/yyyy") + ' đến ngày ' + $filter('date')($scope.enddate, "dd/MM/yyyy"), style: 'headerDate' },
             { text: 'Chi nhánh: ' + $scope.datainfo.branchName, style: ['header', 'headerBranch'] },
             {
                 style: 'tableExample',
                 table: {
                     body: [
                       ['STT', 'Mã hàng', 'Tên hàng', 'Tên NCC', 'SL Sản phẩm', 'Giá trị'],
                       [{ text: 'Số sản phẩm: ' + $scope.metadata.Count, colSpan: 4, alignment: 'left' }, {}, {}, {},
                           $scope.metadata.TotalProduct, $filter('currency')($scope.metadata.TotalPrice, "", 0)],
                     ].concat(items)
                 },
                 layout: {
                     fillColor: function (i, node) {
                         return (i === 0) ? '#337ab7' : (i === 1) ? '#f2eed6' : null;
                         //return (i === 0) ? '#5dba00' : null;
                     }
                 }
             }
            ],

            styles: {
                header: {
                    fontSize: 12,
                    italic: true,
                    alignment: 'left'
                },
                headerTile: {
                    fontSize: 20,
                    bold: true,
                    alignment: 'center'
                },
                headerDate: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                headerBranch: {
                    fontSize: 13,
                    italic: true,
                    alignment: 'center'
                },
                header_table: {
                    alignment: 'center',
                    fillColor: 'red',
                }
            },
        }

        pdfMake.createPdf(docDefinition).open();
    }

    //Cấu hình biểu đồ
    $scope.chart1 = {};
    $scope.chart1.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return $filter("currency")(datasetLabel, "", 0) + "đ"
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return $filter("currency")(value, "", 0) + "đ"
                    }
                }
            }]
        }
    }

    $scope.chart2 = {};
    $scope.chart2.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return datasetLabel//$filter("int")(datasetLabel, "", 0)
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return value//$filter("int")(value, "", 0)
                    }
                }
            }]
        }
    }

}]);

myApp.controller('ReportCustomerController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'Excel', '$timeout', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', '$location', function ChartController($scope, $http, config, ngDialog, md5, $window, Excel, $timeout, cfpLoadingBar, $filter, $mdDialog, $mdToast, $location) {

    $scope.today = new Date();
    $scope.page = 1;
    $scope.query = "2";
    $scope.datetimetext = "tuần này";
    $scope.type_report = 2;

    $scope.DOBformat = "dd/MM/yyyy";
    $scope.DatePopup = {
        startDate: false,
        endDate: false,
    };
    //$scope.today = function () {
    //    $scope.dt = new Date();
    //};
    //$scope.today();
    $scope.dateOptions = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minDate: new Date(1900, 1, 1),
        startingDay: 1
    };

    $scope.openDatePopup = function (i) {
        if (i == 0)
            $scope.DatePopup.startDate = true;
        else
            $scope.DatePopup.endDate = true;
    }

    //Khởi tạo
    $scope.chart1 = {};
    $scope.chart1.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return $filter("currency")(datasetLabel, "", 0) + "đ"
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return $filter("currency")(value, "", 0) + "đ"
                    }
                }
            }]
        }
    }

    $scope.chart2 = {};
    $scope.chart2.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return datasetLabel//$filter("int")(datasetLabel, "", 0)
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return value//$filter("int")(value, "", 0)
                    }
                }
            }]
        }
    }

    $scope.init = function () {
        //init    
        cfpLoadingBar.start();
        $scope.reportCustomerSale();
        $scope.productUrl = '/Reports/Customer/customerSale.html';
    }

    $scope.loadCustomerPage = function () {
        if ($scope.page == 1) {
            if ($scope.type_report == 1) {
                $scope.reportCustomerSaleChart();
                $scope.customerUrlChart = '/Reports/Customer/customerSaleChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportCustomerSale();
                $scope.customerUrl = '/Reports/Customer/customerSale.html';
            }
        }
        else if ($scope.page == 2) {
            $scope.reportCustomerProfit();
            $scope.customerUrl = '/Reports/Customer/customerProfit.html';
        }
        else if ($scope.page == 3) {
            if ($scope.type_report == 1) {
                $scope.reportCustomerDebtsChart();
                $scope.customerUrlChart = '/Reports/Customer/customerDebtsChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportCustomerDebts();
                $scope.customerUrl = '/Reports/Customer/customerDebts.html';
            }
        }
        else if ($scope.page == 4) {
            $scope.reportCustomerProduct();
            $scope.customerUrl = '/Reports/Customer/customerProduct.html';
        }
    }

    //Kiểu hiển thị
    $scope.customerChart = function () {
        $scope.type_report = 1;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.addClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.page == 1) {
            $scope.reportCustomerSaleChart();
            $scope.customerUrlChart = '/Reports/Customer/customerSaleChart.html';
        }
        else if ($scope.page == 3) {
            $scope.reportCustomerDebts();
            $scope.customerUrl = '/Reports/Customer/customerDebts.html';
        }
    }

    $scope.productDoc = function () {
        $scope.type_report = 2;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');
        cfpLoadingBar.start();
        if ($scope.page == 1) {
            $scope.reportCustomerSale();
            $scope.customerUrl = '/Reports/Customer/customerSale.html';
        }
        else if ($scope.page == 2) {
            $scope.reportCustomerProfit();
            $scope.customerUrl = '/Reports/Customer/customerProfit.html';
        }
        else if ($scope.page == 3) {
            $scope.reportCustomerDebts();
            $scope.customerUrl = '/Reports/Customer/customerDebts.html';
        }
        else if ($scope.page == 4) {
            $scope.reportCustomerProduct();
            $scope.customerUrl = '/Reports/Customer/customerProduct.html';
        }
    }

    //Mối quan tâm
    $scope.customerSale = function () {
        $scope.page = 1;
        $scope.query = "2";
        var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        customerTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.type_report == 1) {
            $scope.reportCustomerSaleChart();
            $scope.customerUrlChart = '/Reports/Customer/customerSaleChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportCustomerSale();
            $scope.customerUrl = '/Reports/Customer/customerSale.html';
        }
    }

    $scope.customerProfit = function () {
        $scope.page = 2;
        $scope.query = "2";
        cfpLoadingBar.start();
        var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        customerTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');

        $scope.reportCustomerProfit();
        $scope.customerUrl = '/Reports/Customer/customerProfit.html';
    }

    $scope.customerDebts = function () {
        $scope.page = 3;
        $scope.query = "2";
        cfpLoadingBar.start();
        var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        customerTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');

        if ($scope.type_report == 1) {
            $scope.reportCustomerDebtsChart();
            $scope.customerUrlChart = '/Reports/Customer/customerDebtsChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportCustomerDebts();
            $scope.customerUrl = '/Reports/Customer/customerDebts.html';
        }
    }

    $scope.customerProduct = function () {
        $scope.page = 4;
        $scope.query = "2";
        cfpLoadingBar.start();
        var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        customerTime2.prop('checked', true);
        var typeChart = angular.element(document.querySelector('#typeChart'));
        typeChart.removeClass('hide');

        $scope.reportCustomerProduct();
        $scope.customerUrl = '/Reports/Customer/customerProduct.html';
    }

    //Thời gian
    $scope.customerTime1 = function () {
        $scope.query = "1";
        $scope.datetimetext = "hôm nay";
        cfpLoadingBar.start();
        $scope.loadCustomerPage();
    }

    $scope.customerTime2 = function () {
        $scope.query = "2";
        $scope.datetimetext = "tuần này";
        cfpLoadingBar.start();
        $scope.loadCustomerPage();
    }

    $scope.customerTime3 = function () {
        $scope.query = "3";
        $scope.datetimetext = "tháng nay";
        cfpLoadingBar.start();
        $scope.loadCustomerPage();
    }

    $scope.customerTime4 = function () {
        $scope.query = "4";
        $scope.datetimetext = "quý nay";
        cfpLoadingBar.start();
        $scope.loadCustomerPage();
    }

    $scope.customerTime5 = function () {
        $scope.query = "5";
        $scope.datetimetext = "năm nay";
        cfpLoadingBar.start();
        $scope.loadCustomerPage();
    }

    //Lấy dữ liệu
    $scope.reportCustomerSale = function () {
        $http.get("/api/app/reportCustomerSale?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.customersale = data.data;
                $scope.datesale = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportCustomerSaleChart = function () {
        $http.get("/api/app/reportProductSaleChart1?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats1 = data.data;
                //get chart data
                $scope.chart1.labels = new Array();
                $scope.chart1.data = new Array();
                for (var i = 0; i < $scope.stats1.length; i++) {
                    //console.log($scope.stats1[i].ProductFullName + '-' + $scope.stats[i].Quantity);
                    $scope.chart1.labels.push($scope.stats1[i].ProductFullName);
                    $scope.chart1.data.push($scope.stats1[i].TotalPrice);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportCustomerProfit = function () {
        $http.get("/api/app/reportCustomerProfit?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.customerprofit = data.data;
                $scope.dateprofit = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportCustomerDebts = function () {
        $http.get("/api/app/reportCustomerDebts?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.customerdebts = data.data;
                $scope.datedebts = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportCustomerDebtsChart = function () {
        $http.get("/api/app/reportCustomerDebtsChart?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats1 = data.data;
                //get chart data
                $scope.chart1.labels = new Array();
                $scope.chart1.data = new Array();
                for (var i = 0; i < $scope.stats1.length; i++) {
                    $scope.chart1.labels.push($scope.stats1[i].ProductFullName);
                    $scope.chart1.data.push($scope.stats1[i].Profit);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportCustomerProduct = function () {
        $http.get("/api/app/reportCustomerProduct?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.customerproduct = data.data;
                $scope.dateproduct = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.exportAction = function (option) {
        switch (option) {
            case 'pdf': $scope.$broadcast('export-pdf', {});
                break;
            case 'excel': $scope.$broadcast('export-excel', {});
                break;
            case 'doc': $scope.$broadcast('export-doc', {});
                break;
            case 'csv': $scope.$broadcast('export-csv', {});
                break;
            default: console.log('no event caught');
        }
    }


}]);

myApp.controller('ReportFinanceController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'Excel', '$timeout', 'cfpLoadingBar', '$filter', '$mdDialog', '$mdToast', '$location', function ChartController($scope, $http, config, ngDialog, md5, $window, Excel, $timeout, cfpLoadingBar, $filter, $mdDialog, $mdToast, $location) {

    $scope.today = new Date();
    $scope.page = 1;
    $scope.query = "2";
    $scope.type_report = 1;
    $scope.select_page = 1;

    $scope.init = function () {
        //init    
        cfpLoadingBar.start();
        $scope.reportFinanceMonthChart();
        $scope.financeUrlChart = '/Reports/Finance/financeMonthChart.html';
    }

    $scope.loadCustomerPage = function () {
        if ($scope.page == 1) {
            if ($scope.type_report == 1) {
                $scope.reportCustomerSaleChart();
                $scope.customerUrlChart = '/Reports/Customer/customerSaleChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportCustomerSale();
                $scope.customerUrl = '/Reports/Customer/customerSale.html';
            }
        }
        else if ($scope.page == 2) {
            $scope.reportCustomerProfit();
            $scope.customerUrl = '/Reports/Customer/customerProfit.html';
        }
        else if ($scope.page == 3) {
            if ($scope.type_report == 1) {
                $scope.reportCustomerDebtsChart();
                $scope.customerUrlChart = '/Reports/Customer/customerDebtsChart.html';
            }
            else if ($scope.type_report == 2) {
                $scope.reportCustomerDebts();
                $scope.customerUrl = '/Reports/Customer/customerDebts.html';
            }
        }
        else if ($scope.page == 4) {
            $scope.reportCustomerProduct();
            $scope.customerUrl = '/Reports/Customer/customerProduct.html';
        }
    }

    //Kiểu hiển thị
    $scope.financeChart = function () {
        $scope.type_report = 1;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.addClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.removeClass('hide');
        cfpLoadingBar.start();
        if ($scope.select_page == 1) {
            $scope.reportFinanceMonthChart();
            $scope.financeUrlChart = '/Reports/Finance/financeMonthChart.html';
        }
        else if ($scope.select_page == 2) {
            $scope.reportFinanceQuarterChart();
            $scope.financeUrlChart = '/Reports/Finance/financeQuarterChart.html';
        }
        else if ($scope.select_page == 3) {
            $scope.reportFinanceYearChart();
            $scope.financeUrlChart = '/Reports/Finance/financeYearChart.html';
        }
    }

    $scope.financeDoc = function () {
        $scope.type_report = 2;
        var divDoc = angular.element(document.querySelector('#divDoc'));
        divDoc.removeClass('hide');
        var divChart = angular.element(document.querySelector('#divChart'));
        divChart.addClass('hide');
        cfpLoadingBar.start();
        if ($scope.select_page == 1) {
            $scope.reportFinanceMonth();
            $scope.financeUrl = '/Reports/Finance/financeMonth.html';
        }
        else if ($scope.select_page == 2) {
            $scope.reportFinanceQuarter();
            $scope.financeUrl = '/Reports/Finance/financeQuarter.html';
        }
        else if ($scope.select_page == 3) {
            $scope.reportFinanceYear();
            $scope.financeUrl = '/Reports/Finance/financeYear.html';
        }
    }

    //Mối quan tâm
    $scope.financeMonth = function () {
        $scope.select_page = 1;
        //var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        //customerTime2.prop('checked', true);
        //var typeChart = angular.element(document.querySelector('#typeChart'));
        //typeChart.removeClass('hide');

        cfpLoadingBar.start();
        if ($scope.type_report == 1) {
            $scope.reportFinanceMonthChart();
            $scope.financeUrlChart = '/Reports/Finance/financeMonthChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportFinanceMonth();
            $scope.financeUrl = '/Reports/Finance/financeMonth.html';
        }
    }

    $scope.financeQuarter = function () {
        $scope.select_page = 2;
        cfpLoadingBar.start();
        //var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        //customerTime2.prop('checked', true);
        //var typeChart = angular.element(document.querySelector('#typeChart'));
        //typeChart.removeClass('hide');

        if ($scope.type_report == 1) {
            $scope.reportFinanceQuarterChart();
            $scope.financeUrlChart = '/Reports/Finance/financeQuarterChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportFinanceQuarter();
            $scope.financeUrl = '/Reports/Finance/financeQuarter.html';
        }
    }

    $scope.financeYear = function () {
        $scope.select_page = 3;
        cfpLoadingBar.start();
        //var customerTime2 = angular.element(document.querySelector('#customerTime2'));
        //customerTime2.prop('checked', true);
        //var typeChart = angular.element(document.querySelector('#typeChart'));
        //typeChart.removeClass('hide');

        if ($scope.type_report == 1) {
            $scope.reportFinanceYearChart();
            $scope.financeUrlChart = '/Reports/Finance/financeYearChart.html';
        }
        else if ($scope.type_report == 2) {
            $scope.reportFinanceYear();
            $scope.financeUrl = '/Reports/Finance/financeYear.html';
        }
    }

    //Lấy dữ liệu
    $scope.reportFinanceMonth = function () {
        $http.get("/api/app/reportFinanceMonth?query=" + $scope.query).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.customersale = data.data;
                $scope.datesale = data.metadatadate;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportFinanceMonthChart = function () {
        $http.get("/api/app/reportFinanceMonthChart").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats = data.data;
                //get chart data
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    $scope.chart.labels.push("Tháng " + $scope.stats[i].Month);
                    $scope.chart.data.push($scope.stats[i].TotalProfit);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportFinanceQuarterChart = function () {
        $http.get("/api/app/reportFinanceQuarterChart").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats = data.data;
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    $scope.chart.labels.push("Quý " + $scope.stats[i].Quarter);
                    $scope.chart.data.push($scope.stats[i].TotalProfit);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.reportFinanceYearChart = function () {
        $http.get("/api/app/reportFinanceYearChart").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                $scope.stats = data.data;
                $scope.chart.labels = new Array();
                $scope.chart.data = new Array();
                for (var i = 0; i < $scope.stats.length; i++) {
                    $scope.chart.labels.push("Năm " + $scope.stats[i].Year);
                    $scope.chart.data.push($scope.stats[i].TotalProfit);
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    //Khởi tạo
    $scope.chart = {};
    $scope.chart.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return $filter("currency")(datasetLabel, "", 0) + "đ"
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return $filter("currency")(value, "", 0) + "đ"
                    }
                }
            }]
        }
    }

    $scope.chart2 = {};
    $scope.chart2.options = {
        //type: 'bar',
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.labels[tooltipItem.index];
                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    return datasetLabel//$filter("int")(datasetLabel, "", 0)
                }
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                ticks: {
                    callback: function (value, index, values) {
                        return value//$filter("int")(value, "", 0)
                    }
                }
            }]
        }
    }


}]);

myApp.controller('CMSadminController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function CMSadminController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = { "totaldebt": "0", "totalpaid": "0" };
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.init = function () {
        //init
        cfpLoadingBar.start();
        $scope.loadEmployeeusers();
        $scope.loadProvinces();
    }

    $scope.loadEmployeeusers = function () {
        $http.get("/api/app/GettUserPage?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                console.log(data.data);
                $scope.item_count = data.metadata.Count;
                $scope.employees = data.data;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    };

    
    $scope.getBranches = function (employee) {
        console.log(employee);
        var post = $http({
            method: 'POST'  ,
            url: "/api/app/getBranchesbyShopId",
            data: employee
        });

        post.success(function successCallback(data, status, headers, config) {
            
            if (data.meta.error_code == 200) {
                    $scope.branches = data.data;
            }
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadEmployeeusers();
    }

    $scope.onPageChange = function () {
        $scope.loadEmployeeusers();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.province != '-1') {
            query = "province.ProvinceId = " + $scope.q.province;
        }
        else {
            query = "1=1";
        }

        $scope.query = query;
        $scope.loadEmployeeusers();
    }

    $scope.onOrderChange = function () {

        if ($scope.lo.totaldebt != $scope.o.totaldebt) {
            if ($scope.o.totaldebt == 1)
                $scope.orderby = "TotalDebt desc";
            else
                $scope.orderby = "TotalDebt asc";
            //set lo
            $scope.o.totalpaid = "0";
            $scope.lo.totalpaid = "0";
            $scope.lo.totaldebt = $scope.o.totaldebt;
        }
        if ($scope.lo.totalpaid != $scope.o.totalpaid) {
            if ($scope.o.totalpaid == 1)
                $scope.orderby = "TotalPaid desc";
            else
                $scope.orderby = "TotalPaid asc";
            //set lo
            $scope.o.totaldebt = "0";
            $scope.lo.totaldebt = "0";
            $scope.lo.totalpaid = $scope.o.totalpaid;
        }
        $scope.loadEmployeeusers();
    }

    $scope.expandDetail = function (employ) {
        for (var i = 0; i < $scope.employees.length; i++) {
            if (employ.EmployeeId != $scope.employees[i].EmployeeId) {
                $scope.employees[i].isExpand = false;
            }
        }
        employ.isExpand = !employ.isExpand;
    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadDistricts = function (province) {
        if (!$scope.isEditing) {
            $scope.newVendor.Province.Name = province.Name;
            $scope.newVendor.Province.ProvinceId = province.ProvinceId;
            $scope.newVendor.District.Name = "Quận / Huyện";
            $scope.newVendor.District.DistrictId = -1;
        }
        else {
            $scope.editVendor.Province.Name = province.Name;
            $scope.editVendor.Province.ProvinceId = province.ProvinceId;
            $scope.editVendor.District.Name = "Quận / Huyện";
            $scope.editVendor.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newVendor.District.Name = district.Name;
            $scope.newVendor.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editVendor.District.Name = district.Name;
            $scope.editVendor.District.DistrictId = district.DistrictId;
        }
    }
    $scope.openEditEmployeeDialog = function (employee) {
        cfpLoadingBar.start();
        $scope.getBranches(employee);
        $scope.isEditing = true;
        $scope.editEmployee = angular.copy(employee);
        $scope.editEmployee.BranchId = $scope.editEmployee.BranchId.toString();
        $scope.originEmployeeCustomer = angular.copy(employee);
        //$scope.editEmployee.BranchId = $scope.editEmployee.branch.BranchId + "";
        angular.element(document.querySelector('#editEmployee')).modal({ backdrop: 'static', keyboard: true })
    }

    $scope.updateEmployee = function () {
        if (!$scope.frmEditEmployee.$valid) {
            return;
        }

        cfpLoadingBar.start();
        $scope.editEmployee.BranchId = parseInt($scope.editEmployee.BranchId);

        var post = $http({
            method: "POST",
            url: "/api/app/updateEmployeebyAdmin",
            data: $scope.editEmployee
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            cfpLoadingBar.complete();
            if (data.meta && data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    angular.element(document.querySelector('#editEmployee')).modal('toggle')
                    $mdToast.show($mdToast.simple()
                     .textContent('Cập nhật người dùng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                    $scope.editCustomer = {};
                    $scope.loadEmployeeusers();
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật nhân viên thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
            }
        })
                .error(function (data, status, headers, config) { // optional
                    cfpLoadingBar.complete();
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Cập nhật nhân viên thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );
                });
    }

    $scope.showConfirmEditStartus = function (employee) {
        var confirm = $mdDialog.confirm()
              .title('Thông báo')
              .textContent('Bạn có chắc muốn khóa người dùng ' + employee.EmployeeName + ' hay không?')
              .ok('Đồng ý!')
              .cancel('Hủy bỏ');

        $mdDialog.show(confirm).then(function () {
            if (employee.Status==0){
                $scope.lockEmployee(employee);
            } else {
                $scope.unLockEmployee(employee);
            }
        }, function () {
        });
    }

    $scope.lockEmployee = function (employee) {
        employee.Status=99;
        var post = $http({
            method: "POST",
            url: "/api/app/lockEmployee",
            data: employee
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta && data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.loadEmployeeusers();
                    $mdToast.show($mdToast.simple()
                     .textContent('Khóa người dùng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Khóa người dùng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Khóa người thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

                });
    }
    $scope.unLockEmployee = function (employee) {
        employee.Status = 0;
        var post = $http({
            method: "POST",
            url: "/api/app/unLockEmployee",
            data: employee
        });

        post.success(function successCallback(data, status, headers, config) {
            // success                
            if (data.meta.error_code == 200) {
                if (data.meta.error_code == 200) {
                    $scope.loadEmployeeusers();
                    $mdToast.show($mdToast.simple()
                     .textContent('Mở khóa người dùng thành công!')
                     .position('fixed bottom right')
                     .hideDelay(3000));
                }
            }
            else {
                $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mở khóa người dùng thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

            }
        })
                .error(function (data, status, headers, config) { // optional
                    $mdDialog.show(
                      $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Thông tin')
                        .textContent('Mở khóa thất bại, vui lòng thử lại sau!')
                        .ok('Đóng')
                        .fullscreen(true)
                    );

                });
    }

   
}]);

myApp.controller('ActionsController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function ActionsController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;
    $scope.query = "1=1";
    $scope.q = {};
    $scope.o = {};
    $scope.lo = { "totaldebt": "0", "totalpaid": "0" };
    $scope.orderby = "";
    $scope.item_count = 0;
    $scope.init = function () {
        //init
        $scope.loadActions();
        cfpLoadingBar.start();

    }

    $scope.loadActions = function () {
        $http.get("/api/app/getPageActions?page=" + $scope.page + "&page_size=" + $scope.page_size + "&query=" + $scope.query + "&order_by=" + $scope.orderby).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                console.log(data.data);
                $scope.item_count = data.metadata.Count;
                $scope.actions = data.data;
                $scope.metadata = data.metadata;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadActions();
    }

    $scope.onPageChange = function () {
        $scope.loadActions();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.province != '-1') {
            query = 'TargetType == "' + $scope.q.province + '"';
        }
        else {
            query = "1=1";
        }

        $scope.query = query;
        $scope.loadActions();
    }

    $scope.onOrderChange = function () {

        if ($scope.lo.totaldebt != $scope.o.totaldebt) {
            if ($scope.o.totaldebt == 1)
                $scope.orderby = "TotalDebt desc";
            else
                $scope.orderby = "TotalDebt asc";
            //set lo
            $scope.o.totalpaid = "0";
            $scope.lo.totalpaid = "0";
            $scope.lo.totaldebt = $scope.o.totaldebt;
        }
        if ($scope.lo.totalpaid != $scope.o.totalpaid) {
            if ($scope.o.totalpaid == 1)
                $scope.orderby = "TotalPaid desc";
            else
                $scope.orderby = "TotalPaid asc";
            //set lo
            $scope.o.totaldebt = "0";
            $scope.lo.totaldebt = "0";
            $scope.lo.totalpaid = $scope.o.totalpaid;
        }
        $scope.loadActions();
    }

    $scope.expandDetail = function (vendor) {
        for (var i = 0; i < $scope.vendors.length; i++) {
            if (vendor.VendorId != $scope.vendors[i].VendorId) {
                $scope.vendors[i].isExpand = false;
            }
        }
        vendor.isExpand = !vendor.isExpand;
    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadDistricts = function (province) {
        if (!$scope.isEditing) {
            $scope.newVendor.Province.Name = province.Name;
            $scope.newVendor.Province.ProvinceId = province.ProvinceId;
            $scope.newVendor.District.Name = "Quận / Huyện";
            $scope.newVendor.District.DistrictId = -1;
        }
        else {
            $scope.editVendor.Province.Name = province.Name;
            $scope.editVendor.Province.ProvinceId = province.ProvinceId;
            $scope.editVendor.District.Name = "Quận / Huyện";
            $scope.editVendor.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newVendor.District.Name = district.Name;
            $scope.newVendor.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editVendor.District.Name = district.Name;
            $scope.editVendor.District.DistrictId = district.DistrictId;
        }
    }

    $scope.findClass = function (action) {
        var c = "";
        if (action.TargetType == "ORDER")
            c = "hoadon";
        else if (action.TargetType == "IMPORT")
            c = "inport"
        else if (action.TargetType == "EXPORT")
            c = "export";
        else
            c = "edit";
        return c;
    }

    $scope.findIconClass = function (action) {
        var c = "";
        if (action.TargetType == "ORDER")
            c = "fa fa-file-text-o";
        else if (action.TargetType == "IMPORT")
            c = "fa fa-file-text-o"
        else if (action.TargetType == "EXPORT")
            c = "fa fa-file-text-o";
        else
            c = "fa fa-pencil-square-o";
        return c;
    }

    $scope.findTitle = function (action) {
        var title = "";
        if (action.TargetType == "ORDER")
            title = "HÓA ĐƠN"
        else if (action.TargetType == "IMPORT")
            title = "NHẬP HÀNG";
        else if (action.TargetType == "EXPORT")
            title = "XUẤT HÀNG";
        else if (action.TargetType == "PRODUCT" || action.TargetType == "PRODUCT_ATTRIBUTE")
            title = "SẢN PHẨM";
        else if (action.TargetType == "CUSTOMER")
            title = "KHÁCH HÀNG";
        else if (action.TargetType == "VENDOR")
            title = "NHÀ CUNG CẤP";
        else if (action.TargetType == "RETURN")
            title = "TRẢ HÀNG";
        else if (action.TargetType == "DELIVERY_VENDOR")
            title = "NHÀ CUNG CẤP VẬN CHUYỂN";
        else if (action.TargetType == "IMPORT_RETURN")
            title = "TRẢ HÀNG NHẬP";
        return title;
    }
    $scope.findMessage = function (action) {
        var a = "";
        if (action.target.Price != undefined && action.target.Price > 0)
            a = "với giá trị " + $filter('currency')(action.target.Price, "", 0) + "đ";
        else
            a = action.target.TargetCode;
        return a;
    }

    $scope.loadVendorCashes = function (vendor) {
        $http.get("/api/app/getVendorCashes?id=" + vendor.VendorId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.cashes = data.data;

                for (var i = 0; i < data.data.length; i++) {
                    vendor.TotalOrderPrice = vendor.TotalOrderPrice + vendor.cashes.Total;
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadVendorExims = function (vendor) {
        $http.get("/api/app/loadVendorExims?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.exims = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadCashFlows = function (vendor) {
        $http.get("/api/app/getVendorCashFlows?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                vendor.cashflows = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadVendorEximsProduct = function (vendor) {
        $http.get("/api/app/loadVendorEximsProduct?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.eximsproducts = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findType = function (status) {
        if (status == 0)
            return "Thanh toán";
        else if (status == 1)
            return "Bán hàng";
        else if (status == 2 || status == 4)
            return "Trả hàng";
        else if (status == 3)
            return "Nhập hàng";
        else if (status == 5)
            return "Hủy phiếu";
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.findPaymentStatus = function (cash) {
        if (cash.PaidAmount == 0)
            return "Chưa thanh toán";
        else if (cash.PaidAmount < cash.Total)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else
            return "Đã giao hàng"
    }
}]);

myApp.controller('DetailActionController', ['$scope', '$http', 'config', 'ngDialog', 'md5', '$window', 'cfpLoadingBar', '$mdDialog', '$mdToast', function DetailActionController($scope, $http, config, ngDialog, md5, $window, cfpLoadingBar, $mdDialog, $mdToast) {
    $scope.isEditing = false;
    $scope.page = 1;
    $scope.page_size = 20;

    $scope.detailaction = {};
    $scope.customer = {};
    $scope.exim = {};
    $scope.import_return = {};
    $scope.order = {};
    $scope.product = {};
    $scope.return = {};
    $scope.vendor = {};
    $scope.delivery_vendor = {};

    $scope.o = {};
    $scope.orderby = "";
    $scope.item_count = 0;

    $scope.init = function () {
        //init

        $scope.loadDetailActions();
        cfpLoadingBar.start();

    }

    $scope.loadDetailActions = function () {

        $http.get("/api/app/getDetailAction?Type=" + $scope.detailaction.type + "&ActionDetailId=" + $scope.detailaction.id).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {

                switch ($scope.detailaction.type) {
                    case "CUSTOMER":
                        $scope.customer = data.data;
                        $scope.customer.show = true;
                        break;
                    case "IMPORT":
                        console.log(data.data);
                        $scope.exim = data.data;
                        $scope.exim.show = true;
                        break;
                    case "RETURN":
                        console.log(data.data);
                        $scope.return = data.data;
                        $scope.return.show = true;
                        break;
                    case "IMPORT_RETURN":
                        console.log(data.data);
                        $scope.import_return = data.data;
                        $scope.import_return.show = true;
                        break;
                    case "ORDER":
                        console.log(data.data);
                        $scope.order = data.data;
                        $scope.order.show = true;
                        switch ($scope.order.OrderStatus) {
                            case 2:
                                $scope.order.OrderStatusname = "Đã hoàn thành";
                                break;
                            case -1:
                                $scope.order.OrderStatusname = "Đã Hủy";
                                break;
                            default:
                                $scope.order.OrderStatusname = "Chưa hoàn thành";
                                break;
                        }
                        break;
                    case "VENDOR":
                        console.log(data.data);
                        $scope.vendor = data.data;
                        $scope.vendor.show = true;
                        break;
                    case "DELIVERY_VENDOR":
                        console.log(data.data);
                        $scope.delivery_vendor = data.data;
                        $scope.delivery_vendor.show = true;
                        break;
                    case "PRODUCT_ATTRIBUTE":
                        console.log(data.data);
                        $scope.product = data.data;
                        $scope.product.show = true;
                        break;
                    default:
                        break;
                };

                $scope.meta = data.meta;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadExims = function (product) {
        $http.get("/api/app/getProductExims?id=" + product.ProductAttributeId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                console.log(data.data);
                product.exims = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.onPageSizeChange = function () {
        $scope.page = 1;
        $scope.loadActions();
    }

    $scope.onPageChange = function () {
        $scope.loadActions();
    }

    $scope.onQueryChange = function () {
        var query = "";
        if ($scope.q.province != '-1') {
            query = 'TargetType == "' + $scope.q.province + '"';
        }
        else {
            query = "1=1";
        }

        $scope.query = query;
        $scope.loadActions();
    }

    $scope.onOrderChange = function () {

        if ($scope.lo.totaldebt != $scope.o.totaldebt) {
            if ($scope.o.totaldebt == 1)
                $scope.orderby = "TotalDebt desc";
            else
                $scope.orderby = "TotalDebt asc";
            //set lo
            $scope.o.totalpaid = "0";
            $scope.lo.totalpaid = "0";
            $scope.lo.totaldebt = $scope.o.totaldebt;
        }
        if ($scope.lo.totalpaid != $scope.o.totalpaid) {
            if ($scope.o.totalpaid == 1)
                $scope.orderby = "TotalPaid desc";
            else
                $scope.orderby = "TotalPaid asc";
            //set lo
            $scope.o.totaldebt = "0";
            $scope.lo.totaldebt = "0";
            $scope.lo.totalpaid = $scope.o.totalpaid;
        }
        $scope.loadActions();
    }

    $scope.expandDetail = function (vendor) {
        for (var i = 0; i < $scope.vendors.length; i++) {
            if (vendor.VendorId != $scope.vendors[i].VendorId) {
                $scope.vendors[i].isExpand = false;
            }
        }
        vendor.isExpand = !vendor.isExpand;
    }

    $scope.loadProvinces = function () {
        $http.get("/api/app/getProvinces").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.provinces = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadDistricts = function (province) {
        if (!$scope.isEditing) {
            $scope.newVendor.Province.Name = province.Name;
            $scope.newVendor.Province.ProvinceId = province.ProvinceId;
            $scope.newVendor.District.Name = "Quận / Huyện";
            $scope.newVendor.District.DistrictId = -1;
        }
        else {
            $scope.editVendor.Province.Name = province.Name;
            $scope.editVendor.Province.ProvinceId = province.ProvinceId;
            $scope.editVendor.District.Name = "Quận / Huyện";
            $scope.editVendor.District.DistrictId = -1;
        }
        $http.get("/api/app/getDistricts?id=" + province.ProvinceId).success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                $scope.districts = data.data.Districts;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.selectDistrict = function (district) {
        if (!$scope.isEditing) {
            $scope.newVendor.District.Name = district.Name;
            $scope.newVendor.District.DistrictId = district.DistrictId;
        }
        else {
            $scope.editVendor.District.Name = district.Name;
            $scope.editVendor.District.DistrictId = district.DistrictId;
        }
    }

    $scope.findClass = function (action) {
        var c = "";
        if (action.TargetType == "ORDER")
            c = "hoadon";
        else if (action.TargetType == "IMPORT")
            c = "inport"
        else if (action.TargetType == "EXPORT")
            c = "export";
        else
            c = "edit";
        return c;
    }

    $scope.findIconClass = function (action) {
        var c = "";
        if (action.TargetType == "ORDER")
            c = "fa fa-file-text-o";
        else if (action.TargetType == "IMPORT")
            c = "fa fa-file-text-o"
        else if (action.TargetType == "EXPORT")
            c = "fa fa-file-text-o";
        else
            c = "fa fa-pencil-square-o";
        return c;
    }

    $scope.findTitle = function (action) {
        var title = "";
        if (action.TargetType == "ORDER")
            title = "HÓA ĐƠN"
        else if (action.TargetType == "IMPORT")
            title = "NHẬP HÀNG";
        else if (action.TargetType == "EXPORT")
            title = "XUẤT HÀNG";
        else if (action.TargetType == "PRODUCT" || action.TargetType == "PRODUCT_ATTRIBUTE")
            title = "SẢN PHẨM";
        else if (action.TargetType == "CUSTOMER")
            title = "KHÁCH HÀNG";
        else if (action.TargetType == "VENDOR")
            title = "NHÀ CUNG CẤP";
        else if (action.TargetType == "RETURN")
            title = "TRẢ HÀNG";
        else if (action.TargetType == "DELIVERY_VENDOR")
            title = "NHÀ CUNG CẤP VẬN CHUYỂN";
        else if (action.TargetType == "IMPORT_RETURN")
            title = "TRẢ HÀNG NHẬP";
        return title;
    }
    $scope.findMessage = function (action) {
        var a = "";
        if (action.target.Price != undefined && action.target.Price > 0)
            a = "với giá trị " + $filter('currency')(action.target.Price, "", 0) + "đ";
        else
            a = action.target.TargetCode;
        return a;
    }

    $scope.loadVendorCashes = function (vendor) {
        $http.get("/api/app/getVendorCashes?id=" + vendor.VendorId).success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.cashes = data.data;

                for (var i = 0; i < data.data.length; i++) {
                    vendor.TotalOrderPrice = vendor.TotalOrderPrice + vendor.cashes.Total;
                }
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadVendorExims = function (vendor) {
        $http.get("/api/app/loadVendorExims?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.exims = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.loadCashFlows = function (vendor) {
        $http.get("/api/app/getVendorCashFlows?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            if (data.meta.error_code == 200) {
                vendor.cashflows = data.data;
            }
        }).error(function (data, status, headers, config) {

        });
    }

    $scope.loadVendorEximsProduct = function (vendor) {
        $http.get("/api/app/loadVendorEximsProduct?id=" + vendor.VendorId + "&page=1&page_size=20").success(function (data, status, headers) {
            cfpLoadingBar.complete();
            if (data.meta.error_code == 200) {
                vendor.eximsproducts = data.data;
            }
        }).error(function (data, status, headers, config) {
            cfpLoadingBar.complete();
        });
    }

    $scope.findType = function (status) {
        if (status == 0)
            return "Thanh toán";
        else if (status == 1)
            return "Bán hàng";
        else if (status == 2 || status == 4)
            return "Trả hàng";
        else if (status == 3)
            return "Nhập hàng";
        else if (status == 5)
            return "Hủy phiếu";
    }

    $scope.findMethod = function (cash) {
        if (status == 0)
            return "Tiền mặt";
        else if (status == 1)
            return "Chuyển khoản";
    }

    $scope.findPaymentStatus = function (cash) {
        if (cash.PaidAmount == 0)
            return "Chưa thanh toán";
        else if (cash.PaidAmount < cash.Total)
            return "Chưa thanh toán hết";
        else
            return "Đã thanh toán"
    }

    $scope.findDeliveryStatus = function (status) {
        if (status == 0)
            return "Chưa giao hàng";
        else if (status == 1)
            return "Đang giao hàng";
        else
            return "Đã giao hàng"
    }
}]);










