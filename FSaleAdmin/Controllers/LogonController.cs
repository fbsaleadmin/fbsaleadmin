﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FSale.Models;

namespace FSale.Controllers
{
    [SessionAuthorizeAttribute]
    public class LogonController : Controller
    {
        // GET: Logon
        public ActionResult Index()
        {
            if (Session["Group"] != null)
            {
                //check if shop created
                if (Session["Group"].ToString() == "Admin")
                {
                    return View();
                    //return RedirectToRoute("RouteLinkPage"); 
                    //if (Session["NewShopCreated"] == null || !Boolean.Parse(Session["NewShopCreated"].ToString()))
                    //{
                    //    var result = new AppController().checkShopCreated();
                    //    try
                    //    {
                    //        JObject json = JObject.Parse(result);
                    //        try
                    //        {
                    //            JArray shops = (JArray)json["data"];
                    //            if (shops.Count > 0)
                    //                return View();
                    //            else
                    //            {
                    //                //aa
                    //                Session["isNewUser"] = true;
                    //                return RedirectToRoute("RouteCreateShop");
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Session["isNewUser"] = true;
                    //            return RedirectToRoute("RouteCreateShop");
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Session["isNewUser"] = true;
                    //        return RedirectToRoute("RouteCreateShop");
                    //    }
                    //}    
                    //else
                    //{
                    //    return View();
                    //}
                }
                else if (Session["Group"].ToString() == "Sale")
                {
                    return Redirect("https://chat.fb.sale");
                }
                else
                {                    
                    return RedirectToRoute("RouteShopHome");
                }
            }
            else
            {
                return RedirectToRoute("RouteHomeLogin");
            }
        }
    }
}