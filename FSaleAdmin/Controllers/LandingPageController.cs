﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using FSale.Models;
using Newtonsoft;
using Newtonsoft.Json;
using System.Configuration;
using FSale.Models;
using FSale.Models.Payment;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace FSale.Controllers
{
    public class LandingPageController : Controller
    {
        public ActionResult Index()
        {           
            //new AppController().token();
            //390808924586266//313601012371988
            bool isDebug = Boolean.Parse(ConfigurationManager.AppSettings["IsDebug"].ToString());
            if (isDebug)
                Session["FBAppId"] = "390808924586266";
            else
                Session["FBAppId"] = "313601012371988";          
            ViewBag.FBAppId = Session["FBAppId"].ToString();
            ViewBag.FacebookUserId = Session["FacebookUserId"] != null ? "'" + Session["FacebookUserId"].ToString() + "'" : "''";
            ViewBag.Group = Session["Group"] != null ? "'" + Session["Group"].ToString() + "'" : "''";
            ViewBag.IsLogged = Session["EmployeeId"] != null ? 1 : 0;
            return View();
        }        

        public ActionResult SelectPage()
        {
            ViewBag.FBAppId = Session["FBAppId"].ToString();
            ViewBag.FacebookUserId = Session["FacebookUserId"] != null ? "'" + Session["FacebookUserId"].ToString() + "'" : "''";
            ViewBag.Group = Session["Group"] != null ? "'" + Session["Group"].ToString() + "'" : "''";
            ViewBag.IsLogged = Session["EmployeeId"] != null ? 1 : 0;
            return View();
        }
    }
}