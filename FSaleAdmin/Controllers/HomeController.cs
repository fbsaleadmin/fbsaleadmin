﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FSale.Models;
using RestSharp;
using System.Net.Http;
using System.Threading.Tasks;

namespace FSale.Controllers
{

    public class HomeController : Controller
    {
        // GET: Home
        [SessionAuthorizeAttribute]
        public ActionResult Index()
        {
            ViewBag.ActiveMenu = 0;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Category()
        {
            ViewBag.ActiveMenu = 1;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Products()
        {
            ViewBag.ActiveMenu = 1;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Customers()
        {
            ViewBag.ActiveMenu = 3;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Prices()
        {
            ViewBag.ActiveMenu = 1;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Vendors()
        {
            ViewBag.ActiveMenu = 3;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult DeliveryVendors()
        {
            ViewBag.ActiveMenu = 3;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Imports()
        {
            ViewBag.ActiveMenu = 2;
            ViewBag.EmployeeId = Session["EmployeeId"].ToString();
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Cashes()
        {
            ViewBag.ActiveMenu = 4;
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult Debts()
        {
            ViewBag.ActiveMenu = 4;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Exims()
        {
            ViewBag.ActiveMenu = 2;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Orders()
        {
            ViewBag.ActiveMenu = 2;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Returns()
        {
            ViewBag.ActiveMenu = 2;
            ViewBag.EmployeeId = Session["EmployeeId"].ToString();
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Employees()
        {
            ViewBag.ActiveMenu = -1;
            ViewBag.GroupEployee = Session["Group"].ToString();
            return View();
        }

        //Add by Phung The Nhan
        [SessionAuthorizeAttribute]
        public ActionResult Actions()
        {
            ViewBag.ActiveMenu = 0;
            //ViewBag.EmployeeId = Session["EmployeeId"].ToString();
            //ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }
        [SessionAuthorizeAttribute]
        public ActionResult DetailAction(string type, int id)
        {
            ViewBag.ActiveMenu = 0;
            ViewBag.TypeAction = type;
            ViewBag.DetailActionId = id;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult CMSadmin()
        {
            ViewBag.ActiveMenu = 1;
            return View();
        }

        public ActionResult Logout()
        {
            Session["EmployeeId"] = null;
            Session["ShopId"] = null;
            Session["UserId"] = null;
            Session["EmployeeName"] = null;
            Session["BranchId"] = null;
            Session["FacebookUserToken"] = null;
            Session["access_token"] = null;
            Session["temp_Email"] = null;
            Session["temp_Password"] = null;
            Session["IsTokenValid"] = null;
            Session["isNewUser"] = null;
            Session["FacebookUserId"] = null;
            Session["Group"] = null;
            return Redirect(Request.Url.Scheme + "://" + Request.Url.Host);
        }

        [SessionAuthorizeAttribute]
        public ActionResult Sales()
        {
            ViewBag.EmployeeId = Session["EmployeeId"].ToString();
            ViewBag.BranchId = Session["BranchId"].ToString();
            ViewBag.ActiveMenu = 10;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Transfers()
        {
            ViewBag.ActiveMenu = 2;
            ViewBag.EmployeeId = Session["EmployeeId"].ToString();
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Branches()
        {
            ViewBag.ActiveMenu = -1;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult Pages()
        {
            ViewBag.Group = Session["Group"] != null ? "'" + Session["Group"].ToString() + "'" : "''";
            ViewBag.FacebookUserId = Session["FacebookUserId"] != null ? "'" + Session["FacebookUserId"].ToString() + "'" : "''";
            ViewBag.ActiveMenu = -1;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportEveryDay()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportSale()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportSales()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportCashes()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportOrder()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportProduct()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportCustomer()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportVendor()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportEmployees()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ReportFinance()
        {
            ViewBag.ActiveMenu = 5;
            ViewBag.BranchId = Session["BranchId"].ToString();
            return View();
        }

        public ActionResult Chat()
        {
            //
            DateTime now = DateTime.Now;
            int unix = (int)now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            string input = "fb.sale|" + Session["temp_Email"].ToString() + "|" + Session["temp_Password"].ToString() + "|" + unix;
            string hash = Security.Encrypt("fbsale@!321", input);
            hash = HttpUtility.UrlEncode(hash);
            return Redirect("http://chat.fb.sale?hash=" + hash + "&time=" + unix);
        }

        [SessionAuthorizeAttribute]
        public ActionResult CustomerReturns()
        {
            ViewBag.ActiveMenu = 2;
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult BranchTransfers()
        {
            ViewBag.ActiveMenu = 2;
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult ImportReturns(string code)
        {
            ViewBag.ActiveMenu = 2;
            ViewBag.EmployeeId = Session["EmployeeId"].ToString();
            ViewBag.BranchId = Session["BranchId"].ToString();
            ViewBag.EximCode = "'" + code + "'";
            return View();
        }

        [SessionAuthorizeAttribute]
        public ActionResult VendorReturns()
        {
            ViewBag.ActiveMenu = 2;
            return View();
        }


    }
}