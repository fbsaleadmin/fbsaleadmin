﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FSale.Controllers
{
    public class SharedController : Controller
    {
        public PartialViewResult BoxRecentAction()
        {
            return PartialView();
        }
        public PartialViewResult BoxLogin()
        {
            return PartialView();
        }

        public ActionResult ErrorPage()
        {
            return View();
        }
    }
}