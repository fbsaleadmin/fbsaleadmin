﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestSharp;
using System.Configuration;
using FSale.Models;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.SessionState;
using FSale.Data;
using log4net;
using System.Threading.Tasks;

namespace FSale.Controllers
{
    [RoutePrefix("api/app")]
    public class AppController : ApiController, IRequiresSessionState
    {
        private static readonly ILog log = LogMaster.GetLogger("fbsale", "fbsale");
        private static readonly bool isLocalDebug = Boolean.Parse(ConfigurationManager.AppSettings["IsDebug"].ToString());

        [HttpPost]
        [Route("RedirectToHome")]
        [ActionName("RedirectToHome")]
        public IHttpActionResult RedirectToHome(Shop shop)
        {
            HttpContext.Current.Session["ShopId"] = shop.ShopId;
            HttpContext.Current.Session["ShopName"] = shop.ShopName;
            //refresh token
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/security/token", Method.POST);
            request.AddParameter("grant_type", "password"); // adds to POST or URL querystring based on Method  
            request.AddParameter("userName", HttpContext.Current.Session["temp_Email"].ToString());
            request.AddParameter("password", HttpContext.Current.Session["temp_Password"].ToString());
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = client.Execute(request);
            var content = response.Content; // raw content as string
            TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(content);
            if (tokenResponse != null)
                if (tokenResponse.access_token != null)
                {
                    HttpContext.Current.Session["access_token"] = tokenResponse.access_token;
                }
            return Ok();
        }

        #region home
        public void token(LoginModel loginModel)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/security/token", Method.POST);
            request.AddParameter("grant_type", "password"); // adds to POST or URL querystring based on Method  
            request.AddParameter("userName", loginModel.email);
            request.AddParameter("password", loginModel.password);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(content);
            if (tokenResponse != null)
                if (tokenResponse.access_token != null)
                    HttpContext.Current.Session["access_token"] = tokenResponse.access_token;
        }

        [HttpGet]
        [Route("checkEmailExist")]
        [ActionName("checkEmailExist")]
        public IHttpActionResult checkEmailExist([FromUri]string email)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/users/CheckExistEmail", Method.GET);
            request.AddParameter("email", email);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("login")]
        [ActionName("login")]
        public IHttpActionResult login(LoginModel loginModel)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/login", Method.POST);
            request.AddJsonBody(loginModel);
            request.AddHeader("Content-Type", "application/json");
            //string author = "bearer ";
            //author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            //request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            //set session
            try
            {
                string UserId = json["data"]["UserId"].ToString();
                if (UserId == "3")
                {
                    HttpContext.Current.Session["AdminCMS"] = true;

                    HttpContext.Current.Session["UserId"] = UserId;
                    string EmployeeId = json["data"]["EmployeeId"].ToString();
                    HttpContext.Current.Session["EmployeeId"] = EmployeeId;
                    HttpContext.Current.Session["EmployeeName"] = json["data"]["EmployeeName"].ToString();
                    HttpContext.Current.Session["ShopId"] = json["data"]["ShopId"].ToString();
                    HttpContext.Current.Session["ShopName"] = json["data"]["ShopName"].ToString();
                    HttpContext.Current.Session["BranchId"] = json["data"]["BranchId"].ToString();
                    HttpContext.Current.Session["Group"] = json["data"]["Group"].ToString();
                    HttpContext.Current.Session["FacebookUserId"] = json["data"]["FacebookUserId"].ToString();
                    HttpContext.Current.Session["IsTokenValid"] = false;//aa
                    HttpContext.Current.Session["UserFacebookToken"] = null;
                    HttpContext.Current.Session["access_token"] = null;
                    client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
                    request = new RestRequest("api/security/token", Method.POST);
                    request.AddParameter("grant_type", "password"); // adds to POST or URL querystring based on Method  
                    request.AddParameter("userName", loginModel.email);
                    request.AddParameter("password", loginModel.password);
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    response = client.Execute(request);
                    content = response.Content; // raw content as string

                    TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(content);
                    if (tokenResponse != null)
                        if (tokenResponse.access_token != null)
                        {
                            HttpContext.Current.Session["access_token"] = tokenResponse.access_token;
                            //                      
                            HttpContext.Current.Session["temp_Email"] = loginModel.email;
                            HttpContext.Current.Session["temp_Password"] = loginModel.password;
                        }
                }
                else
                {
                    json = JObject.Parse("{'meta':{'error_code':404,'error_message':'Not Found'},'data':null,'metadata':null,'metadatainfo':null}");
                }
            }
            catch (Exception ex)
            { }
            return Ok(json);
        }

        [HttpPost]
        [Route("loginfacebook")]
        [ActionName("loginfacebook")]
        public IHttpActionResult loginfacebook(LoginFacebookModel loginModel)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/loginfacebook", Method.POST);
            request.AddJsonBody(loginModel);
            request.AddHeader("Content-Type", "application/json");
            //string author = "bearer ";
            //author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            //request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            //set session
            try
            {
                string UserId = json["data"]["UserId"].ToString();
                HttpContext.Current.Session["UserId"] = UserId;
                string EmployeeId = json["data"]["EmployeeId"].ToString();
                HttpContext.Current.Session["EmployeeId"] = EmployeeId;
                HttpContext.Current.Session["EmployeeName"] = json["data"]["EmployeeName"].ToString();
                HttpContext.Current.Session["ShopId"] = json["data"]["ShopId"].ToString();
                HttpContext.Current.Session["ShopName"] = json["data"]["ShopId"].ToString();
                HttpContext.Current.Session["BranchId"] = json["data"]["BranchId"].ToString();
                HttpContext.Current.Session["Group"] = json["data"]["Group"].ToString();
                HttpContext.Current.Session["UserFacebookToken"] = loginModel.token;
                HttpContext.Current.Session["FacebookUserId"] = json["data"]["FacebookUserId"].ToString();
                //token date
                //HttpContext.Current.Session["TokenSince"] = json["data"]["TokenSince"].ToString();
                DateTime tokenSince = DateTime.Parse(json["data"]["TokenSince"].ToString());
                //refresh token if date later then 14 days
                //get token
                client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
                request = new RestRequest("api/security/token", Method.POST);
                request.AddParameter("grant_type", "password"); // adds to POST or URL querystring based on Method  
                request.AddParameter("userName", json["data"]["Email"]);
                request.AddParameter("password", json["data"]["Password"]);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                response = client.Execute(request);
                content = response.Content; // raw content as string
                TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(content);
                if (tokenResponse != null)
                    if (tokenResponse.access_token != null)
                    {
                        HttpContext.Current.Session["access_token"] = tokenResponse.access_token;
                        HttpContext.Current.Session["UserFacebookToken"] = loginModel.token;
                        HttpContext.Current.Session["temp_Email"] = json["data"]["Email"];
                        HttpContext.Current.Session["temp_Password"] = json["data"]["Password"];
                        //update page token       
                        //check if need refresh token

                        if (loginModel.token != null && loginModel.token != "")
                        {
                            HttpContext.Current.Session["IsTokenValid"] = true;
                            if (DateTime.Now > tokenSince.AddDays(14))
                            {
                                //local
                                string urlLongLiveToken;
                                if (isLocalDebug)
                                    urlLongLiveToken = "/oauth/access_token?grant_type=fb_exchange_token&client_id=390808924586266&client_secret=ee73cef9a3c1b75027b0c93f0e9b994f&fb_exchange_token=" + HttpContext.Current.Session["UserFacebookToken"].ToString();
                                else
                                    urlLongLiveToken = "/oauth/access_token?grant_type=fb_exchange_token&client_id=313601012371988&client_secret=a993e6c800e78daf7f299f15e933b588&fb_exchange_token=" + HttpContext.Current.Session["UserFacebookToken"].ToString();
                                client = new RestClient("https://graph.facebook.com");
                                request = new RestRequest(urlLongLiveToken, Method.GET);
                                response = client.Execute(request);
                                content = response.Content; // raw content as string
                                //log.Info("login - long term user token :" + content);
                                string longLiveToken = "";
                                if (content != null && content.Length > 0)
                                {
                                    LongTermToken token = JsonConvert.DeserializeObject<LongTermToken>(content);
                                    longLiveToken = token.access_token;
                                }
                                //generate long live access token
                                client = new RestClient("https://graph.facebook.com");
                                request = new RestRequest("me/accounts/?access_token=" + longLiveToken, Method.GET);
                                response = client.Execute(request);
                                content = response.Content; // raw content as string
                                //log.Info("login - long term page token :" + content);
                                FBAccountResp resp = JsonConvert.DeserializeObject<FBAccountResp>(content);
                                if (resp != null)
                                {
                                    List<String> ids = new List<string>();
                                    List<String> tokens = new List<string>();
                                    if (resp.data != null && resp.data.Count > 0)
                                    {
                                        foreach (FBPage page in resp.data)
                                        {
                                            ids.Add(page.id);
                                            tokens.Add(page.access_token);
                                        }
                                    }
                                    UpdatePageTokenReq updatePage = new UpdatePageTokenReq();
                                    updatePage.ids = ids;
                                    updatePage.tokens = tokens;
                                    //update
                                    client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
                                    request = new RestRequest("api/pages/UpdatePageToken", Method.POST);
                                    request.AddHeader("Content-Type", "application/json");
                                    string author = "bearer ";
                                    author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
                                    request.AddHeader("Authorization", author);
                                    request.AddJsonBody(updatePage);
                                    response = client.Execute(request);
                                    content = response.Content;
                                    log.Info("login - long term page token update result:" + content);
                                }
                            }
                        }
                        else
                        {
                            HttpContext.Current.Session["IsTokenValid"] = false;
                        }
                    }
            }
            catch (Exception ex)
            { }
            return Ok(json);
        }

        //[HttpPost]
        //[Route("register")]
        //[ActionName("register")]
        //public IHttpActionResult register(UserDTO userDTO)
        //{
        //    RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
        //    var request = new RestRequest("api/users", Method.POST);
        //    userDTO.Password = userDTO.Password.ToLower();
        //    request.AddJsonBody(userDTO);
        //    request.AddHeader("Content-Type", "application/json");
        //    //string author = "bearer ";
        //    //author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
        //    //request.AddHeader("Authorization", author);
        //    IRestResponse response = client.Execute(request);
        //    var content = response.Content; // raw content as string
        //    log.Info("create user :" + content);
        //    JObject json = JObject.Parse(content);
        //    try
        //    {
        //        HttpContext.Current.Session["isNewUser"] = true;
        //        HttpContext.Current.Session["UserId"] = json["metadata"]["item_count"].ToString();
        //        HttpContext.Current.Session["EmployeeId"] = json["data"]["EmployeeId"].ToString(); ;
        //        HttpContext.Current.Session["EmployeeName"] = json["data"]["EmployeeName"].ToString();
        //        HttpContext.Current.Session["ShopId"] = json["data"]["ShopId"].ToString();
        //        HttpContext.Current.Session["Group"] = json["data"]["Group"].ToString();
        //        //get access token
        //        client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
        //        request = new RestRequest("api/security/token", Method.POST);
        //        request.AddParameter("grant_type", "password"); // adds to POST or URL querystring based on Method  
        //        request.AddParameter("userName", userDTO.Email);
        //        request.AddParameter("password", userDTO.Password);
        //        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        //        response = client.Execute(request);
        //        content = response.Content; // raw content as string            
        //        TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(content);
        //        if (tokenResponse != null)
        //            if (tokenResponse.access_token != null)
        //            {
        //                HttpContext.Current.Session["access_token"] = tokenResponse.access_token;
        //                //log.Info("access_token :" + tokenResponse.access_token);
        //                HttpContext.Current.Session["temp_Email"] = userDTO.Email;
        //                HttpContext.Current.Session["temp_Password"] = userDTO.Password;
        //            }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Info(ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace);
        //    }
        //    return Ok(json);
        //}

        //public string checkShopCreated()
        //{
        //    RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
        //    string UserId = HttpContext.Current.Session["UserId"] != null ? HttpContext.Current.Session["UserId"].ToString() : "0";
        //    var request = new RestRequest("api/users/" + UserId + "/shops", Method.GET);
        //    string author = "bearer ";
        //    author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
        //    request.AddHeader("Authorization", author);
        //    IRestResponse response = client.Execute(request);
        //    var content = response.Content; // raw content as string
        //    log.Info("exist " + content);
        //    return content;
        //}

        [HttpPost]
        [Route("downloadContent")]
        [ActionName("downloadContent")]
        public IHttpActionResult downloadContent(LoadPageContentReq req)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/facebook/loadPageContent", Method.POST);
            request.AddJsonBody(req);
            request.AddHeader("Content-Type", "application/json");
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("checkUniqueName")]
        [ActionName("checkUniqueName")]
        public IHttpActionResult checkUniqueName([FromUri]string name)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/CheckExistUniqueName", Method.GET);
            request.AddParameter("name", name);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createShop")]
        [ActionName("createShop")]
        public IHttpActionResult createShop(ShopDTO shop)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops", Method.POST);
            shop.ShopOrderPrefix = "DH";
            shop.ShopProductPrefix = "SP";
            shop.ShopCustomerPrefix = "KH";
            shop.UserId = Int32.Parse(HttpContext.Current.Session["UserId"].ToString());
            request.AddJsonBody(shop);
            request.AddHeader("Content-Type", "application/json");
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            //update
            try
            {
                string ShopId = json["data"]["ShopId"].ToString();
                string status = json["meta"]["error_code"].ToString();
                if (status == "200")
                    HttpContext.Current.Session["NewShopCreated"] = true;
                HttpContext.Current.Session["ShopId"] = ShopId;
            }
            catch (Exception ex) { }
            return Ok(json);
        }

        [HttpGet]
        [Route("getShopList")]
        [ActionName("getShopList")]
        public IHttpActionResult getShopList()
        {
            string UserId = HttpContext.Current.Session["UserId"] != null ? HttpContext.Current.Session["UserId"].ToString() : "0";
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/users/" + UserId + "/shops", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getShopStats")]
        [ActionName("getShopStats")]
        public IHttpActionResult getShopStats(int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + id + "/stats", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getShopDetailStats")]
        [ActionName("getShopDetailStats")]
        public IHttpActionResult getShopDetailStats()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/detailstats", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updateToken")]
        [ActionName("updateToken")]
        public IHttpActionResult updateToken(LoginFacebookModel loginModel)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/users/" + HttpContext.Current.Session["UserId"].ToString() + "/updateToken", Method.PUT);
            request.AddJsonBody(loginModel);
            request.AddHeader("Content-Type", "application/json");
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            //string author = "bearer ";
            //author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            //request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            HttpContext.Current.Session["UserFacebookToken"] = loginModel.token;
            return Ok(json);
        }
        #endregion

        #region category

        [HttpPost]
        [Route("createCategory")]
        [ActionName("createCategory")]
        public IHttpActionResult createCategory(CategoryDTO category)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/categories", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);

            category.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            category.Status = 0;
            category.CategoryDesc = category.CategoryName;
            category.AvailableBranchIds = "";

            request.AddJsonBody(category);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("editCategory")]
        [ActionName("editCategory")]
        public IHttpActionResult editCategory(CategoryDTO category)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/categories/" + category.CategoryId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            category.Status = 0;
            category.CategoryDesc = category.CategoryName;
            category.CategoryParentId = category.CategoryParentId == null ? -1 : category.CategoryParentId;
            category.AvailableBranchIds = "";
            request.AddJsonBody(category);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteCategory/{id}")]
        [ActionName("deleteCategory")]
        public IHttpActionResult deleteCategory(int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/categories/" + id, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getCategories")]
        [ActionName("getCategories")]
        public IHttpActionResult getCategories()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/categories", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region dictionary
        [HttpGet]
        [Route("getProvinces")]
        [ActionName("getProvinces")]
        public IHttpActionResult getProvinces()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/provinces", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getDistricts")]
        [ActionName("getDistricts/{id}")]
        public IHttpActionResult getProvinces([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/provinces/" + id, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }


        [HttpGet]
        [Route("getActions")]
        [ActionName("getActions")]
        public IHttpActionResult getActions()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/actions", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getPageActions")]
        [ActionName("getPageActions")]
        public IHttpActionResult getPageActions([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/pageactions?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/pageactions?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);// new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/pageactions", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getDetailAction")]
        [ActionName("getDetailAction")]
        public IHttpActionResult getDetailAction(string Type, int ActionDetailId)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            switch (Type)
            {
                case "CUSTOMER":
                    url = "api/customers/GetCustomerAction?id=" + ActionDetailId;
                    break;
                case "IMPORT":
                    url = "api/actions/geteximsaction?id=" + ActionDetailId;
                    break;
                case "RETURN":
                    url = "api/actions/getcusreturnsaction?id=" + ActionDetailId;
                    break;
                case "IMPORT_RETURN":
                    url = "api/actions/getimport_returnaction?id=" + ActionDetailId;
                    break;
                case "ORDER":
                    url = "api/actions/getorderaction?id=" + ActionDetailId;
                    break;
                case "VENDOR":
                    url = "api/actions/getvendoraction?id=" + ActionDetailId;
                    break;
                case "DELIVERY_VENDOR":
                    url = "api/actions/getdeliveryvendoraction?id=" + ActionDetailId;
                    break;
                case "PRODUCT_ATTRIBUTE":
                    url = "api/actions/getproductaction?id=" + ActionDetailId;
                    break;
                default:
                    break;
            }

            var request = new RestRequest(url, Method.GET);// new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/pageactions", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region manufacture

        [HttpGet]
        [Route("getManufactures")]
        [ActionName("getManufactures")]
        public IHttpActionResult getManufactures()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/manufactures?page=1&page_size=1000", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createManufacture")]
        [ActionName("createManufacture")]
        public IHttpActionResult createManufacture(ManufactureDTO manufacture)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/manufactures", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            manufacture.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            request.AddJsonBody(manufacture);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region attribute

        [HttpGet]
        [Route("getAttributes")]
        [ActionName("getAttributes")]
        public IHttpActionResult getAttributes()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/attributes?page=1&page_size=1000", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createAttribute")]
        [ActionName("createAttribute")]
        public IHttpActionResult createAttribute(AttributeDTO attr)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/attributes", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            attr.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            attr.IsCustom = true;
            attr.CreatedAt = DateTime.Now;
            attr.UpdatedAt = DateTime.Now;
            request.AddJsonBody(attr);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region product
        [HttpGet]
        [Route("getProducts")]
        [ActionName("getProducts")]
        public IHttpActionResult getProducts([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/products?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/products?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getProductCode")]
        [ActionName("getProductCode")]
        public IHttpActionResult getProductCode()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/getProductCode/" + HttpContext.Current.Session["ShopId"].ToString(), Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //api/shops/{id}/createProduct
        [HttpPost]
        [Route("createProduct")]
        [ActionName("createProduct")]
        public IHttpActionResult createProduct(CreateProduct createProduct)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/products", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            createProduct.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(createProduct);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getProductImport")]
        [ActionName("getProductImport")]
        public IHttpActionResult getProductImport([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + id + "/imports?page=1&page_size=15", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getProductExims")]
        [ActionName("getProductExims")]
        public IHttpActionResult getProductExims([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + id + "/exims?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getProductOrder")]
        [ActionName("getProductOrder")]
        public IHttpActionResult getProductOrder([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + id + "/orders?page=1&page_size=5", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpGet]
        [Route("getPAAttributes")]
        [ActionName("getPAAttributes")]
        public IHttpActionResult getPAAttributes([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + id + "/attributes", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updateProduct")]
        [ActionName("updateProduct")]
        public IHttpActionResult updateProduct(UpdateProduct updateProduct)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/products/" + updateProduct.ProductAttributeId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            updateProduct.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(updateProduct);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("stopProduct")]
        [ActionName("stopProduct")]
        public IHttpActionResult stopProduct(ProductAttributeDTO product)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + product.ProductAttributeId + "/notallow", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteProduct")]
        [ActionName("deleteProduct")]
        public IHttpActionResult deleteProduct(ProductAttributeDTO product)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + product.ProductAttributeId, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //[HttpGet]
        //[Route("exportExcel")]
        //public IHttpActionResult exportExcel()
        //{
        //    RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
        //    var request = new RestRequest("api/export/" + HttpContext.Current.Session["ShopId"].ToString() + "/product", Method.GET);
        //    string author = "bearer ";
        //    author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
        //    request.AddHeader("Authorization", author);
        //    IRestResponse response = client.Execute(request);
        //    var content = response.Content; // raw content as string
        //    //JObject json = JObject.Parse(content);
        //    return Ok();
        //}

        #endregion

        #region customer

        [HttpPost]
        [Route("createCustomerGroup")]
        [ActionName("createCustomerGroup")]
        public IHttpActionResult createCustomerGroup(CustomerGroupDTO group)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customergroups", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            group.CreateAt = DateTime.Now;
            group.Description = group.Name;
            group.Status = 0;
            group.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            request.AddJsonBody(group);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getCustomerGroups")]
        [ActionName("getCustomerGroups")]
        public IHttpActionResult getCustomerGroups()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customergroups/GetByPage?page=1&page_size=100", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //api/customers/getCustomerCode/
        [HttpGet]
        [Route("getCustomerCode")]
        [ActionName("getCustomerCode")]
        public IHttpActionResult getCustomerCode()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers/getCustomerCode", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createCustomer")]
        [ActionName("createCustomer")]
        public IHttpActionResult createCustomer(CustomerDTO customer)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            customer.CreatedAt = DateTime.Now;
            //if (customer.CustomerGroupId == -1)
            //    customer.CustomerGroupId = null;
            //check date
            if (customer.DOB != null)
            {
                if (customer.DOB.Value.Year == 1970 && customer.DOB.Value.Month == 0 && customer.DOB.Value.Day == 1)
                    customer.DOB = null;
            }
            customer.Tag = "";
            customer.UpdatedAt = DateTime.Now;
            customer.Status = 0;
            customer.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            customer.Level = 0;
            request.AddJsonBody(customer);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getCustomers")]
        [ActionName("getCustomers")]
        public IHttpActionResult getCustomers([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by != null && order_by.Trim() != "")
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/customers?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            else
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/customers?page=" + page + "&page_size=" + page_size + "&query=" + query;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("editCustomer")]
        [ActionName("editCustomer")]
        public IHttpActionResult editCustomer(CustomerDTO customer)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers/" + customer.CustomerId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            if (customer.CustomerGroupId == null)
                customer.CustomerGroupId = -1;
            request.AddJsonBody(customer);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteCustomer")]
        [ActionName("deleteCustomer")]
        public IHttpActionResult deleteCustomer(CustomerDTO customer)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers/" + customer.CustomerId, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getCustomerOrder")]
        [ActionName("getCustomerOrder")]
        public IHttpActionResult getCustomerOrder([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers/" + id + "/orders?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getCustomerCashFlows")]
        [ActionName("getCustomerCashFlows")]
        public IHttpActionResult getCustomerCashFlows([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers/" + id + "/cashflows?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("loadCustomerEximsProduct")]
        [ActionName("loadCustomerEximsProduct")]
        public IHttpActionResult loadCustomerEximsProduct([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/customers/" + id + "/eximproduct?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createCashFromCustomer")]
        [ActionName("createCashFromCustomer")]
        public IHttpActionResult createCashFromCustomer(CreateCash cash)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/cashes/" + HttpContext.Current.Session["ShopId"].ToString() + "/createcash", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            cash.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(cash);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region product price

        [HttpGet]
        [Route("getProductPrices")]
        [ActionName("getProductPrices")]
        public IHttpActionResult getProductPrices([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/productprices?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/productprices?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getProductPricesImport")]
        [ActionName("getProductPricesImport")]
        public IHttpActionResult getProductPricesImport([FromUri] int id, [FromUri] int uid)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productattributes/" + id + "/" + uid + "/prices-imports?page=1&page_size=5", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updatePrice")]
        [ActionName("updatePrice")]
        public IHttpActionResult updatePrice(UpdateProductPrice price)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/productprices/UpdateNewPrice", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            price.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(price);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region vendor

        [HttpGet]
        [Route("getVendors")]
        [ActionName("getVendors")]
        public IHttpActionResult getVendors([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by != null && order_by.Trim() != "")
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/vendors?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            else
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/vendors?page=" + page + "&page_size=" + page_size + "&query=" + query;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getVendorCode")]
        [ActionName("getVendorCode")]
        public IHttpActionResult getVendorCode()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/getVendorCode", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createVendor")]
        [ActionName("createVendor")]
        public IHttpActionResult createVendor(VendorDTO vendor)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            vendor.CreatedAt = DateTime.Now;
            vendor.UpdatedAt = DateTime.Now;
            vendor.Status = 0;
            vendor.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            vendor.SearchQuery = "";
            if (vendor.Address == null)
                vendor.Address = "";
            if (vendor.DistrictId == null)
                vendor.DistrictId = -1;
            if (vendor.ProvinceId == null)
                vendor.ProvinceId = -1;
            request.AddJsonBody(vendor);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("editVendor")]
        [ActionName("editVendor")]
        public IHttpActionResult editVendor(VendorDTO vendor)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/" + vendor.VendorId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(vendor);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteVendor")]
        [ActionName("deleteVendor")]
        public IHttpActionResult deleteVendor(VendorDTO vendor)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/" + vendor.VendorId, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getVendorCashes")]
        [ActionName("getVendorCashes")]
        public IHttpActionResult getVendorCashes([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/" + id + "/cashes?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("loadVendorExims")]
        [ActionName("loadVendorExims")]
        public IHttpActionResult loadVendorExims([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/" + id + "/exims?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("loadVendorEximsProduct")]
        [ActionName("loadVendorEximsProduct")]
        public IHttpActionResult loadVendorEximsProduct([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/" + id + "/eximproduct?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getVendorCashFlows")]
        [ActionName("getVendorCashFlows")]
        public IHttpActionResult getVendorCashFlows([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/vendors/" + id + "/cashflows?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region delivery vendor

        [HttpGet]
        [Route("getDeliveryVendors")]
        [ActionName("getDeliveryVendors")]
        public IHttpActionResult getDeliveryVendors([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by != null && order_by.Trim() != "")
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/deliveryvendors?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            else
                url = "api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/deliveryvendors?page=" + page + "&page_size=" + page_size + "&query=" + query;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getDeliveryVendorCode")]
        [ActionName("getDeliveryVendorCode")]
        public IHttpActionResult getDeliveryVendorCode()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/deliveryvendors/getVendorCode", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createDeliveryVendor")]
        [ActionName("createDeliveryVendor")]
        public IHttpActionResult createDeliveryVendor(DeliveryVendorDTO vendor)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/deliveryvendors", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            vendor.CreatedAt = DateTime.Now;
            vendor.UpdatedAt = DateTime.Now;
            vendor.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            request.AddJsonBody(vendor);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("editDeliveryVendor")]
        [ActionName("editDeliveryVendor")]
        public IHttpActionResult editDeliveryVendor(DeliveryVendorDTO vendor)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/deliveryvendors/" + vendor.DeliveryVendorId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(vendor);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteDeliveryVendor")]
        [ActionName("deleteDeliveryVendor")]
        public IHttpActionResult deleteDeliveryVendor(DeliveryVendorDTO vendor)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/deliveryvendors/" + vendor.DeliveryVendorId, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getDVendorCashes")]
        [ActionName("getDVendorCashes")]
        public IHttpActionResult getDVendorCashes([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/deliveryvendors/" + id + "/cashes?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region employee
        [HttpGet]
        [Route("getEmployees")]
        [ActionName("getEmployees")]
        public IHttpActionResult getEmployees([FromUri] int page, [FromUri] int page_size, [FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/employees?page=" + page + "&page_size=" + page_size + "&query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region branch
        [HttpGet]
        [Route("getBranches")]
        [ActionName("getBranches")]
        public IHttpActionResult getBranches()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/branches", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("getBranchesbyShopId")]
        [ActionName("getBranchesbyShopId")]
        public IHttpActionResult getBranchesbyShopId(EmpandUserDTO employee)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/"+ employee.ShopId + "/branches", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createBranch")]
        [ActionName("createBranch")]
        public IHttpActionResult createBranch(BranchDTO branch)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/branches", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            branch.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            branch.Status = 0;
            branch.IsMainBranch = false;
            request.AddJsonBody(branch);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updateBranch")]
        [ActionName("updateBranch")]
        public IHttpActionResult updateBranch(BranchDTO branch)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/branches/" + branch.BranchId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(branch);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteBranch")]
        [ActionName("deleteBranch")]
        public IHttpActionResult deleteEmployee(BranchDTO branch)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/branches/" + branch.BranchId, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(branch);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion brach

        #region exim
        [HttpGet]
        [Route("getExims")]
        [ActionName("getExims")]
        public IHttpActionResult getExims([FromUri] int page, [FromUri] int page_size, [FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/exims?page=" + page + "&page_size=" + page_size + "&query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getEximProducts")]
        [ActionName("getEximProducts")]
        public IHttpActionResult getEximProducts([FromUri] int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/exims/" + id + "/products?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getEximCashes")]
        [ActionName("getEximCashes")]
        public IHttpActionResult getEximCashes(int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/exims/" + id + "/cashe?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createCashFromExim")]
        [ActionName("createCashFromExim")]
        public IHttpActionResult createCashFromExim(CreateCash cash)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/cashes/" + HttpContext.Current.Session["ShopId"].ToString() + "/createcash-exims", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            cash.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(cash);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //[HttpPost]
        //[Route("updateExim")]
        //[ActionName("updateExim")]
        //public IHttpActionResult updateExim(EximDTO exim)
        //{
        //    RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
        //    var request = new RestRequest("api/exims/" + exim.EximId, Method.PUT);
        //    string author = "bearer ";
        //    author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
        //    request.AddHeader("Authorization", author);
        //    request.AddJsonBody(exim);
        //    IRestResponse response = client.Execute(request);
        //    var content = response.Content; // raw content as string
        //    JObject json = JObject.Parse(content);
        //    return Ok(json);
        //}
        #endregion

        #region inport
        [HttpPost]
        [Route("createImport")]
        [ActionName("createImport")]
        public IHttpActionResult createImport(CreateImport createImport)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/imports", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(createImport);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpPost]
        [Route("cancelImport")]
        [ActionName("cancelImport")]
        public IHttpActionResult cancelImport(EximDTO eximDTO)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/imports/" + eximDTO.EximId + "/cancel", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            CancelImport cancelImport = new CancelImport();
            cancelImport.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            cancelImport.EximId = eximDTO.EximId;
            request.AddJsonBody(cancelImport);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("completeImport")]
        [ActionName("completeImport")]
        public IHttpActionResult createImport(EximDTO eximDTO)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/imports/" + eximDTO.EximId + "/complete", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            CompleteImport completeImport = new CompleteImport();
            completeImport.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            completeImport.EximId = eximDTO.EximId;
            request.AddJsonBody(completeImport);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region cash
        [HttpGet]
        [Route("getCashTypes")]
        [ActionName("getCashTypes")]
        public IHttpActionResult getCashTypes([FromUri] int type)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/cashtypes?type=" + type, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getCashes")]
        [ActionName("getCashes")]
        public IHttpActionResult getCashes([FromUri] int page, [FromUri] int page_size, [FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/cashes?page=" + page + "&page_size=" + page_size + "&query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpPost]
        [Route("updateCash")]
        [ActionName("updateCash")]
        public IHttpActionResult updateCash(CashDTO cash)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/cashes/" + cash.CashId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(cash);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createCashType")]
        [ActionName("createCashType")]
        public IHttpActionResult createCashType(CashTypeDTO cashType)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/cashtypes", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(cashType);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createCash")]
        [ActionName("createCash")]
        public IHttpActionResult createCash(CreateCash cash)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/cashes", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            cash.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(cash);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("cancelCashes")]
        [ActionName("cancelCashes")]
        public IHttpActionResult cancelCashes(CashDTO cash)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/cashes/" + cash.CashId + "/cancel", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            CancelCashes cancelCash = new CancelCashes();
            cancelCash.CashId = cash.CashId;
            cancelCash.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(cancelCash);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region order
        [HttpGet]
        [Route("getOrders")]
        [ActionName("getOrders")]
        public IHttpActionResult getOrders([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/orders?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getOrderProducts")]
        [ActionName("getOrderProducts")]
        public IHttpActionResult getOrderProducts([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/orders/" + id + "/products", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getOrderCashes")]
        [ActionName("getOrderCashes")]
        public IHttpActionResult getOrderCashes(int id, [FromUri] int page, [FromUri] int page_size)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/orders/" + id + "/cashe?page=" + page + "&page_size=" + page_size, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createOrder")]
        [ActionName("createOrder")]
        public IHttpActionResult createOrder(CreateOrder createOrder)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/orders", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            createOrder.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            createOrder.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(createOrder);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpPost]
        [Route("updateOrder")]
        [ActionName("updateOrder")]
        public IHttpActionResult updateOrder(OrderDTO order)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/orders/" + order.OrderId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(order);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("cancelOrder")]
        [ActionName("cancelOrder")]
        public IHttpActionResult cancelOrder(OrderDTO order)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/orders/" + order.OrderId + "/cancel", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            CancelOrder cancelOrder = new CancelOrder();
            cancelOrder.OrderId = order.OrderId;
            cancelOrder.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(cancelOrder);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createCashFromOrder")]
        [ActionName("createCashFromOrder")]
        public IHttpActionResult createCashFromOrder(CreateCash cash)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/cashes/" + HttpContext.Current.Session["ShopId"].ToString() + "/createcash-order", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            cash.EmployeeId = Int32.Parse(HttpContext.Current.Session["EmployeeId"].ToString());
            request.AddJsonBody(cash);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region returns
        [HttpGet]
        [Route("getReturns")]
        [ActionName("getReturns")]
        public IHttpActionResult getReturns([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/returns?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getReturnProducts")]
        [ActionName("getReturnProducts")]
        public IHttpActionResult getReturnProducts([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/orders/" + id + "/products", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getReturnCashes")]
        [ActionName("getReturnCashes")]
        public IHttpActionResult getReturnCashes([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/returns/" + id + "/cashes", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getReturnOrder")]
        [ActionName("getReturnOrder")]
        public IHttpActionResult getReturnOrder([FromUri] string code)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/orders/" + HttpContext.Current.Session["ShopId"].ToString() + "/" + code + "/returnorder", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updateReturn")]
        [ActionName("updateReturn")]
        public IHttpActionResult updateReturn(ReturnDTO _return)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/returns/" + _return.ReturnId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(_return);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("returnOrder")]
        [ActionName("returnOrder")]
        public IHttpActionResult returnOrder(ReturnOrder returnOrder)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/orders/" + returnOrder.OrderId + "/return", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(returnOrder);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region employee

        [HttpGet]
        [Route("getShopEmployees")]
        [ActionName("getShopEmployees")]
        public IHttpActionResult getShopEmployees()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/employeedetail", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("createEmployee")]
        [ActionName("createEmployee")]
        public IHttpActionResult createEmployee(EmployeeDTO employee)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(employee);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updateEmployee")]
        [ActionName("updateEmployee")]
        public IHttpActionResult updateEmployee(EmployeeDTO employee)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/PutEmployeebyAdmin" + employee.EmployeeId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(employee);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpPost]
        [Route("updateEmployeebyAdmin")]
        [ActionName("updateEmployeebyAdmin")]
        public IHttpActionResult updateEmployeebyAdmin(EmpandUserDTO employeeold)
        {

            EmployeeDTO employee = new EmployeeDTO();
            employee.EmployeeId = employeeold.EmployeeId;
            employee.UserId = employeeold.UserId;
            employee.ShopId = employeeold.ShopId;
            employee.BranchId = employeeold.BranchId;
            employee.EmployeeName = employeeold.EmployeeName;
            employee.Password = employeeold.Password;
            employee.Address = employeeold.Address;
            employee.Phone = employeeold.Phone;
            employee.Email = employeeold.Email;
            employee.Status = employeeold.Status;
            employee.GroupId = employeeold.GroupId;
            employee.Group = employeeold.Group;
            employee.CreatedAt = employeeold.CreatedAt;
            employee.UpdatedAt = employeeold.UpdatedAt;
            employee.TokenSince = employeeold.TokenSince;
            employee.FacebookUserId = employeeold.FacebookUserId;


            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/PutEmployeebyAdmin/" + employee.EmployeeId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(employee);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpPost]
        [Route("lockEmployee")]
        [ActionName("lockEmployee")]
        public IHttpActionResult lockEmployee(EmpandUserDTO employeeold)
        {
            EmployeeDTO employee = new EmployeeDTO();
            employee.EmployeeId = employeeold.EmployeeId;
            employee.UserId = employeeold.UserId;
            employee.ShopId = employeeold.ShopId;
            employee.BranchId = employeeold.BranchId;
            employee.EmployeeName = employeeold.EmployeeName;
            employee.Password = employeeold.Password;
            employee.Address = employeeold.Address;
            employee.Phone = employeeold.Phone;
            employee.Email = employeeold.Email;
            employee.Status = employeeold.Status;
            employee.GroupId = employeeold.GroupId;
            employee.Group = employeeold.Group;
            employee.CreatedAt = employeeold.CreatedAt;
            employee.UpdatedAt = employeeold.UpdatedAt;
            employee.TokenSince = employeeold.TokenSince;
            employee.FacebookUserId = employeeold.FacebookUserId;

            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/PutEmployeebyAdmin/" + employee.EmployeeId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(employee);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        [HttpPost]
        [Route("unLockEmployee")]
        [ActionName("unLockEmployee")]
        public IHttpActionResult UnlockEmployee(EmpandUserDTO employeeold)
        {
            EmployeeDTO employee = new EmployeeDTO();
            employee.EmployeeId = employeeold.EmployeeId;
            employee.UserId = employeeold.UserId;
            employee.ShopId = employeeold.ShopId;
            employee.BranchId = employeeold.BranchId;
            employee.EmployeeName = employeeold.EmployeeName;
            employee.Password = employeeold.Password;
            employee.Address = employeeold.Address;
            employee.Phone = employeeold.Phone;
            employee.Email = employeeold.Email;
            employee.Status = employeeold.Status;
            employee.GroupId = employeeold.GroupId;
            employee.Group = employeeold.Group;
            employee.CreatedAt = employeeold.CreatedAt;
            employee.UpdatedAt = employeeold.UpdatedAt;
            employee.TokenSince = employeeold.TokenSince;
            employee.FacebookUserId = employeeold.FacebookUserId;


            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/PutEmployeebyAdmin/" + employee.EmployeeId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(employee);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("deleteEmployee")]
        [ActionName("deleteEmployee")]
        public IHttpActionResult deleteEmployee(EmployeeDTO employee)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/employees/" + employee.EmployeeId, Method.DELETE);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(employee);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region chart

        [HttpGet]
        [Route("getTodayStats")]
        [ActionName("getTodayStats")]
        public IHttpActionResult getTodayStats([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/todaystats?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region pages

        [HttpGet]
        [Route("getPages")]
        [ActionName("getPages")]
        public IHttpActionResult getPages()
        {
            //check if has token or not
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/pages", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getPageList")]
        [ActionName("getPageList")]
        public async Task<IHttpActionResult> getPageList()
        {
            //check if has token or not
            if (HttpContext.Current.Session["UserFacebookToken"] != null && !HttpContext.Current.Session["UserFacebookToken"].ToString().Equals(""))
            {
                string urlLongLiveToken;
                if (isLocalDebug)
                    urlLongLiveToken = "/oauth/access_token?grant_type=fb_exchange_token&client_id=390808924586266&client_secret=ee73cef9a3c1b75027b0c93f0e9b994f&fb_exchange_token=" + HttpContext.Current.Session["UserFacebookToken"].ToString();
                else
                    urlLongLiveToken = "/oauth/access_token?grant_type=fb_exchange_token&client_id=313601012371988&client_secret=a993e6c800e78daf7f299f15e933b588&fb_exchange_token=" + HttpContext.Current.Session["UserFacebookToken"].ToString();
                var client = new RestClient("https://graph.facebook.com");
                var request = new RestRequest(urlLongLiveToken, Method.GET);
                var response = client.Execute(request);
                var content = response.Content; // raw content as string
                string longLiveToken = "";
                log.Info("get long-term user token : " + content);
                if (content != null && content.Length > 0)
                {
                    LongTermToken token = JsonConvert.DeserializeObject<LongTermToken>(content);
                    longLiveToken = token.access_token;
                    //access_token=EAAEdNZBzxkhQBAO9O0DKvCGZBzlMUH7eOCPZCKlzDc7pg2wtT3ZCDOZCRkE6LLqFmbLayLrDMSlZAN6Aeo5TtZCzrGLCLaEygtwZA2nZAKewiBUB8Xz88EEoLzQKMhdQ1RPgqEYv95NNFjKk18yLrl9Xt9GXIsW7O6gcZD&expires=5183849
                    //content = content.Replace("access_token=", "");
                    //if (content.IndexOf("&expires") > -1)
                    //    content = content.Substring(0, content.IndexOf("&expires"));
                    //longLiveToken = content;
                    //HttpContext.Current.Session["UserFacebookToken"] = content;
                }
                client = new RestClient("https://graph.facebook.com");
                request = new RestRequest("me/accounts/?access_token=" + longLiveToken, Method.GET);
                response = client.Execute(request);
                content = response.Content; // raw content as string
                JObject json = JObject.Parse(content);
                return Ok(json);
            }
            else
            {
                return Ok();
            }
        }

        [HttpGet]
        [Route("RefreshPageAndToken")]
        [ActionName("RefreshPageAndToken")]
        public IHttpActionResult RefreshPageAndToken()
        {
            //check if has token or not
            if (HttpContext.Current.Session["UserFacebookToken"] != null && !HttpContext.Current.Session["UserFacebookToken"].ToString().Equals(""))
            {
                string urlLongLiveToken;
                if (isLocalDebug)
                    urlLongLiveToken = "/oauth/access_token?grant_type=fb_exchange_token&client_id=390808924586266&client_secret=ee73cef9a3c1b75027b0c93f0e9b994f&fb_exchange_token=" + HttpContext.Current.Session["UserFacebookToken"].ToString();
                else
                    urlLongLiveToken = "/oauth/access_token?grant_type=fb_exchange_token&client_id=313601012371988&client_secret=a993e6c800e78daf7f299f15e933b588&fb_exchange_token=" + HttpContext.Current.Session["UserFacebookToken"].ToString();
                var client = new RestClient("https://graph.facebook.com");
                var request = new RestRequest(urlLongLiveToken, Method.GET);
                var response = client.Execute(request);
                var content = response.Content; // raw content as string
                string longLiveToken = "";
                if (content != null && content.Length > 0)
                {
                    LongTermToken token = JsonConvert.DeserializeObject<LongTermToken>(content);
                    longLiveToken = token.access_token;
                    //access_token=EAAEdNZBzxkhQBAO9O0DKvCGZBzlMUH7eOCPZCKlzDc7pg2wtT3ZCDOZCRkE6LLqFmbLayLrDMSlZAN6Aeo5TtZCzrGLCLaEygtwZA2nZAKewiBUB8Xz88EEoLzQKMhdQ1RPgqEYv95NNFjKk18yLrl9Xt9GXIsW7O6gcZD&expires=5183849
                    //content = content.Replace("access_token=", "");
                    //if (content.IndexOf("&expires") > -1)
                    //    content = content.Substring(0, content.IndexOf("&expires"));
                    //longLiveToken = content;
                    //HttpContext.Current.Session["UserFacebookToken"] = content;
                }
                //generate long live access token
                client = new RestClient("https://graph.facebook.com");
                request = new RestRequest("me/accounts/?access_token=" + longLiveToken, Method.GET);
                response = client.Execute(request);
                content = response.Content; // raw content as string
                string pageRequest = content;
                //log.Info("login - long term page token :" + content);
                FBAccountResp resp = JsonConvert.DeserializeObject<FBAccountResp>(content);
                if (resp != null)
                {
                    List<String> ids = new List<string>();
                    List<String> tokens = new List<string>();
                    if (resp.data != null && resp.data.Count > 0)
                    {
                        foreach (FBPage page in resp.data)
                        {
                            ids.Add(page.id);
                            tokens.Add(page.access_token);
                        }
                    }
                    UpdatePageTokenReq updatePage = new UpdatePageTokenReq();
                    updatePage.ids = ids;
                    updatePage.tokens = tokens;
                    //update
                    client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
                    request = new RestRequest("api/pages/UpdatePageToken", Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    string author = "bearer ";
                    author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
                    request.AddHeader("Authorization", author);
                    request.AddJsonBody(updatePage);
                    response = client.Execute(request);
                    content = response.Content;
                    JObject json = JObject.Parse(pageRequest);
                    return Ok(json);
                }
                else
                {
                    return Ok();
                }
            }
            else
            {
                return Ok();
            }
        }

        [HttpPost]
        [Route("linkPage")]
        [ActionName("linkPage")]
        public IHttpActionResult linkPage(FSalePage page)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/pages", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            PageDTO pageDTO = new PageDTO();
            pageDTO.PageFacebookId = page.id;
            pageDTO.PageFacebookToken = page.access_token;
            pageDTO.PageName = page.page_name;
            pageDTO.ShopId = Int32.Parse(HttpContext.Current.Session["ShopId"].ToString());
            pageDTO.Status = 0;
            request.AddJsonBody(pageDTO);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        [HttpPost]
        [Route("unlinkPage")]
        [ActionName("unlinkPage")]
        public IHttpActionResult unlinkPage(Page page)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/pages/" + page.PageId + "/removeSubscribed", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #region transfer
        [HttpGet]
        [Route("getTransfers")]
        [ActionName("getTransfers")]
        public IHttpActionResult getTransfers([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/transfers?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getTransferProducts")]
        [ActionName("getTransferProducts")]
        public IHttpActionResult getTransferProducts([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/transfers/" + id + "/products", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("transferProduct")]
        [ActionName("transferProduct")]
        public IHttpActionResult exchangeProduct(Transfer transfer)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/transfers", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(transfer);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("updateTransfer")]
        [ActionName("updateTransfer")]
        public IHttpActionResult updateTransfer(TransferDTO transfer)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/transfers/" + transfer.TransferId, Method.PUT);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(transfer);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region report

        #region Hàng ngày
        [HttpGet]
        [Route("reportDateSale")]
        [ActionName("reportDateSale")]
        public IHttpActionResult reportDateSale([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/date-sale?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/date-sale?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);

            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportDateCashes")]
        [ActionName("reportDateCashes")]
        public IHttpActionResult reportDateCashes([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/date-cashe?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/date-cashe?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportDateProduct")]
        [ActionName("reportDateProduct")]
        public IHttpActionResult reportDateProduct([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/date-product?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/date-product?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region Bán hàng
        //Báo cáo bán hàng theo thời gian
        [HttpGet]
        [Route("reportSaleTime")]
        [ActionName("reportSaleTime")]
        public IHttpActionResult reportSaleTime([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/sale-time?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/sale-time?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            //string idshop = HttpContext.Current.Session["ShopId"].ToString();
            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/sale-time?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ bán hàng theo thời gian
        [HttpGet]
        [Route("reportSaleTimeChart")]
        [ActionName("reportSaleTimeChart")]
        public IHttpActionResult reportSaleTimeChart([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/sale-time-chart?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Báo cáo lơi nhuận theo hóa đơn
        [HttpGet]
        [Route("reportSaleProfit")]
        [ActionName("reportSaleProfit")]
        public IHttpActionResult reportSaleProfit([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/sale-profit?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/sale-profit?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/sale-profit?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ lơi nhuận theo hóa đơn
        [HttpGet]
        [Route("reportSaleProfitChart")]
        [ActionName("reportSaleProfitChart")]
        public IHttpActionResult reportSaleProfitChart([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();
            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/sale-profit-chart?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Báo cáo trả hàng theo hóa đơn
        [HttpGet]
        [Route("reportSaleReturnProduct")]
        [ActionName("reportSaleReturnProduct")]
        public IHttpActionResult reportSaleReturnProduct([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/sale-return-product?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/sale-return-product?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportSaleSubPrice")]
        [ActionName("reportSaleSubPrice")]
        public IHttpActionResult reportSaleSubPrice([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/sale-subprice?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/sale-subprice?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/sale-subprice?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportSaleEmployees")]
        [ActionName("reportSaleEmployees")]
        public IHttpActionResult reportSaleEmployees([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/sale-employees?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/sale-employees?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region Đặt hàng
        //Báo cáo đặt hàng theo hàng hóa
        [HttpGet]
        [Route("reportOrderProduct")]
        [ActionName("reportOrderProduct")]
        public IHttpActionResult reportOrderProduct([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string idshop = HttpContext.Current.Session["ShopId"].ToString();
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/order-product?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ top 10 hàng hóa được đặt nhiều nhất
        [HttpGet]
        [Route("reportOrderProductChart")]
        [ActionName("reportOrderProductChart")]
        public IHttpActionResult reportOrderProductChart([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string idshop = HttpContext.Current.Session["ShopId"].ToString();
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/order-product-chart?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Báo cáo danh sách đơn đặt hàng
        [HttpGet]
        [Route("reportOrderExchange")]
        [ActionName("reportOrderExchange")]
        public IHttpActionResult reportOrderExchange([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string idshop = HttpContext.Current.Session["ShopId"].ToString();
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/order-exchange?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ Top 10 khách hàng đặt hàng nhiều nhất
        [HttpGet]
        [Route("reportOrderExchangeChart")]
        [ActionName("reportOrderExchangeChart")]
        public IHttpActionResult reportOrderExchangeChart([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string idshop = HttpContext.Current.Session["ShopId"].ToString();
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/order-exchange-chart?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region Hàng hóa
        [HttpGet]
        [Route("reportProductSale")]
        [ActionName("reportProductSale")]
        public IHttpActionResult reportProductSale([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/product-sale?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/product-sale?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);

            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductSaleChart1")]
        [ActionName("reportProductSaleChart1")]
        public IHttpActionResult reportProductSaleChart1([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();
            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/product-sale-chart1?query=" + query, Method.GET);

            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/product-sale-chart1?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductSaleChart2")]
        [ActionName("reportProductSaleChart2")]
        public IHttpActionResult reportProductSaleChart2([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();
            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/product-sale-chart2?query=" + query, Method.GET);

            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/product-sale-chart2?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductProfit")]
        [ActionName("reportProductProfit")]
        public IHttpActionResult reportProductProfit([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/product-profit?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/product-profit?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/product-profit?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductProfitChart1")]
        [ActionName("reportProductProfitChart1")]
        public IHttpActionResult reportProductProfitChart1([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();
            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/product-profit-chart1?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductProfitChart2")]
        [ActionName("reportProductProfitChart2")]
        public IHttpActionResult reportProductProfitChart2([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();
            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/product-profit-chart2?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductExims")]
        [ActionName("reportProductExims")]
        public IHttpActionResult reportProductExims([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/product-exims?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/product-exims?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductEmployees")]
        [ActionName("reportProductEmployees")]
        public IHttpActionResult reportProductEmployees([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/product-employees?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/product-employees?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductCustomer")]
        [ActionName("reportProductCustomer")]
        public IHttpActionResult reportProductCustomer([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/product-customer?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/product-customer?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportProductVendor")]
        [ActionName("reportProductVendor")]
        public IHttpActionResult reportProductVendor([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/product-vendor?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/product-vendor?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/product-vendor?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region Khách hàng
        [HttpGet]
        [Route("reportCustomerSale")]
        [ActionName("reportCustomerSale")]
        public IHttpActionResult reportCustomerSale([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/customer-sale?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportCustomerSaleChart")]
        [ActionName("reportCustomerSaleChart")]
        public IHttpActionResult reportCustomerSaleChart([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/customer-sale-chart?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportCustomerProfit")]
        [ActionName("reportCustomerProfit")]
        public IHttpActionResult reportCustomerProfit([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/customer-profit?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportCustomerDebts")]
        [ActionName("reportCustomerDebts")]
        public IHttpActionResult reportCustomerDebts([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/customer-debts?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportCustomerDebtsChart")]
        [ActionName("reportCustomerDebtsChart")]
        public IHttpActionResult reportCustomerDebtsChart([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/customer-debts-chart?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("reportCustomerProduct")]
        [ActionName("reportCustomerProduct")]
        public IHttpActionResult reportCustomerProduct([FromUri] string query)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/customer-product?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #region Tài chính
        //Báo cáo tài chính theo tháng
        [HttpGet]
        [Route("reportFinanceMonth")]
        [ActionName("reportFinanceMonth")]
        public IHttpActionResult reportFinanceMonth([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            string url = "";
            if (order_by == null || order_by.Trim() == "")
                url = "api/reports/" + shopId + "/" + branchId + "/sale-time?page=" + page + "&page_size=" + page_size + "&query=" + query;
            else
                url = "api/reports/" + shopId + "/" + branchId + "/sale-time?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            var request = new RestRequest(url, Method.GET);
            //string idshop = HttpContext.Current.Session["ShopId"].ToString();
            //var request = new RestRequest("api/reports/" + HttpContext.Current.Session["ShopId"].ToString() + "/sale-time?query=" + query, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ lợi nhuận theo tháng
        [HttpGet]
        [Route("reportFinanceMonthChart")]
        [ActionName("reportFinanceMonthChart")]
        public IHttpActionResult reportFinanceMonthChart()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/finance-month-chart", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ lợi nhuận theo quý
        [HttpGet]
        [Route("reportFinanceQuarterChart")]
        [ActionName("reportFinanceQuarterChart")]
        public IHttpActionResult reportFinanceQuarterChart()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/finance-quarter-chart", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        //Biều đồ lợi nhuận theo năm
        [HttpGet]
        [Route("reportFinanceYearChart")]
        [ActionName("reportFinanceYearChart")]
        public IHttpActionResult reportFinanceYearChart()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string shopId = HttpContext.Current.Session["ShopId"].ToString();
            string branchId = HttpContext.Current.Session["BranchId"].ToString();

            var request = new RestRequest("api/reports/" + shopId + "/" + branchId + "/finance-year-chart", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion

        #endregion

        #region import return
        [HttpGet]
        [Route("getReturnImport")]
        [ActionName("getReturnImport")]
        public IHttpActionResult getReturnImport([FromUri] string code)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/imports/" + code, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpPost]
        [Route("returnImport")]
        [ActionName("returnImport")]
        public IHttpActionResult returnImport(ReturnImport returnImport)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/imports/return", Method.POST);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            request.AddJsonBody(returnImport);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        #endregion

        #region vendor return     
        [HttpGet]
        [Route("getImportReturns")]
        [ActionName("getImportReturns")]
        public IHttpActionResult getImportReturns([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/shops/" + HttpContext.Current.Session["ShopId"].ToString() + "/importreturns?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getImportReturnProducts")]
        [ActionName("getImportReturnProducts")]
        public IHttpActionResult getImportReturnProducts([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/exims/" + id + "/products?page=1&page_size=20", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getImportReturnCashes")]
        [ActionName("getImportReturnCashes")]
        public IHttpActionResult getImportReturnCashes([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/exims/" + id + "/cashes?page=1&page_size=20", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        #endregion vendor return

        #region Employeeuser return

        [HttpGet]
        [Route("GettUserPage")]
        [ActionName("GettUserPage")]
        public IHttpActionResult GettUserPage([FromUri] int page, [FromUri] int page_size, [FromUri] string query, [FromUri] string order_by)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            string url = "";
            if (order_by != null && order_by.Trim() != "")
                url = "api/employees/GetUserPage?page=" + page + "&page_size=" + page_size + "&query=" + query + "&order_by=" + order_by;
            else
                url = "api/employees/GetUserPage?page=" + page + "&page_size=" + page_size + "&query=" + query;
            var request = new RestRequest(url, Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
        /*
        [HttpGet]
        [Route("getImportReturnProducts")]
        [ActionName("getImportReturnProducts")]
        public IHttpActionResult getImportReturnProducts([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/exims/" + id + "/products?page=1&page_size=20", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        [HttpGet]
        [Route("getImportReturnCashes")]
        [ActionName("getImportReturnCashes")]
        public IHttpActionResult getImportReturnCashes([FromUri] int id)
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["baseApiUrl"].ToString());
            var request = new RestRequest("api/exims/" + id + "/cashes?page=1&page_size=20", Method.GET);
            string author = "bearer ";
            author += HttpContext.Current.Session["access_token"] != null ? HttpContext.Current.Session["access_token"].ToString() : "";
            request.AddHeader("Authorization", author);
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            JObject json = JObject.Parse(content);
            return Ok(json);
        }*/
        #endregion Employeeuser return
    }
}
