﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSale.Models
{
    public class FilteredPagination : BasePagination
    {
        [System.ComponentModel.DefaultValue("")]
        public string query { get; set; }
    }
}