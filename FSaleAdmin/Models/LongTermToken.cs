﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSale.Models
{
    public class LongTermToken
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}