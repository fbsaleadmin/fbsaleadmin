﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSale.Models
{
    public class BasePagination
    {
        [System.ComponentModel.DefaultValue(1)]
        public int page { get; set; }
        [System.ComponentModel.DefaultValue(20)]
        public int page_size { get; set; }
        public string order_by { get; set; }
    }
}