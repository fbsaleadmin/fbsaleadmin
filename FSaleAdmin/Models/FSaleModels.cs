﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSale.Models
{
    public class FSalePage
    {
        public string id { get; set; }
        public string page_name { get; set; }
        public string access_token { get; set; }
    }
}