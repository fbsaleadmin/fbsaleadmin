﻿using System.Web;
using System.Web.Optimization;

namespace FSale
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/lib/dist/css/bootstrap.min.css",       
                      //"~/Content/ui-bootstrap-csp.css",
                      "~/Content/ngDialog.min.css",
                       "~/develop/css/styleMain.css",
                      "~/develop/css/color.css",                     
                      "~/develop/fonts/font-awesome/css/font-awesome.min.css",
                      "~/Content/ngDialog-theme-default.css",
                      "~/develop/slides-img/css-zoom/css_product.css",
                      "~/Content/datetimepicker.css",
                      "~/Content/loading-bar.min.css",
                      "~/Content/angular-material.css",
                      "~/Content/print.css",
                      "~/Content/ngPrint.min.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/appjs").Include(                        
                        "~/Scripts/EventEmitter.js",
                        "~/Scripts/imagesloaded.js",
                        "~/Scripts/masonry.pkgd.min.js",
                        "~/develop/js/jsMain.js",
                        "~/develop/ckeditor/ckeditor.js",
                        "~/develop/AccordionList/js/accordion.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/angularjs")
                .Include("~/Scripts/angular.min.js",
                "~/Scripts/angular-mocks.js",
                "~/Scripts/angular-route.min.js",
                "~/Scripts/ngdialog.min.js",
                "~/Scripts/ng-file-upload.min.js",
                "~/Scripts/angular-md5.js",
                "~/Scripts/angular-masonry.min.js",
                "~/Scripts/angular-ui/ui-bootstrap.min.js",
                "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js",
                "~/Scripts/angular-timeago.min.js",
                "~/Scripts/Chart.bundle.js",
                "~/Scripts/angular-chart.min.js",
                "~/Scripts/moment.js",
                "~/Scripts/datetimepicker.js",
                "~/Scripts/datetimepicker.templates.js",
                "~/Scripts/loading-bar.min.js",
                "~/Scripts/export/ngPrint.min.js",
                "~/Scripts/export/FileSaver.min.js",
                "~/Scripts/export/json-export-excel.min.js",
                "~/Scripts/angular-animate.min.js",
                "~/Scripts/angular-aria.min.js",
                "~/Scripts/angular-messages.min.js",
                "~/Scripts/angular-material.min.js",              
                "~/Scripts/app/app.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/export").Include(
                "~/Scripts/export/tableExport.js",
                "~/Scripts/export/base64.js",
                "~/Scripts/export/jquery.base64.js",
                "~/Scripts/export/jspdf.js",
                "~/Scripts/export/sprintf.js"               
                ));

            bundles.Add(new ScriptBundle("~/bundles/report").Include(
                "~/Scripts/reports/js/pdfmake.js",
                "~/Scripts/reports/js/vfs_fonts.js"

                //"~/Scripts/reports/js/jspdf.js",
                //"~/Scripts/reports/js/addimage.js",
                //"~/Scripts/reports/js/from_html.js",
                //"~/Scripts/reports/js/split_text_to_size.js",
                //"~/Scripts/reports/js/standard_fonts_metrics.js",
                //"~/Scripts/reports/js/jspdf.debug.js",
                //"~/Scripts/reports/js/html2pdf.js",
                //"~/Scripts/reports/js/basic.js"
                //"~/Scripts/reports/js/angular-pdf-viewer.min.js",
                //"~/Scripts/reports/js/pdf.combined.js",
                //"~/Scripts/pdfview/l10n.js",
                //"~/Scripts/pdfview/build/pdf.js",
                //"~/Scripts/pdfview/viewer.js"
                ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
