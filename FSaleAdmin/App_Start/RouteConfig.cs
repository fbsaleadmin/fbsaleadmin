﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FSale
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "RouteHomeIndex",
                url: "index.html",
                defaults: new { controller = "LandingPage", action = "Index" }
            );

            routes.MapRoute(
                name: "RouteSelectPage",
                url: "select-page.html",
                defaults: new { controller = "LandingPage", action = "SelectPage" }
            );

            routes.MapRoute(
                name: "RouteHomeLogin",
                url: "login.html",
                defaults: new { controller = "Login", action = "Login" }
            );

            routes.MapRoute(
                name: "RouteCreateShop",
                url: "createshop.html",
                defaults: new { controller = "Login", action = "CreateShop" }
            );

            routes.MapRoute(
                name: "RouteHome",
                url: "home.html",
                defaults: new { controller = "Logon", action = "Index" }
            );

            routes.MapRoute(
                name: "RouteShopHome",
                url: "shop.html",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "RouteShopCategory",
                url: "category.html",
                defaults: new { controller = "Home", action = "Category" }
            );

            routes.MapRoute(
                name: "RouteShopProduct",
                url: "products.html",
                defaults: new { controller = "Home", action = "Products" }
            );

            routes.MapRoute(
                name: "RouteShopCustomer",
                url: "customers.html",
                defaults: new { controller = "Home", action = "Customers" }
            );

            routes.MapRoute(
                name: "RouteShopPrice",
                url: "prices.html",
                defaults: new { controller = "Home", action = "Prices" }
            );

            routes.MapRoute(
                name: "RouteShopVendor",
                url: "vendors.html",
                defaults: new { controller = "Home", action = "Vendors" }
            );

            routes.MapRoute(
                name: "RouteShopDeliveryVendor",
                url: "delivery-vendors.html",
                defaults: new { controller = "Home", action = "DeliveryVendors" }
            );

            routes.MapRoute(
                name: "RouteShopImport",
                url: "imports.html",
                defaults: new { controller = "Home", action = "Imports" }
            );

            routes.MapRoute(
                name: "RouteShopCash",
                url: "cashes.html",
                defaults: new { controller = "Home", action = "Cashes" }
            );
            routes.MapRoute(
                name: "RouteShopDebt",
                url: "debts.html",
                defaults: new { controller = "Home", action = "Debts" }
            );

            routes.MapRoute(
                name: "RouteShopExim",
                url: "exims.html",
                defaults: new { controller = "Home", action = "Exims" }
            );

            routes.MapRoute(
                name: "RouteShopOrder",
                url: "orders.html",
                defaults: new { controller = "Home", action = "Orders" }
            );

            routes.MapRoute(
                name: "RouteShopReturn",
                url: "returns.html",
                defaults: new { controller = "Home", action = "Returns" }
            );

            routes.MapRoute(
                name: "RouteShopEmployee",
                url: "employees.html",
                defaults: new { controller = "Home", action = "Employees" }
            );

            routes.MapRoute(
                name: "RouteLogout",
                url: "logout.html",
                defaults: new { controller = "Home", action = "Logout" }
            );

            routes.MapRoute(
              name: "RouteSales",
              url: "sales.html",
              defaults: new { controller = "Home", action = "Sales" }
          );

            routes.MapRoute(
                name: "RouteShopTransfer",
                url: "transfers.html",
                defaults: new { controller = "Home", action = "Transfers" }
            );

            routes.MapRoute(
                name: "RouteShopBranchTransfer",
                url: "branch-transfers.html",
                defaults: new { controller = "Home", action = "BranchTransfers" }
            );

            routes.MapRoute(
                name: "RouteShopBranch",
                url: "branches.html",
                defaults: new { controller = "Home", action = "Branches" }
            );

            routes.MapRoute(
                name: "RouteShopPages",
                url: "pages.html",
                defaults: new { controller = "Home", action = "Pages" }
            );

            routes.MapRoute(
                name: "RouteLinkPage",
                url: "link-page.html",
                defaults: new { controller = "Login", action = "LinkPage" }
            );

            routes.MapRoute(
                name: "RouteChat",
                url: "chat.html",
                defaults: new { controller = "Home", action = "Chat" }
            );

            routes.MapRoute(
                name: "RouteCustomerReturn",
                url: "customer-returns.html",
                defaults: new { controller = "Home", action = "CustomerReturns" }
            );

            //Report
            routes.MapRoute(
                name: "RouteReportEveryDay",
                url: "report-everyday.html",
                defaults: new { controller = "Home", action = "ReportEveryDay" }
            );

            routes.MapRoute(
                name: "RouteReportSale",
                url: "report-sale.html",
                defaults: new { controller = "Home", action = "ReportSale" }
            );

            routes.MapRoute(
                name: "RouteReportSales",
                url: "report-sales.html",
                defaults: new { controller = "Home", action = "ReportSales" }
            );

            routes.MapRoute(
                name: "RouteReportCashes",
                url: "report-cashes.html",
                defaults: new { controller = "Home", action = "ReportCashes" }
            );

            routes.MapRoute(
                name: "RouteReportOrder",
                url: "report-order.html",
                defaults: new { controller = "Home", action = "ReportOrder" }
            );

            routes.MapRoute(
                name: "RouteReportProduct",
                url: "report-product.html",
                defaults: new { controller = "Home", action = "ReportProduct" }
            );

            routes.MapRoute(
                name: "RouteReportCustomer",
                url: "report-customer.html",
                defaults: new { controller = "Home", action = "ReportCustomer" }
            );

            routes.MapRoute(
                name: "RouteReportVendor",
                url: "report-vendor.html",
                defaults: new { controller = "Home", action = "ReportVendor" }
            );

            routes.MapRoute(
                name: "RouteReportEmployees",
                url: "report-employees.html",
                defaults: new { controller = "Home", action = "ReportEmployees" }
            );

            routes.MapRoute(
                name: "RouteReportFinance",
                url: "report-finance.html",
                defaults: new { controller = "Home", action = "ReportFinance" }
            );

            //End report

            routes.MapRoute(
                name: "RouteDummy",
                url: "dummy.html",
                defaults: new { controller = "Login", action = "Dummy" }
            );

            //Error Page
            routes.MapRoute(
                name: "RouteToError",
                url: "error.html",
                defaults: new { controller = "Home", action = "Error" }
            );

            //import return
            routes.MapRoute(
               name: "RouteToImportReturn",
               url: "imports/{code}/return.html",
               defaults: new { controller = "Home", action = "ImportReturns" }
            );
            routes.MapRoute(
               name: "RouteToVendorReturns",
               url: "vendor-returns.html",
               defaults: new { controller = "Home", action = "VendorReturns" }
            );

            // detail action by Phung The Nhan
            routes.MapRoute(
                name: "RouteActions",
                url: "actions.html",
                defaults: new { controller = "Home", action = "Actions" }
            );

            routes.MapRoute(
                name: "RouteDetailAction",
                url: "detail-action.html",
                defaults: new { controller = "Home", action = "DetailAction" }
            );
            //end action

            routes.MapRoute(
                name: "CMS",
                url: "cmsadmin.html",
                defaults: new { controller = "Home", action = "CMSadmin" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "LandingPage", action = "Index", id = UrlParameter.Optional }
                //defaults: new { controller = "Home", action = "CMSadmin" }
            );
        }
    }
}
